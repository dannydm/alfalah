<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.siswa');

// create application
alfalah.siswa.receivable = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.forms.initialize();
            this.banktransfer.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.banktransfer.Tab);
            this.centerPanel.add(this.forms.Tab);
            this.centerPanel.setActiveTab(this.forms.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.siswa.receivable.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.siswa.receivable.centerPanel.container.dom.clientHeight+90;
            this.Columns = [
                // {   header: "Id", width : 50,
                //     dataIndex : 'keyid', sortable: true,
                //     tooltip:"Id",
                // },
                {   header: "Grade", width : 50,
                    dataIndex : 'nama_jenjang_siswa', sortable: true,
                    tooltip:"Grade",
                },
                {   header: "Nama Siswa", width : 200,
                    dataIndex : 'nama_siswa', sortable: true,
                    tooltip:"Nama Siswa",
                },
                {   header: "Tahun / Bulan", width : 100,
                    dataIndex : 'bultah', sortable: true,
                    tooltip:"Tahun Bulan",
                },
                {   header: "Biaya", width : 100,
                    dataIndex : 'nama_biaya_sekolah', sortable: true,
                    tooltip:"Biaya Sekolah",
                },
                {   header: "Bulan", width : 100,
                    dataIndex : 'bulan', sortable: true,
                    tooltip:"Bulan",
                },
                {   header: "Piutang", width : 100,
                    dataIndex : 'piutang', sortable: true,
                    tooltip:"Piutang",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Potongan", width : 100,
                    dataIndex : 'potongan', sortable: true,
                    tooltip:"Potongan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Pembayaran", width : 100,
                    dataIndex : 'pembayaran', sortable: true,
                    tooltip:"Pembayaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'siswa_id', type: 'integer'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/siswa/3/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start,
                    keyid_siswa:'{{ $KEYID }}' }
            );
            this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.siswa.receivable.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_siswaGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_siswaPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_siswaPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_siswaGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_siswaSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   bodyStyle:'padding:5px',
                items: [ 
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'receivable_key_id',
                            xtype:'textfield',
                            fieldLabel: 'Key.ID',
                            name: 'keyid',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '{{ $KEYID }}',
                            hidden : true,
                        },
                        {   id : 'receivable_created_date', 
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }]
                    },
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'receivable_nopen', 
                            xtype:'textfield',
                            fieldLabel: 'No.Pendaftaran',
                            name: 'no_pendaftaran',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $NOPEN }}'
                        },
                        {   id : 'receivable_nis', 
                            xtype:'textfield',
                            fieldLabel: 'NIS',
                            name: 'nis',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $NIS }}'
                        },
                        {   id : 'receivable_tempat_lahir', 
                            xtype:'textfield',
                            fieldLabel: 'Tempat Lahir',
                            name: 'tempat_lahir',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $BIRTHPLACE }}'
                        },
                        {   id : 'receivable_gender', 
                            xtype:'textfield',
                            fieldLabel: 'Gender',
                            name: 'jenis_kelamin',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $GENDER }}'
                        },
                        ]
                    },
                    {   columnWidth:.7,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'receivable_nama_lengkap', 
                            xtype:'textfield',
                            fieldLabel: 'Nama.Lengkap',
                            name: 'nama_lengkap',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '{{ $FULLNAME }}'
                        },
                        {   id : 'receivable_nama_panggilan', 
                            xtype:'textfield',
                            fieldLabel: 'Nama.Panggilan',
                            name: 'nama_panggilan',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '{{ $NICKNAME }}'
                        },
                        {   id : 'receivable_alamat', 
                            xtype:'textfield',
                            fieldLabel: 'Alamat.Rumah',
                            name: 'alamat_rumah',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '{{ $ADDRESS }}'
                        },
                        {   id : 'receivable_tanggal_lahir', 
                            xtype:'textfield',
                            fieldLabel: 'Tanggal.Lahir',
                            name: 'tanggal_lahir',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '{{ $BIRTHDATE }}'
                        },
                        {   id : 'receivable_kewajiban', 
                            xtype:'textfield',
                            fieldLabel: 'Total.Kewajiban',
                            name: 'total_wajib',
                            anchor:'50%',
                            allowBlank: false,
                            readOnly : true,
                            value : '',
                        },
                        {   id : 'pembayaran', 
                            xtype:'textfield',
                            fieldLabel: 'Total.Pembayaran',
                            name: 'total_bayar',
                            anchor:'50%',
                            allowBlank: false,
                            readOnly : true,
                            value :'' ,
                        },
                        {   id : 'receivable_saldo', 
                            xtype:'textfield',
                            fieldLabel: 'Total.Saldo',
                            name: 'total_saldo',
                            anchor:'50%',
                            allowBlank: true,
                            readOnly : true,
                            value : '',
                        },
                        ]
                    }]
                },
                ]
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_siswaTab",
                jsId : tabId+"_siswaTab",
                title:  '{{ $FULLNAME }}',
                region: 'center',
                layout: 'border',
                items: [
                {   
                    region: 'north',     // position for region
                    split:true,
                    height : this.region_height/4,
                    width: 200,
                    minSize: 200,
                    maxSize: 300,
                    // collapsible: true,
                    layout : 'fit',
                    items:[this.Form]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
    }; // end of public space
}(); // end of app
alfalah.siswa.receivable.banktransfer= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                // {   header: "Id", width : 50,
                //     dataIndex : 'keyid', sortable: true,
                //     tooltip:"Id",
                // },
                {   header: "N.I.S", width : 50,
                    dataIndex : 'nis', sortable: true,
                    tooltip:"N.I.S",
                },
                {   header: "Nama Siswa", width : 200,
                    dataIndex : 'nama_siswa', sortable: true,
                    tooltip:"Nama Siswa",
                },
                {   header: "Tanggal", width : 100,
                    dataIndex : 'tanggal_transfer', sortable: true,
                    tooltip:"Tanggal Transfer",
                },
                {   header: "Jam", width : 100,
                    dataIndex : 'jam_transfer', sortable: true,
                    tooltip:"Jam Transfer",
                },
                {   header: "Type", width : 100,
                    dataIndex : 'dk', sortable: true,
                    tooltip:"Tipe Transfer",
                },
                {   header: "Amount", width : 100,
                    dataIndex : 'mutasi', sortable: true,
                    tooltip:"Amount",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                // {   header: "Saldo", width : 100,
                //     dataIndex : 'saldo', sortable: true,
                //     tooltip:"Saldo",
                // },
                // {   header: "Keterangan", width : 120,
                //     dataIndex : 'keterangan', sortable: true,
                //     tooltip:"Keterangan",
                // },
                {   header: "VA", width : 120,
                    dataIndex : 'va', sortable: true,
                    tooltip:"VA",
                },
                // {   header: "Journal", width : 120,
                //     dataIndex : 'no_jurnal', sortable: true,
                //     tooltip:"Journal",
                // },
                // {   header: "Posting", width : 120,
                //     dataIndex : 'no_posting', sortable: true,
                //     tooltip:"Posting",
                // },
                // {   header: "tgl_posting", width : 100,
                //     dataIndex : 'Posting.Date', sortable: true,
                //     tooltip:"Posting Date",
                // },
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'siswa_id', type: 'integer'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'banktransfer_nis',
                    cid: 'nis',
                    fieldLabel: 'N.I.S',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'banktransfer_nama_siswa',
                    cid: 'nama_siswa',
                    fieldLabel: 'Nama',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'banktransfer_va',
                    cid: 'va',
                    fieldLabel: 'V.A',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_banktransferSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.banktransfer_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/siswa/3/10') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start,
                    nis:'{{ $NIS }}',  
                }
            );
            this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.siswa.receivable.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_banktransferGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_banktransferPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_banktransferPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_banktransferGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_banktransferSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_banktransferTab",
                jsId : tabId+"_banktransferTab",
                title:  "Bank Transfer ku",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        Grid_add : function(button, event)
        {   },
        Grid_edit : function(button, event)
        {   },
        // siswa grid delete records
        Grid_remove: function(button, event)
        {   },
        // siswa receivable search button
        banktransfer_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_banktransferSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.siswa.receivable.banktransfer.task,
                        act: alfalah.siswa.receivable.banktransfer.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
        print_out: function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                alfalah.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        id : the_record.id });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };   
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.siswa.receivable.initialize, alfalah.siswa.receivable);
// end of file
</script>
<div>&nbsp;</div>