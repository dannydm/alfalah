<!-- Using default Layout -->
@extends('layouts_backend._iframe_backend')
<!-- load your extry css styles -->
@section('extra_styles')
<link rel="stylesheet" href="../../w2ui/w2ui-1.5.rc1.min.css" />
<link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
<!-- <style>
.tab {
    width: 100%;
    height: 400px;
    border: 1px solid silver;
    border-top: 0px;
    display: none;
    padding: 10px;
    overflow: auto;
}
</style> -->
@endsection
<!-- load your main content page -->
@section('content')
<!-- Main content -->
<section class="content">
  <div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title">Nama Lengkap Siswa</h3>
      <div class="box-tools pull-right">
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label for="profile_key_id">Key.ID</label>
            <input type="text" class="form-control" id="profile_key_id" 
                    value = "{{ $KEYID }}" placeholder="">
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label for="profile_nis">N.I.S</label>
            <input type="text" class="form-control" id="profile_nis" 
                    value = "{{ $NIS }}" placeholder="">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="profile_no_pendaftaran">No.Pendaftaran</label>
            <input type="text" class="form-control" id="profile_no_pendaftaran" 
                    value = "{{ $NOPEN }}" placeholder="">
          </div>
        </div>
      </div>
      <div class="row">   
        <div class="col-md-6">
          <div class="form-group">
            <label for="profile_nama_lengkap">Nama Lengkap</label>
            <input type="text" class="form-control" id="profile_nama_lengkap" 
                  value = "{{ $FULLNAME }}" placeholder="">
          </div>
          <div class="form-group">
            <label for="profile_tempat_lahir">Tempat Lahir</label>
            <input type="text" class="form-control" id="profile_tempat_lahir" 
                    value = "{{ $BIRTHPLACE }}" placeholder="">
          </div>
          <div class="form-group">
            <label for="profile_alamat_rumah">Alamat Rumah</label>
            <input type="text" class="form-control" id="profile_alamat_rumah" 
                    value = "{{ $ADDRESS }}" placeholder="">
          </div>
          <!-- /.form-group -->
        </div>
        <!-- /.col -->
        <div class="col-md-6">
          <!-- /.form-group -->
          <div class="form-group">
            <label for="profile_jenis_kelamin">Jenis Kelamin</label>
            <input type="text" class="form-control" id="profile_jenis_kelamin" 
                   value = "{{ $GENDER }}" placeholder="">
          </div>
          <!-- /.form-group -->
          <div class="form-group">
            <label for="profile_nama_panggilan">Nama Panggilan</label>
            <input type="text" class="form-control" id="profile_nama_panggilan" 
                    value = "{{ $NICKNAME }}"placeholder="">
          </div>
          <div class="form-group">
            <label for="profile_tangal_lahir">Tanggal Lahir</label>
            <input type="text" class="form-control" id="profile_tanggal_lahir" 
                   value = "{{ $BIRTHDATE }}" placeholder="">
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- <div class="box-footer">
        <button type="submit" class="btn btn-primary">Close</button>
      </div> -->
      <!-- /.row -->
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
@endsection
<!-- load your extra js scripts -->
@section('extra_scripts')
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- <script src="../../bower_components/fastclick/lib/fastclick.js"></script> -->
<!-- AdminLTE App -->
<!-- <script src="../../dist/js/adminlte.min.js"></script> -->
<!-- AdminLTE for demo purposes --><!-- 
<script src="dist/js/demo.js"></script> -->
<!--AdminLTE Iframe-->
<!-- <script src="dist/js/app_iframe.js"></script> -->
<!-- <script src="../../w2ui/w2ui-1.5.rc1.min.js"></script> -->
<script type="text/javascript">
  // Define namespace
  alfalah = parent.alfalah;
  alfalah.namespace('alfalah.profile');
  // create application
  alfalah.profile = function()
  {   // do NOT access DOM from here; elements don't exist yet
      // execute at the first time
      // private variables
      // this.tabId = '{{ $TABID }}';
      this.config;
      // private functions
      // public space
      return {
          centerPanel : 0,
          the_records : [],
          sid : '{{ csrf_token() }}',
          task : ' ',
          act : ' ',
          // public methods
          initialize: function()
          { console.log('initialize');
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
          },
          // prepare the component before layout drawing
          prepare_component: function()
          { console.log('prepare_component');
            
          },
          // build the layout
          build_layout: function()
          { console.log('build_layout');
            
          },
          // finalize the component and layout drawing
          finalize_comp_and_layout: function()
          { console.log('finalize_comp_and_layout');
          },
      }; // end of public space
  }(); // end of app
  $(document).ready(alfalah.profile.initialize());
</script>

@endsection
