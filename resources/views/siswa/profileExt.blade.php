<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.siswa.profile');

// create application
alfalah.siswa.profile = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            // this.profile.initialize();
            this.forms.initialize(null);
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            // this.centerPanel.add(this.forms.Tab);
            // this.centerPanel.setActiveTab(this.forms.Tab);
            this.centerPanel.add(this.forms.Form);
            this.centerPanel.setActiveTab(this.forms.Form);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.siswa.profile.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        popSiteDepartment : '',
        // public methods
        initialize: function(the_record)
        {   this.Records = the_record;
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   bodyStyle:'padding:5px',
                // autoScroll : true,
                // autoHeight : true,
                title : '{{ $FULLNAME }}',
                tbar: [
                    // {   text:'Add Item',
                    //     tooltip:'Add Record',
                    //     iconCls: 'silk-add',
                    //     // handler : this.Grid_add,
                    //     scope : this
                    // }, '-',
                    // {   text:'Delete',
                    //     tooltip:'Delete Record',
                    //     iconCls: 'silk-delete',
                    //     // handler : this.Grid_remove,
                    //     scope : this
                    // }, '-',
                    // {   text:'Save Order',
                    //     tooltip:'Save Record',
                    //     iconCls: 'icon-save',
                    //     // handler : this.Form_save,
                    //     scope : this
                    // }
                ],
                items: [ 
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'profile_key_id',
                            xtype:'textfield',
                            fieldLabel: 'Key.ID',
                            name: 'keyid',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '{{ $KEYID }}',
                            hidden : true,
                        },
                        {   id : 'profile_created_date', 
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }]
                    },
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'profile_jenjang_sekolah', 
                            xtype:'textfield',
                            fieldLabel: 'Jenjang',
                            name: 'jenjang_sekolah',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $JENJANG }}'
                        },
                        {   id : 'profile_nis', 
                            xtype:'textfield',
                            fieldLabel: 'N.I.S',
                            name: 'nis',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $NIS }}'
                        },
                        {   id : 'profile_kelas', 
                            xtype:'textfield',
                            fieldLabel: 'Kelas',
                            name: 'nama_kelas',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $KELAS }}'
                        },
                        {   id : 'profile_tempat_lahir', 
                            xtype:'textfield',
                            fieldLabel: 'Tempat Lahir',
                            name: 'tempat_lahir',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $BIRTHPLACE }}'
                        },
                        {   id : 'profile_gender', 
                            xtype:'textfield',
                            fieldLabel: 'Gender',
                            name: 'jenis_kelamin',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $GENDER }}'
                        },
                        ]
                    },
                    {   columnWidth:.7,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'profile_nama_lengkap', 
                            xtype:'textfield',
                            fieldLabel: 'Nama.Lengkap',
                            name: 'nama_lengkap',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '{{ $FULLNAME }}'
                        },
                        {   id : 'profile_nama_panggilan', 
                            xtype:'textfield',
                            fieldLabel: 'Nama.Panggilan',
                            name: 'nama_panggilan',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '{{ $NICKNAME }}'
                        },
                        {   id : 'profile_alamat', 
                            xtype:'textfield',
                            fieldLabel: 'Alamat.Rumah',
                            name: 'alamat_rumah',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '{{ $ADDRESS }}'
                        },
                        {   id : 'profile_tanggal_lahir', 
                            xtype:'textfield',
                            fieldLabel: 'Tanggal.Lahir',
                            name: 'tanggal_lahir',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '{{ $BIRTHDATE }}'
                        },
                        ]
                    }]
                },
                ]
            });
        },
        // build the layout
        build_layout: function()
        {   
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.siswa.profile.initialize, alfalah.siswa.profile);
// end of file
</script>
<div>&nbsp;</div>