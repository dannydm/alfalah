<!-- Using default Layout -->
@extends('layouts_backend._iframe_backend')
<!-- load your extry css styles -->
@section('extra_styles')
<link rel="stylesheet" href="../../w2ui/w2ui-1.5.rc1.min.css" />
<link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
<style>
.w2ui-field input {
    width: 20px;
    text-align: left;

}
.w2ui-field label {
    padding: 5px;
}
.w2ui-field > div {
    margin-left: 1px;
}
</style>
@endsection
<!-- load your main content page -->
@section('content')
<!-- Main content -->
<div id="siswa_layout" style="width: 100%; height: 100%;" 
>
  <div id="siswa_grid"></div>
</div>
<!-- <div id="siswa_form" class="w2ui-field">
    <div class="box-body">
        <div class="row">
            <div class="col-md-20">
                <div class="form-group">
                    <label style="padding: 5px;">No.Pendaftaran</label>
                    <input style="width: 100%;" class="form-control" 
                      id="no_pendaftaran" name="no_pendaftaran" 
                      placeholder="No Pendaftaran">
                    <label style="padding: 5px;">N.I.S</label>
                    <input style="width: 100%;" class="form-control" 
                      id="nis" name="nis"  placeholder="No Induk Siswa">
                    <label style="padding: 5px;">Nama Lengkap</label>
                    <input style="width: 100%;" class="form-control" 
                      id="nama_lengkap" name="nama_lengkap"
                      placeholder="Nama Lengkap">
                    <label style="padding: 5px;">Nama Panggilan</label>
                    <input style="width: 100%;" class="form-control" 
                      id="nama_panggilan" name="nama_panggilan" 
                      placeholder="Nama Panggilan">
                </div>
            </div>
        </div>
    </div>  
</div> -->
<!-- /.content -->
@endsection
<!-- load your extra js scripts -->
@section('extra_scripts')
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- <script src="../../bower_components/fastclick/lib/fastclick.js"></script> -->
<!-- AdminLTE App -->
<!-- <script src="../../dist/js/adminlte.min.js"></script> -->
<!-- AdminLTE for demo purposes --><!-- 
<script src="dist/js/demo.js"></script> -->
<!--AdminLTE Iframe-->
<!-- <script src="dist/js/app_iframe.js"></script> -->
<script src="../../w2ui/w2ui-1.5.rc1.min.js"></script>
<script type="text/javascript">
    // Define namespace
    alfalah = parent.alfalah;
    alfalah.namespace('alfalah.siswa');
    // create application
    alfalah.siswa = function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        // this.tabId = '{{ $TABID }}';
        this.config;
        // private functions
        // public space
        return {
            centerPanel : 0,
            the_records : [],
            sid : '{{ csrf_token() }}',
            task : ' ',
            act : ' ',
            // public methods
            initialize: function()
            {   console.log('initialize');
                this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {   console.log('prepare_component');
                var pstyle = 'background-color: #F5F6F7; border: 1px solid #dfdfdf; padding: 1px;';

                this.config = {
                    siswa_grid: 
                    {   name    : 'siswa_grid',
                        // url     : '/siswa/2/0',
                        ref_url : '/siswa/2/0',
                        method  : 'GET',
                        recid   : 'record_id',
                        // postData : {
                        //     s : 'init',
                        //     '_token' : alfalah.siswa.sid,
                        //     'x-csrf-token': alfalah.siswa.sid
                        // },
                        selectType: 'cell',
                        show: { 
                            // toolbar: true,
                            // toolbarSave: true,
                            lineNumbers : true,
                            footer: true,
                            
                        },
                        columns: [
                            {   field: 'keyid', caption: 'ID', size: '50px', 
                                sortable: true, resizable: true, searchable: true, 
                                editable: { type: 'text' } },
                            {   field: 'no_pendaftaran', caption: 'No.Pen', size: '100px', 
                                sortable: true, searchable: true, 
                                editable: { type: 'text' } },
                            {   field: 'nis', caption: 'NIS', size: '80px', 
                                sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                            {   field: 'nama_lengkap', caption: 'Nama', 
                                size: '300px', sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                            {   field: 'nama_panggilan', caption: 'Panggilan', 
                                size: '100px', sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                            {   field: 'jenis_kelamin', caption: 'Gender', size: '50px', sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                            {   field: 'tempat_lahir', caption: 'Tempat Lahir', 
                                size: '80px', sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                            {   field: 'tanggal_lahir', caption: 'Tanggal Lahir', 
                                size: '80px', sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                            {   field: 'alamat_rumah', caption: 'Alamat', 
                                size: '400px', sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                            {   field: 'no_telpon1', caption: 'No.Telp', 
                                size: '80px', sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                            {   field: 'keterangan_no_telpon1', caption: 'Keterangan', 
                                size: '100px', sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                            {   field: 'status', caption: 'Status', size: '100px', 
                                sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                            {   field: 'created_date', caption: 'Created', size: '100px' },
                        ],
                        searches: [
                            { type: 'int', field: 'recid', caption: 'ID' },
                            { type: 'text', field: 'desc', caption: 'Description' }
                        ],
                        newRecord : function()
                        {   console.log('add new record');
                            console.log(this);
                            this.add({
                                siswa_id    : "",
                                name        : "New siswa",
                                recid       : "",
                                status      : "1",  // Inactive siswa
                                symbol      : "",
                                created_date: ""
                            }, true); // as fist record
                        },
                        onLoad: function(event)
                        {   console.log("siswa_grid onLoad");
                            console.log(event);
                        }
                    },
                    siswa_layout: 
                    {   name: 'siswa_layout',
                        panels: [
                            // { type: 'top',  size: 50, resizable: true, style: pstyle, content: 'top' },
                            // { type: 'left', size: 200, resizable: true, style: pstyle, content: 'left' },
                            {   type: 'main', style: pstyle, 
                                toolbar: 
                                {   
                                    items: [
                                    // {   type: 'button', text: 'Add', icon: 'fa fa-plus',
                                    //     onClick: function(event)
                                    //     { } 
                                    // },
                                    // {   type: 'button', text: 'Save', icon: 'fa fa-save',
                                    //     onClick: function(event)
                                    //     { alfalah.core.submitGrid(
                                    //         w2ui.siswa_grid, 
                                    //         '/siswa/2/1',
                                    //         alfalah.countries.sid
                                    //       );
                                    //     }
                                    // },
                                    // {   type: 'break'},
                                    // {   type: 'button', text: 'Delete', 
                                    //     icon: 'fa fa-remove',
                                    //     onClick: function(event)
                                    //     { alfalah.core.ajax(
                                    //         '/siswa/2/2',                          //the_url, 
                                    //         {                                 //the_parameters,
                                    //           '_token' : alfalah.countries.sid,
                                    //           json : JSON.stringify([
                                    //             { id : w2ui.siswa_grid.getSelection()[0] 
                                    //             }])
                                    //         },                  
                                    //         "POST",                           //the_type, 
                                    //         function(response)                //fn_success
                                    //         { w2ui.siswa_grid.reload(); 
                                    //         },
                                    //         function(response)                //fn_fail, 
                                    //         { console.log("FAILED");
                                    //           console.log(response);
                                    //         },
                                    //         null                              //fn_always
                                    //       );
                                    //     } 
                                    // },
                                    {   type: 'break'},
                                    // {   type: 'button', text: 'PDF',
                                    //     print_type : 'PDF', 
                                    //     icon: 'fa fa-file-pdf-o',
                                    //     onClick: function (event) 
                                    //     {   alfalah.siswa.print_out(event); } 
                                    // },
                                    {   type: 'button', text: 'XLS',
                                        print_type : 'XLS',  
                                        icon: 'fa fa-file-excel-o',
                                        onClick: function (event) 
                                        {   alfalah.siswa.print_out(event); }  
                                    }],
                                },
                            },
                            // { type: 'preview', size: '50%', resizable: true, style: pstyle, content: 'preview' 
                            // },
                            {   type: 'right', size: 200, resizable: true, style: pstyle, 
                                toolbar: 
                                {   items: [
                                    {   type: 'button', text: 'Search', 
                                        icon: 'fa fa-search',
                                        onClick: function(event )
                                        {   console.log("search click");
                                            alfalah.siswa.search_handler(event); 
                                        }
                                    }],
                                    // onClick: function (event) {
                                    //   console.log("search clisck");
                                    //   console.log(event);
                                    // }
                                }
                            },
                            // { type: 'bottom', size: 50, resizable: true, style: pstyle, content: 'bottom' 
                            // }
                        ]
                    },
                    siswa_form:
                    {   name  : 'siswa_form',
                        url   : 'doremi/post',
                        fields: [
                            {   field: 'no_pendaftaran',  
                                type: 'text', 
                                // html: { caption : 'No.Pen', 
                                //         attr    : 'style="width: 100px"' 
                                //     }
                            },
                            {   field: 'nis',  type: 'text'},
                            {   field: 'nama_lengkap',  type: 'text' },
                        ],
                        style: 'border: 0px; background-color: transparent;',
                        formHTML: 
                            '<div id="siswa_form" class="w2ui-page page-0">'+
                            '    <div class="w2ui-field">'+
                            '        <label style="text-align: left;">No.Pen</label>'+
                            '        <div>'+
                            '            <input name="no_pendaftaran" type="text" size="24" maxlength="20"/>'+
                            '        </div>'+
                            '    </div>'+
                            '    <div class="w2ui-field">'+
                            '        <label style="text-align: left;">N.I.S</label>'+
                            '        <div>'+
                            '            <input name="nis" type="text" size="24" maxlength="12"/>'+
                            '        </div>'+
                            '    </div>'+
                            '    <div class="w2ui-field">'+
                            '        <label style="text-align: left;">Nama</label>'+
                            '        <div>'+
                            '            <input name="nama_lengkap" type="text" size="24" maxlength="20" />'+
                            '        </div>'+
                            '    </div>'+
                            '</div>',
                        // actions: {
                        //     reset: function () {   this.clear();   },
                        //     save: function () { this.save();    }
                        // }
                    },
                };
            },
            // build the layout
            build_layout: function()
            { 
                // console.log('build_layout 1');
                $('#siswa_layout').w2layout(this.config.siswa_layout);
                // console.log('build_layout 2');
                $('#siswa_grid').w2grid(this.config.siswa_grid);
                // console.log('build_layout 3');
                w2ui.siswa_layout.content('main', w2ui.siswa_grid);
                // console.log('build_layout 4');
                
                $('#w2int').w2field('int', { autoFormat: false });

                $('#siswa_form').w2form(this.config.siswa_form);
                w2ui.siswa_layout.content('right', w2ui.siswa_form);
                // w2ui.siswa_layout.content('right', $('#siswa_form').w2form());
                console.log('build_layout 4');
            },
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            {   console.log('finalize_comp_and_layout'); },
            // finalize the component and layout drawing
            getSearchFormParameter: function(the_form)
            {   the_searchs = "";
                $(the_form).each(function()
                    {   the_searchs = $(this).find(':input');
                    });
                the_parameter = alfalah.core.getSearchParameter(the_searchs);
                the_parameter = $.extend( the_parameter, 
                                {   s:"form"    });
                return the_parameter;
            },
            search_handler: function(event)
            {   console.log('search_handlerssssssss');
                the_parameter = this.getSearchFormParameter("#siswa_form");
                // w2utils.settings.dataType = 'HTTP';
                // w2ui.siswa_grid.load(w2ui.siswa_grid.ref_url);
                w2ui.siswa_grid.load(w2ui.siswa_grid.ref_url+ '?'+$.param(the_parameter));
                
                // w2ui.siswa_grid.request(
                //     'GET', 
                //     the_parameter,
                //     // JSON.stringify(the_parameter), 
                //     w2ui.siswa_grid.ref_url
                // );                                 
                // 
                // this.DataStore.baseParams = Ext.apply( the_parameter,
                //     {   task: alfalah.siswa.task,
                //         act: alfalah.siswa.act,
                //         a:2, b:0, s:"form",
                //         limit:this.page_limit, start:this.page_start });
                // this.DataStore.reload();
                // w2ui.siswa_grid.postData = JSON.stringify(the_parameter);
                // w2ui.siswa_grid.method = 'POST';
                // w2ui.siswa_grid.postData = the_parameter;
                // w2ui.siswa_grid.searchData = the_parameter;
                // w2ui.siswa_grid.search();
                // w2utils.settings.RESTfull = true;
                // w2utils.settings.dataType = 'JSON';
            },
            print_out: function(event)
            {   alfalah.core.printOut(
                    event.item.print_type, 
                    w2ui.siswa_grid.ref_url, 
                    this.getSearchFormParameter("#siswa_form")
                );
            },
        }; // end of public space
    }(); // end of app
    $(document).ready(alfalah.siswa.initialize());
</script>

@endsection
