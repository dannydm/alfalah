<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.siswareceivable');

// create application
alfalah.siswareceivable = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.siswareceivable.initialize();
            this.banktransfer.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.banktransfer.Tab);
            this.centerPanel.add(this.siswareceivable.Tab);
            // this.centerPanel.setActiveTab(this.banktransfer.Tab);
            this.centerPanel.setActiveTab(this.siswareceivable.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.siswareceivable.siswareceivable= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                // {   header: "Id", width : 50,
                //     dataIndex : 'keyid', sortable: true,
                //     tooltip:"Id",
                // },
                {   header: "Grade", width : 100,
                    dataIndex : 'nama_jenjang_siswa', sortable: true,
                    tooltip:"Grade",
                },
                {   header: "Nama Siswa", width : 200,
                    dataIndex : 'nama_siswa', sortable: true,
                    tooltip:"Nama Siswa",
                },
                {   header: "Tahun / Bulan", width : 100,
                    dataIndex : 'bultah', sortable: true,
                    tooltip:"Tahun Bulan",
                },
                {   header: "Biaya", width : 100,
                    dataIndex : 'nama_biaya_sekolah', sortable: true,
                    tooltip:"Biaya Sekolah",
                },
                {   header: "Bulan", width : 50,
                    dataIndex : 'bulan', sortable: true,
                    tooltip:"Bulan",
                },
                {   header: "Piutang", width : 100,
                    dataIndex : 'piutang', sortable: true, 
                    tooltip:"Piutang",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Potongan", width : 100,
                    dataIndex : 'potongan', sortable: true,
                    tooltip:"Potongan",  editor : new Ext.form.TextField({ fieldStyle :"text-align:right;"}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Pembayaran", width : 100,
                    dataIndex : 'pembayaran', sortable: true,
                    tooltip:"Pembayaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Sync.Date", width : 100,
                    dataIndex : 'sync_date', sortable: true,
                    tooltip:"Syncronize Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'siswa_id', type: 'integer'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'siswareceivable_bultah',
                    cid: 'bultah',
                    fieldLabel: 'Tahun / Bulan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'siswareceivable_nama_biaya_sekolah',
                    cid: 'nama_biaya_sekolah',
                    fieldLabel: 'Biaya',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'siswareceivable_nama_siswa',
                    cid: 'nama_siswa',
                    fieldLabel: 'Nama',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_siswaSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.siswareceivable_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/siswa/3/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.siswareceivable.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                @if (array_key_exists('SAVE', $MyTasks)) 
                    {   text:'Syncronize',
                        tooltip:'Syncronize',
                        iconCls: 'silk-arrow-refresh',
                        handler : this.siswareceivable_sync_handler,
                        scope : this
                    },'-',
                @endif
                @if (array_key_exists('PRINT_XLS', $MyTasks)) 
                    {   print_type : "xls",
                        text:'Print Excel',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                @endif
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_siswaGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_siswaPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_siswaPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_siswaGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_siswaSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_siswaTab",
                jsId : tabId+"_siswaTab",
                title:  "Pembayaran",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 50,
                                defaultType: 'textfield',
                                items : this.Searchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.siswareceivable_search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // siswa receivable search button
        siswareceivable_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_siswaSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.siswareceivable.task,
                        act: alfalah.siswareceivable.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.removeAll();
                this.DataStore.reload();
            };
        },
    @if (array_key_exists('SAVE', $MyTasks)) 
        siswareceivable_sync_handler : function(button, event)
        {   
            this.SyncDS = alfalah.core.newDataStore(
                "{{ url('/siswa/4/2') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SyncDS.reload();
        },
    @endif
    }; // end of public space
}(); // end of app
alfalah.siswareceivable.banktransfer= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                // {   header: "Id", width : 50,
                //     dataIndex : 'keyid', sortable: true,
                //     tooltip:"Id",
                // },
                {   header: "N.I.S", width : 50,
                    dataIndex : 'nis', sortable: true,
                    tooltip:"N.I.S",
                },
                {   header: "Nama Siswa", width : 200,
                    dataIndex : 'nama_siswa', sortable: true,
                    tooltip:"Nama Siswa",
                },
                {   header: "Tanggal", width : 100,
                    dataIndex : 'tanggal_transfer', sortable: true,
                    tooltip:"Tanggal Transfer",
                },
                {   header: "Jam", width : 50,
                    dataIndex : 'jam_transfer', sortable: true,
                    tooltip:"Jam Transfer",
                },
                {   header: "Type", width : 50,
                    dataIndex : 'dk', sortable: true,
                    tooltip:"Tipe Transfer",
                },
                {   header: "Amount", width : 100,
                    dataIndex : 'mutasi', sortable: true,
                    tooltip:"Amount",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Saldo", width : 100,
                    dataIndex : 'saldo', sortable: true,
                    tooltip:"Saldo",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Keterangan", width : 120,
                    dataIndex : 'keterangan', sortable: true,
                    tooltip:"Keterangan",
                },
                {   header: "VA", width : 120,
                    dataIndex : 'va', sortable: true,
                    tooltip:"VA",
                },
                {   header: "Journal", width : 120,
                    dataIndex : 'no_jurnal', sortable: true,
                    tooltip:"Journal",
                },
                {   header: "Posting", width : 150,
                    dataIndex : 'no_posting', sortable: true,
                    tooltip:"Posting",
                },
                {   header: "tgl_posting", width : 100,
                    dataIndex : 'Posting.Date', sortable: true,
                    tooltip:"Posting Date",
                },
                {   header: "Sync.Date", width : 100,
                    dataIndex : 'sync_date', sortable: true,
                    tooltip:"Syncronize Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'siswa_id', type: 'integer'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'banktransfer_nis',
                    cid: 'nis',
                    fieldLabel: 'N.I.S',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'banktransfer_nama_siswa',
                    cid: 'nama_siswa',
                    fieldLabel: 'Nama',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'banktransfer_va',
                    cid: 'va',
                    fieldLabel: 'V.A',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_banktransferSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.banktransfer_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/siswa/3/10') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.siswareceivable.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                @if (array_key_exists('SAVE', $MyTasks))
                    {   text:'Syncronize',
                        tooltip:'Syncronize',
                        iconCls: 'silk-arrow-refresh',
                        handler : this.banktransfer_sync_handler,
                        scope : this
                    },'-',
                @endif
                @if (array_key_exists('PRINT_XLS', $MyTasks))
                    {   print_type : "xls",
                        text:'Print Excel',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                @endif
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_banktransferGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_banktransferPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_banktransferPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_banktransferGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_banktransferSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_banktransferTab",
                jsId : tabId+"_banktransferTab",
                title:  "Bank Transfer",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 50,
                                defaultType: 'textfield',
                                items : this.Searchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.banktransfer_search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // siswa receivable search button
        banktransfer_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_banktransferSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.siswareceivable.banktransfer.task,
                        act: alfalah.siswareceivable.banktransfer.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    @if (array_key_exists('SAVE', $MyTasks))
        banktransfer_sync_handler : function(button, event)
        {   
            this.SyncDS = alfalah.core.newDataStore(
                "{{ url('/siswa/4/1') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SyncDS.reload();
        },
    @endif
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.siswareceivable.initialize, alfalah.siswareceivable);
// end of file
</script>
<div>&nbsp;</div>