<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.siswa');

// create application
alfalah.siswa = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.siswa.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.siswa.Tab);
            this.centerPanel.setActiveTab(this.siswa.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.siswa.siswa= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.JenjangDS = alfalah.core.newDataStore(
                "{{ url('/siswa/2/80') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.JenjangDS.load();

            this.KelasDS = alfalah.core.newDataStore(
                "{{ url('/siswa/2/81') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.KelasDS.load();

            this.RuangKelasDS = alfalah.core.newDataStore(
                "{{ url('/siswa/2/82') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.RuangKelasDS.load();

            this.Columns = [
                {   header: "Id", width : 50,
                    dataIndex : 'keyid', sortable: true,
                    tooltip:"Id",
                },
                {   header: "Jenjang", width : 100,
                    dataIndex : 'nama_jenjang_sekolah', sortable: true,
                    tooltip:"Jenjang Sekolah",
                },
                {   header: "N.I.S", width : 50,
                    dataIndex : 'nis', sortable: true,
                    tooltip:"Nomor Induk Siswa",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.is_user == 1)
                        {   metaData.attr = "style = background-color:lime;"; };
                        return value;
                    },
                },
                {   header: "Nama Lengkap", width : 200,
                    dataIndex : 'nama_lengkap', sortable: true,
                    tooltip:"Nama Lengkap",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.is_user == 1)
                        {   metaData.attr = "style = background-color:lime;"; };
                        return value;
                    },
                },
                {   header: "Ruang Kelas", width : 150,
                    dataIndex : 'nama_kelas', sortable: true,
                    tooltip:"Nama Kelas",
                },
                {   header: "Panggilan", width : 100,
                    dataIndex : 'nama_panggilan', sortable: true,
                    tooltip:"Nama Lengkap",
                },
                {   header: "Gender", width : 80,
                    dataIndex : 'jenis_kelamin', sortable: true,
                    tooltip:"Jenis Kelamin",
                },
                {   header: "Tempat Lahir", width : 100,
                    dataIndex : 'tempat_lahir', sortable: true,
                    tooltip:"Tempat Lahir",
                },
                {   header: "Tanggal Lahir", width : 100,
                    dataIndex : 'tanggal_lahir', sortable: true,
                    tooltip:"Tanggal Lahir",
                },
                {   header: "Alamat", width : 250,
                    dataIndex : 'alamat_rumah', sortable: true,
                    tooltip:"Alamat Rumah",
                },
                {   header: "No.Telp", width : 100,
                    dataIndex : 'no_telpon1', sortable: true,
                    tooltip:"Nomer Telepon",
                },
                {   header: "No.Pen", width : 80,
                    dataIndex : 'no_pendaftaran', sortable: true,
                    tooltip:"No Pendaftaran",
                },
                {   header: "Keterangan", width : 100,
                    dataIndex : 'keterangan_no_telpon1', sortable: true,
                    tooltip:"Keterangan",
                },
                {   header: "Sync.Date", width : 150,
                    dataIndex : 'sync_date', sortable: true,
                    tooltip:"Syncronize Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'siswa_id', type: 'integer'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'siswa_nis',
                    cid: 'nis',
                    fieldLabel: 'N.I.S',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'siswa_nama_lengkap',
                    cid: 'nama_lengkap',
                    fieldLabel: 'Nama',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'siswa_nama_panggilan',
                    cid: 'nama_panggilan',
                    fieldLabel: 'Panggilan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'siswa_alamat_rumah',
                    cid: 'alamat_rumah',
                    fieldLabel: 'Alamat',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'siswa_gender',
                    cid: 'jenis_kelamin',
                    fieldLabel: 'Gender',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['label'],
                        data : [ ['ALL'], ['Laki-Laki'], ['Perempuan']]
                    }),
                    displayField:'label',
                    valueField :'label',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'ALL'
                },
                {   id : 'siswa_jenjang_sekolah', 
                    cid: 'jenjang_sekolah',
                    xtype : 'combo',
                    fieldLabel: 'Jenjang',
                    labelSeparator : '',
                    name: 'jenjang_sekolah',
                    anchor:'95%',
                    allowBlank: false,
                    readOnly : false,
                    store: this.JenjangDS,
                    displayField: 'jenjang_sekolah',
                    valueField: 'jenjang_sekolah',
                    mode : 'local',
                    forceSelection: true,
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    typeAhead: true,
                    width : 100,
                    value: 'ALL'
                },
                {   id : 'siswa_nama_kelas', 
                    cid: 'nama_kelas',
                    xtype : 'combo',
                    fieldLabel: 'Kelas',
                    labelSeparator : '',
                    name: 'nama_kelas',
                    anchor:'95%',
                    allowBlank: false,
                    readOnly : false,
                    store: this.KelasDS,
                    displayField: 'nama_kelas',
                    valueField: 'nama_kelas',
                    mode : 'local',
                    forceSelection: true,
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    typeAhead: true,
                    width : 100,
                    value: 'ALL'
                },
                {   id : 'siswa_ruang_kelas', 
                    cid: 'ruang_kelas',
                    xtype : 'combo',
                    fieldLabel: 'Ruang',
                    labelSeparator : '',
                    name: 'ruang_kelas',
                    anchor:'95%',
                    allowBlank: false,
                    readOnly : false,
                    store: this.RuangKelasDS,
                    displayField: 'ruang_kelas',
                    valueField: 'ruang_kelas',
                    mode : 'local',
                    forceSelection: true,
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    typeAhead: true,
                    width : 100,
                    value: 'ALL'
                },

            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_siswaSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.siswa_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/siswa/2/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.siswa.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                @if (array_key_exists('SAVE', $MyTasks)) 
                    {   text:'Syncronize',
                        tooltip:'Syncronize',
                        iconCls: 'silk-arrow-refresh',
                        handler : this.siswa_sync_handler,
                        scope : this
                    },'-',
                    {   text:'Create User',
                        tooltip:'Create User',
                        iconCls: 'silk-add',
                        handler : this.Create_User,
                        scope : this
                    },
                    {   text:'Reset Password',
                        tooltip:'Reset Password',
                        iconCls: 'silk-add',
                        handler : this.Reset_Password,
                        scope : this
                    },'-',
                    {   text:'Create All Users',
                        tooltip:'Create All Users',
                        iconCls: 'silk-add',
                        handler : this.Create_All_Users,
                        scope : this
                    },'-',
                @endif
                @if (array_key_exists('PRINT_XLS', $MyTasks)) 
                    {   print_type : "xls",
                        text:'Print Excel',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                @endif
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_siswaGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_siswaPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_siswaPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_siswaGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_siswaSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_siswaTab",
                jsId : tabId+"_siswaTab",
                title:  "Siswa",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 50,
                                defaultType: 'textfield',
                                items : this.Searchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.siswa_search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
    @if (array_key_exists('SAVE', $MyTasks)) 
        Create_User: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection.is_user == 1 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: 'User Login Available !',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                     });
            }
            else
            {   alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/siswa/2/90') }}",
                    {   'x-csrf-token': alfalah.siswa.sid }, 
                    {   t: '0',
                        id: this.Grid.getSelectionModel().selection.record.data.keyid }
                );   
            };
        },
        Create_All_Users: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Msg.show(
            {   title   : 'W A R N I N G ',
                msg     : ' Generate users for all SISWA ? ',
                width   : 250,
                buttons : Ext.Msg.YESNO,
                fn: function(buttonId, text)
                    {   if (buttonId =='yes')
                        {   
                            alfalah.core.submitGrid(
                                this.DataStore, 
                                "{{ url('/siswa/2/90') }}",
                                {   'x-csrf-token': alfalah.siswa.sid }, 
                                {   t: '1', id: null}
                            );
                        };
                    },
                icon: Ext.MessageBox.WARNING
            });
        },
        Reset_Password: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/siswa/2/91') }}",
                {   'x-csrf-token': alfalah.siswa.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.keyid }
            );
        },
        siswa_sync_handler : function(button, event)
        {   
            this.SyncDS = alfalah.core.newDataStore(
                "{{ url('/siswa/4/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SyncDS.reload();
        },
    @endif
        // siswa search button
        siswa_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_siswaSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.siswa.task,
                        act: alfalah.siswa.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.removeAll();
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.siswa.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        popSiteDepartment : '',
        // public methods
        initialize: function(the_record)
        {   this.Records = the_record;
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.CurrencyDS = alfalah.core.newDataStore(
                '/country/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.LoadingDS = alfalah.core.newDataStore(
                '/country/3/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.DischargeDS = alfalah.core.newDataStore(
                '/country/3/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.BuyerDS = alfalah.core.newDataStore(
                '/company/2/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.BankDS = alfalah.core.newDataStore(
                '/company/2/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.OrderDS = alfalah.core.newDataStore(
                '/employee/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.ItemDS = alfalah.core.newDataStore(
                '/administration/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.ContainerDS = alfalah.core.newDataStore(
                '/parameter/5/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.PaymentTermDS = alfalah.core.newDataStore(
                '/parameter/4/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            /************************************
                G R I D S
            ************************************/
            this.Columns = [
                {   header: "Items", width : 250,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Items",
                    editor: new Ext.form.ComboBox(
                    {   store: this.ItemDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'item_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form", t:"B",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.item_id = record.data.item_id;
                                    combo.gridEditor.record.data.item_name = record.data.name;
                                    alfalah.siswa.forms.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> [ '+record.data.item_id+' ] ';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Qty (Fcl)", width : 100,
                    dataIndex : 'quantity_fcl', sortable: true,
                    tooltip:"Quantity in FCL",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Qty (Bags)", width : 100,
                    dataIndex : 'quantity_bags', sortable: true,
                    tooltip:"Quantity in Bags",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Nut Qty (Pcs)", width : 100,
                    dataIndex : 'quantity_nut_pcs', sortable: true,
                    tooltip:"Nut Quantity in Pieces",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Nut Qty (Kgs)", width : 100,
                    dataIndex : 'quantity_nut_kgs', sortable: true,
                    tooltip:"Nut Quantity in Kilograms",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Price (USD/Kgs)", width : 100,
                    dataIndex : 'unit_price', sortable: true,
                    tooltip:"Prices",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Amount (USD)", width : 100,
                    dataIndex : 'amount', sortable: true,
                    tooltip:"Amount",
                    editor : new Ext.form.TextField({allowBlank: false}),
                }
            ];
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/siswa/1/5') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : 200, //alfalah.country.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add Item',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    }, '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    }, '-',
                    {   text:'Save Order',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        scope : this
                    }
                ],
                listeners :
                {   //"beforeedit" : this.Grid_beforeedit,
                    "afteredit" : this.Grid_afteredit
                }
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   bodyStyle:'padding:5px',
                // autoScroll : true,
                // autoHeight : true,
                title : 'H E A D E R',
                // tbar: [
                    
                // ],
                items: [ 
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'order_id',
                            xtype:'textfield',
                            fieldLabel: 'ID',
                            name: 'id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'order_container_type',
                            xtype:'textfield',
                            fieldLabel: 'container_type',
                            name: 'container_type',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'order_port_loading', 
                            xtype:'textfield',
                            fieldLabel: 'Port.Loading',
                            name: 'port_loading',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'order_port_discharge', 
                            xtype:'textfield',
                            fieldLabel: 'Port.Discharge',
                            name: 'port_discharge',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'order_payment_term_id', 
                            xtype:'textfield',
                            fieldLabel: 'Payment.Term.ID',
                            name: 'payment_term_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'order_created_date', 
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }]
                    },
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'order_order_date',
                            xtype:'datefield',
                            fieldLabel: 'Order Date',
                            name: 'order_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            value : new Date()
                        },
                        {   id : 'order_order_no', 
                            xtype:'textfield',
                            fieldLabel: 'Order.No',
                            name: 'order_no',
                            anchor:'95%',
                            allowBlank: false,
                        },
                        {   id : 'order_buyer_id',
                            xtype:'combo',
                            fieldLabel: 'Buyer.ID',
                            name: 'buyer_id',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.BuyerDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'company_id',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form", t:"BU",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('order_buyer_id').setValue(record.data.company_id);
                                        Ext.getCmp('order_buyer_name').setValue(record.data.name);
                                    },
                            }
                        },
                        {   id : 'order_bank_id',
                            xtype:'combo',
                            fieldLabel: 'Bank.ID',
                            name: 'bank_id',
                            anchor:'95%',
                            allowBlank: true,
                            store: this.BankDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'company_id',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form", t:"BN",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('order_bank_id').setValue(record.data.company_id);
                                        Ext.getCmp('order_bank_name').setValue(record.data.name);
                                    },
                            }
                        },
                        {   id : 'order_bank_account',
                            xtype:'textfield',
                            fieldLabel: 'Bank Account',
                            name: 'bank_account',
                            anchor:'95%',
                            // allowBlank: false,
                        },
                        ]
                    },
                    {   columnWidth:.7,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'order_company_name', 
                            xtype:'textfield',
                            fieldLabel: 'Company.Name',
                            name: 'company_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'order_repeat_order',
                            xtype:'combo',
                            fieldLabel: 'Repeat.Order',
                            name: 'repeat_order_no',
                            anchor:'95%',
                            allowBlank: true,
                            store: this.OrderDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'order_no',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('order_reporter_name').setValue(record.data.name);
                                    },
                            }
                        },
                        {   id : 'order_buyer_name', 
                            xtype:'textfield',
                            fieldLabel: 'Buyer.Name',
                            name: 'buyer_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            allowBlank: false,
                        },
                        {   id : 'order_bank_name',
                            xtype:'textfield',
                            fieldLabel: 'Bank Name',
                            name: 'bank_name',
                            anchor:'95%',
                            // allowBlank: false,
                        },
                        {   id : 'order_bank_address',
                            xtype:'textfield',
                            fieldLabel: 'Bank.Address',
                            name: 'bank_address',
                            anchor:'95%',
                            // allowBlank: false,
                        },
                        ]
                    }]
                },  // proforma invoice area
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'order_swift_code',
                            xtype:'textfield',
                            fieldLabel: 'Swift.Code',
                            name: 'swift_code',
                            anchor:'95%',
                            // allowBlank: false,
                        },
                        {   id : 'order_pi_no', 
                            xtype:'textfield',
                            fieldLabel: 'PI.No',
                            name: 'pi_no',
                            anchor:'95%',
                        },
                        ]
                    },
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'order_currency_id',
                            xtype:'combo',
                            fieldLabel: 'Currency',
                            name: 'currency_id',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.CurrencyDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'currency_id',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    // 'select' : function (combo, record, indexVal)
                                    // {   Ext.getCmp('order_asset_name').setValue(record.data.name);
                                    // },
                            }
                        },
                        {   id : 'order_pi_date', 
                            xtype:'datefield',
                            fieldLabel: 'PI.Date',
                            name: 'pi_date',
                            anchor:'95%',
                        },
                        ]
                    }]
                },  // shipment area
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'order_container_type_name', 
                            xtype:'combo',
                            fieldLabel: 'Container.Type',
                            name: 'container_type_name',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.ContainerDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'description',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('order_container_type').setValue(record.data.container_type_id);
                                    },
                            }
                        },
                        {   id : 'order_port_loading_name', 
                            xtype:'combo',
                            fieldLabel: 'Port.Loading',
                            name: 'port_loading_name',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.LoadingDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'display',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('order_port_loading').setValue(record.data.city_id);
                                    },
                            }
                        },
                        {   id : 'order_weight_gross',
                            xtype:'textfield',
                            fieldLabel: 'Gross.Weight',
                            name: 'weight_gross',
                            anchor:'95%',
                            value : 0,
                            // allowBlank: false,
                        }]
                    },
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'order_shipment_date', 
                            xtype:'datefield',
                            fieldLabel: 'Shipment.Date',
                            name: 'shipment_date',
                            anchor:'95%',
                        },
                        {   id : 'order_port_discharge_name', 
                            xtype:'combo',
                            fieldLabel: 'Port.Discharge',
                            name: 'port_discharge_name',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.DischargeDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'display',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('order_port_discharge').setValue(record.data.city_id);
                                    },
                            }
                        },
                        
                        {   id : 'order_weight_nett',
                            xtype:'textfield',
                            fieldLabel: 'Gross.Nett',
                            name: 'weight_nett',
                            anchor:'95%',
                            value : 0,
                            // allowBlank: false,
                        }]
                    },
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'order_etd', 
                            xtype:'datefield',
                            fieldLabel: 'E.T.D',
                            name: 'etd',
                            anchor:'95%',
                        },
                        {   id : 'order_eta', 
                            xtype:'datefield',
                            fieldLabel: 'E.T.A',
                            name: 'eta',
                            anchor:'95%',
                        },
                        {   id : 'order_cbm',
                            xtype:'textfield',
                            fieldLabel: 'CBM',
                            name: 'cbm',
                            anchor:'95%',
                            value : 0,
                            // allowBlank: false,
                        }]
                    },
                    ]
                },  // payment term area
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.9,
                        layout: 'form',
                        border:false,
                        items: [ 
                        {   id : 'order_payment_term', 
                            xtype:'combo',
                            fieldLabel: 'Payment Term',
                            name: 'payment_term',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.PaymentTermDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'description',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('order_payment_term_id').setValue(record.data.payment_term_id);
                                    },
                            }
                        },]
                    }]
                }
                ]
            });
        },
        // build the layout
        build_layout: function()
        {   if (Ext.isObject(this.Records))
            {   the_title = "Edit siswa"; }
            else { the_title = "New siswa"; };

            this.Tab = new Ext.Panel(
            {   id : "order_form",
                jsId : "order_form",
                // title:  the_title,
                title : '<span style="background-color: yellow;">'+the_title+'</span>',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region: 'center',
                    xtype: 'tabpanel', // TabPanel itself has no title
                    activeTab: 0,
                    items:[this.Form, this.detailTab]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   if (Ext.isObject(this.Records))
            {   Ext.each(this.Records.fields.items,
                    function(the_field)
                    {   switch (the_field.name)
                        {   case "id" :
                            case "created_date" :
                            case "order_date" :
                            case "repeat_order" :
                            case "buyer_id" :
                            case "buyer_name" :
                            case "bank_id" :
                            case "bank_name" :
                            case "bank_account" :
                            case "bank_address" :
                            case "swift_code" :
                            case "currency_id" : 
                            case "pi_no" :
                            case "pi_date" :
                            case "container_type" :
                            case "container_type_name" :
                            case "shipment_date" :
                            case "etd" :
                            case "eta" :
                            case "cbm" :
                            case "port_loading" :
                            case "port_loading_name" :
                            case "port_discharge" :
                            case "port_discharge_name" :
                            case "weight_gross" :
                            case "weight_nett" :
                            case "payment_term" :
                            {   //console.log('order_'+the_field.name);
                                Ext.getCmp('order_'+the_field.name).setValue(this.Records.data[the_field.name]);
                            };
                            break;
                            case "order_no" :
                            {   Ext.getCmp('order_'+the_field.name).setValue(this.Records.data[the_field.name]);
                                this.DataStore.baseParams = {   
                                    s:"form", order_no: this.Records.data["order_no"],
                                    limit:this.page_limit, start:this.page_start};
                                this.DataStore.reload();
                            };
                            break;
                            
                        };
                    }, this);
            };

            this.Records = Ext.data.Record.create(
            [   {name: 'order_no', type: 'string'},
                {name: 'item_id', type: 'string'},
                {name: 'quantity_fcl', type: 'string'},
                {name: 'quantity_bags', type: 'string'},
                {name: 'quantity_nut_pcs', type: 'string'},
                {name: 'quantity_nut_kgs', type: 'string'},
                {name: 'unit_price', type: 'string'},
                {name: 'amount', type: 'string'},
                {name: 'currency_id', type: 'string'},
                {name: 'created_date', type: 'date'}
            ]);
        },
        // items grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   item_id: "New Item",
                    quantity_fcl : 0,
                    quantity_bags : 0,
                    quantity_nut_pcs : 0,
                    quantity_nut_kgs : 0,
                    unit_price : 0,
                    amount : 0,
                    currency_id : "",
                    created_date: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 0);
        },
        "Grid_afteredit" : function(the_cell)
        {   var data = the_cell.record.data;
            var value = parseInt(the_cell.value);
            var unit_price = parseInt(data.unit_price);
            var quantity_nut_kgs = parseInt(data.quantity_nut_kgs);
            data.amount = unit_price * quantity_nut_kgs;
            the_cell.grid.getView().refresh();
        },
        // forms grid save records
        Form_save : function(button, event)
        {   var head_data = [];
            var json_data = [];
            var modi_data = [];
            var v_json = {};
            // header validation
            if (alfalah.core.validateFields([
                'order_order_no', 'order_order_date', 'order_buyer_name']))
            {   // detail validation
                var item_count = this.DataStore.getCount()
                if ( item_count < 1 ) 
                {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'Please register minimum 1 Item.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO,
                        width : 300
                    });
                }
                else
                {   // header
                    head_data = alfalah.core.getHeadData([
                        'order_id', 'order_created_date', 'order_order_date',
                        'order_order_no', 'order_repeat_order',
                        'order_buyer_id', 'order_buyer_name', 
                        'order_bank_id', 'order_bank_name',
                        'order_bank_account', 'order_bank_address',
                        'order_swift_code', 'order_currency_id', 
                        'order_pi_no', 'order_pi_date',
                        'order_container_type', 'order_shipment_date', 
                        'order_etd', 'order_eta', 'order_cbm',
                        'order_port_loading', 'order_port_discharge',
                        'order_weight_gross', 'order_weight_nett',
                        'order_payment_term', 'order_payment_term_id'
                        ]);
                    // order detail
                    json_data = alfalah.core.getDetailData(this.DataStore.getModifiedRecords());
                    // submit data
                    alfalah.core.submitForm(
                        'order_form', 
                        alfalah.siswa.siswa.DataStore,
                        "{{ url('/siswa/1/1') }}",
                        {   'x-csrf-token': alfalah.siswa.sid },
                        {   task: 'save',
                            head : Ext.encode(head_data),
                            json : Ext.encode(json_data),
                        });
                };
            };
        },
    }; // end of public space
}(); // end of app
// On Ready

Ext.onReady(alfalah.siswa.initialize, alfalah.siswa);

// end of file
</script>
<div>&nbsp;</div>