<!-- Using default Layout -->
@extends('layouts_backend._iframe_backend')
<!-- load your extry css styles -->
@section('extra_styles')
<link rel="stylesheet" href="../../w2ui/w2ui-1.5.rc1.min.css" />
<link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
<!-- <style>
.tab {
    width: 100%;
    height: 400px;
    border: 1px solid silver;
    border-top: 0px;
    display: none;
    padding: 10px;
    overflow: auto;
}
</style> -->
@endsection
<!-- load your main content page -->
@section('content')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Pembayaran Siswa</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Tahun/Bulan</th>
                <th>Biaya</th>
                <th>Bulan</th>
                <th>Piutang</th>
                <th>Potongan</th>
                <th>Pembayaran</th>
              </tr>
            </thead>
            <tbody>
            @foreach( $RECEIVABLES as $receivable )
                <tr>
                  <td>{{ $receivable->bultah }}</td>
                  <td>{{ $receivable->nama_biaya_sekolah }}</td>
                  <td>{{ $receivable->bulan }}</td>
                  <td>{{ $receivable->piutang }}</td>
                  <td>{{ $receivable->potongan }}</td>
                  <td>{{ $receivable->pembayaran }}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>Tahun/Bulan</th>
                <th>Biaya</th>
                <th>Bulan</th>
                <th>Piutang</th>
                <th>Potongan</th>
                <th>Pembayaran</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->
@endsection
<!-- load your extra js scripts -->
@section('extra_scripts')
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- <script src="../../bower_components/fastclick/lib/fastclick.js"></script> -->
<!-- AdminLTE App -->
<!-- <script src="../../dist/js/adminlte.min.js"></script> -->
<!-- AdminLTE for demo purposes --><!-- 
<script src="dist/js/demo.js"></script> -->
<!--AdminLTE Iframe-->
<!-- <script src="dist/js/app_iframe.js"></script> -->
<!-- <script src="../../w2ui/w2ui-1.5.rc1.min.js"></script> -->
<script type="text/javascript">
  // Define namespace
  alfalah = parent.alfalah;
  alfalah.namespace('alfalah.receivable');
  // create application
  alfalah.receivable = function()
  {   // do NOT access DOM from here; elements don't exist yet
      // execute at the first time
      // private variables
      // this.tabId = '{{ $TABID }}';
      this.config;
      // private functions
      // public space
      return {
          centerPanel : 0,
          the_records : [],
          sid : '{{ csrf_token() }}',
          task : ' ',
          act : ' ',
          // public methods
          initialize: function()
          { console.log('initialize');
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
          },
          // prepare the component before layout drawing
          prepare_component: function()
          { console.log('prepare_component');
            
          },
          // build the layout
          build_layout: function()
          { console.log('build_layout');
            
          },
          // finalize the component and layout drawing
          finalize_comp_and_layout: function()
          { console.log('finalize_comp_and_layout');
          },
      }; // end of public space
  }(); // end of app
  $(document).ready(alfalah.receivable.initialize());
</script>

@endsection
