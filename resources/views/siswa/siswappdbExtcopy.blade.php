<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.calonsiswa');

// create application
alfalah.calonsiswa = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.registration.initialize();
            this.observation.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.registration.Tab);
            this.centerPanel.add(this.observation.Tab);
            this.centerPanel.setActiveTab(this.registration.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.calonsiswa.registration= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header    : "Id", width : 50,
                    dataIndex : 'keyid', sortable: true,
                    tooltip   : "Id",
                    hidden    : true,
                },
                {   header    : "No.Pendaftaran", width : 100,
                    dataIndex : 'no_pendaftaran', sortable: true,
                    tooltip   : "Nomor Pendaftaran",
                    renderer  : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.observation_result == "BERGABUNG")
                        {   metaData.attr = "style = background-color:lime;"; }
                        else if ( record.data.observation_result == "TIDAK BERGABUNG")
                        {   metaData.attr = "style = background-color:red;"; }
                        else { metaData.attr = "style = background-color:yellow;"; };
                        return value;
                    },
                },
                {   header    : "Tgl Daftar", width : 100,
                    dataIndex : 'created_date', sortable: true,
                    tooltip   : "tgl pendaftaran",
                    renderer : function (value, metaData, record, rowIndex, colIndex, store)
                        { return Ext.util.Format.date(value, "l, d F, Y, h:i:s");
                          //  return value;
                        }
                },
                {   header      : "No.Registration", width : 100,
                    dataIndex   : 'no_registration', sortable: true,
                    tooltip     : "Nomor Registration",
                    search_null : true,
                    renderer    : function(value, metaData, record, rowIndex, colIndex, store)
                    {
                        if ( record.data.observation_date)
                        {   metaData.attr = "style = background-color:orange;"; }
                        else if ( record.data.no_registration)
                        {   metaData.attr = "style = background-color:yellow;"; }
                        else { metaData.attr = "style = background-color:red;"; };
                        return value;
                    },
                },
                {   header    : "Jenjang", width : 100,
                    dataIndex : 'jenjang_sekolah_name', sortable: true,
                    tooltip   : "Jenjang Sekolah",
                },

                {   header    : "Nama Lengkap", width : 200,
                    dataIndex : 'nama_lengkap', sortable: true,
                    tooltip   : "Nama Lengkap",
                },
                {   header    : "Panggilan", width : 100,
                    dataIndex : 'nama_panggilan', sortable: true,
                    tooltip   : "Nama Lengkap",
                },
                {   header    : "Gender", width : 80,
                    dataIndex : 'gender_name', sortable: true,
                    tooltip   : "Jenis Kelamin",
                },
                {   header    : "Tempat Lahir", width : 100,
                    dataIndex : 'tempat_lahir', sortable: true,
                    tooltip   : "Tempat Lahir",
                },
                {   header    : "Tanggal Lahir", width : 100,
                    dataIndex : 'tanggal_lahir', sortable: true,
                    tooltip   : "Tanggal Lahir",
                    renderer  : function(value){  return alfalah.core.shortdateRenderer(value,'Y-m-d', 'd/m/Y'); }
                },
                {   header    : "Alamat", width : 250,
                    dataIndex : 'alamat_rumah', sortable: true,
                    tooltip   : "Alamat Rumah",
                },
                {   header    : "kecamatan", width : 100,
                    dataIndex : 'kecamatan', sortable: true,
                    tooltip   : "kecamatan",
                },
                {   header    : "Kota", width : 100,
                    dataIndex : 'kota', sortable: true,
                    tooltip   : "Kota",
                },
                {   header    : "Ayah Kandung", width : 100,
                    dataIndex : 'nama_ayah', sortable: true,
                    tooltip   : "Nama Ayah Kandung",
                },
                {   header    : "Telp Ayah", width : 100,
                    dataIndex : 'telp_hp_ayah', sortable: true,
                    tooltip   : "Telp Ayah ",
                },

                {   header    : "Ibu Kandung", width : 100,
                    dataIndex : 'ibu_kandung', sortable: true,
                    tooltip   : "Nama Ibu Kandung",
                },
                {   header    : "Telp Rumah", width : 100,
                    dataIndex : 'telp_rumah', sortable: true,
                    tooltip   : "Nomer Telepon Rumah",
                },
                {   header    : "Kewarganegaraan", width : 100,
                    dataIndex : 'warganegara', sortable: true,
                    hidden : true,
                    tooltip   : "Kewarganegaraan",
                },
                {   header    : "Agama", width : 100,
                    dataIndex : 'agama', sortable: true,
                    tooltip   : "Agama",
                },
                {   header    : "Keadaan Siswa", width : 100,
                    dataIndex : 'keadaan_siswa', sortable: true,
                    tooltip   : "Keadaan Siswa",
                },
                {   header    : "Tinggal Dengan", width : 100,
                    dataIndex : 'tinggaldengan', sortable: true,
                    tooltip   : "Siswa Tinggal Dengan",
                },
                {   header    : "Tinggi", width : 100,
                    dataIndex : 'tinggi_badan', sortable: true,
                    tooltip   : "Tinggi Badan",
                },
                {   header    : "Berat", width : 100,
                    dataIndex : 'berat_badan', sortable: true,
                    tooltip   : "Berat",
                },
                {   header    : "Gol.Darah", width : 100,
                    dataIndex : 'bloodtype', sortable: true,
                    tooltip   : "Golongan Darah",
                },
                {   header    : "Penyakit Bawaan", width : 100,
                    dataIndex : 'penyakit_bawaan', sortable: true,
                    tooltip   : "Penyakit Bawaan",
                },
                {   header    : "Jenis Pembayaran", width : 100,
                    dataIndex : 'jenis_bayar', sortable: true,
                    tooltip   : "Penyakit Bawaan",
                },
                {   header    : "Internal/External", width : 100,
                    dataIndex : 'sekolah_asal_internal', sortable: true,
                    tooltip   : "internal atau external alfalah",
                },

            ];

            this.Records = Ext.data.Record.create(
            [   {name: 'calonsiswa_id', type: 'integer'},
                {name: 'modified_date', type: 'date'},
            ]);

            this.DepartmentDS = alfalah.core.newDataStore(
                "{{ url('/company/5/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start,
                    type:"Division" }
            );
            this.DepartmentDS.load();

            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/siswa/5/0') }}", false,
                {   s:"init", t:"daftar", limit:this.page_limit, start:this.page_start }
            );

            this.Searchs = [
                {   id             : 'calonsiswa_nopen',
                    cid            : 'nopen',
                    fieldLabel     : 'No.Pen',
                    labelSeparator : '',
                    xtype          : 'textfield',
                    width          : 120
                },
                {   id             : 'calonsiswa_nama_lengkap',
                    cid            : 'nama_lengkap',
                    fieldLabel     : 'Nama',
                    labelSeparator : '',
                    xtype          : 'textfield',
                    width          : 120
                },
                {   id             : 'calonsiswa_nama_panggilan',
                    cid            : 'nama_panggilan',
                    fieldLabel     : 'Panggilan',
                    labelSeparator : '',
                    xtype          : 'textfield',
                    width          : 120
                },
                {   id             : 'calonsiswa_alamat_rumah',
                    cid            : 'alamat_rumah',
                    fieldLabel     : 'Alamat',
                    labelSeparator : '',
                    xtype          : 'textfield',
                    width          : 120
                },
                {   id             : 'calonsiswa_gender',
                    cid            : 'gender',
                    fieldLabel     : 'L/P',
                    labelSeparator : '',
                    xtype          : 'combo',
                    store          : new Ext.data.SimpleStore(
                    {   fields: ['label'],
                        data : [ ['ALL'], ['Laki-Laki'], ['Perempuan']]
                    }),
                    displayField   : 'label',
                    valueField     : 'label',
                    mode           : 'local',
                    triggerAction  : 'all',
                    selectOnFocus  : true,
                    editable       : false,
                    width          : 100,
                    value          : 'ALL'
                },

                {   id             : 'calonsiswa_jenjang_sekolah',
                    cid            : 'jenjang_sekolah_id',
                    xtype          : 'combo',
                    fieldLabel     : 'Jenjang',
                    labelSeparator : '',
                    name           : 'jenjang_sekolah_name',
                    anchor         : '95%',
                    allowBlank     : false,
                    readOnly       : false,
                    store          : this.DepartmentDS,
                    displayField   : 'name',
                    valueField     : 'dept_id',
                    mode           : 'local',
                    forceSelection : true,
                    triggerAction  : 'all',
                    selectOnFocus  : true,
                    editable       : false,
                    typeAhead      : true,
                    width          : 100,
                    value          : 'ALL'
                },
                {   id             : 'calonsiswa_InEx',
                    cid            : 'sekolah_asal_internal',
                    fieldLabel     : 'Int/Ext',
                    labelSeparator : '',
                    xtype          : 'combo',
                    store          : new Ext.data.SimpleStore(
                    {   fields: ['label'],
                        data : [ ['ALL'], ['Internal'], ['External']]
                    }),
                    displayField   : 'label',
                    valueField     : 'label',
                    mode           : 'local',
                    triggerAction  : 'all',
                    selectOnFocus  : true,
                    editable       : false,
                    width          : 100,
                    value          : 'ALL'
                },
                {   id             : 'calonsiswa_jenis_bayar',
                    cid            : 'jenis_bayar',
                    fieldLabel     : 'Jenis Bayar',
                    labelSeparator : '',
                    xtype          : 'combo',
                    store          : new Ext.data.SimpleStore(
                    {   fields: ['label'],
                        data : [ ['ALL'], ['Tunai'], ['Non Tunai']]
                    }),
                    displayField   : 'label',
                    valueField     : 'label',
                    mode           : 'local',
                    triggerAction  : 'all',
                    selectOnFocus  : true,
                    editable       : false,
                    width          : 100,
                    value          : 'ALL'
                },

            ];
            this.SearchBtn = new Ext.Button(
            {   id         : tabId+"_calonsiswaSearchBtn",
                fieldLabel : '',
                text       : 'Search',
                tooltip    : 'Search',
                iconCls    : 'silk-zoom',
                xtype      : 'button',
                width      : 120,
                handler    : this.calonsiswa_search_handler,
                scope      : this
            });
            //prepare null parameter items
            this.Searchs_null = alfalah.core.newSearchNull(this.Columns, 'calonsiswa_N');

            this.Grid = new Ext.grid.EditorGridPanel(
            {   store         :  this.DataStore,
                columns       : this.Columns,
                enableColLock : false,
                loadMask      : true,
                height        : alfalah.calonsiswa.centerPanel.container.dom.clientHeight-50,
                anchor        : '100%',
                autoScroll    : true,
                frame         : true,
                tbar          : [
                    @if (array_key_exists('SAVE', $MyTasks))
                    {   text    : 'Registration',
                        tooltip : 'Edit Record',
                        iconCls : 'silk-accept',
                        handler : this.Grid_edit,
                        scope   : this
                    },
                    '-',
                    @endif
                    @if (array_key_exists('PRINT_XLS', $MyTasks))
                    {   print_type : "xls",
                        text       : 'Print Excel',
                        tooltip    : 'Print to Excell SpreadSheet',
                        iconCls    : 'silk-page-white-excel',
                        handler    : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope      : this
                    },'-',
	            @endif
                    @if (array_key_exists('PRINT_PDF', $MyTasks))
                    {   print_type : "pdf",
                        text       : 'Print Photo',
                        tooltip    : 'Print All Photos to PDF',
                        iconCls    : 'silk-page-white-acrobat',
                        handler    : function(button, event){
                            var the_record = this.Grid.getSelectionModel().selection;
                            if ( the_record )
                            {
                                this.DataStore.baseParams = Ext.apply( the_parameter, the_parameter_null,
                                    {   s:"form", t:"daftar",
                                        no_pendaftaran:the_record.record.data.no_pendaftaran,
                                        limit:this.page_limit, start:this.page_start
                                    });
                                alfalah.core.printButton(button, event, this.DataStore);
                            }
                            else
                            {   Ext.Msg.show(
                                    {   title   : 'I N F O ',
                                        msg     : 'No Data Selected ! ',
                                        buttons : Ext.Msg.OK,
                                        icon    : Ext.MessageBox.INFO
                                    });
                            };
                        },
                        scope      : this
                    },'-',
                    @endif
                ],
                bbar: new Ext.PagingToolbar(
                {   id          : tabId+'_calonsiswaGridBBar',
                    store       : this.DataStore,
                    pageSize    : this.page_limit,
                    displayInfo : true,
                    emptyMsg    : 'No data found',
                    items       : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id            : tabId+'_calonsiswaPageCombo',
                            store         : new Ext.data.SimpleStore(
                            {   fields : ['value'],
                                data   : [[50],[75],[100],[125],[150]]
                            }),
                            displayField  : 'value',
                            valueField    : 'value',
                            value         : 75,
                            editable      : false,
                            mode          : 'local',
                            triggerAction : 'all',
                            selectOnFocus : true,
                            hiddenName    : 'pagesize',
                            width         : 50,
                            listeners     : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_calonsiswaPageCombo').getValue();
                                    bbar            = Ext.getCmp(tabId+'_calonsiswaGridBBar');
                                    bbar.pageSize   = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_calonsiswaSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id     : tabId+"_calonsiswaTab",
                jsId   : tabId+"_calonsiswaTab",
                title  :  "Registration",
                region : 'center',
                layout : 'border',
                items  : [
                {   title       : 'Parameters',
                    region      : 'east',     // position for region
                    split       : true,
                    width       : 200,
                    minSize     : 200,
                    maxSize     : 400,
                    collapsible : true,
                    layout      : 'fit',
                    items       : new Ext.TabPanel(
                        {   border      : false,
                            activeTab   : 0,
                            tabPosition : 'bottom',
                            items       : [
                            new Ext.FormPanel(
                            {   title       : 'S E A R C H',
                                labelWidth  : 50,
                                defaultType : 'textfield',
                                items       : this.Searchs,
                                frame       : true,
                                autoScroll  : true,
                                tbar        : [
                                {   text    : 'Search',
                                    tooltip : 'Search',
                                    iconCls : 'silk-zoom',
                                    handler : this.calonsiswa_search_handler,
                                    scope   : this,
                                }]
                            }),
                            new Ext.FormPanel(
                            {   title       : 'N U L L',
                                labelWidth  : 50,
                                defaultType : 'textfield',
                                items       : this.Searchs_null,
                                frame       : true,
                                autoScroll  : true,
                            }),
                            ]
                        })
                },
                {
                    region : 'center',     // center region is required, no width/height specified
                    xtype  : 'container',
                    layout : 'fit',
                    items  : [this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        @if (array_key_exists('SAVE', $MyTasks))
        Grid_edit : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var form_order = Ext.getCmp("form_order");
                if (form_order)
                {   Ext.Msg.show(
                    {   title   : 'E R R O R ',
                        msg     : 'Order Form Available',
                        buttons : Ext.Msg.OK,
                        icon    : Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    alfalah.calonsiswa.forms.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.calonsiswa.forms.Tab);
                    centerPanel.setActiveTab(alfalah.calonsiswa.forms.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title   : 'I N F O ',
                        msg     : 'No Data Selected ! ',
                        buttons : Ext.Msg.OK,
                        icon    : Ext.MessageBox.INFO
                    });
            };
        },
        @endif
        // calonsiswa search button
        calonsiswa_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title   : 'W A R N I N G ',
                        msg     : ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons : Ext.Msg.YESNO,
                        fn      : function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_calonsiswaSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {
                the_parameter             = alfalah.core.getSearchParameter(this.Searchs);
                the_parameter_null        = alfalah.core.getSearchParameter(this.Searchs_null);
                this.DataStore.baseParams = Ext.apply( the_parameter, the_parameter_null,
                    {   s:"form", t:"daftar",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.removeAll();
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.calonsiswa.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        popSiteDepartment : '',
        // public methods
        initialize: function(the_record)
        {   this.Records = the_record;
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/siswa/5/3') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start, nopen:this.Records.data['no_pendaftaran'] }
            );

            this.ParentsDS = alfalah.core.newDataStore(
                "{{ url('/siswa/5/4') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start, nopen:this.Records.data['no_pendaftaran'] }
            );

            this.kelas_Sd = new Ext.data.SimpleStore(
            {   fields: ['label'],
                data : [ ['TK A'], ['TK B'],['1'], ['2'], ['3'], ['4'],['5'], ['7'],['8']],
              //  storeId : 'kelas_sd_id'
            });

            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   bodyStyle : 'padding:5px',
                iconCls   : 'silk-user',
                title     : 'DATA CALON SISWA',
                items     : [
                {   layout : 'column',
                    border : false,
                    items  :[
                    {   columnWidth :.3,
                        layout      : 'form',
                        border      : false,
                        items       : [ // hidden columns
                        {   id         : 'siswappdb_id',
                            xtype      : 'textfield',
                            fieldLabel : 'ID',
                            name       : 'id',
                            anchor     : '95%',
                            allowBlank : true,
                            readOnly   : true,
                            hidden     : true,
                        },
                        {   id         : 'siswappdb_gender',
                            xtype      : 'textfield',
                            fieldLabel : 'JK',
                            name       : 'gender',
                            anchor     : '95%',
                            allowBlank : true,
                            readOnly   : true,
                            hidden     : true,
                        },
                        {   id         : 'siswappdb_tahunajaran_id',
                            xtype      : 'textfield',
                            fieldLabel : 'Tahun Ajaran',
                            name       : 'tahunajaran_id',
                            anchor     : '95%',
                            allowBlank : true,
                            readOnly   : true,
                            hidden     : true,
                        },
                        {   id         : 'siswappdb_jenjang_sekolah_id',
                            xtype      : 'textfield',
                            fieldLabel : 'Jenjang.ID',
                            name       : 'jenjang_sekolah_id',
                            anchor     : '95%',
                            allowBlank : false,
                            readOnly   : true,
                            hidden     : true
                        },
                        {   id         : 'siswappdb_payment_confirm_by',
                            xtype      : 'textfield',
                            fieldLabel : 'JK',
                            name       : 'payment_confirm_by',
                            anchor     : '95%',
                            allowBlank : true,
                            readOnly   : true,
                            hidden     : true,
                        },
                        {   id         : 'siswappdb_created_date',
                            xtype      : 'textfield',
                            fieldLabel : 'Created Date',
                            name       : 'created_date',
                            anchor     : '95%',
                            allowBlank : true,
                            readOnly   : true,
                            hidden     : true,
                        }]
                    },
                    {   columnWidth :.3,
                        layout      : 'form',
                        border      : false,
                        items       : [
                        {   id         : 'siswappdb_no_pendaftaran',
                            xtype      : 'textfield',
                            fieldLabel : 'No.Pendaftaran',
                            name       : 'no_pendaftaran',
                            anchor     : '95%',
                            allowBlank : false,
                            readOnly   : true,
                        },
                        {   id         : 'siswappdb_no_registration',
                            xtype      : 'textfield',
                            fieldLabel : 'No.Registrasi',
                            name       : 'no_registration',
                            anchor     : '95%',
                            allowBlank : false,
                            readOnly   : true,
                        },
                        {   id         : 'siswappdb_jenjang_sekolah_name',
                            xtype      : 'textfield',
                            fieldLabel : 'Jenjang',
                            name       : 'jenjang_sekolah_name',
                            anchor     : '95%',
                            allowBlank : false,
                            readOnly   : true,
                        },
                        {   id         : 'siswappdb_tempat_lahir',
                            xtype      : 'textfield',
                            fieldLabel : 'Tempat Lahir',
                            name       : 'tempat_lahir',
                            anchor     : '95%',
                            readOnly   : true,
                            allowBlank : false,
                        },
                        {   id         : 'siswappdb_tanggal_lahir',
                            xtype      : 'textfield',
                            fieldLabel : 'Tanggal Lahir',
                            name       : 'tanggal_lahir',
                            anchor     : '95%',
                            readOnly   : true,
                            allowBlank : false,
                        },
                        {   id         : 'siswappdb_warganegara',
                            xtype      : 'textfield',
                            fieldLabel : 'Kewarganegaraan',
                            name       : 'warganegara',
                            anchor     : '95%',
                            readOnly   : true,
                            allowBlank : false,
                        },
                        {   id         : 'siswappdb_agama',
                            xtype      : 'textfield',
                            fieldLabel : 'Agama',
                            name       : 'Agama',
                            anchor     : '95%',
                            readOnly   : true,
                            allowBlank : false,
                        },
                        ]
                    },
                    {   columnWidth :.7,
                        layout      : 'form',
                        border      : false,
                        items       : [
                        {   id         : 'siswappdb_nama_lengkap',
                            xtype      : 'textfield',
                            fieldLabel : 'Nama',
                            name       : 'nama_lengkap',
                            anchor     : '95%',
                            allowBlank : false,
                            readOnly   : true,
                        },
                        {   id         : 'siswappdb_nama_panggilan',
                            xtype      : 'textfield',
                            fieldLabel : 'Panggilan',
                            name       : 'nama_panggilan',
                            anchor     : '95%',
                            allowBlank : false,
                            readOnly   : true,
                        },
                        {   id         : 'siswappdb_ibu_kandung',
                            xtype      : 'textfield',
                            fieldLabel : 'Ibu Kandung',
                            name       : 'ibu_kandung',
                            anchor     : '95%',
                            readOnly   : true,
                            allowBlank : false,
                        },
                        {   id         : 'siswappdb_alamat_rumah',
                            xtype      : 'textfield',
                            fieldLabel : 'Alamat',
                            name       : 'alamat_rumah',
                            anchor     : '95%',
                            readOnly   : true,
                            allowBlank : false,
                        },
                        {   id         : 'siswappdb_kecamatan',
                            xtype      : 'textfield',
                            fieldLabel : 'Kecamatan',
                            name       : 'kecamatan',
                            anchor     : '95%',
                            readOnly   : true,
                            allowBlank : false,
                        },
                        {   id         : 'siswappdb_kota',
                            xtype      : 'textfield',
                            fieldLabel : 'Kota',
                            name       : 'kota',
                            anchor     : '95%',
                            readOnly   : true,
                            allowBlank : false,
                        },
                        {   id         : 'siswappdb_telp_rumah',
                            xtype      : 'textfield',
                            fieldLabel : 'Telp',
                            name       : 'telp_rumah',
                            anchor     : '95%',
                            readOnly   : true,
                            allowBlank : false,
                        },

                        ]
                    }]
                },  // payment term area
                {   layout : 'column',
                    border : false,
                    items  :[
                    {   columnWidth :.5,
                        layout      : 'form',
                        border      : false,
                        items       : [
                        {   title          : 'Informasi Fisik',
                            xtype          : 'fieldset',
                            autoHeight     : true,
                            anchor         : '95%',
                            // defaultType : 'checkbox', // each item will be a checkbox
                            items          : [
                            {   id         : 'siswappdb_gender_name',
                                xtype      : 'textfield',
                                fieldLabel : 'Gender',
                                name       : 'gender_name',
                                anchor     : '95%',
                                readOnly   : true,
                                allowBlank : false,
                            },
                            {   id         : 'siswappdb_tinggi_badan',
                                xtype      : 'textfield',
                                fieldLabel : 'Tinggi (Cm)',
                                name       : 'tinggi_badan',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                            },
                            {   id         : 'siswappdb_berat_badan',
                                xtype      : 'textfield',
                                fieldLabel : 'Berat (Kg)',
                                name       : 'berat_badan',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                            },
                            {   id         : 'siswappdb_bloodtype',
                                xtype      : 'textfield',
                                fieldLabel : 'bloodtype',
                                name       : 'bloodtype',
                                anchor     : '95%',
                                readOnly   : true,
                                allowBlank : false,
                            },
                            {   id         : 'siswappdb_penyakit_bawaan',
                                xtype      : 'textfield',
                                fieldLabel : 'Penyakit Bawaan',
                                name       : 'penyakit_bawaan',
                                anchor     : '95%',
                                readOnly   : true,
                                allowBlank : true,
                                value      : ''
                            }]
                        }]
                    },
                    {   columnWidth :.5,
                        layout      : 'form',
                        border      : false,
                        items       : [
                        {   title      : 'Informasi Lingkungan Siswa',
                            xtype      : 'fieldset',
                            autoHeight : true,
                            anchor     : '95%',
                            items      : [
                            {   id             : 'siswappdb_keadaan_siswa',
                                xtype          : 'combo',
                                fieldLabel     : 'Keadaan Siswa',
                                labelSeparator : '',
                                name           : 'keadaan_siswa',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : true,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['Orang Tua Lengkap'], ['Yatim Piatu'],['Yatim'], ['Piatu']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {   id             : 'siswappdb_tinggaldengan',
                                xtype          : 'combo',
                                fieldLabel     : 'Tinggal Bersama',
                                labelSeparator : '',
                                name           : 'tinggaldengan',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : true,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['Orang Tua'], ['Kakek Nenek'],['Orang Lain']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {   border : false,
                                autoHeight:true,
                                layout:'column',
                                items:[{
                                    columnWidth:.5,
                                    border :false,
                                    layout: 'form',
                                    items: [{
                                        id         : 'siswappdb_anakke',
                                        style      : "background-color: #ffff00; background-image:none;",
                                        xtype      : 'textfield',
                                        style      : "textTransform: uppercase ;",
                                        // allowBlank : false,
                                        readOnly   : true,
                                        fieldLabel : 'Anak ke',
                                        name       : 'anakke',
                                        anchor     : '95%'
                                    }]
                                },{
                                    columnWidth:.5,
                                    layout: 'form',
                                    border :false,
                                    items: [{
                                        id         : 'siswappdb_brpsaudara',
                                        style      : "background-color: #ffff00; background-image:none;",
                                        xtype      : 'textfield',
                                        style      : "textTransform: uppercase ;",
                                        // allowBlank : false,
                                        readOnly   : true,
                                        fieldLabel : 'Dari Brp. Sdr ',
                                        name       : 'brpsaudara',
                                        anchor     : '90%'
                                    }]
                                }]
                            }]
                        }]
                    },
                    ]
                },
                ]
            });
            this.Form_sekolah = new Ext.FormPanel(
            {   bodyStyle : 'padding:5px',
                iconCls   : 'silk-house',
                title     : 'ASAL SEKOLAH',
                items     : [ // INFORMASI SEKOLAH
                {   layout : 'column',
                    border : false,
                    items  : [
                    {   columnWidth : .5,
                        layout      : 'form',
                        border      : false,
                        labelWidth  : 110,
                        labelPad    : 10,
                        items       : [
                        {   title      : 'Informasi Sekolah Asal',
                            xtype      : 'fieldset',
                            autoHeight : true,
                            anchor     : '95%',
                            items      : [
                            {   id         : 'keterangan_internal_external',
                                xtype      : 'fieldset',
                                title      : 'Internal Jika Berasal Dari Alfalah Darussalam, External Jika dari Luar Alfalah Darussalam',
                                name       : 'sekolah_asal_nama',
                                autoHeight : true,
                                items      : [
                                    {
                                        id             : 'siswappdb_sekolah_asal_internal',
                                        xtype          : 'combo',
                                        fieldLabel     : 'Internal/External',
                                        labelSeparator : '',
                                        name           : 'sekolah_asal_internal',
                                        anchor         : '95%',
                                        // allowBlank     : false,
                                        readOnly       : true,
                                        store          : new Ext.data.SimpleStore(
                                        {   fields     : ['label'],
                                            data       : [ ['Internal'], ['External']]
                                        }),
                                        displayField   : 'label',
                                        valueField     : 'label',
                                        mode           : 'local',
                                        triggerAction  : 'all',
                                        selectOnFocus  : true,
                                        editable       : false,
                                        width          : 100,
                                        value          : 'Pilihlah'
                                    },
                                ]
                            },
                            {
                                id         : 'siswappdb_sekolah_asal_nama',
                                xtype      : 'textfield',
                                fieldLabel : 'Sekolah Asal',
                                style      : "textTransform: uppercase ;",
                                name       : 'sekolah_asal_nama',
                                anchor     : '95%',
                                // allowBlank : false,
                                readOnly   : true,
                                value      : ''
                            },

                            {
                                id             : 'siswappdb_sekolah_asal_status',
                                xtype          : 'combo',
                                fieldLabel     : 'Status',
                                labelSeparator : '',
                                name           : 'sekolah_asal_status',
                                anchor         : '95%',
                                // allowBlank     : false,
                                readOnly       : true,
                                store          : new Ext.data.SimpleStore(
                                {   fields     : ['label'],
                                    data       : [ ['Negeri'], ['Swasta']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {
                                id         : 'siswappdb_sekolah_asal_alamat',
                                xtype      : 'textfield',
                                style      : "textTransform: uppercase ;",
                                fieldLabel : 'Alamat',
                                name       : 'sekolah_asal_alamat',
                                anchor     : '95%',
                                // allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {
                                id         : 'siswappdb_sekolah_asal_lulus',
                                xtype      : 'textfield',
                                fieldLabel : 'Tahun Lulus',
                                name       : 'sekolah_asal_lulus',
                                style      : "textTransform: uppercase ;",
                                anchor     : '95%',
                                // allowBlank : false,
                                readOnly   : true,
                                msgTarget  : 'side',
                                blankText  : 'This should not be blank!',
                                value      : ''
                            },
                            {   id         : 'siswappdb_sekolah_asal_akreditas',
                                xtype      : 'textfield',
                                fieldLabel : 'Akreditas Sekolah',
                                name       : 'sekolah_asal_akreditas',
                                style      : "textTransform: uppercase ;",
                                anchor     : '95%',
                                // allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_sekolah_asal_nilai_un',
                                xtype      : 'textfield',
                                fieldLabel : 'Nilai U.N',
                                name       : 'sekolah_asal_nilai_un',
                                style      : "textTransform: uppercase ;",
                                anchor     : '95%',
                                // allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            ]
                        },
                        {
                            title      : 'Khusus Di Isi Jika Siswa Pindahan',
                            xtype      : 'fieldset',
                            autoHeight : true,
                            anchor     : '95%',
                            items: [
                                {
                                    id             : 'siswappdb_kelas_pindahan',
                                    xtype          : 'combo',
                                    fieldLabel     : 'Kelas',
                                    style          : "background-color: #ffff00; background-image:none; ",
                                    name           : 'kelas_pindahan',
                                    anchor         : '95%',
                                    store          : this.kelas_Sd,
                                    allowBlank     : true,
                                    readOnly       : true,
                                    displayField   : 'label',
                                    valueField     : 'label',
                                    mode           : 'local',
                                    forceSelection : true,
                                    triggerAction  : 'all',
                                    selectOnFocus  : false,
                                    editable       : true,
                                    width          : 100,
                                    lastQuery      : '',
                                    value          : '',
                                    // listeners      : { scope : this,
                                    // 'beforequery' : function(combo, query, forceAll, cancel) {
                                    //     console.log('tessssssssss');
                                    //     var a = Ext.getCmp('siswappdb_jenjang_sekolah_id').getValue();
                                    //     console.log(combo.combo.reset());
                                    //     combo.combo.clearValue();
                                    //     if (a==1) {
                                    //         console.log('tralalala');
                                    //         //   combo.combo.setValue();
                                    //         combo.combo.store = this.kelas_Sd;
                                    //         }
                                    // }
                                    // }
                                },
                            ]
                        },

                        ]
                    },
                    {   columnWidth:.5,
                        layout: 'form',
                        border:false,
                        items: [
                            {
                            title: 'Khusus Di Isi Jika Calon Siswa mempunyai saudara di Lembaga Pendidikan Alfalah',
                            xtype: 'fieldset',
                            autoHeight: true,
                            anchor:'95%',
                            items: [
                                {
                                    id : 'siswappdb_saudara_nis',
                                    xtype:'textfield',
                                    style: "background-color: #ffff00; background-image:none; ",
                                    fieldLabel: 'N.I.S',
                                    name: 'saudara_nis',
                                    anchor:'50%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {
                                    id : 'siswappdb_saudara_kelas',
                                    xtype:'textfield',
                                    style: "background-color: #ffff00; background-image:none; ",
                                    fieldLabel: 'Kelas',
                                    name: 'saudara_kelas',
                                    anchor:'50%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {
                                    id : 'siswappdb_saudara_nama',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Nama Siswa',
                                    name: 'saudara_nama',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                }
                            ]
                        },
                    ]}
                ]
                },
                ]
            });
            this.Form_parent = new Ext.FormPanel(
            {   bodyStyle : 'padding:5px',
                iconCls   : 'silk-group',
                title     : 'ORANG TUA',
                items     : [
                // IDENTITAS AYAH & IBU
                {   layout : 'column',
                    border : false,
                    items  : [
                    {   columnWidth :.5,
                        layout      : 'form',
                        border      : false,
                        items       : [
                        {
                            xtype      : 'fieldset',
                            title      : 'Identitas Ayah ',
                            autoHeight : true,
                            anchor     : '95%',
                            items      : [
                            {   id         : 'siswappdb_nama_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Nama.Ayah',
                                name       : 'nama_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_alamat_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Alamat.Ayah',
                                name       : 'alamat_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_tempat_lahir_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Tempat Lahir',
                                name       : 'tempat_lahir_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_tanggal_lahir_ayah',
                                xtype      : 'datefield',
                                fieldLabel : 'Tanggal.Lahir',
                                name       : 'tanggal_lahir_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_telp_rumah_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Telp.Rumah',
                                name       : 'telp_rumah_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_telp_hp_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Telp.Hp',
                                name       : 'telp_hp_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id             : 'siswappdb_hubungan_ayah',
                                xtype          : 'combo',
                                fieldLabel     : 'Hubungan Siswa',
                                labelSeparator : '',
                                name           : 'hubungan_ayah',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['Ayah Kandung'], ['Ayah Tiri'], ['Ayah Angkat / Asuh']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah',
                                readOnly       : true,
                            },
                            {   id             : 'siswappdb_agama_ayah',
                                xtype          : 'combo',
                                fieldLabel     : 'Agama',
                                labelSeparator : '',
                                name           : 'agama_ayah',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['Islam'], ['Kristen'],['Katolik'], ['Hindu'], ['Budha'], ['KongHucu']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah',
                                readOnly       : true,
                            },
                            {   id             : 'siswappdb_pendidikan_ayah',
                                xtype          : 'combo',
                                fieldLabel     : 'Pendidikan',
                                labelSeparator : '',
                                name           : 'pendidikan_ayah',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['SD'], ['SMP'], ['SMA'], ['S1'], ['S2'], ['S3']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah',
                                readOnly       : true,
                            },
                            {   id         : 'siswappdb_pekerjaan_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Pekerjaan',
                                name       : 'pekerjaan_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id             : 'siswappdb_jenis_pekerjaan_ayah',
                                xtype          : 'combo',
                                fieldLabel     : 'Jenis Pekerjaan',
                                labelSeparator : '',
                                name           : 'jenis_pekerjaan_ayah',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['PNS'], ['BUMN'], ['TNI/POLRI'], ['Pegawai Swasta'], ['Pensiunan'], ['Wiraswasta']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah',
                                readOnly       : true,
                            },
                            {   id         : 'siswappdb_jabatan_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Jabatan',
                                name       : 'jabatan_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_institusi_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Institusi',
                                name       : 'institusi_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_institusi_alamat_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Alamat Institusi',
                                name       : 'institusi_alamat_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_institusi_telp_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Telp Institusi',
                                name       : 'institusi_telp_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id             : 'siswappdb_penghasilan_ayah',
                                xtype          : 'combo',
                                fieldLabel     : 'Penghasilan',
                                labelSeparator : '',
                                name           : 'penghasilan_ayah',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [
                                        ['Rp.5.000.000 - Rp. 10.000.000'],
                                        ['Rp.10.000.000 - Rp. 15.000.000'],
                                        ['Rp.15.000.000 - Rp. 25.000.000'],
                                        ['Rp.25.000.000 - Rp. 50.000.000'],
                                        ['Rp.50.000.000 - ke atas']
                                    ]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah',
                                readOnly       : true,
                            },
                            {   id         : 'siswappdb_tanggungan_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Jumlah Tanggungan',
                                name       : 'tanggungan_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },

                            ]
                        },
                        ]
                    },
                    {   columnWidth :.5,
                        layout      : 'form',
                        border      : false,
                        items       : [
                        {
                            xtype      : 'fieldset',
                            title      : 'Identitas Ibu ',
                            autoHeight : true,
                            anchor     : '95%',
                            items      : [
                            {   id         : 'siswappdb_nama_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Nama.Ibu',
                                name       : 'nama_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_alamat_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Alamat.Ibu',
                                name       : 'alamat_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_tempat_lahir_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Tempat Lahir',
                                name       : 'tempat_lahir_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_tanggal_lahir_ibu',
                                xtype      : 'datefield',
                                fieldLabel : 'Tanggal.Lahir',
                                name       : 'tanggal_lahir_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_telp_rumah_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Telp.Rumah',
                                name       : 'telp_rumah_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_telp_hp_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Telp.Hp',
                                name       : 'telp_hp_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id             : 'siswappdb_hubungan_ibu',
                                xtype          : 'combo',
                                fieldLabel     : 'Hubungan Siswa',
                                labelSeparator : '',
                                name           : 'hubungan_ibu',
                                anchor         : '95%',
                                allowBlank     : true,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['Ibu Kandung'], ['Ibu Tiri'], ['Ibu Angkat / Asuh']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah',
                                readOnly       : true,
                            },
                            {   id             : 'siswappdb_agama_ibu',
                                xtype          : 'combo',
                                fieldLabel     : 'Agama',
                                labelSeparator : '',
                                name           : 'agama_ibu',
                                anchor         : '95%',
                                allowBlank     : true,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['Islam'], ['Kristen'],['Katolik'], ['Hindu'], ['Budha'], ['KongHucu']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah',
                                readOnly       : true,
                            },
                            {   id             : 'siswappdb_pendidikan_ibu',
                                xtype          : 'combo',
                                fieldLabel     : 'Pendidikan',
                                labelSeparator : '',
                                name           : 'pendidikan_ibu',
                                anchor         : '95%',
                                allowBlank     : true,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['SD'], ['SMP'], ['SMA'], ['S1'], ['S2'], ['S3']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah',
                                readOnly       : true,
                            },
                            {   id         : 'siswappdb_pekerjaan_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Pekerjaan',
                                name       : 'pekerjaan_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id             : 'siswappdb_jenis_pekerjaan_ibu',
                                xtype          : 'combo',
                                fieldLabel     : 'Jenis Pekerjaan',
                                labelSeparator : '',
                                name           : 'jenis_pekerjaan_ibu',
                                anchor         : '95%',
                                allowBlank     : true,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['PNS'], ['BUMN'], ['TNI/POLRI'], ['Pegawai Swasta'], ['Pensiunan'], ['Wiraswasta']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah',
                                readOnly       : true,
                            },
                            {   id         : 'siswappdb_jabatan_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Jabatan',
                                name       : 'jabatan_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_institusi_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Institusi',
                                name       : 'institusi_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_institusi_alamat_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Alamat Institusi',
                                name       : 'institusi_alamat_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_institusi_telp_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Telp Institusi',
                                name       : 'institusi_telp_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id             : 'siswappdb_penghasilan_ibu',
                                xtype          : 'combo',
                                fieldLabel     : 'Penghasilan',
                                labelSeparator : '',
                                name           : 'penghasilan_ibu',
                                anchor         : '95%',
                                allowBlank     : true,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [
                                        ['Rp.5.000.000 - Rp. 10.000.000'],
                                        ['Rp.10.000.000 - Rp. 15.000.000'],
                                        ['Rp.15.000.000 - Rp. 25.000.000'],
                                        ['Rp.25.000.000 - Rp. 50.000.000'],
                                        ['Rp.50.000.000 - ke atas']
                                    ]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah',
                                readOnly       : true,
                            },
                            {   id         : 'siswappdb_pengeluaran_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Jumlah Pengeluaran',
                                name       : 'pengeluaran_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            ]
                        },
                        ]
                    }]
                },
                ]
            });
            this.Form_wali = new Ext.FormPanel(
            {   bodyStyle : 'padding:5px',
                iconCls   : 'silk-group-add',
                title     : 'WALI',
                items     : [
                // IDENTITAS WALI AYAH & IBU
                {   layout : 'column',
                    border : false,
                    items  : [
                    {   columnWidth :.5,
                        layout      : 'form',
                        border      : false,
                        items       : [
                        {
                            xtype      : 'fieldset',
                            title      : 'Wali.Ayah',
                            autoHeight : true,
                            anchor     : '95%',
                            items      : [
                            {   id         : 'siswappdb_nama_wali_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Nama.Wali.Ayah',
                                name       : 'nama_wali_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_alamat_wali_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Alamat.Wali.Ayah',
                                name       : 'alamat_wali_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_tempat_lahir_wali_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Tempat Lahir',
                                name       : 'tempat_lahir_wali_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_tanggal_lahir_wali_ayah',
                                xtype      : 'datefield',
                                fieldLabel : 'Tanggal.Lahir',
                                name       : 'tanggal_lahir_wali_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_telp_rumah_wali_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Telp.Rumah',
                                name       : 'telp_rumah_wali_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_telp_hp_wali_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Telp.Hp',
                                name       : 'telp_hp_wali_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id             : 'siswappdb_hubungan_wali_ayah',
                                xtype          : 'combo',
                                fieldLabel     : 'Hubungan Siswa',
                                labelSeparator : '',
                                name           : 'hubungan_wali_ayah',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['Wali.Ayah Kandung'], ['Wali.Ayah Tiri'], ['Wali.Ayah Angkat / Asuh']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {   id             : 'siswappdb_agama_wali_ayah',
                                xtype          : 'combo',
                                fieldLabel     : 'Agama',
                                labelSeparator : '',
                                name           : 'agama_wali_ayah',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['Islam'], ['Kristen'],['Katolik'], ['Hindu'], ['Budha'], ['KongHucu']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {   id             : 'siswappdb_pendidikan_wali_ayah',
                                xtype          : 'combo',
                                fieldLabel     : 'Pendidikan',
                                labelSeparator : '',
                                name           : 'pendidikan_wali_ayah',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['SD'], ['SMP'], ['SMA'], ['S1'], ['S2'], ['S3']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {   id         : 'siswappdb_pekerjaan_wali_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Pekerjaan',
                                name       : 'pekerjaan_wali_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id             : 'siswappdb_jenis_pekerjaan_wali_ayah',
                                xtype          : 'combo',
                                fieldLabel     : 'Jenis Pekerjaan',
                                labelSeparator : '',
                                name           : 'jenis_pekerjaan_wali_ayah',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['PNS'], ['BUMN'], ['TNI/POLRI'], ['Pegawai Swasta'], ['Pensiunan'], ['Wiraswasta']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {   id         : 'siswappdb_jabatan_wali_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Jabatan',
                                name       : 'jabatan_wali_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_institusi_wali_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Institusi',
                                name       : 'institusi_wali_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_institusi_alamat_wali_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Alamat Institusi',
                                name       : 'institusi_alamat_wali_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_institusi_telp_wali_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Telp Institusi',
                                name       : 'institusi_telp_wali_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id             : 'siswappdb_penghasilan_wali_ayah',
                                xtype          : 'combo',
                                fieldLabel     : 'Penghasilan',
                                labelSeparator : '',
                                name           : 'penghasilan_wali_ayah',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [
                                        ['Rp.5.000.000 - Rp. 10.000.000'],
                                        ['Rp.10.000.000 - Rp. 15.000.000'],
                                        ['Rp.15.000.000 - Rp. 25.000.000'],
                                        ['Rp.25.000.000 - Rp. 50.000.000'],
                                        ['Rp.50.000.000 - ke atas']
                                    ]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {   id         : 'siswappdb_tanggungan_wali_ayah',
                                xtype      : 'textfield',
                                fieldLabel : 'Jumlah Tanggungan',
                                name       : 'tanggungan_wali_ayah',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            ]
                        },
                        ]
                    },
                    {   columnWidth:.5,
                        layout: 'form',
                        hidden : true,
                        border: false,
                        items: [
                        {
                            xtype      : 'fieldset',
                            title      : 'Identitas Wali.Ibu',
                            autoHeight : true,
                            anchor     : '95%',
                            items      : [
                            {   id         : 'siswappdb_nama_wali_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Nama.Wali.Ibu',
                                name       : 'nama_wali_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_alamat_wali_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Alamat.Wali.Ibu',
                                name       : 'alamat_wali_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_tempat_lahir_wali_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Tempat Lahir',
                                name       : 'tempat_lahir_wali_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_tanggal_lahir_wali_ibu',
                                xtype      : 'datefield',
                                fieldLabel : 'Tanggal.Lahir',
                                name       : 'tanggal_lahir_wali_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_telp_rumah_wali_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Telp.Rumah',
                                name       : 'telp_rumah_wali_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_telp_hp_wali_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Telp.Hp',
                                name       : 'telp_hp_wali_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id             : 'siswappdb_hubungan_wali_ibu',
                                xtype          : 'combo',
                                fieldLabel     : 'Hubungan Siswa',
                                labelSeparator : '',
                                name           : 'hubungan_wali_ibu',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['Wali.Ibu Kandung'], ['Wali.Ibu Tiri'], ['Wali.Ibu Angkat / Asuh']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {   id             : 'siswappdb_agama_wali_ibu',
                                xtype          : 'combo',
                                fieldLabel     : 'Agama',
                                labelSeparator : '',
                                name           : 'agama_wali_ibu',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['Islam'], ['Kristen'],['Katolik'], ['Hindu'], ['Budha'], ['KongHucu']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {   id             : 'siswappdb_pendidikan_wali_ibu',
                                xtype          : 'combo',
                                fieldLabel     : 'Pendidikan',
                                labelSeparator : '',
                                name           : 'pendidikan_wali_ibu',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['SD'], ['SMP'], ['SMA'], ['S1'], ['S2'], ['S3']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {   id         : 'siswappdb_pekerjaan_wali_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Pekerjaan',
                                name       : 'pekerjaan_wali_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id             : 'siswappdb_jenis_pekerjaan_wali_ibu',
                                xtype          : 'combo',
                                fieldLabel     : 'Jenis Pekerjaan',
                                labelSeparator : '',
                                name           : 'jenis_pekerjaan_wali_ibu',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['PNS'], ['BUMN'], ['TNI/POLRI'], ['Pegawai Swasta'], ['Pensiunan'], ['Wiraswasta']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {   id         : 'siswappdb_jabatan_wali_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Jabatan',
                                name       : 'jabatan_wali_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_institusi_wali_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Institusi',
                                name       : 'institusi_wali_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_institusi_alamat_wali_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Alamat Institusi',
                                name       : 'institusi_alamat_wali_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id         : 'siswappdb_institusi_telp_wali_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Telp Institusi',
                                name       : 'institusi_telp_wali_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            {   id             : 'siswappdb_penghasilan_wali_ibu',
                                xtype          : 'combo',
                                fieldLabel     : 'Penghasilan',
                                labelSeparator : '',
                                name           : 'penghasilan_wali_ibu',
                                anchor         : '95%',
                                allowBlank     : true,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [
                                        ['Rp.5.000.000 - Rp. 10.000.000'],
                                        ['Rp.10.000.000 - Rp. 15.000.000'],
                                        ['Rp.15.000.000 - Rp. 25.000.000'],
                                        ['Rp.25.000.000 - Rp. 50.000.000'],
                                        ['Rp.50.000.000 - ke atas']
                                    ]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {   id         : 'siswappdb_pengeluaran_wali_ibu',
                                xtype      : 'textfield',
                                fieldLabel : 'Jumlah Pengeluaran',
                                name       : 'pengeluaran_wali_ibu',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : ''
                            },
                            ]
                        },
                        ]
                    }]
                },
                ]
            });
            this.Form_Expense = new Ext.FormPanel(
            {   bodyStyle : 'padding:5px',
                iconCls   : 'silk-money',
                title     : 'PENGELUARAN',
                items     : [
                {   layout : 'column',
                    border : false,
                    items  : [
                    {   columnWidth :.5,
                        layout      : 'form',
                        border      : false,
                        items       : [
                        {
                            xtype      : 'fieldset',
                            title      : 'Pengeluaran Rutin ',
                            autoHeight : true,
                            anchor     : '95%',
                            items      : [
                            {   id         : 'siswappdb_expense_listrik',
                                xtype      : 'textfield',
                                fieldLabel : 'Listrik per bulan (Rp)',
                                name       : 'expense_listrik',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : '0'
                            },
                            {   id         : 'siswappdb_expense_pdam',
                                xtype      : 'textfield',
                                fieldLabel : 'PDAM per bulan (Rp)',
                                name       : 'expense_pdam',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : '0'
                            },
                            {   id         : 'siswappdb_expense_pulsa',
                                xtype      : 'textfield',
                                fieldLabel : 'Pulsa HP per bulan (Rp)',
                                name       : 'expense_pulsa',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : '0'
                            },
                            {   id         : 'siswappdb_expense_monthly',
                                xtype      : 'textfield',
                                fieldLabel : 'Pengeluaran Rata-rata Per bulan (Rp)',
                                name       : 'expense_monthly',
                                anchor     : '95%',
                                allowBlank : true,
                                readOnly   : true,
                                value      : '0'
                            },
                            ]
                        }
                        ]
                    },
                    ]
                },
                ]
            });
            this.Form_payment = new Ext.FormPanel(
            {   bodyStyle : 'padding:5px',
                iconCls   : 'silk-money',
                title     : 'PEMBAYARAN',
                tbar      : [ ' -',
                    {
                        text    : 'Save Registration',
                        tooltip : 'Save Record',
                        iconCls : 'icon-save',
                        handler : this.Form_save,
                        scope   : this
                    }
                ],
                items: [
                {   layout : 'column',
                    border : false,
                    items  : [
                    {   columnWidth :.3,
                        layout      : 'form',
                        border      : false,
                        items       : [
                        {   title      : 'Observasi & Kommitment',
                            xtype      : 'fieldset',
                            autoHeight : true,
                            anchor     : '95%',
                            // defaultType: 'checkbox', // each item will be a checkbox
                            items      : [
                            {   id         : 'siswappdb_observation_date',
                                xtype      : 'datefield',
                                fieldLabel : 'Observation.Date',
                                name       : 'observation_date',
                                anchor     : '95%',
                                allowBlank : false,
                                format     : 'd/m/Y',
                            },
                            {   id         : 'siswappdb_commitment_date',
                                xtype      : 'datefield',
                                fieldLabel : 'Commitment.Date',
                                name       : 'commitment_date',
                                anchor     : '95%',
                                allowBlank : false,
                                format     : 'd/m/Y',
                            },
                            ]
                        }]
                    },
                    {   columnWidth :.5,
                        layout      : 'form',
                        border      : false,
                        items       : [
                        {   title      : 'Pembayaran Formulir',
                            xtype      : 'fieldset',
                            autoHeight : true,
                            anchor     : '95%',
                            // defaultType: 'checkbox', // each item will be a checkbox
                            items      : [
                            {   id         : 'siswappdb_payment_date',
                                xtype      : 'datefield',
                                fieldLabel : 'Payment.Date',
                                name       : 'payment_date',
                                anchor     : '95%',
                                allowBlank : false,
                                format     : 'd/m/Y',
                            },
                            {   id         : 'siswappdb_payment_amount',
                                xtype      : 'textfield',
                                fieldLabel : 'Amount',
                                name       : 'payment_amount',
                                anchor     : '95%',
                                allowBlank : false,
                            },
                            {   id             : 'siswappdb_jenis_bayar',
                                xtype          : 'combo',
                                fieldLabel     : 'Jenis Bayar',
                                labelSeparator : '',
                                name           : 'jenis_bayar',
                                anchor         : '95%',
                                allowBlank     : false,
                                readOnly       : false,
                                store          : new Ext.data.SimpleStore(
                                {   fields: ['label'],
                                    data : [ ['Tunai'], ['Non Tunai']]
                                }),
                                displayField   : 'label',
                                valueField     : 'label',
                                mode           : 'local',
                                triggerAction  : 'all',
                                selectOnFocus  : true,
                                editable       : false,
                                width          : 100,
                                value          : 'Pilihlah'
                            },
                            {   id         : 'siswappdb_payment_by',
                                xtype      : 'textfield',
                                fieldLabel : 'Payment.By',
                                name       : 'payment_by',
                                anchor     : '95%',
                                allowBlank : false,
                            },
                            {   id         : 'siswappdb_payment_confirm_date',
                                xtype      : 'textfield',
                                fieldLabel : 'Confirm.Date',
                                name       : 'payment_confirm_date',
                                anchor     : '95%',
                                readOnly   : true,
                                value      : new Date()
                            },
                            {   id         : 'siswappdb_payment_confirm_by_name',
                                xtype      : 'textfield',
                                fieldLabel : 'Confirm.By',
                                name       : 'payment_confirm_by_name',
                                anchor     : '95%',
                                readOnly   : true,
                            },
                            ]
                        }]
                    }]
                }]
            });
        },
        // build the layout
        build_layout: function()
        {   //if (Ext.isObject(this.Records))
            if (Ext.isEmpty(this.Records.data.no_registration) )
            {   the_title = "Registrasi Calon Siswa"; }
            else { the_title = "Edit Registrasi Calon Siswa"; };

            this.Tab = new Ext.Panel(
            {   id         : "siswappdb_form",
                jsId       : "siswappdb_form",
                title      : '<span style="background-color: yellow;">'+the_title+'</span>',
                region     : 'center',
                layout     : 'border',
                closable   : true,
                autoScroll : true,
                items      : [
                {
                    region    : 'center',
                    xtype     : 'tabpanel', // TabPanel itself has no title
                    activeTab : 0,
                    items     : [this.Form, this.Form_sekolah, this.Form_parent, this.Form_wali, this.Form_Expense, this.Form_payment]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   // overide TextField Validation to exclude empty string
            this.DataStore.on( 'load', function( store, records, options )
            {   //check the fields
                Ext.each(records[0].store.fields.items,
                    function(the_field)
                    {   console.log(the_field.name);
                        console.log(records[0].data[the_field.name]);
                        switch (the_field.name)
                        {   case "observation_result" :
                            case "commitment_result" :
                            case "created_by" :
                            case "modified_date" :
                            case "modified_by" :
                            {}; //exclude above fields
                            break;
                            case "tanggal_lahir" :
                            case "created_date" :
                            case "observation_date" :
                            case "commitment_date" :
                            case "payment_date" :
                            case "payment_confirm_date" :
                            {
                                Ext.getCmp('siswappdb_'+the_field.name).setValue(alfalah.core.shortdateRenderer(records[0].data[the_field.name],'Y-m-d', 'd/m/Y'));
                            };
                            break;
                            default :
                            {
                                Ext.getCmp('siswappdb_'+the_field.name).setValue(records[0].data[the_field.name]);
                            };
                            break;
                        };
                    });
            });

            this.ParentsDS.on( 'load', function( store, records, options )
            {   //check the fields
                Ext.each(records[0].store.fields.items,
                    function(the_field)
                    {
                        switch (the_field.name)
                        {   case "tanggal_lahir_ayah" :
                            case "tanggal_lahir_ibu" :
                            case "tanggal_lahir_wali_ayah" :
                            case "tanggal_lahir_wali_ibu" :
                            {
                                Ext.getCmp('siswappdb_'+the_field.name).setValue(alfalah.core.shortdateRenderer(records[0].data[the_field.name],'Y-m-d', 'd/m/Y'));
                            };
                            break;
                            default :
                            {
                                Ext.getCmp('siswappdb_'+the_field.name).setValue(records[0].data[the_field.name]);
                            };
                            break;
                        };
                    });
            });

            this.DataStore.load();
            this.ParentsDS.load();

            this.Records = Ext.data.Record.create(
            [   {name: 'siswappdb_no', type: 'string'},
                {name: 'created_date', type: 'date'}
            ]);
        },
        // forms grid save records
        Form_save : function(button, event)
        {   var head_data = [];
            var json_data = [];
            var modi_data = [];
            var v_json = {};
            // header validation
            if (alfalah.core.validateFields([
                'siswappdb_id', 'siswappdb_no_pendaftaran', 'siswappdb_tahunajaran_id',
                'siswappdb_jenjang_sekolah_id', 'siswappdb_nama_lengkap', 'siswappdb_tanggal_lahir',
                'siswappdb_ibu_kandung', 'siswappdb_payment_date', 'siswappdb_payment_amount',
                'siswappdb_payment_by','siswappdb_jenis_bayar'
                ]))
            {   head_data = alfalah.core.getHeadData([
                    'siswappdb_id', 'siswappdb_no_pendaftaran', 'siswappdb_no_registration',
                    'siswappdb_tahunajaran_id', 'siswappdb_jenjang_sekolah_id',
                    'siswappdb_nama_lengkap', 'siswappdb_tanggal_lahir', 'siswappdb_ibu_kandung',
                    'siswappdb_payment_date', 'siswappdb_payment_amount', 'siswappdb_payment_by',
                    'siswappdb_payment_confirm_date', 'siswappdb_payment_confirm_by_name',
                    'siswappdb_observation_date', 'siswappdb_commitment_date',
                    'siswappdb_created_date','siswappdb_jenis_bayar'
                    ]);

                var the_form = Ext.getCmp('siswappdb_form');
                Ext.Ajax.request(
                {   method  : 'POST',
                    url     : "{{ url('/siswa/5/1') }}",
                    headers : {   'x-csrf-token': alfalah.calonsiswa.sid },
                    params: {
                        task : 'save',
                        head : Ext.encode(head_data),
                        json : "",
                    },
                    success: function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title : 'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                        else
                        {
                            alfalah.calonsiswa.forms.DataStore.reload();
                            Ext.Msg.show(
                            {   title   : 'Registrasi Calon Siswa ',
                                msg     : '<BR>Payment Confirmed  '+
                                          '<BR>==================='+
                                          '<BR>No Registration : <B>'+the_response.no_registration+'</B>'+
                                          '<BR>Nama Lengkap : <B>'+the_response.nama_lengkap+'</B>'+
                                          '<BR>No Pendaftaran : <B>'+the_response.no_pendaftaran+'</B>',
                                buttons : Ext.Msg.OK,
                                icon    : Ext.MessageBox.INFO,
                                width   : 300
                            });
                            the_form.destroy();
                            // the_url = "{{ url('/ppdb/0/2') }}"+'?'+'&id='+the_response.server_message;
                            // window.location = the_url;
                        };
                    },
                    failure: function()
                    {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    // scope: this
                });
            };
        },
    }; // end of public space
}(); // end of app
alfalah.calonsiswa.observation= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header    : "No.Pendaftaran", width : 100,
                    dataIndex : 'no_pendaftaran', sortable: true,
                    tooltip   : "Nomor Pendaftaran",
                    hidden : true,
                    renderer  : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.observation_result == "BERGABUNG")
                        {   metaData.attr = "style = background-color:lime;"; }
                        else if ( record.data.observation_result == "TIDAK BERGABUNG")
                        {   metaData.attr = "style = background-color:red;"; }
                        else { metaData.attr = "style = background-color:yellow;"; };
                        return value;
                    },
                },
                {   header: "No.Registration", width : 100,
                    dataIndex : 'no_registration', sortable: true,
                    tooltip: "Nomor Registration",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.observation_date)
                        {   metaData.attr = "style = background-color:orange;"; }
                        else { metaData.attr = "style = background-color:red;"; };
                        return value;
                    },
                },
                {   header    : "Jenjang", width : 100,
                    dataIndex : 'jenjang_sekolah_name', sortable: true,
                    tooltip   : "Jenjang Sekolah",
                },
                {   header    : "Nama Lengkap", width : 200,
                    dataIndex : 'nama_lengkap', sortable: true,
                    tooltip   : "Nama Lengkap",
                },
                {   header      : "Jadwal Observasi", width : 100,
                    dataIndex   : 'observation_date', sortable: true,
                    tooltip     : "Jadwal Observasi",
                    search_null : true,
                    editor      : new Ext.form.DateField({
                        allowBlank : false,
                    }),
                },
                {   header      : "Hasil Observasi", width : 150,
                    dataIndex   : 'observation_result', sortable: true,
                    tooltip     : "Observation Result",
                    search_null : true,
                    editor      :  new Ext.Editor(
                                    new Ext.form.ComboBox(
                                    {   store: new Ext.data.SimpleStore({
                                                    fields: ["label"],
                                                    data : [["BERGABUNG"], ["TIDAK BERGABUNG"]]
                                                }),
                                        displayField:"label",
                                        valueField:"label",
                                        mode: 'local',
                                        typeAhead: true,
                                        triggerAction: "all",
                                        selectOnFocus:true,
                                        forceSelection :true
                                    }),
                                    {autoSize:true}
                    )
                },
                {   header      : "Jadwal Komitment", width : 100,
                    dataIndex   : 'commitment_date', sortable: true,
                    tooltip     : "Jadwal Commitment",
                    search_null : true,
                    editor      : new Ext.form.DateField({
                        allowBlank : false,
                    }),
                },
            ];

            this.Records = Ext.data.Record.create(
            [   {name: 'observation_id', type: 'integer'},
                {name: 'modified_date', type: 'date'},
            ]);

            this.DepartmentDS = alfalah.core.newDataStore(
                "{{ url('/company/5/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start,
                    type:"Division" }
            );
            this.DepartmentDS.load();

            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/siswa/5/0') }}", false,
                {   s:"form", t:"registration", limit:this.page_limit, start:this.page_start }
            );

            this.PrintObsStore = alfalah.core.newDataStore(
                "{{ url('/siswa/5/100') }}", false,
                {   s:"form", t:"registration", limit:this.page_limit, start:this.page_start }
            );

            this.Searchs = [
                {   id             : 'observation_observation_date',
                    cid            : 'observation_date',
                    fieldLabel     : 'Observation',
                    labelSeparator : ' ',
                    xtype          : 'datefield',
                    width          : 80,
                    format         : 'd/m/Y',
                },
                {   id             : 'observation_commitment_date',
                    cid            : 'commitment_date',
                    fieldLabel     : 'Commitment',
                    labelSeparator : ' ',
                    xtype          : 'datefield',
                    width          : 80,
                    format         : 'd/m/Y',
                },
                {   id             : 'observation_nopen',
                    cid            : 'nopen',
                    fieldLabel     : 'No.Pen',
                    labelSeparator : '',
                    xtype          : 'textfield',
                    width          : 120
                },
                {   id             : 'observation_noreg',
                    cid            : 'noreg',
                    fieldLabel     : 'No.Reg',
                    labelSeparator : '',
                    xtype          : 'textfield',
                    width          : 120
                },
                {   id             : 'observation_nama_lengkap',
                    cid            : 'nama_lengkap',
                    fieldLabel     : 'Nama',
                    labelSeparator : '',
                    xtype          : 'textfield',
                    width          : 120
                },
                {   id             : 'observation_gender',
                    cid            : 'gender',
                    fieldLabel     : 'Gender',
                    labelSeparator : '',
                    xtype          : 'combo',
                    store          : new Ext.data.SimpleStore(
                    {   fields: ['label'],
                        data : [ ['ALL'], ['Laki-Laki'], ['Perempuan']]
                    }),
                    displayField   : 'label',
                    valueField     : 'label',
                    mode           : 'local',
                    triggerAction  : 'all',
                    selectOnFocus  : true,
                    editable       : false,
                    width          : 100,
                    value          : 'ALL'
                },
                {   id             : 'observation_jenjang_sekolah_id',
                    cid            : 'jenjang_sekolah_id',
                    xtype          : 'combo',
                    fieldLabel     : 'Jenjang',
                    labelSeparator : '',
                    name           : 'jenjang_sekolah',
                    anchor         : '95%',
                    allowBlank     : false,
                    readOnly       : false,
                    store          : this.DepartmentDS,
                    displayField   : 'name',
                    valueField     : 'dept_id',
                    mode           : 'local',
                    forceSelection : true,
                    triggerAction  : 'all',
                    selectOnFocus  : true,
                    editable       : false,
                    typeAhead      : true,
                    width          : 100,
                    value          : 'ALL'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id         : tabId+"_observationSearchBtn",
                fieldLabel : '',
                text       : 'Search',
                tooltip    : 'Search',
                iconCls    : 'silk-zoom',
                xtype      : 'button',
                width      : 120,
                handler    : this.observation_search_handler,
                scope      : this
            });
            //prepare null parameter items
            this.Searchs_null = alfalah.core.newSearchNull(this.Columns, 'calonsiswa_N');

            this.Grid = new Ext.grid.EditorGridPanel(
            {   store         :  this.DataStore,
                columns       : this.Columns,
                enableColLock : false,
                loadMask      : true,
                height        : alfalah.calonsiswa.centerPanel.container.dom.clientHeight-50,
                anchor        : '100%',
                autoScroll    : true,
                frame         : true,
                tbar          : [
                    @if (array_key_exists('SAVE', $MyTasks))
                    {
                        text    : 'Save',
                        tooltip : 'Save Schedule',
                        iconCls : 'icon-save',
                        handler : this.Grid_save,
                        scope   : this
                    }, '-',
                    '-',
                    @endif
                    @if (array_key_exists('PRINT_XLS', $MyTasks))
                    {   print_type : "xls",
                        text       : 'Print Excel',
                        tooltip    : 'Print to Excell SpreadSheet',
                        iconCls    : 'silk-page-white-excel',
                        // handler    : this.Grid_xls,
                         handler    : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); }, 
                        scope      : this
                    },'-',
                    @endif
                ],
                bbar: new Ext.PagingToolbar(
                {   id          : tabId+'_observationGridBBar',
                    store       : this.DataStore,
                    pageSize    : this.page_limit,
                    displayInfo : true,
                    emptyMsg    : 'No data found',
                    items       : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id            : tabId+'_observationPageCombo',
                            store         : new Ext.data.SimpleStore(
                            {   fields : ['value'],
                                data   : [[50],[75],[100],[125],[150]]
                            }),
                            displayField  : 'value',
                            valueField    : 'value',
                            value         : 75,
                            editable      : false,
                            mode          : 'local',
                            triggerAction : 'all',
                            selectOnFocus : true,
                            hiddenName    : 'pagesize',
                            width         : 50,
                            listeners     : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_observationPageCombo').getValue();
                                    bbar            = Ext.getCmp(tabId+'_observationGridBBar');
                                    bbar.pageSize   = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_observationSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id     : tabId+"_observationTab",
                jsId   : tabId+"_observationTab",
                title  : "Observation & Commitment",
                region : 'center',
                layout : 'border',
                items  : [
                {   title       : 'Parameters',
                    region      : 'east',     // position for region
                    split       : true,
                    width       : 200,
                    minSize     : 200,
                    maxSize     : 400,
                    collapsible : true,
                    layout      : 'fit',
                    items       : new Ext.TabPanel(
                        {   border      : false,
                            activeTab   : 0,
                            tabPosition : 'bottom',
                            items       : [
                            new Ext.FormPanel(
                            {   title       : 'S E A R C H',
                                labelWidth  : 70,
                                defaultType : 'textfield',
                                items       : this.Searchs,
                                frame       : true,
                                autoScroll  : true,
                                tbar        : [
                                {   text    : 'Search',
                                    tooltip : 'Search',
                                    iconCls : 'silk-zoom',
                                    handler : this.observation_search_handler,
                                    scope   : this,
                                }]
                            }),
                            new Ext.FormPanel(
                            {   title       : 'N U L L',
                                labelWidth  : 50,
                                defaultType : 'textfield',
                                items       : this.Searchs_null,
                                frame       : true,
                                autoScroll  : true,
                            }),
                            ]
                        })
                },
                {
                    region : 'center',     // center region is required, no width/height specified
                    xtype  : 'container',
                    layout : 'fit',
                    items  : [this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        @if (array_key_exists('SAVE', $MyTasks))
        Grid_edit : function(button, event)
        {
            // var the_record = this.Grid.getSelectionModel().selection;
            // if ( the_record )
            // {   var form_order = Ext.getCmp("form_order");
            //     if (form_order)
            //     {   Ext.Msg.show(
            //         {   title   : 'E R R O R ',
            //             msg     : 'Order Form Available',
            //             buttons : Ext.Msg.OK,
            //             icon    : Ext.MessageBox.ERROR
            //         });
            //     }
            //     else
            //     {   var centerPanel = Ext.getCmp('center_panel');
            //         alfalah.calonsiswa.forms.initialize(the_record.record);
            //         centerPanel.beginUpdate();
            //         centerPanel.add(alfalah.calonsiswa.forms.Tab);
            //         centerPanel.setActiveTab(alfalah.calonsiswa.forms.Tab);
            //         centerPanel.endUpdate();
            //         alfalah.core.viewport.doLayout();
            //     };
            // }
            // else
            // {   Ext.Msg.show(
            //         {   title   : 'I N F O ',
            //             msg     : 'No Data Selected ! ',
            //             buttons : Ext.Msg.OK,
            //             icon    : Ext.MessageBox.INFO
            //         });
            // };
        },
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                console.log('HAHAHAHAHAHAHAHAHHAAHHA');
                alfalah.core.submitGrid(
                    this.DataStore,
                    "{{ url('/siswa/5/7') }}",
                    {   'x-csrf-token': alfalah.calonsiswa.sid },
                    {   json: Ext.encode(json_data) }
                );
            };
        },







        @endif
        // calonsiswa search button
        observation_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title   : 'W A R N I N G ',
                        msg     : ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons : Ext.Msg.YESNO,
                        fn      : function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_observationSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter             = alfalah.core.getSearchParameter(this.Searchs);
                the_parameter_null        = alfalah.core.getSearchParameter(this.Searchs_null);
                this.DataStore.baseParams = Ext.apply( the_parameter, the_parameter_null,
                    {   s:"form", t:"registration",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.removeAll();
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.calonsiswa.initialize, alfalah.calonsiswa);

</script>
