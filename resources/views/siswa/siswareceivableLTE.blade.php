<!-- Using default Layout -->
@extends('layouts_backend._iframe_backend')
<!-- load your extry css styles -->
@section('extra_styles')
<link rel="stylesheet" href="../../w2ui/w2ui-1.5.rc1.min.css" />
<link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
<style>
.w2ui-field input {
    width: 20px;
    text-align: left;

}
.w2ui-field label {
    padding: 5px;
}
.w2ui-field > div {
    margin-left: 1px;
}
</style>
@endsection
<!-- load your main content page -->
@section('content')
<!-- Main content -->
<div id="siswareceivable_layout" style="width: 100%; height: 100%;" 
>
  <div id="siswareceivable_grid"></div>
</div>
<!-- /.content -->
@endsection
<!-- load your extra js scripts -->
@section('extra_scripts')
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- <script src="../../bower_components/fastclick/lib/fastclick.js"></script> -->
<!-- AdminLTE App -->
<!-- <script src="../../dist/js/adminlte.min.js"></script> -->
<!-- AdminLTE for demo purposes --><!-- 
<script src="dist/js/demo.js"></script> -->
<!--AdminLTE Iframe-->
<!-- <script src="dist/js/app_iframe.js"></script> -->
<script src="../../w2ui/w2ui-1.5.rc1.min.js"></script>
<script type="text/javascript">
    // Define namespace
    alfalah = parent.alfalah;
    alfalah.namespace('alfalah.siswareceivable');
    // create application
    alfalah.siswareceivable = function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        // this.tabId = '{{ $TABID }}';
        this.config;
        // private functions
        // public space
        return {
            centerPanel : 0,
            the_records : [],
            sid : '{{ csrf_token() }}',
            task : ' ',
            act : ' ',
            // public methods
            initialize: function()
            {   console.log('initialize');
                this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {   console.log('prepare_component');
                var pstyle = 'background-color: #F5F6F7; border: 1px solid #dfdfdf; padding: 1px;';

                this.config = {
                    siswareceivable_grid: 
                    {   name    : 'siswareceivable_grid',
                        // url     : '/siswa/3/0',
                        ref_url : '/siswa/3/0',
                        method  : 'GET',
                        // recid   : 'record_id',
                        recid : 'keyid',
                        // postData : {
                        //     s : 'init',
                        //     '_token' : alfalah.siswareceivable.sid,
                        //     'x-csrf-token': alfalah.siswareceivable.sid
                        // },
                        // selectType: 'cell',
                        show: { 
                            // toolbar: true,
                            // toolbarSave: true,
                            lineNumbers : true,
                            footer: true,
                            
                        },
                        columns: [
                            {   field: 'nama_jenjang_siswa', caption: 'Grade', size: '50px', 
                                sortable: true, resizable: true, searchable: true, 
                                editable: { type: 'text' } },
                            {   field: 'nama_siswa', caption: 'Siswa', size: '200px', 
                                sortable: true, resizable: true, searchable: true, 
                                editable: { type: 'text' } },
                            {   field: 'bultah', caption: 'Tahun/ Bulan', size: '80px', 
                                sortable: true, resizable: true, searchable: true, 
                                editable: { type: 'text' } },
                            {   field: 'nama_biaya_sekolah', caption: 'Biaya', size: '50px', 
                                sortable: true, searchable: true, 
                                editable: { type: 'text' } },
                            {   field: 'bulan', caption: 'Bulan', size: '50px', sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                            {   field: 'piutang', caption: 'Piutang', size: '100px', 
                                sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                            {   field: 'potongan', caption: 'Potongan', 
                                size: '100px', sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                            {   field: 'pembayaran', caption: 'Pembayaran', 
                                size: '100px', sortable: true,resizable: true, 
                                editable: { type: 'text' } },
                        ],
                        // searches: [
                        //     { type: 'int', field: 'recid', caption: 'ID' },
                        //     { type: 'text', field: 'desc', caption: 'Description' }
                        // ],
                        newRecord : function()
                        {   console.log('add new record');
                            console.log(this);
                            this.add({
                                siswa_id    : "",
                                name        : "New siswa",
                                recid       : "",
                                status      : "1",  // Inactive siswa
                                symbol      : "",
                                created_date: ""
                            }, true); // as fist record
                        },
                        onLoad: function(event)
                        {   console.log("siswareceivable_grid onLoad");
                            console.log(event);
                        }
                    },
                    siswareceivable_layout: 
                    {   name: 'siswareceivable_layout',
                        panels: [
                            // { type: 'top',  size: 50, resizable: true, style: pstyle, content: 'top' },
                            // { type: 'left', size: 200, resizable: true, style: pstyle, content: 'left' },
                            {   type: 'main', style: pstyle, 
                                toolbar: 
                                {   
                                    items: [
                                    // {   type: 'button', text: 'Add', icon: 'fa fa-plus',
                                    //     onClick: function(event)
                                    //     { } 
                                    // },
                                    // {   type: 'button', text: 'Save', icon: 'fa fa-save',
                                    //     onClick: function(event)
                                    //     { alfalah.core.submitGrid(
                                    //         w2ui.siswareceivable_grid, 
                                    //         '/siswa/2/1',
                                    //         alfalah.countries.sid
                                    //       );
                                    //     }
                                    // },
                                    // {   type: 'break'},
                                    // {   type: 'button', text: 'Delete', 
                                    //     icon: 'fa fa-remove',
                                    //     onClick: function(event)
                                    //     { alfalah.core.ajax(
                                    //         '/siswa/2/2',                          //the_url, 
                                    //         {                                 //the_parameters,
                                    //           '_token' : alfalah.countries.sid,
                                    //           json : JSON.stringify([
                                    //             { id : w2ui.siswareceivable_grid.getSelection()[0] 
                                    //             }])
                                    //         },                  
                                    //         "POST",                           //the_type, 
                                    //         function(response)                //fn_success
                                    //         { w2ui.siswareceivable_grid.reload(); 
                                    //         },
                                    //         function(response)                //fn_fail, 
                                    //         { console.log("FAILED");
                                    //           console.log(response);
                                    //         },
                                    //         null                              //fn_always
                                    //       );
                                    //     } 
                                    // },
                                    {   type: 'break'},
                                    // {   type: 'button', text: 'PDF',
                                    //     print_type : 'PDF', 
                                    //     icon: 'fa fa-file-pdf-o',
                                    //     onClick: function (event) 
                                    //     {   alfalah.siswareceivable.print_out(event); } 
                                    // },
                                    {   type: 'button', text: 'XLS',
                                        print_type : 'XLS',  
                                        icon: 'fa fa-file-excel-o',
                                        onClick: function (event) 
                                        {   alfalah.siswareceivable.print_out(event); }  
                                    }],
                                },
                            },
                            // { type: 'preview', size: '50%', resizable: true, style: pstyle, content: 'preview' 
                            // },
                            {   type: 'right', size: 200, resizable: true, style: pstyle, 
                                toolbar: 
                                {   items: [
                                    {   type: 'button', text: 'Search', 
                                        icon: 'fa fa-search',
                                        onClick: function(event )
                                        {   console.log("search click");
                                            alfalah.siswareceivable.search_handler(event); 
                                        }
                                    }],
                                    // onClick: function (event) {
                                    //   console.log("search clisck");
                                    //   console.log(event);
                                    // }
                                }
                            },
                            // { type: 'bottom', size: 50, resizable: true, style: pstyle, content: 'bottom' 
                            // }
                        ]
                    },
                    siswareceivable_form:
                    {   name  : 'siswareceivable_form',
                        url   : 'doremi/post',
                        fields: [
                            {   field: 'no_pendaftaran',  
                                type: 'text', 
                                // html: { caption : 'No.Pen', 
                                //         attr    : 'style="width: 100px"' 
                                //     }
                            },
                            {   field: 'nis',  type: 'text'},
                            {   field: 'nama_lengkap',  type: 'text' },
                        ],
                        style: 'border: 0px; background-color: transparent;',
                        formHTML: 
                            '<div id="siswareceivable_form" class="w2ui-page page-0">'+
                            '    <div class="w2ui-field">'+
                            '        <label style="text-align: left;">Tahun/Bulan</label>'+
                            '        <div>'+
                            '            <input name="bultah" type="text" size="24" maxlength="20"/>'+
                            '        </div>'+
                            '    </div>'+
                            '    <div class="w2ui-field">'+
                            '        <label style="text-align: left;">Biaya</label>'+
                            '        <div>'+
                            '            <input name="nama_biaya_sekolah" type="text" size="24" maxlength="12"/>'+
                            '        </div>'+
                            '    </div>'+
                            '    <div class="w2ui-field">'+
                            '        <label style="text-align: left;">Nama</label>'+
                            '        <div>'+
                            '            <input name="nama_siswa" type="text" size="24" maxlength="20" />'+
                            '        </div>'+
                            '    </div>'+
                            '</div>',
                        // actions: {
                        //     reset: function () {   this.clear();   },
                        //     save: function () { this.save();    }
                        // }
                    },
                };
            },
            // build the layout
            build_layout: function()
            { 
                // console.log('build_layout 1');
                $('#siswareceivable_layout').w2layout(this.config.siswareceivable_layout);
                // console.log('build_layout 2');
                $('#siswareceivable_grid').w2grid(this.config.siswareceivable_grid);
                // console.log('build_layout 3');
                w2ui.siswareceivable_layout.content('main', w2ui.siswareceivable_grid);
                // console.log('build_layout 4');
                
                $('#w2int').w2field('int', { autoFormat: false });

                $('#siswareceivable_form').w2form(this.config.siswareceivable_form);
                w2ui.siswareceivable_layout.content('right', w2ui.siswareceivable_form);
                // w2ui.siswareceivable_layout.content('right', $('#siswareceivable_form').w2form());
                console.log('build_layout 4');
            },
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            {   console.log('finalize_comp_and_layout'); },
            // finalize the component and layout drawing
            getSearchFormParameter: function(the_form)
            {   the_searchs = "";
                $(the_form).each(function()
                    {   the_searchs = $(this).find(':input');
                    });
                the_parameter = alfalah.core.getSearchParameter(the_searchs);
                the_parameter = $.extend( the_parameter, 
                                {   s:"form"    });
                return the_parameter;
            },
            search_handler: function(event)
            {   console.log('search_handlerssssssss');
                the_parameter = this.getSearchFormParameter("#siswareceivable_form");
                // w2utils.settings.dataType = 'HTTP';
                // w2ui.siswareceivable_grid.load(w2ui.siswareceivable_grid.ref_url);
                w2ui.siswareceivable_grid.load(w2ui.siswareceivable_grid.ref_url+ '?'+$.param(the_parameter));
                
                // w2ui.siswareceivable_grid.request(
                //     'GET', 
                //     the_parameter,
                //     // JSON.stringify(the_parameter), 
                //     w2ui.siswareceivable_grid.ref_url
                // );                                 
                // 
                // this.DataStore.baseParams = Ext.apply( the_parameter,
                //     {   task: alfalah.siswareceivable.task,
                //         act: alfalah.siswareceivable.act,
                //         a:2, b:0, s:"form",
                //         limit:this.page_limit, start:this.page_start });
                // this.DataStore.reload();
                // w2ui.siswareceivable_grid.postData = JSON.stringify(the_parameter);
                // w2ui.siswareceivable_grid.method = 'POST';
                // w2ui.siswareceivable_grid.postData = the_parameter;
                // w2ui.siswareceivable_grid.searchData = the_parameter;
                // w2ui.siswareceivable_grid.search();
                // w2utils.settings.RESTfull = true;
                // w2utils.settings.dataType = 'JSON';
            },
            print_out: function(event)
            {   alfalah.core.printOut(
                    event.item.print_type, 
                    w2ui.siswareceivable_grid.ref_url, 
                    this.getSearchFormParameter("#siswareceivable_form")
                );
            },
        }; // end of public space
    }(); // end of app
    $(document).ready(alfalah.siswareceivable.initialize());
</script>

@endsection
