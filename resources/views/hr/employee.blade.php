<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.employee');

// create application
alfalah.employee = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',
    
        // public methods
        initialize: function()
        {   console.log("tabId");
            console.log(tabId);
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            console.log("centerPanel");
            console.log(this.centerPanel);
            this.employee.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.employee.Tab);
            this.centerPanel.setActiveTab(this.employee.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.employee.employee= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {

            this.DepartmentDS = alfalah.core.newDataStore(
                    "{{ url('/ppdb/0/9') }}", false,
                    {   s:"form", limit:this.page_limit, start:this.page_start,
                        type:"Division"}
            );
            this.DepartmentDS.load();


            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'emp_id', sortable: true,
                    tooltip:"employee ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Name", width : 150,
                    dataIndex : 'name', sortable: true,
                    tooltip:"employee Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                // {   header: "Type", width : 50,
                //     dataIndex : 'emp_type', sortable: true,
                //     tooltip:"Employee Type",
                //     editor : new Ext.form.TextField({allowBlank: false}),
                // },
                {   header: "Address", width : 250,
                    dataIndex : 'address', sortable: true,
                    tooltip:"Employee Address",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Mobile", width : 100,
                    dataIndex : 'mobile_phone', sortable: true,
                    tooltip:"No. handphone",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Born", width : 100,
                    dataIndex : 'place_of_birth', sortable: true,
                    tooltip:"Employee Born at",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "BirthDate", width : 100,
                    dataIndex : 'date_of_birth', sortable: true,
                    tooltip:"Employee Born at",
                    editor: new Ext.form.DateField({
                        format: 'm/d/y',
                        // minValue: '01/01/20',
                    })
                },
                {   header: "Height", width : 50,
                    dataIndex : 'height', sortable: true,
                    tooltip:"Employee Height ",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Weight", width : 50,
                    dataIndex : 'weight', sortable: true,
                    tooltip:"Employee Weight",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Sex", width : 50,
                    dataIndex : 'sex', sortable: true,
                    tooltip:"jenis kelamin",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["label", "key"],
                                               data : [["Laki-laki", "L"], ["Perempuan", "P"]]
                                        }),
                            displayField:"label",
                            valueField:"key",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },                
                {   header: "Marital Status", width : 100,
                    dataIndex : 'marital_status', sortable: true,
                    tooltip:"status Perkawinan",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["label", "key"],
                                               data : [["Nikah", "K"], ["Belum", "T"]]
                                        }),
                            displayField:"label",
                            valueField:"key",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true} 
                    )
                },
                {   header: "Jabatan", width : 100,
                    dataIndex : 'jabatan', sortable: true,
                    tooltip:"jabatan",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Jenjang", width : 100,
                    dataIndex : 'dept_id', sortable: true,
                    tooltip:"jenjang sekolah/departemen",
                    editor : new Ext.form.ComboBox({
                        xtype : 'combo',
                        allowBlank: false,
                        store : this.DepartmentDS,
                        displayField: 'name',
                        valueField: 'dept_id',
                        mode : 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus:true,
                        editable: true,
                        typeAhead: true,
                        width : 100,
                        value: 'Pilih'
                        }),
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    tooltip:"Status",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["label", "key"],
                                               data : [["Active", "0"], ["Inactive", "1"]]
                                        }),
                            displayField:"label",
                            valueField:"key",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_at', sortable: true,
                    tooltip:"employee Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Modified", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"employee modified Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'emp_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'address', type: 'string'},
                {name: 'mobile_phone', type: 'string'},
                {name: 'place_of_birth', type: 'string'},
                {name: 'date_of_birth', type: 'date'},
                {name: 'height', type: 'integer'},
                {name: 'weight', type: 'integer'},
                {name: 'sex', type: 'string'},
                {name: 'marital_status', type: 'string'},
                {name: 'jabatan', type: 'string'},
                {name: 'dept_id', type: 'integer'},
                {name: 'status', type: 'integer'},
                {name: 'created_at', type: 'date'},
                // {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'employee_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'employee_phone',
                    cid: 'mobile_phone',
                    fieldLabel: 'Phone',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'employee_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ [''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_employeeSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.employee_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/employee/1/0')}}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.employee.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Copy',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.Grid_copy,
                        scope : this
                    },
                    //{   text:'Edit',
                    //    tooltip:'Edit Record',
                    //    iconCls: 'silk-page-white-edit',
                    //    //handler : this.employee_grid_
                    //},
                    {   id : tabId+'_employeeSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_employeeGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_employeePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_employeePageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_employeeGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_employeeSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_employeeTab",
                jsId : tabId+"_employeeTab",
                title:  "Employee",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // employee grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   emp_id: "",
                    name: "New Name",
                    // emp_type:"",
                    address: "New address",
                    place_of_birth: "",
                    date_of_birth :"",
                    height:0,
                    weight:0,
                    sex:"",
                    marital_status:"",
                    mobile_phone: "New Phone ",
                    jabatan:"",
                    dept_id:"",
                    status: 0,
                    created_at: "",
                    // modified_date: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
         },
        // employee grid copy and make it a new record
        Grid_copy: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection)
            {   var data = this.Grid.getSelectionModel().selection.record.data;
                this.Grid.store.insert( 0,
                    new this.Records (
                    {  // id: "",
                        emp_id: data.emp_id,
                        name: data.name,
                        status:data.status
                    }));
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },



        // employee grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/employee/1/1') }}",
                    {   'x-csrf-token': alfalah.employee.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },

        // employee grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: '/employee/1/2',
                headers:
                {   'x-csrf-token': alfalah.employee.sid },
                params  :
                {   id: this.Grid.getSelectionModel().selection.record.data.emp_id
                },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Server Message : '+'\n'+the_response.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
        // employee search button
        employee_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_employeeSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
                {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                    this.DataStore.baseParams = Ext.apply( the_parameter, {s:"form", limit:this.page_limit, start:this.page_start});
                    this.DataStore.reload();
                };
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.employee.initialize, alfalah.employee);
// end of file
</script>
<div>&nbsp;</div>