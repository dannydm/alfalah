<script type="text/javascript">
// create namespace
Ext.namespace('saa.assetMachine');

// create application
saa.assetMachine= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : '',
        act : '',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.register.initialize();
            this.machine.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.register.Tab);
            this.centerPanel.add(this.machine.Tab);
            this.centerPanel.setActiveTab(this.register.Tab);
            this.centerPanel.endUpdate();
            saa.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },

    }; // end of public space
}(); // end of app
// create application
saa.assetMachine.register= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Doc.No", width : 130,
                    dataIndex : 'doc_no', sortable: true,
                    tooltip:"Doc.No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> [-'+record.data.doc_date+'-] ';
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Items", width : 200,
                    dataIndex : 'ref_item_id', sortable: true,
                    tooltip:"Items",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.item_name;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Asset.ID", width : 100,
                    dataIndex : 'asset_id', sortable: true,
                    tooltip:"Asset.ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.historyid )
                        {   metaData.attr = "style = background-color:grey;";  }
                        else if ( Ext.isEmpty(record.data.deviceid) )
                        {   metaData.attr = "style = background-color:red;";  }
                        else {   metaData.attr = "style = background-color:lime;"; };
                        result = value+'<br> [ '+record.data.serial_no+' ] <br> '+record.data.deviceid;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "GRIN", width : 140,
                    dataIndex : 'ref_no', sortable: true,
                    tooltip:"Ref.No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> - <br> '+record.data.stock_grin_no;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "PO / PR", width : 140,
                    dataIndex : 'po_no', sortable: true,
                    tooltip:"PO.No / PR.No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> - <br> '+record.data.pr_no;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Remark", width : 200,
                    dataIndex : 'remark', sortable: true,
                    tooltip:"Remark",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> - <br> '+record.data.remarks;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Menu Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"Menu Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'menu_id', type: 'integer'},
                {name: 'parent_menu_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'link', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'level_degree', type: 'string'},
                {name: 'arrange_no', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'am_doc_no',
                    cid: 'doc_no',
                    fieldLabel: 'Doc.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'am_asset_id',
                    cid: 'asset_id',
                    fieldLabel: 'Asset.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'am_item_name',
                    cid: 'item_name',
                    fieldLabel: 'Items',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'am_grin_id',
                    cid: 'grin_id',
                    fieldLabel: 'GRIN.Inc',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'am_grin_no',
                    cid: 'grin_no',
                    fieldLabel: 'GRIN.Out',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'am_po_no',
                    cid: 'po_no',
                    fieldLabel: 'PO.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'am_pr_no',
                    cid: 'pr_no',
                    fieldLabel: 'PR.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'am_item_type',
                    cid: 'item_type',
                    fieldLabel: 'Type',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['type'],
                        data : [ ['-'], ['IT'], ['Non-IT']]
                    }),
                    displayField:'type',
                    valueField :'type',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'IT'
                },
                {   id: 'am_registered',
                    cid: 'registered',
                    fieldLabel: 'Registered',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['type'],
                        data : [ ['ALL'], ['YES'], ['NO'], ['ARC']]
                    }),
                    displayField:'type',
                    valueField :'type',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'NO'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_regSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.reg_search_handler,
                scope : this
            });
            this.DataStore = saa.core.newDataStore(
                '/asset/20/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                // columns : this.Columns,
                colModel: new Ext.ux.grid.LockingColumnModel(this.Columns),
                view: new Ext.ux.grid.LockingGridView(),
                stripeRows : true,
                columnLines: true,
                enableColLock: false,
                loadMask: true,
                height : saa.assetMachine.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_regGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_regPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_regPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_regGridBBar').getPageData().activePage -1) * page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_regSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_regTab",
                jsId : tabId+"_regTab",
                title:  "Register",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 70,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }
                    ]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        reg_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_regSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start
                });
                this.DataStore.load();
            };
        },
    }; // end of public space
}(); // end of app
// create application
saa.assetMachine.machine = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.GridCombo;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;
    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = saa.assetMachine.centerPanel.container.dom.clientHeight;
            this.AssetDS = saa.core.newDataStore(
                '/asset/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.MCTypeDS = saa.core.newDataStore(
                '/asset/11/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.MCBrandDS = saa.core.newDataStore(
                '/asset/20/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SiteDeptDS = saa.core.newDataStore(
                '/asset/0/11', true,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.Columns = [
                {   header: "Status", width : 80,
                    dataIndex : 'status', sortable: true,
                    tooltip:"Remark",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['type'],
                            data : [ ['Active'], ['Inactive'], ['Dispose']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'type',
                        valueField: 'type',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == 'Active'){} else
                        {   metaData.attr = "style = background-color:red;"; };
                        return value;
                    },
                },
                {   header: "Asset.ID", width : 120,
                    dataIndex : 'asset_id', sortable: true,
                    tooltip:"Asset.ID",
                    css : "background-color: #56FF00;",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store: this.AssetDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'asset_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.asset_id = record.data.asset_id;
                                    combo.gridEditor.record.data.item_id = record.data.ref_item_id;
                                    record.data.status = 'ACTIVE';
                                    saa.assetMachine.machine.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( Ext.isEmpty(record.data.asset_id) || (record.data.status != 'ACTIVE') )
                        {   metaData.attr = "style = background-color:red;"; };
                        return value;
                    },
                },
                {   header: "Name", width : 200,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Hardware Name",
                    // locked : true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.tag;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Barcode.ID", width : 150,
                    dataIndex : 'mach_regid', sortable: true,
                    tooltip:"Barcode.ID",
                },
                {   header: "Serial.No", width : 100,
                    dataIndex : 'serial_no', sortable: true,
                    tooltip:"Machine Serial No",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Type", width : 120,
                    dataIndex : 'mactype_id', sortable: true,
                    tooltip:"Machine Type",
                    css : "background-color: #56FF00;",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store: this.MCTypeDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'mactype_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.mactype_id   = record.data.mactype_id;
                                    combo.gridEditor.record.data.mactype_name = record.data.name;
                                    saa.assetMachine.machine.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( Ext.isEmpty(record.data.mactype_id) || (record.data.status != 'Active') )
                        {   metaData.attr = "style = background-color:red;"; };
                        result = record.data.mactype_name+'<br>'+value;
                        return saa.core.gridColumnWrap(result);
                    },
                },
                {   header: "Brand", width : 120,
                    dataIndex : 'machbrand_id', sortable: true,
                    tooltip:"Machine Brand",
                    css : "background-color: #56FF00;",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store: this.MCBrandDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'machbrand_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.machbrand_id   = record.data.machbrand_id;
                                    combo.gridEditor.record.data.machbrand_name = record.data.name;
                                    saa.assetMachine.machine.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( Ext.isEmpty(record.data.machbrand_id) || (record.data.status != 'Active') )
                        {   metaData.attr = "style = background-color:red;"; };
                        result = record.data.machbrand_name+'<br>'+value;
                        return saa.core.gridColumnWrap(result);
                    },
                },
                {   header: "Remark", width : 100,
                    dataIndex : 'remark', sortable: true,
                    tooltip:"Remark",
                    editor : new Ext.form.TextField({allowBlank: false})
                },
                {   header: "Site", width : 100,
                    dataIndex : 'site_id', sortable: true,
                    tooltip:"Site"
                },
                {   header: "Dept.", width : 120,
                    dataIndex : 'dept_id', sortable: true,
                    tooltip:"Dept.Name",
                    editor: new Ext.form.ComboBox(
                    {   store: this.SiteDeptDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'dept_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.company_id = record.data.company_id;
                                    combo.gridEditor.record.data.site_id = record.data.site_id;
                                    combo.gridEditor.record.data.dept_id = record.data.dept_id;
                                    combo.gridEditor.record.data.dept_name = record.data.dept_name;
                                    saa.assetMachine.machine.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.dept_name+'<br>'+value;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Room", width : 200,
                    dataIndex : 'room', sortable: true,
                    tooltip:"Room Location",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Object Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"Object Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Last.Inv.Date", width : 150,
                    dataIndex : 'lastdate', sortable: true,
                    tooltip:"Object Last Inventory Date",
                    css : "background-color: #DCFFDE;",
                    // renderer: function(value){  return saa.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'menu_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'type', type: 'string'},
                {name: 'link', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'machine_asset_id',
                    cid : 'asset_id',
                    fieldLabel: 'Assets.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'machine_item_id',
                    cid:'item_id',
                    fieldLabel: 'Item.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'machine_item_name',
                    cid:'item_name',
                    fieldLabel: 'Item.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'machine_name',
                    cid:'machine_name',
                    fieldLabel: 'Hw.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'machine_tag',
                    cid:'tag',
                    fieldLabel: 'TAG',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'machine_user',
                    cid:'username',
                    fieldLabel: 'User',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'machine_os_name',
                    cid:'os_name',
                    fieldLabel: 'OS',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'machine_processor',
                    cid:'processor',
                    fieldLabel: 'Procs',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'machine_ip_address',
                    cid:'ip_address',
                    fieldLabel: 'IP.Addr',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'is_monitor',
                    cid : 'is_monitor',
                    fieldLabel: 'Monitored',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['YES'], ['NO'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'NO'
                },
                {   id: 'status',
                    cid : 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['ACTIVE'], ['INACTIVE'], ['DISPOSE'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'ALL'
                },
                {   id: 'comp_type',
                    cid : 'comp_type',
                    fieldLabel: 'Class',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['DESKTOP'], ['LAPTOP'], ['SERVER'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'ALL'
                },
                {   id: 'source',
                    cid : 'source',
                    fieldLabel: 'Source',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['OCS'], ['MISSING'], ['ASSET'], ['ARCHIVE'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'OCS'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_machineSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.machine_search_handler,
                scope : this
            });
            /**
             * MAIN-GRID
            */
            this.DataStore = saa.core.newDataStore(
                '/asset/21/0', false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.DataStore.load();
            this.MenuComboDataStore = new Ext.data.Store(
            {   //url: '< ? php echo url::site(false); ?>saa/asset/menu/0/0',
                //default parameter
                baseParams: {s:"init"},
                reader: new Ext.data.JsonReader()
            });
            this.GridCombo = new Ext.form.ComboBox(
            {   store: this.MenuComboDataStore,
                typeAhead: true,
                id: tabId+"_RegMenuObjectCombo",
                jsid: tabId+"_RegMenuObjectCombo",
                width: 250,
                displayField: 'link',
                valueField: 'id',
                mode: 'local',
                forceSelection: true,
                triggerAction: 'all',
                selectOnFocus: true,
                listeners : { scope : this,
                    'select' : function (a,b,c)
                    {   var the_value = Ext.getCmp(this.tabId+"_RegMenuObjectCombo").value;
                        this.DataStore.baseParams = Ext.apply( {}, {s:"combo", limit:this.page_limit, start:this.page_start, menu_id: the_value});
                        this.DataStore.reload();
                    }
                }
            });
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                colModel: new Ext.ux.grid.LockingColumnModel(this.Columns),
                view: new Ext.ux.grid.LockingGridView(),
                stripeRows : true,
                columnLines: true,
                loadMask: true,
                height : (this.region_height)-50,
                anchor: '100%',
                autoWidth  : true,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Copy Below',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.copy_below,
                        scope : this
                    },
                    {   id : tabId+'_machineSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    {   text:'Archive',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    '-','Selected Menu', this.GridCombo,
                    {   //text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.MenuComboDataStore.reload(); },
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_hgBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_hpCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_hpCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_hgBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_machineTab",
                jsId : tabId+"_machineTab",
                title:  "Machine",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'ToolBox',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //{   region: 'west',
                //    title: 'Roles',
                //    split: true,
                //    width: 200,
                //    minSize: 175,
                //    maxSize: 400,
                //    collapsible: true,
                //    margins: '0 0 0 5',
                //    items:[this.EastGrid]
                //},

                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // machine grid add new record
        Grid_add: function(button, event)
        {   var selected_reg = Ext.getCmp(this.tabId+"_RegMenuObjectCombo");
            if (selected_reg.value)
            {   this.Grid.stopEditing();
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   id: "",
                        menu_id: selected_reg.value,
                        name: "New Name",
                        type: "New Type",
                        link: selected_reg.lastSelectionText,
                    }));
                // placed the edit cursor on third-column
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'W A R N I N G',
                        msg: 'No Menu Selected',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                     });
            };
        },
        // menu grid copy and make it a new record
        Grid_copy: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection)
            {   var data = this.Grid.getSelectionModel().selection.record.data;
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   id: "",
                        menu_id: data.menu_id,
                        name: data.name,
                        type: data.type,
                        link: data.link,
                        created_date : null,
                        last_update : null
                    }));
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },
        // machine grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   //console.log("Data changes, Do you want to save it before reloads ? ") ;
                var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        // this.store is equal with saa.assetMachine.machine.DataStore
                        Ext.each(the_record.fields.items, //this.DataStore.reader.meta.fields,
                            function(the_field)
                            {   v_data = {};
                                var the_data = the_record.data;
                                if (Ext.isDate( the_data[the_field["name"]] ) )
                                {   v_data[ the_field["name"] ] = the_data[the_field["name"]].format("d/m/Y");  }
                                else
                                {   v_data[ the_field["name"]] = the_data[the_field["name"]]; };
                                v_json= Ext.apply( v_json, v_data);
                            });
                        json_data.push(v_json);
                    }, this);
                Ext.Ajax.request(
                {   method: 'POST',
                    url: '/asset/21/1',
                    headers:
                    {   'x-csrf-token': saa.assetMachine.sid },
                    params:
                    {   json: Ext.encode(json_data),
                        btn : button.text
                    },
                    success : function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : the_response.message+'\n Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   this.DataStore.reload();    };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    scope: this
                });
            };
        },
        // machine grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: '/asset/21/2',
                headers:
                {   'x-csrf-token': saa.assetMachine.sid },
                params:
                {   s:"form",
                    id: this.Grid.getSelectionModel().selection.record.data.id
                },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : the_response.message+'\n Server Message : '+'\n'+the_response.server_message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
        // machine search button
        machine_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_machineSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        task: saa.assetMachine.task,
                        act: saa.assetMachine.act,
                        a:2, b:0,
                        limit: this.page_limit, start: this.page_start
                });
                this.DataStore.reload();
            };
        },
        "copy_below" : function (button, event)
        {   the_button = button;
            the_event = event;
            the_grid = this.Grid;
            the_datastore = this.DataStore;
            the_xy = the_grid.getSelectionModel().getSelectedCell();
            the_field = the_grid.getColumnModel().getDataIndex(the_xy[1]);
            the_value = the_datastore.getAt(the_xy[0]).get(the_field);
            //convert date value into shortdate format
            // if (Ext.isDate(the_value) )
            // {   the_value = shortdateRenderer(the_value,'Y-m-d', 'd/m/Y');  };

            switch(the_field)
            {   case "status":
                case "is_client":
                case "is_vm":
                case "comp_type":
                {   saa.core.copy_below( the_button, the_event, the_datastore, the_xy, the_field);  };
                break;
                case "dept_id":
                {   saa.core.copy_below( the_button, the_event, the_datastore, the_xy, the_field);
                    saa.core.copy_below( the_button, the_event, the_datastore, the_xy, "company_id");
                    saa.core.copy_below( the_button, the_event, the_datastore, the_xy, "site_id");
                };
                break;
                default :
                {   Ext.Msg.show(
                    {   title:'E R R O R ',
                        msg: 'Could not copied, Read Only Column',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                     });
                };
                break;
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
//Ext.reg('permission', saa);
Ext.onReady(saa.assetMachine.initialize, saa.assetMachine);
// end of file
</script>
<div>&nbsp;</div>