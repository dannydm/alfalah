<script type="text/javascript">
// create namespace
Ext.namespace('saa.assetITD');

// create application
saa.assetITD= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : '',
        act : '',
        index2 : 'index2.php?option=com_garment&no_html=1&',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.register.initialize();
            this.hardware.initialize();
            this.license.initialize();
            this.software.initialize();
            this.asset_software.initialize();
            this.noncomputer.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.register.Tab);
            this.centerPanel.add(this.hardware.Tab);
            this.centerPanel.add(this.license.Tab);
            this.centerPanel.add(this.software.Tab);
            this.centerPanel.add(this.asset_software.Tab);
            this.centerPanel.add(this.noncomputer.Tab);
            this.centerPanel.setActiveTab(this.register.Tab);
            this.centerPanel.endUpdate();
            saa.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
        "copy_below" : function (button, event)
        {   if (button.type == 'order')
            {   var the_grid = this.Grid_30;
                var the_datastore = this.DataStore_30;
            }
            else if (button.type == 'sample')
            {   var the_grid = this.Grid_30_Sample;
                var the_datastore = this.DataStore_30_Sample;
            }
            else
            {   var the_grid = this.Grid_30_Material;
                var the_datastore = this.DataStore_30_Material;
            };
            the_xy = the_grid.getSelectionModel().getSelectedCell();
            the_field = the_grid.getColumnModel().getDataIndex(the_xy[1]);
            the_value = the_datastore.getAt(the_xy[0]).get(the_field);
            is_modified = false;
            switch(the_field)
            {   case "doc_date":
                case "reported_date":
                case "doc_no" :
                case "edi_id" :
                case "ref_company_name" :
                case "response_no" :
                case "no_aju" :
                case "invoice_no" :
                case "invoice_date" :
                case "courier_id" :
                case "port_arrival" :
                case "port_departure" :
                case "vessel_id" :
                case "shipping_term_id" :
                case "quantity" :
                case "quantity_um_id" :
                case "carton_quantity" :
                case "weight" :
                case "weight_um_id" :
                case "volume" :
                case "volume_um_id" :
                case "container_type" :
                case "invoice_currency_id" :
                case "awbl_host" :
                case "awbl_master" :
                case "fob_value" :
                case "invoice_discount" :
                case "other_value" :
                case "currency_id" :
                case "invoice_unit_price" :
                case "hs_no" :
                case "admission_pct" :
                case "package_quantity" :
                case "package_um_id" :
                case "content_quantity" :
                case "content_um_id" :
                case "container_no" :
                case "container_size" :
                case "departure_date" :
                case "estimate_export_date" :
                case "reconsiliation_date" :
                case "payment_type" :
                case "freight_value" :
                case "insurance_value" :
                case "party_name" :
                case "remarks" :
                {   is_modified = true;
                    switch(the_field)
                    {   case "doc_date":
                        case "invoice_date" :
                        case "departure_date" :
                        case "estimate_export_date" :
                        case "reconsiliation_date" :
                        case "reported_date":
                        {   the_value = customs.bc30.shortdateRenderer(the_value,'Y-m-d', 'd/m/Y');
                        };
                        break;
                    };
                };
                break;
            };
            if (is_modified == true)
            {   the_record_count = the_datastore.getCount();
                for (var the_x = the_xy[0]+1; the_x < the_record_count; the_x++)
                {   a = the_datastore.getAt(the_xy[0]).get(the_field);
                    the_datastore.getAt(the_x).set(the_field, a);
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'Could not copied, Read Only Column',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },

    }; // end of public space
}(); // end of app
// create application
saa.assetITD.register= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Doc.No", width : 130,
                    dataIndex : 'doc_no', sortable: true,
                    tooltip:"Doc.No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> [-'+record.data.doc_date+'-] ';
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Items", width : 200,
                    dataIndex : 'ref_item_id', sortable: true,
                    tooltip:"Items",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.item_name;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Asset.ID", width : 100,
                    dataIndex : 'asset_id', sortable: true,
                    tooltip:"Asset.ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.historyid )
                        {   metaData.attr = "style = background-color:grey;";  }
                        else if ( Ext.isEmpty(record.data.deviceid) )
                        {   metaData.attr = "style = background-color:red;";  }
                        else {   metaData.attr = "style = background-color:lime;"; };
                        result = value+'<br> [ '+record.data.serial_no+' ] <br> '+record.data.deviceid;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "GRIN", width : 140,
                    dataIndex : 'ref_no', sortable: true,
                    tooltip:"Ref.No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> - <br> '+record.data.stock_grin_no;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "PO / PR", width : 140,
                    dataIndex : 'po_no', sortable: true,
                    tooltip:"PO.No / PR.No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> - <br> '+record.data.pr_no;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Remark", width : 200,
                    dataIndex : 'remark', sortable: true,
                    tooltip:"Remark",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> - <br> '+record.data.remarks;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Menu Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"Menu Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'menu_id', type: 'integer'},
                {name: 'parent_menu_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'link', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'level_degree', type: 'string'},
                {name: 'arrange_no', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'doc_no',
                    cid: 'doc_no',
                    fieldLabel: 'Doc.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'asset_id',
                    cid: 'asset_id',
                    fieldLabel: 'Asset.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'item_name',
                    cid: 'item_name',
                    fieldLabel: 'Items',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'grin_id',
                    cid: 'grin_id',
                    fieldLabel: 'GRIN.Inc',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'grin_no',
                    cid: 'grin_no',
                    fieldLabel: 'GRIN.Out',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'po_no',
                    cid: 'po_no',
                    fieldLabel: 'PO.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pr_no',
                    cid: 'pr_no',
                    fieldLabel: 'PR.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'item_type',
                    cid: 'item_type',
                    fieldLabel: 'Type',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['type'],
                        data : [ ['-'], ['IT'], ['Non-IT']]
                    }),
                    displayField:'type',
                    valueField :'type',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'IT'
                },
                {   id: 'registered',
                    cid: 'registered',
                    fieldLabel: 'Registered',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['type'],
                        data : [ ['ALL'], ['YES'], ['NO'], ['ARC']]
                    }),
                    displayField:'type',
                    valueField :'type',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'NO'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_regSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.reg_search_handler,
                scope : this
            });
            this.DataStore = saa.core.newDataStore(
                '/asset/1/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                // columns : this.Columns,
                colModel: new Ext.ux.grid.LockingColumnModel(this.Columns),
                view: new Ext.ux.grid.LockingGridView(),
                stripeRows : true,
                columnLines: true,
                enableColLock: false,
                loadMask: true,
                height : saa.assetITD.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Reg.NonComputer',
                        tooltip:'Register Non Computer Hardware',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_regGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_regPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_regPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_regGridBBar').getPageData().activePage -1) * page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_regSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_regTab",
                jsId : tabId+"_regTab",
                title:  "Register",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 70,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }
                    ]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        reg_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_regSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start
                });
                this.DataStore.load();
            };
        },
        // register non-Computer Hardware
        Grid_add : function(button, event)
        {   var the_records = this.Grid.getSelectionModel().selection.record; //this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selection ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   //console.log("Data changes, Do you want to save it before reloads ? ") ;
                var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        // this.store is equal with saa.assetITD.license.DataStore
                        Ext.each(this.DataStore.reader.meta.fields,
                            function(the_field)
                            {   v_data = {};
                                var the_data = the_record.data;
                                if (Ext.isDate( the_data[the_field["name"]] ) )
                                {   v_data[ the_field["name"] ] = the_data[the_field["name"]].format("d/m/Y");  }
                                else
                                {   v_data[ the_field["name"]] = the_data[the_field["name"]]; };
                                v_json= Ext.apply( v_json, v_data);
                            });
                        json_data.push(v_json);
                    }, this);
                Ext.Ajax.request(
                {   method: 'POST',
                    url: '/asset/1/1',
                    headers:
                    {   'x-csrf-token': saa.assetITD.sid },
                    params:
                    {   btn: 'reg',
                        json: Ext.encode(json_data),
                    },
                    success : function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : the_response.message+'\n Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   this.DataStore.reload();    };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    scope: this
                });
            };
        },
        
        
    }; // end of public space
}(); // end of app
// create application
saa.assetITD.hardware = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.GridCombo;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;
    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = saa.assetITD.centerPanel.container.dom.clientHeight;
            this.AssetDS = saa.core.newDataStore(
                '/asset/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SiteDeptDS = saa.core.newDataStore(
                '/asset/0/11', true,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.SiteBuildingDS = saa.core.newDataStore(
                '/company/12/9', true,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.Columns = [
                {   header: "Status", width : 80,
                    dataIndex : 'status', sortable: true,
                    tooltip:"Remark",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['type'],
                            data : [ ['ACTIVE'], ['INACTIVE'], ['DISPOSE']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'type',
                        valueField: 'type',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == 'ACTIVE'){} else
                        {   metaData.attr = "style = background-color:red;"; };
                        return value;
                    },
                },
                {   header: "Asset.ID", width : 120,
                    dataIndex : 'asset_id', sortable: true,
                    tooltip:"Asset.ID",
                    css : "background-color: #56FF00;",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store: this.AssetDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'asset_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.asset_id = record.data.asset_id;
                                    combo.gridEditor.record.data.item_id = record.data.ref_item_id;
                                    record.data.status = 'ACTIVE';
                                    saa.assetITD.hardware.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( Ext.isEmpty(record.data.asset_id) || (record.data.status != 'ACTIVE') )
                        {   metaData.attr = "style = background-color:red;"; };
                        return value;
                    },
                },
                {   header: "Name", width : 200,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Hardware Name",
                    // locked : true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.tag;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "OCS.ID", width : 50,
                    dataIndex : 'id', sortable: true,
                    tooltip:"OCS.ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.itd_id !== value )
                        {   metaData.attr = "style = background-color:red;"; };
                        result = value+'<br>'+record.data.itd_id;
                        return saa.core.gridColumnWrap(result);
                    },
                },
                {   header: "User", width : 100,
                    dataIndex : 'userid', sortable: true,
                    tooltip:"Items",
                },
                {   header: "Class", width : 80,
                    dataIndex : 'comp_type', sortable: true,
                    tooltip:"Type of Hardware",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['type'],
                            data : [ ['DESKTOP'], ['LAPTOP'], ['SERVER']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'type',
                        valueField: 'type',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == "YES")
                        {   metaData.attr = "style = background-color:lime;"; };
                        return value;
                    },
                },
                {   header: "OS", width : 200,
                    dataIndex : 'osname', sortable: true,
                    tooltip:"Items",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return saa.core.gridColumnWrap(value); }
                },
                {   header: "Processor", width : 200,
                    dataIndex : 'processort', sortable: true,
                    tooltip:"Items",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return saa.core.gridColumnWrap(value); }
                },
                {   header: "RAM", width : 50,
                    dataIndex : 'memory', sortable: true,
                    tooltip:"Items"
                },
                {   header: "IP.Addr", width : 100,
                    dataIndex : 'ipaddr', sortable: true,
                    tooltip:"Items"
                },
                {   header: "MAC.Addr", width : 200,
                    dataIndex : 'ori_macaddr', sortable: true,
                    tooltip:"MAC ADDRESS",
                    // locked : true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value !== record.data.macaddr)
                        {   metaData.attr = "style = background-color:red;"; };
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Remark", width : 100,
                    dataIndex : 'remark', sortable: true,
                    tooltip:"Remark",
                    editor : new Ext.form.TextField({allowBlank: false})
                },
                {   header: "Site", width : 100,
                    dataIndex : 'site_id', sortable: true,
                    tooltip:"Site"
                },
                {   header: "Dept.", width : 120,
                    dataIndex : 'dept_id', sortable: true,
                    tooltip:"Dept.Name",
                    editor: new Ext.form.ComboBox(
                    {   store: this.SiteDeptDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'dept_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.company_id = record.data.company_id;
                                    combo.gridEditor.record.data.site_id = record.data.site_id;
                                    combo.gridEditor.record.data.dept_id = record.data.dept_id;
                                    combo.gridEditor.record.data.dept_name = record.data.dept_name;
                                    saa.assetITD.hardware.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.dept_name+'<br>'+value;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Building", width : 100,
                    dataIndex : 'building_id', sortable: true,
                    tooltip:"Building ID",
                    editor: new Ext.form.ComboBox(
                    {   store: this.SiteBuildingDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'building_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.company_id = record.data.company_id;
                                    combo.gridEditor.record.data.site_id = record.data.site_id;
                                    combo.gridEditor.record.data.building_id = record.data.building_id;
                                    saa.assetITD.hardware.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.building_name+'<br>'+value;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Win.Server", width : 80,
                    dataIndex : 'is_server', sortable: true,
                    tooltip:"Is this Windows Server Computer ? Check Windows Version",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['type'],
                            data : [ ['YES'], ['NO']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'type',
                        valueField: 'type',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == "YES")
                        {   metaData.attr = "style = background-color:lime;"; };
                        return value;
                    },
                },
                {   header: "Win.Client", width : 80,
                    dataIndex : 'is_client', sortable: true,
                    tooltip:"Is this Windows Client Computer ? Check Windows Version",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['type'],
                            data : [ ['YES'], ['NO']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'type',
                        valueField: 'type',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == "YES")
                        {   metaData.attr = "style = background-color:lime;"; };
                        return value;
                    },
                },
                {   header: "VMs", width : 80,
                    dataIndex : 'is_vm', sortable: true,
                    tooltip:"Is this Virtual system ?",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['type'],
                            data : [ ['YES'], ['NO']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'type',
                        valueField: 'type',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == "YES")
                        {   metaData.attr = "style = background-color:lime;"; };
                        return value;
                    },
                },
                {   header: "Parent.Asset.ID", width : 120,
                    dataIndex : 'parent_asset_id', sortable: true,
                    tooltip:"Parent.Asset.ID",
                    css : "background-color: #56FF00;",
                    editor: new Ext.form.ComboBox(
                    {   store: this.AssetDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'asset_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.parent_asset_id = record.data.asset_id;
                                    saa.assetITD.hardware.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( Ext.isEmpty(record.data.asset_id) || (record.data.status != 'ACTIVE') )
                        {   metaData.attr = "style = background-color:red;"; };
                        return value;
                    },
                },
                {   header: "Reported", width : 80,
                    dataIndex : 'is_asset_reported', sortable: true,
                    tooltip:"Is Asset Reported ?",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['type'],
                            data : [ ['YES'], ['NO']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'type',
                        valueField: 'type',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == "YES")
                        {   metaData.attr = "style = background-color:lime;"; };
                        return value;
                    },
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Object Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"Object Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Last.Inv.Date", width : 150,
                    dataIndex : 'lastdate', sortable: true,
                    tooltip:"Object Last Inventory Date",
                    css : "background-color: #DCFFDE;",
                    // renderer: function(value){  return saa.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'menu_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'type', type: 'string'},
                {name: 'link', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'hardware_asset_id',
                    cid : 'asset_id',
                    fieldLabel: 'Assets.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'hardware_item_id',
                    cid:'item_id',
                    fieldLabel: 'Item.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'hardware_item_name',
                    cid:'item_name',
                    fieldLabel: 'Item.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'hardware_name',
                    cid:'hardware_name',
                    fieldLabel: 'Hw.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'hardware_tag',
                    cid:'tag',
                    fieldLabel: 'TAG',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'hardware_user',
                    cid:'username',
                    fieldLabel: 'User',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'hardware_os_name',
                    cid:'os_name',
                    fieldLabel: 'OS',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'hardware_processor',
                    cid:'processor',
                    fieldLabel: 'Procs',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'hardware_ip_address',
                    cid:'ip_address',
                    fieldLabel: 'IP.Addr',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'is_monitor',
                    cid : 'is_monitor',
                    fieldLabel: 'Monitored',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['YES'], ['NO'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'NO'
                },
                {   id: 'status',
                    cid : 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['ACTIVE'], ['INACTIVE'], ['DISPOSE'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'ALL'
                },
                {   id: 'comp_type',
                    cid : 'comp_type',
                    fieldLabel: 'Class',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['DESKTOP'], ['LAPTOP'], ['SERVER'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'ALL'
                },
                {   id: 'source',
                    cid : 'source',
                    fieldLabel: 'Source',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['OCS'], ['MISSING'], ['ASSET'], ['ARCHIVE'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'OCS'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_hardwareSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.hardware_search_handler,
                scope : this
            });
            /**
             * MAIN-GRID
            */
            this.DataStore = saa.core.newDataStore(
                '/asset/2/0', false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.MenuComboDataStore = new Ext.data.Store(
            {   //url: '< ? php echo url::site(false); ?>saa/asset/menu/0/0',
                //default parameter
                baseParams: {s:"init"},
                reader: new Ext.data.JsonReader()
            });
            this.GridCombo = new Ext.form.ComboBox(
            {   store: this.MenuComboDataStore,
                typeAhead: true,
                id: tabId+"_RegMenuObjectCombo",
                jsid: tabId+"_RegMenuObjectCombo",
                width: 250,
                displayField: 'link',
                valueField: 'id',
                mode: 'local',
                forceSelection: true,
                triggerAction: 'all',
                selectOnFocus: true,
                listeners : { scope : this,
                    'select' : function (a,b,c)
                    {   var the_value = Ext.getCmp(this.tabId+"_RegMenuObjectCombo").value;
                        this.DataStore.baseParams = Ext.apply( {}, {s:"combo", limit:this.page_limit, start:this.page_start, menu_id: the_value});
                        this.DataStore.reload();
                    }
                }
            });
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                colModel: new Ext.ux.grid.LockingColumnModel(this.Columns),
                view: new Ext.ux.grid.LockingGridView(),
                stripeRows : true,
                columnLines: true,
                loadMask: true,
                height : (this.region_height)-50,
                anchor: '100%',
                autoWidth  : true,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Copy Below',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.copy_below,
                        scope : this
                    },
                    {   id : tabId+'_hardwareSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    {   text:'Archive',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    '-','Selected Menu', this.GridCombo,
                    {   //text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.MenuComboDataStore.reload(); },
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_hgBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_hpCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_hpCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_hgBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_hardwareTab",
                jsId : tabId+"_hardwareTab",
                title:  "Computer",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'ToolBox',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //{   region: 'west',
                //    title: 'Roles',
                //    split: true,
                //    width: 200,
                //    minSize: 175,
                //    maxSize: 400,
                //    collapsible: true,
                //    margins: '0 0 0 5',
                //    items:[this.EastGrid]
                //},

                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // menu grid copy and make it a new record
        Grid_copy: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection)
            {   var data = this.Grid.getSelectionModel().selection.record.data;
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   id: "",
                        menu_id: data.menu_id,
                        name: data.name,
                        type: data.type,
                        link: data.link,
                        created_date : null,
                        last_update : null
                    }));
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },
        // hardware grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   //console.log("Data changes, Do you want to save it before reloads ? ") ;
                var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        // this.store is equal with saa.assetITD.hardware.DataStore
                        Ext.each(the_record.fields.items, //this.DataStore.reader.meta.fields,
                            function(the_field)
                            {   v_data = {};
                                var the_data = the_record.data;
                                if (Ext.isDate( the_data[the_field["name"]] ) )
                                {   v_data[ the_field["name"] ] = the_data[the_field["name"]].format("d/m/Y");  }
                                else
                                {   v_data[ the_field["name"]] = the_data[the_field["name"]]; };
                                v_json= Ext.apply( v_json, v_data);
                            });
                        json_data.push(v_json);
                    }, this);
                Ext.Ajax.request(
                {   method: 'POST',
                    url: '/asset/6/1',
                    headers:
                    {   'x-csrf-token': saa.assetITD.sid },
                    params:
                    {   json: Ext.encode(json_data),
                        btn : button.text
                    },
                    success : function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : the_response.message+'\n Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   this.DataStore.reload();    };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    scope: this
                });
            };
        },
        // hardware grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: '/asset/6/2',
                headers:
                {   'x-csrf-token': saa.assetITD.sid },
                params:
                {   s:"form",
                    id: this.Grid.getSelectionModel().selection.record.data.itd_id
                },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : the_response.message+'\n Server Message : '+'\n'+the_response.server_message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
        // hardware search button
        hardware_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_hardwareSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        task: saa.assetITD.task,
                        act: saa.assetITD.act,
                        a:2, b:0,
                        limit: this.page_limit, start: this.page_start
                });
                this.DataStore.reload();
            };
        },
        "copy_below" : function (button, event)
        {   the_button = button;
            the_event = event;
            the_grid = this.Grid;
            the_datastore = this.DataStore;
            the_xy = the_grid.getSelectionModel().getSelectedCell();
            the_field = the_grid.getColumnModel().getDataIndex(the_xy[1]);
            the_value = the_datastore.getAt(the_xy[0]).get(the_field);
            //convert date value into shortdate format
            // if (Ext.isDate(the_value) )
            // {   the_value = shortdateRenderer(the_value,'Y-m-d', 'd/m/Y');  };
            switch(the_field)
            {   case "status":
                case "is_client":
                case "is_vm":
                case "comp_type":
                case "building_id":
                {   saa.core.copy_below( the_button, the_event, the_datastore, the_xy, the_field);  };
                break;
                case "dept_id":
                {   saa.core.copy_below( the_button, the_event, the_datastore, the_xy, the_field);
                    saa.core.copy_below( the_button, the_event, the_datastore, the_xy, "company_id");
                    saa.core.copy_below( the_button, the_event, the_datastore, the_xy, "site_id");
                };
                break;
                default :
                {   Ext.Msg.show(
                    {   title:'E R R O R ',
                        msg: 'Could not copied, Read Only Column',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                     });
                };
                break;
            };
        },
    }; // end of public space
}(); // end of app
// create application
saa.assetITD.license = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.SoftwareDS = saa.core.newDataStore(
                '/asset/4/9', true,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.OSAssetDS = saa.core.newDataStore(
                '/asset/1/10', true,
                {   s:"form", type: "OS", limit:this.page_limit, start:this.page_start }
            );
            this.Columns = [
                {   header: "Asset.ID", width : 150,
                    dataIndex : 'asset_id', sortable: true,
                    tooltip:"Asset.ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.status != 'ACTIVE' )
                        {   metaData.attr = "style = background-color:red;"; };
                        result = value+'<br>'+record.data.tag;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Asset.Name", width : 150,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Asset.Name",
                    // locked : true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.status != 'ACTIVE' )
                        {   metaData.attr = "style = background-color:red;"; };
                        result = value+'<br>'+record.data.ipsrc;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Users", width : 150,
                    dataIndex : 'userid', sortable: true,
                    tooltip:"User Name",
                    // locked : true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.status != 'ACTIVE' )
                        {   metaData.attr = "style = background-color:red;"; };
                        result = value+'<br>'+record.data.processort;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "OS.Type", width : 100,
                    dataIndex : 'os_type', sortable: true,
                    tooltip:"Operating System Type",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['key'],
                            data : [ ['WINDOWS'], ['LINUX']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'key',
                        valueField: 'key',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.status != 'ACTIVE' )
                        {   metaData.attr = "style = background-color:red;"; };

                        result = value+'<br>'+record.data.status;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "OS.Name", width : 200,
                    dataIndex : 'os_name', sortable: true,
                    tooltip:"Operating System Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value;
                        if ( record.data.a_os_name !== record.data.b1_os_name )
                        {   metaData.attr = "style = background-color:orange;";
                            result = value+'<br>'+record.data.a_os_name;
                        }
                        else
                        {   if ( value != record.data.os_license_name )
                            {   metaData.attr = "style = background-color:yellow;"; }
                            else { metaData.attr = "style = background-color:lime;";  };
                        };
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "OS.Key", width : 220,
                    dataIndex : 'os_key', sortable: true,
                    tooltip:"Operating System Key",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value;
                        if ( record.data.a_os_key !== record.data.b1_os_key )
                        {   metaData.attr = "style = background-color:orange;";
                            result = value+'<br>'+record.data.a_os_key;
                        }
                        else
                        {   if ( value != record.data.os_license_key )
                            {   metaData.attr = "style = background-color:yellow;"; }
                            else { metaData.attr = "style = background-color:lime;"; };
                        };

                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Lic.Type", width : 100,
                    dataIndex : 'os_key_type', sortable: true,
                    tooltip:"Current Operating System License Type",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['key'],
                            data : [ ['NA'], ['OEM'], ['OLP'], ['FPP']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'key',
                        valueField: 'key',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                },
                {   header: "OS.Lic.Name", width : 220,
                    dataIndex : 'os_license_name', sortable: true,
                    tooltip:"assigned OS License Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "OS.Lic.Key", width : 220,
                    dataIndex : 'os_license_key', sortable: true,
                    tooltip:"OS License Key",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "OS.Lic.Type", width : 100,
                    dataIndex : 'os_license_type', sortable: true,
                    tooltip:"Own Operating System License Type",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['key'],
                            data : [ ['NA'], ['OEM'], ['OLP'], ['FPP']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'key',
                        valueField: 'key',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                },
                {   header: "OS.Asset.ID", width : 250,
                    dataIndex : 'os_asset_id', sortable: true,
                    tooltip:"Asset ID of Operating System",
                    // css : "background-color: #56FF00;",
                    editor: new Ext.form.ComboBox(
                    {   store: this.OSAssetDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'asset_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.removeAll();
                                    combo.combo.store.baseParams = {
                                        s:"form", type: "OS",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.os_asset_id = record.data.asset_id;
                                    combo.gridEditor.record.data.os_item_id = record.data.ref_item_id;
                                    saa.assetITD.license.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.os_item_name;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "CALOS.Asset.ID", width : 250,
                    dataIndex : 'cal_os_asset_id', sortable: true,
                    tooltip:"Asset ID of CAL Operating System",
                    // css : "background-color: #56FF00;",
                    editor: new Ext.form.ComboBox(
                    {   store: this.OSAssetDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'asset_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.removeAll();
                                    combo.combo.store.baseParams = {
                                        s:"form", type: "CALOS",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.cal_os_asset_id = record.data.asset_id;
                                    combo.gridEditor.record.data.cal_os_item_id = record.data.ref_item_id;
                                    saa.assetITD.license.Grid.getView().refresh();
                                },
                                'specialkey' : function (combo, button)
                                {   console.log("special");
                                    the_value = combo.lastQuery;
                                    combo.value = the_value;
                                    combo.gridEditor.record.data.invoice_no = the_value;
                                    if (button.getKey() == button.END)
                                    {   Ext.Msg.show(
                                        {   title:'Non-ASSET Selected',
                                            msg: 'You take '+the_value+' as CALOS.ASSET.ID. ',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.WARNING
                                        });
                                    };
                                }
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.cal_os_item_name;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Office.Type", width : 50,
                    dataIndex : 'office_type', sortable: true,
                    tooltip:"Office Type",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['key'],
                            data : [ ['NA'], ['MS'], ['NON-MS']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'key',
                        valueField: 'key',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                },
                {   header: "Office.Name", width : 200,
                    dataIndex : 'office_name', sortable: true,
                    tooltip:"Office Name",
                    // css : "background-color: #56FF00;",
                    editor: new Ext.form.ComboBox(
                    {   store: this.SoftwareDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'software_name',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start,
                                        asset_id : combo.combo.gridEditor.record.data.asset_id };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.office_soft_id = record.data.id;
                                    combo.gridEditor.record.data.office_name = record.data.software_name;
                                    combo.gridEditor.record.data.office_type = record.data.software_type;
                                    saa.assetITD.license.Grid.getView().refresh();
                                },
                                // 'specialkey' : function (combo, button)
                                // {   console.log("special");
                                //     the_value = combo.lastQuery;
                                //     combo.value = the_value;
                                //     combo.gridEditor.record.data.invoice_no = the_value;
                                //     if (button.getKey() == button.END)
                                //     {   Ext.Msg.show(
                                //         {   title:'Non-GEA Selected',
                                //             msg: 'You take '+the_value+' as Permit.No . ',
                                //             buttons: Ext.Msg.OK,
                                //             icon: Ext.MessageBox.WARNING
                                //         });
                                //     };
                                // }
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value != record.data.office_license_name )
                        {   metaData.attr = "style = background-color:yellow;"; }
                        else {   metaData.attr = "style = background-color:lime;"; };

                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Office.Key", width : 200,
                    dataIndex : 'office_key', sortable: true,
                    tooltip:"Office Key",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Office.Lic.Type", width : 50,
                    dataIndex : 'office_license_type', sortable: true,
                    tooltip:"Office License Type",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['key'],
                            data : [ ['NA'], ['OEM'], ['OLP'], ['FPP']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'key',
                        valueField: 'key',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                },
                {   header: "Office.lic.Name", width : 200,
                    dataIndex : 'office_license_name', sortable: true,
                    tooltip:"Assigned Office License Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Office.Asset.ID", width : 200,
                    dataIndex : 'office_asset_id', sortable: true,
                    tooltip:"Asset ID of Office",
                    // css : "background-color: #56FF00;",
                    editor: new Ext.form.ComboBox(
                    {   store: this.OSAssetDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'asset_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form", type: "OFC",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.office_asset_id = record.data.asset_id;
                                    combo.gridEditor.record.data.office_item_id = record.data.ref_item_id;
                                    saa.assetITD.license.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.office_item_name;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "AV.Type", width : 50,
                    dataIndex : 'av_type', sortable: true,
                    tooltip:"AV Type",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['key'],
                            data : [ ['MS'], ['NON-MS']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'key',
                        valueField: 'key',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                },
                {   header: "AV.Name", width : 200,
                    dataIndex : 'av_name', sortable: true,
                    tooltip:"AV Name",
                    // css : "background-color: #56FF00;",
                    editor: new Ext.form.ComboBox(
                    {   store: this.SoftwareDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'software_name',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                    s: "form", limit:this.page_limit, start:this.page_start,
                                    asset_id : combo.combo.gridEditor.record.data.asset_id };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.av_soft_id = record.data.id;
                                    combo.gridEditor.record.data.av_name = record.data.software_name;
                                    combo.gridEditor.record.data.av_type = record.data.software_type;
                                    saa.assetITD.license.Grid.getView().refresh();
                                },
                                // 'specialkey' : function (combo, button)
                                // {   console.log("special");
                                //     the_value = combo.lastQuery;
                                //     combo.value = the_value;
                                //     combo.gridEditor.record.data.invoice_no = the_value;
                                //     if (button.getKey() == button.END)
                                //     {   Ext.Msg.show(
                                //         {   title:'Non-GEA Selected',
                                //             msg: 'You take '+the_value+' as Permit.No . ',
                                //             buttons: Ext.Msg.OK,
                                //             icon: Ext.MessageBox.WARNING
                                //         });
                                //     };
                                // }
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value != record.data.av_license_name )
                        {   metaData.attr = "style = background-color:yellow;"; }
                        else {   metaData.attr = "style = background-color:lime;"; };

                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "AV.Key", width : 200,
                    dataIndex : 'av_key', sortable: true,
                    tooltip:"AV Key",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "AV.Lic.Type", width : 50,
                    dataIndex : 'av_license_type', sortable: true,
                    tooltip:"AV License Type",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['key'],
                            data : [ ['NA'], ['OEM'], ['OLP'], ['FPP']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'key',
                        valueField: 'key',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                },
                {   header: "AV.lic.Name", width : 200,
                    dataIndex : 'av_license_name', sortable: true,
                    tooltip:"Assigned Av License Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Diagram.Type", width : 50,
                    dataIndex : 'diagram_type', sortable: true,
                    tooltip:"Diagram Type",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['key'],
                            data : [ ['MS'], ['NON-MS']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'key',
                        valueField: 'key',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                },
                {   header: "Diagram.Name", width : 200,
                    dataIndex : 'diagram_name', sortable: true,
                    tooltip:"Diagram Name",
                    editor: new Ext.form.ComboBox(
                    {   store: this.SoftwareDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'software_name',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                    s: "form", limit:this.page_limit, start:this.page_start,
                                    asset_id : combo.combo.gridEditor.record.data.asset_id };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.diagram_soft_id = record.data.id;
                                    combo.gridEditor.record.data.diagram_name = record.data.software_name;
                                    combo.gridEditor.record.data.diagram_type = record.data.software_type;
                                    saa.assetITD.license.Grid.getView().refresh();
                                },
                                // 'specialkey' : function (combo, button)
                                // {   console.log("special");
                                //     the_value = combo.lastQuery;
                                //     combo.value = the_value;
                                //     combo.gridEditor.record.data.invoice_no = the_value;
                                //     if (button.getKey() == button.END)
                                //     {   Ext.Msg.show(
                                //         {   title:'Non-GEA Selected',
                                //             msg: 'You take '+the_value+' as Permit.No . ',
                                //             buttons: Ext.Msg.OK,
                                //             icon: Ext.MessageBox.WARNING
                                //         });
                                //     };
                                // }
                        }
                    }),
                },
                {   header: "Diagram.Key", width : 200,
                    dataIndex : 'diagram_key', sortable: true,
                    tooltip:"Diagram Key",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Diagram.Lic.Type", width : 50,
                    dataIndex : 'diagram_license_type', sortable: true,
                    tooltip:"Diagram License Type",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['key'],
                            data : [ ['NA'], ['OEM'], ['OLP']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'key',
                        valueField: 'key',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                },
                {   header: "Installed", width : 200,
                    dataIndex : 'installdate', sortable: true,
                    tooltip:"Installed",
                },
                {   header: "To.Do", width : 200,
                    dataIndex : 'remark', sortable: true,
                    tooltip:"Remark",
                    editor : new Ext.form.TextField({allowBlank: false}),
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'parent_license_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'note', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'license_asset_id',
                    cid: 'asset_id',
                    fieldLabel: 'Asset.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'license_asset_name',
                    cid: 'asset_name',
                    fieldLabel: 'Asset.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'license_tag',
                    cid: 'tag',
                    fieldLabel: 'Tag',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'license_os_name',
                    cid: 'os_name',
                    fieldLabel: 'OS.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'license_os_type',
                    cid : 'os_type',
                    fieldLabel: 'OS.Type',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['LINUX'], ['WINDOWS'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'WINDOWS'
                },
                {   id: 'license_os_key',
                    cid: 'os_key',
                    fieldLabel: 'OS.Key',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'license_os_item_name',
                    cid: 'os_item_name',
                    fieldLabel: 'OS.Ast.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'license_os_key_type',
                    cid : 'os_key_type',
                    fieldLabel: 'Lic.Type',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['NA'], ['OLP'], ['OEM'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'ALL'
                },
                {   id: 'license_os_lic_type',
                    cid : 'os_lic_type',
                    fieldLabel: 'OS.Lic.Type',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['NA'], ['OLP'], ['OEM'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'ALL'
                },
                // {   id: 'license_os_lic_key',
                //     cid: 'os_lic_key',
                //     fieldLabel: 'OS.Lic.Key',
                //     labelSeparator : '',
                //     xtype: 'textfield',
                //     width : 120
                // },
                {   id: 'license_off_key',
                    cid: 'off_key',
                    fieldLabel: 'OFF.Key',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },

                {   id: 'license_status',
                    cid : 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['ACTIVE'], ['INACTIVE'], ['DISPOSE'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'ACTIVE'
                },
                {   id: 'license_allocation',
                    cid : 'allocation',
                    fieldLabel: 'Allocation',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['YES'], ['NO']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'NO'
                },
                {   id: 'license_reported',
                    cid : 'reported',
                    fieldLabel: 'Reported',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['YES'], ['NO']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'YES'
                }
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_licenseSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.license_search_handler,
                scope : this
            });
            this.DataStore = saa.core.newDataStore(
                '/asset/5/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                colModel: new Ext.ux.grid.LockingColumnModel(this.Columns),
                view: new Ext.ux.grid.LockingGridView(),
                stripeRows : true,
                columnLines: true,
                loadMask: true,
                height : saa.assetITD.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   id : tabId+'_licenseSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Copy Below',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.copy_below,
                        scope : this
                    },
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_licenseGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_licensePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_licensePageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_licenseGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_licenseTab",
                jsId : tabId+"_licenseTab",
                title:  "License",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 70,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // license grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   id: "",
                    parent_license_id: "",
                    name: "New Name",
                    note: "New Note",
                    status: 0
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // license grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   //console.log("Data changes, Do you want to save it before reloads ? ") ;
                var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        // this.store is equal with saa.assetITD.license.DataStore
                        Ext.each(this.DataStore.reader.meta.fields,
                            function(the_field)
                            {   v_data = {};
                                var the_data = the_record.data;
                                if (Ext.isDate( the_data[the_field["name"]] ) )
                                {   v_data[ the_field["name"] ] = the_data[the_field["name"]].format("d/m/Y");  }
                                else
                                {   v_data[ the_field["name"]] = the_data[the_field["name"]]; };
                                v_json= Ext.apply( v_json, v_data);
                            });
                        json_data.push(v_json);
                    }, this);
                Ext.Ajax.request(
                {   method: 'POST',
                    url: '/asset/5/1',
                    headers:
                    {   'x-csrf-token': saa.assetITD.sid },
                    params:
                    {   json: Ext.encode(json_data),
                    },
                    success : function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : the_response.message+'\n Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   this.DataStore.reload();    };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    scope: this
                });
            };
        },
        // license grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: '/asset/5/2',
                headers:
                {   'x-csrf-token': saa.assetITD.sid },
                params:
                {   s:"form",
                    id  : this.Grid.getSelectionModel().selection.record.data.asset_id,
                    name: this.Grid.getSelectionModel().selection.record.data.name
                },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : the_response.message+'\n Server Message : '+'\n'+the_response.server_message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
        "copy_below" : function (button, event)
        {   the_button = button;
            the_event = event;
            the_grid = this.Grid;
            the_datastore = this.DataStore;
            the_xy = the_grid.getSelectionModel().getSelectedCell();
            the_field = the_grid.getColumnModel().getDataIndex(the_xy[1]);
            the_value = the_datastore.getAt(the_xy[0]).get(the_field);
            //convert date value into shortdate format
            // if (Ext.isDate(the_value) )
            // {   the_value = shortdateRenderer(the_value,'Y-m-d', 'd/m/Y');  };

            switch(the_field)
            {   case "os_type":
                case "os_license_type":
                case "office_type":
                case "office_license_type":
                case "av_key":
                case "diagram_type":
                case "diagram_license_type":
                {   saa.core.copy_below( the_button, the_event, the_datastore, the_xy, the_field);  };
                break;
                default :
                {   Ext.Msg.show(
                    {   title:'E R R O R ',
                        msg: 'Could not copied, Read Only Column',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                     });
                };
                break;
            };
        },
        // license search button
        license_search_handler : function(button, event)
        {   var the_search = true;
            console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_licenseSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };
            console.log(this.Searchs);
            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s    :"form",
                        task : saa.assetITD.task,
                        act  : saa.assetITD.act,
                        a    : 5, b:0,
                        limit:this.page_limit, start:this.page_start
                    });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
saa.assetITD.software = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Soft.Name", width : 250,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Software Name",
                },
                {   header: "Version", width : 200,
                    dataIndex : 'version', sortable: true,
                    tooltip:"Version",
                },
                {   header: "Installed", width : 200,
                    dataIndex : 'installed', sortable: true,
                    tooltip:"Installed",
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'}
            ]);
            this.Searchs = [
                {   id: 'software_asset_id',
                    cid: 'asset_id',
                    fieldLabel: 'Asset.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'software_asset_name',
                    cid: 'asset_name',
                    fieldLabel: 'Ast.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'software_name',
                    cid: 'software_name',
                    fieldLabel: 'Soft.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_softwareSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.software_search_handler,
                scope : this
            });
            this.DataStore = saa.core.newDataStore(
                '/asset/3/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                //columns: this.Columns,
                colModel: new Ext.ux.grid.LockingColumnModel(this.Columns),
                view: new Ext.ux.grid.LockingGridView(),
                stripeRows : true,
                columnLines: true,
                loadMask: true,
                height : saa.assetITD.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_softwareGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_softwarePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_softwarePageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_softwareGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });

            },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_softwareTab",
                jsId : tabId+"_softwareTab",
                title:  "Software",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // software search button
        software_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_softwareSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter, {s:"form", limit:this.page_limit, start:this.page_start});
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
saa.assetITD.asset_software = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Asset.ID", width : 150,
                    dataIndex : 'asset_id', sortable: true,
                    tooltip:"Asset.ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br><b>'+record.data.asset_name+'</b>';
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Name", width : 250,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Asset_software Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br><b>'+record.data.version+'</b>';
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "OCS.ID", width : 100,
                    dataIndex : 'id', sortable: true,
                    tooltip:"ID on OCS",
                },
                {   header: "Installed", width : 200,
                    dataIndex : 'installdate', sortable: true,
                    tooltip:"Installed",
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'}
            ]);
            this.Searchs = [
                {   id: 'asset_software_asset_id',
                    cid: 'asset_id',
                    fieldLabel: 'Asset.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'asset_software_asset_name',
                    cid: 'asset_name',
                    fieldLabel: 'Ast.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'asset_software_name',
                    cid: 'software_name',
                    fieldLabel: 'Soft.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'asset_software_os_type',
                    cid : 'os_type',
                    fieldLabel: 'OS.Type',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['LINUX'], ['WINDOWS'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'WINDOWS'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_asset_softwareSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.asset_software_search_handler,
                scope : this
            });

            this.DataStore = saa.core.newDataStore(
                '/asset/4/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : saa.assetITD.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_asset_softwareGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_asset_softwarePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_asset_softwarePageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_asset_softwareGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });

            },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_asset_softwareTab",
                jsId : tabId+"_asset_softwareTab",
                title:  "Asset_Softwares",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // asset_software search button
        asset_software_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_asset_softwareSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter, {s:"form", limit:this.page_limit, start:this.page_start});
                this.DataStore.reload();
            };
        },
        // getSearchParameter : function(search_items)
        // {   var the_parameter = {};
        //     Ext.each(search_items,
        //         function(item)
        //         {   v_param = {};
        //             if (Ext.isDate(Ext.get(item.id).getValue()) )
        //             {   v_param[ item.cid ] = Ext.get(item.id).getValue().format("d/m/Y");  }
        //             else //check additional transformation items
        //             {   switch (item.id)
        //                 {   case "company_cmb" :
        //                         v_param[ "company_id"] = Ext.get("company_id").getValue();
        //                     break;
        //                     default :
        //                         v_param[ item.cid ] = Ext.get(item.id).getValue();
        //                     break;
        //                 };
        //             };
        //             the_parameter = Ext.apply( the_parameter, v_param);
        //         }
        //     );
        //     return the_parameter;
        // },
    }; // end of public space
}(); // end of app
// create application
saa.assetITD.noncomputer = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.GridCombo;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;
    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = saa.assetITD.centerPanel.container.dom.clientHeight;
            this.AssetDS = saa.core.newDataStore(
                '/asset/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SiteDeptDS = saa.core.newDataStore(
                '/asset/0/11', true,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.SiteBuildingDS = saa.core.newDataStore(
                '/company/12/9', true,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.Columns = [
                {   header: "Status", width : 80,
                    dataIndex : 'status', sortable: true,
                    tooltip:"Remark",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['type'],
                            data : [ ['ACTIVE'], ['INACTIVE'], ['DISPOSE']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'type',
                        valueField: 'type',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == 'ACTIVE'){} else
                        {   metaData.attr = "style = background-color:red;"; };
                        return value;
                    },
                },
                {   header: "Asset.ID", width : 120,
                    dataIndex : 'asset_id', sortable: true,
                    tooltip:"Asset.ID",
                    css : "background-color: #56FF00;",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store: this.AssetDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'asset_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.asset_id = record.data.asset_id;
                                    combo.gridEditor.record.data.item_id = record.data.ref_item_id;
                                    record.data.status = 'ACTIVE';
                                    saa.assetITD.noncomputer.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( Ext.isEmpty(record.data.asset_id) || (record.data.status != 'ACTIVE') )
                        {   metaData.attr = "style = background-color:red;"; };
                        return value;
                    },
                },
                {   header: "Name", width : 200,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Hardware Name",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //result = value+'<br>'+record.data.item_name;
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Class", width : 80,
                    dataIndex : 'comp_type', sortable: true,
                    tooltip:"Type of Hardware",
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['type'],
                            data : [ ['PRINTER'], ['SCANNER'], ['ACCESS'], ['CCTV']]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'type',
                        valueField: 'type',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == "YES")
                        {   metaData.attr = "style = background-color:lime;"; };
                        return value;
                    },
                },
                {   header: "Remark", width : 250,
                    dataIndex : 'remark', sortable: true,
                    tooltip:"Remark",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //result = value+'<br>'+record.data.remark;
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Site", width : 100,
                    dataIndex : 'site_id', sortable: true,
                    tooltip:"Site"
                },
                {   header: "Dept.", width : 120,
                    dataIndex : 'dept_id', sortable: true,
                    tooltip:"Dept.Name",
                    editor: new Ext.form.ComboBox(
                    {   store: this.SiteDeptDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'dept_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.company_id = record.data.company_id;
                                    combo.gridEditor.record.data.site_id = record.data.site_id;
                                    combo.gridEditor.record.data.dept_id = record.data.dept_id;
                                    combo.gridEditor.record.data.dept_name = record.data.dept_name;
                                    saa.assetITD.noncomputer.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.dept_name+'<br>'+value;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Building", width : 100,
                    dataIndex : 'building_id', sortable: true,
                    tooltip:"Building ID",
                    editor: new Ext.form.ComboBox(
                    {   store: this.SiteBuildingDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'building_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.company_id = record.data.company_id;
                                    combo.gridEditor.record.data.site_id = record.data.site_id;
                                    combo.gridEditor.record.data.building_id = record.data.building_id;
                                    saa.assetITD.noncomputer.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.building_name+'<br>'+value;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Object Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"Object Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'menu_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'type', type: 'string'},
                {name: 'link', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'nc_asset_id',
                    cid : 'asset_id',
                    fieldLabel: 'Assets.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'nc_item_id',
                    cid:'item_id',
                    fieldLabel: 'Item.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'nc_name',
                    cid:'name',
                    fieldLabel: 'Hw.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'nc_type',
                    cid:'type',
                    fieldLabel: 'Class',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'nc_ip_address',
                    cid:'ip_address',
                    fieldLabel: 'IP.Addr',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'nc_status',
                    cid : 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['ACTIVE'], ['INACTIVE'], ['DISPOSE'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'ALL'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_ncSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.nc_search_handler,
                scope : this
            });
            /**
             * MAIN-GRID
            */
            this.DataStore = saa.core.newDataStore(
                '/asset/6/0', false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                colModel: new Ext.ux.grid.LockingColumnModel(this.Columns),
                view: new Ext.ux.grid.LockingGridView(),
                stripeRows : true,
                columnLines: true,
                loadMask: true,
                height : (this.region_height)-50,
                anchor: '100%',
                autoWidth  : true,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Copy Below',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.copy_below,
                        scope : this
                    },
                    {   id : tabId+'_ncSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    {   text:'Archive',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_hgBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_hpCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_hpCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_hgBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_ncTab",
                jsId : tabId+"_ncTab",
                title:  "Non-Computer",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'ToolBox',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //{   region: 'west',
                //    title: 'Roles',
                //    split: true,
                //    width: 200,
                //    minSize: 175,
                //    maxSize: 400,
                //    collapsible: true,
                //    margins: '0 0 0 5',
                //    items:[this.EastGrid]
                //},

                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // menu grid copy and make it a new record
        Grid_copy: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection)
            {   var data = this.Grid.getSelectionModel().selection.record.data;
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   id: "",
                        menu_id: data.menu_id,
                        name: data.name,
                        type: data.type,
                        link: data.link,
                        created_date : null,
                        last_update : null
                    }));
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },
        // noncomputer grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   //console.log("Data changes, Do you want to save it before reloads ? ") ;
                var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        // this.store is equal with saa.assetITD.noncomputer.DataStore
                        Ext.each(the_record.fields.items, //this.DataStore.reader.meta.fields,
                            function(the_field)
                            {   v_data = {};
                                var the_data = the_record.data;
                                if (Ext.isDate( the_data[the_field["name"]] ) )
                                {   v_data[ the_field["name"] ] = the_data[the_field["name"]].format("d/m/Y");  }
                                else
                                {   v_data[ the_field["name"]] = the_data[the_field["name"]]; };
                                v_json= Ext.apply( v_json, v_data);
                            });
                        json_data.push(v_json);
                    }, this);
                Ext.Ajax.request(
                {   method: 'POST',
                    url: '/asset/6/1',
                    headers:
                    {   'x-csrf-token': saa.assetITD.sid },
                    params:
                    {   json: Ext.encode(json_data),
                        btn : button.text
                    },
                    success : function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : the_response.message+'\n Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   this.DataStore.reload();    };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    scope: this
                });
            };
        },
        // noncomputer grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: '/asset/6/2',
                headers:
                {   'x-csrf-token': saa.assetITD.sid },
                params:
                {   s:"form",
                    id: this.Grid.getSelectionModel().selection.record.data.itd_id
                },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : the_response.message+'\n Server Message : '+'\n'+the_response.server_message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
        // noncomputer search button
        nc_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_ncSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        task: saa.assetITD.task,
                        act: saa.assetITD.act,
                        a:2, b:0,
                        limit: this.page_limit, start: this.page_start
                });
                this.DataStore.reload();
            };
        },
        "copy_below" : function (button, event)
        {   the_button = button;
            the_event = event;
            the_grid = this.Grid;
            the_datastore = this.DataStore;
            the_xy = the_grid.getSelectionModel().getSelectedCell();
            the_field = the_grid.getColumnModel().getDataIndex(the_xy[1]);
            the_value = the_datastore.getAt(the_xy[0]).get(the_field);
            //convert date value into shortdate format
            // if (Ext.isDate(the_value) )
            // {   the_value = shortdateRenderer(the_value,'Y-m-d', 'd/m/Y');  };
            switch(the_field)
            {   case "status":
                case "is_client":
                case "is_vm":
                case "comp_type":
                case "building_id":
                {   saa.core.copy_below( the_button, the_event, the_datastore, the_xy, the_field);  };
                break;
                case "dept_id":
                {   saa.core.copy_below( the_button, the_event, the_datastore, the_xy, the_field);
                    saa.core.copy_below( the_button, the_event, the_datastore, the_xy, "company_id");
                    saa.core.copy_below( the_button, the_event, the_datastore, the_xy, "site_id");
                };
                break;
                default :
                {   Ext.Msg.show(
                    {   title:'E R R O R ',
                        msg: 'Could not copied, Read Only Column',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                     });
                };
                break;
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
//Ext.reg('permission', saa);
Ext.onReady(saa.assetITD.initialize, saa.assetITD);
// end of file
</script>
<div>&nbsp;</div>