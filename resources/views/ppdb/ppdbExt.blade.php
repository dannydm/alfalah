<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Language" content="en-us" />
    <title>{{ config('app.name', 'ALFALAH') }}</title>
    <meta name="keywords" content="{{ config('app.name', 'ALFALAH') }}" />
    <meta name="description" content="{{ config('app.name', 'ALFALAH') }}" />
    <meta name="copyright" content="{{ config('app.name', 'ALFALAH') }}" />
    <!-- ** css stylesheet ** -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/resources/css/ext-all.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/resources/css/ext-all.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/shared/icons/silk.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/shared/examples.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/grid/grid-examples.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/ux/css/Portal.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/ux/css/LockingGridView.css') }}" />
    <!-- ** Javascript ** -->
    <!-- ExtJS library: base/adapter -->
    <script type="text/javascript" src="{{ asset('/js/ext3/adapter/ext/ext-base.js') }}"></script>
    <!-- ExtJS library: all widgets -->
    <script type="text/javascript" src="{{ asset('/js/ext3/ext-all.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/ext3/examples/ux/ux-all.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/ext3/examples/ux/LockingGridView.js') }}"></script>

    <!-- alfalah ppdb -->
    <script type="text/javascript">
    // create namespace
    Ext.namespace('alfalah', 'alfalah.ppdb');
    // create application
    alfalah.ppdb = function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.Tab;
        this.image_url;
        this.init_mode;
        this.target;
        this.action;
        this.Grid;
        this.Columns;
        this.Records;
        this.DataStore;
        this.Searchs;
        this.menuPanel;
        this.viewport;

        // private functions
        //**********************
        // public space
        //**********************
        return {
            // execute at the very last time
            // public properties, e.g. strings to translate
            // public methods
            server_date: new Date().format('Y'),
            start_date : new Date(this.server_date, 0, 1),
            end_date   : new Date(this.server_date, 11, 31),
            page_limit : 75,
            page_start : 0,
            sid        : '{{ csrf_token() }}',
            "setParameter" : function(Ajax, Image, Mode)
            {   //this.ajax_url =  Ajax;
                image_url = Image;
                init_mode= Mode;
            },
            "logout" : function()
            {   window.location = "{{ url('/logout') }}";
            },
            "prepare_component" : function()
            {   // NOTE: This is an example showing simple state management. During development,
                // it is generally best to disable state management as dynamically-generated ids
                // can change across page loads, leading to unpredictable results.  The developer
                // should ensure that stable state ids are set for stateful components in real apps.
                Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
                // Ext.BLANK_IMAGE_URL = '/newalfalah/frontend/web/js/ext3/resources/images/default/s.gif ';
                this.start_date = new Date(this.server_date, 0, 1);
                this.end_date = new Date(this.server_date, 11, 31);

                this.SaveBtn = new Ext.Button(
                {   id : "_SaveBtn",
                    fieldLabel: '',
                    text:'<strong> S I M P A N   D A T A</strong>',
                    tooltip:'Simpan data',
                    iconCls: 'silk-disk',
                    xtype: 'button',
                    width : 120,
                    handler : this.Form_save,
                    scope : this,
                }).render(document.body, 'SaveBtn');

                this.FormBtn = new Ext.Button(
                {   id : "_FormBtn",
                    fieldLabel: '',
                    text:'<strong>  I S I  --  F O R M U L I R</strong>',
                    tooltip:'Form Pendaftaran',
                    iconCls: 'silk-page-edit',
                    xtype: 'button',
                    width : 120,
                    handler : function(){
                        Ext.getCmp('center_panel').setActiveTab(2);
                    },
                    scope : this,

                }).render(document.body, 'FormBtn');

                this.LoginBtn = new Ext.Button(
                {   id : "_LoginBtn",
                    fieldLabel: '',
                    text:'<strong> L O G I N</strong>',
                    tooltip:'Login reistered user',
                    iconCls: 'silk-key',
                    xtype: 'button',
                    width : 120,
                    handler : this.Form_login,
                    scope : this,
                })

                this.DepartmentDS = this.newDataStore(
                    "{{ url('/ppdb/0/9') }}", false,
                    {   s:"form", limit:this.page_limit, start:this.page_start,
                        type:"Division", order:"ppdb_seq"}
                );
                this.DepartmentDS.load();

                this.AgeLimitDS = this.newDataStore(
                    "{{ url('/ppdb/0/3') }}", false,
                    {   s:"form", limit:this.page_limit, start:this.page_start, param_name:'ppdb_umur_'}
                );
                this.AgeLimitDS.load();
                /************************************
                    F O R M S
                ************************************/
                this.Form = new Ext.FormPanel(
                {   id : 'ppdb_form',
                    bodyStyle:'padding:5px',
                    // autoHeight : true,
                    // autoScroll  : true,
                    // title : 'TITLE',
                    // tbar: [],
                    // border:false,
                    items: [
                    // INFORMASI PENDAFTAR
                    {   layout:'column',
                        border:false,
                        items:[
                        {   columnWidth:.3,  // hidden columns
                            layout: 'form',
                            border:false,
                            items: [
                            {   id : 'ppdb_key_id',
                                xtype:'textfield',
                                fieldLabel: 'Key.ID',
                                name: 'keyid',
                                anchor:'95%',
                                allowBlank: true,
                                readOnly : true,
                                value : '',
                                hidden : true,
                            },
                            {   id : 'ppdb_created_date',
                                xtype:'textfield',
                                fieldLabel: 'Created Date',
                                name: 'created_date',
                                anchor:'95%',
                                allowBlank: true,
                                readOnly : true,
                                hidden : true,
                            },
                            {   id : 'ppdb_tahunajaran_id',
                                xtype:'textfield',
                                fieldLabel: 'Tahun',
                                name: 'tahunajaran_id',
                                anchor:'95%',
                                allowBlank: false,
                                readOnly : true,
                                hidden : true,
                                value : '{{ $TAHUNAJARAN_ID }}'
                            },]
                        },
                        {   columnWidth:.5,
                            layout: 'form',
                            border:false,
                            labelWidth: 125,
                            labelPad: 10,
                            items: [
                            {
                                xtype: 'fieldset',
                                title: 'Informasi Calon Siswa ',
                                autoHeight: true,
                                anchor:'95%',
                                // defaultType: 'checkbox', // each item will be a checkbox
                                items: [
                                {   id : 'ppdb_jenjang_sekolah_id',
                                    xtype : 'combo',
                                    fieldLabel: 'Jenjang',
                                    labelSeparator : '',
                                    name: 'jenjang_sekolah_id',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store: this.DepartmentDS,
                                    displayField: 'name',
                                    valueField: 'dept_id',
                                    mode : 'local',
                                    forceSelection: true,
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    typeAhead: true,
                                    width : 100,
                                    value: 'Pilihlah',
                                    listeners: { scope : this,
                                        'select': function (view, selection, options) {
                                            Ext.getCmp('ppdb_tanggal_lahir').setValue(null);
                                            Ext.getCmp('_SaveBtn').hide();

                                            // console.log(view);
                                            // console.log(selection.data.abreviation.toLowerCase());
                                            // console.log(options);

                                            // console.log('selection.data.ppdb_seq');
                                            // console.log(selection.data.ppdb_seq);

                                            alfalah.ppdb.ageLimit  = null;
                                            alfalah.ppdb.dateLimit = null;

                                            switch (selection.data.ppdb_seq)
                                            {   //kp
                                                case '1' : alfalah.ppdb.target = "ppdb_umur_kp"; break;
                                                //kb
                                                case '2' : alfalah.ppdb.target = "ppdb_umur_kb"; break;
                                                //tk
                                                case '3' : alfalah.ppdb.target = "ppdb_umur_tk"; break;
                                                // sd & icp
                                                case '4' :
                                                case '5' :
                                                {        alfalah.ppdb.target = "ppdb_umur_sd"; }; break;
                                                // smp
						case '6' : 
						{	alfalah.ppdb.target = "ppdb_umur_smp"; 
							Ext.getCmp('_SaveBtn').show();
						};
						break;
                                            };

                                            Ext.each(this.AgeLimitDS.data.items,
                                                function(record)
                                                {   if ( record.data.param_name == alfalah.ppdb.target)
                                                    {   console.log(record);
                                                        console.log("set limit");
                                                        alfalah.ppdb.ageLimit  = Date.parseDate(record.data.age_limit.substr(0,19), "Y-m-d H:i:s", true);

                                                        alfalah.ppdb.target = record.data.description.split('/');

                                                        alfalah.ppdb.dateLimit = new Date(
                                                                alfalah.ppdb.target[2],
                                                                alfalah.ppdb.target[1] - 1,
                                                                alfalah.ppdb.target[0]
                                                            );
                                                        alfalah.ppdb.target = record.data.param_value;
                                                        console.log(alfalah.ppdb.dateLimit);
                                                        // consolo.log(new Date() );
                                                    };
                                                }
                                            );
                                        }
                                    }
                                },
                                {   id : 'ppdb_nama_lengkap',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Nama.Lengkap',
                                    name: 'nama_lengkap',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_nama_panggilan',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Nama.Panggilan',
                                    name: 'nama_panggilan',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_alamat',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Alamat.Rumah',
                                    name: 'alamat_rumah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_kecamatan',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Kecamatan',
                                    name: 'kecamatan',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_kota',
                                    xtype:'textfield',
                                    fieldLabel: 'Kota',
                                    style: "textTransform: uppercase ;",
                                    name: 'kota',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_telepon_rumah',
                                    xtype:'textfield',
                                    fieldLabel: 'Telp.Rumah',
                                    style: "textTransform: uppercase ;",
                                    name: 'telp_rumah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id: 'ppdb_gender_name',
                                    xtype : 'combo',
                                    fieldLabel: 'L / P',
                                    name: 'gender_name',
                                    labelSeparator : '',
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Laki-Laki'], ['Perempuan']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value : 'Laki-Laki'
                                },
                                {   id : 'ppdb_tempat_lahir',
                                    xtype:'textfield',
                                    fieldLabel: 'Tempat Lahir',
                                    style: "textTransform: uppercase ;",
                                    name: 'tempat_lahir',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_tanggal_lahir',
                                    xtype:'datefield',
                                    fieldLabel: 'Tanggal.Lahir (dd/mm/yyyy)',
                                    name: 'tanggal_lahir',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : '',
                                    format: 'd/m/Y',
                                    listeners: { scope : this,
                                        'blur': function (view, selection, options)
                                        {
                                            var umur_siswa = alfalah.ppdb.getAge(
                                                    alfalah.ppdb.dateLimit,
                                                    Ext.getCmp('ppdb_tanggal_lahir').getValue()
                                            );

                                            console.log('tanggal_lahir');
                                            console.log(view);
                                            // console.log(selection);

                                            console.log(Ext.getCmp('ppdb_tanggal_lahir').getValue());

                                            console.log(Ext.isDate(alfalah.ppdb.ageLimit));
                                            console.log(alfalah.ppdb.ageLimit);
                                            // console.log('dikurangi');
                                            // console.log(Ext.getCmp('ppdb_tanggal_lahir').getValue() - alfalah.ppdb.ageLimit);

                                            console.log(alfalah.ppdb.dateLimit);

                                            // if (selection < alfalah.ppdb.ageLimit)
                                            // {
                                            //     console.log("lebih kecil");
                                            // } else console.log("salah");
                                            console.log("umur siswa = ");
                                            console.log(umur_siswa );
                                            if (    umur_siswa >= alfalah.ppdb.target )
                                            {
                                                console.log("lebih besar");
                                                Ext.getCmp('_SaveBtn').show();
                                            } else
                                            {   console.log("lebih kecil");
                                                console.log("tanggal lahir siswa harus sebelum atau sama dengan tanggal "+ alfalah.ppdb.ageLimit.format('d/m/Y') );
                                                Ext.getCmp('ppdb_tanggal_lahir').setValue(null);
                                                Ext.Msg.show(
                                                    {   title:'E R R O R ',
                                                        minWidth : 350,
                                                        msg:'Umur Anak = '+umur_siswa+' th.<BR>'+
                                                            'Umur Anak Belum Cukup, Hubungi Admin',
                                                        buttons: Ext.Msg.OK,
                                                        icon: Ext.MessageBox.ERROR
                                                    });
                                            };


                                            // var date1 = new Date("7/13/2010");
                                            // var date2 = new Date("12/15/2010");
                                            // var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                                            // var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                                            // alert(diffDays);

                                            // console.log('option');
                                            // console.log(view);

                                            // console.log(this.ageLimit);
                                            // console.log(this.dateLimit);


                                            // var month_rule = new Date();
                                            // month_rule.setMonth(6, 30);

                                            // var minutes = 1000 * 60 ;
                                            // var hours = minutes * 60;
                                            // var days = hours * 24;
                                            // var years = days * 365;

                                            // var get_lahir = Ext.getCmp('ppdb_tanggal_lahir').getValue();
                                            // get_lahir.getMonth();
                                            // var get_syarat = Date.parse(month_rule) - Date.parse(get_lahir);
                                            // var get_syarat_count = Math.round(get_syarat / years);
                                            // var jenjang_sekolah = Ext.getCmp('ppdb_jenjang_sekolah_id').getValue();

                                            // console.log(jenjang_sekolah);
                                            // // smp
                                            // if ( jenjang_sekolah == 2 ){
                                            //     // var   smp_rules_count = 12;
                                            //     // if (get_syarat_count < smp_rules_count) {
                                            //     // Ext.Msg.show(
                                            //     //             {
                                            //     //                 title:'E R R O R ',
                                            //     //                 minWidth : 350,
                                            //     //                 msg: 'Umur Anak Belum Cukup, Hubungi Admin',
                                            //     //                 buttons: Ext.Msg.OK,
                                            //     //                 icon: Ext.MessageBox.ERROR
                                            //     //             });
                                            //     //         } else
                                            //     //     {
                                            //     // };

                                            // }
                                            // else if (jenjang_sekolah == 1) {
                                            //     var   sd_rules_count = 6 ;
                                            //     if (get_syarat_count < sd_rules_count) {
                                            //     Ext.Msg.show(
                                            //                 {   title:'E R R O R ',
                                            //                     minWidth : 350,
                                            //                     msg: 'Umur Anak Belum Cukup, Hubungi Admin',
                                            //                     buttons: Ext.Msg.OK,
                                            //                     icon: Ext.MessageBox.ERROR
                                            //                 });
                                            //             } else
                                            //         {
                                            //     };
                                            // }
                                            // else if (jenjang_sekolah == 4) {
                                            //     var   sd_rules_count = 6 ;
                                            //     if (get_syarat_count < sd_rules_count) {
                                            //     Ext.Msg.show(
                                            //                 {   title:'E R R O R ',
                                            //                     minWidth : 350,
                                            //                     msg: 'Umur Anak Belum Cukup, Hubungi Admin',
                                            //                     buttons: Ext.Msg.OK,
                                            //                     icon: Ext.MessageBox.ERROR
                                            //                 });
                                            //             } else
                                            //         {
                                            //     };
                                            // }
                                            // else if (jenjang_sekolah == 6) {
                                            //     var   pra_rules_count = 3 ;
                                            //     if (get_syarat_count < pra_rules_count) {
                                            //     Ext.Msg.show(
                                            //                 {   title:'E R R O R ',
                                            //                     minWidth : 350,
                                            //                     msg: 'Umur Anak Belum Cukup, Hubungi Admin',
                                            //                     buttons: Ext.Msg.OK,
                                            //                     icon: Ext.MessageBox.ERROR
                                            //                 });
                                            //             } else
                                            //         {
                                            //     };
                                            // }
                                        }
                                    }
                                },
                                {   id : 'ppdb_warganegara',
                                    xtype : 'combo',
                                    fieldLabel: 'Warga negara',
                                    labelSeparator : '',
                                    name: 'warganegara',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Indonesia'], ['Asing']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Indonesia'
                                },
                                {   id : 'ppdb_agama',
                                    xtype : 'combo',
                                    fieldLabel: 'Agama',
                                    labelSeparator : '',
                                    name: 'agama',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Islam'], ['Kristen'],['Katolik'], ['Hindu'], ['Budha'], ['KongHucu']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Islam'
                                },
                                {   id : 'ppdb_ibu_kandung',
                                    xtype:'textfield',
                                    fieldLabel: 'Nama.Ibu.Kandung',
                                    style: "textTransform: uppercase ;",
                                    name: 'ibu_kandung',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                ]
                            },
                            ]
                        },
                        {   columnWidth:.5,
                            layout: 'form',
                            border:false,
                            items: [
                            {   border:false,
                                contentEl: 'simpan_content'
                            }]
                        }]
                    }]
                });
                this.LoginForm = new Ext.FormPanel(
                {   bodyStyle:'padding:5px',
                    items: [
                    {   layout:'column',
                        border:false,
                        items:[
                        {   columnWidth:.5,
                            layout: 'form',
                            border:false,
                            items: [
                            {   border:false,
                                contentEl: 'ppdb_content',  }
                            ]
                        },
                        {   columnWidth:.5,
                            layout: 'form',
                            border: true,
                            items: [
                            {   border:false,
                                contentEl: 'login_content',  },
                            {
                                xtype: 'fieldset',
                                border:false,
                                anchor:'95%',
                                items: [
                                {   id : 'ppdblogin_no_registration',
                                    xtype:'textfield',
                                    fieldLabel: 'No.Registrasi',
                                    name: 'no_registration',
                                    anchor:'95%',
                                    allowBlank: false,
                                    value : ''
                                },
                                {   id : 'ppdblogin_nama_lengkap',
                                    xtype:'textfield',
                                    fieldLabel: 'Nama.Lengkap',
                                    style: "textTransform: uppercase ;",
                                    name: 'nama_lengkap',
                                    anchor:'95%',
                                    allowBlank: false,
                                    value : ''
                                },
                                {   id : 'ppdblogin_tanggal_lahir',
                                    xtype:'datefield',
                                    fieldLabel: 'Tanggal.Lahir (D/M/Y)',
                                    name: 'tanggal_lahir',
                                    anchor:'95%',
                                    allowBlank: false,
                                    value : '',
                                    format: 'd/m/Y',
                                },
                                {   id : 'ppdblogin_ibu_kandung',
                                    style: "textTransform: uppercase ;",
                                    xtype:'textfield',
                                    fieldLabel: 'Ibu.Kandung',
                                    name: 'ibu_kandung',
                                    anchor:'95%',
                                    allowBlank: false,
                                    value : ''
                                },
                                this.LoginBtn]
                            }]
                        }]
                    }]
                });
            },
            "build_layout": function()
            {   this.viewport = new Ext.Viewport(
                {   id: 'viewport',
                    jsId: 'viewport',
                    layout:'border',
                    items:[
                    {   region:'north',
                        id:'north_panel',
                        jsId:'north_panel',
                        height : 100,
                        width: 200,
                        minSize: 200,
                        maxSize: 300,
                        layout : 'fit',
                        items: [{
                            contentEl: 'north_content',
                        },]
                    },
                    {   region: 'center',
                        id:'center_panel',
                        jsId:'center_panel',
                        xtype: 'tabpanel', // TabPanel itself has no title
                        // autoHeight:true,
                        // autoScroll:true,
                        enableTabScroll:true,
                        defaults:{autoScroll:true},
                        activeTab: 0,
                        items: [
                            {   // id:'ppdbTab',
                                // jsId:'ppdbTab',
                                title : 'PERSYARATAN',
                                iconCls: 'silk-folder-table',
                                region: 'center',
                                closable : false,
                                deferredRender : false,
                                preventBodyReset: true,
                                items: [{
                                    contentEl: 'persyaratan_content',
                                },]
                            },
                            {   // id:'ppdbTab',
                                // jsId:'ppdbTab',
                                title : 'FORMULIR PPDB ONLINE',
                                iconCls: 'silk-page-edit',
                                region: 'center',
                                closable : false,
                                deferredRender : false,
                                preventBodyReset: true,
                                // autoHeight:true,
                                // autoScroll:true,
                                width:500,
                                height:600,
                                items: [ this.Form ]
                            },
                            {   // id:'ppdbTab',
                                // jsId:'ppdbTab',
                                title : 'L O G I N',
                                iconCls: 'silk-book-open',
                                region: 'center',
                                closable : false,
                                deferredRender : false,
                                preventBodyReset: true,
                                items: [
                                // {   contentEl: 'ppdb_content',  },
                                {   region: 'center',
                                    xtype: 'panel', // TabPanel itself has no title
                                    // autoHeight:true,
                                    // autoScroll:true,
                                    enableTabScroll:true,
                                    defaults:{autoScroll:true},
                                    activeTab: 0,
                                    items: [
                                        this.LoginForm
                                    ]
                                }]
                            },
                        ]
                    }]
                });
            },
            "finalize_comp_and_layout" : function()
            {   // overide TextField Validation to exclude empty string
                Ext.override(Ext.form.TextField, {
                    validator:function(text){
                        return (text.length==0 || Ext.util.Format.trim(text).length!=0);
                    }
                });
            },
            "initialize": function()
            {   Ext.QuickTips.init();
                this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // combo grid column renderer
            comboRenderer : function(combo, value)
            {   if (Ext.isEmpty(value)) { return "";    }
                else
                {   var record = combo.findRecord(combo.valueField, value);
                    return record ? record.get(combo.displayField) : combo.valueNotFoundText;
                };
            },
            // date grid column renderer
            dateRenderer : function(value)
            {   if (Ext.isEmpty(value))
                {   result = "";    }
                else
                {   the_value = Date.parseDate(value.substr(0,19), "Y-m-d H:i:s", true);
                    result = the_value.format('d/m/Y H:i:s');
                };
                return result;
            },
            shortdateRenderer : function(value, sourceFormat, targetFormat)
            {   if (Ext.isEmpty(value))
                {   result = "";    }
                else
                {   result = Date.parseDate(value.substr(0,10), sourceFormat, true).format(targetFormat); };
                return result;
            },
            printButton: function(button, event, DataStore)
            {   document.location.href = DataStore.proxy.url+'?'+Ext.urlEncode(DataStore.baseParams)+'&pt='+button.print_type;
            },
            printOut: function(button, event, the_url, the_parameter)
            {   document.location.href = the_url+'?'+Ext.urlEncode(the_parameter)+'&pt='+button.print_type;
            },
            gridColumnWrap : function(data)
            {   return '<div style="white-space:normal !important;">'+ data +'</div>';
            },
            copy_below : function (button, event, the_datastore, the_xy, the_field)
            {   //copy below
                the_record_count = the_datastore.getCount();
                for (var the_x = the_xy[0]+1; the_x < the_record_count; the_x++)
                {   a = the_datastore.getAt(the_xy[0]).get(the_field);
                    the_datastore.getAt(the_x).set(the_field, a);
                };
            },
            getCookie : function(value)
            {   cName = "";
                pCOOKIES = new Array();
                pCOOKIES = document.cookie.split('; ');
                for(bb = 0; bb < pCOOKIES.length; bb++)
                {   NmeVal  = new Array();
                    NmeVal  = pCOOKIES[bb].split('=');
                    if(NmeVal[0] == value)
                    {   cName = unescape(NmeVal[1]);    };
                }
                return cName;
            },
            newDataStore : function(new_url, is_remote, new_base_param )
            {   var newDS =  new Ext.data.Store(
                {   proxy:  new Ext.data.HttpProxy(
                    {   method: 'GET',
                        url: new_url,
                    }),
                    baseParams: new_base_param,
                    reader: new Ext.data.JsonReader(),
                    remoteFilter : is_remote
                });

                return newDS;
            },
            getSearchParameter : function(search_items)
            {   var the_parameter = {};
                Ext.each(search_items,
                    function(item)
                    {   v_param = {};
                        // console.log(item);
                        if (Ext.isDate(Ext.get(item.id).getValue()) )
                        {   v_param[ item.cid ] = Ext.get(item.id).getValue().format("d/m/Y");  }
                        else //check additional transformation items
                        {   switch (item.xtype)
                            {   case "combo" :
                                    v_param[ item.cid] = Ext.getCmp(item.id).getValue();
                                break;
                                default :
                                    v_param[ item.cid ] = Ext.getCmp(item.id).getValue();
                                break;
                            };
                        };
                        the_parameter = Ext.apply( the_parameter, v_param);
                    }
                );
                return the_parameter;
            },
            getHeadData : function(the_fields)
            {   var head_data = [];
                var v_json = {};
                Ext.each(the_fields,
                    function(the_field)
                    {   console.log(the_field);
                        // a = Ext.getCmp(the_field);
                        if (Ext.isDate(the_field.getValue()) )
                        {   v_json[the_field.name] = the_field.getValue().format("d/m/Y");  }
                        else if ( the_field.xtype == "combo") // is_combo_box
                        {   if (the_field.lastSelectionText == "Pilihlah")
                            {   v_json[the_field.name] = "";  }
                            else
                            {   //v_json[the_field.name] = the_field.lastSelectionText;
                                v_json[the_field.name] = the_field.getValue();
                            }
                        }
                        else
                        {   v_json[the_field.name] = the_field.getValue();  };

                    });
                head_data.push(v_json);
                return head_data;
            },
            getDetailData : function(the_records)
            {   var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        if (Ext.isEmpty(the_record.store)) {} // deleted record
                        else
                        {   Ext.iterate(the_record.data, function(key, value)
                            {   v_data = {};
                                if (Ext.isDate( value ) )
                                {   v_data[ key ] = value.format("d/m/Y");  }
                                else
                                {   v_data[ key ] = value;  };
                                v_json= Ext.apply( v_json, v_data);
                            });
                            json_data.push(v_json);
                        };
                    });
                return json_data;
            },
            validateFields : function(the_fields)
            {   //console.log('validateFields');
                var is_validate = true;
                Ext.each( the_fields,
                    function(the_field)
                    {   a = Ext.getCmp(the_field);
                        b = a.getValue();
                        if (Ext.isEmpty(b) ) { is_validate = false;};

                        if ( (a.validate() == false) || (is_validate == false) )
                        {   is_validate = false;
                            Ext.Msg.show(
                            {   title:'E R R O R ',
                                msg: 'Column '+a.fieldLabel+' is mandatory.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                            return false;
                        };
                    });
                return is_validate;
            },
            validateGridQuantity : function(the_records, the_fields)
            {   var is_validate = true;
                Ext.each(the_records,
                    function(the_record)
                    {   Ext.iterate(the_record.data, function(key, value)
                        {   Ext.each(the_fields,
                                function(the_field)
                                {   if ( (the_field == key) && (value ==0) )
                                    {   is_validate = false;
                                        Ext.Msg.show(
                                        {   title   :'E R R O R ',
                                            msg     : 'Found zero quantity <br>on Column '+key+
                                                      '<br>of items <br>'+the_record.data.item_name,
                                            buttons : Ext.Msg.OK,
                                            icon    : Ext.MessageBox.ERROR,
                                            width   : 200,
                                        });
                                        return false;  // break looping
                                    };
                                });
                        });
                    });
                return is_validate;
            },
            jsonGet : function( the_url, the_headers, the_parameter, the_success)
            {   Ext.Ajax.request(
                {   method: 'GET',
                    url: the_url,
                    headers: the_headers,
                    params: the_parameter,
                    success: the_success,
                    failure: function()
                    {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                });
            },
            jsonSave : function(the_url, the_params, the_datastore, the_scope)
            {   Ext.Ajax.request(
                {   method: 'POST',
                    url: the_url,
                    params  : the_params,
                    success : function(response, options)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   the_datastore.reload();    };
                    },
                    failure: function(response, options)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'S E R V E R    E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        };
                    },
                    scope: the_scope
                });
            },
            submitGrid : function( the_datastore, the_url, the_headers, the_parameter)
            {   Ext.Ajax.request(
                {   method: 'POST',
                    url: the_url,
                    headers: the_headers,
                    params: the_parameter,
                    success: function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   the_datastore.reload(); };
                    },
                    failure: function()
                    {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    // scope: this
                });
            },
            submitForm : function( the_form_name, the_url, the_headers, the_parameter)
            {   var the_form = Ext.getCmp(the_form_name);
                Ext.Ajax.request(
                {   method: 'POST',
                    url: the_url,
                    headers: the_headers,
                    params: the_parameter,
                    success: function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   //the_form.destroy();
                            the_url = "{{ url('/ppdb/0/2') }}"+'?'+'&id='+the_response.server_message;
                            window.location = the_url;
                        };
                    },
                    failure: function()
                    {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    // scope: this
                });
            },
            Form_save : function(button, event)
            {   var head_data = [];
                var json_data = [];
                var modi_data = [];
                var v_json = {};
                the_ppdb = alfalah.ppdb;

                // header validation
                if (the_ppdb.validateFields([
                    'ppdb_nama_lengkap']))
                {
                    data_siswa = the_ppdb.getHeadData(the_ppdb.Form.getForm().items.items);
                    // data_parent = the_ppdb.getHeadData(the_ppdb.ParentsForm.getForm().items.items);

                    // head_data = Ext.apply( data_siswa[0], data_parent[0] );
                    // submit data
                    the_ppdb.submitForm(
                        'ppdb_form',
                        "{{ url('/ppdb/0/1') }}",
                        {   'x-csrf-token': the_ppdb.sid },
                        {   task: 'save',
                            head : Ext.encode(data_siswa[0]),
                            json : {},
                        });
                };
            },
            // forms Login
            Form_login : function(button, event)
            {   var head_data = [];
                var json_data = [];
                var modi_data = [];
                var v_json = {};
                the_ppdb = alfalah.ppdb;

                // header validation
                if (the_ppdb.validateFields([
                    'ppdblogin_no_registration', 'ppdblogin_nama_lengkap', 'ppdblogin_tanggal_lahir', 'ppdblogin_ibu_kandung']))
                {
                    head_data = the_ppdb.getHeadData(the_ppdb.LoginForm.getForm().items.items);
                    // var the_form = Ext.getCmp(the_form_name);
                    Ext.Ajax.request(
                    {   method      : 'POST',
                        url         : "{{ url('/ppdb/1/1') }}", // login registerd ppdb
                        headers     : {   'x-csrf-token': the_ppdb.sid },
                        params      : {   task: 'login',
                            head    : Ext.encode(head_data),
                            json    : {},
                        },
                        success: function(response)
                        {   var the_response = Ext.decode(response.responseText);
                            if (the_response.success == false)
                            {   Ext.Msg.show(
                                {   title :'E R R O R ',
                                    msg : '<big>'+the_response.message+'<BR><BR>'+the_response.server_message+'</big>',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR,
                                    width : 250
                                 });
                            }
                            else
                            {
                                console.log(response);
                                the_url = "{{ url('/ppdb/1/0') }}"+'?'+'&id='+the_response.server_message;
                                console.log(the_url);
                                window.location = the_url;
                            };
                        },
                        failure: function()
                        {   Ext.Msg.alert("Login Failed : ", "Server communication failure");
                        },
                        // scope: this
                    });

                };
            },

            getAge : function(target,  birthDate)
            {
                // var target = new Date();
                // var birthDate = new Date(dateString);
                var age = target.getFullYear() - birthDate.getFullYear();
                var m = target.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && target.getDate() < birthDate.getDate()))
                {
                    age--;
                }
                return age;
            },


        }; // end of public space
    }(); // end of app
    Ext.onReady(alfalah.ppdb.initialize, alfalah.ppdb);
    </script>
</head>
<body>
    <div id="north_content" class="x-hide-display">
        <div align="center">
            <br>
            <h1><big>PPDB ONLINE {{ $TAHUNAJARAN }}</big></h1>
            <h1><big>LEMBAGA PENDIDIKAN AL FALAH DARUSSALAM TROPODO</big></h1>
      </div>
    </div>
    <div id="ppdb_content" class="x-hide-display">
        <!-- <div align="left">
            <ol><h1><big>Tata Cara Pendaftaran</big></h1></ol>
            <ol>
                <li>Melengkapi Persyaratan</li><br>
                <li>Mengisi Formulir Pendaftaran <div id="FormBtn" style="margin:15px 0;"></div></li>
                <li>Melakukan Pembayaran</li><br>
                <li>Konfirmasi Pembayaran</li><br>
                <li>Dapatkan No Registrasi Test</li><br>
                <li>Ikuti Test Masuk</li><br>
            </ol>
        </div> -->
    </div>
    <div id="login_content" class="x-hide-display">
        <div align="left">
            <ol><h1><big>Sudah Registrasi ? Login Disini</big></h1></ol>
        </div>
    </div>
    <div id="persyaratan_content" class="x-hide-display">
        <div align="left">
            {!! $PRASYARAT !!}
        </div>
    </div>
    <div id="simpan_content" class="x-hide-display">
        <div align="left">
            <ol><h1><big>PASTIKAN DATA ANDA SUDAH BENAR !!!</big></h1></ol>
            <ol>
                <li>Semua data sudah terisi</li><br>
                <li>Tidak ada data yang kosong</li><br>
                <li>Semua pilihan sudah terisi</li><br>
                <li>Jika sudah benar, silahkan menekan tombol simpan dibawah ini
                    <br><div id="SaveBtn" style="margin:15px 0;"></div>
                </li>
                <br>
            </ol>
        </div>
    </div>
</body>
</html>
