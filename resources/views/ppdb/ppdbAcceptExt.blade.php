<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Language" content="en-us" />
    <title>{{ config('app.name', 'ALFALAH') }}</title>
    <meta name="keywords" content="{{ config('app.name', 'ALFALAH') }}" />
    <meta name="description" content="{{ config('app.name', 'ALFALAH') }}" />
    <meta name="copyright" content="{{ config('app.name', 'ALFALAH') }}" />
    <!-- ** css stylesheet ** -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/resources/css/ext-all.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/resources/css/ext-all.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/shared/icons/silk.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/shared/examples.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/grid/grid-examples.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/ux/css/Portal.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/ux/css/LockingGridView.css') }}" />
    <!-- ** Javascript ** -->
    <!-- ExtJS library: base/adapter -->
    <script type="text/javascript" src="{{ asset('/js/ext3/adapter/ext/ext-base.js') }}"></script>
    <!-- ExtJS library: all widgets -->
    <script type="text/javascript" src="{{ asset('/js/ext3/ext-all.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/ext3/examples/ux/ux-all.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/ext3/examples/ux/LockingGridView.js') }}"></script>

    <!-- alfalah ppdb -->
    <script type="text/javascript">
    // create namespace
    Ext.namespace('alfalah', 'alfalah.ppdb');
    // create application
    alfalah.ppdb = function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.Tab;
        this.image_url;
        this.init_mode;
        this.target;
        this.action;
        this.Grid;
        this.Columns;
        this.Records;
        this.DataStore;
        this.Searchs;
        this.menuPanel;
        this.viewport;

        // private functions
        //**********************
        // public space
        //**********************
        return {
            // execute at the very last time
            // public properties, e.g. strings to translate
            // public methods
            server_date: new Date().format('Y'),
            start_date : new Date(this.server_date, 0, 1),
            end_date   : new Date(this.server_date, 11, 31),
            page_limit : 75,
            page_start : 0,
            sid        : '{{ csrf_token() }}',
            "setParameter" : function(Ajax, Image, Mode)
            {   //this.ajax_url =  Ajax;
                image_url = Image;
                init_mode= Mode;
            },
            "logout" : function()
            {   window.location = "{{ url('/logout') }}";
            },
            "prepare_component" : function()
            {   // NOTE: This is an example showing simple state management. During development,
                // it is generally best to disable state management as dynamically-generated ids
                // can change across page loads, leading to unpredictable results.  The developer
                // should ensure that stable state ids are set for stateful components in real apps.
                Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
                // Ext.BLANK_IMAGE_URL = '/newalfalah/frontend/web/js/ext3/resources/images/default/s.gif ';
                this.start_date = new Date(this.server_date, 0, 1);
                this.end_date = new Date(this.server_date, 11, 31);

                this.CloseBtn = new Ext.Button(
                {   id : "_CloseBtn",
                    fieldLabel: '',
                    text:'<strong> K E M B A L I</strong>',
                    tooltip:'Close',
                    iconCls: 'silk-arrow-undo',
                    xtype: 'button',
                    width : 120,
                    handler : function(){
                        window.location = "{{ url('/ppdb/0/0') }}";
                    },
                    scope : this,

                }).render(document.body, 'CloseBtn');
            },
            "build_layout": function()
            {   this.viewport = new Ext.Viewport(
                {   id: 'viewport',
                    jsId: 'viewport',
                    layout:'border',
                    items:[
                    {   region:'north',
                        id:'north_panel',
                        jsId:'north_panel',
                        height : 100,
                        width: 200,
                        minSize: 200,
                        maxSize: 300,
                        layout : 'fit',
                        items: [{
                            contentEl: 'north_content',
                        },]
                    },
                    {   region: 'center',
                        id:'center_panel',
                        jsId:'center_panel',
                        xtype: 'tabpanel', // TabPanel itself has no title
                        // autoHeight:true,
                        // autoScroll:true,
                        enableTabScroll:true,
                        defaults:{autoScroll:true},
                        activeTab: 0,
                        items: [
                            {   // id:'ppdbTab',
                                // jsId:'ppdbTab',
                                title : 'ACCEPTANCE',
                                iconCls: 'silk-book-open',
                                region: 'center',
                                closable : false,
                                deferredRender : false,
                                preventBodyReset: true,
                                items: [{
                                    contentEl: 'ppdb_content',
                                },]
                            },
                        ]
                    }]
                });
            },
            "finalize_comp_and_layout" : function()
            {   // overide TextField Validation to exclude empty string
                Ext.override(Ext.form.TextField, {
                    validator:function(text){
                        return (text.length==0 || Ext.util.Format.trim(text).length!=0);
                    }
                });
            },
            "initialize": function()
            {   Ext.QuickTips.init();
                this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
        }; // end of public space
    }(); // end of app
    Ext.onReady(alfalah.ppdb.initialize, alfalah.ppdb);
    </script>
</head>
<body>
    <div id="north_content" class="x-hide-display">
        <div align="center">
            <br>
            <h1><big>PPDB ONLINE {{ $TAHUNAJARAN }}</big></h1>
            <h1><big>LEMBAGA PENDIDIKAN AL FALAH DARUSSALAM TROPODO</big></h1>
      </div>
    </div>
    <div id="ppdb_content" class="x-hide-display">
        <div align="left">
            <ol><h1><big>DATA anda sudah tersimpan.</big></h1></ol>
            <ol>
                <li>No.Pendaftaran : <strong><big>{{ $PPDB_ID }}</big></strong></li><br>
                <li>Nama Lengkap   : <strong><big>{{ $NAMA_LENGKAP }}</big></strong></li><br>
            </ol>
            <ol></ol>
            <ol><h1><big>Langkah Selanjutnya</big></h1></ol>
            <ol>
                
                <li>Melakukan Transfer Pembayaran ke Rek : 7015608107 a/n Yayasan Masjid Darussalam</li><br>
                <li>Melakukan Konfirmasi Kepada Petugas Admin melalui no WA : +62 858-5574-8137</li><br>
                <li>Mendapatkan jadwal observasi dr panitia PPDB via what's up</li><br>
               
            </ol>
        </div>
        <div id="CloseBtn" style="margin:15px 0;"></div>
    </div>
</body>
</html>
