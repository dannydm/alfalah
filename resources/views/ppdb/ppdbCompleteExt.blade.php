<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Language" content="en-us" />
    <title>{{ config('app.name', 'ALFALAH') }}</title>
    <meta name="keywords" content="{{ config('app.name', 'ALFALAH') }}" />
    <meta name="description" content="{{ config('app.name', 'ALFALAH') }}" />
    <meta name="copyright" content="{{ config('app.name', 'ALFALAH') }}" />
    <!-- ** css stylesheet ** -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/resources/css/ext-all.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/resources/css/ext-all.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/shared/icons/silk.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/shared/examples.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/grid/grid-examples.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/ux/css/Portal.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/ux/css/LockingGridView.css') }}" />
    <!-- ** Javascript ** -->
    <!-- ExtJS library: base/adapter -->
    <script type="text/javascript" src="{{ asset('/js/ext3/adapter/ext/ext-base.js') }}"></script>
    <!-- ExtJS library: all widgets -->
    <script type="text/javascript" src="{{ asset('/js/ext3/ext-all.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/ext3/examples/ux/ux-all.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/ext3/examples/ux/LockingGridView.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/ext3/examples/ux/fileuploadfield/css/fileuploadfield.css') }}" />
    <script type="text/javascript" src="{{ asset('/js/ext3/examples/ux/fileuploadfield/FileUploadField.js') }}"></script>

    <!-- alfalah ppdb -->
    <script type="text/javascript">
    // create namespace
    Ext.namespace('alfalah', 'alfalah.ppdb');
    // create application
    alfalah.ppdb = function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.Tab;
        this.image_url;
        this.init_mode;
        this.target;
        this.action;
        this.Grid;
        this.Columns;
        this.Records;
        this.DataStore;
        this.Searchs;
        this.menuPanel;
        this.viewport;
        this.rupiah;

        // private functions
        //**********************
        // public space
        //**********************
        return {
            // execute at the very last time
            // public properties, e.g. strings to translate
            // public methods
            server_date : new Date().format('Y'),
            start_date  : new Date(this.server_date, 0, 1),
            end_date    : new Date(this.server_date, 11, 31),
            page_limit  : 75,
            page_start  : 0,
            sid         : '{{ csrf_token() }}',
            "setParameter" : function(Ajax, Image, Mode)
            {   //this.ajax_url =  Ajax;
                image_url = Image;
                init_mode = Mode;
            },
            "logout" : function()
            {   window.location = "{{ url('/logout') }}";
            },
            "prepare_component" : function()
            {   // NOTE: This is an example showing simple state management. During development,
                // it is generally best to disable state management as dynamically-generated ids
                // can change across page loads, leading to unpredictable results.  The developer
                // should ensure that stable state ids are set for stateful components in real apps.
                Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
                // Ext.BLANK_IMAGE_URL = '/newalfalah/frontend/web/js/ext3/resources/images/default/s.gif ';
                this.start_date = new Date(this.server_date, 0, 1);
                this.end_date   = new Date(this.server_date, 11, 31);

                this.SaveBtn    = new Ext.Button(
                {
                    id         : "_SaveBtn",
                    fieldLabel : '',
                    text       : '<strong> S I M P A N   D A T A</strong>',
                    tooltip    : 'Simpan data',
                    iconCls    : 'silk-disk',
                    xtype      : 'button',
                    width      : 120,
                    handler    : this.Form_save,
                    scope      : this,
                }).render(document.body, 'SaveBtn');

                this.LoginBtn = new Ext.Button(
                {
                    id         : "_LoginBtn",
                    fieldLabel : '',
                    text       : '<strong> L O G I N</strong>',
                    tooltip    : 'Login reistered user',
                    iconCls    : 'silk-key',
                    xtype      : 'button',
                    width      : 120,
                    handler    : this.Form_login,
                    scope      : this,
                })

                this.DepartmentDS = this.newDataStore(
                    "{{ url('/ppdb/0/9') }}", false,
                    {   s:"form", limit:this.page_limit, start:this.page_start,
                        type:"Division"}
                );
                this.DepartmentDS.load();

                this.DataStore = new Ext.data.Store(
                {   proxy:  new Ext.data.HttpProxy(
                    {   method: 'GET',
                        url: "{{ url('/ppdb/1/3') }}",
                    }),
                    baseParams: {   s:"form", limit:this.page_limit, start:this.page_start, id:"{{ $NOPEN }}" },
                    reader: new Ext.data.JsonReader(),
                    remoteFilter : false,
                });

                this.ParentsDS = new Ext.data.Store(
                {   proxy:  new Ext.data.HttpProxy(
                    {   method: 'GET',
                        url: "{{ url('/ppdb/1/4') }}",
                    }),
                    baseParams: {   s:"form", limit:this.page_limit, start:this.page_start, id:"{{ $NOPEN }}" },
                    reader: new Ext.data.JsonReader(),
                    remoteFilter : false,
		});
		this.FilesDS = new Ext.data.Store(
                {   proxy:  new Ext.data.HttpProxy(
                    {   method: 'GET',
                        url: "{{ url('/ppdb/1/9') }}",
                    }),
                    baseParams: {   s:"form", limit:this.page_limit, start:this.page_start, id:"{{ $NOPEN }}" },
                    reader: new Ext.data.JsonReader(),
                    remoteFilter : false,
                });

                this.kelas_Smp = new Ext.data.SimpleStore(
                            {   fields: ['label'],
                                data : [ ['7'], ['8']],
                            });
                
                this.kelas_Sd = new Ext.data.SimpleStore(
                                                {   fields: ['label'],
                                                    data : [ ['TK A'], ['TK B'],['1'], ['2'], ['3'], ['4'],['5'], ['7'],['8']],
                                                  //  storeId : 'kelas_sd_id'
                                                });

                this.kelas_Tk = new Ext.data.SimpleStore(
                            {   fields: ['label'],
                                data : [ ['A'], ['B']],
                              //  storeId : 'kelas_tk_id'
                            });

                this.empty_arr = [0];

                /************************************
                    F O R M S
                ************************************/
                this.Form = new Ext.FormPanel(
                {   id : 'ppdb_form',
                    bodyStyle:'padding:5px',
                    // autoHeight : true,
                    // autoScroll  : true,
                    // title : 'TITLE',
                    // tbar: [],
                    border: false,
                    items: [
                    // INFORMASI PENDAFTAR
                    {   layout:'column',
                        border:false,
                        items:[
                        {   columnWidth:.3,  // hidden columns
                            layout: 'form',
                            border:false,
                            items: [
                            {   id : 'ppdb_id',
                                xtype:'textfield',
                                fieldLabel: 'ID',
                                name: 'id',
                                anchor:'95%',
                                allowBlank: true,
                                readOnly : true,
                                value : '',
                                hidden : true,
                            },
                            {   id : 'ppdb_gender',
                                xtype:'textfield',
                                fieldLabel: 'L / P',
                                name: 'gender',
                                anchor:'95%',
                                allowBlank: true,
                                readOnly : true,
                                value : '',
                                hidden : true,
                            },
                            {   id : 'ppdb_created_date',
                                xtype:'textfield',
                                fieldLabel: 'Created Date',
                                name: 'created_date',
                                anchor:'95%',
                                allowBlank: true,
                                readOnly : true,
                                hidden : true,
                            },
                            {   id : 'ppdb_tahunajaran_id',
                                xtype:'textfield',
                                fieldLabel: 'Tahun',
                                name: 'tahunajaran_id',
                                anchor:'95%',
                                allowBlank: false,
                                readOnly : true,
                                hidden : true,
                                value : '{{ $TAHUNAJARAN_ID }}'
                            },
                            {   id : 'ppdb_no_pendaftaran',
                                xtype:'textfield',
                                fieldLabel: 'NOPEN',
                                name: 'no_pendaftaran',
                                anchor:'95%',
                                allowBlank: true,
                                readOnly : true,
                                value : '',
                                hidden : true,
                            },]
                        },
                        {   columnWidth:.5,
                            layout: 'form',
                            border:false,
                            items: [
                            {
                                xtype: 'fieldset',
                                title: 'Informasi Pendaftaran ',
                                autoHeight: true,
                                anchor:'95%',
                                // defaultType: 'checkbox', // each item will be a checkbox
                                items: [
                                {   id : 'ppdb_no_registration',
                                    xtype:'textfield',
                                    fieldLabel: 'No.Registration',
                                    name: 'no_registration',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                }]
                            },
                            {
                                xtype: 'fieldset',
                                title: 'Informasi Calon Siswa ',
                                autoHeight: true,
                                anchor:'95%',
                                labelWidth: 120,
                                labelPad: 10,
                                items: [
                                {   id : 'ppdb_jenjang_sekolah_id',
                                    xtype : 'combo',
                                    fieldLabel: 'Jenjang',
                                    labelSeparator : '',
                                    name: 'jenjang_sekolah_id',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store: this.DepartmentDS,
                                    displayField: 'name',
                                    valueField: 'dept_id',
                                    mode : 'local',
                                    forceSelection: true,
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    typeAhead: true,
                                    width : 100,
                                    value: 'Pilihlah',
                                    // listeners : {scope : this,
                                    // 'afterrender' : function(value)
                                    // {  //var get_jenjang = Ext.getCmp('jenjang_sekolah_id').getValue();
                                    //      console.log('haaaaaaaaaaaaaa');
                                    //    // console.log(a.value);
                                    //    // b = alfalah.ppdb.DataStore.data.items[0].data.jenjang_sekolah_id;
                                    //    // var kelas_sd = Ext.getCmp('ppdb_sekolah_pindahan').store=kelas_Sd;
                                    //     if (value == 1) {
                                    //         Ext.getCmp('ppdb_sekolah_pindahan').store = this.kelas_Sd;
                                    //     }
                                    // }
                                    // }
                                },
                                {   id : 'ppdb_nama_lengkap',
                                    xtype:'textfield',
                                    fieldLabel: 'Nama.Lengkap',
                                    style: "textTransform: uppercase ;",
                                    name: 'nama_lengkap',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : '',
                                },
                                {   id : 'ppdb_nama_panggilan',
                                    style: "textTransform: uppercase ;",
                                    xtype:'textfield',
                                    fieldLabel: 'Nama.Panggilan',
                                    name: 'nama_panggilan',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_alamat_rumah',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Alamat.Rumah',
                                    name: 'alamat_rumah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_kecamatan',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Kecamatan',
                                    name: 'kecamatan',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_kota',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Kota',
                                    name: 'kota',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_telp_rumah',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Telp.Rumah',
                                    name: 'telp_rumah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_tempat_lahir',
                                    xtype:'textfield',
                                    fieldLabel: 'Tempat Lahir',
                                    style: "textTransform: uppercase ;",
                                    name: 'tempat_lahir',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_tanggal_lahir',
                                    xtype:'datefield',
                                    fieldLabel: 'Tanggal.Lahir',
                                    name: 'tanggal_lahir',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : '',
                                    format: 'd/m/Y',
                                },
                                {   id : 'ppdb_warganegara',
                                    xtype : 'combo',
                                    fieldLabel: 'Warga negara',
                                    labelSeparator : '',
                                    name: 'warganegara',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Indonesia'], ['Asing']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_agama',
                                    xtype : 'combo',
                                    fieldLabel: 'Agama',
                                    labelSeparator : '',
                                    name: 'agama',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Islam'], ['Kristen'],['Katolik'], ['Hindu'], ['Budha'], ['KongHucu']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Islam'
                                },
                                {   id : 'ppdb_ibu_kandung',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Nama.Ibu.Kandung',
                                    name: 'ibu_kandung',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                ]
                            },
                            ]
                        },
                        {   columnWidth:.5,
                            layout: 'form',
                            border:false,
                            items: [
                            {   title: 'Informasi Fisik',
                                xtype: 'fieldset',
                                autoHeight: true,
                                anchor:'95%',
                                // defaultType: 'checkbox', // each item will be a checkbox
                                items: [
                                {   id : 'ppdb_gender_name',
                                    xtype : 'combo',
                                    fieldLabel: 'L / P',
                                    labelSeparator : '',
                                    name: 'gender_name',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Laki-Laki'], ['Perempuan']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: ' '
                                },
                                {   id : 'ppdb_tinggi_badan',
                                    xtype:'textfield',
                                    fieldLabel: 'Tinggi (Cm)',
                                    style: "textTransform: uppercase ;",
                                    name: 'tinggi_badan',
                                    anchor:'95%',
                                    allowBlank: false,
                                    msgTarget: 'side',
                                    blankText: 'This should not be blank!',
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_berat_badan',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Berat (Kg)',
                                    name: 'berat_badan',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_bloodtype',
                                    xtype : 'combo',
                                    fieldLabel: 'Golongan.Darah',
                                    labelSeparator : '',
                                    name: 'bloodtype',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['A'], ['B'],['AB'], ['O'], ['Tidak Tahu']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_penyakit_bawaan',
                                    xtype:'textfield',
                                    fieldLabel: 'Penyakit Bawaan',
                                    style: "textTransform: uppercase ;",
                                    name: 'penyakit_bawaan',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                ]
                            },
                            {   title: 'Informasi Lingkungan Siswa',
                                xtype: 'fieldset',
                                autoHeight: true,
                                anchor:'95%',
                                // defaultType: 'checkbox', // each item will be a checkbox
                                items: [
                                {   id : 'ppdb_keadaan_siswa',
                                    xtype : 'combo',
                                    fieldLabel: 'Keadaan Siswa',
                                    labelSeparator : '',
                                    name: 'keadaan_siswa',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Orang Tua Lengkap'], ['Yatim Piatu'],['Yatim'], ['Piatu']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_tinggaldengan',
                                    xtype : 'combo',
                                    fieldLabel: 'Tinggal Bersama',
                                    labelSeparator : '',
                                    name: 'tinggaldengan',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Orang Tua'], ['Kakek Nenek'],['Orang Lain']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   border : false,
                                    autoHeight:true,
                                    layout:'column',
                                    items:[{
                                        columnWidth:.5,
                                        border :false,
                                        layout: 'form',
                                        items: [{
                                            id : 'ppdb_anakke',
                                            style: "background-color: #ffff00; background-image:none;",
                                            xtype:'textfield',
                                            style: "textTransform: uppercase ;",
                                            allowBlank: false,
                                            fieldLabel: 'Anak ke',
                                            name: 'anakke',
                                            anchor:'95%'
                                        }]
                                    },{
                                        columnWidth:.5,
                                        layout: 'form',
                                        border :false,
                                        items: [{
                                            id : 'ppdb_brpsaudara',
                                            style: "background-color: #ffff00; background-image:none;",
                                            xtype:'textfield',
                                            style: "textTransform: uppercase ;",
                                            allowBlank: false,
                                            fieldLabel: 'Dari Brp. Sdr ',
                                            name: 'brpsaudara',
                                            anchor:'90%'
                                        }]
                                    }]
                                }
                                ]
                            },
                            ]
                        }]
                    },
                    ]
                });

                this.SekolahForm = new Ext.FormPanel(
                {   bodyStyle:'padding:5px',
                    // autoHeight : true,
                    // autoScroll  : true,
                    // title : 'TITLE',
                    // tbar: [],
                    items: [
                    {   layout:'column',
                        border:false,
                        items:[
                        {   columnWidth:.5,
                            layout: 'form',
                            border:false,
                            labelWidth: 110,
                            labelPad: 10,
                            items: [
                            {   title: 'Informasi Sekolah Asal',
                                xtype: 'fieldset',
                                autoHeight: true,
                                anchor:'95%',
                                // defaultType: 'checkbox', // each item will be a checkbox
                                items: [
                                {   id : 'keterangan_internal_external',
                                    xtype:'fieldset',
                                    title: 'Internal Jika Berasal Dari Alfalah Darussalam, External Jika dari Luar Alfalah Darussalam',
                                    name: 'sekolah_asal_nama',
                                    autoHeight: true,
                                    items: [
                                        {   id : 'ppdb_sekolah_asal_internal',
                                            xtype : 'combo',
                                            fieldLabel: 'Internal/External',
                                            
                                            labelSeparator : '',
                                            name: 'sekolah_asal_internal',
                                            anchor:'95%',
                                            allowBlank: false,
                                            readOnly : false,
                                            store : new Ext.data.SimpleStore(
                                                {   fields: ['label'],
                                                    data : [ ['Internal'], ['External']]
                                                }),
                                            displayField:'label',
                                            valueField :'label',
                                            mode : 'local',
                                            triggerAction: 'all',
                                            selectOnFocus:true,
                                            editable: false,
                                            width : 100,
                                            value: 'Pilihlah'
                                        },
                                    ]
                                },
                                {   id : 'ppdb_sekolah_asal_nama',
                                    xtype:'textfield',
                                    fieldLabel: 'Sekolah Asal',
                                    style: "textTransform: uppercase ;",
                                    name: 'sekolah_asal_nama',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },

                                {   id : 'ppdb_sekolah_asal_status',
                                    xtype : 'combo',
                                    fieldLabel: 'Status',
                                    labelSeparator : '',
                                    name: 'sekolah_asal_status',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Negeri'], ['Swasta']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_sekolah_asal_alamat',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Alamat',
                                    name: 'sekolah_asal_alamat',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_sekolah_asal_lulus',
                                    xtype:'textfield',
                                    fieldLabel: 'Tahun Lulus',
                                    name: 'sekolah_asal_lulus',
                                    style: "textTransform: uppercase ;",
                                    anchor:'95%',
                                    allowBlank: false,
                                    msgTarget: 'side',
                                    blankText: 'This should not be blank!',
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_sekolah_asal_akreditas',
                                    xtype:'textfield',
                                    fieldLabel: 'Akreditas Sekolah',
                                    name: 'sekolah_asal_akreditas',
                                    style: "textTransform: uppercase ;",
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_sekolah_asal_nilai_un',
                                    xtype:'textfield',
                                    fieldLabel: 'Nilai U.N',
                                    name: 'sekolah_asal_nilai_un',
                                    style: "textTransform: uppercase ;",
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                ]
                            },
                            {   title: 'Khusus Di Isi Jika Siswa Pindahan',
                                xtype: 'fieldset',
                                autoHeight: true,
                                anchor:'95%',
                                items: [
                                    {   id : 'ppdb_kelas_pindahan',
                                        xtype : 'combo',
                                        fieldLabel: 'Kelas',
                                        style: "background-color: #ffff00; background-image:none; ",
                                        name: 'kelas_pindahan',
                                        anchor:'95%',
                                        store : this.kelas_Sd,
                                        allowBlank: true,
                                        readOnly : false,
                                        displayField:'label',
                                        valueField :'label',
                                        mode : 'local',
                                        forceSelection : true,
                                        triggerAction: 'all',
                                        selectOnFocus:false,
                                        editable: true,
                                        width : 100,
                                        lastQuery: '',
                                        value: '',
                                        listeners : {scope : this,
                                        'beforequery' : function(combo, query, forceAll, cancel) {
                                        console.log('tessssssssss');
                                        var a = Ext.getCmp('ppdb_jenjang_sekolah_id').getValue();
                                        console.log(combo.combo.reset()); 
                                        combo.combo.clearValue();
                                        if (a==1) {
                                            console.log('tralalala');
                                            //   combo.combo.setValue(); 
                                            combo.combo.store = this.kelas_Sd;
                                            }
                                        }
                                    }
                                    },
                                ]
                            },

                            ]
                        },
                        {   columnWidth:.5,
                            layout: 'form',
                            border:false,
                            items: [
                                {   
                                title: 'Khusus Di Isi Jika Calon Siswa mempunyai saudara di Lembaga Pendidikan Alfalah',
                                xtype: 'fieldset',
                                autoHeight: true,
                                anchor:'95%',
                                items: [ 
                                    {
                                        id : 'ppdb_saudara_nis', 
                                        xtype:'textfield',
                                        style: "background-color: #ffff00; background-image:none; ",
                                        fieldLabel: 'N.I.S',
                                        name: 'saudara_nis',
                                        anchor:'50%',
                                        allowBlank: true,
                                        // readOnly : true,
                                        value : ''
                                    },
                                    {
                                        id : 'ppdb_saudara_kelas', 
                                        xtype:'textfield',
                                        style: "background-color: #ffff00; background-image:none; ",
                                        fieldLabel: 'Kelas',
                                        name: 'saudara_kelas',
                                        anchor:'50%',
                                        allowBlank: true,
                                        // readOnly : true,
                                        value : ''
                                    },
                                    {
                                        id : 'ppdb_saudara_nama', 
                                        xtype:'textfield',
                                        style: "textTransform: uppercase ;",
                                        fieldLabel: 'Nama Siswa',
                                        name: 'saudara_nama',
                                        anchor:'95%',
                                        allowBlank: true,
                                        // readOnly : true,
                                        value : ''
                                    }
                                ]
                            },
                        ]}
                    ]
                    },
                    ]
                });
                this.ParentsForm = new Ext.FormPanel(
                {   bodyStyle:'padding:5px',
                    // autoHeight : true,
                    // autoScroll  : true,
                    // title : 'TITLE',
                    // tbar: [],
                    items: [
                    // IDENTITAS AYAH & IBU
                    {   layout:'column',
                        border:false,
                        items:[
                        {   columnWidth:.5,
                            layout: 'form',
                            border:false,
                            labelWidth: 150,
                            labelPad: 10,
                            items: [
                            {
                                xtype: 'fieldset',
                                title: 'Identitas Ayah ',
                                autoHeight: true,
                                anchor:'95%',
                                // defaultType: 'checkbox', // each item will be a checkbox
                                items: [
                                {   id : 'ppdb_nama_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Nama.Ayah',
                                    style: "textTransform: uppercase ;",
                                    name: 'nama_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_alamat_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Alamat.Ayah',
                                    style: "textTransform: uppercase ;",
                                    name: 'alamat_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_tempat_lahir_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Tempat Lahir',
                                    style: "textTransform: uppercase ;",
                                    name: 'tempat_lahir_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_tanggal_lahir_ayah',
                                    xtype:'datefield',
                                    fieldLabel: 'Tanggal.Lahir',
                                    name: 'tanggal_lahir_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    format: 'd/m/Y',
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_telp_rumah_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Telp.Rumah',
                                    style: "textTransform: uppercase ;",
                                    name: 'telp_rumah_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_telp_hp_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Telp.Hp',
                                    style: "textTransform: uppercase ;",
                                    name: 'telp_hp_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_hubungan_ayah',
                                    xtype : 'combo',
                                    fieldLabel: 'Hubungan Siswa',
                                    labelSeparator : '',
                                    name: 'hubungan_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Ayah Kandung'], ['Ayah Tiri'], ['Ayah Angkat / Asuh']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_agama_ayah',
                                    xtype : 'combo',
                                    fieldLabel: 'Agama',
                                    labelSeparator : '',
                                    name: 'agama_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Islam'], ['Kristen'],['Katolik'], ['Hindu'], ['Budha'], ['KongHucu']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Islam'
                                },
                                {   id : 'ppdb_pendidikan_ayah',
                                    xtype : 'combo',
                                    fieldLabel: 'Pendidikan',
                                    labelSeparator : '',
                                    name: 'pendidikan_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['SD'], ['SMP'], ['SMA'], ['D1'], ['D2'], ['D3'],['D4'], ['S1'], ['S2'], ['S3']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_pekerjaan_ayah',
                                    style: "textTransform: uppercase ;",
                                    xtype:'textfield',
                                    fieldLabel: 'Pekerjaan',
                                    name: 'pekerjaan_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_jenis_pekerjaan_ayah',
                                    xtype : 'combo',
                                    fieldLabel: 'Jenis Pekerjaan',
                                    labelSeparator : '',
                                    name: 'jenis_pekerjaan_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['PNS'], ['BUMN'], ['TNI/POLRI'], ['Pegawai Swasta'], ['Pensiunan'], ['Wiraswasta']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_jabatan_ayah',
                                    style: "textTransform: uppercase ;",
                                    xtype:'textfield',
                                    fieldLabel: 'Jabatan',
                                    name: 'jabatan_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_institusi_ayah',
                                    style: "textTransform: uppercase ;",
                                    xtype:'textfield',
                                    fieldLabel: 'Institusi',
                                    name: 'institusi_ayah',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_institusi_alamat_ayah',
                                    style: "textTransform: uppercase ;",
                                    xtype:'textfield',
                                    fieldLabel: 'Alamat Institusi',
                                    name: 'institusi_alamat_ayah',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_institusi_telp_ayah',
                                    style: "textTransform: uppercase ;",
                                    xtype:'textfield',
                                    fieldLabel: 'Telp Institusi',
                                    name: 'institusi_telp_ayah',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_penghasilan_ayah',
                                    xtype : 'combo',
                                    fieldLabel: 'Penghasilan',
                                    labelSeparator : '',
                                    name: 'penghasilan_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [
                                            ['Rp.5.000.000 - Rp. 10.000.000'],
                                            ['Rp.10.000.000 - Rp. 15.000.000'],
                                            ['Rp.15.000.000 - Rp. 25.000.000'],
                                            ['Rp.25.000.000 - Rp. 50.000.000'],
                                            ['Rp.50.000.000 - ke atas']
                                        ]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_tanggungan_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Jumlah Tanggungan',
                                    style: "textTransform: uppercase ;",
                                    name: 'tanggungan_ayah',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : '0'
                                },

                                ]
                            },
                            ]
                        },
                        {   columnWidth:.5,
                            layout: 'form',
                            border:false,
                            labelWidth: 150,
                            labelPad: 10,
                            items: [
                            {
                                xtype: 'fieldset',
                                title: 'Identitas Ibu ',
                                autoHeight: true,
                                anchor:'95%',
                                // defaultType: 'checkbox', // each item will be a checkbox
                                items: [
                                {   id : 'ppdb_nama_ibu',
                                    xtype:'textfield',
                                    fieldLabel: 'Nama.Ibu',
                                    style: "textTransform: uppercase ;",
                                    name: 'nama_ibu',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_alamat_ibu',
                                    xtype:'textfield',
                                    fieldLabel: 'Alamat.Ibu',
                                    style: "textTransform: uppercase ;",
                                    name: 'alamat_ibu',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_tempat_lahir_ibu',
                                    xtype:'textfield',
                                    fieldLabel: 'Tempat Lahir',
                                    style: "textTransform: uppercase ;",
                                    name: 'tempat_lahir_ibu',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_tanggal_lahir_ibu',
                                    xtype:'datefield',
                                    fieldLabel: 'Tanggal.Lahir',
                                    name: 'tanggal_lahir_ibu',
                                    anchor:'95%',
                                    format: 'd/m/Y',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_telp_rumah_ibu',
                                    xtype:'textfield',
                                    fieldLabel: 'Telp.Rumah',
                                    style: "textTransform: uppercase ;",
                                    name: 'telp_rumah_ibu',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_telp_hp_ibu',
                                    xtype:'textfield',
                                    fieldLabel: 'Telp.Hp',
                                    style: "textTransform: uppercase ;",
                                    name: 'telp_hp_ibu',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_hubungan_ibu',
                                    xtype : 'combo',
                                    fieldLabel: 'Hubungan Siswa',
                                    labelSeparator : '',
                                    name: 'hubungan_ibu',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Ibu Kandung'], ['Ibu Tiri'], ['Ibu Angkat / Asuh']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_agama_ibu',
                                    xtype : 'combo',
                                    fieldLabel: 'Agama',
                                    labelSeparator : '',
                                    name: 'agama_ibu',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Islam'], ['Kristen'],['Katolik'], ['Hindu'], ['Budha'], ['KongHucu']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Islam'
                                },
                                {   id : 'ppdb_pendidikan_ibu',
                                    xtype : 'combo',
                                    fieldLabel: 'Pendidikan',
                                    labelSeparator : '',
                                    name: 'pendidikan_ibu',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['SD'], ['SMP'], ['SMA'], ['D1'], ['D2'], ['D3'],['D4'], ['S1'], ['S2'], ['S3']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_pekerjaan_ibu',
                                    xtype:'textfield',
                                    fieldLabel: 'Pekerjaan',
                                    style: "textTransform: uppercase ;",
                                    name: 'pekerjaan_ibu',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_jenis_pekerjaan_ibu',
                                    xtype : 'combo',
                                    fieldLabel: 'Jenis Pekerjaan',
                                    labelSeparator : '',
                                    name: 'jenis_pekerjaan_ibu',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['PNS'], ['BUMN'], ['TNI/POLRI'], ['Pegawai Swasta'], ['Pensiunan'], ['Wiraswasta'], ['IRT']]
                                          }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah',
                                    listeners: {
                                        'select': function (view, selections, options) {

                                            console.log('option');
                                            console.log(options);
                                            if (selections.data.label =='IRT') {
                                                Ext.getCmp('ppdb_jabatan_ibu').setDisabled('true').el.setStyle('background','gray');
                                                Ext.getCmp('ppdb_institusi_ibu').setDisabled('true').el.setStyle('background','gray');
                                                Ext.getCmp('ppdb_institusi_alamat_ibu').setDisabled('true').el.setStyle('background','gray');
                                                Ext.getCmp('ppdb_institusi_telp_ibu').setDisabled('true').el.setStyle('background','gray');
                                                Ext.getCmp('ppdb_penghasilan_ibu').setValue('Rp. 0 - Rp. 0').el.setStyle('background','lime');

                                            } else {
                                                Ext.getCmp('ppdb_jabatan_ibu').setDisabled().el.setStyle('background','white');
                                                Ext.getCmp('ppdb_institusi_ibu').setDisabled().el.setStyle('background','white');
                                                Ext.getCmp('ppdb_institusi_alamat_ibu').setDisabled().el.setStyle('background','white');
                                                Ext.getCmp('ppdb_institusi_telp_ibu').setDisabled().el.setStyle('background','white');
                                                Ext.getCmp('ppdb_penghasilan_ibu').setValue('Pilihlah').el.setStyle('background','lime');

                                            };
                                            console.log('hehehehhehe');
                                        }
                                    }
                                },
                                {   id : 'ppdb_jabatan_ibu',
                                    xtype:'textfield',
                                    fieldLabel: 'Jabatan',
                                    style: "textTransform: uppercase ;",
                                    name: 'jabatan_ibu',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_institusi_ibu',
                                    xtype:'textfield',
                                    fieldLabel: 'Institusi',
                                    style: "textTransform: uppercase ;",
                                    name: 'institusi_ibu',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_institusi_alamat_ibu',
                                    xtype:'textfield',
                                    fieldLabel: 'Alamat Institusi',
                                    style: "textTransform: uppercase ;",
                                    name: 'institusi_alamat_ibu',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_institusi_telp_ibu',
                                    xtype:'textfield',
                                    fieldLabel: 'Telp Institusi',
                                    style: "textTransform: uppercase ;",
                                    name: 'institusi_telp_ibu',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_penghasilan_ibu',
                                    xtype : 'combo',
                                    fieldLabel: 'Penghasilan',
                                    labelSeparator : '',
                                    name: 'penghasilan_ibu',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [
                                            ['Rp.5.000.000 - Rp. 10.000.000'],
                                            ['Rp.10.000.000 - Rp. 15.000.000'],
                                            ['Rp.15.000.000 - Rp. 25.000.000'],
                                            ['Rp.25.000.000 - Rp. 50.000.000'],
                                            ['Rp.50.000.000 - ke atas']
                                        ]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_pengeluaran_ibu',
                                    xtype:'textfield',
                                    fieldLabel: 'Jumlah Pengeluaran (Rp)',
                                    name: 'pengeluaran_ibu',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // boxMaxWidth : 100,
                                    // style: "text-align: right; background-image:none;",
                                    listeners : { scope : this,
                                    'change' : function(the_field, value)
                                        {
                                             // Ext.getCmp('ppdb_pengeluaran_ibu').setValue(alfalah.ppdb.rpMoney(v));
                                             //   parseInt(v)
                                            the_field.setValue(Ext.util.Format.number(value, '0,000'));
                                        },
                                    "render" : function (the_field)
                                        {   the_field.setValue(Ext.util.Format.number(the_field.value, '0,000'));
                                        }
                                    },
                                },
                                ]
                            },
                            ]
                        }]
                    },
                    ]
                });
                this.WaliForm = new Ext.FormPanel(
                {   bodyStyle:'padding:5px',
                    // autoHeight : true,
                    // autoScroll  : true,
                    // title : 'TITLE',
                    // tbar: [],
                    items: [
                    // IDENTITAS WALI AYAH & IBU
                    {   layout:'column',
                        border:false,
                        items:[
                        {   columnWidth:.5,
                            layout: 'form',
                            border:false,
                            labelWidth: 125,
                            labelPad: 10,
                            items: [
                            {
                                xtype: 'fieldset',
                                title: 'Pilih Wali Siswa, Jika Tidak Sama dengan Ayah/Ibu, Wajib Diisi',
                                autoHeight: true,
                                anchor:'95%',
                                // defaultType: 'checkbox', // each item will be a checkbox
                                items: [
                                {
                                    id : 'ppdb_wali_id',
                                    xtype : 'combo',
                                    fieldLabel: 'Pilih Wali',
                                    style: "background-color: #ffff00; background-image:none;",
                                    labelSeparator : '',
                                    name: 'ppdb_wali',
                                    anchor:'95%',
                                    allowBlank: true,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Sama Dengan Ayah'], ['Sama Dengan Ibu'], ['Tidak Sama']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah',
                                    listeners :
                                    { 'select': function (combo,value){
                                            console.log(combo);
                                            console.log(value.data.label);
                                            if (value.data.label == 'Sama Dengan Ayah'){
                                                    console.log('woeeeeeee');
                                                    Ext.getCmp('ppdb_nama_wali_ayah').setValue(Ext.getCmp('ppdb_nama_ayah').getValue());
                                                    Ext.getCmp('ppdb_alamat_wali_ayah').setValue(Ext.getCmp('ppdb_alamat_ayah').getValue());
                                                    Ext.getCmp('ppdb_tempat_lahir_wali_ayah').setValue(Ext.getCmp('ppdb_tempat_lahir_ayah').getValue());
                                                    Ext.getCmp('ppdb_tanggal_lahir_wali_ayah').setValue(Ext.getCmp('ppdb_tanggal_lahir_ayah').getValue());
                                                    Ext.getCmp('ppdb_telp_rumah_wali_ayah').setValue(Ext.getCmp('ppdb_telp_rumah_ayah').getValue());
                                                    Ext.getCmp('ppdb_telp_hp_wali_ayah').setValue(Ext.getCmp('ppdb_telp_hp_ayah').getValue());
                                                    Ext.getCmp('ppdb_hubungan_wali_ayah').setValue(Ext.getCmp('ppdb_hubungan_ayah').getValue());
                                                    Ext.getCmp('ppdb_agama_wali_ayah').setValue(Ext.getCmp('ppdb_agama_ayah').getValue());
                                                    Ext.getCmp('ppdb_pendidikan_wali_ayah').setValue(Ext.getCmp('ppdb_pendidikan_ayah').getValue());
                                                    Ext.getCmp('ppdb_pekerjaan_wali_ayah').setValue(Ext.getCmp('ppdb_pekerjaan_ayah').getValue());
                                                    Ext.getCmp('ppdb_jenis_pekerjaan_wali_ayah').setValue(Ext.getCmp('ppdb_jenis_pekerjaan_ayah').getValue());
                                                    Ext.getCmp('ppdb_jabatan_wali_ayah').setValue(Ext.getCmp('ppdb_jabatan_ayah').getValue());
                                                    Ext.getCmp('ppdb_institusi_wali_ayah').setValue(Ext.getCmp('ppdb_institusi_ayah').getValue());
                                                    Ext.getCmp('ppdb_institusi_alamat_wali_ayah').setValue(Ext.getCmp('ppdb_institusi_alamat_ayah').getValue());
                                                    Ext.getCmp('ppdb_institusi_telp_wali_ayah').setValue(Ext.getCmp('ppdb_institusi_telp_ayah').getValue());
                                                    Ext.getCmp('ppdb_penghasilan_wali_ayah').setValue(Ext.getCmp('ppdb_penghasilan_ayah').getValue());
                                            }
                                            else if (value.data.label == 'Sama Dengan Ibu')
                                            {
                                                    console.log('ibuneeeeee');
                                                    Ext.getCmp('ppdb_nama_wali_ayah').setValue(Ext.getCmp('ppdb_nama_ibu').getValue());
                                                    Ext.getCmp('ppdb_alamat_wali_ayah').setValue(Ext.getCmp('ppdb_alamat_ibu').getValue());
                                                    Ext.getCmp('ppdb_tempat_lahir_wali_ayah').setValue(Ext.getCmp('ppdb_tempat_lahir_ibu').getValue());
                                                    Ext.getCmp('ppdb_tanggal_lahir_wali_ayah').setValue(Ext.getCmp('ppdb_tanggal_lahir_ibu').getValue());
                                                    Ext.getCmp('ppdb_telp_rumah_wali_ayah').setValue(Ext.getCmp('ppdb_telp_rumah_ibu').getValue());
                                                    Ext.getCmp('ppdb_telp_hp_wali_ayah').setValue(Ext.getCmp('ppdb_telp_hp_ibu').getValue());
                                                    Ext.getCmp('ppdb_hubungan_wali_ayah').setValue(Ext.getCmp('ppdb_hubungan_ibu').getValue());
                                                    Ext.getCmp('ppdb_agama_wali_ayah').setValue(Ext.getCmp('ppdb_agama_ibu').getValue());
                                                    Ext.getCmp('ppdb_pendidikan_wali_ayah').setValue(Ext.getCmp('ppdb_pendidikan_ibu').getValue());
                                                    Ext.getCmp('ppdb_pekerjaan_wali_ayah').setValue(Ext.getCmp('ppdb_pekerjaan_ibu').getValue());
                                                    Ext.getCmp('ppdb_jenis_pekerjaan_wali_ayah').setValue(Ext.getCmp('ppdb_jenis_pekerjaan_ibu').getValue());
                                                    Ext.getCmp('ppdb_jabatan_wali_ayah').setValue(Ext.getCmp('ppdb_jabatan_ibu').getValue());
                                                    Ext.getCmp('ppdb_institusi_wali_ayah').setValue(Ext.getCmp('ppdb_institusi_ibu').getValue());
                                                    Ext.getCmp('ppdb_institusi_alamat_wali_ayah').setValue(Ext.getCmp('ppdb_institusi_alamat_ibu').getValue());
                                                    Ext.getCmp('ppdb_institusi_telp_wali_ayah').setValue(Ext.getCmp('ppdb_institusi_telp_ibu').getValue());
                                                    Ext.getCmp('ppdb_penghasilan_wali_ayah').setValue(Ext.getCmp('ppdb_penghasilan_ibu').getValue());
                                            }
                                            else {
                                                alfalah.ppdb.WaliForm.getForm().reset();
                                            };
                                        }

                                    },
                                },

                                {   id : 'ppdb_nama_wali_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Nama.Wali',
                                    style: "textTransform: uppercase ;",
                                    name: 'nama_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_alamat_wali_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Alamat.Wali',
                                    style: "textTransform: uppercase ;",
                                    name: 'alamat_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_tempat_lahir_wali_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Tempat Lahir',
                                    style: "textTransform: uppercase ;",
                                    name: 'tempat_lahir_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_tanggal_lahir_wali_ayah',
                                    xtype:'datefield',
                                    fieldLabel: 'Tanggal.Lahir',
                                    name: 'tanggal_lahir_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    format: 'd/m/Y',
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_telp_rumah_wali_ayah',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Telp.Rumah',
                                    name: 'telp_rumah_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_telp_hp_wali_ayah',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Telp.Hp',
                                    name: 'telp_hp_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_hubungan_wali_ayah',
                                    xtype : 'combo',
                                    fieldLabel: 'Hubungan Siswa',
                                    labelSeparator : '',
                                    name: 'hubungan_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Wali.Ayah Kandung'], ['Wali.Ayah Tiri'], ['Wali.Ayah Angkat / Asuh']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_agama_wali_ayah',
                                    xtype : 'combo',
                                    fieldLabel: 'Agama',
                                    labelSeparator : '',
                                    name: 'agama_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['Islam'], ['Kristen'],['Katolik'], ['Hindu'], ['Budha'], ['KongHucu']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Islam'
                                },
                                {   id : 'ppdb_pendidikan_wali_ayah',
                                    xtype : 'combo',
                                    fieldLabel: 'Pendidikan',
                                    labelSeparator : '',
                                    name: 'pendidikan_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['SD'], ['SMP'], ['SMA'], ['D1'], ['D2'], ['D3'],['D4'], ['S1'], ['S2'], ['S3']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_pekerjaan_wali_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Pekerjaan',
                                    style: "textTransform: uppercase ;",
                                    name: 'pekerjaan_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_jenis_pekerjaan_wali_ayah',
                                    xtype : 'combo',
                                    fieldLabel: 'Jenis Pekerjaan',
                                    labelSeparator : '',
                                    name: 'jenis_pekerjaan_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [ ['PNS'], ['BUMN'], ['TNI/POLRI'], ['Pegawai Swasta'], ['Pensiunan'], ['Wiraswasta']]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_jabatan_wali_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Jabatan',
                                    style: "textTransform: uppercase ;",
                                    name: 'jabatan_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_institusi_wali_ayah',
                                    xtype:'textfield',
                                    style: "textTransform: uppercase ;",
                                    fieldLabel: 'Institusi',
                                    name: 'institusi_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_institusi_alamat_wali_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Alamat Institusi',
                                    style: "textTransform: uppercase ;",
                                    name: 'institusi_alamat_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_institusi_telp_wali_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Telp Institusi',
                                    style: "textTransform: uppercase ;",
                                    name: 'institusi_telp_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'ppdb_penghasilan_wali_ayah',
                                    xtype : 'combo',
                                    fieldLabel: 'Penghasilan',
                                    labelSeparator : '',
                                    name: 'penghasilan_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: false,
                                    readOnly : false,
                                    store : new Ext.data.SimpleStore(
                                    {   fields: ['label'],
                                        data : [
                                            ['Rp.5.000.000 - Rp. 10.000.000'],
                                            ['Rp.10.000.000 - Rp. 15.000.000'],
                                            ['Rp.15.000.000 - Rp. 25.000.000'],
                                            ['Rp.25.000.000 - Rp. 50.000.000'],
                                            ['Rp.50.000.000 - ke atas']
                                        ]
                                    }),
                                    displayField:'label',
                                    valueField :'label',
                                    mode : 'local',
                                    triggerAction: 'all',
                                    selectOnFocus:true,
                                    editable: false,
                                    width : 100,
                                    value: 'Pilihlah'
                                },
                                {   id : 'ppdb_tanggungan_wali_ayah',
                                    xtype:'textfield',
                                    fieldLabel: 'Jumlah Tanggungan',
                                    style: "textTransform: uppercase ;",
                                    name: 'tanggungan_wali_ayah',
                                    anchor:'95%',
                                    allowBlank: true,
                                    // readOnly : true,
                                    value : '0'
                                },

                                ]
                            },
                            ]
                        },

                        ]

                    },
                    ]
                });
                this.ExpenseForm = new Ext.FormPanel(
                {   bodyStyle:'padding:5px',
                    items: [
                    {   layout:'column',
                        border:false,
                        items:[
                        {   columnWidth:.5,
                            layout: 'form',
                            border:false,
                            labelWidth: 210,
                            labelPad: 10,
                            boxMaxWidth : 400,
                            items: [
                            {
                                xtype: 'fieldset',
                                title: 'Pengeluaran Rutin ',
                                autoHeight: true,
                                anchor:'95%',
                                items: [
                                {   id : 'ppdb_expense_listrik',
                                    xtype:'textfield',
                                    fieldLabel: 'Listrik per bulan (Rp)',
                                    name: 'expense_listrik',
                                    anchor:'95%',
                				    allowBlank: false,
                				    value : ' ',
                				    style: "text-align: right; background-image:none;",
                                    listeners : { scope : this,
                                    'change' : function(the_field, value)
                                        {
                                            // Ext.getCmp('ppdb_expense_listrik').setValue(alfalah.ppdb.rpMoney(v));
                                            //    parseInt(v)
                                            the_field.setValue(Ext.util.Format.number(value, '0,000'));
                                        },
                                    "render" : function (the_field)
                                        {   the_field.setValue(Ext.util.Format.number(the_field.value, '0,000'));
                                        }
                                    },
                                },
                                {   id : 'ppdb_expense_pdam',
                                    xtype:'textfield',
                                    fieldLabel: 'PDAM per bulan (Rp)',
                                    name: 'expense_pdam',
                                    anchor:'95%',
                                    allowBlank: false,
                                    value : ' ',
                                    style: "text-align: right; background-image:none;",
                                    listeners : { scope : this,
                                    'change' : function(the_field, value)
                                        {
                                            // Ext.getCmp('ppdb_expense_pdam').setValue(alfalah.ppdb.rpMoney(v));
                                            //    parseInt(v)
                                            the_field.setValue(Ext.util.Format.number(value, '0,000'));
                                        },
                                    "render" : function (the_field)
                                        {   the_field.setValue(Ext.util.Format.number(the_field.value, '0,000'));
                                        },
                                    },
                                },
                                {   id : 'ppdb_expense_pulsa',
                                    xtype:'textfield',
                                    fieldLabel: 'Pulsa HP per bulan (Rp)',
                                    name: 'expense_pulsa',
                                    anchor:'95%',
                                    allowBlank: false,
                                    value : ' ',
                                    style: "text-align: right; background-image:none;",
                                    listeners : { scope : this,
                                    'change' : function(the_field, value)
                                        {
                                            // Ext.getCmp('ppdb_expense_pulsa').setValue(alfalah.ppdb.rpMoney(v));
                                            //    parseInt(v)
                                            the_field.setValue(Ext.util.Format.number(value, '0,000'));
                                        },
                                    "render" : function (the_field)
                                        {   the_field.setValue(Ext.util.Format.number(the_field.value, '0,000'));
                                        }
                                    },
                                },
                                {   id : 'ppdb_expense_monthly',
                                    xtype:'textfield',
                                    fieldLabel: 'Pengeluaran Rata-rata Per bulan (Rp)',
                                    name: 'expense_monthly',
                                    anchor:'95%',
                                    allowBlank: false,
                                    value : ' ',
                                    style: "text-align: right; background-image:none;",
                                    listeners : { scope : this,
                                    'change' : function(the_field, value)
                                        {
                                            // Ext.getCmp('ppdb_expense_monthly').setValue(alfalah.ppdb.rpMoney(v));
                                            //    parseInt(v)
                                            the_field.setValue(Ext.util.Format.number(value, '0,000'));
                                        },
                                    "render" : function (the_field)
                                        {   the_field.setValue(Ext.util.Format.number(the_field.value, '0,000'));
                                        }
                                    },
                                },
                                ]
                            }
                            ]
                        },
                        ]
                    },
                    ]
                });
                this.UploadForm = new Ext.FormPanel(
                {   //bodyStyle:'padding:5px',
                    fileUpload: true,
                    items: [
                    {   layout:'column',
                        border:false,
                        items:[
                        {   columnWidth:.5,
                            layout: 'form',
                            border:false,
                            // labelWidth: 110,
                            // labelPad: 10,
                            // boxMaxWidth : 700,
                            items: [
                            {
                                xtype: 'fieldset',
                                title: 'Upload Photo Pendukung ',
                                autoHeight: true,
                                // anchor: '95%',
                                items: [
                                {   xtype      : 'textfield',
                                    fieldLabel : 'token',
                                    name       : '_token',
                                    anchor     : '95%',
                                    allowBlank : false,
                                    hidden     : true,
                                    value      : "{{ csrf_token() }}",
                                },
                                {   id         : 'upload_no_registration',
                                    xtype      : 'textfield',
                                    fieldLabel : 'No.Reg',
                                    name       : 'no_registration',
                                    anchor     : '95%',
                                    allowBlank : false,
                                    hidden     : true,
                                    value      : "{{ csrf_token() }}",
                                },
                                {   id         : 'upload_photo_ktp',
                                    xtype      : 'fileuploadfield',
                                    fieldLabel : 'Akte.Lahir.Siswa',
                                    name       : 'photo_ktp',
                                    anchor     : '95%',
                                    allowBlank : true,
                                    buttonText : '',
                                    buttonCfg  : { iconCls: 'silk-layout-add' }
                                },
                                {   id         : 'upload_photo_kk',
                                    xtype      : 'fileuploadfield',
                                    fieldLabel : 'Kartu Keluarga',
                                    name       : 'photo_kk',
                                    anchor     : '95%',
                                    allowBlank : true,
                                    buttonText : '',
                                    buttonCfg  : { iconCls: 'silk-layout-add' }
                                },
                                {   id         : 'upload_photo_siswa',
                                    xtype      : 'fileuploadfield',
                                    fieldLabel : 'Foto.Siswa',
                                    name       : 'photo_siswa',
                                    anchor     : '95%',
                                    allowBlank : true,
                                    buttonText : '',
                                    buttonCfg  : { iconCls: 'silk-layout-add' }
                                },
                                {   id         : 'upload_photo_bukti',
                                    xtype      : 'fileuploadfield',
                                    fieldLabel : 'Bukti.Bayar',
                                    name       : 'photo_bayar',
                                    anchor     : '95%',
                                    allowBlank : true,
                                    buttonText : '',
                                    buttonCfg  : { iconCls: 'silk-layout-add' }
                                },
                                ],
                                buttons: [
                                {   text: 'Save',
                                    handler: this.Form_upload,
                                },
                                // {   text: 'Reset',
                                //     handler: function() {   this.UploadForm.reset(); }
                                // }
                                ]
                            }
                            ]
                        },
                        {   columnWidth:.5,
                            layout: 'form',
                            border:false,
                            // labelWidth: 110,
                            // labelPad: 10,
                            // boxMaxWidth : 700,
                            items: [{
                                    contentEl: 'photo_content',
                                },]
                        },
                        ]
                    },
                    ],
                });
            },
            "build_layout": function()
            {   this.viewport = new Ext.Viewport(
                {   id: 'viewport',
                    jsId: 'viewport',
                    layout:'border',
                    items:[
                    {   region:'north',
                        id:'north_panel',
                        jsId:'north_panel',
                        height : 100,
                        width: 200,
                        minSize: 200,
                        maxSize: 300,
                        layout : 'fit',
                        items: [{
                            contentEl: 'north_content',
                        },]
                    },
                    {   region: 'center',
                        xtype: 'tabpanel', // TabPanel itself has no title
                        // autoHeight:true,
                        // autoScroll:true,
                        enableTabScroll:true,
                        defaults:{autoScroll:true},
                        activeTab: 0,
                        items: [
                        {   title : 'CALON SISWA',
                            iconCls: 'silk-user',
                            region: 'center',
                            closable : false,
                            deferredRender : false,
                            preventBodyReset: true,
                            items: [ this.Form ]
                        },
                        {   title : 'ASAL SEKOLAH',
                            iconCls: 'silk-house',
                            region: 'center',
                            closable : false,
                            deferredRender : false,
                            preventBodyReset: true,
                            items: [ this.SekolahForm ]
                        },
                        {   title : 'ORANG TUA',
                            iconCls: 'silk-group',
                            region: 'center',
                            closable : false,
                            deferredRender : false,
                            preventBodyReset: true,
                            items: [ this.ParentsForm ]
                        },
                        {   title : 'WALI',
                            iconCls: 'silk-group-add',
                            region: 'center',
                            closable : false,
                            deferredRender : false,
                            preventBodyReset: true,
                            items: [ this.WaliForm ]
                        },
                        {   title : 'PENGELUARAN',
                            iconCls: 'silk-money',
                            region: 'center',
                            closable : false,
                            deferredRender : false,
                            preventBodyReset: true,
                            items: [ this.ExpenseForm ]
	                },
                        {   title : 'UPLOAD',
                            iconCls: 'silk-pictures',
                            region: 'center',
                            closable : false,
                            deferredRender : false,
                            preventBodyReset: true,
                            items: [ this.UploadForm ]
                        },
                        {   title : 'S I M P A N',
                            iconCls: 'silk-disk',
                            region: 'center',
                            closable : false,
                            deferredRender : false,
                            preventBodyReset: true,
                            items: [{
                                contentEl: 'simpan_content',
                            },]
                        },
                        ]
                    }
                    ]
                });

                Ext.override(Ext.form.TextField, {
                    validator:function(text){
                        return (text.length==0 || Ext.util.Format.trim(text).length!=0);
                    }
                });
            },
            "finalize_comp_and_layout" : function()
            {   // overide TextField Validation to exclude empty string
                this.DataStore.on( 'load', function( store, records, options )
                {   //check the fields
                    Ext.each(records[0].store.fields.items,
                        function(the_field)
                        {   //console.log(the_field.name);
                            switch (the_field.name)
                            {   //exclude columns
                                case "jenjang_sekolah_name" :
                                case "payment_confirm_by_name" :
                                case "observation_date" :
                                case "observation_result" :
                                case "commitment_date" :
                                {};
                                break;
                                //
                                case "created_by" :
                                case "modified_date" :
                                case "modified_by" :
                                case "payment_by" :
                                case "jenis_bayar" :
                                case "payment_amount" :
                                case "payment_date" :
                                case "payment_confirm_by" :
                                case "payment_confirm_date" :
                                {}; //exclude above fields
                                break;
                                case "tanggal_lahir" :
                                case "created_date" :
                                {
                                    Ext.getCmp('ppdb_'+the_field.name).setValue(alfalah.ppdb.shortdateRenderer(records[0].data[the_field.name],'Y-m-d', 'd/m/Y'));
                                };
                                break;
                                default :
                                {
				    Ext.getCmp('ppdb_'+the_field.name).setValue(records[0].data[the_field.name]);
				    if (the_field.name == "no_registration")
                                    {   Ext.getCmp('upload_'+the_field.name).setValue(records[0].data[the_field.name]);
                                    };
                                };
                                break;
                            };
                        });
                });

                this.ParentsDS.on( 'load', function( store, records, options )
                {   //check the fields
                    Ext.each(records[0].store.fields.items,
                        function(the_field)
                        {   //console.log(the_field.name);
                            switch (the_field.name)
                            {   //exclude columns
                                case "nama_wali_ibu" :
                                case "alamat_wali_ibu" :
                                case "tempat_lahir_wali_ibu" :
                                case "tanggal_lahir_wali_ibu" :
                                case "telp_rumah_wali_ibu" :
                                case "telp_hp_wali_ibu" :
                                case "hubungan_wali_ibu" :
                                case "agama_wali_ibu" :
                                case "pendidikan_wali_ibu" :
                                case "pekerjaan_wali_ibu" :
                                case "jenis_pekerjaan_wali_ibu" :
                                case "jabatan_wali_ibu" :
                                case "institusi_wali_ibu" :
                                case "institusi_alamat_wali_ibu" :
                                case "institusi_telp_wali_ibu" :
                                case "penghasilan_wali_ibu" :
                                case "pengeluaran_wali_ibu" :
                                case "tanggal_lahir_wali_ibu" :
                                {};
                                break;
                                case "tanggal_lahir_ayah" :
                                case "tanggal_lahir_ibu" :
                                case "tanggal_lahir_wali_ayah" :
                                case "tanggal_lahir_wali_ibu" :
                                {
                                    Ext.getCmp('ppdb_'+the_field.name).setValue(alfalah.ppdb.shortdateRenderer(records[0].data[the_field.name],'Y-m-d', 'd/m/Y'));
                                };
                                break;
                                default :
                                {
                                    Ext.getCmp('ppdb_'+the_field.name).setValue(records[0].data[the_field.name]);
                                };
                                break;
                            };
                        });
                });
                
		this.FilesDS.on( 'load', function( store, records, options )
                {   //check the fields
                    Ext.each(records,
                        function(the_record){
                            Ext.getCmp('upload_photo_'+the_record.data.type).setValue(the_record.data.file_name);
                        }
                    );
		}); 

                this.DataStore.load();
		this.ParentsDS.load();
		this.FilesDS.load();
            },
            "initialize": function()
            {   Ext.QuickTips.init();

                this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            rpMoney : function(v) {
                    v = (Math.round((v-0)*100))/100;
                    v = (v == Math.floor(v)) ? v + ".-" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
                    v = String(v);
                    var ps = v.split('.'),
                        whole = ps[0],
                        sub = ps[1] ? '.'+ ps[1] : '.-',
                        r = /(\d+)(\d{3})/;
                    while (r.test(whole)) {
                        whole = whole.replace(r, '$1' + ',' + '$2');
                    }
                    v = whole + sub;
                    if (v.charAt(0) == '-') {
                        return '-Rp ' + v.substr(1);
                    }
                    return "Rp. " +  v;
                },
            // combo grid column renderer
            comboRenderer : function(combo, value)
            {   if (Ext.isEmpty(value)) { return "";    }
                else
                {   var record = combo.findRecord(combo.valueField, value);
                    return record ? record.get(combo.displayField) : combo.valueNotFoundText;
                };
            },
            // date grid column renderer
            dateRenderer : function(value)
            {   if (Ext.isEmpty(value))
                {   result = "";    }
                else
                {   the_value = Date.parseDate(value.substr(0,19), "Y-m-d H:i:s", true);
                    result = the_value.format('d/m/Y H:i:s');
                };
                return result;
            },
            shortdateRenderer : function(value, sourceFormat, targetFormat)
            {   if (Ext.isEmpty(value))
                {   result = "";    }
                else
                {   result = Date.parseDate(value.substr(0,10), sourceFormat, true).format(targetFormat); };
                return result;
            },
            printButton: function(button, event, DataStore)
            {   document.location.href = DataStore.proxy.url+'?'+Ext.urlEncode(DataStore.baseParams)+'&pt='+button.print_type;
            },
            printOut: function(button, event, the_url, the_parameter)
            {   document.location.href = the_url+'?'+Ext.urlEncode(the_parameter)+'&pt='+button.print_type;
            },
            gridColumnWrap : function(data)
            {   return '<div style="white-space:normal !important;">'+ data +'</div>';
            },
            copy_below : function (button, event, the_datastore, the_xy, the_field)
            {   //copy below
                the_record_count = the_datastore.getCount();
                for (var the_x = the_xy[0]+1; the_x < the_record_count; the_x++)
                {   a = the_datastore.getAt(the_xy[0]).get(the_field);
                    the_datastore.getAt(the_x).set(the_field, a);
                };
            },
            getCookie : function(value)
            {   cName = "";
                pCOOKIES = new Array();
                pCOOKIES = document.cookie.split('; ');
                for(bb = 0; bb < pCOOKIES.length; bb++)
                {   NmeVal  = new Array();
                    NmeVal  = pCOOKIES[bb].split('=');
                    if(NmeVal[0] == value)
                    {   cName = unescape(NmeVal[1]);    };
                }
                return cName;
            },
            newDataStore : function(new_url, is_remote, new_base_param )
            {   var newDS =  new Ext.data.Store(
                {   proxy:  new Ext.data.HttpProxy(
                    {   method: 'GET',
                        url: new_url,
                    }),
                    baseParams: new_base_param,
                    reader: new Ext.data.JsonReader(),
                    remoteFilter : is_remote
                });

                return newDS;
            },
            getSearchParameter : function(search_items)
            {   var the_parameter = {};
                Ext.each(search_items,
                    function(item)
                    {   v_param = {};
                        // console.log(item);
                        if (Ext.isDate(Ext.get(item.id).getValue()) )
                        {   v_param[ item.cid ] = Ext.get(item.id).getValue().format("d/m/Y");  }
                        else //check additional transformation items
                        {   switch (item.xtype)
                            {   case "combo" :
                                    v_param[ item.cid] = Ext.getCmp(item.id).getValue();
                                break;
                                default :
                                    v_param[ item.cid ] = Ext.getCmp(item.id).getValue();
                                break;
                            };
                        };
                        the_parameter = Ext.apply( the_parameter, v_param);
                    }
                );
                return the_parameter;
            },
            getHeadData : function(the_fields)
            {   var head_data = [];
                var v_json = {};
                Ext.each(the_fields,
                    function(the_field)
                    {   console.log(the_field);
                        // a = Ext.getCmp(the_field);
                        if (Ext.isDate(the_field.getValue()) )
                        {   v_json[the_field.name] = the_field.getValue().format("d/m/Y");  }
                        else if ( the_field.xtype == "combo") // is_combo_box
                        {   if (the_field.lastSelectionText == "Pilihlah")
                            {   v_json[the_field.name] = "";  }
                            else
                            {   //v_json[the_field.name] = the_field.lastSelectionText;
                                v_json[the_field.name] = the_field.getValue();
                            }
                        }
                        else
                        {   v_json[the_field.name] = the_field.getValue();  };

                    });
                head_data.push(v_json);
                return head_data;
            },
            getDetailData : function(the_records)
            {   var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        if (Ext.isEmpty(the_record.store)) {} // deleted record
                        else
                        {   Ext.iterate(the_record.data, function(key, value)
                            {   v_data = {};
                                if (Ext.isDate( value ) )
                                {   v_data[ key ] = value.format("d/m/Y");  }
                                else
                                {   v_data[ key ] = value;  };
                                v_json= Ext.apply( v_json, v_data);
                            });
                            json_data.push(v_json);
                        };
                    });
                return json_data;
            },
            validateFields : function(the_fields)
            {   //console.log('validateFields');
                var is_validate = true;
                Ext.each( the_fields,
                    function(the_field)
                    {   a = Ext.getCmp(the_field);
                        b = a.getValue();
                        if (Ext.isEmpty(b) ) { is_validate = false;};

                        if ( (a.validate() == false) || (is_validate == false) )
                        {   is_validate = false;
                            Ext.Msg.show(
                            {   title:'E R R O R ',
                                msg: 'Column '+a.fieldLabel+' is mandatory.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                            return false;
                        };
                    });
                return is_validate;
            },
            validateGridQuantity : function(the_records, the_fields)
            {   var is_validate = true;
                Ext.each(the_records,
                    function(the_record)
                    {   Ext.iterate(the_record.data, function(key, value)
                        {   Ext.each(the_fields,
                                function(the_field)
                                {   if ( (the_field == key) && (value ==0) )
                                    {   is_validate = false;
                                        Ext.Msg.show(
                                        {   title   :'E R R O R ',
                                            msg     : 'Found zero quantity <br>on Column '+key+
                                                      '<br>of items <br>'+the_record.data.item_name,
                                            buttons : Ext.Msg.OK,
                                            icon    : Ext.MessageBox.ERROR,
                                            width   : 200,
                                        });
                                        return false;  // break looping
                                    };
                                });
                        });
                    });
                return is_validate;
            },
            jsonGet : function( the_url, the_headers, the_parameter, the_success)
            {   Ext.Ajax.request(
                {   method: 'GET',
                    url: the_url,
                    headers: the_headers,
                    params: the_parameter,
                    success: the_success,
                    failure: function()
                    {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                });
            },
            jsonSave : function(the_url, the_params, the_datastore, the_scope)
            {   Ext.Ajax.request(
                {   method: 'POST',
                    url: the_url,
                    params  : the_params,
                    success : function(response, options)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   the_datastore.reload();    };
                    },
                    failure: function(response, options)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'S E R V E R    E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        };
                    },
                    scope: the_scope
                });
            },
            submitGrid : function( the_datastore, the_url, the_headers, the_parameter)
            {   Ext.Ajax.request(
                {   method: 'POST',
                    url: the_url,
                    headers: the_headers,
                    params: the_parameter,
                    success: function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   the_datastore.reload(); };
                    },
                    failure: function()
                    {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    // scope: this
                });
            },
            submitForm : function( the_form_name, the_url, the_headers, the_parameter)
            {   var the_form = Ext.getCmp(the_form_name);
                Ext.Ajax.request(
                {   method: 'POST',
                    url: the_url,
                    headers: the_headers,
                    params: the_parameter,
                    success: function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   //the_form.destroy();
                            the_url = "{{ url('/ppdb/0/2') }}"+'?'+'&id='+the_response.server_message;
                            window.location = the_url;
                        };
                    },
                    failure: function()
                    {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    // scope: this
                });
            },
            // forms grid save records
            Form_save : function(button, event)
            {   var head_data = [];
                var json_data = [];
                var modi_data = [];
                var v_json = {};
                the_ppdb = alfalah.ppdb;

                // header validation
                if (the_ppdb.validateFields([
                    'ppdb_nama_lengkap',
                    'ppdb_gender_name', 'ppdb_tinggi_badan','ppdb_berat_badan','ppdb_bloodtype','ppdb_penyakit_bawaan','ppdb_keadaan_siswa',
                    'ppdb_tinggaldengan','ppdb_anakke', 'ppdb_brpsaudara',
                    'ppdb_sekolah_asal_nama', 'ppdb_sekolah_asal_status','ppdb_sekolah_asal_lulus','ppdb_sekolah_asal_internal',
                    'ppdb_nama_ayah','ppdb_alamat_ayah','ppdb_tempat_lahir_ayah', 'ppdb_tanggal_lahir_ayah', 'ppdb_telp_rumah_ayah','ppdb_telp_hp_ayah','ppdb_hubungan_ayah',
                    'ppdb_agama_ayah', 'ppdb_pendidikan_ayah','ppdb_pekerjaan_ayah','ppdb_jenis_pekerjaan_ayah','ppdb_jabatan_ayah','ppdb_penghasilan_ayah',
                    'ppdb_nama_ibu','ppdb_alamat_ibu','ppdb_tempat_lahir_ibu','ppdb_tanggal_lahir_ibu','ppdb_telp_rumah_ibu','ppdb_telp_hp_ibu','ppdb_hubungan_ibu',
                    'ppdb_agama_ibu','ppdb_pendidikan_ibu','ppdb_pekerjaan_ibu','ppdb_jenis_pekerjaan_ibu','ppdb_penghasilan_ibu',
                    'ppdb_nama_wali_ayah','ppdb_alamat_wali_ayah','ppdb_tempat_lahir_wali_ayah', 'ppdb_tanggal_lahir_wali_ayah','ppdb_telp_rumah_wali_ayah','ppdb_telp_hp_wali_ayah',
                    'ppdb_hubungan_wali_ayah', 'ppdb_agama_wali_ayah','ppdb_pendidikan_wali_ayah','ppdb_pekerjaan_wali_ayah', 'ppdb_jenis_pekerjaan_wali_ayah','ppdb_jabatan_wali_ayah',
                    'ppdb_penghasilan_wali_ayah',
                    'ppdb_expense_listrik',  'ppdb_expense_pdam','ppdb_expense_pulsa','ppdb_expense_monthly'
                    ]))
                {
                    data_siswa = the_ppdb.getHeadData(the_ppdb.Form.getForm().items.items);
                    data_sekolah = the_ppdb.getHeadData(the_ppdb.SekolahForm.getForm().items.items);
                    data_parent = the_ppdb.getHeadData(the_ppdb.ParentsForm.getForm().items.items);
                    data_wali = the_ppdb.getHeadData(the_ppdb.WaliForm.getForm().items.items);
                    data_expense = the_ppdb.getHeadData(the_ppdb.ExpenseForm.getForm().items.items);

                    head_data = Ext.apply( data_siswa[0], data_sekolah[0]);
                    dtl1_data = Ext.apply( data_parent[0], data_wali[0]);
                    dtl2_data = Ext.apply( data_expense[0] )
                    // submit data
                    the_ppdb.submitForm(
                        'ppdb_form',
                        "{{ url('/ppdb/1/2') }}",
                        {   'x-csrf-token': the_ppdb.sid },
                        {   task: 'save',
                            head : Ext.encode(head_data),
                            dtl1 : Ext.encode(dtl1_data),
                            dtl2 : Ext.encode(dtl2_data),
                            json : {},
                        });
                };
            },
	    // forms upload file
            Form_upload : function(button, event)
            {   the_form = alfalah.ppdb.UploadForm.getForm();
                if( the_form.isValid())
                {
                    the_form.submit(
                    {   clientValidation : true,
                        method           : 'POST',
                        url              : "{{ url('/ppdb/upload') }}",
                        headers : {
                            'Content-type' : 'multipart/form-data; charset=UTF-8',
                            'enctype'      : 'multipart/form-data; charset=UTF-8',
                            // 'x-csrf-token' : alfalah.ppdb.sid,
                        },
                        waitMsg: 'Uploading your photo...',
                        success : function(form, action)
                        {   var the_response = Ext.decode(action.response.responseText);
                            if (the_response.success == false)
                            {   Ext.Msg.show(
                                {   title :'E R R O R ',
                                    msg : 'Server Message : '+'\n'+the_response.message,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                            }
                            else
                            {  Ext.Msg.show(
                                {   title :'I N F O ',
                                    msg : 'Record Saved.',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.INFO
                                });
                                alfalah.ppdb.UploadForm.destroy();
                            };
                        },
                        failure:function(form, action)
                        {
                            console.log("failure");
                            console.log("form");
                            console.log(form);
                            console.log("action");
                            console.log(action);

                            if(action.failureType == 'server')
                            {   Ext.Msg.show(
                                {   title :'E R R O R ',
                                    msg : 'Server Message : '+'\n'+action.result.server_message,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                 });
                            }
                            else
                            {   Ext.Msg.show(
                                {   title :'W A R N I N G ',
                                    msg : 'Server is unreachable : '+'\n'+ action.response.status + '\n' + action.response.statusText,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                            };
                        }
                    });
                }
            },
	    // forms Login
            Form_login : function(button, event)
            {   var head_data = [];
                var json_data = [];
                var modi_data = [];
                var v_json = {};
                the_ppdb = alfalah.ppdb;

                // header validation
                if (the_ppdb.validateFields([
                    'ppdb_no_registration', 'ppdb_nama_lengkap', 'ppdb_tanggal_lahir', 'ppdb_ibu_kandung']))
                {
                    head_data = the_ppdb.getHeadData(the_ppdb.LoginForm.getForm().items.items);
                    // var the_form = Ext.getCmp(the_form_name);
                    Ext.Ajax.request(
                    {   method      : 'POST',
                        url         : "{{ url('/ppdb/1/1') }}", // login registerd ppdb
                        headers     : {   'x-csrf-token': the_ppdb.sid },
                        params      : {   task: 'login',
                            head    : Ext.encode(head_data),
                            json    : {},
                        },
                        success: function(response)
                        {   var the_response = Ext.decode(response.responseText);
                            if (the_response.success == false)
                            {   Ext.Msg.show(
                                {   title :'E R R O R ',
                                    msg : 'Server Message : '+'\n'+the_response.message,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                 });
                            }
                            else
                            {
                                // console.log(response);
                                the_url = "{{ url('/ppdb/1/0') }}"+'?'+'&id='+the_response.server_message;
                                window.location = the_url;
                            };
                        },
                        failure: function()
                        {   Ext.Msg.alert("Login Failed : ", "Server communication failure");
                        },
                        // scope: this
                    });

                };
            },

        }; // end of public space
    }(); // end of app
    Ext.onReady(alfalah.ppdb.initialize, alfalah.ppdb);
    </script>
</head>
<body>
    <div id="north_content" class="x-hide-display">
        <div align="center">
            <br>
            <h1><big>PPDB ONLINE {{ $TAHUNAJARAN }}</big></h1>
            <h1><big>LEMBAGA PENDIDIKAN MASJID AL FALAH DARUSSALAM TROPODO</big></h1>
      </div>
    </div>
    <div id="ppdb_content" class="x-hide-display">
        <div align="left">
            <ol><h1><big>Tata Cara Pendaftaran COMPLETE</big></h1></ol>
            <ol>
                <li>Melengkapi Persyaratan</li><br>
                <li>Mengisi Formulir</li><br>
                <li>Melakukan Pembayaran</li><br>
                <li>Konfirmasi Pembayaran</li><br>
                <li>Dapatkan No Registrasi Test</li><br>
                <li>Ikuti Test Masuk</li><br>
            </ol>
        </div>
    </div>
    <div id="login_content" class="x-hide-display">
        <div align="left">
            <ol><h1><big>Sudah Registrasi ? Login Disini</big></h1></ol>
        </div>
    </div>
    <div id="simpan_content" class="x-hide-display">
        <div align="left">
            <ol><h1><big>PASTIKAN DATA ANDA SUDAH BENAR !!!</big></h1></ol>
            <ol>
                <li>Semua data sudah terisi</li><br>
                <li>Tidak ada data yang kosong</li><br>
                <li>Semua pilihan sudah terisi</li><br>
                <li>Jika sudah benar, silahkan menekan tombol simpan dibawah ini
                    <br><div id="SaveBtn" style="margin:15px 0;"></div>
                </li>
                <br>
            </ol>
        </div>
    </div>
    <div id="photo_content" class="x-hide-display">
        <div align="left">
            <ol><h1><big>Syarat Upload Foto</big></h1></ol>
            <ol>
                <li>Setiap Foto berukuran max 1 MB</li><br>
                <li>Setiap Foto berformat jpg, jpeg, png, gif</li><br>
                <li>Jika sudah benar, silahkan menekan tombol simpan disamping ini
                </li>
                <br>
            </ol>
        </div>
    </div>
</body>
</html>
