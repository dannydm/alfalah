<!-- Using default Layout -->
@extends('layouts_backend._iframe_backend')
<!-- load your extry css styles -->
@section('extra_styles')
<link rel="stylesheet" href="../../w2ui/w2ui-1.5.rc1.min.css" />
<link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="../../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
@endsection
<!-- load your main content page -->
@section('content')
<!-- Main content -->
<div id="layout" style="width: 100%; height: 100%;"></div>
<!-- /.content -->
@endsection
<!-- load your extra js scripts -->
@section('extra_scripts')
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="../../w2ui/w2ui-1.5.rc1.min.js"></script>
<script src="../../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">

alfalah = parent.alfalah;
alfalah.namespace('alfalah.news');
// create application
alfalah.news = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    // this.tabId = '{{ $TABID }}';
    this.config;
    this.popconfig;
    this.pconfig;
    this.news_editor = false;
    // private functions
    // public space
    return {
        centerPanel : 0,
        the_records : [],
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',
        // public methods
        initialize: function()
        {   console.log('initialize');
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   console.log('prepare_component');
            var pstyle = 'background-color: #F5F6F7; border: 1px solid #dfdfdf; padding: 1px;';

            this.config = {
                news_layout : 
                {   name    : 'news_layout',
                    padding : 4,
                    panels  : [
                        // { type: 'top',  size: 50, resizable: true, style: pstyle, content: 'top' },
                        // { type: 'left', size: 200, resizable: true, style: pstyle, content: 'left' },
                        {   type: 'main', style: pstyle, 
                            // toolbar: 
                            // {   
                            //     items: [
                            //     {   type: 'button', text: 'Add', icon: 'fa fa-plus',
                            //         onClick: function(event)
                            //         {   w2ui.news_grid.newRecord(); } 
                            //     },
                            //     {   type: 'button', text: 'Edit Content', icon: 'fa fa-edit',
                            //         onClick: function(event)
                            //         {   // alfalah.news.pop_up(event);
                            //             var id = w2ui.news_grid.getSelection()[0];
                            //             if (id)
                            //             {
                            //                 alfalah.core.ajax(
                            //                     '/news/2/5',    //the_url, 
                            //                     {               //the_parameters,
                            //                       '_token' : alfalah.news.sid,
                            //                       json : JSON.stringify([
                            //                         { id : w2ui.news_grid.getSelection()[0] 
                            //                         }])
                            //                     },                  
                            //                     "POST",             //the_type, 
                            //                     function(response)  //fn_success
                            //                     { w2ui.news_grid.reload(); 
                            //                     },
                            //                     function(response)  //fn_fail, 
                            //                     { console.log("FAILED");
                            //                       console.log(response);
                            //                     },
                            //                     null                //fn_always
                            //                 );

                            //                 alfalah.news.PopupNews(event);
                            //                 // $("#popup_content").wysihtml5();
                            //                 // tinymce.init({selector: '#popup_content'});
                            //             }
                            //             else
                            //             {
                            //                 console.log("no record selected");
                            //             };
                                        
                            //         } 
                            //     },
                            //     {   type: 'button', text: 'Save', icon: 'fa fa-save',
                            //         onClick: function(event)
                            //         {   //alfalah.news.pop_up_open(event);
                            //             document.getElementById('news_form').submit()
                            //             // alfalah.core.submitGrid(
                            //             //     w2ui.news_grid, 
                            //             //     '/news/2/1',
                            //             //     alfalah.news.sid
                            //             // );
                            //         }
                            //     },
                            //     {   type: 'break'},
                            //     {   type: 'button', text: 'Delete', 
                            //         icon: 'fa fa-remove',
                            //         onClick: function(event)
                            //         { 
                            //             // alfalah.news.openPopup(event);

                            //             alfalah.core.ajax(
                            //                 '/news/2/2',    //the_url, 
                            //                 {               //the_parameters,
                            //                   '_token' : alfalah.news.sid,
                            //                   json : JSON.stringify([
                            //                     { id : w2ui.news_grid.getSelection()[0] 
                            //                     }])
                            //                 },                  
                            //                 "POST",             //the_type, 
                            //                 function(response)  //fn_success
                            //                 { w2ui.news_grid.reload(); 
                            //                 },
                            //                 function(response)  //fn_fail, 
                            //                 { console.log("FAILED");
                            //                   console.log(response);
                            //                 },
                            //                 null                //fn_always
                            //             );
                            //         } 
                            //     },
                            //     {   type: 'break'},
                            //     // {   type: 'button', text: 'PDF',
                            //     //     print_type : 'PDF', 
                            //     //     icon: 'fa fa-file-pdf-o',
                            //     //     onClick: function (event) 
                            //     //     {   alfalah.news.print_out(event); } 
                            //     // },
                            //     {   type: 'button', text: 'XLS',
                            //         print_type : 'XLS',  
                            //         icon: 'fa fa-file-excel-o',
                            //         onClick: function (event) 
                            //         {   alfalah.news.print_out(event); }  
                            //     }],
                            // },
                        },
                        {   type: 'right', size: 200, resizable: true, style: pstyle, 
                            toolbar: 
                            {   items: [
                                {   type: 'button', text: 'Search', 
                                    icon: 'fa fa-search',
                                    onClick: function(event )
                                    {   console.log("search click");
                                        alfalah.news.search_handler(event); 
                                    }
                                }],
                                // onClick: function (event) {
                                //   console.log("search clisck");
                                //   console.log(event);
                                // }
                            }
                        },
                        {   type: 'preview', size: '50%', resizable: true, 
                            // hidden: true, 
                        },
                    ]
                },
                // news_grid : 
                // {   name : 'news_grid',
                //     show : 
                //     {
                //         toolbar       : true,
                //         toolbarDelete : true,
                //     },
                //     columns: [
                //         { field: 'fname', caption: 'First Name', size: '33%', sortable: true, searchable: true },
                //         { field: 'lname', caption: 'Last Name', size: '33%', sortable: true, searchable: true },
                //         { field: 'email', caption: 'Email', size: '33%' },
                //         { field: 'sdate', caption: 'Start Date', size: '120px', render: 'date' }
                //     ],
                //     records: [
                //         { recid: 1, fname: 'John', lname: 'Doe', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                //         { recid: 2, fname: 'Stuart', lname: 'Motzart', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                //         { recid: 3, fname: 'Jin', lname: 'Franson', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                //         { recid: 4, fname: 'Susan', lname: 'Ottie', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                //         { recid: 5, fname: 'Kelly', lname: 'Silver', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                //         { recid: 6, fname: 'Francis', lname: 'Gatos', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                //         { recid: 7, fname: 'Mark', lname: 'Welldo', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                //         { recid: 8, fname: 'Thomas', lname: 'Bahh', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                //         { recid: 9, fname: 'Sergei', lname: 'Rachmaninov', email: 'jdoe@gmail.com', sdate: '4/3/2012' }
                //     ],
                //     onClick: function(event) {
                //         var news_grid = this;
                //         var news_form = w2ui.news_form;
                //         console.log('event');
                //         console.log(event);
                //         event.onComplete = function () {
                //             var sel = news_grid.getSelection();
                //             console.log('sel');
                //             console.log(sel);
                //             if (sel.length == 1) {
                //                 news_form.recid  = sel[0];
                //                 news_form.record = $.extend(true, {}, news_grid.get(sel[0]));
                //                 console.log('news_form.record');
                //                 console.log(news_form.record);
                //                 news_form.refresh();
                //             } else {
                //                 news_form.clear();
                //             }
                //         }
                //     }
                // },
                // news_form : 
                // { 
                //     header: 'Edit News Record',
                //     name: 'news_form',
                //     fields: [
                //         { name: 'recid', type: 'text', html: { caption: 'ID', attr: 'size="10" readonly' } },
                //         { name: 'fname', type: 'text', required: true, html: { caption: 'First Name', attr: 'size="40" maxlength="40"' } },
                //         { name: 'lname', type: 'text', required: true, html: { caption: 'Last Name', attr: 'size="40" maxlength="40"' } },
                //         { name: 'email', type: 'email', html: { caption: 'Email', attr: 'size="30"' } },
                //         { name: 'sdate', type: 'date', html: { caption: 'Date', attr: 'size="10"' } }
                //     ],
                //     actions: 
                //     {   
                //         Reset: function () 
                //         {   this.clear();   },
                //         Save: function () 
                //         {   var errors = this.validate();
                //             if (errors.length > 0) return;
                //             if (this.recid == 0) {
                //                 w2ui.news_grid.add($.extend(true, { recid: w2ui.news_grid.records.length + 1 }, this.record));
                //                 w2ui.news_grid.selectNone();
                //                 this.clear();
                //             } else 
                //             {
                //                 w2ui.news_grid.set(this.recid, this.record);
                //                 w2ui.news_grid.selectNone();
                //                 this.clear();
                //             };
                //         },
                //     },
                // },
                news_grid: 
                {   name    : 'news_grid',
                    // url     : '/news/2/0',
                    ref_url : '/news/2/0',
                    method  : 'GET',
                    recid   : 'news_id',
                    // recid : 'keyid',
                    // postData : {
                    //     s : 'init',
                    //     '_token' : alfalah.news.sid,
                    //     'x-csrf-token': alfalah.news.sid
                    // },
                    selectType: 'cell',
                    show: { 
                        // toolbar: true,
                        // toolbarSave: true,
                        lineNumbers : true,
                        footer: true,
                    },
                    columns: [
                        {   field: 'news_id', caption: 'ID', size: '50px', 
                            sortable: true, resizable: true, searchable: true, 
                            editable: { type: 'text' } },
                        {   field: 'title', caption: 'Title', size: '200px', 
                            sortable: true, resizable: true, searchable: true, 
                            editable: { type: 'text' } },
                        {   field: 'category_1_name', caption: 'Category', 
                            size: '100px', sortable: true, resizable: true, 
                            editable: { 
                                type: 'select', 
                                items: [
                                    { id: 'ALL',        text: 'ALL'      },
                                    { id: 'TK',         text: 'TK'       },
                                    { id: 'SD',         text: 'SD'       },
                                    { id: 'SMP',        text: 'SMP'      },
                                    { id: 'SMA',        text: 'SMA'      },
                                    { id: 'perSiswa',   text: 'perSiswa' },
                                ]}, 
                            // render: function (record, index, col_index) 
                            // {   console.log('record');
                            //     console.log(record);
                            //     console.log('index');
                            //     console.log(index);
                            //     console.log('col_index');
                            //     console.log(col_index);
                            //     console.log('this');
                            //     console.log(this.columns[2].editable.items);
                            //     var html = '';
                            //     var people = this.columns[2].editable.items;
                            //     for (var p in people) 
                            //     {   if (people[p].id == this.getCellValue(index, col_index)) html = people[p].text;
                            //     }
                            //     return html;
                            // }
                        },
                        {   field: 'content', caption: 'Content', size: '200px', 
                            sortable: true, resizable: true, searchable: true, 
                            // editable: { type: 'text' } 
                        },
                        {   field: 'status_name', caption: 'Status', 
                            size: '100px', sortable: true, resizable: true, 
                            editable: { 
                                type: 'select', 
                                items: [
                                    { id: 'Active',   text: 'Active'   },
                                    { id: 'Inactive', text: 'Inactive' },
                                ]},
                        },
                    ],
                    newRecord : function()
                    {   console.log('add new record');
                        console.log(this);
                        this.add({
                            recid           : "",
                            news_id         : "",
                            title           : "New Title",
                            category_1_name : "ALL",    // All audience
                            status_name     : "Inactive",     // Inactive news
                            content         : "New Content",
                            created_date    : ""
                        }, true); // as fist record
                    },
                    onDblClick : function(event)
                    {   console.log("onDblClick");
                        console.log(event);
                        console.log("this");
                        console.log(this);
                    },
                    // onClick : function(event) 
                    // {   var grid = this;
                    //     var form = w2ui.news_editor; //w2ui.form;
                    //     console.log(event);
                    //     event.onComplete = function () 
                    //     {
                    //         var sel = grid.getSelection();
                    //         console.log(sel);
                    //         if (sel.length == 1) 
                    //         {   form.news_id  = sel[0];
                    //             form.record = $.extend(true, {}, grid.get(sel[0]));
                    //             form.refresh();
                    //         } 
                    //         else 
                    //         {
                    //             form.clear();
                    //         }
                    //     }
                    // },
                    onClick: function(event) {
                        var news_grid = this;
                        var news_form = w2ui.news_form;
                        console.log(event);
                        console.log('event');
                        event.onComplete = function () {
                            var sel = news_grid.getSelection();
                            w2ui.news_grid.getSelection()
                            console.log('sel');
                            console.log(sel);
                            if (sel.length == 1) {
                                news_form.recid  = sel[0].recid;
                                news_form.record = $.extend(true, {}, news_grid.get(sel[0].recid));
                                console.log('news_form.record');
                                console.log(news_form.record);
                                news_form.refresh();
                            } else {
                                news_form.clear();
                            }
                        }
                    }
                },
                news_form : 
                { 
                    header: 'Edit News Record',
                    name: 'news_form',
                    fields: [
                        // { name: 'news_id', type: 'text', html: { caption: 'ID', attr: 'size="10" readonly hidden' } },
                        // { name: 'title', type: 'text', required: true, html: { caption: 'Title', attr: 'size="40" maxlength="40"' } },
                        // { name: 'category_1_name', type: 'text', required: true, html: { caption: 'Category', attr: 'size="40" maxlength="40"' } },
                        { name: 'content', type: 'text', required: true, html: { caption: 'Content', attr: 'size="40" maxlength="40"' } },
                        // { name: 'status_name', type: 'text', required: true, html: { caption: 'Status', attr: 'size="40" maxlength="40"' } },
                    ],
                    actions: 
                    {   
                        Reset: function () 
                        {   this.clear();   },
                        Save: function () 
                        {   var errors = this.validate();
                            if (errors.length > 0) return;
                            if (this.recid == 0) {
                                w2ui.news_grid.add($.extend(true, { recid: w2ui.news_grid.records.length + 1 }, this.record));
                                w2ui.news_grid.selectNone();
                                this.clear();
                            } else 
                            {
                                w2ui.news_grid.set(this.recid, this.record);
                                w2ui.news_grid.selectNone();
                                this.clear();
                            };
                        },
                    },
                },
                search_form:
                {   name  : 'search_form',
                    // url   : 'doremi/post',
                    fields: [
                        {   name: 'news_id', type: 'text', required: true },
                        {   name: 'title', type: 'text', required: true },
                    ],
                    // style: 'border: 0px; background-color: transparent;',
                    formHTML: 
                        '<div id="search_form" style="width: 20px;">'+
                        '   <div class="w2ui-page page-0">'+
                        '       <div class="w2ui-field">'+
                        '           <label style="width:30px;">IDL</label>'+
                        '           <div style="width:50px; margin-left: 40px;">'+
                        '               <input name="news_id" type="text" maxlength="15" size="15"/>'+
                        '           </div>'+
                        '       </div>'+
                        '       <div class="w2ui-field">'+
                        '           <label style="width:30px; ">Title</label>'+
                        '           <div style="width:50px; margin-left: 40px;">'+
                        '               <input name="title" type="text" maxlength="50" size="15"/>'+
                        '           </div>'+
                        '       </div>'+
                        '    </div>'+
                        '</div>',
                    actions: {
                        reset: function () {   this.clear();   },
                        save: function () { this.save();    }
                    }
                },
            };
        },
        // build the layout
        build_layout: function()
        {   console.log('build_layout');
            $('#layout').w2layout(this.config.news_layout);
            w2ui.news_layout.content('main', $().w2grid(this.config.news_grid));
            w2ui.news_layout.content('right', $('#search_form').w2form(this.config.search_form));
            w2ui.news_layout.content('preview', $().w2form(this.config.news_form));
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log('finalize_comp_and_layout 101'); },
        getSearchFormParameter: function(the_form)
            {   the_searchs = "";
                $(the_form).each(function()
                    {   the_searchs = $(this).find(':input');
                    });
                the_parameter = alfalah.core.getSearchParameter(the_searchs);
                the_parameter = $.extend( the_parameter, 
                                {   s:"form"    });
                return the_parameter;
            },
            search_handler: function(event)
            {   the_parameter = this.getSearchFormParameter("#search_form");
                // w2utils.settings.dataType = 'HTTP';
                // w2ui.news_grid.load(w2ui.news_grid.ref_url);
                w2ui.news_grid.load(w2ui.news_grid.ref_url+ '?'+$.param(the_parameter));
                
                // w2ui.news_grid.request(
                //     'GET', 
                //     the_parameter,
                //     // JSON.stringify(the_parameter), 
                //     w2ui.news_grid.ref_url
                // );                                 
                // 
                // this.DataStore.baseParams = Ext.apply( the_parameter,
                //     {   task: alfalah.news.task,
                //         act: alfalah.news.act,
                //         a:2, b:0, s:"form",
                //         limit:this.page_limit, start:this.page_start });
                // this.DataStore.reload();
                // w2ui.news_grid.postData = JSON.stringify(the_parameter);
                // w2ui.news_grid.method = 'POST';
                // w2ui.news_grid.postData = the_parameter;
                // w2ui.news_grid.searchData = the_parameter;
                // w2ui.news_grid.search();
                // w2utils.settings.RESTfull = true;
                // w2utils.settings.dataType = 'JSON';
            },
            
    }
    }(); // end of app
    $(document).ready(alfalah.news.initialize());
</script>


@endsection
