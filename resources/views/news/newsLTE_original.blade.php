<!-- Using default Layout -->
@extends('layouts_backend._iframe_backend')
<!-- load your extry css styles -->
@section('extra_styles')
<link rel="stylesheet" href="../../w2ui/w2ui-1.5.rc1.min.css" />
<link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="../../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<style>
.w2ui-field input {
    width: 20px;
    text-align: left;

}
.w2ui-field label {
    padding: 5px;
}
.w2ui-field > div {
    margin-left: 1px;
}
</style>
@endsection
<!-- load your main content page -->
@section('content')
<!-- Main content -->
<div id="news_layout" style="width: 100%; height: 100%;">
    <div id="news_grid"></div>
    <!-- <div id="news_editor" class="w2ui-page page-0">
        <div class="w2ui-field">
            <label style="text-align: left;">News Content</label>
            <form id="news_form" method="post"">
                <textarea name="news_content"></textarea>
            </form>
        </div>
    </div> -->
</div>

<!-- <div id="popup_layout" style="width: 100%; height: 100%;">
    <div id="popup_editor"></div>
</div> -->

<!-- <div id="fpopup" style="width: 100%; height: 100%;">
    <form id="fpopup_form" method="post">
        <textarea id="fpopup_content" name="fpopup_content">Testing</textarea>
    </form>
</div> -->
<!-- /.content -->
@endsection
<!-- load your extra js scripts -->
@section('extra_scripts')
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- FastClick -->
<!-- <script src="../../bower_components/fastclick/lib/fastclick.js"></script> -->
<!-- AdminLTE App -->
<!-- <script src="../../dist/js/adminlte.min.js"></script> -->
<!-- AdminLTE for demo purposes --><!-- 
<script src="dist/js/demo.js"></script> -->
<!--AdminLTE Iframe-->
<!-- <script src="dist/js/app_iframe.js"></script> -->
<script src="../../w2ui/w2ui-1.5.rc1.min.js"></script>
<script src="../../js/tinymce/tinymce.min.js"></script>
<!-- <script>
  tinymce.init({    selector: '#mytextarea' });
</script> -->
<script type="text/javascript">
    // Define namespace
    alfalah = parent.alfalah;
    alfalah.namespace('alfalah.news');
    // create application
    alfalah.news = function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        // this.tabId = '{{ $TABID }}';
        this.config;
        this.popconfig;
        this.pconfig;
        this.news_editor = false;
        // private functions
        // public space
        return {
            centerPanel : 0,
            the_records : [],
            sid : '{{ csrf_token() }}',
            task : ' ',
            act : ' ',
            // public methods
            initialize: function()
            {   console.log('initialize');
                this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {   console.log('prepare_component');
                var pstyle = 'background-color: #F5F6F7; border: 1px solid #dfdfdf; padding: 1px;';

                this.config = {
                    news_grid: 
                    {   name    : 'news_grid',
                        // url     : '/news/2/0',
                        ref_url : '/news/2/0',
                        method  : 'GET',
                        recid   : 'news_id',
                        // recid : 'keyid',
                        // postData : {
                        //     s : 'init',
                        //     '_token' : alfalah.news.sid,
                        //     'x-csrf-token': alfalah.news.sid
                        // },
                        selectType: 'cell',
                        show: { 
                            // toolbar: true,
                            // toolbarSave: true,
                            lineNumbers : true,
                            footer: true,
                            
                        },
                        columns: [
                            {   field: 'news_id', caption: 'ID', size: '50px', 
                                sortable: true, resizable: true, searchable: true, 
                                editable: { type: 'text' } },
                            {   field: 'title', caption: 'Title', size: '200px', 
                                sortable: true, resizable: true, searchable: true, 
                                editable: { type: 'text' } },
                            {   field: 'category_1_name', caption: 'Category', 
                                size: '100px', sortable: true, resizable: true, 
                                editable: { 
                                    type: 'select', 
                                    items: [
                                        { id: 'ALL',        text: 'ALL'      },
                                        { id: 'TK',         text: 'TK'       },
                                        { id: 'SD',         text: 'SD'       },
                                        { id: 'SMP',        text: 'SMP'      },
                                        { id: 'SMA',        text: 'SMA'      },
                                        { id: 'perSiswa',   text: 'perSiswa' },
                                    ]}, 
                                // render: function (record, index, col_index) 
                                // {   console.log('record');
                                //     console.log(record);
                                //     console.log('index');
                                //     console.log(index);
                                //     console.log('col_index');
                                //     console.log(col_index);
                                //     console.log('this');
                                //     console.log(this.columns[2].editable.items);
                                //     var html = '';
                                //     var people = this.columns[2].editable.items;
                                //     for (var p in people) 
                                //     {   if (people[p].id == this.getCellValue(index, col_index)) html = people[p].text;
                                //     }
                                //     return html;
                                // }
                            },
                            {   field: 'content', caption: 'Content', size: '200px', 
                                sortable: true, resizable: true, searchable: true, 
                                // editable: { type: 'text' } 
                            },
                            {   field: 'status_name', caption: 'Status', 
                                size: '100px', sortable: true, resizable: true, 
                                editable: { 
                                    type: 'select', 
                                    items: [
                                        { id: 'Active',   text: 'Active'   },
                                        { id: 'Inactive', text: 'Inactive' },
                                    ]},
                            },
                            
                        ],
                        newRecord : function()
                        {   console.log('add new record');
                            console.log(this);
                            this.add({
                                recid           : "",
                                news_id         : "",
                                title           : "New Title",
                                category_1_name : "ALL",    // All audience
                                status_name     : "Inactive",     // Inactive news
                                content         : "New Content",
                                created_date    : ""
                            }, true); // as fist record
                        },
                        onDblClick : function(event)
                        {   console.log("onDblClick");
                            console.log(event);
                            console.log("this");
                            console.log(this);
                        },
                        onClick : function(event) 
                        {   var grid = this;
                            var form = w2ui.news_editor; //w2ui.form;
                            console.log(event);
                            event.onComplete = function () 
                            {
                                var sel = grid.getSelection();
                                console.log(sel);
                                if (sel.length == 1) 
                                {   form.news_id  = sel[0];
                                    form.record = $.extend(true, {}, grid.get(sel[0]));
                                    form.refresh();
                                } 
                                else 
                                {
                                    form.clear();
                                }
                            }
                        }
                    },
                    news_layout: 
                    {   name: 'news_layout',
                        panels: [
                            // { type: 'top',  size: 50, resizable: true, style: pstyle, content: 'top' },
                            // { type: 'left', size: 200, resizable: true, style: pstyle, content: 'left' },
                            {   type: 'main', style: pstyle, 
                                toolbar: 
                                {   
                                    items: [
                                    {   type: 'button', text: 'Add', icon: 'fa fa-plus',
                                        onClick: function(event)
                                        {   w2ui.news_grid.newRecord(); } 
                                    },
                                    {   type: 'button', text: 'Edit Content', icon: 'fa fa-edit',
                                        onClick: function(event)
                                        {   // alfalah.news.pop_up(event);
                                            var id = w2ui.news_grid.getSelection()[0];
                                            if (id)
                                            {
                                                alfalah.core.ajax(
                                                    '/news/2/5',    //the_url, 
                                                    {               //the_parameters,
                                                      '_token' : alfalah.news.sid,
                                                      json : JSON.stringify([
                                                        { id : w2ui.news_grid.getSelection()[0] 
                                                        }])
                                                    },                  
                                                    "POST",             //the_type, 
                                                    function(response)  //fn_success
                                                    { w2ui.news_grid.reload(); 
                                                    },
                                                    function(response)  //fn_fail, 
                                                    { console.log("FAILED");
                                                      console.log(response);
                                                    },
                                                    null                //fn_always
                                                );

                                                alfalah.news.PopupNews(event);
                                                // $("#popup_content").wysihtml5();
                                                // tinymce.init({selector: '#popup_content'});
                                            }
                                            else
                                            {
                                                console.log("no record selected");
                                            };
                                            
                                        } 
                                    },
                                    {   type: 'button', text: 'Save', icon: 'fa fa-save',
                                        onClick: function(event)
                                        {   //alfalah.news.pop_up_open(event);
                                            document.getElementById('news_form').submit()
                                            // alfalah.core.submitGrid(
                                            //     w2ui.news_grid, 
                                            //     '/news/2/1',
                                            //     alfalah.news.sid
                                            // );
                                        }
                                    },
                                    {   type: 'break'},
                                    {   type: 'button', text: 'Delete', 
                                        icon: 'fa fa-remove',
                                        onClick: function(event)
                                        { 
                                            // alfalah.news.openPopup(event);

                                            alfalah.core.ajax(
                                                '/news/2/2',    //the_url, 
                                                {               //the_parameters,
                                                  '_token' : alfalah.news.sid,
                                                  json : JSON.stringify([
                                                    { id : w2ui.news_grid.getSelection()[0] 
                                                    }])
                                                },                  
                                                "POST",             //the_type, 
                                                function(response)  //fn_success
                                                { w2ui.news_grid.reload(); 
                                                },
                                                function(response)  //fn_fail, 
                                                { console.log("FAILED");
                                                  console.log(response);
                                                },
                                                null                //fn_always
                                            );
                                        } 
                                    },
                                    {   type: 'break'},
                                    // {   type: 'button', text: 'PDF',
                                    //     print_type : 'PDF', 
                                    //     icon: 'fa fa-file-pdf-o',
                                    //     onClick: function (event) 
                                    //     {   alfalah.news.print_out(event); } 
                                    // },
                                    {   type: 'button', text: 'XLS',
                                        print_type : 'XLS',  
                                        icon: 'fa fa-file-excel-o',
                                        onClick: function (event) 
                                        {   alfalah.news.print_out(event); }  
                                    }],
                                },
                            },
                            // { type: 'preview', size: '50%', resizable: true, style: pstyle, content: 'preview' 
                            // },
                            {   type: 'right', size: 200, resizable: true, style: pstyle, 
                                toolbar: 
                                {   items: [
                                    {   type: 'button', text: 'Search', 
                                        icon: 'fa fa-search',
                                        onClick: function(event )
                                        {   console.log("search click");
                                            alfalah.news.search_handler(event); 
                                        }
                                    }],
                                    // onClick: function (event) {
                                    //   console.log("search clisck");
                                    //   console.log(event);
                                    // }
                                }
                            },
                            { type: 'bottom', size: '65%', resizable: true, style: pstyle, hidden:true 
                            }
                        ]
                    },
                    search_form:
                    {   name  : 'search_form',
                        url   : 'doremi/post',
                        fields: [
                            {   field: 'news_id',  
                                type: 'text', 
                                // html: { caption : 'No.Pen', 
                                //         attr    : 'style="width: 100px"' 
                                //     }
                            },
                            {   field: 'title',  type: 'text'}
                        ],
                        style: 'border: 0px; background-color: transparent;',
                        formHTML: 
                            '<div id="search_form" class="w2ui-page page-0">'+
                            '    <div class="w2ui-field">'+
                            '        <label style="text-align: left;">ID</label>'+
                            '        <div>'+
                            '            <input name="news_id" type="text" size="24" maxlength="20"/>'+
                            '        </div>'+
                            '    </div>'+
                            '    <div class="w2ui-field">'+
                            '        <label style="text-align: left;">Title</label>'+
                            '        <div>'+
                            '            <input name="title" type="text" size="24" maxlength="12"/>'+
                            '        </div>'+
                            '    </div>'+
                            '</div>',
                        // actions: {
                        //     reset: function () {   this.clear();   },
                        //     save: function () { this.save();    }
                        // }
                    },
                    news_editor:
                    {   name  : 'news_editor',
                        fields: [
                            {   field: 'content',  
                                type: 'textarea', 
                            },
                            {   field: 'news_id',  
                                type: 'text', 
                            },
                            {   field: 'title',  
                                type: 'text', 
                            },
            //                 { name: 'recid', type: 'text', html: { caption: 'ID', attr: 'size="10" readonly' } },
            // { name: 'fname', type: 'text', required: true, html: { caption: 'First Name', attr: 'size="40" maxlength="40"' } },
            // { name: 'lname', type: 'text', required: true, html: { caption: 'Last Name', attr: 'size="40" maxlength="40"' } },
            // { name: 'email', type: 'email', html: { caption: 'Email', attr: 'size="30"' } },
            // { name: 'sdate', type: 'date', html: { caption: 'Date', attr: 'size="10"' } }

                        ],
                        style: 'border: 0px; background-color: transparent;',
                        // formHTML: 
                        //     '<div id="news_editor" class="w2ui-page page-0">'+
                        //     '    <div class="w2ui-field">'+
                        //     // '       <label style="text-align: left;">News Content</label>'+
                        //     '       <form id="news_form" method="post"">'+
                        //     '           <textarea id="content" name="content">TESTING LOH</textarea>'+
                        //     '           <input type="text" id="news_id" name="news_id">'+
                        //     '           <input type="text" id="title" name="itle">'+
                        //     '       </form>'+
                        //     '    </div>'+
                        //     '</div>',
                        onRender: function(event )
                        {   console.log("onRender");
                            console.log(event);                 
                        },
                        onShow: function(event )
                        {   console.log("onShow");
                            console.log(event); 
                        },
                    }
                };
                this.popconfig = {
                    popup_layout: 
                    {   name: 'popup_layout',
                        panels: [
                            {   type: 'main', style: pstyle, 
                                toolbar: 
                                {   
                                    items: [
                                    {   type: 'button', text: 'Save', icon: 'fa fa-save',
                                        onClick: function(event)
                                        { alfalah.core.submitGrid(
                                            w2ui.news_grid, 
                                            '/news/2/1',
                                            alfalah.news.sid
                                          );
                                        }
                                    }],
                                },
                            },
                        ],
                    },
                    popup_editor:
                    {   name  : 'popup_editor',
                        url   : 'doremi/post',
                        fields: [
                            {   field: 'popup_content',  
                                type: 'textarea', 
                            },
                        ],
                        style: 'border: 0px; background-color: transparent;',
                        formHTML: 
                            '<div id="popup_editor" class="w2ui-page page-0">'+
                            '    <div class="w2ui-field">'+
                            // '<div id="popup_editor" class="box">'+
                            // '    <div class="box-body pad">'+
                            // '       <label style="text-align: left;">ID</label>'+
                            '       <form id="popup_form" method="post">'+
                            '           <textarea id="popup_content" name="popup_content" style="width: 100%; height: 100%; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>'+
                            '       </form>'+
                            '    </div>'+
                            '</div>',
                        onRender: function(event )
                        {   console.log("onRender");
                            console.log(event);
                            $("#popup_content").wysihtml5();                 
                        },
                        onShow: function(event )
                        {   console.log("onShow");
                            console.log(event); 
                            $("#popup_content").wysihtml5();
                        },
                    },
                };

                this.pconfig = {
                        layout: {
                            name: 'playout',
                            padding: 4,
                            panels: [
                                { type: 'left', size: '50%', resizable: true, minSize: 300 },
                                { type: 'main', minSize: 300 }
                            ]
                        },
                        grid: { 
                            name: 'pgrid',
                            columns: [
                                { field: 'fname', caption: 'First Name', size: '33%', sortable: true, searchable: true },
                                { field: 'lname', caption: 'Last Name', size: '33%', sortable: true, searchable: true },
                                { field: 'email', caption: 'Email', size: '33%' },
                                { field: 'sdate', caption: 'Start Date', size: '120px', render: 'date' }
                            ],
                            records: [
                                { recid: 1, fname: 'John', lname: 'Doe', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                                { recid: 2, fname: 'Stuart', lname: 'Motzart', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                                { recid: 3, fname: 'Jin', lname: 'Franson', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                                { recid: 4, fname: 'Susan', lname: 'Ottie', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                                { recid: 5, fname: 'Kelly', lname: 'Silver', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                                { recid: 6, fname: 'Francis', lname: 'Gatos', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                                { recid: 7, fname: 'Mark', lname: 'Welldo', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                                { recid: 8, fname: 'Thomas', lname: 'Bahh', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
                                { recid: 9, fname: 'Sergei', lname: 'Rachmaninov', email: 'jdoe@gmail.com', sdate: '4/3/2012' }
                            ],
                            onClick: function(event) {
                                var pgrid = this;
                                var pform = w2ui.pform;
                                event.onComplete = function () {
                                    var sel = pgrid.getSelection();
                                    if (sel.length == 1) {
                                        pform.recid  = sel[0];
                                        pform.record = $.extend(true, {}, pgrid.get(sel[0]));
                                        pform.refresh();
                                    } else {
                                        pform.clear();
                                    }
                                }
                            }
                        },
                        form: { 
                            header: 'Edit Record',
                            name: 'pform',
                            fields: [
                                { name: 'recid', type: 'text', html: { caption: 'ID', attr: 'size="10" readonly' } },
                                { name: 'fname', type: 'text', required: true, html: { caption: 'First Name', attr: 'size="40" maxlength="40"' } },
                                { name: 'lname', type: 'text', required: true, html: { caption: 'Last Name', attr: 'size="40" maxlength="40"' } },
                                { name: 'email', type: 'email', html: { caption: 'Email', attr: 'size="30"' } },
                                { name: 'sdate', type: 'date', html: { caption: 'Date', attr: 'size="10"' } },
                                { id: 'remarks', name: 'remarks', type: 'textarea', required: true, html: { caption: 'Remarks', attr: 'size="40" maxlength="40"' } },
                            ],
                            actions: {
                                Reset: function () {
                                    this.clear();
                                },
                                Save: function () {
                                    var errors = this.validate();
                                    if (errors.length > 0) return;
                                    if (this.recid == 0) {
                                        w2ui.pgrid.add($.extend(true, { recid: w2ui.pgrid.records.length + 1 }, this.record));
                                        w2ui.pgrid.selectNone();
                                        this.clear();
                                    } else {
                                        w2ui.pgrid.set(this.recid, this.record);
                                        w2ui.pgrid.selectNone();
                                        this.clear();
                                    }
                                }
                            },
                            onRender: function(event) {
                                console.log('render huahahaha');
                                // tinymce.init({    selector: '#remarks' });
                            }
                        }
                    };
            },
            // build the layout
            build_layout: function()
            { 
                $('#news_layout').w2layout(this.config.news_layout);
                $('#news_grid').w2grid(this.config.news_grid);
                w2ui.news_layout.content('main', w2ui.news_grid);
                $('#news_editor').w2form(this.config.news_editor);
                w2ui.news_layout.content('bottom', w2ui.news_editor);

                $('#w2int').w2field('int', { autoFormat: false });
                $('#search_form').w2form(this.config.search_form);
                w2ui.news_layout.content('right', w2ui.search_form);

                $().w2layout(alfalah.news.popconfig.popup_layout);
                $().w2form(alfalah.news.popconfig.popup_editor);
                w2ui.popup_layout.content('main', w2ui.popup_editor);
                // tinymce.init({ selector: '#popup_content' });
                $("#popup_content").wysihtml5();

                // $(function () {
                //     // initialization in memory
                //     $().w2layout(alfalah.news.pconfig.layout);
                //     $().w2grid(alfalah.news.pconfig.grid);
                //     $().w2form(alfalah.news.pconfig.form);

                //     w2ui.pform.on('*', function (event) {
                //         console.log('Event: '+ event.type, 'Target: '+ event.target, event);
                //     });
                // });
            },
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            {   console.log('finalize_comp_and_layout');
                
            },
            // finalize the component and layout drawing
            getSearchFormParameter: function(the_form)
            {   the_searchs = "";
                $(the_form).each(function()
                    {   the_searchs = $(this).find(':input');
                    });
                the_parameter = alfalah.core.getSearchParameter(the_searchs);
                the_parameter = $.extend( the_parameter, 
                                {   s:"form"    });
                return the_parameter;
            },
            search_handler: function(event)
            {   the_parameter = this.getSearchFormParameter("#search_form");
                // w2utils.settings.dataType = 'HTTP';
                // w2ui.news_grid.load(w2ui.news_grid.ref_url);
                w2ui.news_grid.load(w2ui.news_grid.ref_url+ '?'+$.param(the_parameter));
                
                // w2ui.news_grid.request(
                //     'GET', 
                //     the_parameter,
                //     // JSON.stringify(the_parameter), 
                //     w2ui.news_grid.ref_url
                // );                                 
                // 
                // this.DataStore.baseParams = Ext.apply( the_parameter,
                //     {   task: alfalah.news.task,
                //         act: alfalah.news.act,
                //         a:2, b:0, s:"form",
                //         limit:this.page_limit, start:this.page_start });
                // this.DataStore.reload();
                // w2ui.news_grid.postData = JSON.stringify(the_parameter);
                // w2ui.news_grid.method = 'POST';
                // w2ui.news_grid.postData = the_parameter;
                // w2ui.news_grid.searchData = the_parameter;
                // w2ui.news_grid.search();
                // w2utils.settings.RESTfull = true;
                // w2utils.settings.dataType = 'JSON';
            },
            print_out: function(event)
            {   alfalah.core.printOut(
                    event.item.print_type, 
                    w2ui.news_grid.ref_url, 
                    this.getSearchFormParameter("#search_form")
                );
            },
            pop_up: function(event)
            {
                w2popup.open({
                    title   : 'News Content',
                    width   : 800,
                    height  : 400,
                    showMax : true,
                    body    : '<div id="popup_main" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px;"></div>',
                    onOpen  : function (event) {
                        console.log('onOpen');
                        console.log(event);
                        event.onComplete = function () {
                            $('#w2ui-popup #popup_main').w2render(w2ui.popup_layout);
                            console.log('satu');
                            // tinymce.init({ selector: '#popup_content' });
                            console.log('dua');
                        };
                        console.log('lima');
                        // tinymce.init({ selector: '#popup_content' });
                            
                        // event.onComplete = alfalah.news.pop_up_open(event);
                    },
                    onToggle: function (event) { 
                        console.log('onToggle');
                        console.log(event);
                        event.onComplete = function () {
                            w2ui.popup_layout.resize();
                        }
                    }        
                });
                console.log('tiga');
                // tinymce.init({    selector: '#popup_content' });
            },
            pop_up_open: function(event)
            {
                // $('#popup_layout').w2layout(alfalah.news.popconfig.popup_layout);
                // $('#popup_editor').w2form(alfalah.news.popconfig.popup_editor);
                // $('#w2ui-popup #main').w2render('popup_layout');
                // w2ui.popup_layout.content('main', w2ui.popup_editor);

                // tinymce.init({    selector: '#popup_content' });
                // $('#fpopup').w2popup(
                //     {   title   : 'News Content',
                //         body    : ' '+
                //         '    <form id="fpopup_form" method="post">'+
                //         '       <textarea id="fpopup_content" name="fpopup_content"></textarea>'+
                //         '   </form>',
                        // onOpen  : function (event) {
                        //     console.log('fpopup onOpen');
                        //     console.log(event);
                        //     event.onComplete = function () {
                        //         tinymce.init({    selector: '#fpopup_content' });
                        //     };
                        // },
                //         onToggle: function (event) { 
                //             console.log('onToggle');
                //             console.log(event);
                //             event.onComplete = function () {
                //                 // w2ui.popup_layout.resize();
                //                 tinymce.init({    selector: '#fpopup_content' });
                //             }
                //         }
                // });

                $('#fpopup').w2popup({   
                    title   : 'News Content 1',
                    onOpen  : function (event) {
                        console.log('fpopup onOpen');
                        console.log(event);
                        event.onComplete = function () {
                            // tinymce.init({    selector: '#fpopup_content' });
                        }
                    },
                });
                // tinymce.init({    selector: '#fpopup_content' });

                // w2popup.load({ url: '/country/0/0' }); // content loaded from the server
            },

            openPopup: function(event) 
            {
                w2popup.open({
                    title   : 'Popup',
                    width   : 900,
                    height  : 600,
                    showMax : true,
                    modal   : true,
                    body    : '<div id="main" style="position: absolute; left: 5px; top: 5px; right: 5px; bottom: 5px;"></div>',
                    onOpen  : function (event) {
                        event.onComplete = function () {
                            $('#w2ui-popup #main').w2render('playout');
                            w2ui.playout.content('left', w2ui.pgrid);
                            w2ui.playout.content('main', w2ui.pform);
                            
                        };
                    },
                    onToggle: function (event) { 
                        event.onComplete = function () {
                            w2ui.playout.resize();
                        }
                    }
                });
            },
            PopupNews: function(event)
            {   
                w2ui.news_layout.toggle('bottom');
                if (alfalah.news.news_editor)
                {
                    // w2ui.news_layout.content('main', w2ui.news_grid);
                    alfalah.news.news_editor = false;
                }
                else
                {   
                    // w2ui.news_layout.content('main', w2ui.popup_editor);
                    // $("#popup_content").wysihtml5();
                    console.log('check news_content');
                    console.log($('#news_content'));
                    tinymce.init({ 
                        selector: 'textarea#news_content',
                        height  : 325,
                        menubar : false,
                        plugins : [
                            'advlist autolink lists link image charmap print preview anchor',
                            'searchreplace visualblocks code fullscreen',
                            'insertdatetime media table paste code help wordcount'
                        ],
                        toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                    });

                    alfalah.news.news_editor = true; 
                };
            }
        }; // end of public space
    }(); // end of app
    $(document).ready(alfalah.news.initialize());
</script>

@endsection
