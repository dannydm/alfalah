<script type="text/javascript">
    // create namespace
    Ext.namespace('alfalah.mcoa');
    
    // create application
    alfalah.mcoa = function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.tabId = '{{ $TABID }}';
        // private functions
        // public space
        return {
            centerPanel : 0,
            sid : '{{ csrf_token() }}',
            task : ' ',
            act : ' ',
            // public methods
            initialize: function()
            {   this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {   this.centerPanel = Ext.getCmp(tabId);
                this.mcoa.initialize();
                // this.sumberdana.initialize();
                // this.role.initialize();
                // this.role_mcoa.initialize();
                // this.role_user.initialize();
                // this.role_page.initialize();
                // this.user_page.initialize();
            },
            // build the layout
            build_layout: function()
            {   this.centerPanel.beginUpdate();
                this.centerPanel.add(this.mcoa.Tab);
                // this.centerPanel.add(this.sumberdana.Tab);
                this.centerPanel.setActiveTab(this.mcoa.Tab);
                this.centerPanel.endUpdate();
                alfalah.core.viewport.doLayout();
            },
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            {   console.log(4); },
        }; // end of public space
    }(); // end of app
    // create application
    alfalah.mcoa.mcoa= function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.Tab;
        this.Grid;
        this.Columns;
        this.Records;
        this.DataStore;
        this.Searchs;
        this.SearchBtn;
        // private functions
        // public space
        return {
            // execute at the very last time
            // public variable
            page_limit : 75,
            page_start : 0,
            // public methods
            initialize: function()
            {   this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {
                this.Columns = [
                    {   header: "ID", width : 50,
                        dataIndex : 'mcoa_id', sortable: true,
                        tooltip:"mcoa ID",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Parent.ID", width : 100,
                        dataIndex : 'parent_mcoa_id', sortable: true,
                        tooltip:"Parent mcoa ID",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Name", width: 200,
                        dataIndex : 'name', sortable: true,
                        tooltip:"Name",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Name print", width: 200,
                        dataIndex : 'name_print', sortable: true,
                        tooltip:"Name print",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Name Old", width: 200,
                        dataIndex : 'name_old', sortable: true,
                        tooltip:"Name old",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Value", width : 50,
                        dataIndex : 'need_value', sortable: true,
                        tooltip:"Header / detail",
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                   fields: ["label", "key"],
                                                   data : [["Header", "0"], ["detail", "1"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },
                    {   header: "D/K", width : 50,
                        dataIndex : 'd_k', sortable: true,
                        tooltip:"Debet / Kredit",
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                   fields: ["label", "key"],
                                                   data : [["Debet", "0"], ["Kredit", "1"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },

                    {   header: "Type", width : 100,
                        dataIndex : 'type', sortable: true,
                        tooltip:"mcoa Type",
                        // editor : new Ext.form.TextField({allowBlank: false}),
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                   fields: ["label", "key"],
                                                   data : [["Asset", "0"], ["Kewajiban", "1"], ["Ekuitas Dana", "2"],["Pendapatan", "3"], ["Belanja", "4"], ["Pembiayaan", "5"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },
                    {   header: "Status", width : 50,
                        dataIndex : 'status', sortable: true,
                        tooltip:"mcoa Status",
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                   fields: ["label", "key"],
                                                   data : [["Active", "0"], ["Inactive", "1"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },
                    {   header: "Created", width : 150,
                        dataIndex : 'created_date', sortable: true,
                        tooltip:"mcoa Created Date",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    },
                    {   header: "Updated", width : 150,
                        dataIndex : 'modified_date', sortable: true,
                        tooltip:"mcoa Last Updated",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    }
                ];
                this.Records = Ext.data.Record.create(
                [   {name: 'mcoa_id', type: 'string'},
                    {name: 'parent_mcoa_id', type: 'string'},
                    {name: 'name', type: 'string'},
                    {name: 'name_print', type: 'string'},
                    {name: 'name_old', type: 'string'},
                    {name: 'need_value', type: 'integer'},
                    {name: 'd_k', type: 'integer'},
                    {name: 'type', type: 'integer'},
                    {name: 'status', type: 'string'},
                    {name: 'created_date', type: 'date'},
                    {name: 'modified_date', type: 'date'},
                ]);
                this.Searchs = [
                    {   id: 'mcoa_name',
                        cid: 'name',
                        fieldLabel: 'Name',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'mcoa_type',
                        cid: 'type',
                        fieldLabel: 'Type',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'mcoa_status',
                        cid: 'status',
                        fieldLabel: 'Status',
                        labelSeparator : '',
                        xtype : 'combo',
                        store : new Ext.data.SimpleStore(
                        {   fields: ['status'],
                            data : [ [''], ['Active'], ['Inactive']]
                        }),
                        displayField:'status',
                        valueField :'status',
                        mode : 'local',
                        triggerAction: 'all',
                        selectOnFocus:true,
                        editable: false,
                        width : 100,
                        value: ''
                    },
                ];
                this.SearchBtn = new Ext.Button(
                {   id : tabId+"_mcoaSearchBtn",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.mcoa_search_handler,
                    scope : this
                });
                this.DataStore = alfalah.core.newDataStore(
                    "{{url('/mcoa/1/0')}}", false,
                    {   s:"init", limit:this.page_limit, start:this.page_start }
                );
                // this.DataStore.load();
                this.Grid = new Ext.grid.EditorGridPanel(
                {   store:  this.DataStore,
                    columns: this.Columns,
                    //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                    enableColLock: false,
                    //trackMouseOver: true,
                    loadMask: true,
                    height : alfalah.mcoa.centerPanel.container.dom.clientHeight-50,
                    anchor: '100%',
                    autoScroll  : true,
                    frame: true,
                    //stripeRows : true,
                    // inline buttons
                    //buttons: [{text:'Save'},{text:'Cancel'}],
                    //buttonAlign:'center',
                    tbar: [
                        {   text:'Add',
                            tooltip:'Add Record',
                            iconCls: 'silk-add',
                            handler : this.Grid_add,
                            scope : this
                        },
                        {   text:'Copy',
                            tooltip:'Copy Record',
                            iconCls: 'silk-page-copy',
                            handler : this.Grid_copy,
                            scope : this
                        },
                        //{   text:'Edit',
                        //    tooltip:'Edit Record',
                        //    iconCls: 'silk-page-white-edit',
                        //    //handler : this.mcoa_grid_
                        //},
                        {   id : tabId+'_mcoaSaveBtn',
                            text:'Save',
                            tooltip:'Save Record',
                            iconCls: 'icon-save',
                            handler : this.Grid_save,
                            scope : this
                        },
                        '-',
                        {   text:'Delete',
                            tooltip:'Delete Record',
                            iconCls: 'silk-delete',
                            handler : this.Grid_remove,
                            scope : this
                        },
                        '-',
                        {   print_type : "pdf",
                            text:'Print PDF',
                            tooltip:'Print to PDF',
                            iconCls: 'silk-page-white-acrobat',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },
                        {   print_type : "xls",
                            text:'Print Excell',
                            tooltip:'Print to Excell SpreadSheet',
                            iconCls: 'silk-page-white-excel',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },'-',
                    ],
                    bbar: new Ext.PagingToolbar(
                    {   id : tabId+'_mcoaGridBBar',
                        store: this.DataStore,
                        pageSize: this.page_limit,
                        displayInfo: true,
                        emptyMsg: 'No data found',
                        items : [
                            '-',
                            'Displayed : ',
                            new Ext.form.ComboBox(
                            {   id : tabId+'_mcoaPageCombo',
                                store: new Ext.data.SimpleStore(
                                            {   fields: ['value'],
                                                data : [[50],[75],[100],[125],[150]]
                                            }),
                                displayField:'value',
                                valueField :'value',
                                value : 75,
                                editable: false,
                                mode: 'local',
                                triggerAction: 'all',
                                selectOnFocus:true,
                                hiddenName: 'pagesize',
                                width:50,
                                listeners : { scope : this,
                                    'select' : function (a,b,c)
                                    {   page_limit = Ext.get(tabId+'_mcoaPageCombo').getValue();
                                        page_start = ( Ext.getCmp(tabId+'_mcoaGridBBar').getPageData().activePage -1) * page_limit;
                                        this.SearchBtn.handler.call(this.SearchBtn.scope);
                                        //Ext.getCmp(tabId+"_mcoaSearchBtn").handler.call();
                                        //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                    }
                                }
                            }),
                            ' records at a time'
                        ]
                    }),
                });
            },
            // build the layout
            build_layout: function()
            {   this.Tab = new Ext.Panel(
                {   id : tabId+"_mcoaTab",
                    jsId : tabId+"_mcoaTab",
                    title:  "COA",
                    region: 'center',
                    layout: 'border',
                    items: [
                    {   title: 'ToolBox',
                        region: 'east',     // position for region
                        split:true,
                        width: 200,
                        minSize: 200,
                        maxSize: 400,
                        collapsible: true,
                        layout : 'accordion',
                        items:[
                        {   title: 'S E A R C H',
                            labelWidth: 50,
                            defaultType: 'textfield',
                            xtype: 'form',
                            frame: true,
                            autoScroll : true,
                            items : [ this.Searchs, this.SearchBtn]
                        },
                        { title : 'Setting'
                        }]
                    },
                    //new Ext.TabPanel(
                    //    { id:'center-panel',
                    //      region:'center'
                    //    })
                    {   region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.Grid]
                    }
                    ]
                });
            },
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            {},
            // mcoa grid add new record
            Grid_add: function(button, event)
            {   this.Grid.stopEditing();
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   mcoa_id: "",
                        parent_mcoa_id: "",
                        name: "New Name",
                        name_print: "",
                        name_old: "",
                        need_value: 0,
                        d_k: 0,
                        type: 0,
                        status: 0,
                        created_date: "",
                        modified_date: ""
                    }));
                // placed the edit cursor on third-column
                this.Grid.startEditing(0, 2);
            },
            // mcoa grid copy and make it a new record
            Grid_copy: function(button, event)
            {   this.Grid.stopEditing();
                var selection = this.Grid.getSelectionModel().selection;
                if (selection)
                {   var data = this.Grid.getSelectionModel().selection.record.data;
                    this.Grid.store.insert( 0,
                        new this.Records (
                        {   id: "",
                            parent_mcoa_id: data.parent_mcoa_id,
                            name: data.name,
                            type: data.type,
                            description: data.description,
                            status: data.status
                        }));
                    this.Grid.startEditing(0, 2);
                }
                else
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Selected Record ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                         });
                };
            },
            // mcoa grid save records
            Grid_save : function(button, event)
            {   var the_records = this.DataStore.getModifiedRecords();
                // check data modification
                if ( the_records.length == 0 )
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Data modified ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                         });
                } else
                {   var json_data = alfalah.core.getDetailData(the_records);
                    alfalah.core.submitGrid(
                        this.DataStore, 
                        "{{url('/mcoa/1/1')}}",
                        {   'x-csrf-token': alfalah.mcoa.sid }, 
                        {   json: Ext.encode(json_data) }
                    );
                };
            },
            // mcoa grid delete records
            Grid_remove: function(button, event)
            {   this.Grid.stopEditing();
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{url('/mcoa/3/2')}}",
                    {   'x-csrf-token': alfalah.mcoa.sid }, 
                    {   id: this.Grid.getSelectionModel().selection.record.data.mcoa_id }
                );
            },
            // mcoa search button
            mcoa_search_handler : function(button, event)
            {   var the_search = true;
                if ( this.DataStore.getModifiedRecords().length > 0 )
                {   Ext.Msg.show(
                        {   title:'W A R N I N G ',
                            msg: ' Modified Data Found, Do you want to save it before search process ? ',
                            buttons: Ext.Msg.YESNO,
                            fn: function(buttonId, text)
                                {   if (buttonId =='yes')
                                    {   Ext.getCmp(tabId+'_mcoaSaveBtn').handler.call();
                                        the_search = false;
                                    } else the_search = true;
                                },
                            icon: Ext.MessageBox.WARNING
                         });
                };
    
                if (the_search == true) // no modification records flag then we can go to search
                {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                    this.DataStore.baseParams = Ext.apply( the_parameter, {s:"form", limit:this.page_limit, start:this.page_start});
                    this.DataStore.reload();
                };
            },
        }; // end of public space
    }(); // end of app
// On Ready
    //Ext.reg('project', alfalah.project);
    Ext.onReady(alfalah.mcoa.initialize, alfalah.mcoa);
    // end of file
    </script>
    <div>&nbsp;</div>