
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>{{ config('app.name', 'Larravel') }}</title>

    {{-- <title>ALFALAH</title> --}}

    <!-- Favicon -->
    <link rel="icon" href="{{asset('/academy/img/core-img/iconalfalah.ico')}}">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{asset('/academy/style.css')}}">

</head>

<body>
    <!-- ##### Preloader ##### -->
    <div id="preloader">
        <i class="circle-preloader"></i>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->
        <div class="top-header">
            <div class="container h-100">
                <div class="row h-100">
                    <div class="col-12 h-100">
                        <div class="header-content h-100 d-flex align-items-center justify-content-between">
                            <div class="academy-logo" height="100">
                                <a href="#"><img src="{{asset('/academy/img/core-img/logodarusalam1.png')}}" alt=""></a>
                            </div>
                            <div class="login-content">
                                {{-- <a href="#">Register / Login</a> --}}
                                @if (Auth::check())
                                <a href="{{ url('/home') }}">Home</a>
                            @else
                            <ul>
                               <a href="{{ url('/login') }}" class="btn academy-btn btn-sm">LOGIN</a>
                               
                                <a href="{{ url('/ppdb/0/0') }}" class="btn academy-btn btn-sm">PPDB ONLINE</a> 
                            </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="academy-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="academyNav">

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <!-- <div class="classy-menu">

                            <!-- close btn -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Pages</a>
                                        {{-- <ul class="dropdown">
                                            <li><a href="#">Home</a></li>
                                            <li><a href="#">About Us</a></li>
                                            <li><a href="#">School</a></li>
                                            <li><a href="#">Blog</a></li>
                                            <li><a href="#">Contact</a></li>
                                            <li><a href="#">Elements</a></li>
                                        </ul> --}}
                                    </li>
                                    {{-- <li><a href="#">Mega Menu</a>
                                        <div class="megamenu">
                                            <ul class="single-mega cn-col-4">
                                                <li><a href="#">Home</a></li>
                                                <li><a href="#">Services &amp; Features</a></li>
                                                <li><a href="#">Accordions and tabs</a></li>
                                                <li><a href="#">Menu ideas</a></li>
                                                <li><a href="#">Students Gallery</a></li>
                                            </ul>
                                            <ul class="single-mega cn-col-4">
                                                <li><a href="#">Home</a></li>
                                                <li><a href="#">Services &amp; Features</a></li>
                                                <li><a href="#">Accordions and tabs</a></li>
                                                <li><a href="#">Menu ideas</a></li>
                                                <li><a href="#">Students Gallery</a></li>
                                            </ul>
                                            <ul class="single-mega cn-col-4">
                                                <li><a href="#">Home</a></li>
                                                <li><a href="#">Services &amp; Features</a></li>
                                                <li><a href="#">Accordions and tabs</a></li>
                                                <li><a href="#">Menu ideas</a></li>
                                                <li><a href="#">Students Gallery</a></li>
                                            </ul>
                                            <div class="single-mega cn-col-4">
                                                <img src="{{asset('/academy/img/bg-img/bg-1.jpg')}}" alt="">
                                            </div>
                                        </div>
                                    </li> --}}
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">School</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                            <!-- Nav End -->
                        </div> -->

                        <!-- Calling Info -->
                        <div class="calling-info">
                            <div class="call-center">
                                <a href="#"><i class="icon-telephone-2"></i> <span>0318672828, 8664323.</span></a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Hero Area Start ##### -->
    <section class="hero-area">
        <div class="hero-slides owl-carousel">

            <!-- Single Hero Slide -->
            <div class="single-hero-slide bg-img" style="background-image: url(academy/img/bg-img/alfalah-bg-1.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="hero-slides-content">
                                <h4 data-animation="fadeInUp" data-delay="100ms">All the lesson you need</h4>
                                <h2 data-animation="fadeInUp" data-delay="400ms">Wellcome to our <br>Online School</h2>
                                <a href="#" class="btn academy-btn" data-animation="fadeInUp" data-delay="700ms">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Single Hero Slide -->
            <div class="single-hero-slide bg-img" style="background-image: url(academy/img/bg-img/alfalah-bg-2.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="hero-slides-content">
                                <h4 data-animation="fadeInUp" data-delay="100ms">All the courses you need</h4>
                                <h2 data-animation="fadeInUp" data-delay="400ms">Wellcome to our <br>Online School</h2>
                                <a href="#" class="btn academy-btn" data-animation="fadeInUp" data-delay="700ms">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <!-- Single Hero Slide -->
             <div class="single-hero-slide bg-img" style="background-image: url(academy/img/bg-img/alfalah-bg-3.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="hero-slides-content">
                                <h4 data-animation="fadeInUp" data-delay="100ms">All the education you need</h4>
                                <h2 data-animation="fadeInUp" data-delay="400ms">Wellcome to our <br>Online School</h2>
                                <a href="#" class="btn academy-btn" data-animation="fadeInUp" data-delay="700ms">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Single Hero Slide -->
            <div class="single-hero-slide bg-img" style="background-image: url(academy/img/bg-img/alfalah-bg-4.jpg);">
                            <div class="container h-100">
                                <div class="row h-100 align-items-center">
                                    <div class="col-12">
                                        <div class="hero-slides-content">
                                            <h4 data-animation="fadeInUp" data-delay="100ms">All the lesson you need</h4>
                                            <h2 data-animation="fadeInUp" data-delay="400ms">Wellcome to our <br>Online School</h2>
                                            <a href="#" class="btn academy-btn" data-animation="fadeInUp" data-delay="700ms">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

            <!-- Single Hero Slide -->
            <div class="single-hero-slide bg-img" style="background-image: url(academy/img/bg-img/alfalah-bg-5.jpg);">
                            <div class="container h-100">
                                <div class="row h-100 align-items-center">
                                    <div class="col-12">
                                        <div class="hero-slides-content">
                                            <h4 data-animation="fadeInUp" data-delay="100ms">All the lesson you need</h4>
                                            <h2 data-animation="fadeInUp" data-delay="400ms">Wellcome to our <br>Online School</h2>
                                            <a href="#" class="btn academy-btn" data-animation="fadeInUp" data-delay="700ms">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


        </div>
    </section>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Top Feature Area Start ##### -->
    <div class="top-features-area wow fadeInUp" data-wow-delay="300ms">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="features-content">
                        <div class="row no-gutters">
                            <!-- Single Top Features -->
                            <div class="col-12 col-md-4">
                                <div class="single-top-features d-flex align-items-center justify-content-center">
                                    <i class="icon-agenda-1"></i>
                                    <a href="{{ url('/ppdb/0/0') }}"><h5>Online Registration</h5></a> 
                                </div>
                            </div>
                            <!-- Single Top Features -->
                            <div class="col-12 col-md-4">
                                <div class="single-top-features d-flex align-items-center justify-content-center">
                                    <i class="icon-assistance"></i>
                                    <h5>Amazing Teachers</h5>
                                </div>
                            </div>
                            <!-- Single Top Features -->
                            <div class="col-12 col-md-4">
                                <div class="single-top-features d-flex align-items-center justify-content-center">
                                    <i class="icon-telephone-3"></i>
                                    <h5>Great Support</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Top Feature Area End ##### -->

    <!-- ##### Course Area Start ##### -->
    <!-- <div class="academy-courses-area section-padding-100-0"> -->
        <!-- <div class="container"> -->
            <div class="row">
            <div class="col-12">

                <!-- ##### Top Popular Courses Details Area Start ##### -->
                <!-- <div class="popular-course-details-area wow fadeInUp" data-wow-delay="300ms"> -->
                    <div class="single-top-popular-course d-flex align-items-center flex-wrap">
                        <div class="popular-course-content width">
                            <h5>Dokumentasi Manual Kartu Pembayaran Siswa</h5>
                            <span>By SDQ   |  January, 2020</span>
                            <div class="course-ratings">
                                <!-- <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i> -->
                            </div>
                            <p>sistem informasi ini dibuat untuk membantu orang tua wali dan murid untuk melihat data pembayaran, berikut ini adalah petunjuk penggunaannya, Klik Tombol Login, Masukan NIS dan @Alfalah.com, masukan password tgl lahir siswa , contoh 12-12-2020, klik login </p>
                            <a href="#" class="btn academy-btn btn-sm mt-15">See More</a>
                        </div>
                        <div class="popular-course-thumb bg-img"  style="background-image: url(academy/img/bg-img/pembayaran3.gif);"></div>
                    </div>
                <!-- </div> -->

            </div>
        </div>
    <!-- </div> -->
    <!-- </div> -->
    <!-- ##### Course Area End ##### -->

    <!-- ##### Testimonials Area Start ##### -->
    <div class="testimonials-area section-padding-100 bg-img bg-overlay" style="background-image: url(img/bg-img/bg-2.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading text-center mx-auto white wow fadeInUp" data-wow-delay="300ms">
                        <span>our testimonials</span>
                        <h3>See what our satisfied Parents are saying about us</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- Single Testimonials Area -->
                <div class="col-12 col-md-6">
                    <div class="single-testimonial-area mb-100 d-flex wow fadeInUp" data-wow-delay="400ms">
                        <div class="testimonial-thumb">
                            <img src="{{asset('/academy/img/bg-img/alfalah-t1.jpg')}}" alt="">
                        </div>
                        <div class="testimonial-content">
                            <h5>Great education</h5>
                            <p>Lembaga Pendidikan ini sangat mengerti tentang Pendidikan. Sebelum belajar ilmu apapun diajar mengaji terlebih dahulu. Pengurus Yayasan juga sangat peduli terhadap Pendidikan. Guru-Gurunya juga sangat berkualitas. Semoga Lembaga Pendidikan seperti LPFDT bias menginspirasi lembaga-lembaga pendidikan yang lain. </p>
                            <h6><span>Saifullah Yusuf,</span> Wkl Gubernur Jatim, 2014-2019</h6>
                        </div>
                    </div>
                </div>
                <!-- Single Testimonials Area -->
                <div class="col-12 col-md-6">
                    <div class="single-testimonial-area mb-100 d-flex wow fadeInUp" data-wow-delay="500ms">
                        <div class="testimonial-thumb">
                            <img src="{{asset('/academy/img/bg-img/alfalah-t2.png')}}" alt="">
                        </div>
                        <div class="testimonial-content">
                            <h5>Based on Al-Quran</h5>
                            <p>Alhamdulillah saya sangat bersyukur dan terharu kepada Lembaga Pendidikan Al Falah Darussalam Tropodo yang telah menyelenggarakan Pendidikan yang didalamnya mengajarkan Al Qur’an disamping ilmu-ilmu yang lain. Hari ini saya menyaksikan sendiri siswa-siswa yang luar biasa yang sudah hafal beberapa Juz. Semoga Lembaga Pendidikan Al Falah Darussalam ini selalu diberkahi Allah SWT. Aamiin. </p>
                            <h6><span>Shech Ali Jabeer,</span>Ulama dan Penceramah Internasional</h6>
                        </div>
                    </div>
                </div>
                <!-- Single Testimonials Area -->
                <div class="col-12 col-md-6">
                    <div class="single-testimonial-area mb-100 d-flex wow fadeInUp" data-wow-delay="600ms">
                        <div class="testimonial-thumb">
                            <img src="{{asset('/academy/img/bg-img/alfalah-t3.jpg')}}" alt="">
                        </div>
                        <div class="testimonial-content">
                            <h5>I just love the courses here</h5>
                            <p>Alhamdulillah kami sangat bersyukur kepada Allah serta mengucapkan banyak terimakasih kepada Al Falah Darussalam yang telah mendidik anak saya sehingga anak saya menjadi anak yang sholih dan sholihah. Semua anak saya sekolah di Al Falah Darussalam Tropodo. Semoga kedepan Al Falah Darussalam selalu lebih baik dan semoga sukses selalu. Aamiin. </p>
                            <h6><span>Bunda Rasya,</span> Wali Murid Kelas 6</h6>
                        </div>
                    </div>
                </div>
                <!-- Single Testimonials Area -->
                <div class="col-12 col-md-6">
                    <div class="single-testimonial-area mb-100 d-flex wow fadeInUp" data-wow-delay="700ms">
                        <div class="testimonial-thumb">
                            <img src="{{asset('/academy/img/bg-img/t4.jpg')}}" alt="">
                        </div>
                        <div class="testimonial-content">
                            <h5>One good academy</h5>
                            <p>Vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibu lum est mat tis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio ves tibul. Etiam nec odio vestibulum est mat tis effic iturut magnaNec odio vestibulum est mattis effic iturut magna.</p>
                            <h6><span>James Williams,</span> Student</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="load-more-btn text-center wow fadeInUp" data-wow-delay="800ms">
                        <a href="#" class="btn academy-btn">See More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Testimonials Area End ##### -->

    <!-- ##### Top Popular Courses Area Start ##### -->
    <div class="top-popular-courses-area section-padding-100-70">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading text-center mx-auto wow fadeInUp" data-wow-delay="300ms">
                        <span>The Best</span>
                        <h3>Teacher</h3>
                    </div>
                </div>
            </div>
            <div class="row">
               
                <!-- Single Top Popular Course -->
                <div class="col-12 col-lg-6">
                    <div class="single-top-popular-course d-flex align-items-center flex-wrap mb-30 wow fadeInUp" data-wow-delay="400ms">
                        <div class="popular-course-content">
                            <h5>USTADZAH FAUZIAH</h5>
                            <span>Juara 3 Lomba Mendongeng Tingkat Kabupaten</span>
                            <div class="course-ratings">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                            </div>
                            <p> Guru KB yang telah menyabet Juara 3 Lomba Mendongeng Tingkat Kabupaten. Semoga bermanfaat untuk semua guru dan siswa</p>
                            <a href="#" class="btn academy-btn btn-sm">See More</a>
                        </div>
                        <div class="popular-course-thumb bg-img" style="background-image: url(academy/img/bg-img/alfalah-fauziah.png);"></div>
                    </div>
                </div>

                <!-- Single Top Popular Course -->
                <div class="col-12 col-lg-6">
                    <div class="single-top-popular-course d-flex align-items-center flex-wrap mb-30 wow fadeInUp" data-wow-delay="700ms">
                        <div class="popular-course-content">
                            <h5>USTADZAH DEWI</h5>
                            <span>Juara 1 Lomba Mendongeng Tingkat Kabupaten Sidoarjo</span>
                            <div class="course-ratings">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                            </div>
                            <p>Alhamdulillah ustadzah wali kelas TK ini telah berprestasi merebut Juara 1 Lomba Mendongeng Tingkat Kabupaten Sidoarjo</p>
                            <a href="#" class="btn academy-btn btn-sm">See More</a>
                        </div>
                        <div class="popular-course-thumb bg-img" style="background-image: url(academy/img/bg-img/alfalah-dewi.png);"></div>
                    </div>
                </div>    
            
                
                <!-- Single Top Popular Course -->
                <div class="col-12 col-lg-6">
                    <div class="single-top-popular-course d-flex align-items-center flex-wrap mb-30 wow fadeInUp" data-wow-delay="600ms">
                        <div class="popular-course-content">
                            <h5>MURYADI</h5>
                            <span>Juara 1 Olimpiade TIK Kemdikbud Tingkat Propinsi Jawa Timur</span>
                            <div class="course-ratings">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                            </div>
                            <p>Menjuarai kejuaran olimpiade TIK Pembelajaran Tingkat Propinsi Jawa Timur yang diikuti oleh peserta seluruh Jawa Timur. Semoga bisa bermanfaat bagi LPFDT</p>
                            <a href="#" class="btn academy-btn btn-sm">See More</a>
                        </div>
                        <div class="popular-course-thumb bg-img" style="background-image: url(academy/img/bg-img/alfalah-muryadi.png);"></div>
                    </div>
                </div>
                

    <!-- Single Top Popular Course -->
    <div class="col-12 col-lg-6">
                    <div class="single-top-popular-course d-flex align-items-center flex-wrap mb-30 wow fadeInUp" data-wow-delay="500ms">
                        <div class="popular-course-content">
                            <h5>IMAWATI</h5>
                            <span>Juara 1 Karya Ilmiah Pendidikan tingkat Nasional di Kemdikbud Jakarta</span>
                            <div class="course-ratings">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                            </div>
                            <p> ustadzah Imawati telah menyabet Juara 1 penulisan karya ilmiah yang diselenggarakan oleh Kemdikbud pusat.</p>
                            <a href="#" class="btn academy-btn btn-sm">See More</a>
                        </div>
                        <div class="popular-course-thumb bg-img" style="background-image: url(academy/img/bg-img/alfalah-imawati.png);"></div>
                    </div>
                </div>               
            </div>
        </div>
    </div>
    <!-- ##### Top Popular Courses Area End ##### -->

    <!-- ##### Partner Area Start ##### -->
    <div class="partner-area section-padding-0-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="partners-logo d-flex align-items-center justify-content-between flex-wrap">
                        {{-- <a href="#"><img src="{{asset('/academy/img/clients-img/partner-1.png')}}" alt=""></a>
                        <a href="#"><img src="{{asset('/academy/img/clients-img/partner-2.png')}}" alt=""></a>
                        <a href="#"><img src="{{asset('/academy/img/clients-img/partner-3.png')}}" alt=""></a>
                        <a href="#"><img src="{{asset('/academy/img/clients-img/partner-4.png')}}" alt=""></a>
                        <a href="#"><img src="{{asset('/academy/img/clients-img/partner-5.png')}}" alt=""></a> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Partner Area End ##### -->

    <!-- ##### CTA Area Start ##### -->
    <div class="call-to-action-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="cta-content d-flex align-items-center justify-content-between flex-wrap">
                        <h3>Do you want to enrole at our School? Get in touch!</h3>
                        <a href="#" class="btn academy-btn">See More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### CTA Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <div class="main-footer-area section-padding-100-0">
            <div class="container">
                <div class="row">
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <a href="#"><img src="{{asset('/academy/img/core-img/logodarusalam.png')}}" alt=""></a>
                            </div>
                            <p>Cras vitae turpis lacinia, lacinia lacus non, fermentum nisi. Donec et sollicitudin est, in euismod erat. Ut at erat et arcu pulvinar cursus a eget.</p>
                            <div class="footer-social-info">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                <a href="#"><i class="fa fa-behance"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <h6>Usefull Links</h6>
                            </div>
                            <nav>
                                <ul class="useful-links">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Services &amp; Features</a></li>
                                    <li><a href="#">Accordions and tabs</a></li>
                                    <li><a href="#">Menu ideas</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <h6>Gallery</h6>
                            </div>
                            <div class="gallery-list d-flex justify-content-between flex-wrap">
                                <a href="{{asset('/academy/img/bg-img/alfalah-gallery1.jpg')}}" class="gallery-img" title="Gallery Image 1"><img src="{{asset('/academy/img/bg-img/alfalah-gallery1.jpg')}}" alt=""></a>
                                <a href="{{asset('/academy/img/bg-img/alfalah-gallery2.jpg')}}" class="gallery-img" title="Gallery Image 2"><img src="{{asset('/academy/img/bg-img/alfalah-gallery2.jpg')}}" alt=""></a>
                                <a href="{{asset('/academy/img/bg-img/gallery3.jpg')}}" class="gallery-img" title="Gallery Image 3"><img src="{{asset('/academy/img/bg-img/gallery3.jpg')}}" alt=""></a>
                                <a href="{{asset('/academy/img/bg-img/gallery4.jpg')}}" class="gallery-img" title="Gallery Image 4"><img src="{{asset('/academy/img/bg-img/gallery4.jpg')}}" alt=""></a>
                                <a href="{{asset('/academy/img/bg-img/gallery5.jpg')}}" class="gallery-img" title="Gallery Image 5"><img src="{{asset('/academy/img/bg-img/gallery5.jpg')}}" alt=""></a>
                                <a href="{{asset('/academy/img/bg-img/gallery6.jpg')}}" class="gallery-img" title="Gallery Image 6"><img src="{{asset('/academy/img/bg-img/gallery6.jpg')}}" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <h6>Contact</h6>
                            </div>
                            <div class="single-contact d-flex mb-30">
                                <i class="icon-placeholder"></i>
                                <p>Jl. Anggrek No.1 Wisma Tropodo Waru Sidoarjo Indonesia</p>
                            </div>
                            <div class="single-contact d-flex mb-30">
                                <i class="icon-telephone-1"></i>
                                <p>Main: 0318672828 <br>Office: 8664323</p>
                            </div>
                            <div class="single-contact d-flex">
                                <i class="icon-contract"></i>
                                <p>alfalah.darussalam@yahoo.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="{{asset('/academy/js/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{asset('/academy/js/bootstrap/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('/academy/js/bootstrap/bootstrap.min.js')}}"></script>
    <!-- All Plugins js -->
    <script src="{{asset('/academy/js/plugins/plugins.js')}}"></script>
    <!-- Active js -->
    <script src="{{asset('/academy/js/active.js')}}"></script>
</body>

</html>