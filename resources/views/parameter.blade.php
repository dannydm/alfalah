<script type="text/javascript">
// create namespace
Ext.namespace('saa.parameter');

// create application
saa.parameter = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.parameter.initialize();
            this.um.initialize();
            this.umconv.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            // this.centerPanel.add(this.description.Tab);
            this.centerPanel.add(this.parameter.Tab);
            this.centerPanel.add(this.um.Tab);
            this.centerPanel.add(this.umconv.Tab);
            this.centerPanel.setActiveTab(this.parameter.Tab);
            this.centerPanel.endUpdate();
            saa.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
saa.parameter.parameter= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Name", width : 200,
                    dataIndex : 'param_name', sortable: true,
                    tooltip:"parameter Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Value", width : 150,
                    dataIndex : 'param_value', sortable: true,
                    tooltip:"parameter Value",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Description", width : 200,
                    dataIndex : 'description', sortable: true,
                    tooltip:"Parameter Description",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"parameter Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_', sortable: true,
                    tooltip:"parameter Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'param_name', type: 'integer'},
                {name: 'param_value', type: 'string'},
                {name: 'description', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'parameter_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'parameter_value',
                    cid: 'value',
                    fieldLabel: 'Value',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'parameter_description',
                    cid: 'description',
                    fieldLabel: 'Description',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_parameterSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.parameter_search_handler,
                scope : this
            });
            this.DataStore = saa.core.newDataStore(
                '/parameter/1/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : saa.parameter.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Copy',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.Grid_copy,
                        scope : this
                    },
                    //{   text:'Edit',
                    //    tooltip:'Edit Record',
                    //    iconCls: 'silk-page-white-edit',
                    //    //handler : this.parameter_grid_
                    //},
                    {   id : tabId+'_parameterSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_parameterGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_parameterPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_parameterPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_parameterGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_parameterSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_parameterTab",
                jsId : tabId+"_parameterTab",
                title:  "Parameter",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // parameter grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   param_name: "New Name",
                    param_value: "New Value",
                    description: "New Description",
                    created_date: "",
                    modified_date: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 0);
        },
        // parameter grid copy and make it a new record
        Grid_copy: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection)
            {   var data = this.Grid.getSelectionModel().selection.record.data;
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   id: "",
                        parent_param_name: data.parent_param_name,
                        name: data.name,
                        object_id : data.object_id,
                        link: data.link,
                        status: data.status,
                        has_child : data.has_child,
                        level_degree: data.level_degree,
                        arrange_no: data.arange_no
                    }));
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },
        // parameter grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = saa.core.getDetailData(the_records);
                saa.core.submitGrid(
                    this.DataStore, 
                    '/parameter/1/1',
                    {   'x-csrf-token': saa.parameter.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // parameter grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            saa.core.submitGrid(
                this.DataStore, 
                '/parameter/1/2',
                {   'x-csrf-token': saa.parameter.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.param_name }
            );
        },
        // parameter search button
        parameter_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_parameterSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: saa.parameter.task,
                        act: saa.parameter.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
saa.parameter.um = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'um_id', sortable: true,
                    tooltip:"UM ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Name", width : 100,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Unit Measurement Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Description", width : 100,
                    dataIndex : 'description', sortable: true,
                    tooltip:"Unit Measurement Description",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"um Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'um_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'created_date', type: 'date'}
            ]);
            this.Searchs = [
                {   id: 'um_um_id',
                    cid: 'um_id',
                    fieldLabel: 'UM.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'um_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_umSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.um_search_handler,
                scope : this
            });
            this.DataStore = saa.core.newDataStore(
                '/parameter/2/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : saa.parameter.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    //{   text:'Edit',
                    //    tooltip:'Edit Record',
                    //    iconCls: 'silk-page-white-edit',
                    //    //handler : this.um_grid_
                    //},
                    {   id : tabId+'_umSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_umGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_umPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_umPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_umGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_umTab",
                jsId : tabId+"_umTab",
                title:  "Unit Measurement",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // um grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   um_id: "",
                    name: "New Name",
                    description: "New Desc",
                    created_date: "",
                    modified_date: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // um grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = saa.core.getDetailData(the_records);
                saa.core.submitGrid(
                    this.DataStore, 
                    '/parameter/2/1',
                    {   'x-csrf-token': saa.parameter.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // um grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            saa.core.submitGrid(
                this.DataStore, 
                '/parameter/2/2',
                {   'x-csrf-token': saa.parameter.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.um_id }
            );
        },
        // um search button
        um_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_umSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter, {s:"form", limit:this.page_limit, start:this.page_start});
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
saa.parameter.umconv = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   console.log("initialize umconv");
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'um_conv_id', sortable: true,
                    tooltip:"UM ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "First", width : 50,
                    dataIndex : 'first_um_id', sortable: true,
                    tooltip:"First UM ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "First.Value", width : 100,
                    dataIndex : 'first_value', sortable: true,
                    tooltip:"First UM Value",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Second", width : 50,
                    dataIndex : 'second_um_id', sortable: true,
                    tooltip:"Second UM ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Second.Value", width : 100,
                    dataIndex : 'second_value', sortable: true,
                    tooltip:"Second UM Value",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"um Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
            ];
            this.Records = Ext.data.Record.create(
            [   {first_value: 'um_conv_id', type: 'string'},
                {first_value: 'first_um_id', type: 'string'},
                {first_value: 'first_value', type: 'string'},
                {first_value: 'second_um_id', type: 'string'},
                {first_value: 'second_value', type: 'string'},
                {first_value: 'created_date', type: 'date'}
            ]);
            this.Searchs = [
                {   id: 'umconv_id',
                    cid: 'um_conv_id',
                    fieldLabel: 'UM.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'umconv_first_id',
                    cid: 'first_um_id',
                    fieldLabel: '1st.UM',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'umconv_second_id',
                    cid: 'second_um_id',
                    fieldLabel: '2nd.UM',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_umConvSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.umconv_search_handler,
                scope : this
            });
            this.DataStore = saa.core.newDataStore(
                '/parameter/3/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : saa.parameter.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    //{   text:'Edit',
                    //    tooltip:'Edit Record',
                    //    iconCls: 'silk-page-white-edit',
                    //    //handler : this.umconv_grid_
                    //},
                    {   id : tabId+'_umConvSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_umConvGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_umConvPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_umConvPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_umConvGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_umConvSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_umConvTab",
                jsId : tabId+"_umConvTab",
                title:  "UM.Conv",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // um grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   umconv_conv_id: "",
                    first_value: "New Name",
                    description: "New Desc",
                    created_date: "",
                    modified_date: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // um grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = saa.core.getDetailData(the_records);
                saa.core.submitGrid(
                    this.DataStore, 
                    '/parameter/3/1',
                    {   'x-csrf-token': saa.parameter.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // um grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            saa.core.submitGrid(
                this.DataStore, 
                '/parameter/3/2',
                {   'x-csrf-token': saa.parameter.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.umconv_conv_id }
            );
        },
        // um search button
        umconv_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_umConvSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter, {s:"form", limit:this.page_limit, start:this.page_start});
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(saa.parameter.initialize, saa.parameter);
// end of file
</script>
<div>&nbsp;</div>