<!DOCTYPE html>
<html>
<head>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
  @yield('extra_styles')
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper"  style="background-color: #ecf0f5;">
    @yield('content') 
    @yield('extra_scripts')
  </div>
</body>
</html>