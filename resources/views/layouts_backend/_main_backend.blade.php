<!DOCTYPE html>
<html>
<head>
    @include('layouts_backend._head_backend')
    @include('layouts_backend._css_backend')
    @yield('extra_styles')
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    @include('layouts_backend._north_backend')
    @include('layouts_backend._east_backend')
    @yield('content') 
    <footer class="main-footer">
      @include('layouts_backend._south_backend')
    </footer>
    @include('layouts_backend._west_backend')
    @include('layouts_backend._scripts_backend')
    @yield('extra_scripts')
  </div>
</body>
</html>