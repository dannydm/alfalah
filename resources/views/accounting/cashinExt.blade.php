<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.cashin');

// create application
alfalah.cashin = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.cashin.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.cashin.Tab);
            this.centerPanel.setActiveTab(this.cashin.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
    }; // end of public space
}(); // end of app
// create application
alfalah.cashin.cashin= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                new Ext.grid.RowNumberer(
                        {header : 'No.',
                         width: 30,
                         sortable: true,
                         }
                    ),
                {   header: "Nomor", width : 100,
                    dataIndex : 'cashin_no', sortable: true,
                    tooltip:"cashin No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_1 == 0) { metaData.attr = "style = background-color:yellow;"; }
                        // not yet approve
                        else if ( record.data.approve_status_1 == 1){ metaData.attr = "style = background-color:lime;"; }
                        // rejected
                        else {  metaData.attr = "style = background-color:red;"; };
                        return value;
                    }
                },
                {   header: "Keterangan", width : 200,
                    dataIndex : 'cashin_description',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mcashin_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Tanggal Transaksi", width : 150,
                    dataIndex : 'cashin_transaction_date', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mcashin_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Jumlah", width : 100,
                    dataIndex : 'cashin_ammount', sortable: true,
                    tooltip:"jumlah nominal",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Created by", width : 100,
                    dataIndex : 'created_by', sortable: true,
                    tooltip:"cashin Created by",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },

                {   header: "Created", width : 100,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"cashin Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated by", width : 100,
                    dataIndex : 'modified_by', sortable: true,
                    tooltip:"cashin Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 100,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"cashin Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    hidden :true,
                    tooltip:" Status",
                },
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                // {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'cashin_no_id',
                    cid: 'cashin_no',
                    fieldLabel: 'Nomor',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'cashin_description_id',
                    cid: "cashin_description",
                    fieldLabel: 'Keterangan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'cashin_transaction_date_id',
                    cid: 'cashin_transaction_date',
                    fieldLabel: 'Tgl Trans',
                    labelSeparator : '',
                    xtype: 'datefield',
                    width : 120
                },
                {   id: 'cashin_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },

            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_cashinSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.cashin_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/cashin/1/0')}}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                stripeRows :true,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.cashin.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        // hidden :true,
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Edit',
                        tooltip:'Edit Record',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_edit,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        id : 'btndelete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this,
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
 //                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        handler : this.Grid_pdf,
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        hidden:true,
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        //  handler : this.Grid_pdf,
                        scope : this
                    },'-',
                    '->',
                    '_T O T A L_',
                    new Ext.form.TextField(
                    {   id : 'grand_total_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        // style: "text-align: right",
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        scope : this,
                    }),
                        ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_cashinGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_cashinPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_cashinPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_cashinGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_cashinSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_cashinTab",
                jsId : tabId+"_cashinTab",
                title:  "CASH IN",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.cashin_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        //  {},

        // Grid_grand_total : function(the_cell)
        {
            this.DataStore.on( 'load', function( store, records, options )
            {
                console.log(records);
                var total_data = 0;
                Ext.each(records,
                    function(the_record)
                    { 
                        total_data = total_data + parseInt(the_record.data.total_biaya);
                    });

                console.log("total jumlah = "+total_data);
                Ext.getCmp('grand_total_id').setValue(Ext.util.Format.number(total_data, '0,000'));
            });
        },

        Grid_add: function(button, event)
        {   var centerPanel = Ext.getCmp('center_panel');
            alfalah.cashin.forms.initialize();
            centerPanel.beginUpdate();
            centerPanel.add(alfalah.cashin.forms.Tab);
            centerPanel.setActiveTab(alfalah.cashin.forms.Tab);
            centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        Grid_edit : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var the_form = Ext.getCmp("cashinFrm");
                var the_approve= this.Grid.getSelectionModel().selection.record.data.approve_status_1;
                if (the_approve == 1)
                 {  console.log('tidak boleh edit');
                      console.log(the_record);
                       Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Server Message : '+'\n'+'Telah Disetujui Kabid, Tidak bisa di Edit',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                }
                else if (the_form)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'cashin Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                 
                    // console.log('the_record.record.data.cashin_no'+the_record.record.data.cashin_no);
                    alfalah.cashin.forms.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.cashin.forms.Tab);
                    centerPanel.setActiveTab(alfalah.cashin.forms.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_detail : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var form_order = Ext.getCmp("form_order");
                if (form_order)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Order Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    alfalah.cashin.detailFrm.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.cashin.detailFrm.Tab);
                    centerPanel.setActiveTab(alfalah.cashin.detailFrm.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        // cashin grid save records

        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            } else
            {   var head_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/cashin/1/1') }}",
                    {   'x-csrf-token': alfalah.cashin.sid }, 
                    {   head : Ext.encode(head_data) }
                );
            };
        },
        // cashin grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var the_approve= this.Grid.getSelectionModel().selection.record.data.approve_status_1;
                if (the_approve == 1)
                    {  console.log('hapussss');
                    console.log(the_record);
                        Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : "has been approved, don't delete it",
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                else
                    {
                        Ext.Ajax.request(
                        {   method: 'POST',
                            url: "{{ url('/cashin/1/2') }}",
                            headers:
                            {   'x-csrf-token': alfalah.cashin.sid },
                            params  :
                            {   cashin_no: this.Grid.getSelectionModel().selection.record.data.cashin_no
                            },
                            success: function(response)
                            {   var the_response = Ext.decode(response.responseText);
                                if (the_response.success == false)
                                {   Ext.Msg.show(
                                    {   title :'E R R O R ',
                                        msg : 'Server Message : '+'\n'+the_response.message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                }
                                else
                                {   this.DataStore.reload();
                                    //datastore.remove(grid.getSelectionModel().selections.items[0]);
                                };
                            },
                            failure: function()
                            { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                            },
                        scope: this
                        });
                    }
            }
        },

        Grid_pdf : function(button, event)
        {     var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                console.log(the_record)
                alfalah.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        cashin_no : the_record.cashin_no });
                    
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };
        },
        
        Gridafteredit : function(the_cell)
        { switch (the_cell.field)
            {   case "volume":
                case "tarif":
                {
                    var total_biaya = 0;
                    Ext.each(the_data,
                    function(the_record)
                    { 
                        total_biaya = total_biaya + parseInt(the_record.data.jumlah);
                    });
            
                    Ext.getCmp('total_biaya_id').setValue(Ext.util.Format.number(total_nota, '0,000'));

                }; 
                break; 
            };
        },


        // cashin search button
        cashin_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_cashinSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.cashin.task,
                        act: alfalah.cashin.act,
                        a:3, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
// create application
alfalah.cashin.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthRecords;
    this.SouthDataStore;
    this.typecoaDS;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
            {  

            this.typecoaDS = alfalah.core.newDataStore(
                "{{ url('/mcoa/4/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start}
            ); 
            alfalah.cashin.forms.typecoaDS.load();
          //  this.typecoaDs.load();
         

            this.coaDS = alfalah.core.newDataStore(
                "{{ url('/mcoa/1/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start, type: 0  }
            ); 


            this.region_height = alfalah.cashin.centerPanel.container.dom.clientHeight;
            this.Columns = new Ext.FormPanel(
            {   id : 'cashinFrm',
                width: '100%',
                frame: true,
                title : 'HEADER',
                // autoHeight: true,
                bodyStyle: 'padding: 10px 10px 0 10px;',
                labelWidth: 100,
                defaults: {
                // anchor: '100%',
                allowBlank: false,
                msgTarget: 'side'
                },
                items: [
                {   layout:'column',
                    height : 200,
                    border:false,
                    items:[
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'cashinFrm_id',
                            xtype:'textfield',
                            fieldLabel: 'ID',
                            name: 'id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '',
                            hidden : true,
                        },
                        {   id : 'cashinFrm_created_date', 
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'65%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'cashinFrm_tahun_ajaran_id', 
                            xtype:'textfield',
                            fieldLabel: 'Tahun',
                            name: 'tahun_ajaran_id',
                            anchor:'65%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $TAHUNAJARAN_ID }}',
                            hidden : true,
                        }]
                    },
                    {   columnWidth:.5,
                        layout: 'form',
                        border:false,
                        items: [
                        {   xtype: 'fieldset',
                          //  title: 'HEADER CASH IN ',
                            autoHeight: true,
                            anchor:'95%',
                            items: [
                            {   id : 'cashinFrm_cashin_no', 
                                xtype:'textfield',
                                fieldLabel: 'Nomor',
                                name: 'cashin_no',
                                anchor:'65%',
                                allowBlank: true,
                                readOnly : true,
                                value : 'by System'
                            },
                            {   id : 'cashinFrm_tahun_ajaran_name', 
                                xtype:'textfield',
                                fieldLabel: 'Periode',
                                name: 'tahun_ajaran_name',
                                anchor:'65%',
                                allowBlank: false,
                                readOnly : true,
                                value : '{{ $TAHUNAJARAN }}',
                            },
                            {   id : 'cashinFrm_reference_no_id', 
                                xtype:'textfield',
                                fieldLabel: 'No Referensi',
                                name: 'cash_in_reference_no', 
                                anchor:'65%',
                                allowBlank: false,
                                value : ''
                            },
                            {   id : 'cashinFrm_transaction_date_id', 
                                xtype:'datefield',
                                fieldLabel: 'Tanggal',
                                name: 'cash_in_transaction_date',
                                anchor:'65%',
                                allowBlank: false,
                                value : ''
                            },
                            {   id : 'cashinFrm_valuta_id', 
                                xtype:'textfield',
                                fieldLabel: 'Valuta',
                                name: 'cash_in_valuta',
                                anchor:'95%',
                                allowBlank: false,
                                value : ''
                            },
                            {   id : 'cashinFrm_kurs_valuta_id', 
                                xtype:'textfield',
                                fieldLabel: 'Kurs Valuta',
                                name: 'cash_in_kurs_valuta',
                                anchor:'95%',
                                allowBlank: false,
                                value : ''
                            },
                            ]
                        },
                        ]
                    },
                    {   columnWidth:.5,
                        layout: 'form',
                        border:false,
                        items: [
                        {   xtype: 'fieldset',
                          //  title: 'HEADER CASH IN',
                            autoHeight: true,
                            anchor:'95%',
                            items: [
                                {   border : false,
                                    autoHeight:true,
                                    layout:'column',
                                    items:[{
                                        columnWidth:.5,
                                        border :false,
                                        layout: 'form',
                                        items: [{
                                            id : 'cashinFrm_cash_type_id',
                                            xtype:'combo',
                                            allowBlank: false,
                                            fieldLabel: 'Kode Kas/Bank',
                                            name: 'cash_type_id',
                                            anchor:'95%',
                                            labelSeparator : '',
                                            readOnly : false,
                                         //   store : this.typecoaDs,
                                            displayField: 'name',
                                            valueField: 'id',
                                            hiddenName: 'id',
                                            hiddenVAlue: '',
                                            mode : 'local',
                                            forceSelection: true,
                                            triggerAction: 'all',
                                            selectOnFocus:true,
                                            editable: false,
                                            typeAhead: true,
                                            value: ''

                                        }]
                                    },
                                    {
                                        columnWidth:.5,
                                        layout: 'form',
                                        border :false,
                                        items: [{
                                                    id : 'cashinFrm_cashin_balance_id',
                                                    style: "background-color: #ffff00; background-image:none;",
                                                    xtype:'textfield',
                                                    allowBlank: false,
                                                    fieldLabel: 'Saldo',
                                                    name: 'cashin_balance_id',
                                                    anchor:'90%'
                                                }]
                                    }]
                                },
                                {   id : 'cashinFrm_receive_by_id', 
                                    xtype:'textfield',
                                    fieldLabel: 'Kepada',
                                    name: 'cash_in_receive_by',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'cashinFrm_receive_address_id', 
                                    xtype:'textfield',
                                    fieldLabel: 'Alamat',
                                    name: 'cash_in_receive_address',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'cashinFrm_description_id', 
                                    xtype:'textarea',
                                    fieldLabel: 'Keterangan',
                                    name: 'cash_in_name',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                }
                            ]
                        },
                        ]
                    }
                    ],
                }],
            });
            this.Searchs = [
                {   id: 'cashin_serapan_organisasi',
                    cid: "organisasi_mcashin_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield', 
                    width : 120
                },
                {   id: 'cashin_serapan_urusan',
                    cid: 'urusan_mcashin_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_cashinSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.cashin_serapan_search_handler,
                scope : this
            });
            
            this.Grid = new Ext.Panel(
            {   // title:  "H E A D E R",
                region: 'center',
                height : 200,
                layout: 'border',
                items: [
                    {   region: 'center', 
                        xtype: 'container',
                        // layout: 'fit',
                        items:[this.Columns]
                    },
                ],

                });
            /**
                * SOUTH-GRID
                */
            this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/cashin/2/3') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.SouthDataStore.load();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.SouthDataStore,
               
                columns: [ 
                    new Ext.grid.RowNumberer(
                        {header : 'No.',
                         width: 30,
                         sortable: true,
                         }
                    ),
                {   header: "No. Kas", width : 100,
                    dataIndex : 'cashin_no', sortable: true,
                    tooltip:"No. header kas",
                },
                {   header: "Kode Rekening", width : 150,
                    dataIndex : 'coa_id', sortable: true,
                    tooltip:"kode rekening",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Nama Rekening", width : 200,
                    dataIndex : 'coa_name', sortable: true,
                    tooltip:"nama rekening",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Uraian", width : 300,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Uraian",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Qty", width : 150,
                    dataIndex : 'cashin_item_qty', sortable: true,
                    tooltip:"Quantity of item",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Harga @", width : 120,
                    dataIndex : 'cashin_item_ammount', sortable: true,
                    tooltip:"harga satuan ",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "T o t a l", width : 150,
                    id : 'cashin_total_amount_id',
                    dataIndex : 'cashin_total_amount', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        return Ext.util.Format.number(value, '000.00');
                    },
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created_date', sortable: true,
                    hidden:true,
                    tooltip:"cashin Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    hidden:true,
                    tooltip:"cashin Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ],
            loadMask: true,
            height : (this.region_height/2)-30,
            autoScroll  : true,
            stripeRows : true,
            frame: true,
            tbar: [
                {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },'-',
                    {   text:'Save',
                        id : 'btnsave',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        scope : this
                    },
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '->',
                    '_TOTAL PEMASUKAN_',
                    new Ext.form.TextField(
                    {   id : 'cashin_grand_total_id',
                        // store:this.DataStore,
                        displayField: 'total_income',
                        valueField: 'total_income',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
              ],
            bbar: new Ext.PagingToolbar(
            {   id : tabId+'_southGridBBar',
                pageSize: this.page_limit,
                displayInfo: true,
                emptyMsg: 'No data found',
                items : [
                    '-',
                    'Displayed : ',
                    new Ext.form.ComboBox(
                    {   id : tabId+'_southPageCombo',
                        store: new Ext.data.SimpleStore(
                                    {   fields: ['value'],
                                        data : [[50],[75],[100],[125],[150]]
                                    }),
                        displayField:'value',
                        valueField :'value',
                        value : 75,
                        editable: false,
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus:true,
                        hiddenName: 'pagesize',
                        width:50,
                        listeners : { scope : this,
                            'select' : function (a,b,c)
                            {   this.page_limit = Ext.get(tabId+'_southPageCombo').getValue();
                                bbar = Ext.getCmp(tabId+'_southGridBBar');
                                bbar.pageSize = parseInt(this.page_limit);
                                this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                this.SearchBtn.handler.call(this.SearchBtn.scope);
                            }
                        }
                    }),
                    ' records at a time'
                ]
            }),
        });
        this.detailTab = new Ext.Panel(
        {  // title:  "D E T A I L",
            region: 'center',
            layout: 'border',
            items: [
            {   region: 'center', 
                xtype: 'container',
                layout: 'fit',
                items:[this.Grid]
            }],
        });

    },
    // build the layout
    build_layout: function()
    {   this.Tab = new Ext.Panel(
        {   id : tabId+"_cashinTab",
            jsId : tabId+"_cashinTab",
            title:  "TRANSACTION CASH IN",
            region: 'center',
            closable : true,
            layout: 'border',
            items: [
            {   region: 'center',     // center region is required, no width/height specified
                xtype: 'container',
                layout: 'fit',
                items:[this.Grid]
            },
            {
                region: 'south',
                // title: 'D E T A I L',
                split: true,
                height : 450,
                // collapsible: true,
                margins: '0 0 0 0',
                layout : 'border',
                items :[ 
                {   
                    region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.SouthGrid]
                }]
            }]
        });
    },
    // finalize the component and layout drawing
    finalize_comp_and_layout: function()
    {
        console.log('prepare');
            console.log(this.Records);
            if (Ext.isObject(this.Records))
            {   Ext.each(this.Records.fields.items,
                function(the_field)
                {   switch (the_field.name)
                    {   case "id" :
                        case "tahun_ajaran_id" :
                        case "cash_type_id" :
                        case "masukan" :
                        case "tgt_masukan" :
                        case "keluaran" :
                        case "tgt_keluaran" :
                        case "hasil" :
                        case "sasaran" :
                        case "pic_id" :
                        case "tgl_mulai" :
                        case "tgl_selesai" :
                        case "jumlahn" :
                        case "status" :
                        case "created_date" :
                        {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(this.Records.data[the_field.name]);
                        };
                        break;
                        case "rapbs_no" :
                        {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(this.Records.data[the_field.name]);

                            this.DataStore.baseParams = {   
                                s:"form", limit:this.page_limit, start:this.page_start,
                                rapbs_no: this.Records.data[the_field.name]
                            };
                            this.DataStore.reload();
                        };
                        break;
                        case "organisasi_mrapbs_id" :
                        {   the_organisasi = this.Records.data[the_field.name]; //+'_name'
                            this.organisasiDS.on( 'load', function( store, records, options )
                            {   if (the_organisasi)
                                {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(the_organisasi);
                                    the_organisasi = false;
                                };
                            });
                        };
                        break;
                        case "cash_type_id" :
                        {   the_typeofcoa = this.Records.data[the_field.name]; //+'_name'
                            this.typecoaDs.on( 'load', function( store, records, options )
                            {  // console.log('onload urusan_mrapbs_id');
                                Ext.getCmp('cashinFrm_'+the_field.name).setValue(the_typeofcoa);  
                                the_urusan = false;
                            });
                        };
                        break;
                        case "program_mrapbs_id" :
                        {   the_program = this.Records.data[the_field.name]; //+'_name'
                            this.programDS.on( 'load', function( store, records, options )
                            {   if (the_program)
                                {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(the_program);
                                    the_program = false;
                                };
                            });
                        };
                        break;
                        case "kegiatan_mrapbs_id" :
                        {   the_kegiatan = this.Records.data[the_field.name]; //+'_name'
                            this.kegiatanDS.on( 'load', function( store, records, options )
                            {   if (the_kegiatan)
                                {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(the_kegiatan);
                                    the_kegiatan = false;
                                };
                            });
                        };
                        break;
                        case "sumberdana_id" :
                        {   the_sumber = this.Records.data[the_field.name]; //+'_name'
                            this.coaDS.on( 'load', function( store, records, options )
                            {   if (the_sumber)
                                {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(the_sumber);
                                    the_sumber = false;
                                };
                            });
                        };
                        break;
                        case "coa_id" :
                        {   the_coa = this.Records.data[the_field.name]; //+'_name'
                            this.coaDS.on( 'load', function( store, records, options )
                            {   if (the_coa)
                                {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(the_coa);
                                    the_coa = false;
                                }; 
                            });
                        };
                        break;
                        case "jumlahke_n" :
                        {   
                            var total_biaya_rapbs = alfalah.newrapbs.newrapbs.Grid.getSelectionModel().
                            selection.record.data.total_biaya
                            Ext.getCmp('rapbsFrm_jumlahke_n').
                            setValue(Ext.util.Format.number(total_biaya_rapbs, '0,000'));
                        };
                        break;

                    };
                }, this);
            };
         //   this.typecoaDs.reload();

            this.Records = Ext.data.Record.create(
            [   {name: 'cashin_item_id', type: 'integer'},
                {name: 'modified_date', type: 'date'},
            ]);
    },
    Grid_add: function(button, event)
        {   this.SouthGrid.stopEditing();
            this.SouthGrid.store.insert( 0,
                new this.Records (
                {   cashin_item_id: "",
                    cashin_no: "",
                    coa_id: "",
                    coa_name: " ",
                    cashin_item_description: " ",
                    cashin_item_qty: 0,
                    cashin_item_ammount:0,
                    cashin_total_amount: 0,
                    created_date: "",
                    modified_date: "",
                    created_by :" ",
                    modified_by:" ",
                    remark :"ADD",
                    action: "ADD",
                }));
            // placed the edit cursor on third-column
            this.SouthGrid.startEditing(0, 3);
        },

    Form_save : function(button, event) 

    {   var head_data = [];
        var json_data = [];
        var modi_data = [];
        var v_json = {};

        // header validation
        if (alfalah.core.validateFields([
            'cashinFrm_tahun_ajaran_id','cashinFrm_organisasi_mcashin_id','cashinFrm_urusan_mcashin_id', 'cashinFrm_organisasi_mcashin_id', 
            'cashinFrm_program_mcashin_id', 'cashinFrm_kegiatan_mcashin_id', 
            'cashinFrm_sub_kegiatan_mcashin_id','cashinFrm_jumlahn']))
        {   // detail validation
            // header data
            head_data = alfalah.core.getHeadData([
                'cashinFrm_id', 'cashinFrm_cashin_no', 'cashinFrm_tahun_ajaran_id', 
                'cashinFrm_organisasi_mcashin_id','cashinFrm_urusan_mcashin_id', 'cashinFrm_organisasi_mcashin_id', 
                'cashinFrm_program_mcashin_id', 'cashinFrm_kegiatan_mcashin_id', 
                'cashinFrm_sub_kegiatan_mcashin_id', 'cashinFrm_sumberdana_id', 'cashinFrm_coa_id',
                'cashinFrm_masukan', 'cashinFrm_tgt_masukan', 'cashinFrm_keluaran', 
                'cashinFrm_tgt_keluaran', 'cashinFrm_hasil', 'cashinFrm_sasaran', 'cashinFrm_jumlahn','cashinFrm_jumlahke_n',
                'cashinFrm_pic_id', 'cashinFrm_tgl_mulai', 'cashinFrm_tgl_selesai', 
                'cashinFrm_status', 'cashinFrm_created_date',
                // 'cashinFrm_modified_date',
                ]);
            //detail data
            json_data = alfalah.core.getDetailData(this.DataStore.getModifiedRecords());
            // submit data
            alfalah.core.submitForm(
                tabId+"_FormsTab", 
                alfalah.cashin.cashin.DataStore,
                "{{ url('/cashin/1/1') }}",
                {   'x-csrf-token': alfalah.cashin.sid },
                {   task: 'save',
                    head : Ext.encode(head_data),
                    json : Ext.encode(json_data),
                });
        };
    },
    Grid_approve: function(button, event)
    {   
        the_cashin_no = Ext.getCmp('cashinFrm_cashin_no').getValue();
        if (the_cashin_no == "")
        {
            Ext.Msg.show(
            {   title :'E R R O R ',
                msg : 'cashin not saved yet',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
        else
        {
            this.SouthGrid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: "{{ url('/newcashin/1/705') }}",
                headers:
                {   'x-csrf-token': alfalah.newcashin.sid },
                params  :
                {   cashin_no: the_cashin_no },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Server Message : '+'\n'+the_response.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                    else
                    {   
                        Ext.getCmp(tabId+"_FormsTab").destroy();
                        alfalah.newcashin.newcashin.DataStore.reload();
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
            scope: this
            });
        };
    },

    Grid_remove: function(button, event)
    {   this.Grid.stopEditing();
        var the_record = this.Grid.getSelectionModel().selection;
        // b = saa.purchase.po.forms.DataStore.removeAt(a.cell[0])
        if ( the_record )
        {   if (Ext.isEmpty(the_record.record.data.id))
            {   this.Grid.getStore().removeAt(the_record.cell[0]);  }
            else
            {   the_record.record.set("remark", "REMOVED"); };
            this.isRecalculate = false;
        }
        else
        {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Data Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
        };
    },


    // pencairan search button
    cashin_search_handler : function(button, event)
    {   var the_search = true;
        if ( this.DataStore.getModifiedRecords().length > 0 )
        {   Ext.Msg.show(
                {   title:'W A R N I N G ',
                    msg: ' Modified Data Found, Do you want to save it before search process ? ',
                    buttons: Ext.Msg.YESNO,
                    fn: function(buttonId, text)
                        {   if (buttonId =='yes')
                            {   Ext.getCmp(tabId+'_pencairanSaveBtn').handler.call();
                                the_search = false;
                            } else the_search = true;
                        },
                    icon: Ext.MessageBox.WARNING
                    });
            };
            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.cashin.task,
                        act: alfalah.cashin.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
Ext.onReady(alfalah.cashin.initialize, alfalah.cashin);
// end of file
</script>
<div>&nbsp;</div>