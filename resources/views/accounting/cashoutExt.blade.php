<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.cashout');

// create application
alfalah.cashout = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.cashout.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.cashout.Tab);
            this.centerPanel.setActiveTab(this.cashout.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
    }; // end of public space
}(); // end of app
// create application
alfalah.cashout.cashout= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Nomor", width : 100,
                    dataIndex : 'cashout_no', sortable: true,
                    tooltip:"cashout No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_1 == 0) { metaData.attr = "style = background-color:yellow;"; }
                        // not yet approve
                        else if ( record.data.approve_status_1 == 1){ metaData.attr = "style = background-color:lime;"; }
                        // rejected
                        else {  metaData.attr = "style = background-color:red;"; };
                        return value;
                    }
                },
                {   header: "Keterangan", width : 200,
                    dataIndex : 'cashout_description',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mcashout_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Tanggal Transaksi", width : 150,
                    dataIndex : 'cashout_transaction_date', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mcashout_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Jumlah", width : 100,
                    dataIndex : 'cashout_ammount', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Created by", width : 100,
                    dataIndex : 'created_by', sortable: true,
                    tooltip:"cashout Created by",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },

                {   header: "Created", width : 100,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"cashout Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated by", width : 100,
                    dataIndex : 'modified_by', sortable: true,
                    tooltip:"cashout Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 100,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"cashout Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    hidden :true,
                    tooltip:" Status",
                },
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                // {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'cashout_no_id',
                    cid: 'cashout_no',
                    fieldLabel: 'Nomor',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'cashout_description_id',
                    cid: "cashout_description",
                    fieldLabel: 'Keterangan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'cashout_transaction_date_id',
                    cid: 'cashout_transaction_date',
                    fieldLabel: 'Tgl Trans',
                    labelSeparator : '',
                    xtype: 'datefield',
                    width : 120
                },
                {   id: 'cashout_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },

            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_cashoutSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.cashout_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/cashout/1/0')}}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                stripeRows :true,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.cashout.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        // hidden :true,
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Edit',
                        tooltip:'Edit Record',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_edit,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        id : 'btndelete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this,
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
 //                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        handler : this.Grid_pdf,
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        hidden:true,
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        //  handler : this.Grid_pdf,
                        scope : this
                    },'-',
                    '->',
                    '_T O T A L_',
                    new Ext.form.TextField(
                    {   id : 'grand_total_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        // style: "text-align: right",
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        scope : this,
                    }),
                        ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_cashoutGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_cashoutPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_cashoutPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_cashoutGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_cashoutSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_cashoutTab",
                jsId : tabId+"_cashoutTab",
                title:  "HEADER",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.cashout_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        //  {},

        // Grid_grand_total : function(the_cell)
        {
            this.DataStore.on( 'load', function( store, records, options )
            {
                console.log(records);
                var total_data = 0;
                Ext.each(records,
                    function(the_record)
                    { 
                        total_data = total_data + parseInt(the_record.data.total_biaya);
                    });

                console.log("total jumlah = "+total_data);
                Ext.getCmp('grand_total_id').setValue(Ext.util.Format.number(total_data, '0,000'));
            });
        },

        Grid_add: function(button, event)
        {   var centerPanel = Ext.getCmp('center_panel');
            alfalah.cashout.forms.initialize();
            centerPanel.beginUpdate();
            centerPanel.add(alfalah.cashout.forms.Tab);
            centerPanel.setActiveTab(alfalah.cashout.forms.Tab);
            centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        Grid_edit : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var the_form = Ext.getCmp("cashoutFrm");
                var the_approve= this.Grid.getSelectionModel().selection.record.data.approve_status_1;
                if (the_approve == 1)
                 {  console.log('tidak boleh edit');
                      console.log(the_record);
                       Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Server Message : '+'\n'+'Telah Disetujui Kabid, Tidak bisa di Edit',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                }
                else if (the_form)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'cashout Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                 
                    // console.log('the_record.record.data.cashout_no'+the_record.record.data.cashout_no);
                    alfalah.cashout.forms.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.cashout.forms.Tab);
                    centerPanel.setActiveTab(alfalah.cashout.forms.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_detail : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var form_order = Ext.getCmp("form_order");
                if (form_order)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Order Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    alfalah.cashout.detailFrm.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.cashout.detailFrm.Tab);
                    centerPanel.setActiveTab(alfalah.cashout.detailFrm.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        // cashout grid save records

        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            } else
            {   var head_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/cashout/1/1') }}",
                    {   'x-csrf-token': alfalah.cashout.sid }, 
                    {   head : Ext.encode(head_data) }
                );
            };
        },
        // cashout grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var the_approve= this.Grid.getSelectionModel().selection.record.data.approve_status_1;
                if (the_approve == 1)
                    {  console.log('hapussss');
                    console.log(the_record);
                        Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : "has been approved, don't delete it",
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                else
                    {
                        Ext.Ajax.request(
                        {   method: 'POST',
                            url: "{{ url('/cashout/1/2') }}",
                            headers:
                            {   'x-csrf-token': alfalah.cashout.sid },
                            params  :
                            {   cashout_no: this.Grid.getSelectionModel().selection.record.data.cashout_no
                            },
                            success: function(response)
                            {   var the_response = Ext.decode(response.responseText);
                                if (the_response.success == false)
                                {   Ext.Msg.show(
                                    {   title :'E R R O R ',
                                        msg : 'Server Message : '+'\n'+the_response.message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                }
                                else
                                {   this.DataStore.reload();
                                    //datastore.remove(grid.getSelectionModel().selections.items[0]);
                                };
                            },
                            failure: function()
                            { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                            },
                        scope: this
                        });
                    }
            }
        },

        Grid_pdf : function(button, event)
        {     var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                console.log(the_record)
                alfalah.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        cashout_no : the_record.cashout_no });
                    
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };
        },
        
        Gridafteredit : function(the_cell)
        { switch (the_cell.field)
            {   case "volume":
                case "tarif":
                {
                    var total_biaya = 0;
                    Ext.each(the_data,
                    function(the_record)
                    { 
                        total_biaya = total_biaya + parseInt(the_record.data.jumlah);
                    });
            
                    Ext.getCmp('total_biaya_id').setValue(Ext.util.Format.number(total_nota, '0,000'));

                }; 
                break; 
            };
        },


        // cashout search button
        cashout_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_cashoutSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.cashout.task,
                        act: alfalah.cashout.act,
                        a:3, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
// create application
alfalah.cashout.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
            {  
            this.SerapanDs = alfalah.core.newDataStore(
            "{{ url('/cashout/2/2') }}", false,
            {   s:"form", limit:this.page_limit, start:this.page_start });
            this.SerapanDs.load();

            this.region_height = alfalah.cashout.centerPanel.container.dom.clientHeight;
            this.Columns = new Ext.FormPanel(
            {   id : 'cashoutFrm',
                width: '100%',
                frame: true,
                title : 'HEADER',
                // autoHeight: true,
                bodyStyle: 'padding: 10px 10px 0 10px;',
                labelWidth: 100,
                defaults: {
                // anchor: '100%',
                allowBlank: false,
                msgTarget: 'side'
                },
                items: [
                {   layout:'column',
                    height : 200,
                    border:false,
                    items:[
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'cashoutFrm_id',
                            xtype:'textfield',
                            fieldLabel: 'ID',
                            name: 'id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '',
                            hidden : true,
                        },
                        {   id : 'cashoutFrm_created_date', 
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'65%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'cashoutFrm_tahun_ajaran_id', 
                            xtype:'textfield',
                            fieldLabel: 'Tahun',
                            name: 'tahun_ajaran_id',
                            anchor:'65%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $TAHUNAJARAN_ID }}',
                            hidden : true,
                        }]
                    },
                    {   columnWidth:.5,
                        layout: 'form',
                        border:false,
                        items: [
                        {   xtype: 'fieldset',
                          //  title: 'HEADER CASH IN ',
                            autoHeight: true,
                            anchor:'95%',
                            items: [
                            {   id : 'cashoutFrm_cashout_no', 
                                xtype:'textfield',
                                fieldLabel: 'Nomor',
                                name: 'cashout_no',
                                anchor:'65%',
                                allowBlank: true,
                                readOnly : true,
                                value : 'by System'
                            },
                            {   id : 'cashoutFrm_tahun_ajaran_name', 
                                xtype:'textfield',
                                fieldLabel: 'Periode',
                                name: 'tahun_ajaran_name',
                                anchor:'65%',
                                allowBlank: false,
                                readOnly : true,
                                value : '{{ $TAHUNAJARAN }}',
                            },
                            {   id : 'cashoutFrm_reference_no_id', 
                                xtype:'textfield',
                                fieldLabel: 'No Referensi',
                                name: 'cash_in_reference_no', 
                                anchor:'65%',
                                allowBlank: false,
                                value : ''
                            },
                            {   id : 'cashoutFrm_transaction_date_id', 
                                xtype:'datefield',
                                fieldLabel: 'Tanggal',
                                name: 'cash_in_transaction_date',
                                anchor:'65%',
                                allowBlank: false,
                                value : ''
                            },
                            {   id : 'cashoutFrm_valuta_id', 
                                xtype:'textfield',
                                fieldLabel: 'Valuta',
                                name: 'cash_in_valuta',
                                anchor:'95%',
                                allowBlank: false,
                                value : ''
                            },
                            {   id : 'cashoutFrm_kurs_valuta_id', 
                                xtype:'textfield',
                                fieldLabel: 'Kurs Valuta',
                                name: 'cash_in_kurs_valuta',
                                anchor:'95%',
                                allowBlank: false,
                                value : ''
                            },
                            ]
                        },
                        ]
                    },
                    {   columnWidth:.5,
                        layout: 'form',
                        border:false,
                        items: [
                        {   xtype: 'fieldset',
                          //  title: 'HEADER CASH IN',
                            autoHeight: true,
                            anchor:'95%',
                            items: [
                                {   border : false,
                                    autoHeight:true,
                                    layout:'column',
                                    items:[{
                                        columnWidth:.5,
                                        border :false,
                                        layout: 'form',
                                        items: [{
                                            id : 'cashoutFrm_cash_type_id',
                                            xtype:'combo',
                                            allowBlank: false,
                                            fieldLabel: 'Kode Kas/Bank',
                                            name: 'anakke',
                                            anchor:'95%'
                                        }]
                                    },{
                                        columnWidth:.5,
                                        layout: 'form',
                                        border :false,
                                        items: [{
                                        id : 'cashoutFrm_cashout_balance_id',
                                        style: "background-color: #ffff00; background-image:none;",
                                        xtype:'textfield',
                                        allowBlank: false,
                                        fieldLabel: 'Saldo',
                                        name: 'cashout_balance_id',
                                        anchor:'90%'
                                        }]
                                    }]
                                },
                                {   id : 'cashoutFrm_receive_by_id', 
                                    xtype:'textfield',
                                    fieldLabel: 'Kepada',
                                    name: 'cash_in_receive_by',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'cashoutFrm_receive_address_id', 
                                    xtype:'textfield',
                                    fieldLabel: 'Alamat',
                                    name: 'cash_in_receive_address',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                },
                                {   id : 'cashoutFrm_description_id', 
                                    xtype:'textarea',
                                    fieldLabel: 'Keterangan',
                                    name: 'cash_in_description',
                                    anchor:'95%',
                                    allowBlank: false,
                                    // readOnly : true,
                                    value : ''
                                }
                            ]
                        },
                        ]
                    }
                    ],
                }],
            });
            this.Searchs = [
                {   id: 'cashout_serapan_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield', 
                    width : 120
                },
                {   id: 'cashout_serapan_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_cashoutSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.cashout_serapan_search_handler,
                scope : this
            });
            
            this.Grid = new Ext.Panel(
            {   // title:  "H E A D E R",
                region: 'center',
                height : 200,
                layout: 'border',
                items: [
                    {   region: 'center', 
                        xtype: 'container',
                        // layout: 'fit',
                        items:[this.Columns]
                    },
                ],

                });
            /**
                * SOUTH-GRID
                */
            this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/cashout/2/3') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.SouthDataStore.load();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.SouthDataStore,
                columns: [ 
                {   header: "No. Kas", width : 100,
                    dataIndex : 'cashout_no', sortable: true,
                    tooltip:"No. header kas",
                },
                {   header: "Kode Rekening", width : 150,
                    dataIndex : 'account_number', sortable: true,
                    tooltip:"kode rekening",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Nama Rekening", width : 200,
                    dataIndex : 'cashout_account_name', sortable: true,
                    tooltip:"nama rekening",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Uraian", width : 300,
                    dataIndex : 'cashout_item_description', sortable: true,
                    tooltip:"Uraian",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Qty", width : 150,
                    dataIndex : 'cashout_qty', sortable: true,
                    tooltip:"Quantity of item",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Harga @", width : 120,
                    dataIndex : 'cashout_item_ammount', sortable: true,
                    tooltip:"harga satuan ",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "T o t a l", width : 150,
                    id : 'cashout_total_amount_id',
                    dataIndex : 'cashout_total_amount', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        return Ext.util.Format.number(value, '000.00');
                    },
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created_date', sortable: true,
                    hidden:true,
                    tooltip:"cashout Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    hidden:true,
                    tooltip:"cashout Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ],
            loadMask: true,
            height : (this.region_height/2)-30,
            autoScroll  : true,
            stripeRows : true,
            frame: true,
            tbar: [
                {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },'-',
                    {   text:'Save',
                        id : 'btnsave',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        scope : this
                    },
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '->',
                    '_TOTAL PENGELUARAN_',
                    new Ext.form.TextField(
                    {   id : 'cashout_grand_total_id',
                        // store:this.DataStore,
                        displayField: 'total_income',
                        valueField: 'total_income',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
              ],
            bbar: new Ext.PagingToolbar(
            {   id : tabId+'_southGridBBar',
                pageSize: this.page_limit,
                displayInfo: true,
                emptyMsg: 'No data found',
                items : [
                    '-',
                    'Displayed : ',
                    new Ext.form.ComboBox(
                    {   id : tabId+'_southPageCombo',
                        store: new Ext.data.SimpleStore(
                                    {   fields: ['value'],
                                        data : [[50],[75],[100],[125],[150]]
                                    }),
                        displayField:'value',
                        valueField :'value',
                        value : 75,
                        editable: false,
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus:true,
                        hiddenName: 'pagesize',
                        width:50,
                        listeners : { scope : this,
                            'select' : function (a,b,c)
                            {   this.page_limit = Ext.get(tabId+'_southPageCombo').getValue();
                                bbar = Ext.getCmp(tabId+'_southGridBBar');
                                bbar.pageSize = parseInt(this.page_limit);
                                this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                this.SearchBtn.handler.call(this.SearchBtn.scope);
                            }
                        }
                    }),
                    ' records at a time'
                ]
            }),
        });
        this.detailTab = new Ext.Panel(
        {  // title:  "D E T A I L",
            region: 'center',
            layout: 'border',
            items: [
            {   region: 'center', 
                xtype: 'container',
                layout: 'fit',
                items:[this.Grid]
            }],
        });

    },
    // build the layout
    build_layout: function()
    {   this.Tab = new Ext.Panel(
        {   id : tabId+"_cashoutTab",
            jsId : tabId+"_cashoutTab",
            title:  "TRANSACTION CASH OUT",
            region: 'center',
            closable : true,
            layout: 'border',
            items: [
            {   region: 'center',     // center region is required, no width/height specified
                xtype: 'container',
                layout: 'fit',
                items:[this.Grid]
            },
            {
                region: 'south',
                // title: 'D E T A I L',
                split: true,
                height : 450,
                // collapsible: true,
                margins: '0 0 0 0',
                layout : 'border',
                items :[ 
                {   
                    region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.SouthGrid]
                }]
            }]
        });
    },
    // finalize the component and layout drawing
    finalize_comp_and_layout: function()
    {

    
    },

    // pencairan search button
    cashout_search_handler : function(button, event)
    {   var the_search = true;
        if ( this.DataStore.getModifiedRecords().length > 0 )
        {   Ext.Msg.show(
                {   title:'W A R N I N G ',
                    msg: ' Modified Data Found, Do you want to save it before search process ? ',
                    buttons: Ext.Msg.YESNO,
                    fn: function(buttonId, text)
                        {   if (buttonId =='yes')
                            {   Ext.getCmp(tabId+'_pencairanSaveBtn').handler.call();
                                the_search = false;
                            } else the_search = true;
                        },
                    icon: Ext.MessageBox.WARNING
                    });
            };
            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.cashout.task,
                        act: alfalah.cashout.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
Ext.onReady(alfalah.cashout.initialize, alfalah.cashout);
// end of file
</script>
<div>&nbsp;</div>