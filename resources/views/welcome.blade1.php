<!DOCTYPE html>
<html lang="en">
<head>
    <title>LOGIN ALFALAH</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="{{asset('/Login_v16/images/icons/favicon.ico')}}"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('/Login_v16/vendor/bootstrap/css/bootstrap.min.css')}}"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('/Login_v16/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('/Login_v16/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('/Login_v16/vendor/animate/animate.css')}}"/>
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="{{asset('/Login_v16/vendor/css-hamburgers/hamburgers.min.css')}}"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('/Login_v16/vendor/animsition/css/animsition.min.css')}}"/>
<!--===============================================================================================--> 
    <link rel="stylesheet" type="text/css" href="{{asset('/Login_v16/vendor/select2/select2.min.css')}}"/>
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="{{asset('/Login_v16/vendor/daterangepicker/daterangepicker.css')}}"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('/Login_v16/css/util.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('/Login_v16/css/main.css')}}"/>
<!--===============================================================================================-->

</head>
<body>
    
    <div class="limiter">
   
        <div class="container-login100" style="background-image: url('academy/img/bg-img/alfalah-bg-1.jpg');">
        

                <div class="wrap-login100 p-t-30 p-b-50">
                
                    <span class="login100-form-title p-b-41">
                        ALFALAH Login
                       
                    </span>
                    <div class="container">
    <button class='tombol' href="{{ url('/ppdb/0/0') }}">PPDB ONLINE</button>
    </div>  

                    <form class="login100-form validate-form p-b-33 p-t-5" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="wrap-input100 validate-input" data-validate = "{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" class="input100" type="email" name="email" placeholder="User name" value="{{ old('email') }}" required autofocus>
                            
                            <span class="focus-input100" data-placeholder="&#xe82a;">
                                @if ($errors->has('email'))
                                    {{ $errors->first('email') }}
                                @endif
                            </span>
                            
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" class="input100" type="password" name="password" placeholder="Password" required>
                            <span class="focus-input100" data-placeholder="&#xe80f;">
                                @if ($errors->has('password'))
                                    {{ $errors->first('password') }}
                                @endif
                            </span>
                        </div>

                        <div class="container-login100-form-btn m-t-32">
                            <button class="login100-form-btn"><a href="{{ url('ppdb/0/0') }}">Login</button>
                            <!-- <a href="{{ route('password.request') }}"></a> -->
                        </div>

                    </form>
                   
                <div>
             
            <!-- </div> -->
        </div>
  
    </div>    
 
    <div id="dropDownSelect1"></div>
    
    

<!--===============================================================================================-->
    <script src="{{asset('/Login_v16/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('/Login_v16/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('/Login_v16/vendor/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('/Login_v16/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('/Login_v16/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('/Login_v16/vendor/daterangepicker/moment.min.js')}}"></script>
    <script src="{{asset('/Login_v16/vendor/daterangepicker/daterangepicker.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('/Login_v16/vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('/Login_v16/js/main.js')}}"></script>

</body>

</html>

