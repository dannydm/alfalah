<script type="text/javascript">
// create namespace
Ext.namespace('saa.inventory.incoming');

// create application
saa.inventory.incoming = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.poref.initialize();
            this.incoming.initialize();
            this.newsubdtl.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.poref.Tab);
            this.centerPanel.add(this.incoming.Tab);
            this.centerPanel.add(this.newsubdtl.Tab);
            this.centerPanel.setActiveTab(this.poref.Tab);
            this.centerPanel.endUpdate();
            saa.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
saa.inventory.incoming.incoming= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Company", width : 100,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"Company ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.company_name;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Grin.No", width : 100,
                    dataIndex : 'grin_no', sortable: true,
                    tooltip:"Grin No",
                },
                {   header: "Grin.Date", width : 100,
                    dataIndex : 'grin_date', sortable: true,
                    tooltip:"Grin Date",
                    // renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Reference", width : 100,
                    dataIndex : 'ref_no', sortable: true,
                    tooltip:"Document Reference No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.ref_type;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Supplier", width : 200,
                    dataIndex : 'party_name', sortable: true,
                    tooltip:"Supplier Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.party_id;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Grin Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:" Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                }
            ];
            this.Searchs = [
                {   id: 'inc_grin_no',
                    cid: 'grin_no',
                    fieldLabel: 'Grin.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'inc_ref_no',
                    cid: 'ref_no',
                    fieldLabel: 'Ref.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'inc_party_name',
                    cid: 'party_name',
                    fieldLabel: 'Supplier',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'inc_grin_date_1',
                    cid: 'grin_date_1',
                    fieldLabel: 'Grin.Date.1',
                    labelSeparator : '',
                    xtype: 'datefield',
                    width : 120,
                    format:'d/m/Y'
                    // value : new Date()
                },
                {   id: 'inc_grin_date_2',
                    cid: 'grin_date_2',
                    fieldLabel: 'Grin.Date.2',
                    labelSeparator : '',
                    xtype: 'datefield',
                    width : 120,
                    format:'d/m/Y'
                    // value : new Date()
                },
            ];
            this.DataStore = saa.core.newDataStore(
                '/inventory/1/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : saa.inventory.incoming.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'View',
                        tooltip:'View Incoming',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_view,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : this.print_out,
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_grinGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_grinPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_grinPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_grinGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    //this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_grinSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_grinTab",
                jsId : tabId+"_grinTab",
                title:  "Inventory Incoming",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 60,
                                defaultType: 'textfield',
                                items : this.Searchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.grin_search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        Grid_view : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   centerPanel = Ext.getCmp('center-panel');
                var grin_inc_form = Ext.getCmp("grin_inc_form");
                if (grin_inc_form)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Incoming Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    saa.inventory.incoming.forms.initialize(the_record.record);
                    saa.inventory.incoming.forms.mode_view = true;
                    centerPanel.beginUpdate();
                    centerPanel.add(saa.inventory.incoming.forms.Tab);
                    centerPanel.setActiveTab(saa.inventory.incoming.forms.Tab);
                    centerPanel.endUpdate();
                    saa.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        //  grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            this.DataStore.removeAll();
            saa.core.submitGrid(
                this.DataStore, 
                '/inventory/2/2',
                {   'x-csrf-token': saa.inventory.incoming.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.id }
            );
        },
        //  search button
        grin_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_grinSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
        print_out: function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                saa.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        id : the_record.id });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };   
        },
    }; // end of public space
}(); // end of app
// create application
saa.inventory.incoming.poref= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Company", width : 100,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"Company ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.company_name;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "PO.No", width : 150,
                    dataIndex : 'ref_no', sortable: true,
                    tooltip:"PO No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.ref_date;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Item name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.item_id;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Supplier", width : 200,
                    dataIndex : 'party_name', sortable: true,
                    tooltip:"Supplier Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.party_id;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "PO.Qty", width : 80,
                    dataIndex : 'quantity_primary', sortable: true,
                    tooltip:"PO Quantity",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.um_primary;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Incoming.Qty", width : 100,
                    dataIndex : 'incoming_qty', sortable: true,
                    tooltip:"Incoming Quantity",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.um_primary;
                        return saa.core.gridColumnWrap(result);
                    }
                }
            ];
            this.Searchs = [
                {   id: 'poref_po_no',
                    cid: 'po_no',
                    fieldLabel: 'PO.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'poref_item_name',
                    cid: 'item_name',
                    fieldLabel: 'Item.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'poref_supplier_name',
                    cid: 'supplier_name',
                    fieldLabel: 'Supplier',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.DataStore = saa.core.newDataStore(
                '/inventory/1/11', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : saa.inventory.incoming.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'New Incoming Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    '-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_porefGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_porefPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_porefPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_porefGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    //this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_poSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_porefTab",
                jsId : tabId+"_porefTab",
                title:  "Search.New.Incoming",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 50,
                                defaultType: 'textfield',
                                items : this.Searchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   //id : 'search_btn_pritems',
                                    text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.po_search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        Grid_add : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var incoming_form = Ext.getCmp("incoming_form");
                if (incoming_form)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Incoming Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    saa.inventory.incoming.forms.initialize(the_record.record);
                    saa.inventory.incoming.forms.mode_view = false;
                    centerPanel.beginUpdate();
                    centerPanel.add(saa.inventory.incoming.forms.Tab);
                    centerPanel.setActiveTab(saa.inventory.incoming.forms.Tab);
                    centerPanel.endUpdate();
                    saa.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        //  search button
        po_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_poSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
saa.inventory.incoming.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        mode_view  : false,
        // public methods
        initialize: function(the_record)
        {   this.Records = the_record;
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.CurrencyDS = saa.core.newDataStore(
                '/country/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.PartyDS = saa.core.newDataStore(
                '/company/2/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.WarehouseDS = saa.core.newDataStore(
                '/company/9/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.ItemDS = saa.core.newDataStore(
                '/inventory/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Columns = [
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Items",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.item_id+' ';
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "PO.Qty", width : 100,
                    dataIndex : 'po_qty_primary', sortable: true,
                    tooltip:"PO.Quantity",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if (saa.inventory.incoming.forms.mode_view == true){}
                        else
                        {   if ( record.data.incoming_qty > 1)
                            {   metaData.attr = "style = background-color:yellow;";  }
                            else if ( record.data.incoming_qty > value) 
                                {   metaData.attr = "style = background-color:red;"; }
                            else {   metaData.attr = "style = background-color:lime;"; };
                        };
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Incoming.Qty", width : 100,
                    dataIndex : 'incoming_qty', sortable: true,
                    tooltip:"Incoming.Quantity",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if (saa.inventory.incoming.forms.mode_view == true){}
                        else
                        {   if ( value > 1)
                            {   metaData.attr = "style = background-color:yellow;";  }
                            else if ( value > record.data.po_qty_primary) 
                                {   metaData.attr = "style = background-color:red;"; }
                            else {   metaData.attr = "style = background-color:lime;"; };
                        };
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Quantity", width : 100,
                    dataIndex : 'quantity_primary', sortable: true,
                    tooltip:"Quantity",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if (saa.inventory.incoming.forms.mode_view == true){}
                        else
                        {   if  ( ( value == 0) || 
                                  ( value < (   parseInt(record.data.po_qty_primary) - 
                                                parseInt(record.data.incoming_qty) + 1)
                                  )
                                )
                                {   metaData.attr = "style = background-color:lime;"; }
                            else {   metaData.attr = "style = background-color:red;"; };
                        };
                        return saa.core.gridColumnWrap(value);
                    }  
                },
                {   header: "Reject", width : 80,
                    dataIndex : 'reject_primary', sortable: true,
                    tooltip:"Reject Quantity",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if (value > 0)
                        {   metaData.attr = "style = background-color:red;"; };
                        return value;
                    }
                },
                {   header: "Total", width : 100,
                    dataIndex : 'total_primary', sortable: true,
                    tooltip:"TotalQuantity",
                },
                {   header: "Pcs@bag", width : 80,
                    dataIndex : 'pcs_subdtl', sortable: true,
                    tooltip:"Pieces per bag",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "UM", width : 80,
                    dataIndex : 'um_primary', sortable: true,
                    tooltip:"Unit Measurement",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                // {   header: "Price", width : 100,
                //     dataIndex : 'unit_price', sortable: true,
                //     tooltip:"Prices",
                //     editor : new Ext.form.TextField({allowBlank: false}),
                // },
                // {   header: "Amount", width : 100,
                //     dataIndex : 'amount', sortable: true,
                //     tooltip:"Amount",
                //     editor : new Ext.form.TextField({allowBlank: false}),
                // },
                {   header: "Remark", width : 300,
                    dataIndex : 'remark', sortable: true,
                    tooltip:"Item Remark",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
            ];
            if (Ext.isObject(this.Records))
            {   if (Ext.isEmpty(this.Records.data.grin_no))
                {   the_title = "New Incoming"; 
                    the_save = {   
                        text:'Save Incoming',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        scope : this
                    };
                    // load reference detail
                    this.DataStore = saa.core.newDataStore(
                        '/inventory/1/12', false,
                        {   s:"form", po_no: this.Records.data["ref_no"],
                            limit:this.page_limit, start:this.page_start }
                    );
                } 
                else 
                {  the_title = "View Incoming"; 
                   the_save = '-';
                   // load grin detail
                   this.DataStore = saa.core.newDataStore(
                        '/inventory/1/5', false,
                        {   s:"form", grin_no : this.Records.data.grin_no,
                            limit:this.page_limit, start:this.page_start }
                    );
                };
            };
            this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : 200, //saa.country.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [the_save],
                listeners :
                {   //"beforeedit" : this.Grid_beforeedit,
                    "afteredit" : this.Grid_afteredit
                }
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   id : 'inc_form',
                title : 'H E A D E R',
                bodyStyle:'padding:5px',
                // autoScroll : true,
                // autoHeight : true,
                items: [
                {   layout:'column',
                    border:false,
                    items:[ // hidden columns
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'incfrm_id',
                            xtype:'textfield',
                            fieldLabel: 'ID',
                            name: 'id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }, 
                        {   id : 'incfrm_ref_type',
                            xtype:'textfield',
                            fieldLabel: 'Ref.Type',
                            name: 'ref_type',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'incfrm_company_id',
                            xtype:'textfield',
                            fieldLabel: 'Company.ID',
                            name: 'company_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'incfrm_party_id',
                            xtype:'textfield',
                            fieldLabel: 'Party.ID',
                            name: 'party_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'incfrm_grin_status',
                            xtype:'textfield',
                            fieldLabel: 'Grin.Status',
                            name: 'grin_status',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'incfrm_to_site_id',  
                            xtype:'textfield',
                            fieldLabel: 'Site.ID',
                            name: 'site_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }, 
                        {   id : 'incfrm_currency_id', 
                            xtype:'textfield',
                            fieldLabel: 'Currency',
                            name: 'currency_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'incfrm_warehouse_id',
                            xtype:'textfield',
                            fieldLabel: 'Warehouse.ID',
                            name: 'warehouse_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'incfrm_created_date',  
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            hidden : true,
                        }]
                    }, 
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'incfrm_grin_date', 
                            xtype:'datefield',
                            fieldLabel: 'Grin Date',
                            name: 'grin_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            value : new Date(),
                            readOnly : true,
                        },
                        {   id : 'incfrm_ref_no', 
                            xtype:'textfield',
                            fieldLabel: 'REF.No',
                            name: 'ref_no',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                        },
                        {   id : 'incfrm_grin_no', 
                            xtype:'textfield',
                            fieldLabel: 'Grin.No',
                            name: 'grin_no',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly: true,
                        },
                        {   id : 'incfrm_delivery_no', 
                            xtype:'textfield',
                            fieldLabel: 'Delivery.No',
                            name: 'delivery_no',
                            anchor:'95%',
                            allowBlank: true,
                        },
                        {   id : 'incfrm_delivery_date', 
                            xtype:'datefield',
                            fieldLabel: 'Delivery.Date',
                            name: 'delivery_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                        },
                        {   id : 'incfrm_warehouse_name',
                            xtype:'combo',
                            fieldLabel: 'Warehouse',
                            name: 'warehouse_name',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.WarehouseDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'name',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form", 
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('incfrm_to_site_id').setValue(record.data.site_id);
                                        Ext.getCmp('incfrm_warehouse_id').setValue(record.data.warehouse_id);
                                    },
                            }
                        },
                        ]
                    }, 
                    {   columnWidth:.7, 
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'incfrm_company_name', 
                            xtype:'textfield',
                            fieldLabel: 'Company.Name',
                            name: 'company_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'incfrm_party_name',
                            xtype:'combo',
                            fieldLabel: 'Supplier',
                            name: 'party_name',
                            anchor:'95%',
                            allowBlank: true,
                            store: this.PartyDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'name',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form", t:"SP",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('incfrm_party_id').setValue(record.data.company_id);
                                    },
                            }
                        },
                        {   id : 'incfrm_company_name', 
                            xtype:'textfield',
                            fieldLabel: 'Company.Name',
                            name: 'company_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'incfrm_curier_no', 
                            xtype:'textfield',
                            fieldLabel: 'Ship/Vehicle.No',
                            name: 'curier_no',
                            anchor:'95%',
                            allowBlank: true,
                        },
                        {   id : 'incfrm_curier_name', 
                            xtype:'textfield',
                            fieldLabel: 'Ship/Vehicle.Name',
                            name: 'curier_name',
                            anchor:'95%',
                            allowBlank: true,
                        },
                        {   id : 'incfrm_curier_pic', 
                            xtype:'textfield',
                            fieldLabel: 'Currier.PIC',
                            name: 'curier_pic',
                            anchor:'95%',
                            allowBlank: true,
                        },
                        {   id : 'incfrm_curier_phone_no', 
                            xtype:'textfield',
                            fieldLabel: 'Currier.Phone',
                            name: 'curier_phone_no',
                            anchor:'95%',
                            allowBlank: true,
                        }
                        ]
                    }]  
                },  // description area
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.9,
                        layout: 'form',
                        border:false,
                        items: [ 
                        {   id : 'incfrm_description', 
                            xtype:'textarea',
                            fieldLabel: 'Description',
                            name: 'description',
                            anchor:'95%',
                            // allowBlank: false,
                            height : 100,
                        }]
                    }]
                }]
            });
        },
        // build the layout
        build_layout: function()
        {   
            this.Tab = new Ext.Panel(
            {   id : "grin_inc_form",
                jsId : "grin_inc_form",
                // title:  the_title,
                title : '<span style="background-color: yellow;">'+the_title+'</span>',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region: 'center',     
                    xtype: 'tabpanel',
                    activeTab: 0,
                    items:[this.Form, this.detailTab]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   
            if (Ext.isObject(this.Records))
            {   Ext.each(this.Records.fields.items,
                    function(the_field)
                    {   switch (the_field.name)
                        {   case "id" :
                            case "created_date" :
                            case "company_id" :
                            case "company_name" :
                            case "grin_date" :
                            case "grin_status" :
                            case "to_site_id" :
                            case "currency_id" :
                            case "to_dept_name" :
                            case "description" :
                            case "party_id" :
                            case "party_name" :
                            case "delivery_no" :
                            case "delivery_date" :
                            case "ref_type" :
                            case "warehouse_id" :
                            case "warehouse_name" :
                            case "curier_no" :
                            case "curier_name" :
                            case "curier_pic" :
                            case "curier_phone_no" :
                            {   //console.log('incfrm_'+the_field.name);
                                Ext.getCmp('incfrm_'+the_field.name).setValue(this.Records.data[the_field.name]);
                            };
                            break;
                            case "ref_no" :
                            {   Ext.getCmp('incfrm_'+the_field.name).setValue(this.Records.data[the_field.name]);
                                // this.DataStore.baseParams = {   
                                //     s:"form", grin_no: this.Records.data["grin_no"],
                                //     limit:this.page_limit, start:this.page_start};
                                // this.DataStore.reload();
                            };
                            break;
                            case "grin_no" :
                            {   Ext.getCmp('incfrm_'+the_field.name).setValue(this.Records.data[the_field.name]);
                                // this.DataStore.baseParams = {   
                                //     s:"form", grin_no: this.Records.data["grin_no"],
                                //     limit:this.page_limit, start:this.page_start};
                                // this.DataStore.reload();
                            };
                            break;
                        };
                    }, this);
            };
            this.Records = Ext.data.Record.create(
            [   {name: 'grin_no', type: 'string'},
                {name: 'ref_no', type: 'string'},
                {name: 'ref_type', type: 'string'},
                {name: 'item_id', type: 'string'},
                {name: 'item_name', type: 'string'},
                {name: 'quantity_primary', type: 'string'},
                {name: 'um_primary', type: 'string'},
                {name: 'unit_price', type: 'string'},
                {name: 'amount', type: 'string'},
                {name: 'remark', type: 'string'},
                {name: 'created_date', type: 'date'}
            ]);
        },
        // pritems search button
        pritems_search_handler : function(button, event)
        {   the_parameter = saa.core.getSearchParameter(this.pritemsSearchs);
            this.pritemsDataStore.removeAll();
            this.pritemsDataStore.baseParams = Ext.apply( the_parameter,
            {   s:"form",
                limit:this.page_limit, start:this.page_start });
            this.pritemsDataStore.reload();
        },
        Grid_afteredit : function(the_cell)
        {   switch (the_cell.field)
            {   case "quantity_primary":
                case "reject_primary":
                case "unit_price":
                {   var data             = the_cell.record.data;
                    var value            = parseInt(the_cell.value);
                    var unit_price       = parseInt(data.unit_price);
                    var po_qty_primary   = parseInt(data.po_qty_primary);
                    var incoming_qty     = parseInt(data.incoming_qty);
                    var quantity_primary = parseInt(data.quantity_primary);
                    var reject_primary   = parseInt(data.reject_primary);
                    var is_continue      = true;

                    if ( (quantity_primary + reject_primary) > (po_qty_primary - incoming_qty) )
                    {   Ext.Msg.show(
                        {   title:'W A R N I N G ',
                            msg: ' Incoming.Quantity is higher than Remaining.Quantity <BR>Do you want to process it ? ',
                            buttons: Ext.Msg.YESNO,
                            fn: function(buttonId, text)
                                {   if (buttonId =='yes')
                                    {   the_cell.grid.getView().refresh();
                                    } else 
                                    {   data.quantity_primary = 0;
                                        data.reject_primary   = 0;
                                        data.total_primary    = 0;
                                        is_continue = false;
                                        the_cell.grid.getView().refresh();
                                    };
                                },
                            icon: Ext.MessageBox.WARNING
                        });
                    };
                    // total quantity
                    if (is_continue == true)
                    {   data.total_primary = quantity_primary + reject_primary;
                        the_cell.grid.getView().refresh();
                    };
                };
                break;
            };
        },
        // forms grid save records
        Form_save : function(button, event)
        {   var head_data = [];
            var json_data = [];
            var modi_data = [];
            var v_json = {};
            var the_datastore = this.DataStore;
            if (saa.core.validateFields([
                'incfrm_party_name', 'incfrm_delivery_date']))
            {   // detail validation
                var item_count = this.DataStore.getCount()
                if ( item_count < 1 ) 
                {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'Please register minimum 1 Item.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO,
                        width : 300
                    });
                }
                else 
                    // if ( saa.core.validateGridQuantity(
                    //         // this.DataStore.getModifiedRecords(),
                    //         the_datastore.data.items,
                    //         ['quantity_primary']))
                {   // header
                    head_data = saa.core.getHeadData([
                        'incfrm_id', 'incfrm_company_id', 'incfrm_party_id', 'incfrm_grin_status',
                        'incfrm_to_site_id', 'incfrm_currency_id', 'incfrm_warehouse_id',
                        'incfrm_created_date', 'incfrm_grin_date', 'incfrm_grin_no', 
                        'incfrm_ref_no', 'incfrm_ref_type', 
                        'incfrm_delivery_date', 'incfrm_company_name', 'incfrm_party_name',
                        'incfrm_warehouse_name', 'incfrm_delivery_no', 'incfrm_description',
                        'incfrm_curier_no', 'incfrm_curier_name', 'incfrm_curier_pic', 'incfrm_curier_phone_no']);
                    // detail
                    json_data = saa.core.getDetailData(this.DataStore.getModifiedRecords());
                    // submit data
                    saa.core.submitForm(
                        'grin_inc_form', 
                        saa.inventory.incoming.incoming.DataStore,
                        '/inventory/1/1',
                        {   'x-csrf-token': saa.inventory.incoming.sid },
                        {   task: 'save',
                            head : Ext.encode(head_data),
                            json : Ext.encode(json_data),
                        });
                };
            };
        },
    }; // end of public space
}(); // end of app
// create application
saa.inventory.incoming.newsubdtl= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "GRIN.No", width : 100,
                    dataIndex : 'grin_no', sortable: true,
                    tooltip:"GRIN No",
                },
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Item name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.item_id;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Quantity", width : 100,
                    dataIndex : 'quantity_primary', sortable: true,
                    tooltip:"Supplier Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( parseInt(record.data.quantity_primary) > 
                                (   parseInt(record.data.subdetail_qty) +
                                    parseInt(record.data.reject_primary)
                                ) )
                        {   metaData.attr = "style = background-color:yellow;";  }
                        else {   metaData.attr = "style = background-color:lime;"; };
                        result = value+'<br>'+record.data.um_primary;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "SubDTL.Qty", width : 100,
                    dataIndex : 'subdetail_qty', sortable: true,
                    tooltip:"Total Quantity of SubDetail",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( parseInt(record.data.quantity_primary) > 
                                (   parseInt(record.data.subdetail_qty) +
                                    parseInt(record.data.reject_primary)
                                ) )
                        {   metaData.attr = "style = background-color:yellow;";  }
                        else if ( Ext.isEmpty(record.data.subdetail_qty) )
                        {   metaData.attr = "style = background-color:red;";  }
                        else {   metaData.attr = "style = background-color:lime;"; };
                        result = value+'<br>'+record.data.um_primary;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Reject.Qty", width : 100,
                    dataIndex : 'reject_primary', sortable: true,
                    tooltip:"Total Reject Quantity",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value > 0)
                        {   metaData.attr = "style = background-color:red;";  };
                        result = value+'<br>'+record.data.um_primary;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"PO Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:" Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                }
            ];
            this.Searchs = [
                {   id: 'newsubdtl_grin_no',
                    cid: 'grin_no',
                    fieldLabel: 'GRIN.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'newsubdtl_item_name',
                    cid: 'item_name',
                    fieldLabel: 'Item.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'newsubdtl_supplier_name',
                    cid: 'supplier_name',
                    fieldLabel: 'Supplier',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.DataStore = saa.core.newDataStore(
                '/inventory/1/13', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : saa.inventory.incoming.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Edit',
                        tooltip:'SubDetail of Incoming',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_subdetail,
                        scope : this
                    },
                    '-',
                    {   text:'Sync Quantity',
                        tooltip:'Syncronize Quantity',
                        iconCls: 'silk-arrow-refresh',
                        handler : this.Grid_syncronize,
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_newsubdtlGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_newsubdtlPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_newsubdtlPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_newsubdtlGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    //this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_poSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_newsubdtlTab",
                jsId : tabId+"_newsubdtlTab",
                title:  "Sub-Detail",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 50,
                                defaultType: 'textfield',
                                items : this.Searchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   //id : 'search_btn_pritems',
                                    text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        Grid_subdetail : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   centerPanel = Ext.getCmp('center-panel');
                var grin_inc_form = Ext.getCmp("grin_detail");
                if (grin_inc_form)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Grin Detail Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    saa.inventory.incoming.subdetail.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(saa.inventory.incoming.subdetail.Tab);
                    centerPanel.setActiveTab(saa.inventory.incoming.subdetail.Tab);
                    centerPanel.endUpdate();
                    saa.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        //  grid syncronize quantity
        Grid_syncronize: function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   saa.core.submitGrid(
                    this.DataStore, 
                    '/inventory/1/17',
                    {   'x-csrf-token': saa.inventory.incoming.sid }, 
                    {   id: this.Grid.getSelectionModel().selection.record.data.id,
                        grin_no: this.Grid.getSelectionModel().selection.record.data.grin_no 
                    }
                );
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        //  search button
        search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_poSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
saa.inventory.incoming.subdetail= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        seq_no : 1,
        is_inserted : true,
        // public methods
        initialize: function(the_record)
        {   this.Records = the_record;
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.CurrencyDS = saa.core.newDataStore(
                '/country/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.PartyDS = saa.core.newDataStore(
                '/company/2/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.WarehouseDS = saa.core.newDataStore(
                '/company/9/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.ItemDS = saa.core.newDataStore(
                '/inventory/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Columns = [
                {   header: "NO of Bags", width : 80,
                    dataIndex : 'seq_no', sortable: true,
                    tooltip:"Sequence No",
                },
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Items",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.item_id+' ';
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Quantity", width : 80,
                    dataIndex : 'quantity_primary', sortable: true,
                    tooltip:"Quantity",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Rjc.Pecah", width : 80,
                    dataIndex : 'reject_pecah', sortable: true,
                    tooltip:"Reject Pecah",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value > 0)
                        {   metaData.attr = "style = background-color:red;";  };
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Rjc.Tunas", width : 80,
                    dataIndex : 'reject_tunas', sortable: true,
                    tooltip:"Reject Tunas",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value > 0)
                        {   metaData.attr = "style = background-color:red;";  };
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Rjc.Muda", width : 80,
                    dataIndex : 'reject_muda', sortable: true,
                    tooltip:"Reject Muda",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value > 0)
                        {   metaData.attr = "style = background-color:red;";  };
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Rjc.Retak", width : 80,
                    dataIndex : 'reject_retak', sortable: true,
                    tooltip:"Reject Retak",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value > 0)
                        {   metaData.attr = "style = background-color:red;";  };
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Rjc.Basah", width : 80,
                    dataIndex : 'reject_basah', sortable: true,
                    tooltip:"Reject Basah",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value > 0)
                        {   metaData.attr = "style = background-color:red;";  };
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Rjc.Busuk", width : 80,
                    dataIndex : 'reject_busuk', sortable: true,
                    tooltip:"Reject Busuk",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value > 0)
                        {   metaData.attr = "style = background-color:red;";  };
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Group.No", width : 80,
                    dataIndex : 'group_no', sortable: true,
                    tooltip:"Group No",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    // renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    // {   if ( value > 0)
                    //     {   metaData.attr = "style = background-color:red;";  };
                    //     return saa.core.gridColumnWrap(value);
                    // }
                },
                {   header: "UM", width : 80,
                    dataIndex : 'um_primary', sortable: true,
                    tooltip:"Unit Measurement",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Remark", width : 300,
                    dataIndex : 'remark', sortable: true,
                    tooltip:"Item Remark",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
            ];
            the_title = "Edit SubDetail"; 
            // load grin subdetail
            this.DataStore = new Ext.data.Store(
                {   proxy:  new Ext.data.HttpProxy(
                    {   method: 'GET',
                        url: '/inventory/1/14',
                    }),
                    baseParams: {   s:"form", grin_no : this.Records.data.grin_no,
                                    limit:this.page_limit, start:this.page_start },
                    reader: new Ext.data.JsonReader(),
                    remoteFilter : false,
                    pruneModifiedRecords : true
                });
            this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : 200, //saa.country.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    '-',
                    {   text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    }
                ],
                // listeners :
                // {   //"beforeedit" : this.Grid_beforeedit,
                //     "afteredit" : this.Grid_afteredit
                // },
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_incdtlGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_incdtlPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_incdtlPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_incdtlGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    //this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_grinSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.subdetailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   id : 'inc_subdetail',
                title : 'H E A D E R',
                bodyStyle:'padding:5px',
                // autoScroll : true,
                // autoHeight : true,
                
                items: [
                {   layout:'column',
                    border:false,
                    items:[ // hidden columns
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'incdtl_id',
                            xtype:'textfield',
                            fieldLabel: 'ID',
                            name: 'id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }, 
                        {   id : 'incdtl_ref_type',
                            xtype:'textfield',
                            fieldLabel: 'Ref.Type',
                            name: 'ref_type',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'incdtl_company_id',
                            xtype:'textfield',
                            fieldLabel: 'Company.ID',
                            name: 'company_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'incdtl_party_id',
                            xtype:'textfield',
                            fieldLabel: 'Party.ID',
                            name: 'party_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'incdtl_grin_status',
                            xtype:'textfield',
                            fieldLabel: 'Grin.Status',
                            name: 'grin_status',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'incdtl_to_site_id',  
                            xtype:'textfield',
                            fieldLabel: 'Site.ID',
                            name: 'site_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }, 
                        {   id : 'incdtl_currency_id', 
                            xtype:'textfield',
                            fieldLabel: 'Currency',
                            name: 'currency_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'incdtl_warehouse_id',
                            xtype:'textfield',
                            fieldLabel: 'Warehouse.ID',
                            name: 'warehouse_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'incdtl_seq_no',
                            xtype:'textfield',
                            fieldLabel: 'Max.Seq.No',
                            name: 'seq_no',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'incdtl_created_date',  
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            hidden : true,
                        }]
                    }, 
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'incdtl_grin_date', 
                            xtype:'datefield',
                            fieldLabel: 'Grin Date',
                            name: 'grin_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            value : new Date(),
                            readOnly : true,
                        },
                        {   id : 'incdtl_ref_no', 
                            xtype:'textfield',
                            fieldLabel: 'REF.No',
                            name: 'ref_no',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                        },
                        {   id : 'incdtl_grin_no', 
                            xtype:'textfield',
                            fieldLabel: 'Grin.No',
                            name: 'grin_no',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly: true,
                        },
                        {   id : 'incdtl_delivery_no', 
                            xtype:'textfield',
                            fieldLabel: 'Delivery.No',
                            name: 'delivery_no',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'incdtl_delivery_date', 
                            xtype:'datefield',
                            fieldLabel: 'Delivery.Date',
                            name: 'delivery_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            readOnly : true,
                        },
                        {   id : 'incdtl_warehouse_name',
                            xtype:'combo',
                            fieldLabel: 'Warehouse',
                            name: 'warehouse_name',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            store: this.WarehouseDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'name',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form", 
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('incdtl_to_site_id').setValue(record.data.site_id);
                                        Ext.getCmp('incdtl_warehouse_id').setValue(record.data.warehouse_id);
                                    },
                            }
                        },
                        ]
                    }, 
                    {   columnWidth:.7, 
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'incdtl_company_name', 
                            xtype:'textfield',
                            fieldLabel: 'Company.Name',
                            name: 'company_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'incdtl_party_name',
                            xtype:'combo',
                            fieldLabel: 'Supplier',
                            name: 'party_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            store: this.PartyDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'name',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form", t:"SP",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('incdtl_party_id').setValue(record.data.company_id);
                                    },
                            }
                        },
                        {   id : 'incdtl_company_name', 
                            xtype:'textfield',
                            fieldLabel: 'Company.Name',
                            name: 'company_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'incdtl_curier_no', 
                            xtype:'textfield',
                            fieldLabel: 'Ship/Vehicle.No',
                            name: 'curier_no',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'incdtl_curier_name', 
                            xtype:'textfield',
                            fieldLabel: 'Ship/Vehicle.Name',
                            name: 'curier_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'incdtl_curier_pic', 
                            xtype:'textfield',
                            fieldLabel: 'Currier.PIC',
                            name: 'curier_pic',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'incdtl_curier_phone_no', 
                            xtype:'textfield',
                            fieldLabel: 'Currier.Phone',
                            name: 'curier_phone_no',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        }
                        ]
                    }]  
                },  // description area
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.9,
                        layout: 'form',
                        border:false,
                        items: [ 
                        {   id : 'incdtl_description', 
                            xtype:'textarea',
                            fieldLabel: 'Description',
                            name: 'description',
                            anchor:'95%',
                            readOnly : true,
                            height : 100,
                        }]
                    }]
                }]
            });
        },
        // build the layout
        build_layout: function()
        {   
            this.Tab = new Ext.Panel(
            {   id : "grin_subdetail",
                jsId : "grin_subdetail",
                // title:  the_title,
                title : '<span style="background-color: yellow;">'+the_title+'</span>',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region: 'center',     
                    xtype: 'tabpanel',
                    activeTab: 0,
                    items:[this.Form, this.subdetailTab]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   
            if (Ext.isObject(this.Records))
            {   Ext.each(this.Records.fields.items,
                    function(the_field)
                    {   switch (the_field.name)
                        {   case "id" :
                            case "created_date" :
                            case "company_id" :
                            case "company_name" :
                            case "grin_no":
                            case "grin_date" :
                            case "grin_status" :
                            case "to_site_id" :
                            case "currency_id" :
                            case "to_dept_name" :
                            case "description" :
                            case "party_id" :
                            case "party_name" :
                            case "delivery_no" :
                            case "delivery_date" :
                            case "ref_no" :
                            case "ref_type" :
                            case "warehouse_id" :
                            case "warehouse_name" :
                            case "curier_no" :
                            case "curier_name" :
                            case "curier_pic" :
                            case "curier_phone_no" :
                            {   Ext.getCmp('incdtl_'+the_field.name).setValue(this.Records.data[the_field.name]);
                            };
                            break;
                            case "max_seq_no" :
                            {   console.log("max_seq_no");
                                Ext.getCmp('incdtl_seq_no').setValue(parseInt(this.Records.data[the_field.name]));
                                // this.seq_no = parseInt(this.Records.data[the_field.name]) + 1;
                            };
                            break;
                            case "subdetail_qty" :
                            {   if ( parseInt(this.Records.data["subdetail_qty"]) > parseInt(this.Records.data["quantity_primary"]) )
                                {   this.is_inserted = false;   };
                            };
                            break;
                        };
                    }, this);
            };
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'grin_no', type: 'string'},
                {name: 'item_id', type: 'string'},
                {name: 'seq_no', type: 'string'},
                {name: 'item_name', type: 'string'},
                {name: 'quantity_primary', type: 'string'}, 
                {name: 'reject_pecah', type: 'string'}, 
                {name: 'reject_tunas', type: 'string'}, 
                {name: 'reject_muda', type: 'string'}, 
                {name: 'reject_retak', type: 'string'}, 
                {name: 'reject_basah', type: 'string'}, 
                {name: 'reject_busuk', type: 'string'}, 
                {name: 'group_no', type: 'string'}, 
                {name: 'um_primary', type: 'string'},
                {name: 'remark', type: 'string'},
                {name: 'created_date', type: 'date'}
            ]);
        },
        Store_insert : function (is_inserted, the_record)
        {   if (is_inserted == true)
            {   max_seq_no = parseInt(Ext.getCmp('incdtl_seq_no').getValue());
                if (max_seq_no > 0) { the_seq_no = max_seq_no + 1; }
                else { the_seq_no = parseInt(the_record.record.data.seq_no) + 1; };
                // console.log("the_seq_no "+the_seq_no);
                the_record_count = the_seq_no + 5;
                // console.log("the_record_count "+the_record_count);
                var the_x = the_seq_no;
                // console.log("the_x "+the_x);
                for (the_x; the_x < the_record_count; the_x++)
                {   this.Grid.store.insert( 0,
                        new this.Records (
                        {   id : "",
                            grin_no: the_record.record.data.grin_no,
                            item_id: the_record.record.data.item_id,
                            item_name: the_record.record.data.item_name,
                            seq_no : the_x,
                            quantity_primary : 0,
                            um_primary   : the_record.record.data.um_primary,
                            reject_pecah : 0,
                            reject_tunas : 0, 
                            reject_muda  : 0,
                            reject_retak : 0,
                            reject_basah : 0,
                            reject_busuk : 0,
                            group_no     : 0,
                            remark : "",
                            created_date: "",
                        }));
                };
                Ext.getCmp('incdtl_seq_no').setValue(the_x -1);
                this.Grid.startEditing(0, 0);
            };
        },
        Grid_add : function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   is_inserted = this.is_inserted;
                if (is_inserted == false)
                {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' SubDetail.Quantity is higher than Detail.Quantity <BR>Do you want to process it ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   saa.inventory.incoming.subdetail.Store_insert(true, the_record);    };
                            },
                        icon: Ext.MessageBox.WARNING
                    });
                };
                if (is_inserted == true)
                {   this.Store_insert(is_inserted, the_record); };
            }
            else
            {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Data Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            };
        },
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   if (Ext.isEmpty(the_record.record.data.id))
                {   the_store = this.Grid.getStore();
                    the_store.removeAt(the_record.cell[0]);  
                }
                else
                {   the_record.record.set("remark", "REMOVED"); };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        validateSubdetailQuantity: function(the_records)
        {   validation = true;
            Ext.each(the_records,
            function(the_record)
            {   the_data = the_record.data;
                TotalQuantity = parseInt(the_data.quantity_primary) + 
                                parseInt(the_data.reject_pecah) +
                                parseInt(the_data.reject_tunas) +
                                parseInt(the_data.reject_muda) +
                                parseInt(the_data.reject_retak) +
                                parseInt(the_data.reject_basah) +
                                parseInt(the_data.reject_busuk);
                if (TotalQuantity < 1 ) 
                {   validation = false; 
                    Ext.Msg.show(
                    {   title:'E R R O R ',
                        msg: 'Found Zero quantiy of TotalQuantity from item '+the_data.item_name,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        width : 300
                    });
                }
            });
            return validation;
        },
        // subdetail grid save records
        Form_save : function(button, event)
        {   var head_data = [];
            var json_data = [];
            var modi_data = [];
            var v_json = {};
            var the_datastore = this.DataStore;
            console.log(the_datastore);
            if (saa.core.validateFields([
                'incdtl_party_name', 'incdtl_delivery_date']))
            {   // subdetail validation
                var item_count = this.DataStore.getCount()
                if ( item_count < 1 ) 
                {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'Please register minimum 1 Item.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO,
                        width : 300
                    });
                }
                else if ( this.validateSubdetailQuantity(the_datastore.getModifiedRecords()))
                    // saa.core.validateGridQuantity(
                    //         // this.DataStore.getModifiedRecords(),
                    //         the_datastore.data.items,
                    //         ['quantity_primary'])
                {   json_data = saa.core.getDetailData(this.DataStore.getModifiedRecords());
                    // submit data
                    saa.core.submitForm(
                        'grin_subdetail', 
                        saa.inventory.incoming.newsubdtl.DataStore,
                        '/inventory/1/15',
                        {   'x-csrf-token': saa.inventory.incoming.sid },
                        {   task: 'save',
                            json : Ext.encode(json_data),
                        });
                };
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(saa.inventory.incoming.initialize, saa.inventory.incoming);
// end of file
</script>
<div>&nbsp;</div>