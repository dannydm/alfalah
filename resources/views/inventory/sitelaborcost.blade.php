<script type="text/javascript">
// create namespace
Ext.namespace('saa.inventory.master');

// create application
saa.inventory.master = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.sitelaborcost.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.sitelaborcost.Tab);
            this.centerPanel.setActiveTab(this.sitelaborcost.Tab);
            this.centerPanel.endUpdate();
            saa.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
saa.inventory.master.sitelaborcost= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.SiteDeptDS = saa.core.newDataStore(
                '/company/6/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SiteDeptDS.load();            
            
            this.Columns = [
                {   header: "Comp.ID", width : 50,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"company ID"
                },
                {   header: "Site.Name", width : 150,
                    dataIndex : 'site_name', sortable: true,
                    tooltip:"Site Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.site_id;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Dept.", width : 140,
                    dataIndex : 'dept_id', sortable: true,
                    tooltip:"Dept.Name",
                    editor: new Ext.form.ComboBox(
                    {   store: this.SiteDeptDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'dept_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.company_id = record.data.company_id;
                                    combo.gridEditor.record.data.site_id = record.data.site_id;
                                    combo.gridEditor.record.data.dept_id = record.data.dept_id;
                                    combo.gridEditor.record.data.dept_name = record.data.dept_name;
                                    saa.inventory.master.sitelaborcost.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.dept_name+'<br>'+value;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Unloading", width : 100,
                    dataIndex : 'unloading_cost', sortable: true,
                    tooltip:"Cost of Unloading Activity",
                    css : "background-color: #56FF00;",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Sorting", width : 100,
                    dataIndex : 'sorting_cost', sortable: true,
                    tooltip:"Cost of Sorting Activity",
                    css : "background-color: #56FF00;",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Packing", width : 100,
                    dataIndex : 'packing_cost', sortable: true,
                    tooltip:"Cost of Packing Activity",
                    css : "background-color: #56FF00;",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Loading", width : 100,
                    dataIndex : 'loading_cost', sortable: true,
                    tooltip:"Cost of loading Activity",
                    css : "background-color: #56FF00;",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"sitelaborcost Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"sitelaborcost Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                {name: 'company_id', type: 'string'},
                {name: 'site_id', type: 'string'},
                {name: 'dept_id', type: 'string'},
                {name: 'unloading_cost', type: 'string'},
                {name: 'sorting_cost', type: 'string'},
                {name: 'packing_cost', type: 'string'},
                {name: 'loading_cost', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'sitelaborcost_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sitelaborcost_symbol',
                    cid: 'symbol',
                    fieldLabel: 'Symbol',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sitelaborcost_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ [''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: ''
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_sitelaborcostSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.sitelaborcost_search_handler,
                scope : this
            });
            this.DataStore = saa.core.newDataStore(
                '/inventory/5/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : saa.inventory.master.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   id : tabId+'_sitelaborcostSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event)
                        {   saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event)
                        {   saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_sitelaborcostGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_sitelaborcostPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_sitelaborcostPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_sitelaborcostGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_sitelaborcostTab",
                jsId : tabId+"_sitelaborcostTab",
                title:  "Site Labor Costs",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // sitelaborcost grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   id: "",
                    company_id : "",
                    site_id: "",
                    dept_id : "",
                    dept_name: "New Name",
                    unloading_cost: 0,
                    sorting_cost: 0,
                    packing_cost: 0,
                    loading_cost: 0,
                    created_date: "",
                    modified_date: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // sitelaborcost grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = saa.core.getDetailData(the_records);
                saa.core.submitGrid(
                    this.DataStore, 
                    '/inventory/5/1',
                    {   'x-csrf-token': saa.inventory.master.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // sitelaborcost grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            saa.core.submitGrid(
                this.DataStore, 
                '/inventory/5/2',
                {   'x-csrf-token': saa.inventory.master.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.id }
            );
        },
        // sitelaborcost search button
        sitelaborcost_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_sitelaborcostSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);

                this.DataStore.baseParams = Ext.apply( the_parameter, 
                    {s:"form", limit:this.page_limit, start:this.page_start, abc: 0});
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(saa.inventory.master.initialize, saa.inventory.master);
// end of file
</script>
<div>&nbsp;</div>