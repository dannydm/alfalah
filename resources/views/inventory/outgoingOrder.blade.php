<script type="text/javascript">
// create namespace
Ext.namespace('saa.inventory.outgoingOrder');

// create application
saa.inventory.outgoingOrder = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.odoref.initialize();
            this.outgoing.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.odoref.Tab);
            this.centerPanel.add(this.outgoing.Tab);
            this.centerPanel.setActiveTab(this.odoref.Tab);
            this.centerPanel.endUpdate();
            saa.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
saa.inventory.outgoingOrder.odoref= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.WarehouseCDS = saa.core.newDataStore(
                '/company/9/9', false,
                {   s:"init", limit:this.page_limit, start:this.page_start,
                    t:"COCO"
                }
            ); 
            this.WarehouseCDS.load();
            this.SiteDeptCDS = saa.core.newDataStore(
                '/company/6/9', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SiteDeptCDS.load();
            this.Columns = [
                {   header: "Company", width : 100,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"Company ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.company_name;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Order.No", width : 100,
                    dataIndex : 'order_no', sortable: true,
                    tooltip:"Order No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.order_date;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Buyer", width : 200,
                    dataIndex : 'buyer_name', sortable: true,
                    tooltip:"Buyer Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.buyer_id;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Item Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.item_id;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Remain.Qty", width : 100,
                    dataIndex : 'remain_qty', sortable: true,
                    tooltip:"Remain Quantity",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == 0 )
                        {   metaData.attr = "style = background-color:red;";  };
                        result = value+'<br>'+record.data.um_primary;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Stocks.Qty", width : 100,
                    dataIndex : 'stock_qty', sortable: true,
                    tooltip:"Remain Quantity",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == 0 )
                        {   metaData.attr = "style = background-color:red;";  };
                        result = value+'<br>'+record.data.um_primary;
                        return saa.core.gridColumnWrap(result);
                    }
                }
            ];
            this.Searchs = [
                {   id: 'odoref_order_no',
                    cid: 'order_no',
                    fieldLabel: 'Order.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'odoref_buyer_name',
                    cid: 'buyer_name',
                    fieldLabel: 'Buyer',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'odoref_company_id',
                    cid: 'company_id',
                    fieldLabel: 'Company',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120,
                    hidden : true
                },
                {   id: 'odoref_site_id',
                    cid: 'site_id',
                    fieldLabel: 'Site',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120,
                    hidden : true
                },
                {   id: 'odoref_warehouse_id',
                    cid: 'warehouse_id',
                    fieldLabel: 'Warehouse.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120,
                    hidden : true
                },
                new Ext.form.ComboBox(
                    {   store: this.WarehouseCDS,
                        typeAhead: true,
                        id: 'odoref_warehouse_name',
                        width: 120,
                        fieldLabel: 'Warehouse',
                        displayField: 'name',
                        valueField: 'name',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        listeners : { scope : this,
                            'select' : function (a,records,c)
                            {   Ext.getCmp("odoref_company_id").setValue(records.data.company_id);
                                Ext.getCmp("odoref_site_id").setValue(records.data.site_id);
                                Ext.getCmp("odoref_warehouse_id").setValue(records.data.warehouse_id);
                            }
                        }
                    }),
                new Ext.form.ComboBox(
                    {   store: this.SiteDeptCDS,
                        typeAhead: true,
                        id: 'odoref_sitedept_name',
                        width: 120,
                        fieldLabel: 'Site.Dept',
                        displayField: 'display',
                        valueField: 'dept_id',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                {   id: 'odoref_stocks',
                    cid: 'stocks',
                    fieldLabel: 'Stocks',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['YES'], ['NO']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'YES'
                },
            ];
            this.DataStore = saa.core.newDataStore(
                '/inventory/2/11', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : saa.inventory.outgoingOrder.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Orders',
                        tooltip:'New Outgoing Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    // {   text:'Department',
                    //     tooltip:'New Outgoing Record',
                    //     iconCls: 'silk-add',
                    //     handler : this.Grid_add,
                    //     scope : this
                    // },
                    '-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_odorefGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_odorefPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_odorefPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_odorefGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    //this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_poSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_odorefTab",
                jsId : tabId+"_odorefTab",
                title:  "Search.New.Outgoing",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 220,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 70,
                                defaultType: 'textfield',
                                items : this.Searchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   //id : 'search_btn_pritems',
                                    text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        Grid_add : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var the_warehouse = Ext.getCmp('odoref_warehouse_name').getValue();
                if ( the_warehouse )
                {   var out_ord_form = Ext.getCmp("out_ord_form");
                    if (out_ord_form)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Outgoing Form Available',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                    else
                    {   if (the_record.record.data.remain_qty == 0)
                        {   Ext.Msg.show(
                            {   title :'W A R N I N G ',
                                msg : 'Remain Quantity is not available',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                        };

                        if (the_record.record.data.stock_qty == 0)
                        {   Ext.Msg.show(
                            {   title :'W A R N I N G ',
                                msg : 'Stock Quantity is not available',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                        };

                        var centerPanel = Ext.getCmp('center_panel');
                        saa.inventory.outgoingOrder.order_forms.mode_view = false;
                        saa.inventory.outgoingOrder.order_forms.initialize(null, {
                            company_id     : Ext.getCmp("odoref_company_id").getValue(),
                            site_id        : Ext.getCmp("odoref_site_id").getValue(),
                            warehouse_id   : Ext.getCmp("odoref_warehouse_id").getValue(),
                            warehouse_name : Ext.getCmp("odoref_warehouse_name").getValue(),
                            ref_no         : the_record.record.data.order_no,
                            party_id       : the_record.record.data.buyer_id,
                            party_name     : the_record.record.data.buyer_name,
                            item_id        : the_record.record.data.item_id,
                            remain_qty     : the_record.record.data.remain_qty,
                            type           : button.type
                        });
                        centerPanel.beginUpdate();
                        centerPanel.add(saa.inventory.outgoingOrder.order_forms.Tab);
                        centerPanel.setActiveTab(saa.inventory.outgoingOrder.order_forms.Tab);
                        centerPanel.endUpdate();
                        saa.core.viewport.doLayout();
                    };
                }
                else
                {   Ext.Msg.show(
                    {   title:'E R R O R ',
                        msg: 'No Warehouse Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });

                };
            }
            else
            {   Ext.Msg.show(
                {   title:'E R R O R ',
                    msg: 'No Data Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            };
        },
        //  search button
        search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_odorefSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
saa.inventory.outgoingOrder.outgoing= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.WarehouseCDS = saa.core.newDataStore(
                '/company/9/9', false,
                {   s:"init", limit:this.page_limit, start:this.page_start,
                    t:"COCO"
                }
            ); 
            this.WarehouseCDS.load();
            this.Columns = [
                {   header: "Company", width : 150,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"Company ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.company_name;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Grin.No", width : 100,
                    dataIndex : 'grin_no', sortable: true,
                    tooltip:"Grin No",
                },
                {   header: "Grin.Date", width : 100,
                    dataIndex : 'grin_date', sortable: true,
                    tooltip:"Grin Date",
                    // renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Orders", width : 100,
                    dataIndex : 'ref_no', sortable: true,
                    tooltip:"Order Reference No",
                    // renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    // {   result = value+'<br>'+record.data.ref_type;
                    //     return saa.core.gridColumnWrap(result);
                    // }
                },
                {   header: "Buyers", width : 200,
                    dataIndex : 'party_name', sortable: true,
                    tooltip:"Buyer Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.party_id;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Grin Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:" Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                }
            ];
            this.Searchs = [
                {   id: 'odo_grin_no',
                    cid: 'grin_no',
                    fieldLabel: 'Grin.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'odo_ref_no',
                    cid: 'ref_no',
                    fieldLabel: 'Ref.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'odo_party_id',
                    cid: 'party_id',
                    fieldLabel: 'Party.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'odo_party_name',
                    cid: 'party_name',
                    fieldLabel: 'Party.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'odo_company_id',
                    cid: 'company_id',
                    fieldLabel: 'Company',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120,
                    hidden : true
                },
                {   id: 'odo_site_id',
                    cid: 'site_id',
                    fieldLabel: 'Site',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120,
                    hidden : true
                },
                {   id: 'odo_warehouse_id',
                    cid: 'warehouse_id',
                    fieldLabel: 'Warehouse.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120,
                    hidden : true
                },
                new Ext.form.ComboBox(
                    {   store: this.WarehouseCDS,
                        typeAhead: true,
                        id: 'odo_warehouse_name',
                        cid: 'warehouse_name',
                        width: 120,
                        fieldLabel: 'Warehouse',
                        displayField: 'name',
                        valueField: 'name',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        listeners : { scope : this,
                            'select' : function (a,records,c)
                            {   Ext.getCmp("odo_company_id").setValue(records.data.company_id);
                                Ext.getCmp("odo_site_id").setValue(records.data.site_id);
                                Ext.getCmp("odo_warehouse_id").setValue(records.data.warehouse_id);
                            }
                        }
                    }),
                
            ];
            this.DataStore = saa.core.newDataStore(
                '/inventory/2/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : saa.inventory.outgoingOrder.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'View',
                        tooltip:'View outgoingOrder',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_view,
                        scope : this
                    },
                    '-',
                    // {   text:'Delete',
                    //     tooltip:'Delete Record',
                    //     iconCls: 'silk-delete',
                    //     handler : this.Grid_remove,
                    //     scope : this
                    // },
                    // '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : this.print_out,
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_grinGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_grinPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_grinPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_grinGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    //this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_grinSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_grinTab",
                jsId : tabId+"_grinTab",
                title:  "Inventory Outgoing",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 220,
                    minSize: 220,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 70,
                                defaultType: 'textfield',
                                items : this.Searchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.grin_search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        Grid_view : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   centerPanel = Ext.getCmp('center-panel');
                var out_ord_form = Ext.getCmp("out_ord_form");
                if (out_ord_form)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Outgoing Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    // saa.inventory.outgoingOrder.order_forms.initialize(the_record.record);
                    saa.inventory.outgoingOrder.order_forms.mode_view = true;
                    saa.inventory.outgoingOrder.order_forms.initialize(the_record.record, {
                            company_id     : the_record.record.data.company,
                            site_id        : the_record.record.data.site_id,
                            warehouse_id   : the_record.record.data.warehouse_id,
                            warehouse_name : the_record.record.data.warehouse_name,
                            ref_no         : the_record.record.data.order_no,
                            party_id       : the_record.record.data.buyer_id,
                            party_name     : the_record.record.data.buyer_name,
                            item_id        : the_record.record.data.item_id,
                            remain_qty     : the_record.record.data.remain_qty,
                            type           : button.type
                        });
                    centerPanel.beginUpdate();
                    centerPanel.add(saa.inventory.outgoingOrder.order_forms.Tab);
                    centerPanel.setActiveTab(saa.inventory.outgoingOrder.order_forms.Tab);
                    centerPanel.endUpdate();
                    saa.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Data Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            };
        },
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            this.DataStore.removeAll();
            saa.core.submitGrid(
                this.DataStore, 
                '/inventory/2/2',
                {   'x-csrf-token': saa.inventory.outgoingOrder.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.id }
            );
        },
        //  search button
        grin_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_grinSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
        print_out: function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                saa.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        id : the_record.id });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };   
        },
    }; // end of public space
}(); // end of app
// create application
saa.inventory.outgoingOrder.order_forms= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;
    this.Headers;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit  : 75,
        page_start  : 0,
        mode_view   : false,
        seq_no      : 1,
        // public methods
        initialize: function(the_record, the_header)
        {   this.Records = the_record;
            this.Headers = the_header;
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.CurrencyDS = saa.core.newDataStore(
                '/country/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.PartyDS = saa.core.newDataStore(
                '/company/2/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.WarehouseDS = saa.core.newDataStore(
                '/company/9/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.ItemDS = saa.core.newDataStore(
                '/inventory/2/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   //id : 'out_form',
                title : 'H E A D E R',
                bodyStyle:'padding:5px',
                // autoScroll : true,
                // autoHeight : true,
                items: [
                {   layout:'column',
                    border:false,
                    items:[ // hidden columns
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'outordfrm_id',
                            xtype:'textfield',
                            fieldLabel: 'ID',
                            name: 'id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }, 
                        {   id : 'outordfrm_ref_type',
                            xtype:'textfield',
                            fieldLabel: 'Ref.Type',
                            name: 'ref_type',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'outordfrm_company_id',
                            xtype:'textfield',
                            fieldLabel: 'Company.ID',
                            name: 'company_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'outordfrm_party_id',
                            xtype:'textfield',
                            fieldLabel: 'Party.ID',
                            name: 'party_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'outordfrm_grin_status',
                            xtype:'textfield',
                            fieldLabel: 'Grin.Status',
                            name: 'grin_status',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'outordfrm_site_id',  
                            xtype:'textfield',
                            fieldLabel: 'Site.ID',
                            name: 'site_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }, 
                        {   id : 'outordfrm_currency_id', 
                            xtype:'textfield',
                            fieldLabel: 'Currency',
                            name: 'currency_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'outordfrm_warehouse_id',
                            xtype:'textfield',
                            fieldLabel: 'Warehouse.ID',
                            name: 'warehouse_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'outordfrm_item_id',
                            xtype:'textfield',
                            fieldLabel: 'Item.ID',
                            name: 'item_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'outordfrm_remain_qty',
                            xtype:'textfield',
                            fieldLabel: 'Remain.Qty',
                            name: 'remain_qty',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'outordfrm_created_date',  
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            hidden : true,
                        }]
                    }, 
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'outordfrm_grin_date', 
                            xtype:'datefield',
                            fieldLabel: 'Grin Date',
                            name: 'grin_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            value : new Date(),
                            readOnly : true,
                        },
                        {   id : 'outordfrm_ref_no', 
                            xtype:'textfield',
                            fieldLabel: 'REF.No',
                            name: 'ref_no',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                        },
                        {   id : 'outordfrm_grin_no', 
                            xtype:'textfield',
                            fieldLabel: 'Grin.No',
                            name: 'grin_no',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly: true,
                        },
                        {   id : 'outordfrm_delivery_no', 
                            xtype:'textfield',
                            fieldLabel: 'Delivery.No',
                            name: 'delivery_no',
                            anchor:'95%',
                            allowBlank: true,
                        },
                        {   id : 'outordfrm_delivery_date', 
                            xtype:'datefield',
                            fieldLabel: 'Delivery.Date',
                            name: 'delivery_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                        },
                        {   id : 'outordfrm_warehouse_name', 
                            xtype:'textfield',
                            fieldLabel: 'Warehouse',
                            name: 'warehouse_name',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true
                        },
                        ]
                    }, 
                    {   columnWidth:.7, 
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'outordfrm_company_name', 
                            xtype:'textfield',
                            fieldLabel: 'Company.Name',
                            name: 'company_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'outordfrm_party_name',
                            xtype:'combo',
                            fieldLabel: 'Party',
                            name: 'party_name',
                            anchor:'95%',
                            allowBlank: true,
                            store: this.PartyDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'name',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form", t:"SP",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('outordfrm_party_id').setValue(record.data.company_id);
                                    },
                            }
                        },
                        {   id : 'outordfrm_company_name', 
                            xtype:'textfield',
                            fieldLabel: 'Company.Name',
                            name: 'company_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'outordfrm_curier_no', 
                            xtype:'textfield',
                            fieldLabel: 'Ship/Vehicle.No',
                            name: 'curier_no',
                            anchor:'95%',
                            allowBlank: true,
                        },
                        {   id : 'outordfrm_curier_name', 
                            xtype:'textfield',
                            fieldLabel: 'Ship/Vehicle.Name',
                            name: 'curier_name',
                            anchor:'95%',
                            allowBlank: true,
                        },
                        {   id : 'outordfrm_curier_pic', 
                            xtype:'textfield',
                            fieldLabel: 'Currier.PIC',
                            name: 'curier_pic',
                            anchor:'95%',
                            allowBlank: true,
                        },
                        {   id : 'outordfrm_curier_phone_no', 
                            xtype:'textfield',
                            fieldLabel: 'Currier.Phone',
                            name: 'curier_phone_no',
                            anchor:'95%',
                            allowBlank: true,
                        }
                        ]
                    }]  
                },  // description area
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.9,
                        layout: 'form',
                        border:false,
                        items: [ 
                        {   id : 'outordfrm_description', 
                            xtype:'textarea',
                            fieldLabel: 'Description',
                            name: 'description',
                            anchor:'95%',
                            // allowBlank: false,
                            height : 100,
                        }]
                    }]
                }]
            });
            /************************************
                D E T A I L
            ************************************/
            this.Columns = [
                {   header: "NO of Bags", width : 50,
                    dataIndex : 'seq_no', sortable: true,
                    tooltip:"Sequence No",
                },
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Items",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.item_id+' ';
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Out.Remain", width : 80,
                    dataIndex : 'remain_qty', sortable: true,
                    tooltip:"Available.Quantity",
                    hidden : true
                },
                // {   header: "Stocks", width : 80,
                //     dataIndex : 'stock_qty', sortable: true,
                //     tooltip:"Available Stocks Quantity",
                // },
                {   header: "Groups", width : 80,
                    dataIndex : 'group_no', sortable: true,
                    tooltip:"Stocks Group No",
                },
                {   header: "Quantity", width : 80,
                    dataIndex : 'quantity_primary', sortable: true,
                    tooltip:"Quantity",
                    // css : "background-color: #DCFFDE;",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "UM", width : 100,
                    dataIndex : 'um_primary', sortable: true,
                    tooltip:"Unit Measurement",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Ref.No", width : 200,
                    dataIndex : 'stock_ref_no', sortable: true,
                    tooltip:"Stock Reference Number",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.stock_party_name+' ';
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Remark", width : 300,
                    dataIndex : 'remark', sortable: true,
                    tooltip:"Item Remark",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
            ];
            if (Ext.isObject(this.Records))
            {   the_title = "View Outgoing Order"; 
                the_save = '-';
                // load grin detail
                this.DataStore = saa.core.newDataStore(
                    '/inventory/2/5', false,
                    {   s:"form", grin_no : this.Records.data.grin_no,
                        limit:this.page_limit, start:this.page_start }
                );
                this.DataStore.load();    
            }
            else
            {   the_title = "New Outgoing Order"; 
                the_save = {   
                    text:'Save Outgoing',
                    tooltip:'Save Record',
                    iconCls: 'icon-save',
                    handler : this.Form_save,
                    scope : this
                };
                // load reference detail
                this.DataStore = saa.core.newDataStore(
                    '/inventory/2/12', false,
                    {   s:"form", ref_no: this.Headers.ref_no, stocks:"YES",
                        limit:this.page_limit, start:this.page_start }
                );
            };
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : 200, //saa.country.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   text:'Recalculate',
                        tooltip:'Recalculate Record',
                        iconCls: 'silk-calculator',
                        handler : this.Grid_recalculate,
                        scope : this
                    }
                ],
                listeners :
                {   //"beforeedit" : this.Grid_beforeedit,
                    "afteredit" : this.Grid_afteredit
                }
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
            /************************************
                S U M M A R Y 
            ************************************/
            this.summaryColumns = [
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Items",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.item_id+' ';
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Quantity", width : 100,
                    dataIndex : 'quantity_primary', sortable: true,
                    tooltip:"Quantity",
                },
                {   header: "UM", width : 100,
                    dataIndex : 'um_primary', sortable: true,
                    tooltip:"Unit Measurement",
                },
            ];
            this.summaryRecords = Ext.data.Record.create(
            [   {name: 'item_id', type: 'string'},
                {name: 'item_name', type: 'string'},
                {name: 'quantity_primary', type: 'string'},
                {name: 'um_primary', type: 'string'},
            ]);
            this.summaryDataStore = saa.core.newDataStore(
                '/purchase/2/6', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.summaryGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.summaryDataStore,
                columns: this.summaryColumns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : 200, //saa.country.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [the_save],
                scope : this
            });
            this.summaryTab = new Ext.Panel(
            {   title:  "S U M M A R Y",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.summaryGrid]
                }],
            });
            /************************************
                SEARCH ITEM TAB
            ************************************/
            this.stocksColumns = [
                {   header: "Warehouse", width : 100,
                    dataIndex : 'warehouse_name', sortable: true,
                    tooltip:"Warehouse Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.company_id+'-'+record.data.site_id+'<br>'+value;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Supplier", width : 150,
                    dataIndex : 'party_name', sortable: true,
                    tooltip:"Supplier Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.party_id+' ';
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Ref.No", width : 100,
                    dataIndex : 'ref_no', sortable: true,
                    tooltip:"Reference",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.ref_type+' ';
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Items",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.item_id+' ';
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Groups", width : 80,
                    dataIndex : 'group_no', sortable: true,
                    tooltip:"Group No",
                    // renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    // {   result = value+'<br> '+record.data.ref_type+' ';
                    //     return saa.core.gridColumnWrap(result);
                    // }
                },
                {   header: "Quantity", width : 80,
                    dataIndex : 'available_qty', sortable: true,
                    tooltip:"Available Quantity Primary",

                },
                {   header: "UM", width : 50,
                    dataIndex : 'um_primary', sortable: true,
                    tooltip:"Unit Measurement",
                },
            ];
            this.stocksDataStore = saa.core.newDataStore(
                '/inventory/3/0', false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            // this.stocksDataStore.load();
            this.stocksSearchs = [
                {   id: 'stckord_warehouse_name',
                    cid: 'warehouse_name',
                    fieldLabel: 'Warehouse',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120,
                    readOnly : true
                },
                {   id: 'stckord_item_id',
                    cid: 'item_id',
                    fieldLabel: 'Item.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'stckord_item_name',
                    cid: 'item_name',
                    fieldLabel: 'Item.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.stocksGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.stocksDataStore,
                columns: this.stocksColumns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : 200, //saa.country.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add Items',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add_items,
                        scope : this
                    },
                ],
                scope : this
            });
            this.stocksTab = new Ext.Panel(
            {   title:  "F I N D  -  I T E M S",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 210,
                    minSize: 210,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 70,
                                defaultType: 'textfield',
                                items : this.stocksSearchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   //id : 'search_btn_stocks',
                                    text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.stocks_search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.stocksGrid]
                }],
            });
        },
        // build the layout
        build_layout: function()
        {   
            this.Tab = new Ext.Panel(
            {   id : "out_ord_form",
                jsId : "out_ord_form",
                // title:  the_title,
                title : '<span style="background-color: yellow;">'+the_title+'</span>',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region: 'center',     
                    xtype: 'tabpanel',
                    activeTab: 0,
                    items:[this.Form, this.detailTab, this.summaryTab, this.stocksTab]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   // check headers
            // console.log(this.Headers);
            v_ref_no = "";
            v_item_id = "";
            if (Ext.isObject(this.Headers))
            {   Ext.each(this.Headers,
                    function(the_header)
                    {   Ext.getCmp('outordfrm_company_id').setValue(the_header.company_id);
                        Ext.getCmp('outordfrm_company_name').setValue(the_header.company_name);
                        Ext.getCmp('outordfrm_grin_no').setValue(the_header.grin_no);
                        Ext.getCmp('outordfrm_ref_no').setValue(the_header.ref_no);
                        v_ref_no = the_header.ref_no;
                        Ext.getCmp('outordfrm_ref_type').setValue('O');
                        Ext.getCmp('outordfrm_party_id').setValue(the_header.party_id);
                        Ext.getCmp('outordfrm_party_name').setValue(the_header.party_name);
                        Ext.getCmp('outordfrm_delivery_no').setValue(the_header.party_delivery_no);
                        Ext.getCmp('outordfrm_delivery_date').setValue(
                            saa.core.shortdateRenderer(the_header.party_delivery_date,'Y-m-d', 'd/m/Y'));
                        Ext.getCmp('outordfrm_site_id').setValue(the_header.site_id);
                        Ext.getCmp('outordfrm_warehouse_id').setValue(the_header.warehouse_id);
                        Ext.getCmp('outordfrm_warehouse_name').setValue(the_header.warehouse_name);
                        Ext.getCmp('outordfrm_curier_no').setValue(the_header.curier_no);
                        Ext.getCmp('outordfrm_curier_name').setValue(the_header.curier_name);
                        Ext.getCmp('outordfrm_curier_pic').setValue(the_header.curier_pic);
                        Ext.getCmp('outordfrm_curier_phone_no').setValue(the_header.curier_phone_no);
                        Ext.getCmp('outordfrm_description').setValue(the_header.description);
                        Ext.getCmp('outordfrm_item_id').setValue(the_header.item_id);
                        v_item_id = the_header.item_id;
                        Ext.getCmp('outordfrm_remain_qty').setValue(the_header.remain_qty);
                        
                    }, this);
            };
            // check records
            if (Ext.isObject(this.Records))
            {   the_record = this.Records;
                Ext.each(the_record.fields.items,
                    function(the_field)
                    {   switch (the_field.name)
                        {   case "company_id" :
                            case "company_name" :
                            case "grin_no" :
                            case "ref_no" :
                            case "ref_type" :
                            case "party_id" :
                            case "party_name" :
                            case "site_id" :
                            case "warehouse_id" :
                            case "warehouse_name" :
                            case "curier_no" :
                            case "curier_name" :
                            case "curier_pic" :
                            case "curier_phone_no" :
                            case "description" :
                            {   Ext.getCmp('outordfrm_'+the_field.name).setValue(the_record.data[the_field.name]);
                            };
                            break;
                            case "party_delivery_no" :
                            {   Ext.getCmp('outordfrm_delivery_no').setValue(the_record.data[the_field.name]);
                            };
                            break;
                            case "party_delivery_date" :
                            {   Ext.getCmp('outordfrm_delivery_date').setValue(saa.core.shortdateRenderer(the_record.data[the_field.name],'Y-m-d', 'd/m/Y'));
                            };
                            break;
                        };
                    });
            };

            this.stocksDataStore.baseParams = {   
                s:"form",
                company_id : this.Headers.company_id,
                site_id : this.Headers.site_id,
                warehouse_id : this.Headers.warehouse_id,
                order_no : v_ref_no,
                item_id : v_item_id };
            // load data if its not view mode
            if (this.mode_view == true ){} else { this.stocksDataStore.load(); };

            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'grin_no', type: 'string'},
                {name: 'seq_no', type: 'string'},
                {name: 'item_id', type: 'string'},
                {name: 'item_name', type: 'string'},
                {name: 'available_qty', type: 'string'},
                {name: 'quantity_primary', type: 'string'},
                {name: 'um_primary', type: 'string'},
                {name: 'stock_ref_no', type: 'string'},
                {name: 'stock_ref_type', type: 'string'},
                {name: 'stock_grin_no', type: 'string'},
                {name: 'stock_party_name', type: 'string'},
                {name: 'remark', type: 'string'},
                {name: 'created_date', type: 'date'}
            ]);
        },
        // stocks item search button
        stocks_search_handler : function(button, event)
        {   the_parameter = saa.core.getSearchParameter(this.stocksSearchs);
            this.stocksDataStore.removeAll();
            this.stocksDataStore.baseParams = Ext.apply( the_parameter,
            {   s:"form",
                company_id : this.Headers.company_id,
                site_id : this.Headers.site_id,
                warehouse_id : this.Headers.warehouse_id,
                limit:this.page_limit, start:this.page_start });
            this.stocksDataStore.reload();
        },  
        Grid_add_items: function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.stocksGrid.getSelectionModel().selection;
            var is_inserted = false;
            var the_remain_qty = Ext.getCmp('outordfrm_remain_qty').getValue();
            // var the_seq_no = this.seq_no;
            if ( the_record )
            {   record = the_record.record.data;
                //get ajax for subdetail records
                //when they founds, then insert them all
                the_this = this;
                the_records = saa.core.jsonGet( 
                    '/inventory/2/18', 
                    {   'x-csrf-token': saa.inventory.outgoingOrder.sid }, 
                    {   s:"form", limit:this.page_limit, start:this.page_start,
                        grin_no : record.ref_no,
                        item_id : record.item_id,
                        group_no: record.group_no
                    }, 
                    function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   order_forms = saa.inventory.outgoingOrder.order_forms;
                            Ext.each(the_response.rows,
                            function(response)
                            {   
                                order_forms.Grid.store.insert( 0,
                                new order_forms.Records (
                                {   id              : "",
                                    grin_no         : "",
                                    seq_no          : response.seq_no,
                                    item_id         : response.item_id,
                                    item_name       : response.item_name,
                                    available_qty   : 0,
                                    group_no        : response.group_no,
                                    quantity_primary: response.quantity_primary,
                                    um_primary      : response.um_primary,
                                    stock_grin_no   : response.grin_no,
                                    stock_ref_no    : response.grin_no, //response.ref_no,
                                    stock_ref_type  : record.ref_type,
                                    stock_party_name: record.party_name,
                                    remain_qty      : the_remain_qty,
                                    remark          : "",
                                    created_date    : "",
                                    reject_pecah    : 0, // no reject for outgoingOrder
                                    reject_tunas    : 0, // no reject for outgoingOrder
                                    reject_muda     : 0, // no reject for outgoingOrder
                                    reject_retak    : 0, // no reject for outgoingOrder
                                    reject_basah    : 0, // no reject for outgoingOrder
                                    reject_busuk    : 0  // no reject for outgoingOrder
                                }));
                                // the_seq_no = the_seq_no + 1;
                                is_inserted = true;
                            });  
                        };
                    });                                
                // this.seq_no = the_seq_no;
                the_stocks = this.stocksGrid.getStore();
                the_stocks.removeAt(the_record.cell[0]);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_afteredit : function(the_cell)
        {   switch (the_cell.field)
            {   case "quantity_primary":
                {   var data = the_cell.record.data;
                    var value = parseInt(the_cell.value);
                    // var unit_price = parseInt(data.unit_price);
                    // var po_qty_primary = parseInt(data.po_qty_primary);
                    var quantity_primary = parseInt(data.quantity_primary);
                    var remain_qty = parseInt(data.remain_qty);
                    var stock_qty = parseInt(data.stock_qty);
                    if (quantity_primary > remain_qty)
                    {   Ext.Msg.show(
                        {   title:'W A R N I N G ',
                            msg: ' Outgoing.Qty is higher than Remain.Qty <BR>Do you want to process it ? ',
                            buttons: Ext.Msg.YESNO,
                            fn: function(buttonId, text)
                                {   if (buttonId =='yes')
                                    {   the_cell.grid.getView().refresh();
                                    } else 
                                    {   data.quantity_primary = 0;
                                        the_cell.grid.getView().refresh();
                                    };
                                },
                            icon: Ext.MessageBox.WARNING
                        });
                    };
                    if (quantity_primary > stock_qty)
                    {   Ext.Msg.show(
                        {   title:'E R R O R ',
                            msg: ' Outgoing.Qty is higher than Stock.Qty.',
                            buttons: Ext.Msg.OK,
                            fn: function(buttonId, text)
                                {   data.quantity_primary = 0;
                                    the_cell.grid.getView().refresh();
                                },
                            icon: Ext.MessageBox.WARNING
                        });
                    };
                };
                break;
            };
        },
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   
                if (Ext.isEmpty(the_record.record.data.id))
                {   the_store = this.Grid.getStore();
                    removed_record = the_record.cell[0];
                    the_modified = the_store.getModifiedRecords();
                    // delete the Grid_UI
                    the_store.removeAt(removed_record); 
                }
                else
                {   the_record.record.set("remark", "REMOVED"); };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_recalculate: function(button, event)
        {   this.Grid.stopEditing();
            var the_records = this.DataStore.data.items;
            var json_data = [];
            var v_json = {};
            var v_data = {};
            var is_new_item = false;
            Ext.each(the_records,
                function(the_record)
                {   is_new_item = false;
                    the_data = the_record.data;
                    if (json_data.length == 0)
                    {   is_new_item = true;
                        json_data.push({
                            id              : "",
                            grin_no         : "",
                            item_id         : the_data.item_id,
                            item_name       : the_data.item_name,
                            quantity_primary: parseInt(the_data.quantity_primary),
                            um_primary      : the_data.um_primary,
                            remark          : "",
                            created_date    : "",
                            unit_price      : 0,    // default 0, for outgoingOrder order
                            amount          : 0,    // default 0, for outgoingOrder order
                            po_qty_primary  : 0,    // default 0, for outgoingOrder order
                            pcs_subdtl      : 0,    // default 0, for outgoingOrder order  
                            has_subdetail   : 1,    // default 0, for outgoingOrder order
                        });
                    }
                    else
                    {   Ext.each(json_data,
                            function(v_json)
                            {   if (v_json.item_id == the_data.item_id)
                                {   v_json.quantity_primary = v_json.quantity_primary + parseInt(the_data.quantity_primary);
                                }
                                else // add new items
                                {   is_new_item = true; };
                            });
                        if( is_new_item )
                        {   json_data.push({
                                item_id: the_data.item_id,
                                item_name: the_data.item_name,
                                quantity_primary : parseInt(the_data.quantity_primary),
                                um_primary : the_data.um_primary,
                            });

                        };
                    };
                });
            the_summaryGrid = this.summaryGrid;
            the_summaryRecords = this.summaryRecords;
            if (json_data.length > 0)
            {   this.summaryDataStore.rejectChanges();
                this.summaryDataStore.removeAll();
                Ext.each(json_data,
                    function(v_json)
                    {   the_summaryGrid.store.insert( 0, new the_summaryRecords(v_json)); });
                this.isRecalculate = true;
            };
        },
        // order_forms grid save records
        Form_save : function(button, event)
        {   var head_data = [];
            var json_data = [];
            var modi_data = [];
            var v_json = {};
            var the_datastore = this.DataStore;
            if (saa.core.validateFields([
                'outordfrm_party_name', 'outordfrm_delivery_date']))
            {   // detail validation
                var item_count = this.DataStore.getCount()
                if ( item_count < 1 ) 
                {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'Please register minimum 1 Item.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO,
                        width : 300
                    });
                }
                else if ( saa.core.validateGridQuantity(
                            // this.DataStore.getModifiedRecords(),
                            the_datastore.data.items,
                            ['quantity_primary']))
                {   // header
                    head_data = saa.core.getHeadData([
                        'outordfrm_id', 'outordfrm_company_id', 'outordfrm_party_id', 
                        'outordfrm_grin_status','outordfrm_site_id', 'outordfrm_currency_id',
                        'outordfrm_warehouse_id', 'outordfrm_created_date', 
                        'outordfrm_grin_date', 'outordfrm_grin_no', 
                        'outordfrm_ref_no', 'outordfrm_ref_type', 
                        'outordfrm_delivery_date', 'outordfrm_company_name', 
                        'outordfrm_party_name', 'outordfrm_warehouse_name', 
                        'outordfrm_delivery_no', 'outordfrm_description',
                        'outordfrm_curier_no', 'outordfrm_curier_name', 
                        'outordfrm_curier_pic', 'outordfrm_curier_phone_no']);
                    // detail
                    json_data = saa.core.getDetailData(this.summaryDataStore.getModifiedRecords());
                    modi_data = saa.core.getDetailData(this.DataStore.getModifiedRecords());
                    
                    // submit data
                    saa.core.submitForm(
                        'out_ord_form', 
                        saa.inventory.outgoingOrder.odoref.DataStore,
                        '/inventory/2/1',
                        {   'x-csrf-token': saa.inventory.outgoingOrder.sid },
                        {   task : 'save', st: 'O',
                            head : Ext.encode(head_data),
                            json : Ext.encode(json_data),
                            modi : Ext.encode(modi_data)
                        });
                };
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(saa.inventory.outgoingOrder.initialize, saa.inventory.outgoingOrder);
// end of file
</script>
<div>&nbsp;</div>