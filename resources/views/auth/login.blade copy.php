@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-4">
            <div class="single-top-popular-course " >
                    {{-- <div class="single-top-popular-course d-flex align-items-center flex-wrap mb-30 wow fadeInUp" data-wow-delay="700ms"> --}}

{{-- batas form --}}
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <br>
                            <label for="email" class="col-md-8 control-label">NO. INDUK SISWA</label>
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-8 control-label">PASSWORD</label>
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <button type="submit" class="btn academy-btn btn-sm">Login</button>
                                <a class="btn academy-btn btn-sm" href="{{ route('password.request') }}">Forgot Your Password ?</a>
                            </div>
                        </div>
                        <br>
                    </form>
                    {{-- batas form --}}


                </div>
            </div>
        </div>
    </div>
    {{-- <div class="row"> --}}
        <div class="col-12">
            <div class="about-slides owl-carousel mt-100 wow fadeInUp" data-wow-delay="600ms">
                <img src="{{asset('/academy/img/bg-img/bg-3.jpg')}}" alt="">
                <img src="{{asset('/academy/img/bg-img/bg-2.jpg')}}" alt="">
                <img src="{{asset('/academy/img/bg-img/bg-1.jpg')}}" alt="">
            </div>
        </div>
    </div>
</div>    
@endsection
