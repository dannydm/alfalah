@extends('layouts_backend._main_backend')
@section('extra_styles')
<!-- Put your style in here -->
  <style type="text/css">
      html {
          overflow: hidden;
      }
  </style>
@endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" id="content-wrapper" style="min-height: 421px;">
    <!--Content TAB-->
    <div class="content-tabs">
        <button class="roll-nav roll-left tabLeft" onclick="scrollTabLeft()">
          <i class="fa fa-backward"></i>
        </button>
        <nav class="page-tabs menuTabs tab-ui-menu" id="tab-menu">
            <div class="page-tabs-content" style="margin-left: 0px;">

            </div>
        </nav>
        <button class="roll-nav roll-right tabRight" onclick="scrollTabRight()">
          <i class="fa fa-forward" style="margin-left: 3px;"></i>
        </button>
        <div class="btn-group roll-nav roll-right">
            <button class="dropdown tabClose" data-toggle="dropdown">
                Button<i class="fa fa-caret-down" style="padding-left: 3px;"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" style="min-width: 128px;">
                <li><a class="tabReload" href="javascript:refreshTab();">Tab Reload</a></li>
                <li><a class="tabCloseCurrent" href="javascript:closeCurrentTab(true);">Close Current</a></li>
                <li><a class="tabCloseAll" href="javascript:closeOtherTabs(true);">Close All</a></li>
                <li><a class="tabCloseOther" href="javascript:closeOtherTabs(true);">Close Other</a></li>
            </ul>
        </div>
        <button class="roll-nav roll-right fullscreen" onclick="App.handleFullScreen()">
          <i class="fa fa-arrows-alt"></i>
        </button>
    </div>
    <div class="content-iframe" style="background-color: #ffffff; ">
      <div class="tab-content" id="tab-content">
      </div>
    </div>
  </div>
@endsection


@section('extra_scripts')
<script>
  // define namespace
  var alfalah = {};

  alfalah.namespace = function() 
  {
    var ln = arguments.length, i, value, split, x, xln, parts, object;
    
    for (i = 0; i < ln; i++) 
    {
      value = arguments[i];
      parts = value.split(".");
      object = window[parts[0]] = Object(window[parts[0]]);
      
      for (x = 1, xln = parts.length; x < xln; x++) 
      {
        object = object[parts[x]] = Object(object[parts[x]]);
      };
    };
    return object;
  };
  alfalah.namespace('alfalah.core');
  alfalah.core = function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.image_url;
        this.init_mode;
        this.target;
        this.action;
        this.Grid;
        this.Columns;
        this.Records;
        this.DataStore;
        this.Searchs;
        this.menuPanel;
        this.viewport;

        // private functions
        //**********************
        // public space
        //**********************
        return {
            // execute at the very last time
            // public properties, e.g. strings to translate
            // public methods
            server_date: new Date().getFullYear(),
            start_date : new Date(this.server_date, 0, 1),
            end_date   : new Date(this.server_date, 11, 31),
            page_limit : 75,
            page_start : 0,
            sid        : '{{ csrf_token() }}',
            initialize: function()
            { //console.log('initialize core');
              this.prepare_component();
              this.build_layout();
              this.finalize_comp_and_layout();
            },
            prepare_component : function()
            { // load the menu
              this.ajax(
                "/dashboard/0/1",   //the_url, 
                {},                 //the_parameters, 
                "GET",              //the_type, 
                function(response)
                { //console.log(response);
                  alfalah.core.build_menu(response);
                  $('.sidebar-menu').sidebarMenu({data: response}); 
                },
                function(response)  //fn_fail, 
                { 
                  console.log("FAILED");
                  console.log(response);
                },
                null                //fn_always
              );

              App.setbasePath("/");
              App.setGlobalImgPath("dist/img/");
              
              addTabs({
                  id        : '10008',
                  title     : 'Dashboard',
                  close     : false,
                  url       : '/',
                  urlType   : "relative", //relative
                  targetTab : "#tab-content"
              });
            },
            build_layout: function()
            {
            },
            finalize_comp_and_layout : function()
            {

            },
            finalize_left_menu : function()
            { 
            },
            ajax : function(the_url, the_parameters, the_type, fn_success, fn_fail, fn_always)
            {
              $dataType = "json";
              $contentType = "application/x-www-form-urlencoded; charset=UTF-8";
              if (the_type == 'POST') 
              { $dataType = "text";
                // $contentType = "application/json; charset=utf-8"; 
              };
              $.ajax({
                // The URL for the request
                url: the_url,
                // The data parameter in array to send (will be converted to a query string)
                data: the_parameters,
                // Whether this is a POST or GET request
                type: the_type, 
                // The type of data we expect back, xml, json, script, text, html
                dataType : $dataType,
                contentType: $contentType,
              })
              // Code to run if the request succeeds (is done);
              // The response is passed to the function
              .done(fn_success)
              // Code to run if the request fails; the raw request and
              // status codes are passed to the function
              .fail(fn_fail)
              // Code to run regardless of success or failure;
              .always(fn_always);
            },
            build_menu : function(response)
            { 
              $.each(response, function()
              { 
                if (this.expanded)
                { // Administrator
                  this.icon = "fa fa-folder";
                  alfalah.core.build_menu(this.children);
                }
                else
                { 
                  this.url = this.link;
                  this.icon = "fa fa-circle-o";
                  this.targetType = "iframe-tab";
                  this.urlType = 'absolute'; //absolute (external link) or relative
                };
              });
            },
            grid_layout : function(target_div)
            { a = $('#'+target_div);
              var pstyle = 'border: 1px solid #dfdfdf; padding: 5px;';
              $('#'+target_div).w2layout({
                  name: target_div,
                  panels: [
                      { type: 'main', style: pstyle, content: 'main' },
                      { type: 'right', size: 200, resizable: true, hidden: false, style: pstyle, content: 'right' }
                  ]
              });
            },
            submitGrid : function( the_grid, the_url, the_sid)
            { // collect grid changes
              records = the_grid.getChanges();
              result = [];

              $.each(a, function(index, value) { console.log(value); value.click });

              $.each(records, function( index, value ) {
                record = the_grid.get(value.recid);
                $.each(value, function(index, value){
                  record[index] = value;
                });
                record.w2ui = null;
                result.push(record);  
              }); 
              // send grid changes to server
              this.ajax(
                the_url,                          //the_url, 
                {                                 //the_parameters,
                  '_token' : the_sid,
                  json : JSON.stringify(result)
                },                  
                "POST",                           //the_type, 
                function(response)                //fn_success
                { console.log("SUCCES");
                  console.log(response);
                  the_grid.reload(); 
                },
                function(response)                //fn_fail, 
                { 
                  console.log("FAILED");
                  console.log(response);
                },
                null                              //fn_always
              );
            },
            getSearchParameter : function(search_items)
            {   var the_parameter = {};
                console.log('getSearchParameter');
                console.log(search_items);
                search_items.each(
                  function() 
                  { v_param = {};
                    v_param[ this.name ] = $(this).val();
                    console.log('the_parameter');
                    console.log(the_parameter);
                    console.log('v_param extend');
                    console.log(v_param);
                    the_parameter = $.extend( the_parameter, v_param);
                  });
                console.log('the_parameter');
                console.log(the_parameter);
                return the_parameter;
            },
            printOut: function(print_type, the_url, the_parameter)
            { console.log("printOut");
              document.location.href = the_url+'?'+$.param(the_parameter)+'&pt='+print_type;
            },
        };
    }();

  $(document).ready(alfalah.core.initialize());
</script>
@endsection