<div class="container">
    <div class="banner-info wow bounceInDown" data-wow-duration="1s" data-wow-delay="0s">
        <div class="callbacks_container">
            <ul class="rslides callbacks callbacks1" id="slider3">
                <li id="callbacks1_s0" class="callbacks1_on" style="display: block; float: left; position: relative; opacity: 1; z-index: 2; transition: opacity 500ms ease-in-out 0s;">
                    <div class="banner-text">
                        <h3>The best place for </h3>
                        <h4>education</h4>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem ab illo.</p>
                        <div class="more-button">
        <a href="#" data-toggle="modal" data-target="#myModal1">Read More</a>
    </div>
                    </div>
                </li>
                <li id="callbacks1_s1" class="" style="display: block; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;">
                    <div class="banner-text">
                        <h3>Better education for a</h3>
                        <h4>Better world</h4>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem ab illo.</p>
                        <div class="more-button">
        <a href="#" data-toggle="modal" data-target="#myModal1">Read More</a>
    </div>
                    </div>
                </li>
                <li id="callbacks1_s2" class="" style="display: block; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;">
                    <div class="banner-text">
                        <h3>Tips to succeed in</h3>
                        <h4>education</h4>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem ab illo.</p>
                        <div class="more-button">
        <a href="#" data-toggle="modal" data-target="#myModal1">Read More</a>
    </div>
                    </div>
                </li>
                <li id="callbacks1_s3" class="" style="display: block; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;">
                    <div class="banner-text">
                        <h3>Better education for a</h3>
                        <h4>Better world</h4>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem ab illo.</p>
                        <div class="more-button">
        <a href="#" data-toggle="modal" data-target="#myModal1">Read More</a>
    </div>
                    </div>
                </li>
            </ul><a href="#" class="callbacks_nav callbacks1_nav prev">Previous</a><a href="#" class="callbacks_nav callbacks1_nav next">Next</a><ul class="callbacks_tabs callbacks1_tabs"><li class="callbacks1_s1 callbacks_here"><a href="#" class="callbacks1_s1">1</a></li><li class="callbacks1_s2"><a href="#" class="callbacks1_s2">2</a></li><li class="callbacks1_s3"><a href="#" class="callbacks1_s3">3</a></li><li class="callbacks1_s4"><a href="#" class="callbacks1_s4">4</a></li></ul>
        </div>	
    </div>
    
    </div>