<script type="text/javascript">
// create namespace
Ext.namespace('saa.assetMaster');

// create application
saa.assetMaster= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : '',
        act : '',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.docCode.initialize();
            this.mcType.initialize();
            this.mcBrand.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.docCode.Tab);
            this.centerPanel.add(this.mcType.Tab);
            this.centerPanel.add(this.mcBrand.Tab);
            this.centerPanel.setActiveTab(this.docCode.Tab);
            this.centerPanel.endUpdate();
            saa.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },

    }; // end of public space
}(); // end of app
// create application
saa.assetMaster.docCode= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.SiteDeptDS = saa.core.newDataStore(
                '/asset/0/11', true,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );

            this.Columns = [
                {   header: "Doc.Code", width : 130,
                    dataIndex : 'doc_code', sortable: true,
                    tooltip:"Doc.Code",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Site", width : 100,
                    dataIndex : 'site_id', sortable: true,
                    tooltip:"Site"
                },
                {   header: "Dept.", width : 120,
                    dataIndex : 'dept_id', sortable: true,
                    tooltip:"Dept.Name",
                    editor: new Ext.form.ComboBox(
                    {   store: this.SiteDeptDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'dept_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.company_id = record.data.company_id;
                                    combo.gridEditor.record.data.site_id = record.data.site_id;
                                    combo.gridEditor.record.data.dept_id = record.data.dept_id;
                                    combo.gridEditor.record.data.dept_name = record.data.dept_name;
                                    saa.assetMaster.docCode.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.dept_name+'<br>'+value;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Name", width : 200,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Doc Code Description",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return saa.core.gridColumnWrap(value);  }
                },
                {   header: "Status", width : 80,
                    dataIndex : 'status_name', sortable: true,
                    tooltip:"Remark",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['type'],
                            data : [ ['Active'], ['Inactive'] ]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'type',
                        valueField: 'type',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == 'Active'){} else
                        {   metaData.attr = "style = background-color:red;"; };
                        return value;
                    },
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Menu Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'doc_code', type: 'string'},
                {name: 'company_id', type: 'string'},
                {name: 'site_id', type: 'string'},
                {name: 'dept_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'created_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'doc_code',
                    cid: 'doc_code',
                    fieldLabel: 'Doc.Code',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'site_id',
                    cid: 'site_id',
                    fieldLabel: 'Site.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'dc_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_dcSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.reg_search_handler,
                scope : this
            });
            this.DataStore = saa.core.newDataStore(
                '/asset/10/0', false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                // columns : this.Columns,
                colModel: new Ext.ux.grid.LockingColumnModel(this.Columns),
                view: new Ext.ux.grid.LockingGridView(),
                stripeRows : true,
                columnLines: true,
                enableColLock: false,
                loadMask: true,
                height : saa.assetMaster.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Copy',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.Grid_copy,
                        scope : this
                    },
                    //{   text:'Edit',
                    //    tooltip:'Edit Record',
                    //    iconCls: 'silk-page-white-edit',
                    //    //handler : this.menu_grid_
                    //},
                    {   id : tabId+'_dcSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_dcGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_dcPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_dcPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_dcGridBBar').getPageData().activePage -1) * page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_dcSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_dcTab",
                jsId : tabId+"_dcTab",
                title:  "Doc.Code",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 70,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }
                    ]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // menu grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   doc_code: "New Code",
                    company_id: "SAA",
                    site_id: "PRB",
                    dept_id: "New Dept",
                    name: "New Name",
                    status: 0,
                    created_date: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 0);
        },
        // menu grid copy and make it a new record
        Grid_copy: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection)
            {   var data = this.Grid.getSelectionModel().selection.record.data;
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   id: "",
                        parent_menu_id: data.parent_menu_id,
                        name: data.name,
                        link: data.link,
                        status: data.status,
                        level_degree: data.level_degree,
                        arrange_no: data.arange_no
                    }));
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },
        // menu grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   //console.log("Data changes, Do you want to save it before reloads ? ") ;
                var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        Ext.each(this.DataStore.reader.meta.fields,
                            function(the_field)
                            {   v_data = {};
                                var the_data = the_record.data;
                                if (Ext.isDate( the_data[the_field["name"]] ) )
                                {   v_data[ the_field["name"] ] = the_data[the_field["name"]].format("d/m/Y");  }
                                else
                                {   v_data[ the_field["name"]] = the_data[the_field["name"]]; };
                                v_json= Ext.apply( v_json, v_data);
                            });
                        json_data.push(v_json);
                    }, this);
                Ext.Ajax.request(
                {   method: 'POST',
                    url: '/asset/10/1',
                    headers:
                    {   'x-csrf-token': saa.assetMaster.sid },
                    params:
                    {   json: Ext.encode(json_data),
                        btn : button.text
                    },
                    success : function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : the_response.message+'\n Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   this.DataStore.reload();    };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    scope: this
                });
            };
        },
        // menu grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: '/asset/10/2',
                headers:
                {   'x-csrf-token': saa.assetMaster.sid },
                params:
                {   s:"form",
                    id: this.Grid.getSelectionModel().selection.record.data.id,
                    cid: this.Grid.getSelectionModel().selection.record.data.company_id,
                    sid: this.Grid.getSelectionModel().selection.record.data.site_id,
                    dc: this.Grid.getSelectionModel().selection.record.data.doc_code
                },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Server Message : '+'\n'+the_response.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
        // menu search button
        reg_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_dcSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {
                the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        task: saa.assetMaster.task,
                        act: saa.assetMaster.act,
                        a:1, b:0,
                        limit:this.page_limit, start:this.page_start
                });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
saa.assetMaster.mcType = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.GridCombo;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;
    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = saa.assetMaster.centerPanel.container.dom.clientHeight;
            this.Columns = [
                {   header: "ID", width : 80,
                    dataIndex : 'mactype_id', sortable: true,
                    tooltip:"Machine Type ID",
                    // css : "background-color: #56FF00;",
                    editor : new Ext.form.TextField({allowBlank: false})
                },
                {   header: "Name", width : 250,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Machine Type Name",
                    editor : new Ext.form.TextField({allowBlank: false})
                },
                {   header: "Status", width : 80,
                    dataIndex : 'status_name', sortable: true,
                    tooltip:"Remark",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['type'],
                            data : [ ['ACTIVE'], ['INACTIVE'] ]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'type',
                        valueField: 'type',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == 'Active'){} else
                        {   metaData.attr = "style = background-color:red;"; };
                        return value;
                    },
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Object Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'mactype_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'status_name', type: 'string'},
                {name: 'created_date', type: 'date'}
            ]);
            this.Searchs = [
                {   id: 'mcType_id',
                    cid : 'id',
                    fieldLabel: 'Mc.Typ.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'mcType_name',
                    cid:'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'mcType_status',
                    cid : 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['Active'], ['Inactive'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'ALL'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_mcTypeSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.mcType_search_handler,
                scope : this
            });
            /**
             * MAIN-GRID
            */
            this.DataStore = saa.core.newDataStore(
                '/asset/11/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                colModel: new Ext.ux.grid.LockingColumnModel(this.Columns),
                view: new Ext.ux.grid.LockingGridView(),
                stripeRows : true,
                columnLines: true,
                loadMask: true,
                height : (this.region_height)-50,
                anchor: '100%',
                autoWidth  : true,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   id : tabId+'_mcTypeSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    {   text:'Archive',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_mcTypeGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_mcTypePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_mcTypePageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_mcTypeGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_mcTypeTab",
                jsId : tabId+"_mcTypeTab",
                title:  "Machine Type",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'ToolBox',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //{   region: 'west',
                //    title: 'Roles',
                //    split: true,
                //    width: 200,
                //    minSize: 175,
                //    maxSize: 400,
                //    collapsible: true,
                //    margins: '0 0 0 5',
                //    items:[this.EastGrid]
                //},

                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // mcType grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   mactype_id: "New ID",
                    name: "New Name",
                    status: 0,
                    status_name: "Active",
                    created_date: ""
                }));
            this.Grid.startEditing(0, 0);
        },
        // mcType grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   //console.log("Data changes, Do you want to save it before reloads ? ") ;
                var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        // this.store is equal with saa.assetMaster.mcType.DataStore
                        Ext.each(the_record.fields.items, //this.DataStore.reader.meta.fields,
                            function(the_field)
                            {   v_data = {};
                                var the_data = the_record.data;
                                if (Ext.isDate( the_data[the_field["name"]] ) )
                                {   v_data[ the_field["name"] ] = the_data[the_field["name"]].format("d/m/Y");  }
                                else
                                {   v_data[ the_field["name"]] = the_data[the_field["name"]]; };
                                v_json= Ext.apply( v_json, v_data);
                            });
                        json_data.push(v_json);
                    }, this);
                Ext.Ajax.request(
                {   method: 'POST',
                    url: '/asset/11/1',
                    headers:
                    {   'x-csrf-token': saa.assetMaster.sid },
                    params:
                    {   s:"form",
                        json: Ext.encode(json_data),
                        btn : button.text
                    },
                    success : function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : the_response.message+'\n Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   this.DataStore.reload();    };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    scope: this
                });
            };
        },
        // mcType grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: '/asset/11/2',
                headers:
                {   'x-csrf-token': saa.assetMaster.sid },
                params:
                {   s: "form",
                    id: this.Grid.getSelectionModel().selection.record.data.mactype_id
                },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Server Message : '+'\n'+the_response.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
        // mcType search button
        mcType_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_mcTypeSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        task: saa.assetMaster.task,
                        act: saa.assetMaster.act,
                        a:2, b:0,
                        limit:this.page_limit, start:this.page_start
                });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
saa.assetMaster.mcBrand = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.GridCombo;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;
    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = saa.assetMaster.centerPanel.container.dom.clientHeight;
            this.Columns = [
                {   header: "ID", width : 80,
                    dataIndex : 'machbrand_id', sortable: true,
                    tooltip:"Machine Type ID",
                    // css : "background-color: #56FF00;",
                    editor : new Ext.form.TextField({allowBlank: false})
                },
                {   header: "Name", width : 250,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Machine Type Name",
                    editor : new Ext.form.TextField({allowBlank: false})
                },
                {   header: "Status", width : 80,
                    dataIndex : 'status_name', sortable: true,
                    tooltip:"Remark",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store : new Ext.data.SimpleStore(
                        {   fields: ['type'],
                            data : [ ['ACTIVE'], ['INACTIVE'] ]
                        }),
                        typeAhead: true,
                        width: 250,
                        displayField: 'type',
                        valueField: 'type',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == 'Active'){} else
                        {   metaData.attr = "style = background-color:red;"; };
                        return value;
                    },
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Object Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'machbrand_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'status_name', type: 'string'},
                {name: 'created_date', type: 'date'}
            ]);
            this.Searchs = [
                {   id: 'mcBrand_id',
                    cid : 'id',
                    fieldLabel: 'Mc.Brd.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'mcBrand_name',
                    cid:'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'mcBrand_status',
                    cid : 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['Active'], ['Inactive'] ]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value : 'ALL'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_mcBrandSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.mcBrand_search_handler,
                scope : this
            });
            /**
             * MAIN-GRID
            */
            this.DataStore = saa.core.newDataStore(
                '/asset/12/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                colModel: new Ext.ux.grid.LockingColumnModel(this.Columns),
                view: new Ext.ux.grid.LockingGridView(),
                stripeRows : true,
                columnLines: true,
                loadMask: true,
                height : (this.region_height)-50,
                anchor: '100%',
                autoWidth  : true,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   id : tabId+'_mcBrandSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    {   text:'Archive',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_mcBrandGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_mcBrandPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_mcBrandPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_mcBrandGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_mcBrandTab",
                jsId : tabId+"_mcBrandTab",
                title:  "Machine Brand",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'ToolBox',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //{   region: 'west',
                //    title: 'Roles',
                //    split: true,
                //    width: 200,
                //    minSize: 175,
                //    maxSize: 400,
                //    collapsible: true,
                //    margins: '0 0 0 5',
                //    items:[this.EastGrid]
                //},

                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // mcBrand grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   machbrand_id: "New ID",
                    name: "New Name",
                    status: 0,
                    status_name: "Active",
                    created_date: ""
                }));
            this.Grid.startEditing(0, 0);
        },
        // mcBrand grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   //console.log("Data changes, Do you want to save it before reloads ? ") ;
                var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        // this.store is equal with saa.assetMaster.mcBrand.DataStore
                        Ext.each(the_record.fields.items, //this.DataStore.reader.meta.fields,
                            function(the_field)
                            {   v_data = {};
                                var the_data = the_record.data;
                                if (Ext.isDate( the_data[the_field["name"]] ) )
                                {   v_data[ the_field["name"] ] = the_data[the_field["name"]].format("d/m/Y");  }
                                else
                                {   v_data[ the_field["name"]] = the_data[the_field["name"]]; };
                                v_json= Ext.apply( v_json, v_data);
                            });
                        json_data.push(v_json);
                    }, this);
                Ext.Ajax.request(
                {   method: 'POST',
                    url: '/asset/12/1',
                    headers:
                    {   'x-csrf-token': saa.assetMaster.sid },
                    params:
                    {   s:"form",
                        json: Ext.encode(json_data),
                        btn : button.text
                    },
                    success : function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : the_response.message+'\n Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   this.DataStore.reload();    };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    scope: this
                });
            };
        },
        // mcBrand grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: '/asset/12/2',
                headers:
                {   'x-csrf-token': saa.assetMaster.sid },
                params:
                {   s: "form",
                    id: this.Grid.getSelectionModel().selection.record.data.machbrand_id
                },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Server Message : '+'\n'+the_response.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
        // mcBrand search button
        mcBrand_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_mcBrandSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        task: saa.assetMaster.task,
                        act: saa.assetMaster.act,
                        a:2, b:0,
                        limit:this.page_limit, start:this.page_start
                });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(saa.assetMaster.initialize, saa.assetMaster);
// end of file
</script>
<div>&nbsp;</div>