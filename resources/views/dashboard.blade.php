<div id="container">
<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.dashboard');

// create application
alfalah.dashboard = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.o_pass = '';
    this.n_pass = '';
    this.MyPr_Grid;
    this.MyPr_Columns;
    this.MyPr_Records;
    this.MyPr_DataStore;

    this.AngGridPanel;
    this.Ang_DataStore;
    this.anggaranTab;
    // private functions

    // public space
    return {
        // execute at the very last time
        //public variable
        refresh_time : ((1000*60)*15), // 1000 = 1 second
        centerPanel : 0,
        page_limit : 75,
        page_start : 0,
        tabId : '{{ $TABID }}',
        task : '',
        act : '',
        // public methods
        initialize: function()
        {   Ext.chart.Chart.CHART_URL = "asset('/js/ext3/resources/charts.swf') }}";
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.tools = [
            {   id:'gear',
               handler: function()
                   {   Ext.Msg.alert('Message', 'The Settings tool was clicked.'); }
            },
            {   id:'close',
                handler: function(e, target, panel)
                    {   panel.ownerCt.remove(panel, true);  }
            }];

            this.centerPanel = Ext.getCmp(this.tabId);
            this.Ang_DataStore = alfalah.core.newDataStore(
                "{{ url('/monitoring/2/2') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start });
            this.Ang_DataStore.load();

            this.AngGridPanel = new Ext.grid.EditorGridPanel( 
            {  store : this.Ang_DataStore,
               columns : [            
                {   header: "Organisasi ", width : 150,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    hidden : true,
                    tooltip:"Jenjang/Bidang/Departemen",
                },
                {   header: "Urusan", width : 250,
                    dataIndex : 'urusan_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Usulan Thn ini", width : 110,
                    dataIndex : 'total_biaya', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Anggaran thn lalu", width : 110,
                    dataIndex : 'jumlahn', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya tahun lalu",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "% ", width : 100,
                    dataIndex : 'j_persen', sortable: true,
                    tooltip:"Persentase naik / turun",
                    // css : "style = content-align:right;",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        // metaData.attr="style = text-align:right;";
                        t_pengurang = ((record.data.total_biaya)-(record.data.jumlahn));
                        value = (t_pengurang/(record.data.jumlahn)) * 100;
                        if ((value > 0.00) && (value < 100.00)) 
                            { metaData.attr = "style = background-color:cyan;background-image:none;"; }
                        else if (value < 0.00)
                        { metaData.attr = "style = background-color:lime;background-image:none;"; }
                        else {  metaData.attr = "style = background-color:red;background-image:none;"; };
                        return Ext.util.Format.number(value, '000.00');

                    },
                },
                {   header: "% serapan ", width : 100,
                    dataIndex : 's_persen', sortable: true,
                    tooltip:"presentase serapan anggaran",
                    // css : "style = content-align:right;",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        // metaData.attr="style = text-align:right;";
                        s_pengurang = ((record.data.total_biaya)-(record.data.total_uangmukanota));
                        value = (s_pengurang/(record.data.total_uangmukanota)) * 100;
                        if ((value > 0.00) && (value < 100.00)) 
                            { metaData.attr = "style = background-color:cyan;background-image:none;"; }
                        else if (value < 0.00)
                        { metaData.attr = "style = background-color:lime;background-image:none;"; }
                        else {  metaData.attr = "style = background-color:red;background-image:none;"; };
                        return Ext.util.Format.number(value, '000.00');

                    },
                },
                ],
                tbar: [
                    {   text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : this.AngGridPanel_refresh,
                        scope : this
                    },
                ]
            });         

            this.receive_DataStore = alfalah.core.newDataStore(
                "{{ url('/monitoring/1/11') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start });
            this.receive_DataStore.load();

            this.receive_GridPanel = new Ext.grid.EditorGridPanel( 
            {  store : this.receive_DataStore,
                columns: [ 
                    {   header: "Urusan", width : 280,
                        dataIndex : 'urusan_mrapbs_id_name',
                        sortable: true,
                        tooltip:"Jenjang/Bidang/Departemen",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   result = record.data.urusan_mrapbs_id_name;
                            return alfalah.core.gridColumnWrap(result);
                        }
                    },
                    {   header: "Jumlah Pengajuan Biaya", width : 150,
                        // hidden:true,
                        dataIndex : 'total_ajuan', sortable: true,
                        tooltip:"Jumlah total biaya",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                            return Ext.util.Format.number(value, '0,000');
                        },
                    },
                    {   header: "Jumlah Pengajuan Uangmuka", width : 170,
                        // hidden:true,
                        dataIndex : 'total_uangmuka', sortable: true,
                        tooltip:"Jumlah total biaya",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                            return Ext.util.Format.number(value, '0,000');
                        },
                    },
                    {   id : 'status_pencairan_id',
                        header: "Status Pencairan", width : 200,
                        dataIndex : 'approve_status',
                        hidden : true,
                        sortable: true,
                        tooltip:"Status Pencairan anggaran",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   //
                            if ( record.data.approve_status_3 == 0 ) { value = 'WAITING APPROVAL'; }
                            // not yet approve
                            else if ( record.data.approve_status_3 == 1 ){ value = 'RECEIVED'; }
                            else { value = 'CEK MONITORING'; };
                            return value;
                        }
                    },
                ],
                tbar: [
                    {   text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : this.receive_GridPanel_refresh,
                        scope : this
                    },
                ]
            });         

        },

        // build the layout
        build_layout: function()
        { 
            this.Tab = new Ext.Panel(
            {   region: 'center',
                autoScroll  : true,
                // autoHeight : true,
                items: [
                    {   xtype:'portal',
                        region:'center',
                        margins:'1 1 1 1',
                        // autoHeight : true,
                        autoScroll  : true,
                        items:[
                            {  // columnWidth:.50,
                            style:'padding:10px 0 0px 5px',
                            items:[
                            {   title: 'Grafik Anggaran',
                                region : 'center', 
                                // tools: this.tools,
                                // html: " DO RE MI FA SOL",
                                //autoLoad:"{{ url('/news/1/10') }}",
                                // autoHeight : true,
                                autoScroll  : true,
                                width:1000,
                                height:380,
                                xtype : 'container',
                                    autoScroll  : true,
                                    layout : 'fit',
                                    items: [{
                                            xtype: 'container',
                                            contentEl: 'alfalahchart',
                                            
                                        },
                                       
                                    ]
                                },
                            ]
                        },
                        // {   columnWidth:.50,
                        //     style:'padding:10px 0 10px 5px',
                        //     items:[
                        //     {   title: 'P E N G U M U M A N',
                        //         region : 'center', 
                        //         // tools: this.tools,
                        //         // html: " DO RE MI FA SOL",
                        //         autoLoad:"{{ url('/news/1/10') }}",
                        //         // autoHeight : true,
                        //         autoScroll  : true,
                        //         width:50,
                        //         height:380,
                        //     },
                        //     ]
                        // },
                        {   columnWidth:.49,
                            style:'padding:5px 0 0px 5px ',  
                            items:[
                            {   title: 'REKAPITULASI USULAN ANGGARAN BERDASARKAN URUSAN',
                                region : 'east', 
                                autoScroll  : true,
                                width:50,
                                height:400,
                                items: [
                                    {   
                                        region: 'center',     // center region is required, no width/height specified
                                        // split:true,
                                        width: 610,
                                        height : 320,
                                        layout : 'fit',
                                        items : [this.AngGridPanel]
                                    }
                                ]}
                            ]
                        },
                        {   columnWidth:.49,
                            style:'padding:5px 0 0px 5px',
                            items:[
                                {   title: 'REKAPITULASI PENGAJUAN BERDASAR URUSAN',
                                    region : 'south', 
                                    autoScroll  : true,
                                    width:50,
                                    height:360,
                                    items: [
                                    {   
                                        region: 'center',     // center region is required, no width/height specified
                                        // split:true,
                                        width: 610,
                                        height : 320,
                                        layout : 'fit',
                                        items : [this.receive_GridPanel]
                                    }
                                ]                                    
                                },
                            ]
                        },
                        ]
                    },
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   this.centerPanel.add(this.Tab);
            alfalah.core.viewport.doLayout();
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.dashboard.initialize, alfalah.dashboard);
// end of file
</script>
<div>&nbsp;</div>
<body>
<div>   
 
<script>
    $(document).ready(function() {
        Highcharts.chart('urusanchart', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'URUSAN CHART COMPARISON' 
            },
            subtitle: {
                text: ''
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total Urusan'
                }
        
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.f}'
                    }
                }
            },
        
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}</b> total<br/>'
            },
        
            series: [
                {
                    name: "Urusan",
                    colorByPoint: true,
                    data: [
                        

                    ]
                }
            ],
        });
    });
</script>
</div>
    <div id = "alfalahchart"> 
        <div id ="urusanchart"></div>
    </div>
</body>
