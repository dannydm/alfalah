<script type="text/javascript">
// create namespace
Ext.namespace('saa.shipping.claim');

// create application
saa.shipping.claim = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // claimepare the comclaimnent before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.claim.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.claim.Tab);
            this.centerPanel.setActiveTab(this.claim.Tab);
            this.centerPanel.endUpdate();
            saa.core.viewport.doLayout();
        },
        // finalize the comclaimnent and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
saa.shipping.claim.claim= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // claimepare the comclaimnent before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Asset.ID", width : 100,
                    dataIndex : 'asset_id', sortable: true,
                    tooltip:"asset ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( Ext.isEmpty(record.data.repair_started) )
                        {   metaData.attr = "style = background-color:yellow;";  }
                        else if ( Ext.isEmpty(record.data.repair_ended) )
                        {   metaData.attr = "style = background-color:red;";  }
                        else {   metaData.attr = "style = background-color:lime;"; };
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Name", width : 200,
                    dataIndex : 'name', sortable: true,
                    tooltip:"claim Name",
                },
                {   header: "Broken", width : 120,
                    dataIndex : 'broken_date', sortable: true,
                    tooltip:"Broken Date",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> - <br> '+record.data.broken_time;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Reclaimrter", width : 100,
                    dataIndex : 'reclaimrter_name', sortable: true,
                    tooltip:"Reclaimrter",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> - <br> '+record.data.assigned_from_name;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Repair Start", width : 120,
                    dataIndex : 'repair_date', sortable: true,
                    tooltip:"Repair Started",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> - <br> '+record.data.repair_time;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Technician", width : 100,
                    dataIndex : 'technician_name', sortable: true,
                    tooltip:"Technician Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> - <br> '+record.data.assigned_to_name;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Repair End", width : 120,
                    dataIndex : 'finish_date', sortable: true,
                    tooltip:"Repair End",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> - <br> '+record.data.finish_time;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"claim Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_', sortable: true,
                    tooltip:"claim Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'claim_id', type: 'integer'},
                {name: 'name', type: 'string'},
                {name: 'capitol', type: 'string'},
                {name: 'currency_id', type: 'string'},
                {name: 'phone_area', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'claim_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'claim_capitol',
                    cid: 'capitol',
                    fieldLabel: 'Capitol',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'claim_currency',
                    cid: 'currency_id',
                    fieldLabel: 'Currency',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'claim_phone',
                    cid: 'phone_area',
                    fieldLabel: 'Phone',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'claim_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ [''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_claimSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.claim_search_handler,
                scope : this
            });
            this.DataStore = saa.core.newDataStore(
                '/asset/30/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : saa.shipping.claim.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    // {   text:'Copy',
                    //     tooltip:'Copy Record',
                    //     iconCls: 'silk-page-copy',
                    //     handler : this.Grid_copy,
                    //     scope : this
                    // },
                    {   text:'Edit',
                        tooltip:'Edit Record',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_edit,
                        scope : this
                    },
                    {   id : tabId+'_claimSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   claimint_type : "pdf",
                        text:'claimint PDF',
                        tooltip:'claimint to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ saa.core.claimintButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   claimint_type : "xls",
                        text:'claimint Excell',
                        tooltip:'claimint to Excell SclaimeadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.claimintButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_claimGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_claimPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_claimPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_claimGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_claimSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_claimTab",
                jsId : tabId+"_claimTab",
                title:  "Shippment Claims",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // claimsition for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the comclaimnent and layout drawing
        finalize_comp_and_layout: function()
        {},
        Grid_add : function(button, event)
        {   var form_downtime = Ext.getCmp("form_downtime");
            if (form_downtime)
            {   Ext.Msg.show(
                {   title :'E R R O R ',
                    msg : 'DownTime Form Available',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
            else
            {   saa.shipping.claim.forms.initialize(null);
                saa.shipping.claim.centerPanel.beginUpdate();
                saa.shipping.claim.centerPanel.add(saa.shipping.claim.forms.Tab);
                saa.shipping.claim.centerPanel.setActiveTab(saa.shipping.claim.forms.Tab);
                saa.shipping.claim.centerPanel.endUpdate();
                saa.core.viewport.doLayout();
            };
        },
        Grid_edit : function(button, event)
        {   console.log("Grid_edit");
            var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   centerPanel = Ext.getCmp('center-panel');
                var form_downtime = Ext.getCmp("form_downtime");
                if (form_downtime)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'DownTime Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   saa.shipping.claim.forms.initialize(the_record.record);
                    saa.shipping.claim.centerPanel.beginUpdate();
                    saa.shipping.claim.centerPanel.add(saa.shipping.claim.forms.Tab);
                    saa.shipping.claim.centerPanel.setActiveTab(saa.shipping.claim.forms.Tab);
                    saa.shipping.claim.centerPanel.endUpdate();
                    saa.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        // claim grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   //console.log("Data changes, Do you want to save it before reloads ? ") ;
                var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        // this.store is equal with saa.shipping.claim.claim.DataStore
                        Ext.each(this.DataStore.reader.meta.fields,
                            function(the_field)
                            {   v_data = {};
                                var the_data = the_record.data;
                                if (Ext.isDate( the_data[the_field["name"]] ) )
                                {   v_data[ the_field["name"] ] = the_data[the_field["name"]].format("d/m/Y");  }
                                else
                                {   v_data[ the_field["name"]] = the_data[the_field["name"]]; };
                                v_json= Ext.apply( v_json, v_data);
                            });
                        json_data.push(v_json);
                    }, this);
                Ext.Ajax.request(
                {   method: 'claimST',
                    url: '/asset/30/1',
                    headers:
                    {   'x-csrf-token': saa.shipping.claim.sid },
                    params  :
                    {   json: Ext.encode(json_data)
                    },
                    success : function(resclaimnse)
                    {   var the_resclaimnse = Ext.decode(resclaimnse.resclaimnseText);
                        if (the_resclaimnse.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_resclaimnse.message+'\n '+the_resclaimnse.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   this.DataStore.reload();    };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    scope: this
                });
            };
        },
        // claim grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'claimST',
                url: '/asset/30/2',
                headers:
                {   'x-csrf-token': saa.shipping.claim.sid },
                params  :
                {   id: this.Grid.getSelectionModel().selection.record.data.claim_id
                },
                success: function(resclaimnse)
                {   var the_resclaimnse = Ext.decode(resclaimnse.resclaimnseText);
                    if (the_resclaimnse.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Server Message : '+'\n'+the_resclaimnse.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
        // claim search button
        claim_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search claimocess ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_claimSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: saa.shipping.claim.task,
                        act: saa.shipping.claim.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
saa.shipping.claim.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        claimpSiteDepartment : '',
        // public methods
        initialize: function(the_record)
        {   this.Records = the_record;
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // claimepare the comclaimnent before layout drawing
        prepare_component: function()
        {   
            this.AssetDS = saa.core.newDataStore(
                '/asset/2/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.McDS = saa.core.newDataStore(
                '/asset/21/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.EmployeeDS = saa.core.newDataStore(
                '/employee/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.UserDS = saa.core.newDataStore(
                '/permission/2/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            
            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   bodyStyle:'padding:5px',
                // url: +"ajax/ajax.gea.php?task=save",
                tbar: [
                    {   text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        scope : this
                    }
                ],
                items: [
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'downtime_id',
                            xtype:'textfield',
                            fieldLabel: 'ID',
                            name: 'id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'downtime_created_date', 
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            hidden : true,
                        }]
                    },
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        // {   id : 'downtime_asset_id',
                        //     xtype:'textfield',
                        //     fieldLabel: 'Asset.ID',
                        //     name: 'asset_id',
                        //     anchor:'95%',
                        //     allowBlank: false,
                        // },

                        {   disabled : true,
                            id : 'downtime_asset_id',
                            xtype:'combo',
                            fieldLabel: 'Asset.ID',
                            name: 'asset_id',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.AssetDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'asset_id',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('downtime_asset_name').setValue(record.data.name);
                                    },
                            }
                        },
                        {   id : 'downtime_mach_regid',
                            xtype:'combo',
                            fieldLabel: 'Barcode.ID',
                            name: 'mach_regid',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.McDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'mach_regid',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('downtime_asset_id').setValue(record.data.asset_id);
                                        Ext.getCmp('downtime_asset_name').setValue(record.data.name);
                                        Ext.getCmp('downtime_mach_name').setValue(record.data.name);
                                    },
                            }
                        },
                        {   id : 'downtime_reclaimrter_id',
                            xtype:'combo',
                            fieldLabel: 'Reclaimrter.ID',
                            name: 'reclaimrter_id',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.EmployeeDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'emp_id',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('downtime_reclaimrter_name').setValue(record.data.name);
                                    },
                            }
                        },
                        {   id : 'downtime_technician_id',
                            xtype:'combo',
                            fieldLabel: 'Technician.ID',
                            name: 'technician_id',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.EmployeeDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'emp_id',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form", sec:"mechanic",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('downtime_technician_name').setValue(record.data.name);
                                    },
                            }
                        },
                        {   disabled : true,
                            id : 'downtime_assigned_from',
                            xtype:'combo',
                            fieldLabel: 'Assigned.From',
                            name: 'assigned_from',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.UserDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'user_id',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('downtime_from_name').setValue(record.data.username);
                                    },
                            }
                        },
                        {   disabled : true,
                            id : 'downtime_assigned_to',
                            xtype:'combo',
                            fieldLabel: 'Assigned.To',
                            name: 'assigned_to',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.UserDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'user_id',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('downtime_to_name').setValue(record.data.username);
                                    },
                            }
                        },]
                    },
                    {   columnWidth:.7,
                        layout: 'form',
                        border:false,
                        items: [
                        {   disabled : true,
                            id : 'downtime_name', 
                            xtype:'textfield',
                            fieldLabel: 'Asset.Name',
                            name: 'asset_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'downtime_mach_name', 
                            xtype:'textfield',
                            fieldLabel: 'Barcode.Name',
                            name: 'mach_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'downtime_reclaimrter_name', 
                            xtype:'textfield',
                            fieldLabel: 'Reclaimrter.Name',
                            name: 'reclaimrter_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'downtime_technician_name', 
                            xtype:'textfield',
                            fieldLabel: 'Technician.Name',
                            name: 'technician_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   disabled : true,
                            id : 'downtime_from_name', 
                            xtype:'textfield',
                            fieldLabel: 'From.Name',
                            name: 'from_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   disabled : true,
                            id : 'downtime_to_name', 
                            xtype:'textfield',
                            fieldLabel: 'To.Name',
                            name: 'to_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },]
                    }]
                },  // broken
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'downtime_broken_date',
                            xtype:'datefield',
                            fieldLabel: 'Broken Date',
                            name: 'broken_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            value : new Date()
                        }]
                    },
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'downtime_broken_time',
                            xtype:'timefield',
                            fieldLabel: 'Broken Time',
                            name: 'broken_time',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'H:i',
                        }]
                    }]
                },  // description
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.8,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'downtime_broken_description', 
                            xtype:'textarea',
                            fieldLabel: 'Broken Desc.',
                            name: 'broken_description',
                            anchor:'95%',
                            allowBlank: false,
                            height : 100,
                        }]
                    }]
                },  // repair
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'downtime_repair_date',
                            xtype:'datefield',
                            fieldLabel: 'Repair Date',
                            name: 'repair_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                        }]
                    },
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'downtime_repair_time',
                            xtype:'timefield',
                            fieldLabel: 'Repair Time',
                            name: 'repair_time',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'H:i',
                        }]
                    }]
                },  // actions
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.8,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'downtime_repair_action',
                            xtype:'textarea',
                            fieldLabel: 'Actions',
                            name: 'repair_action',
                            anchor:'95%',
                            allowBlank: false,
                            height : 100,
                        }]
                    }]
                },  // finish
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'downtime_finish_date',
                            xtype:'datefield',
                            fieldLabel: 'Finish Date',
                            name: 'finish_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                        }]
                    },
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'downtime_finish_time',
                            xtype:'timefield',
                            fieldLabel: 'Finish Time',
                            name: 'finish_time',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'H:i',
                        }]
                    }]
                },]
            });
        },
        // build the layout
        build_layout: function()
        {   if (Ext.isObject(this.Records))
            {   the_title = "Edit claim"; }
            else { the_title = "New claim"; };

            this.Tab = new Ext.Panel(
            {   id : "downtime",
                jsId : "downtime",
                title:  the_title,
                region: 'center',
                layout: 'border',
                closable : true,
                //autoScroll  : true,
                items: [
                {   region: 'center',     
                    xtype: 'container',
                    items:[this.Form]
                }]
            });
        },
        // finalize the comclaimnent and layout drawing
        finalize_comp_and_layout: function()
        {   if (Ext.isObject(this.Records))
            {   Ext.each(this.Records.fields.items,
                    function(the_field)
                    {   switch (the_field.name)
                        {   case "id" :
                            case "created_date" :
                            case "asset_id" :
                            case "name" :
                            case "mach_regid" :
                            case "mach_name" :
                            case "reclaimrter_id" :
                            case "reclaimrter_name" :
                            case "technician_id" :
                            case "technician_name" :
                            case "assigned_from" :
                            case "from_name" :
                            case "assigned_to" :
                            case "to_name" :
                            case "broken_date" :
                            case "broken_time" :
                            case "broken_description" :
                            case "repair_date" :
                            case "repair_time" :
                            case "repair_action" :
                            case "finish_date" :
                            case "finish_time" :
                            {   console.log('downtime_'+the_field.name);
                                Ext.getCmp('downtime_'+the_field.name).setValue(this.Records.data[the_field.name]);
                            };
                            break;
                            case "asset_type" :
                            {   if (this.Records.data[the_field.name] == 'MAC')
                                {   Ext.getCmp('downtime_'+'mach_regid').setValue(this.Records.data['asset_id']);
                                    Ext.getCmp('downtime_'+'mach_name').setValue(this.Records.data['name']);
                                };
                                

                            };
                            break;
                        };
                    }, this);
                // this.DataStore.baseParams = { task:"searchDetail", limit:this.page_limit, gea_no:this.Records.data["gea_no"]};
                // this.DataStore.load();
            };
        },
        search_button: function(button, event)
        {   var the_search = true;
            if ( this.shipmentDataStore.getModifiedRecords().length > 0 )
            {};
            if (the_search == true) // no modification records flag then we can go to search
            {   this.shipmentDataStore.removeAll();
                the_parameter = saa.shipping.claim.get_parameter(this.shipmentSearchs);
                // this.page_limit = Ext.get("page_combo").getValue();
                this.shipmentDataStore.baseParams = Ext.apply( the_parameter, {task:"searchShipment", limit:this.page_limit}); // DO NOT PUT START PARAMETER FOR OUTO PAGING
                this.shipmentDataStore.load();
            };
        },
        shipmentGrid_add: function(button, event)
        {   var the_record = this.shipmentGrid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                this.Grid.stopEditing();
                if (this.DataStore.find("edi_id", the_record.edi_id) == -1)
                {   this.Grid.store.insert( 0,
                    new this.Records (
                    {   edi_id : the_record.edi_id,
                        order_no: the_record.order_no,
                        buyer_claim_number: the_record.buyer_claim_number,
                        xfty_date : the_record.xfty_date,
                        delivery_qty : the_record.balanced_quantity,
                        carton_qty : 0,
                        volume : 0,
                        status : the_record.status,
                        remarks : "",
                        created_date : ""
                    }));
                }
                else
                {   Ext.Msg.show(
                    {   title:'E R R O R ',
                        msg: 'Record Available ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 0);
        },
        // forms grid save records
        Form_save : function(button, event)
        {   var json_data = [];
            var v_json = {};
            // header
            Ext.each(['downtime_id', 'downtime_created_date',
                'downtime_asset_id', 'downtime_name',
                'downtime_mach_regid', 
                'downtime_reclaimrter_id', 'downtime_technician_id',
                'downtime_assigned_from', 'downtime_assigned_to',
                'downtime_broken_date', 'downtime_broken_time', 'downtime_broken_description',
                'downtime_repair_date', 'downtime_repair_time', 'downtime_repair_action',
                'downtime_finish_date', 'downtime_finish_time'
                ],
                function(the_field)
                {   a = Ext.getCmp(the_field);
                    if (Ext.isDate(a.getValue()) )
                    {   v_json[a.name] = a.getValue().format("d/m/Y");  }
                    else
                    {   v_json[a.name] = a.getValue();  };
                });
            json_data.push(v_json);
            Ext.Ajax.request(
                {   method: 'claimST',
                    url: '/asset/30/1',
                    headers:
                    {   'x-csrf-token': saa.shipping.claim.sid },
                    params:
                    {   task: 'save',
                        //site_id : Ext.get("hidden_site").getValue(),
                        json : Ext.encode(json_data)
                    },
                    success: function(resclaimnse)
                    {   var the_resclaimnse = Ext.decode(resclaimnse.resclaimnseText);
                        if (the_resclaimnse.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_resclaimnse.message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   Ext.getCmp("downtime").destroy();
                            saa.shipping.claim.claim.DataStore.reload();
                        };
                    },
                    failure: function()
                    {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    scope: this
                });
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(saa.shipping.claim.initialize, saa.shipping.claim);
// end of file
</script>
<div>&nbsp;</div>