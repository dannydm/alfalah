<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.pengesahan');

// create application
alfalah.pengesahan = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.pengesahan.initialize();
            this.budgeting.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.pengesahan.Tab);
            this.centerPanel.add(this.budgeting.Tab);
            this.centerPanel.setActiveTab(this.pengesahan.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.pengesahan.pengesahan= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   var cbSelModel = new Ext.grid.CheckboxSelectionModel();
            this.Columns = [ cbSelModel,
                {   header: "RAPBS.No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"RAPBS No",
                },
                {   header: "Organisasi / Urusan", width : 200,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 200,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Sumber Dana", width : 150,
                    dataIndex : 'sumberdana_id_name', sortable: true,
                    tooltip:"sumber pendanaan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Keluaran", width : 200,
                    dataIndex : 'keluaran', sortable: true,
                    tooltip:"keluaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Hasil", width : 200,
                    dataIndex : 'hasil', sortable: true,
                    tooltip:"hasil",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Sasaran", width : 200,
                    dataIndex : 'sasaran', sortable: true,
                    tooltip:"sasaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },

                {   header: "Coa", width : 150,
                    dataIndex : 'coa_id_name', sortable: true,
                    tooltip:"kode rekening",
                    hidden : true,
                },
                {   header: "Jumlah Biaya", width : 100,
                    id : 'total_biaya_urusan',
                    hidden:true,
                    dataIndex : 'total_biaya', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Tahun Lalu", width : 100,
                    dataIndex : 'jumlahn', sortable: true,
                    tooltip:"Jumlah Anggaran Tahun Sebelumnya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Tahun Ke N", width : 100,
                    dataIndex : 'jumlahke_n', sortable: true,
                    tooltip:"Jumlah Anggaran Sampai dengan Tahun Lalu",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   value = (record.data.total_biaya);
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "% perubahan", width : 100,
                    dataIndex : 'j_persen', sortable: true,
                    tooltip:"Persentase naik / turun",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        t_pengurang = ((record.data.total_biaya)-(record.data.jumlahn));
                        value = (t_pengurang/(record.data.jumlahn)) * 100;
                        
                        if (value < 100.00) 
                            { metaData.attr = "style = background-color:cyan; "; }
                        else {  metaData.attr = "style = background-color:red; "; };
                        return Ext.util.Format.number(value, '000.00');

                    },
                },

                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    hidden :true,
                    tooltip:" Status",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    hidden:true,
                    tooltip:"newrapbs Created Date",
                    css : "background-color: #ff80ff;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    hidden:true,
                    tooltip:"newrapbs Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                // {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'pengesahan_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengesahan_Urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengesahan_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengesahan_kegiatan',
                    cid: 'kegiatan_mrapbs_id_name',
                    fieldLabel: 'Kegiatan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengesahan_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'Sub.Keg',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengesahan_status',
                    cid: 'status',
                    hidden:true,
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },

            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_pengesahanSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.pengesahan_search_handler,
                scope : this
            });
            
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/pengesahan/1/0')}}"
                , false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.Grid = new Ext.grid.EditorGridPanel(
            { 
                store:  this.DataStore,
                columns: this.Columns,
                selModel: cbSelModel,
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.pengesahan.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    @if (array_key_exists('APPROVE KETUA', $MyTasks))
                    {   text:'Approve Ketua',
                        tooltip:'approval Usulan Anggaran',
                        iconCls: 'silk-tick',
                        handler : this.Grid_approve_ketua,
                        scope : this
                    },'-',
                    @endif
                    '_TOTAL_THN_LALU_',
                    new Ext.form.TextField(
                    {   id : 'total_thn_lalu_id',
                        // store:this.DataStore,
                        displayField: 'total_thn_lalu',
                        valueField: 'total_thn_lalu',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),'-',
                    '_TOTAL_TAHUN_INI_',
                    new Ext.form.TextField(
                    {   id : 'grand_total_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),'-',
                    '_PERSENTASE + -',
                    new Ext.form.TextField(
                    {   id : 't_persen_id',
                        // store:this.DataStore,
                        displayField: 't_persen',
                        valueField: 't_persen',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_pengesahanGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_pengesahanPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_pengesahanPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_pengesahanGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_pengesahanSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_pengesahanTab",
                jsId : tabId+"_pengesahanTab",
                title:  "PENGESAHAN RENCANA ANGGARAN",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[ 
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                                {
                                    text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.pengesahan_search_handler,
                                    scope : this,
                                }
                            ]
                        }
                    ]  
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
            this.DataStore.on( 'load', function( store, records, options )
            {
                console.log(records);
                var total_data = 0;
                var total_thn_lalu=0;
                var t_persentase=0;

                Ext.each(records,
                    function(the_record)
                    { 
                        total_data = total_data + parseInt(the_record.data.total_biaya);
                        total_thn_lalu = total_thn_lalu + parseInt(the_record.data.jumlahn);

                        // t_pengurang = ((record.data.total_biaya)-(record.data.jumlahn));
                        // value = (t_pengurang/(record.data.jumlahn)) * 100;
                        t_persentase = total_data-total_thn_lalu;
                        t_persen = (t_persentase/total_thn_lalu)*100;
                    });

                console.log("total jumlah = "+t_persen);
 //                console.log(Ext.getCmp('grand_total_id').setValue(total_data));
                Ext.getCmp('grand_total_id').setValue(Ext.util.Format.number(total_data, '0,000'));
                Ext.getCmp('total_thn_lalu_id').setValue(Ext.util.Format.number(total_thn_lalu, '0,000'));
                Ext.getCmp('t_persen_id').setValue(Ext.util.Format.number(t_persen, '000.00'));

            });
            // this.DataStore.load();
        },

        @if (array_key_exists('APPROVE KETUA', $MyTasks)) 
        Grid_approve_ketua : function(button, event) 
        {   var detail_data = alfalah.core.getDetailData(alfalah.pengesahan.pengesahan.Grid.getSelectionModel().selections.items);
 //       
            if (detail_data)
            {  console.log(detail_data)
                alfalah.core.submitGrid(
                    this.DataStore,
                    "{{ url('/pengesahan/1/706') }}",
                    {   'x-csrf-token': alfalah.pengesahan.sid },
                    {   json: Ext.encode(detail_data) }
                );
            } 
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        @endif


        // pengesahan search button
        pengesahan_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_pengesahanSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.pengesahan.task,
                        act: alfalah.pengesahan.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.pengesahan.budgeting= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Searchs;
    this.SearchBtn;
    this.Columns;
    this.Grid;
    this.updateTab;
    this.updateForm;
    this.Records;
    this.DetailRecords;

    this.EastGrid;
    this.EastDataStore;
    this.EastGridPanel;
    this.EastRecords;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        load_1     : true,
        // user_data : '',
        // public methods
        initialize: function(the_records)  
        {  
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
    prepare_component: function()
    { this.region_widht = alfalah.pengesahan.centerPanel.container.dom.clientWidht;
        
        this.Columns = [ 
            {   header: "Organisasi ", width : 150,
                dataIndex : 'organisasi_mrapbs_id_name',
                sortable: true,
                hidden : true,
                tooltip:"Jenjang/Bidang/Departemen",
            },
            {   header: "Urusan", width : 300,
                dataIndex : 'urusan_mrapbs_id_name',
                sortable: true,
                tooltip:"Jenjang/Bidang/Departemen",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   result = record.data.urusan_mrapbs_id_name;
                    return alfalah.core.gridColumnWrap(result);
                }
            },
            {   header: "Jumlah Biaya", width : 100,
                dataIndex : 'total_biaya', sortable: true,
                // hidden : true,
                tooltip:"Jumlah total biaya",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   metaData.attr="style = text-align:right;";
                    return Ext.util.Format.number(value, '0,000');
                },
            },
        ];
        
        this.DataStore = alfalah.core.newDataStore(
                "{{ url('/budgeting/1/9') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start });
        this.DataStore.load();
        this.Grid = new Ext.grid.EditorGridPanel(
        {   store:  this.DataStore,
            columns: this.Columns,
            // selModel: cbSelModel,
            enableColLock: false,
            loadMask: true,
            widht : (this.region_widht/2)-50, 
            // anchor: '100%',
            autoScroll  : true,
            // frame: true,
            tbar: [
                '->',
                    'TOTAL ANGGARAN_',
                    new Ext.form.TextField(
                    {   id : 'grand_anggaran_id',
                        // store:this.DataStore,
                        displayField: 'total_anggaran',
                        valueField: 'total_anggaran',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
            ],
            bbar: new Ext.Toolbar(
            {   id : 'bbarCenter',
                // height : 100,
                layout : 'form',
                items:
                [ ],
            }),
        });

        /*************
        * EAST GRID
        *************/
        this.EastDataStore = alfalah.core.newDataStore(
            "{{ url('/budgeting/1/10') }}", false,
            {   s:"init", limit:this.page_limit, start:this.page_start }
        );
        this.EastDataStore.load();
        
            this.EastGridPanel = new Ext.grid.EditorGridPanel( 
            {  store : this.EastDataStore,
               columns : [
                {   header: "ID", width : 50,
                    dataIndex : 'id', sortable: true,
                    tooltip:"ID",
                    hidden: true,
                },
                {   header: "Tahun Pendapatan", width : 150,
                    dataIndex : 'income_master_no', sortable: true,
                    hidden :true,
                    tooltip:"tahun income",
                },
                {   header: "Kode Pendapatan", width : 200,
                    dataIndex : 'income_detail_id', sortable: true,
                    tooltip:"jenis income",
                    hidden :true,
                    readonly:true,
                },
                {   header: "Nama Pendapatan", width : 200,
                    dataIndex : 'income_name', sortable: true,
                    tooltip:"nama jenis pendapatan",
                },
                {   header: "Uraian", width : 150,
                    dataIndex : 'income_uraian', sortable: true,
                    tooltip:"Uraian",
                },
                {   header: "Jumlah Pendapatan", width : 200,
                    dataIndex : 'income_amount', sortable: true,
                    tooltip:"jumlah pendapatan tahun ini",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.income_amount = value;
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
            ],
            tbar: [
                    @if (array_key_exists('APPROVE KETUA', $MyTasks))
                    {   text:'Approve Ketua',
                        tooltip:'approval Usulan Anggaran',
                        iconCls: 'silk-tick',
                        handler : this.Grid_approve_ketua,
                        scope : this
                    },
                    @endif
                    '->',
                    'TOTAL PENDAPATAN_', 
                    new Ext.form.TextField(
                    {   id : 'grand_pendapatan_id',
                        // store:this.DataStore,
                        displayField: 'total_pendapatan',
                        valueField: 'total_pendapatan',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),

            ],
            bbar: new Ext.Toolbar(
            {   id : 'bbarEast',
                // height : 100,
                layout : 'form',
                items:
                [ ],
            }),
        });         

        this.detailTab1 = new Ext.Panel(
        {   id: "pendapatanTab_id",
            title:  "P E N D A P A T A N",
            region: 'center',
            layout: 'border',
            
            items: [
            {   region: 'center', 
                xtype: 'container',
                layout: 'fit',
                items:[this.EastGridPanel]
            }],
        });

        this.detailTab2 = new Ext.Panel(
        {   id: "anggaranTab_id",
            title:  "A N G G A R A N",
            region: 'center',
            layout: 'border',
            items: [
            {   region: 'center', 
                xtype: 'container',
                layout: 'fit',
                items:[this.Grid]
            }],
        });
    },

    // build the layout
    build_layout: function()
    {   
        if (Ext.isObject(this.Records))
            {   the_title = "PENDAPATAN DAN ANGGARAN"; }
        else { the_title = "PENDAPATAN DAN ANGGARAN"; };

        this.Tab = new Ext.Panel(
        {   id : tabId+"_budgetingTab",
            jsId : tabId+"_budgetingTab",
            title:  the_title,
            region: 'center',
            closable : true,
            layout: 'border',
            items: [
                {   
                region: 'center',     // center region is required, no width/height specified
                split:true,
                width: 350,
                minSize: 175,
                maxSize: 400,
                layout : 'fit',
                items: [this.detailTab1]
                },
                {  // title: 'N O T A',
                    region: 'east',     // position for region
                    split:true,
                    width: 650,
                    minSize: 175,
                    maxSize: 400,
                    layout : 'fit',
                    items: [this.detailTab2]
                },
            ]
        });
    },
    // finalize the component and layout drawing

    finalize_comp_and_layout: function()
        { 
            this.DataStore.on( 'load', function( store, records, options )
                {
                    console.log(records);
                    var total_anggaran = 0;
                    Ext.each(records,
                        function(the_record)
                        {   
                            
                            total_anggaran = total_anggaran + parseInt(the_record.data.total_biaya);
                            });


                        Ext.getCmp('grand_anggaran_id').setValue(Ext.util.Format.number(total_anggaran, '0,000'));
                });

            this.EastDataStore.on( 'load', function( store, records, options )
                {
                    console.log(records);
                    var total_pendapatan = 0;
                    Ext.each(records,
                        function(the_record)
                        { 
                            total_pendapatan = total_pendapatan + parseInt(the_record.data.income_amount);
                            });


                        Ext.getCmp('grand_pendapatan_id').setValue(Ext.util.Format.number(total_pendapatan, '0,000'));
                }
            );

        },

        @if (array_key_exists('APPROVE KETUA', $MyTasks)) 
        Grid_approve_ketua : function(button, event) 
        {        var detail_data = alfalah.core.getDetailData(alfalah.pengesahan.pengesahan.Grid.getSelectionModel().selections.items);
            console.log('json_data');
            console.log(detail_data);
            alfalah.core.submitGrid(
                this.DataStore,
                "{{ url('/budgeting/1/706') }}",
                {   'x-csrf-token': alfalah.pengesahan.sid },
                    // {   pengajuan_no:the_record  },
                {   json: Ext.encode(detail_data) }
            );

        },
        @endif
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.pengesahan.initialize, alfalah.pengesahan);
// end of file
</script>
<div>&nbsp;</div>