<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.approvalketua');

// create application
alfalah.approvalketua = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.approvalketua.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.approvalketua.Tab);
            this.centerPanel.setActiveTab(this.approvalketua.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.approvalketua.approvalketua= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            // var cbSelModel = new Ext.grid.CheckboxSelectionModel();
            this.Columns = [
            // cbSelModel,
            {   header: "Nomor Pengajuan", width : 100,
                dataIndex : 'pengajuan_no', sortable: true,
                tooltip:"Nomor pengajuan",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   //approve
                    if ( record.data.approve_status_3 == 1) { metaData.attr = "style = background-color:lime"; }
                    // not yet approve
                    else if ( record.data.approve_status_3 == null){ metaData.attr = "style = background-color:yellow;"; }
                    // rejected
                    else {  metaData.attr = "style = background-color:red;"; };
                    return value;
                }
            },
            {   header: "Keterangan", width : 150,
                dataIndex : 'pengajuan_keterangan_id',
                sortable: true,
                tooltip:"Keterangan Pengajuan",
            },
            {   header: "Organisasi", width : 150,
                dataIndex : 'organisasi_mrapbs_id_name',
                sortable: true,
                // hidden :true,
                tooltip:"Keterangan Pengajuan",
            },
            {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },

            {   header: "Jumlah Biaya", width : 100,
                dataIndex : 'total_biaya', sortable: true,
                tooltip:"Jumlah total biaya",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   metaData.attr="style = text-align:right;";
                    return Ext.util.Format.number(value, '0,000');
                },
            },
            {   header: "Created by", width : 150,
                dataIndex : 'created_by', sortable: true,
                tooltip:"Diajukan oleh",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   result = ' ['+value+'] '+record.data.user;
                    return alfalah.core.gridColumnWrap(result);
                }
            },
            {   header: "Approved by", width : 150,
                dataIndex : 'approve_by_2', sortable: true,
                tooltip:"Disetujui oleh",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   result = ' ['+value+'] '+record.data.kabid;
                    return alfalah.core.gridColumnWrap(result);
                }
            },

            {   header: "Status", width : 50,
                dataIndex : 'status', sortable: true,
                hidden:true,
                tooltip:" Status",
            },
            {   header: "Created", width : 50,
                dataIndex : 'created', sortable: true,
                tooltip:"approvalketua Created Date",
                css : "background-color: #DCFFDE;",
                hidden :true,
                renderer: function(value){  return alfalah.core.dateRenderer(value); }
            },
            {   header: "Updated", width : 50,
                dataIndex : 'modified_date', sortable: true,
                tooltip:"approvalketua Last Updated",
                css : "background-color: #DCFFDE;",
                hidden :true,
                renderer: function(value){  return alfalah.core.dateRenderer(value); }
            }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                // {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'approvalketua_organisasi_id',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Org.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'approvalketua_keterangan_id',
                    cid: "keterangan",
                    fieldLabel: 'ket.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },

            ];

            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_approvalketuaSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.approvalketua_search_handler,
                scope : this 
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/approvalketua/1/0')}}", false,
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   
            store:  this.DataStore,
            columns: this.Columns,
            // selModel: cbSelModel,
            //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
            enableColLock: false,
            //trackMouseOver: true,
            loadMask: true,
            height : alfalah.approvalketua.centerPanel.container.dom.clientHeight-50,
            anchor: '100%',
            autoScroll  : true,
            frame: true,
            //stripeRows : true,
            // inline buttons
            //buttons: [{text:'Save'},{text:'Cancel'}],
            //buttonAlign:'center',
            tbar: [
                {   text:'Detail kegiatan',
                    tooltip:'view Pengajuan berdasar rapbs.no',
                    iconCls: 'silk-page-white-edit',
                    handler : this.Grid_pengajuan,
                    scope : this
                },

                {   text:'Detail Biaya',
                    tooltip:'view detail',
                    iconCls: 'silk-page-white-edit',
                    hidden :true,
                    handler : this.Grid_edit,
                    scope : this
                },
                '-',
                {   print_type : "pdf",
                    text:'Print PDF',
                    tooltip:'Print to PDF',
                    iconCls: 'silk-page-white-acrobat',
                    handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                    scope : this
                },
                {   print_type : "xls",
                    text:'Print Excell',
                    hidden :true,
                    tooltip:'Print to Excell SpreadSheet',
                    iconCls: 'silk-page-white-excel',
                    handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                    scope : this
                },'-',
                // {   text:'Keterangan Warna',
                //     iconCls:'silk-information ',
                //     scope : this
                // },'-',
                {   text:'Approve',
                    tooltip:'approvalketua Anggaran',
                    // xType : 'dataview',
                    style:'background-color:lime',
                    iconCls: 'silk-tick',
                    handler : this.Grid_approve,
                    scope : this
                },'-',
                {   text:'Not Yet Approve',
                    tooltip:'Anggaran Belum di approve',
                    // xType : 'dataview',
                    hidden : true,
                    style:'background-color:yellow',
                    iconCls: 'silk-stop',
                    // handler : this.Grid_reject,
                    // scope : this
                },'-',
                {   text:'Reject',
                    tooltip:'Reject Pengajuan Bulanan',
                    // xType : 'dataview',
                    // hidden :true,
                    style:'background-color:red',
                    iconCls: 'silk-cancel',
                    handler : this.Grid_reject,
                    scope : this
                },
                '->',
                '_T O T A L_',
                new Ext.form.TextField(
                {   id : 'grand_total_id',
                    // store:this.DataStore,
                    displayField: 'total_data',
                    valueField: 'total_data',
                    allowBlank : false,
                    readOnly : true,
                    style: "text-align: right; background-color: #ffff80; background-image:none;",
                    mode: 'local',
                    forceSelection: true,
                    triggerAction: 'all',
                    selectOnFocus: true,
                    // listeners :
                    // {   "beforeedit" : this.Grid_grand_total },
                    // scope : this
                }),
            ],
            bbar: new Ext.PagingToolbar(
            {   id : tabId+'_approvalketuaGridBBar',
                store: this.DataStore,
                pageSize: this.page_limit,
                displayInfo: true,
                emptyMsg: 'No data found',
                items : [
                    '-',
                    'Displayed : ',
                    new Ext.form.ComboBox(
                    {   id : tabId+'_approvalketuaPageCombo',
                        store: new Ext.data.SimpleStore(
                                    {   fields: ['value'],
                                        data : [[50],[75],[100],[125],[150]]
                                    }),
                        displayField:'value',
                        valueField :'value',
                        value : 75,
                        editable: false,
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus:true,
                        hiddenName: 'pagesize',
                        width:50,
                        listeners : { scope : this,
                            'select' : function (a,b,c)
                            {   this.page_limit = Ext.get(tabId+'_approvalketuaPageCombo').getValue();
                                bbar = Ext.getCmp(tabId+'_approvalketuaGridBBar');
                                bbar.pageSize = parseInt(this.page_limit);
                                this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                this.SearchBtn.handler.call(this.SearchBtn.scope);
                                //Ext.getCmp(tabId+"_approvalketuaSearchBtn").handler.call();
                                //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                            }
                        }
                    }),
                    ' records at a time'
                ]
            }),
        });
    },
    // build the layout
    build_layout: function()
    {   this.Tab = new Ext.Panel(
        {   id : tabId+"_approvalketuaTab",
            jsId : tabId+"_approvalketuaTab",
            title:  "Data Pengajuan approvalketua",
            region: 'center',
            layout: 'border',
            items: [
            {   title: 'Parameters',
                region: 'east',     // position for region
                split:true,
                width: 200,
                minSize: 200,
                maxSize: 400,
                collapsible: true,
                layout : 'fit',
                items:[
                {   //title: 'S E A R C H',
                    labelWidth: 50,
                    defaultType: 'textfield',
                    xtype: 'form',
                    frame: true,
                    autoScroll : true,
                    items : this.Searchs,
                    tbar: [
                        {   text:'Search',
                            tooltip:'Search',
                            iconCls: 'silk-zoom',
                            handler : this.approvalketua_search_handler,
                            scope : this,
                        }]
                }]
            },
            {   region: 'center',     // center region is required, no width/height specified
                xtype: 'container',
                layout: 'fit',
                items:[this.Grid]
            }
            ]
        });
    },
    // finalize the component and layout drawing
    finalize_comp_and_layout: function()
    {
        this.DataStore.on( 'load', function( store, records, options )
        {
            console.log(records);
            var total_data = 0;
            Ext.each(records,
                function(the_record)
                { 
                    total_data = total_data + parseInt(the_record.data.total_biaya);
                });

            console.log("total jumlah = "+total_data);
//                console.log(Ext.getCmp('grand_total_id').setValue(total_data));
            Ext.getCmp('grand_total_id').setValue(Ext.util.Format.number(total_data, '0,000'));
        });
        // this.DataStore.load();
        
    },

    Grid_pengajuan: function(button, event)
    {  var the_record = this.Grid.getSelectionModel().selection;
        if ( the_record )
        {
            var centerPanel = Ext.getCmp('center_panel');
            alfalah.approvalketua.pengajuan.initialize(the_record);
            centerPanel.beginUpdate();
            centerPanel.add(alfalah.approvalketua.pengajuan.Tab);
            centerPanel.setActiveTab(alfalah.approvalketua.pengajuan.Tab);
            centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
            }
            else
        {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Data Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
        };
    },

    Grid_add: function(button, event)
    {   var centerPanel = Ext.getCmp('center_panel');
        alfalah.approvalketua.forms.initialize();
        centerPanel.beginUpdate();
        centerPanel.add(alfalah.approvalketua.forms.Tab);
        centerPanel.setActiveTab(alfalah.approvalketua.forms.Tab);
        centerPanel.endUpdate();
        alfalah.core.viewport.doLayout();
    },
    Grid_edit : function(button, event)
    {   var the_record = this.Grid.getSelectionModel().selection;
        if ( the_record )
            { 
                    var centerPanel = Ext.getCmp('center_panel');
                alfalah.approvalketua.forms.initialize(the_record);
                centerPanel.beginUpdate();
                centerPanel.add(alfalah.approvalketua.forms.Tab);
                centerPanel.setActiveTab(alfalah.approvalketua.forms.Tab);
                centerPanel.endUpdate();
                alfalah.core.viewport.doLayout();
            }
        
        else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
    },
    Grid_detail : function(button, event)
    {   var the_record = this.Grid.getSelectionModel().selection;
        if ( the_record )
        {   var form_order = Ext.getCmp("form_order");
            if (form_order)
            {   Ext.Msg.show(
                {   title :'E R R O R ',
                    msg : 'Order Form Available',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
            else
            {   var centerPanel = Ext.getCmp('center_panel');
                alfalah.approvalketua.detailFrm.initialize(the_record.record);
                centerPanel.beginUpdate();
                centerPanel.add(alfalah.approvalketua.detailFrm.Tab);
                centerPanel.setActiveTab(alfalah.approvalketua.detailFrm.Tab);
                centerPanel.endUpdate();
                alfalah.core.viewport.doLayout();
            };
        }
        else
        {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Data Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
        };
    },
    // approvalketua grid save records

    Grid_save : function(button, event)
    {   var the_records = this.DataStore.getModifiedRecords();
        // check data modification
        if ( the_records.length == 0 )
        {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Data modified ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
        } else
        {   var head_data = alfalah.core.getDetailData(the_records);
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/approvalketua/1/1') }}",
                {   'x-csrf-token': alfalah.approvalketua.sid }, 
                {   head : Ext.encode(head_data) }
            );
        };
    },
    // approvalketua grid delete records
    Grid_remove: function(button, event)
    {   this.Grid.stopEditing();
        Ext.Ajax.request(
        {   method: 'POST',
            url: "{{ url('/approvalketua/1/2') }}",
            headers:
            {   'x-csrf-token': alfalah.approvalketua.sid },
            params  :
            {   rapbs_no: this.Grid.getSelectionModel().selection.record.data.rapbs_no
            },
            success: function(response)
            {   var the_response = Ext.decode(response.responseText);
                if (the_response.success == false)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Server Message : '+'\n'+the_response.message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                        });
                }
                else
                {   this.DataStore.reload();
                    //datastore.remove(grid.getSelectionModel().selections.items[0]);
                };
            },
            failure: function()
            { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
            },
            scope: this
        });
    },
    
    Grid_approve : function(button, event) 
    {   var the_record = this.Grid.getSelectionModel().selection;
        if ( the_record )
        {   Ext.Msg.show(
                {   title :'A P P R O V A L',
                    msg : 'APPROVE ?',
                    width:250,
                    buttons: Ext.Msg.YESNO,
                    fn: function(buttonId, text)
                    { if (buttonId =='yes')
                {  
                    alfalah.core.submitGrid(
                        alfalah.approvalketua.approvalketua.DataStore, 
                    "{{ url('/approvalketua/1/705') }}",
                    {   'x-csrf-token': alfalah.approvalketua.sid }, 
                    {   pengajuan_no: alfalah.approvalketua.approvalketua.Grid.getSelectionModel().selection.record.data.pengajuan_no }
                );
            } else
                        { this.DataStore.reload(); };
                    },
                    icon: Ext.MessageBox.WARNING
                });
        }
        else
        {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Data Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
        };
    },

    Grid_reject: function(button, event)
        {   this.Grid.stopEditing();
            var selection_data = alfalah.approvalketua.approvalketua.Grid.getSelectionModel().selection;
            if (selection_data)
            {   var app_data = selection_data.record.data.approve_status_3;
                var data = selection_data.record.data.pengajuan_no;
                var head_data = [selection_data.record.data]; 
                get_head = Ext.apply(head_data);
                // console.log('ini get head_data');
                //     console.log(get_head);
                if (app_data == null) {
                    
                    Ext.MessageBox.show({
                        title: 'WARNING !!!',
                        msg: 'DATA WILL BE REJECT, ARE YOU SURE ??? <br /><br />Reject Reason : <input type="text"  style="width: 250px" id="catat_id"/>',
                        buttons: Ext.MessageBox.OKCANCEL,
                        grow: true,
                        width : 400,
                      //  activeItem : true,
                        fn: function (buttonId, catatan, head) {
                            if (buttonId =='ok' )
                                {   
                                    Ext.Ajax.request(
                                    {   method: 'POST',
                                        url: "{{ url('/approvalketua/1/100') }}",
                                        headers: {   'x-csrf-token': alfalah.approvalketua.sid },
                                        params: {   
                                                    catatan : Ext.get('catat_id').getValue(),
                                                    head : Ext.encode(get_head),
                                                },
                                        success: function(response)
                                        {   var the_response = Ext.decode(response.responseText); 
                                            if (the_response.success == false)
                                            {   Ext.Msg.show(
                                                {   title :'E R R O R ',
                                                    msg : 'Server Message : '+'\n'+the_response.message,
                                                    buttons: Ext.Msg.OK,
                                                    icon: Ext.MessageBox.ERROR
                                                });
                                            }
                                            else
                                            {   //alfalah.pencairan.pencairan.DataStore.reload(); 
                                                Ext.MessageBox.show({
                                                    title: 'DATA HAS BEEN REJECTED',
                                                    buttons: Ext.Msg.OK,
                                                    closable:false,
                                                    width : 200,
                                                    fn : function (btn) { 
                                                        if (btn == "ok") 
                                                        {  
                                                            alfalah.core.submitGrid(
                                                                alfalah.approvalketua.approvalketua.DataStore,
                                                                "{{ url('/approvalketua/1/2') }}",
                                                                {   'x-csrf-token': alfalah.approvalketua.sid }, 
                                                                {   pengajuan_no: alfalah.approvalketua.approvalketua.Grid.getSelectionModel().selection.record.data.pengajuan_no }
                                                            );
                                                        } else
                                                        { this.DataStore.reload(); };
                                                    }
                                                });
                                                //console.log('test reject pengajuan');
                                            };
                                        },
                                        failure: function()
                                        {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                                        },
                                    });
                                       
                            }
                            
                        }
                    }
                ) 
                    
                }
                else
                {  
                    Ext.Msg.show(
                    {   title   :'I N F O ',
                        msg     : 'Data Has Been Verification',
                        buttons : Ext.Msg.OK,
                        icon    : Ext.MessageBox.INFO,
                        width   : 200
                    });
                   

                }    
            }
            else
            {   Ext.Msg.show(
                {   title   :'I N F O ',
                    msg     : 'No Selected Record ! ',
                    buttons : Ext.Msg.OK,
                    icon    : Ext.MessageBox.INFO,
                    width   : 200
                });
            };
        },

    // approvalketua search button
    approvalketua_search_handler : function(button, event)
    {   var the_search = true;
        if ( this.DataStore.getModifiedRecords().length > 0 )
        {   Ext.Msg.show(
                {   title:'W A R N I N G ',
                    msg: ' Modified Data Found, Do you want to save it before search process ? ',
                    buttons: Ext.Msg.YESNO,
                    fn: function(buttonId, text)
                        {   if (buttonId =='yes')
                            {   Ext.getCmp(tabId+'_approvalketuaSaveBtn').handler.call();
                                the_search = false;
                            } else the_search = true;
                        },
                    icon: Ext.MessageBox.WARNING
                    });
        };

        if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.approvalketua.task,
                        act: alfalah.approvalketua.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
// create application
alfalah.approvalketua.pengajuan= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;
    this.updateTab;
    this.updateForm;
    this.DetailRecords;

    this.SouthGrid;
    this.SouthDataStore;    

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        load_1 :true,
        // public methods
        initialize: function(the_records)  
        {   console.log('initialize approvalketua.pengajuan');
            if (the_records)
            {   console.log(the_records);
                this.Records = the_records; 
            };
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },

        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.approvalketua.centerPanel.container.dom.clientHeight;
            this.Columns = [
                {   header: "RAPBS.No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"RAPBS No",
                },
                {   header: "Urusan / Organisasi", width : 150,
                    dataIndex : 'urusan_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.organisasi_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 250,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Sumber Dana", width : 150,
                    dataIndex : 'sumberdana_id_name', sortable: true,
                    hidden :true,
                    tooltip:"sumber pendanaan",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Jumlah Biaya", width : 100,
                    id : 'total_biaya_urusan',
                    dataIndex : 'total', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    hidden : true,
                    tooltip:" Status",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    hidden :true,
                    tooltip:"approvalketua Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    hidden :true,
                    tooltip:"approvalketua Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Searchs = [
                {   id: 'pengajuan_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengajuan_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengajuan_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_pengajuanSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                // handler : this.approvalketua_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/approvalketua/1/99')}}", false,
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );

            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.approvalketua.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        hidden :true,
                        tooltip:'Print to Excell SpreadSheet', 
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                    {   text:'Action',
                        iconCls:'silk-information ',
                        hidden :true,
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_pengajuanGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_pengajuanPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_pengajuanPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_pengajuanGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_approvalketuaSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            listeners : { scope : this,
                    'cellclick': function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent)
                    {
                        console.log('cell click');
                        if (iColIdx == 0 ) // kolom pertama
                        {   the_record = this.Grid.getSelectionModel().selection.record.data.pengajuan_no
                            console.log(the_record);
                            // this.southdatastore.baseParam = {new param in here};
                            alfalah.approvalketua.pengajuan.SouthDataStore.baseParams = {   task: alfalah.approvalketua.task,
                                act: alfalah.approvalketua.act,
                                a:2, b:0, s:"forms", pengajuan_no : the_record,
                                limit:this.page_limit, start:this.page_start 
                            };
                            console.log('alfalah.pencairan.pencairan.SouthDataStore')    
                            console.log(alfalah.approvalketua.pengajuan.SouthDataStore)             
                            alfalah.approvalketua.pengajuan.SouthDataStore.reload();
                        };
                    },
                    'rowselect' : function (a,b,c)
                    {   console.log('listener grid select');
                        console.log(a);
                        console.log(b);
                        console.log(c);
                    }
                }
            });

            /**
                * SOUTH-GRID
                */
                this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/approvalketua/1/10') }}", false,
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );
            // this.SouthDataStore.load();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.SouthDataStore,
                columns: [ 
                {   header: "Pengajuan No", width : 100,
                    dataIndex : 'pengajuan_no', sortable: true,
                    readonly : true,
                    tooltip:"Nomor Pengajuan anggaran",
                },
                {   header: "RAPBS.No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    hidden : true,
                    readonly : true,
                    tooltip:"RAPBS No",
                },
                {   header: "Kode Rekening", width : 100,
                    dataIndex : 'coa_id', sortable: true,
                    tooltip:"Kode rekening",
                    readonly:true,
                },
                {   header: "Name", width : 200,
                    dataIndex : 'coa_name', sortable: true,
                    tooltip:"Pos Belanja Name",
                    readonly : true,
                },
                {   header: "Uraian", width : 150,
                    dataIndex : 'uraian', sortable: true,
                    tooltip:"Uraian",
                    readonly : true,
                },
                {   header: "Volume", width : 100,
                    dataIndex : 'vol_ajuan', sortable: true,
                    tooltip:"Volume",
                    readonly : true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.vol_ajuan = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Satuan", width : 100,
                    dataIndex : 'satuan', sortable: true,
                    tooltip:"Satuan",
                    readonly : true,
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },

                {   header: "Tarif", width : 100,
                    dataIndex : 'tarif', sortable: true,
                    tooltip:"Tarif",
                    readonly : true,
                    // editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.tarif = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Jumlah pengajuan", width : 100,
                    id: 'jumlah_id',
                    dataIndex : 'jum_ajuan', sortable: true,
                    tooltip:"Jumlah",
                    readonly : true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.jum_ajuan = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
            ],
            loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Save',
                        id : 'btnSave',
                        tooltip:'Save Record',
                        hidden :true,
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        scope : this,
                    },

                  ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_southGridBBar',
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_southPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_southPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_southGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_pengajuanTab",
                jsId : tabId+"_pengajuanTab",
                title:  "DATA PENGAJUAN",
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                // handler : this.pencairan_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {
                    region: 'south',
                    title: 'DETAIL BIAYA',
                    split: true,
                    height : this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    layout : 'border',
                    items :[ 
                    {   
                        region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.SouthGrid]
                    }]
                }]
            });
        },

        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   
            the_records = this.Records
            if (the_records) {

                console.log('records di finalize');
                console.log(the_records);

            this.DataStore.baseParams = Ext.apply( the_parameter,    
            {   s:"forms", limit:this.page_limit, start:this.page_start,
                pengajuan_no : the_records.record.data.pengajuan_no 
            })
            this.DataStore.load();}    
        },

        Grid_pdf : function(button, event)
        {     var the_records = this.Records;
            if ( the_records )
            {   the_record = the_records.record.data;
                console.log(the_record)
                alfalah.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        pengajuan_no : the_record.pengajuan_no });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };
        },


        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: "{{ url('/approvalketua/1/2') }}",
                headers:
                {   'x-csrf-token': alfalah.approvalketua.sid },
                params  :
                {   rapbs_no: this.Grid.getSelectionModel().selection.record.data.rapbs_no
                },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Server Message : '+'\n'+the_response.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.approvalketua.initialize, alfalah.approvalketua);
// end of file
</script>
<div>&nbsp;</div>