<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.apby');

// create application
alfalah.apby = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.apby.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.apby.Tab);
            this.centerPanel.setActiveTab(this.apby.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.apby.apby= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            var cbSelModel = new Ext.grid.CheckboxSelectionModel();
            this.Columns = [
                 cbSelModel,
                {   header: "No. Kegiatan", width : 120,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"RAPBS No",
                },
                {   header: "Organisasi/Urusan ", width : 200,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 200,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Sumber Dana", width : 100,
                    dataIndex : 'sumberdana_id_name', sortable: true,
                    tooltip:"sumber pendanaan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Coa", width : 150,
                    dataIndex : 'coa_id_name', sortable: true,
                    tooltip:"kode rekening",
                    hidden :true,
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {  
                    header: "Jumlah Tahun Ke N", width : 100,
                    dataIndex : 'total_biaya', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {  
                    header: "Sisa Pengajuan", width : 100,
                    dataIndex : 'sisa_biaya', sortable: true,
                    tooltip:"sisa biaya pengajuan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    { 
                        if (value > 0) 
                          { value = parseInt(record.data.total_biaya) - parseInt(record.data.sisa_biaya); }
                        else {value = (record.data.total_biaya) ;};     
                        metaData.attr="style = text-align:right;";
                        // { metaData.attr = "style = background-color:yellow ;";}
                        return Ext.util.Format.number(value, '0,000');   
                    },
                },
                {   header: "Jumlah Tahun Lalu", width : 100,
                    dataIndex : 'jumlahn', sortable: true,
                    hidden :true,
                    tooltip:"Jumlah Anggaran Tahun Sebelumnya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Tahun Ke N", width : 100,
                    dataIndex : 'jumlahke_n', sortable: true,
                    tooltip:"Jumlah Anggaran Tahun ini",
                    hidden :true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   value = (record.data.sisa_biaya);
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Status", width : 50,
                    hidden : true,
                    dataIndex : 'status', sortable: true,
                    tooltip:" Status",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"apby Created Date",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"apby Last Updated",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                // {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'apby_urusan',
                    cid: "urusan_mrapbs_id_name",
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'apby_organisasi',
                    cid: 'organisasi_mrapbs_id_name',
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'apby_kegiatan',
                    cid: 'kegiatan_mrapbs_id_name',
                    fieldLabel: 'Kegiatan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'apby_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'Sub.Keg',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                
                {   id: 'apby_status',
                    cid: 'status',
                    hidden : true,
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },

            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_apbySearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.apby_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/apby/1/0')}}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   
                store:  this.DataStore,
                columns: this.Columns,
                selModel: cbSelModel,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.apby.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Pengajuan Kegiatan',
                        tooltip:'pengajuan anggaran',
                        iconCls: 'silk-add',
                        handler : this.Grid_apby,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        hidden :true,
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        hidden:true,
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    '->',
                    '_T O T A L_',
                    new Ext.form.TextField(
                    {   id : 'grand_total_biaya_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        // listeners :
                        // {   "beforeedit" : this.Grid_grand_total },
                        // scope : this
                    }),


                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_apbyGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_apbyPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_apbyPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_apbyGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_apbyTab",
                jsId : tabId+"_apbyTab",
                title:  "PENGAJUAN ANGGARAN",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.apby_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
            this.DataStore.on( 'load', function( store, records, options )
            {
                console.log(records);
                var total_data = 0;
                Ext.each(records,
                    function(the_record)
                    { 
                        total_data = total_data + parseInt(the_record.data.total_biaya);
                    });
                Ext.getCmp('grand_total_biaya_id').setValue(Ext.util.Format.number(total_data, '0,000'));
            });
        },
        Grid_apby: function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selections.items;
            if (the_record)
                {   
                    var centerPanel = Ext.getCmp('center_panel');
                    alfalah.apby.pengajuan.initialize(the_record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.apby.pengajuan.Tab);
                    centerPanel.setActiveTab(alfalah.apby.pengajuan.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                }    
            else
            {
                Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },

        Grid_edit : function(button, event)
        {  console.log('grid_edit');
             var the_record = this.Grid.getSelectionModel().selection;
             console.log(the_record);
            if ( the_record )
            { 
                    var centerPanel = Ext.getCmp('center_panel');
                    alfalah.apby.forms.initialize(the_record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.apby.forms.Tab);    
                    centerPanel.setActiveTab(alfalah.apby.forms.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_detail : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var form_order = Ext.getCmp("form_order");
                if (form_order)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Order Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    alfalah.apby.detailFrm.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.apby.detailFrm.Tab);
                    centerPanel.setActiveTab(alfalah.apby.detailFrm.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        // apby grid delete records
        Grid_approve : function(button, event) 
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   if (the_record.record.data.approve_status_1 == null)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Need approval',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   alfalah.core.submitGrid(
                        this.DataStore, 
                        "{{ url('/apby/1/705') }}",
                        {   'x-csrf-token': alfalah.apby.sid }, 
                        {   rapbs_no: this.Grid.getSelectionModel().selection.record.data.rapbs_no }
                    );
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },

        Grid_reject: function(button, event)
        {   
            // the_rapbs_no = Ext.getCmp('rapbsFrm_rapbs_no').getValue();
            the_rapbs_no = this.Grid.getSelectionModel()
            if (the_rapbs_no == "")
            {
                Ext.Msg.show(
                {   title :'E R R O R ',
                    msg : 'RAPBS not saved yet',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
            else
            {
                this.Grid.stopEditing();
                Ext.Ajax.request(
                {   method: 'POST',
                    url: "{{ url('/apby/1/707') }}",
                    headers:
                    {   'x-csrf-token': alfalah.apby.sid },
                    params  :
                    {   rapbs_no: the_rapbs_no },
                    success: function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                        else
                        {   
                            Ext.getCmp(tabId+"_FormsTab").destroy();
                            alfalah.apby.apby.DataStore.reload();
                        };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                  scope: this
                });
            };
        },
        // apby search button
        apby_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_apbySaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.apby.task,
                        act: alfalah.apby.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
// create application
alfalah.apby.pengajuan= function()
{   // do NOT access DOM from here; elements don't exist yet
// execute at the first time
    // private variables
    this.Tab;
    this.Searchs;
    this.SearchBtn;
    this.Form;
    this.updateTab;
    this.updateForm;
    this.Records;
    this.DetailRecords;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        load_1     : true,
        // user_data : '',
        // public methods
        initialize: function(the_records)  
        {   console.log('initialize pengajuan');
            if (the_records)
            {   console.log(the_records);
                this.Records = the_records; 
            };
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.total_biaya_rapbs();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   console.log('prepare');
            console.log(this.Records);
            
            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   id : 'apbyFrm',
                // width: '100%',
                frame: true,
                title : 'H E A D E R',
                // autoHeight: true,
                bodyStyle: 'padding: 10px 10px 0 10px;',
                labelWidth: 100,
                defaults: {
                    anchor: '100%',
                    allowBlank: false,
                    msgTarget: 'side'
                },
                items: [
                {   layout:'column',
                    border:false,
                    items:
                    [{   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'apbyFrm_id',
                            xtype:'textfield',
                            fieldLabel: 'ID',
                            name: 'id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '',
                            hidden : true,
                        },
                        {   id : 'apbyFrm_created_date', 
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                            value : '',
                        },
                        {   id : 'apbyFrm_tahun_ajaran_id', 
                            xtype:'textfield',
                            fieldLabel: 'Tahun',
                            name: 'tahun_ajaran_id',
                            anchor:'65%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $TAHUNANGGARAN_ID }}',
                            hidden : true,
                        },
                        {   id : 'apbyFrm_pengajuan_no', 
                            xtype:'textfield',
                            fieldLabel: 'No. Pengajuan',
                            name: 'pengajuan_no',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : 'by-system',
                        },
                        {   id : 'apbyFrm_bln_pengajuan', 
                            xtype:'datefield',
                            fieldLabel: 'Bulan Pengajuan',
                            format: 'm/Y',
                            name: 'bln_pengajuan',
                            anchor:'95%',
                            // readOnly : true,
                            allowBlank: false,
                            value : new Date(),
                        },
                        {   id : 'apbyFrm_pengajuan_keterangan_id', 
                            xtype:'textfield',
                            fieldLabel: 'keterangan',
                            name: 'pengajuan_keterangan_id',
                            // format: 'd/m/Y',
                            value: 'Pengajuan Anggaran'+' '+ new Date().format('d/m/Y'),
                            anchor:'95%',
                            // readOnly : true,
                            allowBlank: false,
                            
                        }]
                    }]
                }],
            });
            /************************************
                G R I D S
            ************************************/ 
            var cbSelModel = new Ext.grid.CheckboxSelectionModel();
             
            this.Columns = [ cbSelModel,
           
                {   header: "ID", width : 50,
                    dataIndex : 'id', sortable: true,
                    tooltip:"ID",
                    // hidden: true,
                },
                {   header: "No Kegiatan", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"Doc.No",
                },
                {   header: "Sub Kegiatan", width : 150,
                    hidden :true,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Pos.Belanja.No", width : 100,
                    dataIndex : 'coa_id', sortable: true,
                    tooltip:"Pos Belanja",
                    readonly:true,
                },

                {   header: "Name", width : 200,
                    dataIndex : 'coa_name', sortable: true,
                    readonly:true,
                    tooltip:"Pos Belanja Name",
                    editor: new Ext.form.ComboBox(
                    {   store: this.coaDS_2,
                        typeAhead: false,
                        width: 250,
                        readonly:true,

                        displayField: 'display',
                        valueField: 'coa_name',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        // triggerAction: "All",
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.coa_id = record.data.mcoa_id;
                                    combo.gridEditor.record.data.coa_name = record.data.coa_name;
                                    alfalah.apby.forms.Grid.getView().refresh();
                                },
                        }
                    }),
                },
                {   header: "Uraian", width : 150,
                    dataIndex : 'uraian', sortable: true,
                    tooltip:"Uraian",
                    readonly:true,
                },
                {   header: "Volume awal", width : 100,
                    dataIndex : 'volume', sortable: true,
                    tooltip:"Volume awal",
                    readonly:true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.volume = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   id:'volume_sisa_id',
                    header: "Volume Sisa", width : 100,
                    dataIndex : 'vol_sisa', sortable: true,
                    tooltip:"Volume Sisa Pengajuan",
                    readonly:true,
                    // renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    // {   value = parseInt(record.data.volume) - parseInt(record.data.vol_ajuan);
                    //     record.data.vol_sisa = value;
                    //     return Ext.util.Format.number(value, '0,000');
                    // }
                }, 
                {   id:'volume_ajuan_id',
                    header: "Volume Pengajuan", width : 100,
                    dataIndex : 'vol_ajuan', sortable: true,
                    tooltip:"Volume yang diajukan",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    readonly:false,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store, view)
                    {   { metaData.attr = "style = background-color:lime;"; }
                        if (record.data.vol_sisa == 0) { value = 0 ; record.data.vol_ajuan=0;
                        metaData.attr =  "style = background-color:red;"; };
                        return Ext.util.Format.number(value, '0,000');
                    
                    },
                    
                },
                {   header: "Satuan", width : 100,
                    dataIndex : 'satuan', sortable: true,
                    tooltip:"Satuan",
                    readonly:true,
                },
                {   header: "Tarif", width : 100,
                    dataIndex : 'tarif', sortable: true,
                    tooltip:"Tarif",
                    readonly:true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.tarif = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Jumlah asal", width : 100,
                    dataIndex :'jumlah', sortable: true,
                    readonly:true,
                    tooltip:"Jumlah",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        record.data.jumlah = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Jumlah Pengajuan", width : 100,
                    dataIndex : 'jum_ajuan', sortable: true,
                    readonly:false,
                    tooltip:"Jumlah dari pengajuan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                       
                        value = parseInt(record.data.vol_ajuan) * parseInt(record.data.tarif);
                        record.data.jum_ajuan = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },

                {   header: "Ber-ulang", width : 60,
                    dataIndex : 'berulang', sortable: true,
                    hidden :true,
                    tooltip:"variabel pembagi anggaran yg berulang",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Status", width : 100,
                    hidden : true,
                    dataIndex : 'status_name', sortable: true,
                    readonly:true,

                    tooltip:"Status",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                            fields: ["label"],
                                            data : [["Active"], ["InActive"]]
                                        }),
                            displayField:"label",
                            valueField:"label",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
            ];
            
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/apby/1/10') }}", false,
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );
            //  this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                selModel: cbSelModel,
                enableColLock: false,
                loadMask: true,
                height : alfalah.apby.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Save',
                        id : 'btnSave_apby',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        scope : this, 
                    },
                    '-',
                    {   text:'Approve',
                        tooltip:'Approve Record',
                        iconCls: 'silk-tick',
                        hidden :true,
                        handler : this.Grid_approve,
                        scope : this
                    },
                    '-',
                    '_T O T A L_B I A Y A',
                    new Ext.form.TextField(
                    {   id : 'total_biaya_id',
                        // store:this.DataStore,
                        displayField: 'total_biaya',
                        valueField: 'total_biaya',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                    '-',
                    // '_TOTAL_BIAYA_AJUAN',
                    // new Ext.form.TextField(
                    // {   id : 'total_ajuan_id',
                    //     // store:this.DataStore,
                    //     displayField: 'total_ajuan',
                    //     valueField: 'total_ajuan',
                    //     allowBlank : false,
                    //     readOnly : true,
                    //     style: "text-align: right; background-color: #00ff40; background-image:none;",
                    //     mode: 'local',
                    //     forceSelection: true,
                    //     triggerAction: 'all',
                    //     selectOnFocus: true,
                    // }),

                ],
                listeners :
                    {   'afteredit' : this.Gridafteredit
                      //  'select'    : this.Grid_jum_ajuan_onchange
                    
                     },
                scope : this,

                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_pengajuanGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_pengajuanPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_pengajuanPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_pengajuanGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
        },
        // build the layout
        build_layout: function()
        {   
            if (Ext.isObject(this.Records))
                {   the_title = "EDIT PENGAJUAN"; }
            else { the_title = "PENGAJUAN KEGIATAN"; };

            this.Tab = new Ext.Panel(
            {   id : tabId+"_pengajuanTab",
                jsId : tabId+"_pengajuanTab",
                title:  '<span style="background-color: yellow;">'+the_title+'</span>',
                iconCls: 'silk-note_add',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region   : 'center', // center region is required, no width/height specified
                    xtype    : 'tabpanel',
                    activeTab: 0,
                    items    : [this.Form, this.detailTab]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log('finalize_comp_and_layout');

            the_records = this.Records   
            this.TargetRecord = Ext.data.Record.create(
            [ 
                {name: 'rapbs_id', type: 'string'},
                {name: 'rapbs_no', type: 'string'},
            ]);
            if (the_records)
            { 
                console.log('the_records');
                console.log(the_records);
                var json_data = [];
                var v_data = {};

                Ext.each(the_records,
                    function(the_record)
                    {   console.log('the_record');
                        console.log(the_record.data.rapbs_no);
                        v_data = {};
                        v_data[ 'rapbs_no' ] = the_record.data.rapbs_no;
                        json_data.push(v_data);  
                    }, this );
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"forms", limit:this.page_limit, start:this.page_start,
                        rapbs_no : Ext.encode(json_data),
                        
                    });
                console.log(this.DataStore.baseParams);
                this.DataStore.load();
            };
        },

        Form_save : function(button, event) //yang ini om
        {   var head_data = [];
            var detail1_data =[];
            var json_data = [];
            var modi_data = [];
            var v_json = {};
            var the_records = alfalah.apby.pengajuan.Grid.getSelectionModel().selections.items;

            if ( the_records.length < 1 )  
            {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'Please select minimum 1 Item.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    width : 300
                });
            } // header validation
            else if (alfalah.core.validateFields([
                'apbyFrm_pengajuan_no', 'apbyFrm_pengajuan_keterangan_id','apbyFrm_bln_pengajuan']))
            {   //detail validasi
                console.log("HUAHAHAHAHAHA");
                if ( alfalah.core.validateGridQuantity(
                                the_records,
                                ['vol_ajuan']))
                {   console.log("HIHIHIHIHIHI");
                    data_pengajuan = alfalah.core.getHeadData(alfalah.apby.pengajuan.Form.getForm().items.keys);                     
                    data_detail1 = alfalah.core.getDetailData(alfalah.apby.apby.Grid.getSelectionModel().selections.items);
                    data_detail2 = alfalah.core.getDetailData(alfalah.apby.pengajuan.Grid.getSelectionModel().selections.items) ; 
                    data_detail = Ext.apply(data_detail2)
                    // submit data 
                    alfalah.core.submitForm(
                    // 'apbyFrm', 
                    tabId+"_pengajuanTab",
                    alfalah.apby.pengajuan.DataStore,
                    
                    "{{ url('/apby/1/1') }}", 
                    {   'x-csrf-token': alfalah.apby.sid },
                    {   task: 'save', 
                        head : Ext.encode(data_pengajuan[0]),
                        json : Ext.encode(data_detail2),
                        detail1 : Ext.encode(data_detail1),
                    }, Ext.getCmp('btnSave_apby').disabled=true
                    );
                };

                
            }; 
        },

        Grid_approve: function(button, event)
        {   
            the_rapbs_no = Ext.getCmp('apbyFrm_rapbs_no').getValue();
            if (the_rapbs_no == "")
            {
                Ext.Msg.show(
                {   title :'E R R O R ',
                    msg : 'RAPBS not saved yet',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
            else
            {
                this.Grid.stopEditing();
                Ext.Ajax.request(
                {   method: 'POST',
                    url: "{{ url('/apby/1/705') }}",
                    headers:
                    {   'x-csrf-token': alfalah.apby.sid },
                    params  :
                    {   rapbs_no: the_rapbs_no },
                    success: function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                        else
                        {   
                            Ext.getCmp(tabId+"_FormsTab").destroy();
                            alfalah.apby.apby.DataStore.reload();
                        };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    scope: this
                });
            };
        },




        // Grid_reject: function(button, event)
        // {   
        //     the_rapbs_no = Ext.getCmp('apbyFrm_rapbs_no').getValue();
        //     if (the_rapbs_no == "")
        //     {
        //         Ext.Msg.show(
        //         {   title :'E R R O R ',
        //             msg : 'RAPBS not saved yet',
        //             buttons: Ext.Msg.OK,
        //             icon: Ext.MessageBox.ERROR
        //         });
        //     }
        //     else
        //     {
        //         this.Grid.stopEditing();
        //         Ext.Ajax.request(
        //         {   method: 'POST',
        //             url: "{{ url('/apby/1/707') }}",
        //             headers:
        //             {   'x-csrf-token': alfalah.apby.sid },
        //             params  :
        //             {   rapbs_no: the_rapbs_no },
        //             success: function(response)
        //             {   var the_response = Ext.decode(response.responseText);
        //                 if (the_response.success == false)
        //                 {   Ext.Msg.show(
        //                     {   title :'E R R O R ',
        //                         msg : 'Server Message : '+'\n'+the_response.message,
        //                         buttons: Ext.Msg.OK,
        //                         icon: Ext.MessageBox.ERROR
        //                     });
        //                 }
        //                 else
        //                 {   
        //                     Ext.getCmp(tabId+"_FormsTab").destroy();
        //                     alfalah.apby.apby.DataStore.reload();
        //                 };
        //             },
        //             failure: function()
        //             { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
        //             },
        //             scope: this
        //         });
        //     };
        // }, 

        Gridafteredit : function(the_cell)
        { 
            switch (the_cell.field)
            { 
                case "vol_ajuan":
                // case "jum_ajuan" :
                // case "vol_pakai":
                 case "vol_sisa":
                {   //console.log(the_cell);  
                    var the_data = alfalah.apby.pengajuan.Grid.getSelectionModel().selections.items;
                    // var total_biaya_rapbs = 0;
                    //var total_biaya_ajuan = 0;
                    
                    Ext.each(the_data,
                        function(the_record)
                        {  console.log('ini recordnya woiiii');
                            console.log(the_record);
                            var volume_ajuan    = parseInt(the_record.data.vol_ajuan) ;
                            var volume_sisa     = parseInt(the_record.data.vol_sisa);
                            var volume_asal     = parseInt(the_record.data.volume);
                            var volume_pakai     = parseInt(the_record.data.vol_pakai);
                            // var jumlah_ajuan     = parseInt(the_record.data.jum_ajuan);
                            
                            // console.log('volume_asal');
                            // console.log(volume_asal);
                            // console.log('volume_ajuan');
                            // console.log(volume_ajuan);
                            // console.log('volume_sisa');
                            // console.log(volume_sisa);
                            // console.log('volume_pakai');
                            // console.log(volume_pakai);
                            
                            if ( volume_ajuan > volume_sisa )
                            { 
                                // console.log('woiiiii  kenapa g kluar');
                                Ext.Msg.show(
                                {   title:'E R R O R ',
                                    msg: 'VOLUME AJUAN  >  VOLUME SISA',
                                    buttons: Ext.Msg.OK,
                                    fn: function(buttonId,text)
                                        {   the_record.data.vol_ajuan = 0;
                                            the_cell.grid.getView().refresh();
                                        },
                                    icon: Ext.MessageBox.WARNING
                                });
                            };
                            if (volume_ajuan > volume_asal)
                            {
                                Ext.Msg.show(
                                {   title:'E R R O R ',
                                    msg: 'VOLUME AJUAN  > DARI VOLUME ASAL ',
                                    buttons: Ext.Msg.OK,
                                    fn: function(buttonId,text)
                                        {   the_record.data.vol_ajuan = 0;
                                            the_cell.grid.getView().refresh();
                                        },
                                    icon: Ext.MessageBox.WARNING
                                });
                            };
                            if ((volume_pakai) > (volume_asal)) 
                            {
                                Ext.Msg.show(
                                {   title:'E R R O R ',
                                    msg: 'VOLUME PAKAI > VOLUME ASAL',
                                    buttons: Ext.Msg.OK,
                                    fn: function(buttonId,text)
                                        {   the_record.data.vol_ajuan = 0;
                                            the_cell.grid.getView().refresh();
                                        },
                                    icon: Ext.MessageBox.WARNING
                                });
                            };

                            // total_biaya_rapbs = total_biaya_rapbs + parseInt(the_record.data.jumlah);
                            // total_biaya_ajuan = total_biaya_ajuan + jumlah_ajuan;

                        });
                        // Ext.getCmp('total_biaya_id').setValue(Ext.util.Format.number(total_biaya_rapbs, '0,000'));
                        // Ext.getCmp('total_ajuan_id').setValue(Ext.util.Format.number(total_biaya_ajuan, '0,000'));
                }; 
                break;
            };
        },

        total_biaya_rapbs: function()
        { 
            alfalah.apby.pengajuan.DataStore.on( 'load', function( store, records, options )
            {   console.log('records');
                console.log(records);
                var total_biaya_rapbs = 0;
                // var total_biaya_ajuan = 0;

                Ext.each(records,
                    function(the_record)
                    { 
                        total_biaya_rapbs = total_biaya_rapbs + parseInt(the_record.data.jumlah);
                        // total_biaya_ajuan = total_biaya_ajuan + parseInt(the_record.data.jum_ajuan);

                    });
                // Ext.getCmp('total_biaya_id').setValue(Ext.util.Format.number(total_biaya_rapbs, '0,000'));
            });
        },
    }; // end of public space
}(); // end of app

Ext.onReady(alfalah.apby.initialize, alfalah.apby);
// end of file
</script>
<div>&nbsp;</div>