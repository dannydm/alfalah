<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.budgeting'); // daftar pengajuan
// create application
alfalah.budgeting = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',

        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.budgeting.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.budgeting.Tab);
            this.centerPanel.setActiveTab(this.budgeting.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.budgeting.budgeting= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Searchs;
    this.SearchBtn;
    this.Columns;
    this.Grid;
    this.updateTab;
    this.updateForm;
    this.Records;
    this.DetailRecords;

    this.EastGrid;
    this.EastDataStore;
    this.EastGridPanel;
    this.EastRecords;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        load_1     : true,
        // user_data : '',
        // public methods
        initialize: function(the_records)  
        {  
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
    prepare_component: function()
    { this.region_widht = alfalah.budgeting.centerPanel.container.dom.clientWidht;
        
        this.Columns = [ 
            {   header: "Organisasi ", width : 150,
                dataIndex : 'organisasi_mrapbs_id_name',
                sortable: true,
                hidden : true,
                tooltip:"Jenjang/Bidang/Departemen",
            },
            {   header: "Urusan", width : 300,
                dataIndex : 'urusan_mrapbs_id_name',
                sortable: true,
                tooltip:"Jenjang/Bidang/Departemen",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   result = record.data.urusan_mrapbs_id_name;
                    return alfalah.core.gridColumnWrap(result);
                }
            },
            {   header: "Jumlah Biaya", width : 100,
                dataIndex : 'total_biaya', sortable: true,
                // hidden : true,
                tooltip:"Jumlah total biaya",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   metaData.attr="style = text-align:right;";
                    return Ext.util.Format.number(value, '0,000');
                },
            },
        ];
        
        this.DataStore = alfalah.core.newDataStore(
                "{{ url('/budgeting/1/9') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start });
        this.DataStore.load();
        this.Grid = new Ext.grid.EditorGridPanel(
        {   store:  this.DataStore,
            columns: this.Columns,
            // selModel: cbSelModel,
            enableColLock: false,
            loadMask: true,
            widht : (this.region_widht/2)-50, 
            // anchor: '100%',
            autoScroll  : true,
            // frame: true,
            tbar: [
                '->',
                    'TOTAL ANGGARAN_',
                    new Ext.form.TextField(
                    {   id : 'grand_anggaran_id',
                        // store:this.DataStore,
                        displayField: 'total_anggaran',
                        valueField: 'total_anggaran',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
            ],
            bbar: new Ext.Toolbar(
            {   id : 'bbarCenter',
                // height : 100,
                layout : 'form',
                items:
                [ ],
            }),
        });

        /*************
        * EAST GRID
        *************/
        this.EastDataStore = alfalah.core.newDataStore(
            "{{ url('/budgeting/1/10') }}", false,
            {   s:"init", limit:this.page_limit, start:this.page_start }
        );
        this.EastDataStore.load();
        
            this.EastGridPanel = new Ext.grid.EditorGridPanel( 
            {  store : this.EastDataStore,
               columns : [
                {   header: "ID", width : 50,
                    dataIndex : 'id', sortable: true,
                    tooltip:"ID",
                    hidden: true,
                },
                {   header: "Tahun Pendapatan", width : 150,
                    dataIndex : 'income_master_no', sortable: true,
                    hidden :true,
                    tooltip:"tahun income",
                },
                {   header: "Kode Pendapatan", width : 200,
                    dataIndex : 'income_detail_id', sortable: true,
                    tooltip:"jenis income",
                    hidden :true,
                    readonly:true,
                },
                {   header: "Nama Pendapatan", width : 200,
                    dataIndex : 'income_name', sortable: true,
                    tooltip:"nama jenis pendapatan",
                },
                {   header: "Uraian", width : 150,
                    dataIndex : 'income_uraian', sortable: true,
                    tooltip:"Uraian",
                },
                {   header: "Jumlah Pendapatan", width : 200,
                    dataIndex : 'income_amount', sortable: true,
                    tooltip:"jumlah pendapatan tahun ini",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.income_amount = value;
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
            ],
            tbar: [
                @if (array_key_exists('APPROVE PEMBINA', $MyTasks))
                    {   text:'Approve',
                        tooltip:'approval Usulan Anggaran',
                        iconCls: 'silk-tick',
                        handler : this.Grid_approve_pembina,
                        scope : this
                    },'-',
                    @endif
                    @if (array_key_exists('APPROVE KETUA', $MyTasks))
                    {   text:'Approve Ketua',
                        tooltip:'approval Usulan Anggaran',
                        iconCls: 'silk-tick',
                        handler : this.Grid_approve_ketua,
                        scope : this
                    },
                    @endif
                    '->',
                    'TOTAL PENDAPATAN_', 
                    new Ext.form.TextField(
                    {   id : 'grand_pendapatan_id',
                        // store:this.DataStore,
                        displayField: 'total_pendapatan',
                        valueField: 'total_pendapatan',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),

            ],
            bbar: new Ext.Toolbar(
            {   id : 'bbarEast',
                // height : 100,
                layout : 'form',
                items:
                [ ],
            }),
        });         

        this.detailTab1 = new Ext.Panel(
        {   id: "pendapatanTab_id",
            title:  "P E N D A P A T A N",
            region: 'center',
            layout: 'border',
            
            items: [
            {   region: 'center', 
                xtype: 'container',
                layout: 'fit',
                // css : "background-color: #ffff80;",       
                items:[this.EastGridPanel]
            }],
        });

        this.detailTab2 = new Ext.Panel(
        {   id: "anggaranTab_id",
            title:  "A N G G A R A N",
            region: 'center',
            layout: 'border',
            items: [
            {   region: 'center', 
                xtype: 'container',
                layout: 'fit',
                items:[this.Grid]
            }],
        });
    },

    // build the layout
    build_layout: function()
    {   
        if (Ext.isObject(this.Records))
            {   the_title = "ANGGARAN DAN PENDAPATAN"; }
        else { the_title = "ANGGARAN DAN PENDAPATAN"; };

        this.Tab = new Ext.Panel(
        {   id : tabId+"_budgetingTab",
            jsId : tabId+"_budgetingTab",
            title:  '<span style="background-color: yellow;">'+the_title+'</span>',
            region: 'center',
            closable : true,
            layout: 'border',
            items: [
                {   
                    region: 'center',     // center region is required, no width/height specified
                    split:true,
                    width: 350,
                    minSize: 175,
                    maxSize: 400,
                    layout : 'fit',
                    items: [this.detailTab1]
                },
                {  // title: 'N O T A',
                    region: 'east',     // position for region
                    split:true,
                    width: 650,
                    minSize: 175,
                    maxSize: 400,
                    layout : 'fit',
                    items: [this.detailTab2]
                },
            ],
            bbar: new Ext.Toolbar(
            {  
                layout : 'form',
                items:
                [
                    new Ext.form.TextField( 
                    {   
                        anchor:'30%',
                        margin: '0 30',
                        xtype: 'displayfield',
                        labelStyle: 'width: auto',
                        fieldLabel: 'Surplus/Defisit Perencanaan',
                        allowBlank : false,
                        readOnly : true,
                        style: 'text-align: right',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
          
                 ],
            }),
        });
    },
    // finalize the component and layout drawing

    finalize_comp_and_layout: function()
        { 
            this.DataStore.on( 'load', function( store, records, options )
                {
                    console.log(records);
                    var total_anggaran = 0;
                    Ext.each(records,
                        function(the_record)
                        {   
                            
                            total_anggaran = total_anggaran + parseInt(the_record.data.total_biaya);
                            });


                        Ext.getCmp('grand_anggaran_id').setValue(Ext.util.Format.number(total_anggaran, '0,000'));
                });

            this.EastDataStore.on( 'load', function( store, records, options )
                {
                    console.log(records);
                    var total_pendapatan = 0;
                    Ext.each(records,
                        function(the_record)
                        { 
                            total_pendapatan = total_pendapatan + parseInt(the_record.data.income_amount);
                            });


                        Ext.getCmp('grand_pendapatan_id').setValue(Ext.util.Format.number(total_pendapatan, '0,000'));
                }
            );

        },

        @if (array_key_exists('APPROVE KETUA', $MyTasks)) 
        Grid_approve_ketua : function(button, event) 
        {        var detail_data = alfalah.core.getDetailData(alfalah.pengesahan.pengesahan.Grid.getSelectionModel().selections.items);
            console.log('json_data');
            console.log(detail_data);
            alfalah.core.submitGrid(
                this.DataStore,
                "{{ url('/budgeting/1/706') }}",
                {   'x-csrf-token': alfalah.budgeting.sid },
                    // {   pengajuan_no:the_record  },
                {   json: Ext.encode(detail_data) }
            );

        },
        @endif

        @if (array_key_exists('APPROVE PEMBINA', $MyTasks)) 
        Grid_approve_pembina : function(button, event) 
        {        var detail_app_pembina = alfalah.core.getDetailData(alfalah.budgeting.budgeting.Grid.getSelectionModel().selections.items);
                console.log('json_data');
                console.log(detail_app_pembina);
            alfalah.core.submitGrid(
                this.DataStore,
                "{{ url('/budgeting/1/707') }}",
                {   'x-csrf-token': alfalah.budgeting.sid },
                    // {   pengajuan_no:the_record  },
                {   json: Ext.encode(detail_app_pembina) }
            );

        },
        @endif


    }; // end of public space
}(); // end of app
//onready
Ext.onReady(alfalah.budgeting.initialize, alfalah.budgeting);
// end of file
</script>
<div>&nbsp;</div>