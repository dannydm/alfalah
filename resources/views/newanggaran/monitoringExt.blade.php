<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.monitoring');
// create application
alfalah.monitoring = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',

        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.monitoring.initialize();
            this.monitoringcair.initialize();
            this.monitoringserapan.initialize();
            this.allstatus.initialize();
            this.monitoringcashflow.initialize();

        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.monitoring.Tab);
            this.centerPanel.add(this.monitoringcair.Tab);
            this.centerPanel.add(this.allstatus.Tab);
            this.centerPanel.add(this.monitoringserapan.Tab);
            this.centerPanel.add(this.monitoringcashflow.Tab);
            this.centerPanel.setActiveTab(this.monitoring.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {  },
    }; // end of public space
}(); // end of app
// create application
alfalah.monitoring.monitoring= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.monitoring.centerPanel.container.dom.clientHeight;
            this.Columns = [  
                
                {   header: "RAPBS.No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"RAPBS No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_1 == null ) { metaData.attr = "style = background-color:"; }
                        // not yet approve
                        else if ( record.data.approve_status_3 == 1 ){ metaData.attr = "style = background-color:;"; }
                        // rejected
                        else {  metaData.attr = "style = background-color:;"; };
                        return value;
                    },
                },
                {   header: "Organisasi / Urusan", width : 150,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 150,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 100,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Sumber Dana", width : 150,
                    dataIndex : 'sumberdana_id_name', sortable: true,
                    tooltip:"sumber pendanaan",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Keluaran", width : 150,
                    dataIndex : 'keluaran', sortable: true,
                    tooltip:"keluaran",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Hasil", width : 150,
                    dataIndex : 'hasil', sortable: true,
                    tooltip:"hasil",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Sasaran", width : 150,
                    dataIndex : 'sasaran', sortable: true,
                    tooltip:"sasaran",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },

                {   header: "Coa", width : 150,
                    dataIndex : 'coa_id_name', sortable: true,
                    tooltip:"kode rekening",
                    hidden : true,
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Jumlah Biaya", width : 100,
                    id : 'total_biaya_urusan',
                    hidden:true,
                    dataIndex : 'total_biaya', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Tahun Lalu", width : 100,
                    dataIndex : 'jumlahn', sortable: true,
                    tooltip:"Jumlah Anggaran Tahun Sebelumnya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Tahun Ke N", width : 100,
                    dataIndex : 'jumlahke_n', sortable: true,
                    tooltip:"Jumlah Anggaran Sampai dengan Tahun Lalu",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   value = (record.data.total_biaya);
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "% perubahan", width : 100,
                    dataIndex : 'j_persen', sortable: true,
                    tooltip:"Persentase naik / turun",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        t_pengurang = ((record.data.total_biaya)-(record.data.jumlahn));
                        value = (t_pengurang/(record.data.jumlahn)) * 100;
                        
                        if (value < 100.00) 
                            { metaData.attr = "style = background-color:cyan; "; }
                        else {  metaData.attr = "style = background-color:red; "; };
                        return Ext.util.Format.number(value, '000.00');

                    },
                },
                {   header: "Status Pengesahan", width : 150,
                dataIndex : 'status_pengesahan', sortable: true,
                tooltip:" Status",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {  
                    
                    if (record.data.approve_status_3 == 1) { value = "Telah Disahkan ;" ; }
                    else { value = "Belum Disahkan ;" ; } ;
                    
                    return value;
                },
              },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    hidden :true,
                    tooltip:" Status",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"monitoring Created Date",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"monitoring Last Updated",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Searchs = [
                {   id: 'monitoring_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoring_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoring_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoring_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub. Keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoring_sumberdana',
                    cid: 'sumber_dana_id_name',
                    fieldLabel: 'Dana',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoring_status',
                    cid: 'status',
                    hidden :true,
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_monitoringSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.monitoring_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/monitoring/1/0')}}", false, 
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [ 
                        'TOTAL RAPBY. THN LALU',
                        new Ext.form.TextField(
                        {   id : 'total_thn_lalu_id',
                        // store:this.DataStore,
                        displayField: 'total_thn_lalu',
                        valueField: 'total_thn_lalu',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),'-',

                    'PERSENTASE + -',
                    new Ext.form.TextField(
                    {   id : 't_persen_id',
                        // store:this.DataStore,
                        displayField: 't_persen',
                        valueField: 't_persen',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                    '->',
                    'TOTAL RAPBY. THN INI',
                    new Ext.form.TextField(
                    {   id : 'grand_total_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),

                    ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_monitoringGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_monitoringPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_monitoringPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_monitoringGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
                listeners : { scope : this,
                    'cellclick': function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent)
                    {
                        console.log('cell click');
                        // console.log(iColIdx);
                        // console.log(iView);
                        // console.log(iCellEl);
                        // console.log(iColIdx);
                        // console.log(iStore);
                        // console.log(iRowEl);
                        // console.log(iRowIdx);
                        // console.log(iEvent);
                        if (iColIdx == 0 ) // collom pertama
                        {   the_record = this.Grid.getSelectionModel().selection.record.data.rapbs_no
                            console.log(the_record);
                            // this.southdatastore.baseParam = {new param in here};
                            alfalah.monitoring.monitoring.SouthDataStore.baseParams = {   task: alfalah.monitoring.task,
                                act: alfalah.monitoring.act,
                                a:2, b:0, s:"form", rapbs_no : the_record,
                                limit:this.page_limit, start:this.page_start 
                            };                 
                            alfalah.monitoring.monitoring.SouthDataStore.reload();
                        };
                    }
                    ,
                    'rowselect' : function (a,b,c)
                    {   console.log('listener grid select');
                        console.log(a);
                        console.log(b);
                        console.log(c);

                        // this.south_datastore.baseParam = {new param in here};
                        // this.south_datastrore.reload();
                        }
                }
            });
            /**
            * SOUTH-GRID
            */
            this.EastDataStore = alfalah.core.newDataStore(
                "{{ url('/monitoring/1/11') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.EastDataStore.load();
            this.EastGrid =   new Ext.grid.EditorGridPanel( 
            {  store : this.EastDataStore,
                columns: [ 
                    {   header: "ID", width : 50,
                        dataIndex : 'organisasi_mrapbs_id', sortable: true,
                        hidden :true,
                        tooltip:"ID",
                    },
                    {   header: "Nama Organisasi", width : 250,
                        dataIndex : 'organisasi_mrapbs_id_name', sortable: true,
                        tooltip:"Doc.No",
                    },
                    {   header: "Jumlah Usulan Anggaran", width : 150,
                        dataIndex : 'total_biaya', sortable: true,
                        tooltip:"jumlah anggaran yang telah diusulkan",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                            {   metaData.attr="style = text-align:right;";
                                return Ext.util.Format.number(value, '0,000');
                            },
                    },
                    {   header: "Jumlah Ajuan Anggaran", width : 150,
                        hidden :true,
                        dataIndex : 'total_biaya_ajuan', sortable: true,
                        tooltip:"jumlah anggaran yang telah diusulkan",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                            {   metaData.attr="style = text-align:right;";
                                return Ext.util.Format.number(value, '0,000');
                            },
                    },
                    {   header: "Jumlah Pencairan", width : 100,
                        hidden :true,
                        dataIndex : 'rapbs_name', sortable: true,
                        tooltip:"Doc.No",
                    },
                ],

                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [  ],
                bbar: new Ext.PagingToolbar(
                {  
                    id : tabId+'_eastGridBBar',
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_eastPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 50,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_eastPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_eastGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                
                }),

            });

            this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/monitoring/1/10') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.SouthDataStore.load();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.SouthDataStore,
                columns: [ 
                    {   header: "ID", width : 50,
                        dataIndex : 'rapbs_id', sortable: true,
                        tooltip:"ID",
                        hidden: true,
                    },
                    {   header: "No", width : 100,
                        dataIndex : 'rapbs_no', sortable: true,
                        tooltip:"Doc.No",
                    },
                    {   header: "Pos.Belanja", width : 150,
                        dataIndex : 'coa_id', sortable: true,
                        tooltip:"Pos Belanja",
                        readonly:true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>['+value+']</b>'+record.data.coa_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                    },
                    {   header: "Name", width : 200,
                        dataIndex : 'coa_name', sortable: true,
                        readonly:true,
                        hidden :true,
                        tooltip:"Pos Belanja Name",
                    },
                    {   header: "Uraian", width : 150,
                        dataIndex : 'uraian', sortable: true,
                        tooltip:"Uraian",
                        readonly:true,
                    },
                    {   header: "Volume", width : 100,
                        dataIndex : 'volume', sortable: true,
                        tooltip:"Volume",
                        readonly:true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                            {   record.data.volume = value;
                                value1 = Ext.util.Format.number(value, '0,000');
                                result = '<b>'+value1+'</b>'+'&ensp;'+record.data.satuan;
                                return alfalah.core.gridColumnWrap(result);
                            }
                    },
                    {   header: "Satuan", width : 100,
                        dataIndex : 'satuan', sortable: true,
                        tooltip:"Satuan",
                        hidden :true,
                        readonly:true,
                    },
                    {   header: "Tarif", width : 100,
                        dataIndex : 'tarif', sortable: true,
                        tooltip:"Tarif",
                        readonly:true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   record.data.tarif = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Jumlah", width : 100,
                        dataIndex : 'jumlah', sortable: true,
                        readonly:true,
                        tooltip:"Jumlah",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   
                            value = parseInt(record.data.volume) * parseInt(record.data.tarif);
                            record.data.jumlah = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Ber-ulang", width : 60,
                        dataIndex : 'berulang', sortable: true,
                        hidden :true,
                        tooltip:"variabel pembagi anggaran yg berulang",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Status", width : 100,
                        dataIndex : 'status_name', sortable: true,
                        readonly:true,
                        hidden :true,   
                        tooltip:"Status",
                    },
                ],
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [  ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_newsGridBBar',
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_newsPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_newsPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_newsGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });

    // *****************************************
        
    },
    // build the layout
    build_layout: function()
    {   this.Tab = new Ext.Panel(
        {   id : tabId+"_monitoringTab",
            jsId : tabId+"_monitoringTab",
            title:  "MONITORING USULAN ANGGARAN",
            region: 'center',
            layout: 'border',
            items: [
            {   region: 'center',     // center region is required, no width/height specified
                xtype: 'container',
                layout: 'fit',
                items:[this.Grid]
            },
            {   title: 'Parameters',
                region: 'east',     // position for region
                split:true,
                width: 200,
                minSize: 175,
                maxSize: 400,
                collapsible: true,
                layout : 'accordion',
                items:[
                {   //title: 'S E A R C H',
                    labelWidth: 50,
                    defaultType: 'textfield',
                    xtype: 'form',
                    frame: true,
                    autoScroll : true,
                    items : this.Searchs,
                    tbar: [
                        {   text:'Search',
                            tooltip:'Search',
                            iconCls: 'silk-zoom',
                            handler : this.monitoring_search_handler,
                            scope : this,
                        }]
                }]
            },
            {
                region: 'south',
                title: 'DETAIL ANGGARAN',
                split: true,
                height : this.region_height/2,
                collapsible: true,
                margins: '0 0 0 0',
                layout : 'border',
                items :[ 
                {   
                    region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.SouthGrid]
                },
                {   region: 'east',     // position for region
                    // store : this.EastDatastore,
                    title: 'INFORMASI',
                    split:true,
                    width: 550,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[this.EastGrid]
                }]
            }]
        });
    },
    // finalize the component and layout drawing
    finalize_comp_and_layout: function()
    {
        this.DataStore.on( 'load', function( store, records, options )
        {
            console.log(records);
            var total_data = 0;
            var total_thn_lalu=0;
            var t_persentase=0;

            Ext.each(records,
                function(the_record)
                {  // console.log('cari record');
                   // console.log(the_record.data.jumlahn);
                    total_data = total_data + parseInt(the_record.data.total_biaya);
                    total_thn_lalu = total_thn_lalu + parseInt(the_record.data.jumlahn);

                    // t_pengurang = ((record.data.total_biaya)-(record.data.jumlahn));
                    // value = (t_pengurang/(record.data.jumlahn)) * 100;
                    t_persentase = total_data-total_thn_lalu;
                    t_persen = (t_persentase/total_thn_lalu)*100;
                });

            console.log("total persentase = "+t_persen);
            console.log("total tahun lalu = "+total_thn_lalu);
    //                console.log(Ext.getCmp('grand_total_id').setValue(total_data));
            Ext.getCmp('grand_total_id').setValue(Ext.util.Format.number(total_data, '0,000'));
            Ext.getCmp('total_thn_lalu_id').setValue(Ext.util.Format.number(total_thn_lalu, '0,000'));
            Ext.getCmp('t_persen_id').setValue(Ext.util.Format.number(t_persen, '000.00'));

        });
        // this.DataStore.load();

    },
    // monitoring search button
    monitoring_search_handler : function(button, event)
    {   var the_search = true;
        if ( this.DataStore.getModifiedRecords().length > 0 )
        {   Ext.Msg.show(
                {   title:'W A R N I N G ',
                    msg: ' Modified Data Found, Do you want to save it before search process ? ',
                    buttons: Ext.Msg.YESNO,
                    fn: function(buttonId, text)
                        {   if (buttonId =='yes')
                            {   Ext.getCmp(tabId+'_monitoringSaveBtn').handler.call();
                                the_search = false;
                            } else the_search = true;
                        },
                    icon: Ext.MessageBox.WARNING
                    });
        };

        if (the_search == true) // no modification records flag then we can go to search
        {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
            this.DataStore.baseParams = Ext.apply( the_parameter,
                {   task: alfalah.monitoring.task,
                    act: alfalah.monitoring.act,
                    a:2, b:0, s:"form",
                    limit:this.page_limit, start:this.page_start });
            this.DataStore.reload(); 
        };
    },
}; // end of public space
}(); // end of app
// create application
alfalah.monitoring.monitoringcair = function()
{   // do NOT access DOM from here; elements don't exist yet
// execute at the first time
// private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn_center;
    this.Searchs_center;

    this.SouthGrid;
    this.SouthDataStore;
    this.SearchBtn_south;
    this.Searchs_south;
    this.Records_south;
    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.monitoring.centerPanel.container.dom.clientHeight;
            this.Columns = [
                {   header: "Nomor Pengajuan", width : 100,
                    dataIndex : 'pengajuan_no', sortable: true,
                    tooltip:"Nomor Uang muka",
                },
                {   header: "Keterangan", width : 150,
                    dataIndex : 'pengajuan_keterangan_id',
                    sortable: true,
                    tooltip:"Keterangan Pengajuan",
                },
                {   header: "Organisasi / Urusan", width : 200,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    hidden : true, 
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 200,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    hidden : true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Jumlah Pengajuan", width : 100,
                    // hidden:true,
                    dataIndex : 'total_biaya', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Status Release", width : 100,
                    dataIndex : 'status_pengajuan_id',
                    sortable: true,
                    tooltip:"Status Pengajuan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_1 == 0) { value = 'Not Yet release'; }
                        // not yet approve
                        else if  ( record.data.approve_status_1 == 1){ value = 'Release'; };
                        
                        return value;
                    }
                },
                {   header: "Approve Kabid", width : 120,
                    dataIndex : 'status_approve_2',
                    sortable: true,
                    tooltip:"Status Pengajuan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_2 == null) { value = 'Not Yet Approve'; }
                        // not yet approve
                        else if  ( record.data.approve_status_2 == 1){ value = 'Approve by Kabid'; };
                        
                        return value;
                    }
                },
                {   header: "Approve Ketua", width : 120,
                    dataIndex : 'status_approve_3',
                    sortable: true,
                    tooltip:"Status Pengajuan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_3 == null) { value = 'Not Yet Approve'; }
                        // not yet approve
                        else if  ( record.data.approve_status_3 == 1){ value = 'Approve by Ketua'; };
                        
                        return value;
                    }
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"monitoringuser Created Date",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"monitoringuser Last Updated",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }

            ];
            this.Searchs_center = [
                {   id: 'monitoringum_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringum_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringum_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringum_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub. Keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn_center = new Ext.Button(
                {   id : tabId+"_monitoringumSearchBtn_center",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.monitoringum_search_handler_center,
                    scope : this
                });
                this.Searchs_south = [
                {   id: 'monitoringcair_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringcair_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringcair_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringcair_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub. Keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringcair_status',
                    cid: 'approve_status_3',
                    fieldLabel: 'By Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['label','key'],
                        data : [["ALL", 0], ["RECEIVED", 1]]
                    }),
                    displayField:'label',
                    valueField :'key',    
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100
                    // value: 'Active'
                },
            ];
            this.SearchBtn_south = new Ext.Button(
                {   id : tabId+"_monitoringumSearchBtn_south",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.monitoring_search_handler_south,
                    scope : this
                });

            /**
                * MAIN-GRID
            */
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/monitoring/2/0') }}", false, 
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    '->',
                    'TOTAL PENGAJUAN',
                    new Ext.form.TextField(
                    {   id : 'grand_pengajuanum_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_monitoringumGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_monitoringumPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_monitoringumPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_monitoringumGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn_center.handler.call(this.SearchBtn_center.scope);
                                }
                            }
                        }),
                        ' records at a time tes'
                    ]
                }),
            });

            /**
                * SOUTH-GRID
            */
            this.SouthDataStore = alfalah.core.newDataStore( 
                "{{ url('/monitoring/2/1') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   /*id : tabId+'_monitoringcairSouthGrid',
                jsId : tabId+'_monitoringcairSouthGrid',*/
                store:  this.SouthDataStore,
                columns: [ 
                    {   header: "Nomor Uang Muka", width : 100,
                        dataIndex : 'uangmuka_no', sortable: true,
                        tooltip:"Nomor Uang muka",
                    },
                    {   header: "RAPBS.No", width : 100,
                        dataIndex : 'rapbs_no', sortable: true,
                        tooltip:"RAPBS No",
                    },
                    {   header: "Keterangan", width : 150,
                        dataIndex : 'uangmuka_keterangan_id',
                        sortable: true,
                        tooltip:"Keterangan Pengajuan",
                    },
                    {   header: "Organisasi / Urusan", width : 200,
                        dataIndex : 'organisasi_mrapbs_id_name',
                        sortable: true,
                        hidden : true,
                        tooltip:"Jenjang/Bidang/Departemen",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                            return alfalah.core.gridColumnWrap(result);
                        }
                    },
                    {   header: "Program / Kegiatan", width : 200,
                        dataIndex : 'program_mrapbs_id_name', sortable: true,
                        tooltip:"Nama Program",
                        hidden : true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                            return alfalah.core.gridColumnWrap(result);
                        }
                    },
                    {   header: "Sub Kegiatan", width : 200,
                        dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                        tooltip:"nama sub kegiatan",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   return alfalah.core.gridColumnWrap(value);
                        }
                    },
                    {   header: "Jumlah Pengajuan", width : 100,
                        // hidden:true,
                        dataIndex : 'total_biaya', sortable: true,
                        tooltip:"Jumlah total biaya",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                            return Ext.util.Format.number(value, '0,000');
                        },
                    },
                    {   
                        header: "Jatuh Tempo", width : 150,
                        dataIndex : 'uangmuka_due_date',
                        sortable: true,
                        tooltip:"Status pengajuan uang muka",
                        renderer : function (value, metaData, record, rowIndex, colIndex, store)
                        {  
                                today = new Date().format("Y-m-d H:i:s");
                                start_date = record.data.bln_pengajuan;
                                end_date   = record.data.uangmuka_due_date ;
                                get_seven_day_before =  new Date(record.data.uangmuka_due_date); 
                                get_seven_day_before.setDate( new Date(get_seven_day_before.getDate()) - 7);
                                c = get_seven_day_before.format("Y-m-d H:i:s");

                                console.log(record.data.approve_status_3); 

                                if ((record.data.approve_status_3 == 1) && (today > end_date))
                                {  //console.log('hihihiih');
                                    metaData.attr = "style = background-color:lime;" } 
                                else if ((record.data.approve_status_3 == 0)  && (today >= end_date))
                                {  //console.log('hahahahha');
                                    metaData.attr = "style = background-color:red;" }
                                else 
                                { //console.log('heheheheh');
                                    metaData.attr = "style = background-color:yellow;"; };

                                    return Ext.util.Format.date(value, "l, d F, Y");
                          //  return value;
                        }
                    },
                    {   
                        header: "Status UangMuka", width : 100,
                        dataIndex : 'approve_status_1',
                        sortable: true,
                        tooltip:"Status pengajuan uang muka",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   //
                            if ( record.data.approve_status_1 == 0)  { value = 'Not Yet Approve' }
                            else  { value = 'Approve By Kabid'; };
                            return value;
                        }
                    },
                    {   id : 'status_pencairan_id',
                        header: "Status Pencairan", width : 150,
                        dataIndex : 'approve_status_3',
                        sortable: true,
                        tooltip:"Status Pencairan anggaran",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   //
                            if (( record.data.approve_status_2 == 1) && ( record.data.approve_status_3 == 0 )) { value = 'Please Contact Finance'; }
                            // not yet approve
                            else if  (( record.data.approve_status_2 == 1) && ( record.data.approve_status_3 == 1 )){ value = 'Received'; }
                            else { value = 'Not Yet Approved By Finance'; };
                            
                            return value;
                        }
                    },
                    {   header: "Created", width : 50,
                        dataIndex : 'created', sortable: true,
                        tooltip:"monitoringuser Created Date",
                        css : "background-color: #DCFFDE;",
                        hidden :true,
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    },
                    {   header: "Updated", width : 50,
                        dataIndex : 'modified_date', sortable: true,
                        tooltip:"monitoringuser Last Updated",
                        css : "background-color: #DCFFDE;",
                        hidden :true,
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    }
                ],

                // selModel: cbSelModel,
                //selModel: new Ext.grid.RowSelectionModel(),
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [
                    '->',
                    'TOTAL PENCAIRAN',
                    new Ext.form.TextField(
                    {   id : 'total_pencairan_id',
                        // store:this.DataStore,
                        displayField: 'total_pencairan',
                        valueField: 'total_pencairan',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_monitoringcairSouthGridBBar',
                    store: this.SouthDataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_monitoringcairSouthGridPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_monitoringcairSouthGridPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_monitoringcairSouthGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn_south.handler.call(this.SearchBtn_south.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_monitoringcairTab", 
                jsId : tabId+"_monitoringcairTab",
                title:  "MONITORING STATUS PENGAJUAN",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    title: 'MONITORING PENGAJUAN UANG MUKA',
                    split: true,
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'ToolBox',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    { //  title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs_center,
                        tbar: [
                        {   text:'Search',
                            tooltip:'Search',
                            iconCls: 'silk-zoom',
                            handler : this.monitoring_search_handler_center,
                            scope : this,
                        }]
                        
                    },
                    { title : 'Setting'
                    }]
                },
                {// lazily created panel (xtype:'panel' is default)
                    region: 'south',
                    title: 'MONITORING STATUS PENCAIRAN',
                    split: true,
                    height : this.region_height/2,
                    layout : 'border',
                    collapsible: true,
                    margins: '0 0 0 0',
                    items :[ 
                    {   
                        region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.SouthGrid]
                    },
                    {   region: 'east',     // position for region
                        title: 'ToolBox',
                        split:true,
                        width: 200,
                        minSize: 175,
                        maxSize: 400,
                        collapsible: true,
                        layout : 'accordion',
                        items:[
                        {   title: 'S E A R C H',
                            labelWidth: 50,
                            defaultType: 'textfield',
                            xtype: 'form',
                            frame: true,
                            autoScroll : true,
                            items : this.Searchs_south, 
                            tbar: [
                                {   text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.monitoring_search_handler_south,
                                    scope : this,
                                }] 
                            
                        },
                        { title : 'Setting'
                        }]
                    }]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
            this.DataStore.on( 'load', function( store, records, options )
                {
                    console.log(records);
                    var total_pengajuan = 0;
                    Ext.each(records,
                        function(the_record)
                        {  // console.log('cari record');
                          //  console.log(the_record.data.approve_status_3);
                           
                            total_pengajuan = total_pengajuan + parseInt(the_record.data.total_biaya);
                         });
                        Ext.getCmp('grand_pengajuanum_id').setValue(Ext.util.Format.number(total_pengajuan, '0,000'));
                });

                this.SouthDataStore.on( 'load', function( store, records, options )
                {
                    console.log(records);
                    var total_pencairan = 0;
                    Ext.each(records,
                        function(the_record)
                        {  // console.log('cari record');
                          //  console.log(the_record.data.approve_status_3);
                            // if (the_record.data.approve_status_3==1)
                            total_pencairan = total_pencairan + parseInt(the_record.data.total_biaya);
                         });
                        Ext.getCmp('total_pencairan_id').setValue(Ext.util.Format.number(total_pencairan, '0,000'));
                });
        },

        // monitoringcair search button
        monitoring_search_handler_center : function(button, event)
        {   var the_search_center = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_monitoringcairSaveBtn').handler.call();
                                    the_search_center = false;
                                } else the_search_center = true;
                            },
                        icon: Ext.MessageBox.WARNING
                        });
            };

            if (the_search_center == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs_center);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.monitoring.task,
                        act: alfalah.monitoring.act,
                        s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },

        monitoring_search_handler_south : function(button, event)
        {   var the_search_south = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_monitoringcairSaveBtn').handler.call();
                                    the_search_south = false;
                                } else the_search_south = true;
                            },
                        icon: Ext.MessageBox.WARNING
                        });
            };

            if (the_search_south == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs_south);
                this.SouthDataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.monitoring.task,
                        act: alfalah.monitoring.act,
                        s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.SouthDataStore.reload();
            };
        },        
    }; // end of public space
}(); // end of app
alfalah.monitoring.allstatus= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {         var expander = new Ext.ux.grid.RowExpander({
                        expandOnDblClick : true,  
                    });
 
            this.Columns = [
              
                {   header: "RAPBS.No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"RAPBS No",
                },
                {   header: "Pengajuan.No", width : 100,
                    dataIndex : 'pengajuan_no', sortable: true,
                    tooltip:"PENGAJUAN No",
                },
                {   header: "Uang Muka .No", width : 100,
                    dataIndex : 'uangmuka_no', sortable: true,
                    tooltip:"UANG MUKA No",
                },
                {   header: "Organisasi / Urusan", width : 200,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 200,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Jumlah Tahun Ke N", width : 100,
                    dataIndex : 'jumlahke_n', sortable: true,
                    tooltip:"Jumlah Anggaran Sampai dengan Tahun Lalu",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   value = (record.data.total_biaya);
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Pengajuan", width : 100,
                    dataIndex : 'total_biaya_ajuan', sortable: true,
                    tooltip:"Jumlah pada waktu pengajuan biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   value = (record.data.total_biaya_ajuan);
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Uang Muka", width : 100,
                    dataIndex : 'jumlah_uangmuka', sortable: true,
                    tooltip:"jumlah pada waktu pengajuan uang muka",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   value = (record.data.jumlah_uangmuka);
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Tgl Pengesahan Ketua", width : 100,
                    dataIndex : 'ketua_approve_date', sortable: true,
                    tooltip:"tanggal pengesahan oleh ketua yayasan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return Ext.util.Format.date(value, "l, d F, Y"); },

                },
                {   header: "Tgl Pengajuan", width : 100,
                    dataIndex : 'pengajuan_approve_date', sortable: true,
                    tooltip:"tanggal pengajuan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return Ext.util.Format.date(value, "l, d F, Y"); },

                },
                {   header: "Tgl Realisasi User", width : 100,
                    dataIndex : 'pencairan_approve_date', sortable: true,
                    tooltip:"tanggal entry realisasi user",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return Ext.util.Format.date(value, "l, d F, Y"); },
                },
                {   header: "Approval Realisasi Kabid", width : 100,
                    dataIndex : 'pencairan_approve_date', sortable: true,
                    tooltip:"tanggal aproval realislasi",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return Ext.util.Format.date(value, "l, d F, Y"); },
                },
                {   header: "Tgl Pencairan", width : 100,
                    dataIndex : 'pencairan_approve_date', sortable: true,
                    tooltip:"tanggal pencairan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return Ext.util.Format.date(value, "l, d F, Y"); },
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    hidden:true,
                    tooltip:"allstatus Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    hidden:true,
                    tooltip:"allstatus Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                // {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'allstatus_organisasi',
                    cid: 'organisasi_mrapbs_id_name',
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'allstatus_urusan',
                    cid: "urusan_mrapbs_id_name",
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'allstatus_kegiatan',
                    cid: 'kegiatan_mrapbs_id_name',
                    fieldLabel: 'Kegiatan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'allstatus_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'allstatus_status',
                    cid: 'status',
                    hidden :true,
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },

            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_allstatusSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.allstatus_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/monitoring/1/15')}}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.monitoring.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    // '->',
                    // '_T O T A L_',
                    new Ext.form.TextField(
                    {   id : 'grand_total_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        hidden : true,
                        readOnly : true,
                        // style: "text-align: right",
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        scope : this,
                    }),
                        ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_allstatusGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_allstatusPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_allstatusPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_allstatusGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_allstatusSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_allstatusTab",
                jsId : tabId+"_allstatusTab",
                title:  "ALL STATUS",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.allstatus_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        //  {},

        // Grid_grand_total : function(the_cell)
        {
            this.DataStore.on( 'load', function( store, records, options )
            {
                console.log(records);
                var total_data = 0;
                Ext.each(records,
                    function(the_record)
                    { 
                        total_data = total_data + parseInt(the_record.data.total_biaya);
                    });

                console.log("total jumlah = "+total_data);
                Ext.getCmp('grand_total_id').setValue(Ext.util.Format.number(total_data, '0,000'));
            });
        },

        
        Gridafteredit : function(the_cell)
        { switch (the_cell.field)
            {   case "volume":
                case "tarif":
                {
                    var total_biaya = 0;
                    Ext.each(the_data,
                    function(the_record)
                    { 
                        total_biaya = total_biaya + parseInt(the_record.data.jumlah);
                    });
            
                    Ext.getCmp('total_biaya_id').setValue(Ext.util.Format.number(total_nota, '0,000'));

                }; 
                break; 
            };
        },


        // allstatus search button
        allstatus_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_allstatusSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.monitoring.task,
                        act: alfalah.monitoring.act,
                        a:3, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.monitoring.monitoringserapan= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()


            {  
                this.SerapanDs = alfalah.core.newDataStore(
                    "{{ url('/monitoring/2/2') }}", false,
                    {   s:"form", limit:this.page_limit, start:this.page_start });
                    this.SerapanDs.load();

                this.region_height = alfalah.monitoring.centerPanel.container.dom.clientHeight;
                this.Columns = [
                   // store : this.SerapanDs,
                {   header: "Organisasi ", width : 150,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    // hidden : true,
                    tooltip:"Jenjang/Bidang/Departemen",
                },
                {   header: "Urusan", width : 200,
                    dataIndex : 'urusan_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Anggaran thn lalu", width : 110,
                    dataIndex : 'jumlahn', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya tahun lalu",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Usulan Thn ini", width : 110,
                    dataIndex : 'total_biaya', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        value = (record.data.total_biaya) - (record.data.total_perubahan);
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Perubahan Anggaran", width : 120,
                    dataIndex : 'total_perubahan', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total perubahan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Total Anggaran", width : 120,
                    dataIndex : 'total_anggaran', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total perubahan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        value = (record.data.total_biaya)
                        return Ext.util.Format.number(value, '0,000');
                    },
                },

                {   header: "% Thn Ini Vs Thn Lalu", width : 120,
                    dataIndex : 'j_persen', sortable: true,
                    tooltip:"Persentase naik / turun",
                    // css : "style = content-align:right;",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        // metaData.attr="style = text-align:right;";
                        t_pengurang = ((record.data.total_biaya)-(record.data.jumlahn));
                        value = (t_pengurang/(record.data.jumlahn)) * 100;
                        if ((value > 0.00) && (value < 100.00)) 
                            { metaData.attr = "style = background-color:cyan;bg-image:arrow-up.gif;"; }
                          //{ metaData.css = '<class = "custom-first-last" >' }
                        else if (value < 0.00)
                        { metaData.attr = "style = background-color:lime;background-image:none;  text-align:right; "; }
                        else if (value = isFinite(value) ? value : 0.0) 
                        { metaData.attr = "style = background-color:magenta;background-image:none;  text-align:right;";}
                        else {  metaData.attr = "style = background-color:red;background-image:none;"; };
                        return Ext.util.Format.number(value, '000.00');

                    },
                },
                {   header: "Total Pencairan", width : 110,
                    dataIndex : 'total_pencairan', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total uang yang telah dicairkan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        console.log('cari uang cair');
                       //console.log(record.data.realisasi_biaya);
                    //   value = record.data.realisasi_biaya;
                        return Ext.util.Format.number(value, '0,000');
                    },
                },

                {   header: "Total Realisasi", width : 110,
                    dataIndex : 'total_realisasi', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total uang muka",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        console.log('cari uangmuka');
                       //console.log(record.data.realisasi_biaya);
                    //   value = record.data.realisasi_biaya;
                        return Ext.util.Format.number(value, '0,000');
                    },
                },

                {   header: "% serapan ", width : 100,
                    dataIndex : 's_persen', sortable: true,
                    tooltip:"presentase serapan anggaran",
                    // css : "style = content-align:right;", 
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        // metaData.attr="style = text-align:right;";
                        value = ((record.data.total_realisasi)/(record.data.total_biaya))*100;
                        // value = s_persen;
                        if ((value >= 0.00) && (value <= 80.00)) 
                            { metaData.attr = "style = background-color:lime;background-image:none;"; }
                        else if (value > 80.00)
                        { metaData.attr = "style = background-color:yellow;background-image:none;"; }
                        else if (value = isFinite(value) ? value : 0.0) 
                        { metaData.attr = "style = background-color:magenta;background-image:none;";}
                        else {  metaData.attr = "style = background-color:red;background-image:none;"; };
                        return Ext.util.Format.number(value, '000.00');

                        // theResult = isFinite(theResult) && theResult || 0;

                    },
                },
                ];
                this.Records = Ext.data.Record.create(
                [   {name: 'id', type: 'integer'},
                    // {name: 'modified_date', type: 'date'},
                ]);
                this.Searchs = [
                    {   id: 'monitoring_serapan_organisasi',
                        cid: "organisasi_mrapbs_id_name",
                        fieldLabel: 'Organisasi',
                        labelSeparator : '',
                        xtype: 'textfield', 
                        width : 120
                    },
                    {   id: 'monitoring_serapan_urusan',
                        cid: 'urusan_mrapbs_id_name',
                        fieldLabel: 'Urusan',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                ];
                this.SearchBtn = new Ext.Button(
                {   id : tabId+"_monitoringserapanSearchBtn",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.monitoring_serapan_search_handler,
                    scope : this
                });
                // this.DataStore = alfalah.core.newDataStore( 
                //     "{{url('/pencairan/2/0')}}", false,
                //     {   s:"form", limit:this.page_limit, start:this.page_start }
                // );
                // this.DataStore.load();
                this.Grid = new Ext.grid.EditorGridPanel(
                {   
                    store:  this.SerapanDs,
                    stripeRows : true,
                    columns: this.Columns,
                    enableColLock: false,
                    loadMask: true,
                    height : (this.region_height/2)-50,
                    anchor: '100%',
                    autoScroll  : true,
                    frame: true,
                    tbar: [
                        {   print_type : "pdf",
                            text:'Print PDF', 
                            tooltip:'Print to PDF',
                            hidden :true,
                            iconCls: 'silk-page-white-acrobat',
                            // handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            handler : this.Grid_pdf,
                            scope : this
                        },
                        {   print_type : "xls",
                            text:'Print Excell',
                            hidden : true,
                            tooltip:'Print to Excell SpreadSheet',
                            iconCls: 'silk-page-white-excel',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },'-',
                        '->',
                    '_T O T A L_',
                    new Ext.form.TextField(
                        {   id : 'grand_pencairan_id',
                            // store:this.DataStore,
                            displayField: 'total_data',
                            valueField: 'total_data',
                            allowBlank : false,
                            readOnly : true,
                            style: "text-align: right; background-color: #ffff80; background-image:none;",
                            mode: 'local',
                            forceSelection: true,
                            triggerAction: 'all',
                            selectOnFocus: true,
                            // listeners :
                            // {   "beforeedit" : this.Grid_grand_total },
                            // scope : this
                        }),

                    ],
                    bbar: new Ext.PagingToolbar(
                    {   id : tabId+'_monitoringserapanGridBBar',
                        store: this.DataStore,
                        pageSize: this.page_limit,
                        displayInfo: true,
                        emptyMsg: 'No data found',
                        items : [
                            '-',
                            'Displayed : ',
                            new Ext.form.ComboBox(
                            {   id : tabId+'_monitoringserapanPageCombo',
                                store: new Ext.data.SimpleStore(
                                            {   fields: ['value'],
                                                data : [[50],[75],[100],[125],[150]]
                                            }),
                                displayField:'value',
                                valueField :'value',
                                value : 75,
                                editable: false,
                                mode: 'local',
                                triggerAction: 'all',
                                selectOnFocus:true,
                                hiddenName: 'pagesize',
                                width:50,
                                listeners : { scope : this,
                                    'select' : function (a,b,c)
                                    {   this.page_limit = Ext.get(tabId+'_monitoringserapanPageCombo').getValue();
                                        bbar = Ext.getCmp(tabId+'_monitoringserapanGridBBar');
                                        bbar.pageSize = parseInt(this.page_limit);
                                        this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                        this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    }
                                }
                            }),
                            ' records at a time'
                        ]
                    })
                    ,
                    listeners : { scope : this,
                            'cellclick': function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent)
                            {
                                console.log('cell click');
                                if (iColIdx == 1 ) // kolom pertama
                                {   the_record = this.Grid.getSelectionModel().selection.record.data.urusan_mrapbs_id;
                                    
                                    console.log(the_record);
                                    // this.southdatastore.baseParam = {new param in here};
                                    this.SouthDataStore.removeAll();
                                    alfalah.monitoring.monitoringserapan.SouthDataStore.baseParams = {
                                           
                                        task: alfalah.monitoring.task,
                                        act: alfalah.monitoring.act,
                                        a:2, b:0, s:"form", urusan_mrapbs_id : the_record,
                                        limit:this.page_limit, start:this.page_start 
                                    };
                                    // alfalah.monitoring.monitoringserapan.SouthGrid.getView().refresh();         
                                    alfalah.monitoring.monitoringserapan.SouthDataStore.reload();
                                };
                            },
                            'rowselect' : function (a,b,c)
                            {   console.log('listener grid select');
                                console.log(a);
                                console.log(b);
                                console.log(c);
                            }
                        }
                });
                /**
                    * SOUTH-GRID
                    */
                this.SouthDataStore = alfalah.core.newDataStore(
                    "{{ url('/monitoring/2/3') }}", false,
                    {   s:"init", limit:this.page_limit, start:this.page_start }
                );
                // this.SouthDataStore.load();
                this.SouthGrid = new Ext.grid.EditorGridPanel(
                {   store:  this.SouthDataStore,
                    columns: [ 
                    {   header: "Uang Muka No", width : 100,
                        dataIndex : 'uangmuka_no', sortable: true,
                        hidden : true,
                        tooltip:"Uang Muka No",
                    },
                    {   header: "Pengajuan No", width : 100,
                        dataIndex : 'pengajuan_no', sortable: true,
                        hidden : true,
                        tooltip:"RAPBS No",
                    },
                    {   header: "RAPBS.No", width : 130,
                        dataIndex : 'rapbs_no', sortable: true,
                        tooltip:"RAPBS No",
                    },

                    {   header: "Uraian", width : 150,
                        dataIndex : 'uraian', sortable: true,
                        hidden : true,
                        tooltip:"Uraian",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Urusan", width : 150,
                        dataIndex : 'urusan_mrapbs_id_name', sortable: true,
                        hidden : true,
                        tooltip:"Nama Urusan",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   result = record.data.urusan_mrapbs_id_name;
                            return alfalah.core.gridColumnWrap(result);
                        }
                    },
                    {   header: "Sub Kegiatan", width : 300,
                        dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                        tooltip:"nama sub kegiatan",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                            {   return alfalah.core.gridColumnWrap(value);
                            }
                     },
                    {   header: "Anggaran Thn Lalu", width : 150,
                        dataIndex : 'jumlahn', sortable: true,
                        tooltip:"Jumlah total biaya",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                            return Ext.util.Format.number(value, '0,000');
                        },
                    },
                    {   header: "Anggaran Tahun ini", width : 120,
                        dataIndex : 'total_biaya', sortable: true,
                        tooltip:"Jumlah total biaya",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                            return Ext.util.Format.number(value, '0,000');
                        },
                    },
                    {   header: "% Thn ini Vs Thn Lalu", width : 150,
                        id : 'total_biaya_urusan',
                        dataIndex : 'persen_all', sortable: true,
                        tooltip:"Jumlah total biaya",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {  
                            // metaData.attr="style = text-align:right;";
                            t_pengurang = ((record.data.total_biaya)-(record.data.jumlahn));
                            value = (t_pengurang/(record.data.jumlahn)) * 100;
                            if ((value > 0.00) && (value < 100.00))
                            { metaData.attr = "style = background-color:cyan;background-image:none;"; } 
                              //  { metaData.attr = "style = background-color:cyan; background-image: url(\js\ext3\examples\shared\icons\arrow-up.gif) !important; background-repeat: no-repeat;"; }
                              
                            else if (value < 0.00)
                            { metaData.attr = "style = background-color:lime;background-image:none;"; }
                            else {  metaData.attr = "style = background-color:red;background-image:none;"; };
                            return Ext.util.Format.number(value, '000.00');
                        },
                    },
                    {   header: "Total Realisasi", width : 110,
                        dataIndex : 'total_realisasi', sortable: true,
                        // hidden : true,
                        tooltip:"Jumlah total uang muka",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                      
                            return Ext.util.Format.number(value, '0,000');
                        },
                    },
                    {   header: "% Serapan", width : 100,
                        id : 'total_biaya_urusan',
                        dataIndex : 'total', sortable: true,
                        tooltip:"Jumlah total biaya",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {  
                            // metaData.attr="style = text-align:right;";
                            s_pengurang = ((record.data.total_realisasi)/(record.data.total_biaya))*100;
                            value = s_pengurang;
                            if ((value >= 0.00) && (value <= 80.00)) 
                                { metaData.attr = "style = background-color:lime;background-image:none;"; }
                            else if (value > 80.00)
                            { metaData.attr = "style = background-color:yellow;background-image:none;"; }
                            else {  metaData.attr = "style = background-color:red;background-image:none;"; };
                            return Ext.util.Format.number(value, '000.00');

                        },
                    }
                ],
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                stripeRows : true,
                frame: true,
                tbar: [  ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_southGridBBar',
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_southPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_southPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_southGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });

        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_monitoringserapanTab",
                jsId : tabId+"_monitoringserapanTab",
                title:  "SERAPAN ANGGARAN",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {
                    region: 'south',
                    title: 'SERAPAN ANGGARAN BERDASAR KEGIATAN',
                    split: true,
                    height : this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    layout : 'border',
                    items :[ 
                    {   
                        region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.SouthGrid]
                    }]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
    //             this.DataStore.on( 'load', function( store, records, options )
    //             {
    //                 console.log(records);
    //                 var total_data = 0;
    //                 Ext.each(records,
    //                     function(the_record)
    //                     { 
    //                         total_data = total_data + parseInt(the_record.data.total_biaya);
    //                     });

    //                 console.log("total jumlah = "+total_data);
    //  //                console.log(Ext.getCmp('grand_total_id').setValue(total_data));
    //                 Ext.getCmp('grand_pencairan_id').setValue(Ext.util.Format.number(total_data, '0,000'));
    //             });
            // this.DataStore.load();
      
        },

        // pencairan search button
        monitoringserapan_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_pencairanSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };
            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.monitoring.task,
                        act: alfalah.monitoring.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.monitoring.monitoringcashflow = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.o_pass = '';
    this.n_pass = '';
    this.MyPr_Grid;
    this.MyPr_Columns;
    this.MyPr_Records;
    this.MyPr_DataStore;

    this.AngGridPanel;
    this.Ang_DataStore;
    this.anggaranTab;
    this.Revenue_grid;
    // private functions

    // public space
    return {
        // execute at the very last time
        //public variable
        refresh_time : ((1000*60)*15), // 1000 = 1 second
        centerPanel : 0,
        page_limit : 75,
        page_start : 0,
        tabId : '{{ $TABID }}',
        task : '',
        act : '',
        // public methods
        initialize: function()
        {   Ext.chart.Chart.CHART_URL = "asset('/js/ext3/resources/charts.swf') }}";
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.tools = [
            {   id:'gear',
               handler: function()
                   {   Ext.Msg.alert('Message', 'The Settings tool was clicked.'); }
            },
            {   id:'close',
                handler: function(e, target, panel)
                    {   panel.ownerCt.remove(panel, true);  }
            }];

            this.centerPanel = Ext.getCmp(this.tabId);

            this.Forecasting_Ds = alfalah.core.newDataStore(
                "{{ url('/monitoring/2/5') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start });
                this.Forecasting_Ds.load();

            
                

            this.FsGridPanel = new Ext.grid.EditorGridPanel( 
            {  store : this.Forecasting_Ds,
                stripeRows : true,
                columns : [            
                {   header: "Kode Rek", width : 80,
                    dataIndex : 'coa_id',
                    sortable: true,
                    // hidden : true,
                    tooltip:"kode rekening",
                },
                {   header: "Nama Rekening", width : 150,
                    dataIndex : 'coa_name',
                    sortable: true,
                    tooltip:"Nama Rekening",
                },
                {   header: "Agustus", width : 70,
                    dataIndex : 'agustus_cost', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya yang telah dicairkan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        current_date = 8;
                        get_month = record.data.fs_month
                        //console.log(current_date);
                        if (current_date ==  get_month){
                            value = record.data.monthly_cost;
                        } else { value = 0 };

                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "September", width : 70,
                    dataIndex : 'september_cost', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya yang telah dicairkan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        current_date = 9;
                        get_month = record.data.fs_month
                        //console.log(current_date);
                        if (current_date ==  get_month){
                            value = record.data.monthly_cost;
                        } else { value = 0 };
                        
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Oktober", width : 70,
                    dataIndex : 'oktober_cost', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya yang telah dicairkan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        current_date = 10;
                        get_month = record.data.fs_month
                        //console.log(current_date);
                        if (current_date ==  get_month){
                            value = record.data.monthly_cost;
                        } else { value = 0 };
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "November", width : 70,
                    dataIndex : 'november_cost', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya yang telah dicairkan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        current_date = 11;
                        get_month = record.data.fs_month
                        //console.log(current_date);
                        if (current_date ==  get_month){
                            value = record.data.monthly_cost;
                        } else { value = 0 };
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Desember", width : 70,
                    dataIndex : 'desember_cost', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya yang telah dicairkan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        current_date = 12;
                        get_month = record.data.fs_month
                        //console.log(current_date);
                        if (current_date ==  get_month){
                            value = record.data.monthly_cost;
                        } else { value = 0 };
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                ],
                tbar: [
                    {   text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : this.FsGridPanel,
                        scope : this
                    },
                ]
            });
        },

        // build the layout
        build_layout: function()
        { 
            this.Tab = new Ext.Panel(
            {   region: 'center',
                title : 'CASHFLOW',
               // autoScroll  : true,
                items: [
                    {   xtype:'portal',
                        region:'center',
                        margins:'1 1 1 1',
                        autoScroll  : true,
                        items:[
                        {   columnWidth:.50,
                            style:'padding:10px 0 0px 19px ',
                            items:[
                            {   title: 'CASHFLOW',
                                region : 'center', 
                                // autoScroll  : true,
                                // width:30,
                                // height:635,
                                items: [
                                    {   
                                        region: 'center',     // center region is required, no width/height specified
                                        width: 570,
                                        height : 570,
                                        autoScroll  : true,
                                        layout : 'fit',
                                        items: [{
                                                xtype: 'container',
                                               // autoLoad:"{{ url('/monitoring/2/4') }}",
                                                contentEl: 'cost_revenue',
                                                //tplWriteMode : 'overwrite',
                                                
                                            },]

                                    }
                                ]
                            },  
                            ]
                        },
                        {   columnWidth:.49,
                            style:'padding:10px 0 0px 10px ',  
                            items:[
                            {   title: 'FORECASTING REPORT',
                                region : 'east', 
                                autoScroll  : true,
                                width:50,
                                height:620,
                                items: [
                                    {   
                                        region: 'center',     // center region is required, no width/height specified
                                        // split:true,
                                        width: 610,
                                        height : 570,
                                        layout : 'fit',
                                        items : [this.FsGridPanel]
                                    }
                                ]}
                            ]
                        },
                        ]
                    },
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {  // this.centerPanel.add(this.Tab);
           // alfalah.core.viewport.doLayout();
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.monitoring.initialize, alfalah.monitoring);
// end of file
</script>

<body>
<div id="cost_revenue">
    <table class="x-data-table">
        <tr>
            <h2 style="text-align: center; ">CASHFLOW REPORT</h2>
            <button  style="position: right: 0;" class= "babale">Expand All</button>
            <button  style="position: right: 0;" class= "basambunyi">Collapse All</button>
        </tr>       
        
        <thead>
            <tr>
                <th style="text-align: center;" width="20%"><h3>Kode Rekening</h3></th>
                <th style="text-align: center;" width="50%"><h3>Keterangan</h3></th>
                <th style="text-align: center;" width="30%"><h3>Jumlah (Rp)</h3></th>
            </tr>
        </thead>
        <tbody>
            <tbody class="labels">
           
                <tr>
                    <td colspan="3">
                        <label for="income">P E N D A P A T A N</label>
                        <input type="checkbox" name="income" id="income" data-toggle="toggle">
                    </td>
                </tr>
        </tbody>
        <tbody class="hide">
            
                <tr>
                    @php ($total_income = 0)
                    @foreach($INCOME as $item)
                    <tr>
                        <td style="background-color:#80ff80">{{$item->income_detail_id}}</td>
                        <td style="background-color:#80ff80">{{$item->income_name}}</td>
                        <td style="text-align: right; background-color:#80ff80">{{number_format($item->income_amount, 0, ',' , '.')}}&nbsp;</td>
                    </tr>
                    @php ($total_income = $total_income + $item->income_amount)
                    @endforeach
                </tr>
            </tbody>
            <tr>
                <td colspan="2" style = "background-color:#c0c0c0; font-weight: bold">T O T A L</td>
                <td style="text-align: right; font-weight: bold; background-color:#c0c0c0">{{number_format($total_income, 0, ',' , '.')}} &nbsp;</td>
            </tr>
            <tbody class="labels">
                <tr>
                    <td colspan="3">
                        <label for="cost">REALISASI BIAYA</label>
                        <input type="checkbox" name="cost" id="cost" data-toggle="toggle">
                    </td>
                </tr>
            </tbody>
            <tbody class="hide">
               @php ($total_cost = 0)
                @foreach($COST as $biaya)
                <tr>
                    <td style="background-color:#80ff80">{{$biaya->coa_id}}</td>
                    <td style="background-color:#80ff80">{{$biaya->coa_name}}</td>
                    <td style="text-align: right; background-color:#80ff80">{{number_format($biaya->total_cost, 0, ',' , '.')}} &nbsp;</td>
                </tr>
                @php ($total_cost = $total_cost + $biaya->total_cost)
                @endforeach
            </tbody>
            <tr>
                <td colspan="2" style = "background-color:#c0c0c0; font-weight: bold">T O T A L</td>
                <td style="text-align: right; font-weight: bold; background-color:#c0c0c0">{{number_format($total_cost, 0, ',' , '.')}} &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style = "background-color:#80ff80;"><h3>SALDO</h3></td>
                <td style="text-align: right; font-weight: bold; background-color:#80ff80">{{number_format($total_income-$total_cost, 0, ',' , '.')}} &nbsp;</td>
            </tr>            		
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function() {
        $('[data-toggle="toggle"]').change(function(){
            $(this).parents().next('.hide').toggle();
        });

        var $childRows = $('.hide');
        $(this).find('button.basambunyi').click(function() {
            $childRows.hide();
        });

        $(this).find('button.babale').click(function() {
            $childRows.show();
        });
    });
</script>

<style>
    .x-data-table {
        border-collapse: collapse;
        border-spacing: 0;
        min-width: 98%
    }

    .x-data-table th {
        text-align: left;
        background-color: #80ffff;
        
    }

    .x-data-table td,
    th {
        padding: .5em;
        border: 2px solid #ffffff;
    }

    .labels tr td {
        background-color: #2cc16a;
        border: 2px solid #00ff40;
        font-weight: bold;
        color: #fff;
    }

    .label tr td label {
        display: block;
        
    }

    [data-toggle="toggle"] {
	display: none;
    }
   
</style>
</body>

