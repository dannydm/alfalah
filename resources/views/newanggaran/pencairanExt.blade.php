<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.pencairan'); // daftar pengajuan

// create application
alfalah.pencairan = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',
 
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.pencairan.initialize();
            this.monitoringbiaya.initialize();

        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.pencairan.Tab);
            this.centerPanel.add(this.monitoringbiaya.Tab);
            this.centerPanel.setActiveTab(this.pencairan.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.pencairan.pencairan= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
            {   this.region_height = alfalah.pencairan.centerPanel.container.dom.clientHeight;
                this.Columns = [
                    {   header: "Nomor Uang Muka", width : 100,
                        dataIndex : 'uangmuka_no', sortable: true,
                        tooltip:"Nomor Uang muka",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                            {   //approve
                            if ( record.data.approve_status_3 == 1) { metaData.attr = "style = background-color:lime"; }
                            // not yet approve
                            else if ( record.data.approve_status_3 == 0){ metaData.attr = "style = background-color:yellow;"; }
                            // rejected
                            else {  metaData.attr = "style = background-color:red;"; };
                            return value;
                        }
                    },
                    {   header: "Keterangan", width : 150,
                        dataIndex : 'uangmuka_keterangan_id',
                        sortable: true,
                        tooltip:"Keterangan Pengajuan",
                    },
                    {   header: "Organisasi / Urusan", width : 150,
                        dataIndex : 'organisasi_mrapbs_id_name',
                        sortable: true,
                        tooltip:"Jenjang/Bidang/Departemen",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                            return alfalah.core.gridColumnWrap(result);
                        }
                    },
                    {   header: "Program / Kegiatan", width : 250,
                        dataIndex : 'program_mrapbs_id_name', sortable: true,
                        tooltip:"Nama Program",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                            return alfalah.core.gridColumnWrap(result);
                        }
                    },
                    {   header: "Sub Kegiatan", width : 250,
                        dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                        tooltip:"Nama Program",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   
                            return alfalah.core.gridColumnWrap(value);
                        }
                    },

                    {   header: "Jumlah Biaya", width : 100,
                        dataIndex : 'total_biaya', sortable: true,
                        tooltip:"Jumlah total biaya",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                            return Ext.util.Format.number(value, '0,000');
                        },
                    },
                    {   header: "Pengajuan by", width : 150,
                        dataIndex : 'username', sortable: true,
                        tooltip:"Diajukan oleh",
                    },
                    {   header: "Status", width : 50,
                        dataIndex : 'status', sortable: true,
                        hidden : true,
                        tooltip:" Status",
                    },
                    {   header: "Created", width : 50,
                        dataIndex : 'created', sortable: true,
                        tooltip:"pencairan Created Date",
                        css : "background-color: #DCFFDE;",
                        hidden :true,
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    },
                    {   header: "Updated", width : 50,
                        dataIndex : 'modified_date', sortable: true,
                        tooltip:"pencairan Last Updated",
                        css : "background-color: #DCFFDE;",
                        hidden :true,
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    }
                ];
                this.Records = Ext.data.Record.create(
                [   {name: 'id', type: 'integer'},
                    // {name: 'modified_date', type: 'date'},
                ]);
                this.Searchs = [
                    {   id: 'pencairan_organisasi',
                        cid: "organisasi_mrapbs_id_name",
                        fieldLabel: 'Organisasi',
                        labelSeparator : '',
                        xtype: 'textfield', 
                        width : 120
                    },
                    {   id: 'pencairan_urusan',
                        cid: 'urusan_mrapbs_id_name',
                        fieldLabel: 'Urusan',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'pencairan_kegiatan',
                        cid: 'kegiatan_mrapbs_id_name',
                        fieldLabel: 'Kegiatan',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'pencairan_sub_kegiatan',
                        cid: 'sub_kegiatan_mrapbs_id',
                        fieldLabel: 'Sub.Keg',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },

                    {   id: 'pencairan_status',
                        cid: 'status',
                        fieldLabel: 'Status',
                        hidden : true,
                        labelSeparator : '',
                        xtype : 'combo',
                        store : new Ext.data.SimpleStore(
                        {   fields: ['status'],
                            data : [[''], ['Active'], ['Inactive']]
                        }),
                        displayField:'status',
                        valueField :'status',
                        mode : 'local',
                        triggerAction: 'all',
                        selectOnFocus:true,
                        editable: false,
                        width : 100,
                        value: 'Active'
                    },
                ];
                this.SearchBtn = new Ext.Button(
                {   id : tabId+"_pencairanSearchBtn",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.pencairan_search_handler,
                    scope : this
                });
                this.DataStore = alfalah.core.newDataStore(
                    "{{url('/pencairan/1/0')}}", false,
                    {   s:"form", limit:this.page_limit, start:this.page_start }
                );
                // this.DataStore.load();
                this.Grid = new Ext.grid.EditorGridPanel(
                {   
                    store:  this.DataStore,
                    columns: this.Columns,
                    enableColLock: false,
                    loadMask: true,
                    selectOnFocus:true,
                    height : (this.region_height/2)-50,
                    anchor: '100%',
                    autoScroll  : true,
                    frame: true,
                    tbar: [
                        {   id :'print_kwitansi_id',
                            print_type : "pdf",
                            text:'Print Kwitansi', 
                            tooltip:'Print to PDF',
                            iconCls: 'silk-page-white-acrobat',
                            // handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            handler : this.Grid_pdf,
                            scope : this
                        },
                        {   print_type : "xls",
                            text:'Print Excell',
                            hidden : true,
                            tooltip:'Print to Excell SpreadSheet',
                            iconCls: 'silk-page-white-excel',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },'-',
                        {   text:'Pencairan',
                            tooltip:'pencairan Anggaran',
                            // xType : 'dataview',
                            style:'background-color:lime',
                            hidden :true,
                            iconCls: 'silk-tick',
                            handler : this.Grid_approve,
                            scope : this
                        },'-',
                        {   text:'Pencairan',
                            tooltip:'pencairan Anggaran',
                            // xType : 'dataview',
                            style:'background-color:lime',
                            iconCls: 'silk-tick',
                            handler : this.Grid_payment,
                            scope : this
                        },'-',
                        {   print_type : "pdf",
                            text:'Cetak Kwitansi', 
                            tooltip:'Print to PDF',
                            hidden :true,
                            iconCls: 'silk-page-white-acrobat',
                            // handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            handler : this.Grid_pdf_kwitansi,
                            scope : this
                        },

                        '->',
                    '_T O T A L_',
                    new Ext.form.TextField(
                    {   id : 'grand_total_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        // listeners :
                        // {   "beforeedit" : this.Grid_grand_total },
                        // scope : this
                    }),

                    ],
                    bbar: new Ext.PagingToolbar(
                    {   id : tabId+'_pencairanGridBBar',
                        store: this.DataStore,
                        pageSize: this.page_limit,
                        displayInfo: true,
                        selectOnFocus:true,
                        emptyMsg: 'No data found',
                        items : [
                            '-',
                            'Displayed : ',
                            new Ext.form.ComboBox(
                            {   id : tabId+'_pencairanPageCombo',
                                store: new Ext.data.SimpleStore(
                                            {   fields: ['value'],
                                                data : [[50],[75],[100],[125],[150]]
                                            }),
                                displayField:'value',
                                valueField :'value',
                                value : 75,
                                editable: false,
                                mode: 'local',
                                triggerAction: 'all',
                                selectOnFocus:true,
                                hiddenName: 'pagesize',
                                width:50,
                                listeners : { scope : this,
                                    'select' : function (a,b,c)
                                    {   this.page_limit = Ext.get(tabId+'_pencairanPageCombo').getValue();
                                        bbar = Ext.getCmp(tabId+'_pencairanGridBBar');
                                        bbar.pageSize = parseInt(this.page_limit);
                                        this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                        this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    }
                                }
                            }),
                            ' records at a time'
                        ]
                    }),
                    listeners : { scope : this,
                            'cellclick': function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent)
                            {
                                console.log('cell click');
                                if (iColIdx == 0 ) // kolom pertama
                                {   the_record = this.Grid.getSelectionModel().selection.record.data.uangmuka_no
                                    console.log(the_record);
                                    // this.southdatastore.baseParam = {new param in here};
                                    alfalah.pencairan.pencairan.SouthDataStore.baseParams = {   task: alfalah.pencairan.task,
                                        act: alfalah.pencairan.act,
                                        a:2, b:0, s:"form", uangmuka_no : the_record,
                                        limit:this.page_limit, start:this.page_start 
                                    };
                                    console.log('alfalah.pencairan.pencairan.SouthDataStore')    
                                    console.log(alfalah.pencairan.pencairan.SouthDataStore)             
                                    alfalah.pencairan.pencairan.SouthDataStore.reload();
                                };
                            },
                            'rowselect' : function (a,b,c)
                            {   console.log('listener grid select');
                                console.log(a);
                                console.log(b);
                                console.log(c);
                            }
                        }
                });
                /**
                    * SOUTH-GRID
                    */
                this.SouthDataStore = alfalah.core.newDataStore(
                    "{{ url('/pencairan/1/10') }}", false,
                    {   s:"init", limit:this.page_limit, start:this.page_start }
                );
                // this.SouthDataStore.load();
                this.SouthGrid = new Ext.grid.EditorGridPanel(
                {   store:  this.SouthDataStore,
                    columns: [ 
                    {   header: "Uang Muka No", width : 100,
                        dataIndex : 'uangmuka_no', sortable: true,
                        readonly : true,
                        tooltip:"Uang Muka No",
                    },
                    {   header: "Pengajuan No", width : 100,
                        dataIndex : 'pengajuan_no', sortable: true,
                        readonly : true,
                        tooltip:"RAPBS No",
                    },
                    {   header: "RAPBS.No", width : 100,
                        dataIndex : 'rapbs_no', sortable: true,
                        readonly : true,
                        tooltip:"RAPBS No",
                    },
                    {   header: "Kode Rekening", width : 100,
                        dataIndex : 'coa_id', sortable: true,
                        tooltip:"Kode rekening",
                        readonly:true,
                    },
                    {   header: "Name", width : 200,
                        dataIndex : 'coa_name', sortable: true,
                        tooltip:"Pos Belanja Name",
                        readonly : true,
                    },
                    {   header: "Uraian", width : 150,
                        dataIndex : 'uraian', sortable: true,
                        tooltip:"Uraian",
                        readonly : true,
                    },
                    {   header: "Volume", width : 100,
                        dataIndex : 'vol_ajuan', sortable: true,
                        tooltip:"Volume",
                        readonly : true,
                        // editor : new Ext.form.TextField({allowBlank: false}),
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   
                            record.data.vol_ajuan = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Satuan", width : 100,
                        dataIndex : 'satuan', sortable: true,
                        tooltip:"Satuan",
                        readonly : true,
                        // editor : new Ext.form.TextField({allowBlank: false}),
                    },

                    {   header: "Tarif", width : 100,
                        dataIndex : 'tarif', sortable: true,
                        tooltip:"Tarif",
                        readonly : true,
                        // editor : new Ext.form.TextField({allowBlank: false}),
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   
                            record.data.tarif = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Jumlah Cair", width : 100,
                        id: 'jumlah_id',
                        dataIndex : 'jum_ajuan', sortable: true,
                        tooltip:"Jumlah",
                        readonly : true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   
                            record.data.jum_ajuan = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Ber-ulang", width : 60,
                        dataIndex : 'berulang', sortable: true,
                        hidden :true,
                        tooltip:"variabel pembagi anggaran yg berulang",
                        readonly : true,
                        // editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Realisasi", width : 100,
                        id : 'realisasi_biaya',
                        hidden :true,
                        dataIndex : 'biaya_realisasi', sortable: true,
                        tooltip:"Jumlah realisasi biaya",
                        
                        editor : new Ext.form.TextField({allowBlank: false}),
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                            return Ext.util.Format.number(value, '0,000');
                        },
                    },
                    // {   header: "Status", width : 50,
                    //     dataIndex : 'status', sortable: true,
                    //     tooltip:" Status",
                    // },
                    // {   header: "Created", width : 50,
                    //     dataIndex : 'created', sortable: true,
                    //     tooltip:"pencairan Created Date",
                    //     css : "background-color: #DCFFDE;",
                    //     renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    // },
                    {   header: "Updated", width : 50,
                        dataIndex : 'modified_date', sortable: true,
                        tooltip:"pencairan Last Updated",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    }
                ],
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Save',
                        id : 'btnSave',
                        tooltip:'Save Record',
                        hidden:true,
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        scope : this,
                    },

                  ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_southGridBBar',
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_southPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_southPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_southGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });

        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_pencairanTab",
                jsId : tabId+"_pencairanTab",
                title:  "REKAP PENCAIRAN",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.pencairan_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {
                    region: 'south',
                    title: 'DETAIL UANG MUKA',
                    split: true,
                    height : this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    layout : 'border',
                    items :[ 
                    {   
                        region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.SouthGrid]
                    }]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
            this.DataStore.on( 'load', function( store, records, options )
            {
                console.log(records);
                var total_data = 0;
                Ext.each(records,
                    function(the_record)
                    { 
                        total_data = total_data + parseInt(the_record.data.total_biaya);
                    });

                console.log("total jumlah = "+total_data);
 //                console.log(Ext.getCmp('grand_total_id').setValue(total_data));
                Ext.getCmp('grand_total_id').setValue(Ext.util.Format.number(total_data, '0,000'));
            });
            // this.DataStore.load();
          
        },

        Grid_approve : function(button, event) 
        {
         //   var the_record = this.Grid.getSelectionModel().selection;
            var the_record = alfalah.core.getDetailData(alfalah.pencairan.pencairan.Grid.getSelectionModel().selection.record.data);
           
            if ( the_record )
            {   Ext.Msg.show(
                    {   title :'A P P R O V A L',
                        msg : 'APPROVE ?',
                        width:250,
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                        {   if (buttonId =='yes')
                            {  
                                alfalah.core.submitGrid(
                                    alfalah.pencairan.pencairan.DataStore,
                                            "{{ url('/pencairan/1/100') }}",
                                        {   'x-csrf-token': alfalah.pencairan.sid }, 
                                        {   uangmuka_no: alfalah.pencairan.pencairan.Grid.getSelectionModel().selection.record.data.uangmuka_no 
                                            
                                        }
                                );
                            } else
                            { this.DataStore.reload(); };
                        },
                    icon: Ext.MessageBox.WARNING
                    }
                );
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },

        Grid_payment: function(button, event)
        {   this.Grid.stopEditing();
            var selection_data = alfalah.pencairan.pencairan.Grid.getSelectionModel().selection;
            if (selection_data)
            {   var app_data = selection_data.record.data.approve_status_3;
                var data = selection_data.record.data.uangmuka_no;

                if (app_data == 0) {
                    
                    Ext.MessageBox.show({
                        title: 'Verification',
                        msg: 'Pilih Jenis Pencairan.<br /><br /><select id="jenis_bayar"><option value="1">Tunai</option><option value="2">Transfer</option></select><br />Catatan : <br /><input type="text" id="cttn_id"/>',
                        buttons: Ext.MessageBox.OKCANCEL,
                        width : 350,
                      //  activeItem : true,
                        fn: function (buttonId, jenis_byr, catatan, opt) {
                            if (buttonId =='ok' )
                                {   
                                    Ext.Ajax.request(
                                    {   method: 'POST',
                                        url: "{{ url('/pencairan/1/100') }}",
                                        headers: {   'x-csrf-token': alfalah.pencairan.sid },
                                        params: {   uangmuka_no: data,
                                                    jenis_byr : Ext.get('jenis_bayar').getValue(), 
                                                    catatan : Ext.get('cttn_id').getValue(),
                                                },
                                        success: function(response)
                                        {   var the_response = Ext.decode(response.responseText);
                                            if (the_response.success == false)
                                            {   Ext.Msg.show(
                                                {   title :'E R R O R ',
                                                    msg : 'Server Message : '+'\n'+the_response.message,
                                                    buttons: Ext.Msg.OK,
                                                    icon: Ext.MessageBox.ERROR
                                                });
                                            }
                                            else
                                            {   //alfalah.pencairan.pencairan.DataStore.reload(); 
                                                Ext.Msg.alert("Print Kwitansi : ", "Harap Menunggu", 
                                                function (btn) { if (btn == "ok") 
                                                { 
                                                    Ext.getCmp('print_kwitansi_id').el.dom.click();
                                                  //  alfalah.pencairan.pencairan.DataStore.reload();
                                                }});
                                            };
                                        },
                                        failure: function()
                                        {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                                        },
                                    });
                                       
                            }
                            
                        }
                    }
                ) 
                    
                }
                else
                {  
                    Ext.Msg.show(
                    {   title   :'I N F O ',
                        msg     : 'Data Has Been Verification',
                        buttons : Ext.Msg.OK,
                        icon    : Ext.MessageBox.INFO,
                        width   : 200
                    });
                   

                }    
            }
            else
            {   Ext.Msg.show(
                {   title   :'I N F O ',
                    msg     : 'No Selected Record ! ',
                    buttons : Ext.Msg.OK,
                    icon    : Ext.MessageBox.INFO,
                    width   : 200
                });
            };
        },

        Grid_pdf : function(button, event)
        {     var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                console.log(the_record)
                if (the_record.approve_status_2==1)
                {   
                    alfalah.core.printOut(button, event, this.DataStore.proxy.url, 

                    {   s: "form",
                        uangmuka_no : the_record.uangmuka_no });
                        alfalah.pencairan.pencairan.DataStore.reload();
                }
                else
                {
                    Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'Data must be approved ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                    
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };
        },

        // Grid_pdf_kwitansi : function(button, event)
        // {     var the_record = this.Grid.getSelectionModel().selection;
        //     if ( the_record )
        //     {   the_record = the_record.record.data;
               
        //         alfalah.core.printOut(button, event, "{{url('/pencairan/1/0')}}", 
        //             {   s: "form",
        //                 uangmuka_no : the_record.uangmuka_no });
        //     }
        //     else
        //     {   Ext.Msg.show(
        //             {   title:'I N F O ',
        //                 msg: 'No Data Selected ! ',
        //                 buttons: Ext.Msg.OK,
        //                 icon: Ext.MessageBox.INFO
        //             });
                
        //     };
        // },

        // pencairan search button
        pencairan_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_pencairanSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.pencairan.task,
                        act: alfalah.pencairan.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.pencairan.monitoringbiaya= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
            {   this.region_height = alfalah.pencairan.centerPanel.container.dom.clientHeight;
                this.Columns = [

                    {   header: "Organisasi ", width : 150,
                        dataIndex : 'organisasi_mrapbs_id_name',
                        sortable: true,
                        tooltip:"Jenjang/Bidang/Departemen",
                    },
                    {   header: "Urusan", width : 300,
                        dataIndex : 'urusan_mrapbs_id_name',
                        sortable: true,
                        tooltip:"Jenjang/Bidang/Departemen",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   result = record.data.urusan_mrapbs_id_name;
                            return alfalah.core.gridColumnWrap(result);
                        }
                    },
                    {   header: "Jumlah Pencairan", width : 100,
                        dataIndex : 'total_pencairan', sortable: true,
                        // hidden : true,
                        tooltip:"Jumlah total biaya",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                            return Ext.util.Format.number(value, '0,000');
                        },
                    },
                ];
                this.Records = Ext.data.Record.create(
                [   {name: 'id', type: 'integer'},
                    // {name: 'modified_date', type: 'date'},
                ]);
                this.Searchs = [
                    {   id: 'pencairan_monitoring_organisasi',
                        cid: "organisasi_mrapbs_id_name",
                        fieldLabel: 'Organisasi',
                        labelSeparator : '',
                        xtype: 'textfield', 
                        width : 120
                    },
                    {   id: 'pencairan_monitoring_urusan',
                        cid: 'urusan_mrapbs_id_name',
                        fieldLabel: 'Urusan',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                ];
                this.SearchBtn = new Ext.Button(
                {   id : tabId+"_pencairanmonitoringSearchBtn",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.pencairan_search_handler,
                    scope : this
                });
                this.DataStore = alfalah.core.newDataStore( 
                    "{{url('/pencairan/2/0')}}", false,
                    {   s:"form", limit:this.page_limit, start:this.page_start }
                );
                this.DataStore.load();

                this.Grid = new Ext.grid.EditorGridPanel(
                {   
                    store:  this.DataStore,
                    columns: this.Columns,
                    enableColLock: false,
                    loadMask: true,
                    height : (this.region_height/2)-50,
                    anchor: '100%',
                    autoScroll  : true,
                    frame: true,
                    tbar: [
                        {   print_type : "pdf",
                            text:'Print PDF', 
                            tooltip:'Print to PDF',
                            hidden :true,
                            iconCls: 'silk-page-white-acrobat',
                            // handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            handler : this.Grid_pdf,
                            scope : this
                        },
                        {   print_type : "xls",
                            text:'Print Excell',
                            hidden : true,
                            tooltip:'Print to Excell SpreadSheet',
                            iconCls: 'silk-page-white-excel',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },'-',
                        '->',
                    '_T O T A L_',
                    new Ext.form.TextField(
                    {   id : 'grand_pencairan_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        // listeners :
                        // {   "beforeedit" : this.Grid_grand_total },
                        // scope : this
                    }),

                    ],
                    bbar: new Ext.PagingToolbar(
                    {   id : tabId+'_pencairanmonitoringGridBBar',
                        store: this.DataStore,
                        pageSize: this.page_limit,
                        displayInfo: true,
                        emptyMsg: 'No data found',
                        items : [
                            '-',
                            'Displayed : ',
                            new Ext.form.ComboBox(
                            {   id : tabId+'_pencairanmonitoringPageCombo',
                                store: new Ext.data.SimpleStore(
                                            {   fields: ['value'],
                                                data : [[50],[75],[100],[125],[150]]
                                            }),
                                displayField:'value',
                                valueField :'value',
                                value : 75,
                                editable: false,
                                mode: 'local',
                                triggerAction: 'all',
                                selectOnFocus:true,
                                hiddenName: 'pagesize',
                                width:50,
                                listeners : { scope : this,
                                    'select' : function (a,b,c)
                                    {   this.page_limit = Ext.get(tabId+'_pencairanmonitoringPageCombo').getValue();
                                        bbar = Ext.getCmp(tabId+'_pencairanmonitoringGridBBar');
                                        bbar.pageSize = parseInt(this.page_limit);
                                        this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                        this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    }
                                }
                            }),
                            ' records at a time'
                        ]
                    })
                    ,
                    listeners : { scope : this,
                            'cellclick': function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent)
                            {
                                console.log('cell click');
                                if (iColIdx == 0 ) // kolom pertama
                                {   the_record = this.Grid.getSelectionModel().selection.record.data.urusan_mrapbs_id
                                    console.log(the_record);
                                    // this.southdatastore.baseParam = {new param in here};
                                    alfalah.pencairan.monitoringbiaya.SouthDataStore.baseParams = {   task: alfalah.pencairan.task,
                                        act: alfalah.pencairan.act,
                                        a:2, b:0, s:"form", urusan_mrapbs_id : the_record,
                                        limit:this.page_limit, start:this.page_start 
                                    };
                                    console.log('alfalah.pencairan.monitoringbiaya.SouthDataStore')    
                                    console.log(alfalah.pencairan.monitoringbiaya.SouthDataStore)             
                                    alfalah.pencairan.monitoringbiaya.SouthDataStore.reload();
                                };
                            },
                            'rowselect' : function (a,b,c)
                            {   console.log('listener grid select');
                                console.log(a);
                                console.log(b);
                                console.log(c);
                            }
                        }
                });
                /**
                    * SOUTH-GRID
                    */
                this.SouthDataStore = alfalah.core.newDataStore(
                    "{{ url('/pencairan/2/1') }}", false, 
                    {   s:"init", limit:this.page_limit, start:this.page_start }
                );
                // this.SouthDataStore.load();
                this.SouthGrid = new Ext.grid.EditorGridPanel(
                {   store:  this.SouthDataStore,
                    columns: [ 
                    {   header: "Uang Muka No", width : 100,
                        dataIndex : 'uangmuka_no', sortable: true,
                        tooltip:"Uang Muka No",
                    },
                    {   header: "Pengajuan No", width : 100,
                        dataIndex : 'pengajuan_no', sortable: true,
                        tooltip:"RAPBS No",
                    },
                    {   header: "RAPBS.No", width : 100,
                        dataIndex : 'rapbs_no', sortable: true,
                        tooltip:"RAPBS No",
                    },
                    {   header: "Kode Rekening", width : 100,
                        dataIndex : 'coa_id', sortable: true,
                        tooltip:"Kode rekening",
                        readonly:true,
                    },
                    {   header: "Name", width : 200,
                        dataIndex : 'coa_name', sortable: true,
                        tooltip:"Pos Belanja Name",
                    },
                    {   header: "Uraian", width : 150,
                        dataIndex : 'uraian', sortable: true,
                        tooltip:"Uraian",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Volume", width : 100,
                        dataIndex : 'vol_ajuan', sortable: true,
                        tooltip:"Volume",
                        editor : new Ext.form.TextField({allowBlank: false}),
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   
                            record.data.vol_ajuan = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Satuan", width : 100,
                        dataIndex : 'satuan', sortable: true,
                        tooltip:"Satuan",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },

                    {   header: "Tarif", width : 100,
                        dataIndex : 'tarif', sortable: true,
                        tooltip:"Tarif",
                        readonly:false,
                        editor : new Ext.form.TextField({allowBlank: false}),
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   
                            record.data.tarif = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Jumlah", width : 100,
                        id: 'jumlah_id',
                        dataIndex : 'jum_ajuan', sortable: true,
                        tooltip:"Jumlah",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   
                            value = parseInt(record.data.vol_ajuan) * parseInt(record.data.tarif);
                            record.data.jum_ajuan = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Ber-ulang", width : 60,
                        dataIndex : 'berulang', sortable: true,
                        hidden :true,
                        tooltip:"variabel pembagi anggaran yg berulang",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Jumlah Biaya", width : 100,
                        id : 'total_biaya_urusan',
                        hidden:true,
                        dataIndex : 'total', sortable: true,
                        tooltip:"Jumlah total biaya",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                            return Ext.util.Format.number(value, '0,000');
                        },
                    }
                ],
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [  ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_southGridBBar',
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_southPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_southPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_southGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });

        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_pencairanmonitoringTab",
                jsId : tabId+"_pencairanmonitoringTab",
                title:  "REKAP PENCAIRAN BERDASAR URUSAN",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.pencairan_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {
                    region: 'south',
                    title: 'DETAIL BIAYA',
                    split: true,
                    height : this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    layout : 'border',
                    items :[ 
                    {   
                        region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.SouthGrid]
                    }]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
            this.DataStore.on( 'load', function( store, records, options )
            {
                console.log(records);
                var total_data = 0;
                Ext.each(records,
                    function(the_record)
                    { 
                        total_data = total_data + parseInt(the_record.data.total_biaya);
                    });

                console.log("total jumlah = "+total_data);
 //                console.log(Ext.getCmp('grand_total_id').setValue(total_data));
                Ext.getCmp('grand_pencairan_id').setValue(Ext.util.Format.number(total_data, '0,000'));
            });
            // this.DataStore.load();
      
        },

        Grid_pdf : function(button, event)
        {     var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                console.log(the_record)
                alfalah.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   
                        s: "form",
                        uangmuka_no : the_record.uangmuka_no });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };
        },

        // pencairan search button
        pencairan_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_pencairanSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.pencairan.task,
                        act: alfalah.pencairan.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
//on Ready
Ext.onReady(alfalah.pencairan.initialize, alfalah.pencairan);
// end of file
</script>
<div>&nbsp;</div>