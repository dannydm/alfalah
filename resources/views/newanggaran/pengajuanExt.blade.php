<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.pengajuan');

// create application
alfalah.pengajuan = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.pengajuan.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.pengajuan.Tab);
            this.centerPanel.setActiveTab(this.pengajuan.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.pengajuan.pengajuan= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "RAPBS.No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"RAPBS No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_1 == 0) { metaData.attr = "style = background-color:lime;"; }
                        // not yet approve
                        else if ( record.data.approve_status_1 == 1){ metaData.attr = "style = background-color:yellow;"; }
                        // rejected
                        else {  metaData.attr = "style = background-color:red;"; };

                        return value;
                    }
                },
                {   header: "Urusan / Organisasi", width : 150,
                    dataIndex : 'urusan_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.organisasi_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 150,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 100,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Sumber Dana", width : 150,
                    dataIndex : 'sumberdana_id_name', sortable: true,
                    tooltip:"sumber pendanaan",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Coa", width : 150,
                    dataIndex : 'coa_id_name', sortable: true,
                    tooltip:"kode rekening",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Jumlah Biaya", width : 100,
                    dataIndex : 'total_biaya', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },

                {   header: "Jumlah Tahun Lalu", width : 100,
                    dataIndex : 'jumlahn', sortable: true,
                    tooltip:"Jumlah Anggaran Tahun Sebelumnya",
                    // renderer: function(value)
                    //     {  return value.substring(0,100); }
                },
                {   header: "Jumlah Tahun Ke N", width : 100,
                    dataIndex : 'jumlahke_n', sortable: true,
                    tooltip:"Jumlah Anggaran Sampai dengan Tahun Lalu",
                    // renderer: function(value)
                    //     {  return value.substring(0,100); }
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    tooltip:" Status",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"pengajuan Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"pengajuan Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                // {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'pengajuan_urusan',
                    cid: "urusan_mrapbs_id",
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengajuan_kegiatan',
                    cid: 'kegiatan_mrapbs_id',
                    fieldLabel: 'Kegiatan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengajuan_subkegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'Sub Kegiatan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengajuan_sumberdana',
                    cid: 'sumberdana',
                    fieldLabel: 'Sumber Dana',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengajuan_validasi',
                    cid: 'rapbs_validasi',
                    fieldLabel: 'Validasi',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['validasi'],
                        data : [[''], ['Yes'], ['No']]
                    }),
                    displayField:'validasi',
                    valueField :'validasi',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: ' '
                },
                {   id: 'pengajuan_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },

            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_pengajuanSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.pengajuan_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/pengajuan/1/0')}}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.pengajuan.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    // {   text:'Add',
                    //     tooltip:'Add Record',
                    //     iconCls: 'silk-add',
                    //     handler : this.Grid_add,
                    //     scope : this
                    // },
                    {   text:'View',
                        tooltip:'lihat Data',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_edit,
                        scope : this
                    },
                    '-',
                    // {   text:'Delete',
                    //     tooltip:'Delete Record',
                    //     iconCls: 'silk-delete',
                    //     handler : this.Grid_remove,
                    //     scope : this
                    // },
                    // '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                    {   text:'Keterangan Warna',
                        iconCls:'silk-information ',
                        scope : this
                    },'-',
                    {   text:'Validasi',
                        tooltip:'Warna hijau telah di validasi',
                        xType : 'dataview',
                        style:'background-color:lime',
                        // iconCls: 'silk-information',
                        // handler : this.Grid_remove,
                        scope : this
                    },'-',
                    {   text:'Belum Validasi',
                        tooltip:'Warna kuning belum validasi',
                        xType : 'dataview',
                        style:'background-color:yellow',
                        // iconCls: 'silk-information',
                        // handler : this.Grid_remove,
                        scope : this
                    },


                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_pengajuanGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_pengajuanPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_pengajuanPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_pengajuanGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_pengajuanSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_pengajuanTab",
                jsId : tabId+"_pengajuanTab",
                title:  "REKAP RAPBS",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.pengajuan_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        Grid_add: function(button, event)
        {   var centerPanel = Ext.getCmp('center_panel');
            alfalah.pengajuan.forms.initialize();
            centerPanel.beginUpdate();
            centerPanel.add(alfalah.pengajuan.forms.Tab);
            centerPanel.setActiveTab(alfalah.pengajuan.forms.Tab);
            centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        Grid_edit : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var the_form = Ext.getCmp("rapbsFrm");
                if (the_form)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'RAPBS Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    alfalah.pengajuan.forms.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.pengajuan.forms.Tab);
                    centerPanel.setActiveTab(alfalah.pengajuan.forms.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_detail : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var form_order = Ext.getCmp("form_order");
                if (form_order)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Order Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    alfalah.pengajuan.detailFrm.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.pengajuan.detailFrm.Tab);
                    centerPanel.setActiveTab(alfalah.pengajuan.detailFrm.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        // pengajuan grid save records

        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            } else
            {   var head_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/pengajuan/1/1') }}",
                    {   'x-csrf-token': alfalah.pengajuan.sid }, 
                    {   head : Ext.encode(head_data) }
                );
            };
        },
        // pengajuan grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: "{{ url('/pengajuan/1/2') }}",
                headers:
                {   'x-csrf-token': alfalah.pengajuan.sid },
                params  :
                {   rapbs_no: this.Grid.getSelectionModel().selection.record.data.rapbs_no
                },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Server Message : '+'\n'+the_response.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
        // pengajuan search button
        pengajuan_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_pengajuanSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.pengajuan.task,
                        act: alfalah.pengajuan.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
// create application
alfalah.pengajuan.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
// execute at the first time
    // private variables
    this.Tab;
    this.Searchs;
    this.SearchBtn;
    this.Form;
    this.updateTab;
    this.updateForm;
    this.Records;
    this.DetailRecords;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        load_1     : true,
        // user_data : '',
        // public methods
        initialize: function(the_records)  
        {   console.log('initialize');
            if (the_records)
            {   console.log(the_records);
                this.Records = the_records; };
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   console.log('prepare');
            console.log(this.Records);
            this.urusanDS = alfalah.core.newDataStore(
                "{{ url('/mrapbs/1/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start, type:"1"}
            ); 
            
            this.organisasiDS = alfalah.core.newDataStore(
                "{{ url('/mrapbs/1/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start, type:"2"}
            ); 
            
            this.programDS = alfalah.core.newDataStore(
                "{{ url('/mrapbs/1/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start, type:"3"}
            ); 
            
            this.kegiatanDS = alfalah.core.newDataStore(
                "{{ url('/mrapbs/1/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start, type:"4"}
            ); 
            
            this.sumberdanaDS = alfalah.core.newDataStore(
                "{{ url('/mrapbs/2/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            ); 
            
            this.coaDS = alfalah.core.newDataStore(
                "{{ url('/mcoa/1/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start, type: 0  }
            ); 
            
            this.employeeDS = alfalah.core.newDataStore(
                "{{ url('/employee/1/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start}
            ); 

            this.coaDS_2 = alfalah.core.newDataStore(
                "{{ url('/mcoa/1/9') }}", true,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            
            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   id : 'rapbsFrm',
                // width: '100%',
                frame: true,
                title : 'H E A D E R',
                // autoHeight: true,
                bodyStyle: 'padding: 10px 10px 0 10px;',
                labelWidth: 100,
                defaults: {
                    anchor: '100%',
                    allowBlank: false,
                    msgTarget: 'side'
                },
                items: [
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'rapbsFrm_id',
                            xtype:'textfield',
                            fieldLabel: 'ID',
                            name: 'id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '',
                            hidden : true,
                        },
                        {   id : 'rapbsFrm_created_date', 
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'rapbsFrm_tahun_ajaran_id', 
                            xtype:'textfield',
                            fieldLabel: 'Tahun',
                            name: 'tahun_ajaran_id',
                            anchor:'65%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $TAHUNANGGARAN_ID }}',
                            hidden : true,
                        }]
                    },
                    {   columnWidth:.4,
                        layout: 'form',
                        border:false,
                        items: [
                        {   xtype: 'fieldset',
                            title: 'NOMOR KEGIATAN',
                            autoHeight: true,
                            anchor:'95%',
                            // defaultType: 'checkbox', // each item will be a checkbox
                            items: [
                            {   id : 'rapbsFrm_rapbs_no', 
                                xtype:'textfield',
                                fieldLabel: 'No. Referensi',
                                name: 'rapbs_no',
                                anchor:'95%',
                                allowBlank: true,
                                readOnly : true,
                                value : 'by System'
                            }]
                        },
                        {   xtype: 'fieldset',
                            title: 'INPUT RENCANA ANGGARAN',
                            autoHeight: true,
                            anchor:'95%',
                            // defaultType: 'checkbox', // each item will be a checkbox
                            items: [
                            {   id : 'rapbsFrm_tahun_ajaran_name', 
                                xtype:'textfield',
                                fieldLabel: 'Tahun Anggaran',
                                name: 'tahun_ajaran_name',
                                anchor:'65%',
                                allowBlank: false,
                                readOnly : true,
                                value : '{{ $TAHUNANGGARAN }}',
                            },
                            {   id : 'rapbsFrm_urusan_mrapbs_id', 
                                xtype : 'combo',
                                fieldLabel: 'Urusan',
                                labelSeparator : '',
                                name: 'urusan_mrapbs_id',
                                anchor:'95%',
                                allowBlank: false,
                                readOnly : true,
                                store: this.urusanDS,
                                displayField: 'name',
                                valueField: 'mrapbs_id',
                                hiddenName: 'mrapbs_id',
                                hiddenVAlue: '',
                                mode : 'local',
                                forceSelection: true,
                                triggerAction: 'all',
                                selectOnFocus:true,
                                editable: false,
                                typeAhead: true,
                                width : 100,
                                value: '',
                                listeners : 
                                {   scope : this,
                                    'select' : function (combo, record, indexVal)
                                    {   // reset inherit combo
                                        Ext.getCmp('rapbsFrm_organisasi_mrapbs_id').setValue('');
                                        Ext.getCmp('rapbsFrm_program_mrapbs_id').setValue('');
                                        Ext.getCmp('rapbsFrm_kegiatan_mrapbs_id').setValue('');
                                        this.organisasiDS.removeAll();
                                        this.programDS.removeAll();
                                        this.kegiatanDS.removeAll();

                                        this.organisasiDS.baseParams = {
                                            s       : "form",
                                            limit   : this.page_limit, 
                                            start   : this.page_start,
                                            type    : "2",
                                            parent  : record.data.mrapbs_id 
                                        };
                                        this.organisasiDS.reload();
                                    }
                                }
                            },
                            {   id : 'rapbsFrm_organisasi_mrapbs_id', 
                                xtype : 'combo',
                                fieldLabel: 'Organisasi',
                                labelSeparator : '',
                                name: 'organisasi_mrapbs_id',
                                anchor:'95%',
                                allowBlank: false,
                                readOnly : true,
                                store: this.organisasiDS,
                                displayField: 'name',
                                valueField: 'mrapbs_id',
                                mode : 'local',
                                forceSelection: true,
                                triggerAction: 'all',
                                selectOnFocus:true,
                                editable: false,
                                typeAhead: true,
                                width : 100,
                                value: 'Pilih',
                                listeners : 
                                {   scope : this,
                                    'select' : function (combo, record, indexVal)
                                    {   // reset inherit combo
                                        console.log('select rapbsFrm_organisasi_mrapbs_id');
                                        Ext.getCmp('rapbsFrm_program_mrapbs_id').setValue('');
                                        Ext.getCmp('rapbsFrm_kegiatan_mrapbs_id').setValue('');
                                        this.programDS.removeAll();
                                        this.kegiatanDS.removeAll();
                                        
                                        this.programDS.baseParams = {
                                            s       : "form",
                                            limit   : this.page_limit, 
                                            start   : this.page_start,
                                            type    : "3",
                                            parent  : record.data.mrapbs_id 
                                        };
                                        this.programDS.reload();
                                    }
                                }
                            },
                            {   id : 'rapbsFrm_program_mrapbs_id', 
                                xtype : 'combo',
                                fieldLabel: 'Program',
                                labelSeparator : '',
                                name: 'program_mrapbs_id',
                                anchor:'95%',
                                allowBlank: false,
                                readOnly : true,
                                store: this.programDS,
                                displayField: 'name',
                                valueField: 'mrapbs_id',
                                mode : 'local',
                                forceSelection: true,
                                triggerAction: 'all',
                                selectOnFocus:true,
                                editable: false,
                                typeAhead: true,
                                width : 100,
                                value: 'Pilih',
                                listeners : 
                                { scope : this,
                                    'select' : function (combo, record, indexVal)
                                    {   // reset inherit combo
                                        Ext.getCmp('rapbsFrm_kegiatan_mrapbs_id').setValue('');
                                        this.kegiatanDS.removeAll();
                                        
                                        this.kegiatanDS.baseParams = {
                                            s       : "form",
                                            limit   : this.page_limit, 
                                            start   : this.page_start,
                                            type    : "4",
                                            parent  : record.data.mrapbs_id 
                                        };
                                        this.kegiatanDS.reload();
                                    }
                                }
                            },
                            {   id : 'rapbsFrm_kegiatan_mrapbs_id', 
                                xtype : 'combo',
                                fieldLabel: 'Kegiatan',
                                labelSeparator : '',
                                name: 'kegiatan_mrapbs_id',
                                anchor:'95%',
                                allowBlank: false,
                                readOnly : true,
                                store: this.kegiatanDS,
                                displayField: 'name',
                                valueField: 'mrapbs_id',
                                mode : 'local',
                                forceSelection: true,
                                triggerAction: 'all',
                                selectOnFocus:true,
                                editable: false,
                                typeAhead: true,
                                width : 100,
                                value: 'Pilih'
                            },
                            {   id : 'rapbsFrm_sub_kegiatan_mrapbs_id', 
                                xtype:'textfield',
                                fieldLabel: 'Sub Kegiatan',
                                name: 'sub_kegiatan_mrapbs_id',
                                anchor:'95%',
                                allowBlank: false,
                                value : ''
                            },
                            {   id : 'rapbsFrm_sumberdana_id', 
                                xtype : 'combo',
                                fieldLabel: 'Sumber Dana',
                                labelSeparator : '',
                                name: 'sumberdana_id',
                                anchor:'95%',
                                allowBlank: false,
                                readOnly : true,
                                store: this.sumberdanaDS,
                                displayField: 'name',
                                valueField: 'sumberdana_id',
                                mode : 'local',
                                forceSelection: true,
                                triggerAction: 'all',
                                selectOnFocus:true,
                                editable: false,
                                typeAhead: true,
                                width : 100,
                                value: ''
                            },
                            {   id : 'rapbsFrm_coa_id', 
                                xtype : 'combo',
                                fieldLabel: 'COA',
                                labelSeparator : '',
                                name: 'coa_id',
                                anchor:'95%',
                                allowBlank: false,
                                readOnly : true,
                                store: this.coaDS,
                                displayField: 'name',
                                valueField: 'mcoa_id',
                                mode : 'local',
                                forceSelection: true,
                                triggerAction: 'all',
                                selectOnFocus:true,
                                editable: false,
                                typeAhead: true,
                                width : 100,
                                value: ''
                            },
                            ]
                        },
                        {   title: 'PENANGGUNG JAWAB DAN PELAKSANAAN',
                            xtype: 'fieldset',
                            autoHeight: true,
                            anchor:'95%',
                            // defaultType: 'checkbox', // each item will be a checkbox
                            items: [
                            {   id : 'rapbsFrm_pic_id', 
                                xtype : 'combo',
                                fieldLabel: 'PIC',
                                labelSeparator : '',
                                name: 'pic_id',
                                anchor:'95%',
                                allowBlank: true,
                                readOnly : true,
                                store: this.employeeDS,
                                displayField: 'name',
                                valueField: 'name',
                                mode : 'local',
                                forceSelection: true,
                                triggerAction: 'all',
                                selectOnFocus:true,
                                editable: false,
                                typeAhead: true,
                                width : 100,
                                value: 'Pilih'
                            },
                            {   id : 'rapbsFrm_tgl_mulai', 
                                xtype:'datefield',
                                fieldLabel: 'Tgl Mulai',
                                name: 'tgl_mulai',
                                anchor:'95%',
                                format: 'd/m/Y',
                                allowBlank: false,
                                readOnly : true,
                                value : new Date()
                            },
                            {   id : 'rapbsFrm_tgl_selesai', 
                                xtype:'datefield',
                                fieldLabel: 'Tgl Selesai',
                                name: 'tgl_selesai',
                                anchor:'95%',
                                format: 'd/m/Y',
                                allowBlank: false,
                                readOnly : true,
                                value : new Date()
                            }]
                        }]
                    },
                    {   columnWidth:.6,
                        layout: 'form',
                        border:false,
                        items: [
                        {   title: 'INDIKATOR DAN TOLOK UKUR',
                            xtype: 'fieldset',
                            autoHeight: true,
                            anchor:'95%',
                            // defaultType: 'checkbox', // each item will be a checkbox
                            items: [
                            {   layout:'column',
                                border:false,
                                items:[
                                {   columnWidth:.7,
                                    layout: 'form',
                                    border:false,
                                    items: [ // hidden columns
                                    {   id : 'rapbsFrm_masukan', 
                                        xtype:'textarea',
                                        fieldLabel: 'Masukan',
                                        name: 'masukan',
                                        anchor:'95%',
                                        allowBlank: false,
                                        readOnly : true,
                                        value : ''  
                                    },
                                    {   id : 'rapbsFrm_keluaran', 
                                        xtype:'textarea',
                                        fieldLabel: 'Keluaran',
                                        name: 'keluaran',
                                        anchor:'95%',
                                        allowBlank: false,
                                        readOnly : true,
                                        value : ''
                                    },
                                    {   id : 'rapbsFrm_hasil', 
                                        xtype:'textarea',
                                        fieldLabel: 'Hasil',
                                        name: 'hasil',
                                        anchor:'95%',
                                        allowBlank: false,
                                        readOnly : true,
                                        value : ''
                                    },
                                    {   id : 'rapbsFrm_sasaran', 
                                        xtype:'textarea',
                                        fieldLabel: 'Sasaran Kegiatan',
                                        name: 'sasaran',
                                        anchor:'95%',
                                        allowBlank: false,
                                        readOnly : true,
                                        value : ''
                                    }]
                                },
                                {   columnWidth:.3,
                                    layout: 'form',
                                    border:false,
                                    items: [ // hidden columns
                                    {   id : 'rapbsFrm_tgt_masukan', 
                                        xtype:'textfield',
                                        fieldLabel: 'Target Masuk',
                                        name: 'tgt_masukan',
                                        anchor:'80%',
                                        allowBlank: false,
                                        readOnly : true,
                                        value : ''  
                                    }, 
                                    {   height:40 
                                    },
                                    {   id : 'rapbsFrm_tgt_keluaran', 
                                        xtype:'textfield',
                                        fieldLabel: 'Target Keluar',
                                        name: 'tgt_keluaran',
                                        anchor:'80%',
                                        allowBlank: false,
                                        
                                        readOnly : true,
                                        value : ''
                                    }]
                                }]
                            }]
                        },
                        {   title: 'ANGGARAN SEBELUMNYA',
                            xtype: 'fieldset',
                            autoHeight: true,
                            anchor:'95%',
                            // defaultType: 'checkbox', // each item will be a checkbox
                            items: [
                            {   id : 'rapbsFrm_jumlahn', 
                                xtype:'textfield',
                                fieldLabel: 'Jumlah n-1',
                                name: 'jumlahn',
                                anchor:'95%',
                                allowBlank: true,
                                readOnly : true,
                                value : ''
                            },
                            {   id : 'rapbsFrm_jumlahke_n', 
                                xtype:'textfield',
                                fieldLabel: 'Jumlah Tahun n',
                                name: 'jumlahke_n',
                                anchor:'95%',
                                allowBlank: true,
                                readOnly : true,
                                value : ''
                            }]
                        },
                        {   id : 'rapbsFrm_status', 
                            xtype : 'combo',
                            fieldLabel: 'Status',
                            labelSeparator : '',
                            name: 'status',
                            anchor:'50%',
                            allowBlank: false,
                            readOnly : false,
                            store : new Ext.data.SimpleStore(
                            {   fields: ["label", "key"],
                                data : [ ["Active", "0"], ["Inactive", "1"]]
                            }),
                            displayField:'label',
                            valueField :'key',
                            mode : 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            editable: false,
                            width : 100,
                            value: 0
                        }]
                    }],
                }],
            });
            /************************************
                G R I D S
            ************************************/  
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'rapbs_id', sortable: true,
                    tooltip:"ID",
                    hidden: true,
                },
                {   header: "No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"Doc.No",
                    readonly:true,
                },
                {   header: "Account.No", width : 100,
                    dataIndex : 'coa_id', sortable: true,
                    tooltip:"Accounting COA No",
                    readonly:true,
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Name", width : 200,
                    dataIndex : 'coa_name', sortable: true,
                    tooltip:"Account Name",
                    readonly:true,
                    editor: new Ext.form.ComboBox(
                    {   store: this.coaDS_2,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'coa_name',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                // 'beforequery' : function (combo, query, forceAll, cancel)
                                // {   combo.combo.store.baseParams = {
                                //         s:"form",
                                //         limit:this.page_limit, start:this.page_start,
                                //         asset_id : combo.combo.gridEditor.record.data.asset_id };
                                // },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.coa_id = record.data.mcoa_id;
                                    combo.gridEditor.record.data.coa_name = record.data.coa_name;
                                    alfalah.pengajuan.forms.Grid.getView().refresh();
                                },
                        }
                    }),
                },
                {   header: "Uraian", width : 150,
                    dataIndex : 'uraian', sortable: true,
                    tooltip:"Uraian",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Volume", width : 100,
                    dataIndex : 'volume', sortable: true,
                    tooltip:"Volume",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Tarif", width : 100,
                    dataIndex : 'tarif', sortable: true,
                    tooltip:"Tarif",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Satuan", width : 100,
                    dataIndex : 'satuan', sortable: true,
                    tooltip:"Satuan",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Jumlah", width : 100,
                    dataIndex : 'jumlah', sortable: true,
                    tooltip:"Jumlah",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        value = parseInt(record.data.volume) * parseInt(record.data.tarif);
                        record.data.jumlah = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Status", width : 100,
                    dataIndex : 'status_name', sortable: true,
                    tooltip:"Status",
                },
            ];

            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/pengajuan/1/10') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.pengajuan.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Approve',
                        tooltip:'Approve Record',
                        iconCls: 'silk-tick',
                        handler : this.Grid_approve,
                        scope : this
                    },
                    '-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_newsGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_newsPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_newsPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_newsGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_newsSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
        },
        // build the layout
        build_layout: function()
        {   
            if (Ext.isObject(this.Records))
                {   the_title = "VIEW DATA"; }
            else { the_title = "VIEW DATA"; };

            this.Tab = new Ext.Panel(
            {   id : tabId+"_FormsTab",
                jsId : tabId+"_FormsTab",
                title:  '<span style="background-color: yellow;">'+the_title+'</span>',
                iconCls: 'silk-note_add',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region   : 'center', // center region is required, no width/height specified
                    xtype    : 'tabpanel',
                    activeTab: 0,
                    items    : [this.Form, this.detailTab]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log('prepare');
            console.log(this.Records);
            
            if (Ext.isObject(this.Records))
            {   Ext.each(this.Records.fields.items,
                function(the_field)
                {   switch (the_field.name)
                    {   case "id" :
                        case "tahun_ajaran_id" :
                        case "sub_kegiatan_mrapbs_id" :
                        case "masukan" :
                        case "tgt_masukan" :
                        case "keluaran" :
                        case "tgt_keluaran" :
                        case "hasil" :
                        case "sasaran" :
                        case "pic_id" :
                        case "tgl_mulai" :
                        case "tgl_selesai" :
                        case "jumlahn" :
                        case "jumlahke_n" :
                        case "status" :
                        case "created_date" :
                        {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(this.Records.data[the_field.name]);
                        };
                        break;
                        case "rapbs_no" :
                        {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(this.Records.data[the_field.name]);

                            this.DataStore.baseParams = {   
                                s:"form", limit:this.page_limit, start:this.page_start,
                                rapbs_no: this.Records.data[the_field.name]
                            };
                            this.DataStore.reload();
                        };
                        break;
                        case "urusan_mrapbs_id" :
                        {   the_urusan = this.Records.data[the_field.name]; //+'_name'
                            this.urusanDS.on( 'load', function( store, records, options )
                            {   console.log('onload urusan_mrapbs_id');
                                Ext.getCmp('rapbsFrm_'+the_field.name).setValue(the_urusan);  
                                the_urusan = false;
                            });
                        };
                        break;
                        case "organisasi_mrapbs_id" :
                        {   the_organisasi = this.Records.data[the_field.name]; //+'_name'
                            this.organisasiDS.on( 'load', function( store, records, options )
                            {   if (the_organisasi)
                                {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(the_organisasi);
                                    the_organisasi = false;
                                };
                            });
                        };
                        break;
                        case "program_mrapbs_id" :
                        {   the_program = this.Records.data[the_field.name]; //+'_name'
                            this.programDS.on( 'load', function( store, records, options )
                            {   if (the_program)
                                {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(the_program);
                                    the_program = false;
                                };
                            });
                        };
                        break;
                        case "kegiatan_mrapbs_id" :
                        {   the_kegiatan = this.Records.data[the_field.name]; //+'_name'
                            this.kegiatanDS.on( 'load', function( store, records, options )
                            {   if (the_kegiatan)
                                {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(the_kegiatan);
                                    the_kegiatan = false;
                                };
                            });
                        };
                        break;
                        case "sumberdana_id" :
                        {   the_sumber = this.Records.data[the_field.name]; //+'_name'
                            this.coaDS.on( 'load', function( store, records, options )
                            {   if (the_sumber)
                                {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(the_sumber);
                                    the_sumber = false;
                                };
                            });
                        };
                        break;
                        case "coa_id" :
                        {   the_coa = this.Records.data[the_field.name]; //+'_name'
                            this.coaDS.on( 'load', function( store, records, options )
                            {   if (the_coa)
                                {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(the_coa);
                                    the_coa = false;
                                }; 
                            });
                        };
                        break;
                        case "pic_id" :
                        {   the_pic = this.Records.data[the_field.name]; //+'_name'
                            this.employeeDS.on( 'load', function( store, records, options )
                            {   if (the_pic)
                                {   Ext.getCmp('rapbsFrm_'+the_field.name).setValue(the_pic);
                                    the_pic = false;
                                }; 
                            });
                        };
                        break;
                    };
                }, this);
            };
            this.urusanDS.reload();
            this.organisasiDS.load();
            this.programDS.load();
            this.kegiatanDS.load();
            this.sumberdanaDS.load();
            this.coaDS.load();
            this.employeeDS.load();

            this.Records = Ext.data.Record.create(
            [   {name: 'rapbs_id', type: 'integer'},
                {name: 'modified_date', type: 'date'},
            ]);
        },
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   rapbs_id: "",
                    rapbs_no: "",
                    coa_id: "",
                    coa_name: "New Account",
                    uraian: "New Uraian",
                    volume: 0,
                    tarif: 0,
                    satuan: "unit",
                    jumlah: 0,
                    status_name: "Active",
                    created_date: "",
                    modified_date: "",
                    action: "ADD",
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 3);
        },
        Form_save : function(button, event)
        {   var head_data = [];
            var json_data = [];
            var modi_data = [];
            var v_json = {};
            // header validation
            if (alfalah.core.validateFields([
                'rapbsFrm_tahun_ajaran_id','rapbsFrm_urusan_mrapbs_id', 'rapbsFrm_organisasi_mrapbs_id', 
                'rapbsFrm_program_mrapbs_id', 'rapbsFrm_kegiatan_mrapbs_id', 
                'rapbsFrm_sub_kegiatan_mrapbs_id']))
            {   // detail validation
                // header data
                head_data = alfalah.core.getHeadData([
                    'rapbsFrm_id', 'rapbsFrm_rapbs_no', 'rapbsFrm_tahun_ajaran_id', 
                    'rapbsFrm_urusan_mrapbs_id', 'rapbsFrm_organisasi_mrapbs_id', 
                    'rapbsFrm_program_mrapbs_id', 'rapbsFrm_kegiatan_mrapbs_id', 
                    'rapbsFrm_sub_kegiatan_mrapbs_id', 'rapbsFrm_sumberdana_id', 'rapbsFrm_coa_id',
                    'rapbsFrm_masukan', 'rapbsFrm_tgt_masukan', 'rapbsFrm_keluaran', 
                    'rapbsFrm_tgt_keluaran', 'rapbsFrm_hasil', 'rapbsFrm_sasaran',
                    'rapbsFrm_pic_id', 'rapbsFrm_tgl_mulai', 'rapbsFrm_tgl_selesai', 
                    'rapbsFrm_status',
                    'rapbsFrm_created_date',
                     // 'rapbsFrm_modified_date',
                    ]);
                //detail data
                json_data = alfalah.core.getDetailData(this.DataStore.getModifiedRecords());
                // submit data
                alfalah.core.submitForm(
                    tabId+"_FormsTab", 
                    alfalah.pengajuan.pengajuan.DataStore,
                    "{{ url('/pengajuan/1/1') }}",
                    {   'x-csrf-token': alfalah.pengajuan.sid },
                    {   task: 'save',
                        head : Ext.encode(head_data),
                        json : Ext.encode(json_data),
                    });
            };
        },
        Grid_approve: function(button, event)
        {   
            the_rapbs_no = Ext.getCmp('rapbsFrm_rapbs_no').getValue();
            if (the_rapbs_no == "")
            {
                Ext.Msg.show(
                {   title :'E R R O R ',
                    msg : 'RAPBS not saved yet',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
            else
            {
                this.Grid.stopEditing();
                Ext.Ajax.request(
                {   method: 'POST',
                    url: "{{ url('/pengajuan/1/705') }}",
                    headers:
                    {   'x-csrf-token': alfalah.pengajuan.sid },
                    params  :
                    {   rapbs_no: the_rapbs_no },
                    success: function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                        else
                        {   
                            Ext.getCmp(tabId+"_FormsTab").destroy();
                            alfalah.pengajuan.pengajuan.DataStore.reload();
                        };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                  scope: this
                });
            };
        },                  
     }; // end of public space
}(); // end of app

Ext.onReady(alfalah.pengajuan.initialize, alfalah.pengajuan);
// end of file
</script>
<div>&nbsp;</div>