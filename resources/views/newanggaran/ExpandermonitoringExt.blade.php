<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.monitoring');

// create application
alfalah.monitoring = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
 
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',
  
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.monitoring.initialize();
            this.monitoringcair.initialize();
            this.allstatus.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.monitoring.Tab);
            this.centerPanel.add(this.monitoringcair.Tab);
            this.centerPanel.add(this.allstatus.Tab);
            this.centerPanel.setActiveTab(this.monitoring.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {  },
    }; // end of public space
}(); // end of app
// create application
alfalah.monitoring.monitoring= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.monitoring.centerPanel.container.dom.clientHeight;
            this.Columns = [  
                
                {   header: "RAPBS.No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"RAPBS No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_1 == null
                    ) { metaData.attr = "style = background-color:"; }
                        // not yet approve
                        else if ( record.data.approve_status_3 == 1
                    ){ metaData.attr = "style = background-color:;"; }
                        // rejected
                        else {  metaData.attr = "style = background-color:;"; };
                        return value;
                    },
                },
                {   header: "Organisasi / Urusan", width : 150,
                dataIndex : 'organisasi_mrapbs_id_name',
                sortable: true,
                tooltip:"Jenjang/Bidang/Departemen",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                    return alfalah.core.gridColumnWrap(result);
                }
            },
            {   header: "Program / Kegiatan", width : 150,
                dataIndex : 'program_mrapbs_id_name', sortable: true,
                tooltip:"Nama Program",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                    return alfalah.core.gridColumnWrap(result);
                }
            },
            {   header: "Sub Kegiatan", width : 100,
                dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                tooltip:"nama sub kegiatan",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   return alfalah.core.gridColumnWrap(value);
                }
            },
            {   header: "Sumber Dana", width : 150,
                dataIndex : 'sumberdana_id_name', sortable: true,
                tooltip:"sumber pendanaan",
                // editor : new Ext.form.TextField({allowBlank: false}),
            },
            {   header: "Keluaran", width : 150,
                dataIndex : 'keluaran', sortable: true,
                tooltip:"keluaran",
                // editor : new Ext.form.TextField({allowBlank: false}),
            },
            {   header: "Hasil", width : 150,
                dataIndex : 'hasil', sortable: true,
                tooltip:"hasil",
                // editor : new Ext.form.TextField({allowBlank: false}),
            },
            {   header: "Sasaran", width : 150,
                dataIndex : 'sasaran', sortable: true,
                tooltip:"sasaran",
                // editor : new Ext.form.TextField({allowBlank: false}),
            },

            {   header: "Coa", width : 150,
                dataIndex : 'coa_id_name', sortable: true,
                tooltip:"kode rekening",
                hidden : true,
                // editor : new Ext.form.TextField({allowBlank: false}),
            },
            {   header: "Jumlah Biaya", width : 100,
                id : 'total_biaya_urusan',
                hidden:true,
                dataIndex : 'total_biaya', sortable: true,
                tooltip:"Jumlah total biaya",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   metaData.attr="style = text-align:right;";
                    return Ext.util.Format.number(value, '0,000');
                },
            },
            {   header: "Jumlah Tahun Lalu", width : 100,
                dataIndex : 'jumlahn', sortable: true,
                tooltip:"Jumlah Anggaran Tahun Sebelumnya",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   metaData.attr="style = text-align:right;";
                    return Ext.util.Format.number(value, '0,000');
                },
            },
            {   header: "Jumlah Tahun Ke N", width : 100,
                dataIndex : 'jumlahke_n', sortable: true,
                tooltip:"Jumlah Anggaran Sampai dengan Tahun Lalu",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   value = (record.data.total_biaya);
                    metaData.attr="style = text-align:right;";
                    return Ext.util.Format.number(value, '0,000');
                },
            },
            {   header: "% perubahan", width : 100,
                dataIndex : 'j_persen', sortable: true,
                tooltip:"Persentase naik / turun",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {  
                    t_pengurang = ((record.data.total_biaya)-(record.data.jumlahn));
                    value = (t_pengurang/(record.data.jumlahn)) * 100;
                    
                    if (value < 100.00) 
                        { metaData.attr = "style = background-color:cyan; "; }
                    else {  metaData.attr = "style = background-color:red; "; };
                    return Ext.util.Format.number(value, '000.00');

                },
            },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    hidden :true,
                    tooltip:" Status",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"monitoring Created Date",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"monitoring Last Updated",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Searchs = [
                {   id: 'monitoring_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoring_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoring_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoring_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub. Keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoring_sumberdana',
                    cid: 'sumber_dana_id_name',
                    fieldLabel: 'Dana',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },


                {   id: 'monitoring_status',
                    cid: 'status',
                    hidden :true,
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_monitoringSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.monitoring_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/monitoring/1/0')}}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [ 
                        'TOTAL RAPBY. THN LALU',
                        new Ext.form.TextField(
                        {   id : 'total_thn_lalu_id',
                        // store:this.DataStore,
                        displayField: 'total_thn_lalu',
                        valueField: 'total_thn_lalu',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),'-',

                    'PERSENTASE + -',
                    new Ext.form.TextField(
                    {   id : 't_persen_id',
                        // store:this.DataStore,
                        displayField: 't_persen',
                        valueField: 't_persen',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                    '->',
                    'TOTAL RAPBY. THN INI',
                    new Ext.form.TextField(
                    {   id : 'grand_total_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),

                    ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_monitoringGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_monitoringPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_monitoringPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_monitoringGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
                listeners : { scope : this,
                    'cellclick': function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent)
                    {
                        console.log('cell click');
                        if (iColIdx == 0 ) // collom pertama
                        {   the_record = this.Grid.getSelectionModel().selection.record.data.rapbs_no
                            console.log(the_record);
                            // this.southdatastore.baseParam = {new param in here};
                            alfalah.monitoring.monitoring.SouthDataStore.baseParams = {   task: alfalah.monitoring.task,
                                act: alfalah.monitoring.act,
                                a:2, b:0, s:"form", rapbs_no : the_record,
                                limit:this.page_limit, start:this.page_start 
                            };                 
                            alfalah.monitoring.monitoring.SouthDataStore.reload();
                        };
                    }
                    ,
                    'rowselect' : function (a,b,c)
                    {   console.log('listener grid select');
                        console.log(a);
                        console.log(b);
                        console.log(c);

                        // this.south_datastore.baseParam = {new param in here};
                        // this.south_datastrore.reload();
                        }
                }
            });
            /**
            * SOUTH-GRID
            */
            this.EastDataStore = alfalah.core.newDataStore(
                "{{ url('/monitoring/1/11') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.EastDataStore.load();
            this.EastGrid =   new Ext.grid.EditorGridPanel( 
            {  store : this.EastDataStore,
                columns: [ 
                    {   header: "ID", width : 50,
                        dataIndex : 'organisasi_mrapbs_id', sortable: true,
                        hidden :true,
                        tooltip:"ID",
                    },
                    {   header: "Nama Organisasi", width : 250,
                        dataIndex : 'organisasi_mrapbs_id_name', sortable: true,
                        tooltip:"Doc.No",
                    },
                    {   header: "Jumlah Usulan Anggaran", width : 150,
                        dataIndex : 'total_biaya', sortable: true,
                        tooltip:"jumlah anggaran yang telah diusulkan",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                            {   metaData.attr="style = text-align:right;";
                                return Ext.util.Format.number(value, '0,000');
                            },
                    },
                    {   header: "Jumlah Ajuan Anggaran", width : 150,
                        hidden :true,
                        dataIndex : 'total_biaya_ajuan', sortable: true,
                        tooltip:"jumlah anggaran yang telah diusulkan",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                            {   metaData.attr="style = text-align:right;";
                                return Ext.util.Format.number(value, '0,000');
                            },
                    },

                    {   header: "Jumlah Pencairan", width : 100,
                        hidden :true,
                        dataIndex : 'rapbs_name', sortable: true,
                        tooltip:"Doc.No",
                    },
                ],

                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [  ],
                bbar: new Ext.PagingToolbar(
                {  
                    id : tabId+'_eastGridBBar',
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_eastPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 50,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_eastPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_eastGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                
                }),

            });

            this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/monitoring/1/10') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.SouthDataStore.load();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.SouthDataStore,
                columns: [ 
                    {   header: "ID", width : 50,
                        dataIndex : 'rapbs_id', sortable: true,
                        tooltip:"ID",
                        hidden: true,
                    },
                    {   header: "No", width : 100,
                        dataIndex : 'rapbs_no', sortable: true,
                        tooltip:"Doc.No",
                    },
                    {   header: "Pos.Belanja", width : 150,
                        dataIndex : 'coa_id', sortable: true,
                        tooltip:"Pos Belanja",
                        readonly:true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>['+value+']</b>'+record.data.coa_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                    },
                    {   header: "Name", width : 200,
                        dataIndex : 'coa_name', sortable: true,
                        readonly:true,
                        hidden :true,
                        tooltip:"Pos Belanja Name",
                    },
                    {   header: "Uraian", width : 150,
                        dataIndex : 'uraian', sortable: true,
                        tooltip:"Uraian",
                        readonly:true,
                    },
                    {   header: "Volume", width : 100,
                        dataIndex : 'volume', sortable: true,
                        tooltip:"Volume",
                        readonly:true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                            {   record.data.volume = value;
                                value1 = Ext.util.Format.number(value, '0,000');
                                result = '<b>'+value1+'</b>'+'&ensp;'+record.data.satuan;
                                return alfalah.core.gridColumnWrap(result);
                            }
                    },
                    {   header: "Satuan", width : 100,
                        dataIndex : 'satuan', sortable: true,
                        tooltip:"Satuan",
                        hidden :true,
                        readonly:true,
                    },
                    {   header: "Tarif", width : 100,
                        dataIndex : 'tarif', sortable: true,
                        tooltip:"Tarif",
                        readonly:true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   record.data.tarif = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Jumlah", width : 100,
                        dataIndex : 'jumlah', sortable: true,
                        readonly:true,
                        tooltip:"Jumlah",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   
                            value = parseInt(record.data.volume) * parseInt(record.data.tarif);
                            record.data.jumlah = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Ber-ulang", width : 60,
                        dataIndex : 'berulang', sortable: true,
                        hidden :true,
                        tooltip:"variabel pembagi anggaran yg berulang",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Status", width : 100,
                        dataIndex : 'status_name', sortable: true,
                        readonly:true,
                        hidden :true,   
                        tooltip:"Status",
                    },
                ],
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [  ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_newsGridBBar',
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_newsPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_newsPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_newsGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });

    // *****************************************
        
    },
    // build the layout
    build_layout: function()
    {   this.Tab = new Ext.Panel(
        {   id : tabId+"_monitoringTab",
            jsId : tabId+"_monitoringTab",
            title:  "MONITORING USULAN ANGGARAN",
            region: 'center',
            layout: 'border',
            items: [
            {   region: 'center',     // center region is required, no width/height specified
                xtype: 'container',
                layout: 'fit',
                items:[this.Grid]
            },
            {   title: 'Parameters',
                region: 'east',     // position for region
                split:true,
                width: 200,
                minSize: 175,
                maxSize: 400,
                collapsible: true,
                layout : 'accordion',
                items:[
                {   //title: 'S E A R C H',
                    labelWidth: 50,
                    defaultType: 'textfield',
                    xtype: 'form',
                    frame: true,
                    autoScroll : true,
                    items : this.Searchs,
                    tbar: [
                        {   text:'Search',
                            tooltip:'Search',
                            iconCls: 'silk-zoom',
                            handler : this.monitoring_search_handler,
                            scope : this,
                        }]
                }]
            },
            {
                region: 'south',
                title: 'DETAIL ANGGARAN',
                split: true,
                height : this.region_height/2,
                collapsible: true,
                margins: '0 0 0 0',
                layout : 'border',
                items :[ 
                {   
                    region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.SouthGrid]
                },
                {   region: 'east',     // position for region
                    // store : this.EastDatastore,
                    title: 'INFORMASI',
                    split:true,
                    width: 550,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[this.EastGrid]
                }]
            }]
        });
    },
    // finalize the component and layout drawing
    finalize_comp_and_layout: function()
    {
        this.DataStore.on( 'load', function( store, records, options )
        {
            console.log(records);
            var total_data = 0;
            var total_thn_lalu=0;
            var t_persentase=0;

            Ext.each(records,
                function(the_record)
                {   console.log('cari record');
                    console.log(the_record.data.jumlahn);
                    total_data = total_data + parseInt(the_record.data.total_biaya);
                    total_thn_lalu = total_thn_lalu + parseInt(the_record.data.jumlahn);

                    // t_pengurang = ((record.data.total_biaya)-(record.data.jumlahn));
                    // value = (t_pengurang/(record.data.jumlahn)) * 100;
                    t_persentase = total_data-total_thn_lalu;
                    t_persen = (t_persentase/total_thn_lalu)*100;
                });

            console.log("total persentase = "+t_persen);
            console.log("total tahun lalu = "+total_thn_lalu);
    //                console.log(Ext.getCmp('grand_total_id').setValue(total_data));
            Ext.getCmp('grand_total_id').setValue(Ext.util.Format.number(total_data, '0,000'));
            Ext.getCmp('total_thn_lalu_id').setValue(Ext.util.Format.number(total_thn_lalu, '0,000'));
            Ext.getCmp('t_persen_id').setValue(Ext.util.Format.number(t_persen, '000.00'));

        });
        // this.DataStore.load();

    },
    // monitoring search button
    monitoring_search_handler : function(button, event)
    {   var the_search = true;
        if ( this.DataStore.getModifiedRecords().length > 0 )
        {   Ext.Msg.show(
                {   title:'W A R N I N G ',
                    msg: ' Modified Data Found, Do you want to save it before search process ? ',
                    buttons: Ext.Msg.YESNO,
                    fn: function(buttonId, text)
                        {   if (buttonId =='yes')
                            {   Ext.getCmp(tabId+'_monitoringSaveBtn').handler.call();
                                the_search = false;
                            } else the_search = true;
                        },
                    icon: Ext.MessageBox.WARNING
                    });
        };

        if (the_search == true) // no modification records flag then we can go to search
        {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
            this.DataStore.baseParams = Ext.apply( the_parameter,
                {   task: alfalah.monitoring.task,
                    act: alfalah.monitoring.act,
                    a:2, b:0, s:"form", 
                    limit:this.page_limit, start:this.page_start });
            this.DataStore.reload(); 
        };
    },
}; // end of public space
}(); // end of app
// create application
alfalah.monitoring.monitoringcair = function()
{   // do NOT access DOM from here; elements don't exist yet
// execute at the first time
// private variables
    this.Tab;
    this.Grid;
    
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn_center;
    this.Searchs_center;
    this.expander;
    this.ExpanderDs;
    this.SouthGrid;
    this.SouthDataStore;
    this.SearchBtn_south;
    this.Searchs_south;
    this.Records_south;
    // private functions
 
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;

        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.monitoring.centerPanel.container.dom.clientHeight;
          

            this.ExpanderDs = alfalah.core.newDataStore(
                "{{ url('/monitoring/2/10') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.ExpanderDs.load();

            var tpl = new Ext.XTemplate(
                //start table
                '<table class="x-data-table" id ="detail_id" width="60%" style="margin:5px;">',
                    //Header table
                    '<tr>',
                        '<th width="5%">No.</th>',
                        '<th width="5%">No.Pengajuan</th>',
                        '<th width="5%">No. Rapby</th>',
                        '<th width="10%">Kode Rekening</th>',
                        '<th width="20%">Uraian</th>',
                        '<th width="10%">Jumlah Pengajuan</th>',
                    '</tr>',
                    
                    '<tpl foreach=".">', //Start loop tpl
                    '<tr>',
                        '<td width="5%">{#}</td>',
                        '<td width="5%" id={pengajuan_no}> ini id</td>',
                        '<td width="5%">{rapbs_no}</td>',
                        '<td width="10%">{coa_id}</td>',
                        '<td width="20%">{uraian}</td>',
                        '<td width="10%">{total_biaya}</td>',
                    '</tr>',
                    '</tpl>', //End loop tpl
                '</table>' //Close table
    //            {compiled: true},
            );
           
            // Ext.getBody().on('click', function(event, target){
            //         var element = Ext.get(target);
            //         //perform your action here
            //         console.log('delegate event');
            //         console.log(target);
            //         }, null, { delegate: 'detail_id' });

            this.expander = new Ext.grid.RowExpander({
               autoLoad:"{{ url('/monitoring/2/10') }}",
            //    store :this.ExpanderDs,
                typeAhead: false,
                forceSelection: true,
                pageSize:25,
                hideTrigger:true,
                itemSelector: 'table.x-data-table,rapbs_no,uraian',
                autoLoad: true,
                
                tpl : tpl,
                listeners: {
                            'expand': {
                                element: 'el',
                                delegate: "table.x-data-table",
                                fn : function( expander, record, body, rowIndex) {

                                    console.log(expander);
                                    console.log(body);
                                    if (body){
                                     
                                    }


                                    

                                }
                            }
                        }
                // renderTo: Ext.getBody()
                    
            });



          
            this.Columns = [ 
                
                   this.expander,

                {   id : 'header_pengajuan_no_id',
                    header: "Nomor Pengajuan", width : 100,
                    dataIndex : 'pengajuan_no', sortable: true,
                    tooltip:"Nomor Uang muka",
                    
                },
                {   header: "Keterangan", width : 150,
                    dataIndex : 'pengajuan_keterangan_id',
                    sortable: true,
                    tooltip:"Keterangan Pengajuan",
                },
                {   header: "Organisasi / Urusan", width : 200,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 200,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Jumlah Pengajuan", width : 100,
                    // hidden:true,
                    dataIndex : 'total_biaya', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Status Release", width : 150,
                    dataIndex : 'status_pengajuan_id',
                    sortable: true,
                    tooltip:"Status Pengajuan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_1 == 0) { value = 'Not Yet release'; }
                        // not yet approve
                        else if  ( record.data.approve_status_1 == 1){ value = 'Release'; };
                        
                        return value;
                    }
                },
                {   header: "Approve Kabid", width : 150,
                    dataIndex : 'status_approve_2',
                    sortable: true,
                    tooltip:"Status Pengajuan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_2 == 0) { value = 'Not Yet Approve'; }
                        // not yet approve
                        else if  ( record.data.approve_status_2 == 1){ value = 'Approve by Kabid'; };
                        
                        return value;
                    }
                },
                {   header: "Approve Ketua", width : 150,
                    dataIndex : 'status_approve_3',
                    sortable: true,
                    tooltip:"Status Pengajuan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_2 == 0) { value = 'Not Yet Approve'; }
                        // not yet approve
                        else if  ( record.data.approve_status_2 == 1){ value = 'Approve by Kabid'; };
                        
                        return value;
                    }
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"monitoring Created Date",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"monitoring Last Updated",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
           
            this.Searchs_center = [
                {   id: 'monitoringum_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringum_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringum_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringum_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub. Keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn_center = new Ext.Button(
                {   id : tabId+"_monitoringumSearchBtn_center",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.monitoringum_search_handler_center,
                    scope : this
                });
                this.Searchs_south = [
                {   id: 'monitoringcair_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringcair_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringcair_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringcair_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub. Keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringcair_status',
                    cid: 'approve_status_3',
                    fieldLabel: 'By Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['label','key'],
                        data : [["ALL", 0], ["RECEIVED", 1]]
                    }),
                    displayField:'label',
                    valueField :'key',    
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100
                    // value: 'Active'
                },

            ];
            this.SearchBtn_south = new Ext.Button(
                {   id : tabId+"_monitoringumSearchBtn_south",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.monitoring_search_handler_south,
                    scope : this
                });

            /**
                * MAIN-GRID
            */

            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/monitoring/2/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );


            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
               plugins : this.expander,
 //               plugins: [this.expander],
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    '->',
                    'TOTAL PENGAJUAN',
                    new Ext.form.TextField(
                    {   id : 'grand_pengajuanum_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_monitoringumGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_monitoringumPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_monitoringumPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_monitoringumGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn_center.handler.call(this.SearchBtn_center.scope);
                                }
                            }
                        }),
                        ' records at a time tes'
                    ]
                })

            });






            /**
                * SOUTH-GRID
            */
            this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/monitoring/2/1') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   /*id : tabId+'_monitoringcairSouthGrid',
                jsId : tabId+'_monitoringcairSouthGrid',*/
                store:  this.SouthDataStore,
                columns: [ 
                    {   header: "Nomor Uang Muka", width : 100,
                    dataIndex : 'uangmuka_no', sortable: true,
                    tooltip:"Nomor Uang muka",
                },
                {   header: "RAPBS.No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"RAPBS No",
                },
                {   header: "Keterangan", width : 150,
                    dataIndex : 'uangmuka_keterangan_id',
                    sortable: true,
                    tooltip:"Keterangan Pengajuan",
                },
                {   header: "Organisasi / Urusan", width : 200,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 200,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Jumlah Pengajuan", width : 100,
                    // hidden:true,
                    dataIndex : 'total_biaya', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   id : 'status_pencairan_id',
                    header: "Status Pencairan", width : 200,
                    dataIndex : 'approve_status_3',
                    sortable: true,
                    tooltip:"Status Pencairan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //
                        if (( record.data.approve_status_2 == 1) && ( record.data.approve_status_3 == 0 )) { value = 'WAITING'; }
                        // not yet approve
                        else if  (( record.data.approve_status_2 == 1) && ( record.data.approve_status_3 == 1 )){ value = 'RECEIVED'; }
                        else { value = 'Not Yet Approved By Finance'; };
                        
                        return value;
                    }
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"monitoring Created Date",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"monitoring Last Updated",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
                ],

                // selModel: cbSelModel,
                //selModel: new Ext.grid.RowSelectionModel(),
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [
                    '->',
                    'TOTAL PENCAIRAN',
                    new Ext.form.TextField(
                    {   id : 'total_pencairan_id',
                        // store:this.DataStore,
                        displayField: 'total_pencairan',
                        valueField: 'total_pencairan',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_monitoringcairSouthGridBBar',
                    store: this.SouthDataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_monitoringcairSouthGridPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_monitoringcairSouthGridPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_monitoringcairSouthGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn_south.handler.call(this.SearchBtn_south.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_monitoringcairTab", 
                jsId : tabId+"_monitoringcairTab",
                title:  "MONITORING PENGAJUAN BULANAN",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    title: 'Monitoring Pengajuan Bulanan',
                    split: true,
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'ToolBox',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    { //  title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs_center,
                        tbar: [
                        {   text:'Search',
                            tooltip:'Search',
                            iconCls: 'silk-zoom',
                            handler : this.monitoring_search_handler_center,
                            scope : this,
                        }]
                        
                    },
                    { title : 'Setting'
                    }]
                },
                {// lazily created panel (xtype:'panel' is default)
                    region: 'south',
                    title: 'MONITORING STATUS PENCAIRAN',
                    split: true,
                    height : this.region_height/2,
                    layout : 'border',
                    collapsible: true,
                    margins: '0 0 0 0',
                    items :[ 
                    {   
                        region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.SouthGrid]
                    },
                    {   region: 'east',     // position for region
                        title: 'ToolBox',
                        split:true,
                        width: 200,
                        minSize: 175,
                        maxSize: 400,
                        collapsible: true,
                        layout : 'accordion',
                        items:[
                        {   title: 'S E A R C H',
                            labelWidth: 50,
                            defaultType: 'textfield',
                            xtype: 'form',
                            frame: true,
                            autoScroll : true,
                            items : this.Searchs_south, 
                            tbar: [
                                {   text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.monitoring_search_handler_south,
                                    scope : this,
                                }] 
                            
                        },
                        { title : 'Setting'
                        }]
                    }]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
            this.DataStore.on( 'load', function( store, records, options )
                {
                    console.log(records);
                    var total_pengajuan = 0;
                    Ext.each(records,
                        function(the_record)
                        {   console.log('cari record');
                            console.log(the_record.data.approve_status_3);
                           
                            total_pengajuan = total_pengajuan + parseInt(the_record.data.total_biaya);
                         });


                        Ext.getCmp('grand_pengajuanum_id').setValue(Ext.util.Format.number(total_pengajuan, '0,000'));
                });

                this.SouthDataStore.on( 'load', function( store, records, options )
                {
                    console.log(records);
                    var total_pencairan = 0;
                    Ext.each(records,
                        function(the_record)
                        {   console.log('cari record');
                            console.log(the_record.data.approve_status_3);
                            // if (the_record.data.approve_status_3==1)
                            total_pencairan = total_pencairan + parseInt(the_record.data.total_biaya);
                         });


                        Ext.getCmp('total_pencairan_id').setValue(Ext.util.Format.number(total_pencairan, '0,000'));
                });

                this.ExpanderDs.on( 'load', function( store, records, options )
                {
                    
                    Ext.each(records,
                        function(the_record)
                        {   console.log('nyobaaaaaaaaaaaaaaa');
                            console.log(the_record.data.pengajuan_no);
                         });

                });


        },

        // monitoringcair search button
        monitoring_search_handler_center : function(button, event)
        {   var the_search_center = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_monitoringcairSaveBtn').handler.call();
                                    the_search_center = false;
                                } else the_search_center = true;
                            },
                        icon: Ext.MessageBox.WARNING
                        });
            };

            if (the_search_center == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs_center);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.monitoring.task,
                        act: alfalah.monitoring.act,
                        s:"form", 
                        limit:this.page_limit, start:this.page_start });
                        this.DataStore.reload();
            };
        },

        monitoring_search_handler_south : function(button, event)
        {   var the_search_south = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_monitoringcairSaveBtn').handler.call();
                                    the_search_south = false;
                                } else the_search_south = true;
                            },
                        icon: Ext.MessageBox.WARNING
                        });
            };

            if (the_search_south == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs_south);
                this.SouthDataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.monitoring.task,
                        act: alfalah.monitoring.act,
                        s:"form", 
                        limit:this.page_limit, start:this.page_start });
                this.SouthDataStore.reload();
            };
        },        
    }; // end of public space
}(); // end of app
alfalah.monitoring.allstatus= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
 
            this.Columns = [
              
                {   header: "RAPBS.No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"RAPBS No",
                },
                {   header: "Pengajuan.No", width : 100,
                    dataIndex : 'pengajuan_no', sortable: true,
                    tooltip:"PENGAJUAN No",
                },
                {   header: "Uang Muka .No", width : 100,
                    dataIndex : 'uangmuka_no', sortable: true,
                    tooltip:"UANG MUKA No",
                },
                {   header: "Organisasi / Urusan", width : 200,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 200,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Jumlah Tahun Ke N", width : 100,
                    dataIndex : 'jumlahke_n', sortable: true,
                    tooltip:"Jumlah Anggaran Sampai dengan Tahun Lalu",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   value = (record.data.total_biaya);
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Pengajuan", width : 100,
                    dataIndex : 'total_biaya_ajuan', sortable: true,
                    tooltip:"Jumlah pada waktu pengajuan biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   value = (record.data.total_biaya_ajuan);
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Uang Muka", width : 100,
                    dataIndex : 'jumlah_uangmuka', sortable: true,
                    tooltip:"jumlah pada waktu pengajuan uang muka",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   value = (record.data.jumlah_uangmuka);
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Tgl Pengesahan Ketua", width : 100,
                    dataIndex : 'ketua_approve_date', sortable: true,
                    tooltip:"tanggal pengesahan oleh ketua yayasan",
                },
                {   header: "Tgl Pengajuan", width : 100,
                    dataIndex : 'pengajuan_approve_date', sortable: true,
                    tooltip:"tanggal pengajuan",
                },
                {   header: "Tgl Pencairan", width : 100,
                    dataIndex : 'pencairan_approve_date', sortable: true,
                    tooltip:"tanggal pencairan",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    hidden:true,
                    tooltip:"allstatus Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    hidden:true,
                    tooltip:"allstatus Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                // {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'allstatus_organisasi',
                    cid: 'organisasi_mrapbs_id_name',
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'allstatus_urusan',
                    cid: "urusan_mrapbs_id_name",
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'allstatus_kegiatan',
                    cid: 'kegiatan_mrapbs_id_name',
                    fieldLabel: 'Kegiatan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'allstatus_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'allstatus_status',
                    cid: 'status',
                    hidden :true,
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },

            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_allstatusSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.allstatus_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/monitoring/1/15')}}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.monitoring.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    // '->',
                    // '_T O T A L_',
                    new Ext.form.TextField(
                    {   id : 'grand_total_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        hidden : true,
                        readOnly : true,
                        // style: "text-align: right",
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        scope : this,
                    }),
                        ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_allstatusGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_allstatusPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_allstatusPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_allstatusGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_allstatusSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_allstatusTab",
                jsId : tabId+"_allstatusTab",
                title:  "ALL STATUS",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.allstatus_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        //  {},

        // Grid_grand_total : function(the_cell)
        {
            this.DataStore.on( 'load', function( store, records, options )
            {
                console.log(records);
                var total_data = 0;
                Ext.each(records,
                    function(the_record)
                    { 
                        total_data = total_data + parseInt(the_record.data.total_biaya);
                    });

                console.log("total jumlah = "+total_data);
                Ext.getCmp('grand_total_id').setValue(Ext.util.Format.number(total_data, '0,000'));
            });
        },

        
        Gridafteredit : function(the_cell)
        { switch (the_cell.field)
            {   case "volume":
                case "tarif":
                {
                    var total_biaya = 0;
                    Ext.each(the_data,
                    function(the_record)
                    { 
                        total_biaya = total_biaya + parseInt(the_record.data.jumlah);
                    });
            
                    Ext.getCmp('total_biaya_id').setValue(Ext.util.Format.number(total_nota, '0,000'));

                }; 
                break; 
            };
        },


        // allstatus search button
        allstatus_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_allstatusSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.monitoring.task,
                        act: alfalah.monitoring.act,
                        a:3, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app

// On Ready
Ext.onReady(alfalah.monitoring.initialize, alfalah.monitoring);
// end of file
</script>
<style>
    .x-data-table {
        border-collapse: collapse;
        border-spacing: 0;
        min-width: 100%
    }

    .x-data-table th {
        text-align: left;
        background-color: #80ffff;
    }

    .x-data-table td,
    th {
        padding: .5em;
        border: 1px solid #999;
    }
</style>

<div>&nbsp;</div>