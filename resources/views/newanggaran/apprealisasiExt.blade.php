<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.apprealisasi'); // daftar pengajuan
// create application
alfalah.apprealisasi = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',

        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.apprealisasi.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.apprealisasi.Tab);
            this.centerPanel.setActiveTab(this.apprealisasi.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.apprealisasi.apprealisasi= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
            { // this.region_height = alfalah.apprealisasi.centerPanel.container.dom.clientHeight;
                this.Columns = [
                {   header: "Nomor Uang Muka", width : 100,
                    dataIndex : 'uangmuka_no', sortable: true,
                    tooltip:"Nomor Uang muka",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_4 == 1) { metaData.attr = "style = background-color:lime"; }
                        // not yet approve
                        else if ( record.data.approve_status_4 == 0){ metaData.attr = "style = background-color:yellow;"; }
                        // rejected
                        else {  metaData.attr = "style = background-color:red;"; };
                        return value;
                    }
                },
                {   header: "Keterangan", width : 150,
                    dataIndex : 'uangmuka_keterangan_id',
                    sortable: true,
                    tooltip:"Keterangan Pengajuan",
                },
                {   header: "Organisasi / Urusan", width : 150,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 250,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Jumlah Pencairan", width : 100,
                    dataIndex : 'total_biaya', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Realisasi", width : 100,
                    dataIndex : 'total_realisasi', sortable: true,
                    tooltip:"total biaya yang telah direalisasi",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Realisasi Hasil", width : 150,
                    dataIndex : 'realisasi_hasil',
                    sortable: true,
                    tooltip:"realisasi hasil anggaran",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  { metaData.attr = "style = background-color:lime"; };
                        result = '<b>'+value+'</b><br>';
                        return alfalah.core.gridColumnWrap(result);
                    }

                },
                {   header: "Pengajuan by", width : 150,
                    dataIndex : 'username', sortable: true,
                    tooltip:"Diajukan oleh",
                },
                {   header: "Status", width : 50,
                    hidden:true,
                    dataIndex : 'status', sortable: true,
                    tooltip:" Status",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"apprealisasi Created Date",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"apprealisasi Last Updated",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                // {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'apprealisasi_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield', 
                    width : 120
                },
                {   id: 'apprealisasi_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'apprealisasi_kegiatan',
                    cid: 'kegiatan_mrapbs_id_name',
                    fieldLabel: 'Kegiatan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'apprealisasi_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_apprealisasiSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.apprealisasi_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/realisasi/2/0')}}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
        //    console.log(this.DataStore.load());
            this.Grid = new Ext.grid.EditorGridPanel(
            {   
                store:  this.DataStore,
                columns: this.Columns,
                enableColLock: false,
                loadMask: true,
                height : alfalah.apprealisasi.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {  
                        text:'View Realisasi', 
                        tooltip:'view detail biaya dan nota',
                        iconCls: 'silk-application-view-list',
                        // handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        handler : this.Grid_detail,
                        scope : this
                    },
                    {   text:'Save',
                        tooltip:'Save hasil',
                        hidden:true,
                        iconCls: 'icon-save',
                        handler : this.Grid_savehasil,
                        scope : this
                    },
                    {   text:'Approve',
                        tooltip:'approval realisasi',
                        style:'background-color:lime',
                        iconCls: 'silk-tick',
                        handler : this.Grid_approve_realisasi,
                        scope : this
                    },
                    {   print_type : "pdf",
                        text:'Print PDF', 
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : this.Grid_pdf,
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        hidden :true,
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                    {   text:'Approve apprealisasi',
                        tooltip:'apprealisasi Anggaran',
                        hidden :true,
                        style:'background-color:lime',
                        iconCls: 'silk-tick',
                        handler : this.Grid_approve,
                        scope : this
                    },'-',
                    '->',
                '_T O T A L_',
                new Ext.form.TextField(
                {   id : 'grand_total_id',
                    // store:this.DataStore,
                    displayField: 'total_data',
                    valueField: 'total_data',
                    allowBlank : false,
                    readOnly : true,
                    style: "text-align: right; background-color: #ffff80; background-image:none;",
                    mode: 'local',
                    forceSelection: true,
                    triggerAction: 'all',
                    selectOnFocus: true,
                    // renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    //     {
                    //         metaData.attr = "style = background-color:lime"; 
                    //     },

                    // listeners :
                    // {   "beforeedit" : this.Grid_grand_total },
                    // scope : this
                }),

                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_apprealisasiGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_apprealisasiPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_apprealisasiPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_apprealisasiGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                })
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_apprealisasiTab",
                jsId : tabId+"_apprealisasiTab",
                title:  "Approval Realisasi",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.apprealisasi_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
            this.DataStore.on( 'load', function( store, records, options )
            {
                console.log(records);
                var total_data = 0;
                Ext.each(records,
                    function(the_record)
                    { 
                        total_data = total_data + parseInt(the_record.data.total_biaya);
                    });

                console.log("total jumlah = "+total_data);
    //                console.log(Ext.getCmp('grand_total_id').setValue(total_data));
                Ext.getCmp('grand_total_id').setValue(Ext.util.Format.number(total_data, '0,000'));

            });
            // this.DataStore.load();
           
        },

        // Grid_pdf : function(button, event)
        // {     var the_record = this.Grid.getSelectionModel().selection;
        //     if ( the_record )
        //     {   the_record = the_record.record.data;
        //         console.log(the_record)
        //         alfalah.core.printOut(button, event, this.DataStore.proxy.url, 
        //             {   s: "form",
        //                 uangmuka_no : the_record.uangmuka_no });
        //     }
        //     else
        //     {   Ext.Msg.show(
        //             {   title:'I N F O ',
        //                 msg: 'No Data Selected ! ',
        //                 buttons: Ext.Msg.OK,
        //                 icon: Ext.MessageBox.INFO
        //             });
                
        //     };
        // },

        Grid_pdf : function(button, event)
        {     var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                console.log(the_record)
                if (the_record.approve_status_3==1)
                {
                    console.log('the_record.approve_status_4');
                    console.log(the_record.approve_status_4);
                    alfalah.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        uangmuka_no : the_record.uangmuka_no });
                }
                else
                {
                    Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'Must be approved by Finance ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                    
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };
        },


        Grid_detail : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
                { 
                    var centerPanel = Ext.getCmp('center_panel');
                    alfalah.apprealisasi.forms.initialize(the_record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.apprealisasi.forms.Tab);
                    centerPanel.setActiveTab(alfalah.apprealisasi.forms.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                }
            
            else
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Data Selected ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });
                };
        },

        Grid_approve_realisasi : function(button, event) 
        {
         //   var the_record = this.Grid.getSelectionModel().selection;
            var the_record = alfalah.core.getDetailData(alfalah.apprealisasi.apprealisasi.Grid.getSelectionModel().selection.record.data);
           
            if ( the_record )
            {   Ext.Msg.show(
                    {   title :'A P P R O V A L',
                        msg : 'APPROVE ?',
                        width:250,
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                        {   if (buttonId =='yes')
                            {  
                                alfalah.core.submitGrid(
                                    alfalah.apprealisasi.apprealisasi.DataStore,
                                            "{{ url('/realisasi/2/705') }}",
                                        {   'x-csrf-token': alfalah.apprealisasi.sid }, 
                                        {   uangmuka_no: alfalah.apprealisasi.apprealisasi.Grid.getSelectionModel().selection.record.data.uangmuka_no 
                                            
                                        }
                                );
                            } else
                            { this.DataStore.reload(); };
                        },
                    icon: Ext.MessageBox.WARNING
                    }
                );
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },

        // apprealisasi search button
        apprealisasi_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_apprealisasiSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.apprealisasi.task,
                        act: alfalah.apprealisasi.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.apprealisasi.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
// execute at the first time
    // private variables
    this.Tab;
    this.Searchs;
    this.SearchBtn;
    this.Columns;
    this.Grid;
    this.updateTab;
    this.updateForm;
    this.Records;
    this.DetailRecords;

    this.EastGrid;
    this.EastDataStore;
    this.EastGridPanel;
    this.EastRecords;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        load_1     : true,
        // user_data : '',
        // public methods
        initialize: function(the_records)  
        {   console.log('initialize forms');
            if (the_records)
            {   console.log(the_records);
                this.Records = the_records; 
            };
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.total_nota();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        { this.region_widht = alfalah.apprealisasi.centerPanel.container.dom.clientWidht;
              console.log('prepare');
            console.log(this.Records);

            /************************************
                G R I D S
            ************************************/ 
            // var cbSelModel = new Ext.grid.CheckboxSelectionModel(); 

            this.Columns = [ 
                // cbSelModel,
                {   header: "ID", width : 50,
                    dataIndex : 'rapbs_id', sortable: true,
                    tooltip:"ID",
                    hidden: true,
                },
                {   header: "ID", width : 100,
                    dataIndex : 'id', sortable: true,
                    tooltip:"id number",
                    hidden: false,
                },
                {   header: "No Kegiatan", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"Doc.No",
                    hidden: true,
                },
                {   header: "Kode Rek", width : 60,
                    dataIndex :'coa_id', sortable: true,
                    tooltip:"Pos Belanja",
                    readonly: true,
                },
                {   header: "Nama Rekening", width : 150,
                    dataIndex : 'coa_name', sortable: true,
                    readonly:true,
                    tooltip:"Pos Belanja Name",
                    editor: new Ext.form.ComboBox(
                    {   store: this.coaDS_2,
                        typeAhead: false,
                        width: 250,
                        readonly:true,
                        displayField: 'display',
                        valueField: 'coa_name',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        // triggerAction: "All",
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.coa_id = record.data.mcoa_id;
                                    combo.gridEditor.record.data.coa_name = record.data.coa_name;
                                    alfalah.apprealisasi.forms.Grid.getView().refresh();
                                },
                        }
                    }),
                },
                {   header: "Uraian", width : 150,
                    dataIndex : 'uraian', sortable: true,
                    tooltip:"Uraian",
                    readonly:true,
                },
                {   header: "Volume", width : 50,
                    dataIndex : 'volume', sortable: true,
                    tooltip:"Volume",
                    readonly:true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.volume = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Satuan", width : 50,
                    dataIndex : 'satuan', sortable: true,
                    tooltip:"Satuan",
                    readonly:true,
                },
                {   header: "Tarif", width : 100,
                    dataIndex : 'tarif', sortable: true,
                    tooltip:"Tarif",
                    readonly:true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        metaData.attr="style = text-align:right;";
                        record.data.tarif = value;
                        return Ext.util.Format.number(value, '0,000');
                        
                    }
                },
                {   header: "Uang Muka", width : 120,
                    dataIndex : 'jum_ajuan', sortable: true,
                    readonly:true,
                    tooltip:"Uang Muka",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        metaData.attr="style = text-align:right;";
                        record.data.jumlah = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Ber-ulang", width : 60,
                    dataIndex : 'berulang', sortable: true,
                    hidden : true,
                    tooltip:"variabel pembagi anggaran yg berulang",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Status", width : 100,
                    dataIndex : 'status_name', sortable: true,
                    readonly:true,
                    hidden : true,
                    tooltip:"Status",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                            fields: ["label"],
                                            data : [["Active"], ["InActive"]]
                                        }),
                            displayField:"label",
                            valueField:"label",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
            ];
            
            this.DataStore = alfalah.core.newDataStore( 
                    "{{ url('/realisasi/2/10') }}", false,
                    {   s:"form", limit:this.page_limit, start:this.page_start }
                );

            //  this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                // selModel: cbSelModel,
                enableColLock: false,
                loadMask: true,
                widht : (this.region_widht/2)-50, 
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Save',
                        tooltip:'apprealisasi nota',
                        iconCls: 'icon-save',
                        hidden:true,
                        handler : this.Grid_savenota,
                        scope : this
                    },
                    {   text:'Add Item Nota',
                        tooltip:'tambah item nota',
                        iconCls: 'silk-add',
                        hidden :true,
                        handler : this.Grid_addnota,
                        scope : this
                    },
 
                    '-',
                    {   text:'Approve',
                        tooltip:'Approve Record',
                        iconCls: 'silk-tick',
                        hidden :true,
                        handler : this.Grid_approve,
                        scope : this
                    },
                    '-',
                    '_T O T A L_B I A Y A_  ',
                    new Ext.form.TextField(
                    {   id : 'biaya_id',
                        store:this.DataStore,
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        displayField: 'total_biaya',
                        valueField: 'total_biaya',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        // value: 'total_biaya',
                        listeners : { scope : this,
                            "render" : function ()
                            { Ext.getCmp("biaya_id").setValue(Ext.util.Format.number(alfalah.apprealisasi.forms.Records.record.data.total_biaya, '0,000'));
                               
                            }
                        }
                    }),
                    '-',
                    '_T O T A L_N O T A_  ',
                    new Ext.form.TextField(
                    {   id : 'grand_total_nota_id',
                        store:this.EastDataStore,
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        displayField: 'grand_total_nota',
                        valueField: 'grand_total_nota',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        value: 'grand_total_nota',
                        listeners : { scope : this,
                            "render" : function ()
                            { Ext.getCmp("grand_total_nota_id").setValue(Ext.util.Format.number(alfalah.apprealisasi.forms.Records.record.data.total_apprealisasi, '0,000'));
                               
                            }
                        }
                    }),
                    '-',
                    '_S I S A_  ',
                    new Ext.form.TextField(
                    {   id : 'sisa_id',
                        store:this.EastDataStore,
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #80ff00; background-image:none;",
                        mode: 'local',
                        displayField: 'sisa',
                        valueField: 'sisa',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        value: 'sisa',
                        listeners : { scope : this,
                            "render" : function ()
                            {   var total_nota = alfalah.apprealisasi.forms.Records.record.data.total_apprealisasi;
                                var total_biaya =  alfalah.apprealisasi.forms.Records.record.data.total_biaya;
                                var sisa = total_biaya - total_nota;
                                Ext.getCmp("sisa_id").setValue(Ext.util.Format.number(sisa, '0,000'));
                            }
                        }
                    }),


                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_formsGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_formsPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_formsPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_formsGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
                listeners : { scope : this,
                            'cellclick': function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent)
                            {
                                console.log('cell click uhuiiii');
                                console.log(iColIdx);
                                if (iColIdx == 1 ) // kolom pertama
                                {   the_record = alfalah.apprealisasi.forms.Grid.getSelectionModel().selection.record.data.id
                                    console.log(the_record);
                                    // this.eastdatastore.baseParam = {new param in here};
                                    this.EastDataStore.removeAll();
                                    alfalah.apprealisasi.forms.EastDataStore.baseParams = {   task: alfalah.apprealisasi.task,
                                        act: alfalah.apprealisasi.act,
                                        a:2, b:0, s:"form", id : the_record,
                                        limit:this.page_limit, start:this.page_start 
                                    };
                                    console.log('alfalah.apprealisasi.form.EastDataStore');    
                                    console.log(alfalah.apprealisasi.forms.EastDataStore);             
                                   alfalah.apprealisasi.forms.EastDataStore.reload();
                        
                                };
                            },
                            'rowselect' : function (a,b,c)
                            {   console.log('listener grid select');
                                console.log(a);
                                console.log(b);
                                console.log(c);
                            }
                        }
            });

            /*************
            * EAST GRID
            *************/
            this.EastDataStore = alfalah.core.newDataStore(
                "{{ url('/realisasi/2/11') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            // this.EastDataStore.load();
            
             this.EastGridPanel = new Ext.grid.EditorGridPanel( 
             {  store : this.EastDataStore,
                
                columns : [
                {   header: "ID", width : 20,
                    dataIndex : 'biaya_id', sortable: true, 
                    tooltip:"ID",
                    hidden: true,
                },
                {   header: "Nota No.", width : 60,
                    dataIndex : 'nota_no', sortable: true,
                    tooltip:"nomor nota",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Tanggal Nota", width : 60,
                    dataIndex : 'invoice_date', sortable: true,
                    tooltip: "masukan tanggal nota",
                    editor      : new Ext.form.DateField({
                        allowBlank : false,
                    }),
                },
                {   header: "Keterangan", width : 125,
                    dataIndex : 'keterangan_nota', sortable: true,
                    tooltip:"keterangan dari nota",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Volume", width : 50,
                    dataIndex : 'volume_nota', sortable: true,
                    tooltip:"Volume",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.volume = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Satuan", width : 50,
                    dataIndex : 'satuan_nota', sortable: true,
                    tooltip:"Satuan",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Tarif", width : 100,
                    dataIndex : 'tarif_nota', sortable: true,
                    tooltip:"Tarif",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;"; 
                        record.data.tarif = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {  
                    header: "Jumlah", width : 80,
                    dataIndex : 'jumlah_nota', sortable: true,
                    readonly:true,
                    // css : "background-color: #ffff80;",
                    tooltip:"Jumlah",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        value = parseInt(record.data.volume_nota) * parseInt(record.data.tarif_nota);
                        record.data.jumlah_nota = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
            ],
            tbar: [
                    {   text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        hidden :true,
                        scope : this
                    },
                    '-',
                    {   text:'Add Item',
                        tooltip:'tambah nota',
                        iconCls: 'silk-add',
                        hidden :true,
                        handler : this.Grid_addnota,
                        scope : this
                    },
                    // '->',
                    // '_T O T A L_N O T A',
                    // new Ext.form.TextField(
                    // {   id : 'total_nota_id',
                    //     // store:this.DataStore,
                    //     allowBlank : false,
                    //     readOnly : true,
                    //     style: 'text-align: right',
                    //     mode: 'local',
                    //     forceSelection: true,
                    //     triggerAction: 'all',
                    //     selectOnFocus: true,
                    // }),

                ],
                listeners :
                    {   "afteredit" : this.Gridafteredit },
                scope : this,

                bbar: new Ext.Toolbar(
                    {   id : 'bbarEast',
                        height : 100,
                        layout : 'form',
                    items:
                        [ '->',                          
                            new Ext.form.TextField(
                            {   id : 'total_nota_id',
                                // store:this.DataStore,
                                fieldLabel: 'Total Nota ',
                                allowBlank : false,
                                readOnly : true,
                                style: "text-align: right; background-color: #ffff80; background-image:none;",
                                mode: 'local',
                                forceSelection: true,
                                triggerAction: 'all',
                                selectOnFocus: true,
                                
                            }), 
                            
                            new Ext.form.TextField(
                            {   id : 'total_nota_id_1',
                                // store:this.DataStore,
                                fieldLabel: 'Sisa ',
                                allowBlank : false,
                                readOnly : true,
                                style: 'text-align: right',
                                mode: 'local',
                                forceSelection: true,
                                triggerAction: 'all',
                                selectOnFocus: true,
                            }),
                        ],
                }),
                

        });         

        this.EastRecords = Ext.data.Record.create(
        [   {name: 'id', type: 'integer'},
            {name: 'nota_no', type: 'string'},
            {name: 'keterangan_nota', type: 'string'},
            {name: 'volume_nota', type: 'string'},
            {name: 'tarif_nota', type: 'string'},
            {name: 'jumlah_nota', type: 'string'},
            {name: 'created_date', type: 'date'},
            {name: 'modified_date', type: 'date'},
        ]);

            this.detailTab = new Ext.Panel(
            {   id: "notaTab",
                title:  "D E T A I L   N O T A",
                region: 'center',
                layout: 'border',
                
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    // css : "background-color: #ffff80;",       
                    items:[this.EastGridPanel]
                }],
            });
        },

        // build the layout
        build_layout: function()
        {   
            if (Ext.isObject(this.Records))
                {   the_title = "Detail Biaya"; }
            else { the_title = "Edit Detail Biaya"; };

            this.Tab = new Ext.Panel(
            {   id : tabId+"_FormsTab",
                jsId : tabId+"_FormsTab",
                title:  '<span style="background-color: yellow;">'+the_title+'</span>',
                region: 'center',
                closable : true,
                
                layout: 'border',
                items: [
                    {   
                    region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[
                        {   region : 'center',
                            width: 500,
                            minSize: 175,
                            maxSize: 400,
                            layout: 'fit',
                            
                            items : [this.Grid]
                        },
                       ]
                    },
                    {  // title: 'N O T A',
                    region: 'east',     // position for region
                    split:true,
                    width: 500,
                    minSize: 175,
                    maxSize: 400,
                    // collapsible: true,
                    
                    layout : 'fit',
                    items: [this.detailTab]
                    },
                ]
            });
        },
        // finalize the component and layout drawing

        finalize_comp_and_layout: function()
      
        {   
            the_records = this.Records
            if (the_records) {

            this.DataStore.baseParams = Ext.apply( the_parameter,    
            {   s:"form", limit:this.page_limit, start:this.page_start,
                uangmuka_no : the_records.record.data.uangmuka_no
            })
            this.DataStore.load();}    
        },

        total_nota: function()
        { 
            alfalah.apprealisasi.forms.EastDataStore.on( 'load', function( store, records, options )
            {
                console.log(records);
                var total_nota = 0;
                var total_nota_all = alfalah.apprealisasi.forms.Records.record.data.total_apprealisasi;
                var total_biaya =  alfalah.apprealisasi.forms.Records.record.data.total_apprealisasi;
                Ext.each(records,
                    function(the_record)
                    { 
                        total_nota = total_nota + parseInt(the_record.data.jumlah_nota);
                    });

                // console.log("total jumlah = "+total_nota);
                Ext.getCmp('total_nota_id').setValue(Ext.util.Format.number(total_nota, '0,000'));
                var sisa = alfalah.apprealisasi.forms.Grid.getSelectionModel().selection.record.data.jum_ajuan-total_nota;
                Ext.getCmp('total_nota_id_1').setValue(Ext.util.Format.number(sisa, '0,000'));

                var total_sisa = total_biaya-total_nota_all;
            });
        },

        Grid_addnota: function(button, event)
        { 
            var selected = alfalah.apprealisasi.forms.Grid.getSelectionModel().selection.record.data;
            if (selected){
                
                this.EastGridPanel.stopEditing();
                this.EastGridPanel.store.insert( 0,
                new this.EastRecords  (
                {   uangmuka_no : selected.uangmuka_no,
                    pengajuan_no : selected.pengajuan_no,
                    rapbs_no :selected.rapbs_no,
                    biaya_id : selected.id,
                    coa_id : selected.coa_id,
                    id:'',
                    nota_no: "",
                    keterangan_nota: "new keterangan",
                    volume_nota: 0,
                    satuan_nota: "unit",
                    jumlah_nota: 0,
                    created_date: "",
                    modified_date:""
                }));
            // placed the edit cursor on 2-column
            this.EastGridPanel.startEditing(0, 1);
        }
            else
            {   Ext.Msg.show( //ini msh blom bisa keluar kenapa ?????
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },

        Grid_savenota : function(button, event)
        { 
        json_data = alfalah.core.getDetailData(alfalah.apprealisasi.forms.EastDataStore.getModifiedRecords());
        // check data modification
            console.log('detail_data');
            console.log(json_data);
 //           data_detail = Ext.apply(json_data);
            alfalah.core.submitGrid(
            alfalah.apprealisasi.forms.EastDataStore,
            "{{ url('/apprealisasi/1/1') }}",
            {   'x-csrf-token': alfalah.apprealisasi.sid },
            {   task: 'save',
 //           head_data : Ext.encode(head),
                json : Ext.encode(json_data),
        });

    },

    Gridafteredit : function(the_cell)
        { switch (the_cell.field)
            { case "volume_nota":
                case "tarif_nota":
                {
                    var the_data = alfalah.apprealisasi.forms.EastDataStore.data.items;
                    var total_nota = 0;
                    Ext.each(the_data,
                    function(the_record)
                    { 
                        total_nota = total_nota + parseInt(the_record.data.jumlah_nota);
                    });
            
                    Ext.getCmp('total_nota_id').setValue(Ext.util.Format.number(total_nota, '0,000'));
                    var sisa = alfalah.apprealisasi.forms.Grid.getSelectionModel().selection.record.data.jum_ajuan-total_nota;
                    Ext.getCmp('total_nota_id_1').setValue(Ext.util.Format.number(sisa, '0,000'));

                }; 
                break;
            };
        },

    }; // end of public space
}(); // end of app
//onready
Ext.onReady(alfalah.apprealisasi.initialize, alfalah.apprealisasi);
// end of file
</script>
<div>&nbsp;</div>