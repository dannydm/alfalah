<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.newrapbsbend');
// create application
alfalah.newrapbsbend = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',

        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.newrapbsbend.initialize();
            this.newrapbsbendserapan.initialize();
            this.spj.initialize();
            this.newrapbsbendcashflow.initialize();

        },

        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.newrapbsbend.Tab);
            this.centerPanel.add(this.newrapbsbendserapan.Tab);
            this.centerPanel.add(this.spj.Tab);
            this.centerPanel.add(this.newrapbsbendcashflow.Tab);
            this.centerPanel.setActiveTab(this.newrapbsbend.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {  },
    }; // end of public space
}(); // end of app
// create application
alfalah.newrapbsbend.newrapbsbend = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        { 
             this.region_height = alfalah.newrapbsbend.centerPanel.container.dom.clientHeight;
            this.Columns = [  
                
                {   header: "RAPBS.No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    hidden : true,
                    tooltip:"RAPBS No",
                },
                {   header: "Organisasi / Urusan", width : 150,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 150,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 100,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Sumber Dana", width : 150,
                    dataIndex : 'sumberdana_id_name', sortable: true,
                    tooltip:"sumber pendanaan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Keluaran", width : 150,
                    dataIndex : 'keluaran', sortable: true,
                    tooltip:"keluaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {
                        return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Hasil", width : 150,
                    dataIndex : 'hasil', sortable: true,
                    tooltip:"hasil",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Sasaran", width : 150,
                    dataIndex : 'sasaran', sortable: true,
                    tooltip:"sasaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Waktu", width : 150,
                    dataIndex : 'jadwal', sortable: true,
                    tooltip:"Waktu Pelaksanaan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.tgl_mulai+'<br>'+record.data.tgl_selesai;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Jumlah Biaya", width : 100,
                    id : 'total_biaya_urusan',
                    hidden:true,
                    dataIndex : 'total_biaya', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Tahun Lalu", width : 100,
                    dataIndex : 'jumlahn', sortable: true,
                    tooltip:"Jumlah Anggaran Tahun Sebelumnya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Tahun Ke N", width : 100,
                    dataIndex : 'jumlahke_n', sortable: true,
                    tooltip:"Jumlah Anggaran Sampai dengan Tahun Lalu",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   value = (record.data.total_biaya);
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "% perubahan", width : 100,
                    dataIndex : 'j_persen', sortable: true,
                    tooltip:"Persentase naik / turun",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        t_pengurang = ((record.data.total_biaya)-(record.data.jumlahn));
                        value = (t_pengurang/(record.data.jumlahn)) * 100;
                        
                        if (value < 100.00) 
                            { metaData.attr = "style = background-color:cyan; "; }
                        else {  metaData.attr = "style = background-color:red; "; };
                        return Ext.util.Format.number(value, '000.00');

                    },
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    hidden :true,
                    tooltip:" Status",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"Created Date",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"Last Updated",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Searchs = [
                {   id: 'newrapbsbend_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'newrapbsbend_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'newrapbsbend_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'newrapbsbend_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub. Keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'newrapbsbend_sumberdana',
                    cid: 'sumber_dana_id_name',
                    fieldLabel: 'Dana',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'newrapbsbend_status',
                    cid: 'status',
                    hidden :true,
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_newrapbsbendSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.newrapbsbend_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/newrapbsbend/1/0')}}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [ 
                        {   print_type : "xls",
                            text:'Print Excell',
                            tooltip:'Print to Excell SpreadSheet',
                            iconCls: 'silk-page-white-excel',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },'-',


                    ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_newrapbsbendGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_newrapbsbendPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_newrapbsbendPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_newrapbsbendGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });


    // *****************************************
        
    },
    // build the layout
    build_layout: function()
    {   this.Tab = new Ext.Panel(
        {   id : tabId+"_newrapbsbendTab",
            jsId : tabId+"_newrapbsbendTab",
            title:  "REKAPITULASI VERIFIKASI ANGGARAN",
            region: 'center',
            layout: 'border',
            items: [
            {   region: 'center',     // center region is required, no width/height specified
                xtype: 'container',
                layout: 'fit',
                items:[this.Grid]
            },
            {   title: 'Parameters',
                region: 'east',     // position for region
                split:true,
                width: 200,
                minSize: 175,
                maxSize: 400,
                collapsible: true,
                layout : 'accordion',
                items:[
                {   //title: 'S E A R C H',
                    labelWidth: 50,
                    defaultType: 'textfield',
                    xtype: 'form',
                    frame: true,
                    autoScroll : true,
                    items : this.Searchs,
                    tbar: [
                        {   text:'Search',
                            tooltip:'Search',
                            iconCls: 'silk-zoom',
                            handler : this.newrapbsbend_search_handler,
                            scope : this,
                        }]
                }]
            },
            ]
        });
    },
    // finalize the component and layout drawing
    finalize_comp_and_layout: function()
    {

    },
    // newrapbsbend search button
    newrapbsbend_search_handler : function(button, event)
    {   var the_search = true;
        if ( this.DataStore.getModifiedRecords().length > 0 )
        {   Ext.Msg.show(
                {   title:'W A R N I N G ',
                    msg: ' Modified Data Found, Do you want to save it before search process ? ',
                    buttons: Ext.Msg.YESNO,
                    fn: function(buttonId, text)
                        {   if (buttonId =='yes')
                            {   Ext.getCmp(tabId+'_newrapbsbendSaveBtn').handler.call();
                                the_search = false;
                            } else the_search = true;
                        },
                    icon: Ext.MessageBox.WARNING
                    });
        };

        if (the_search == true) // no modification records flag then we can go to search
        {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
            this.DataStore.baseParams = Ext.apply( the_parameter,
                {   task: alfalah.newrapbsbend.task,
                    act: alfalah.newrapbsbend.act,
                    a:2, b:0, s:"form",
                    limit:this.page_limit, start:this.page_start });
            this.DataStore.reload(); 
        };
    },
    }; // end of public space
}(); // end of app
alfalah.newrapbsbend.newrapbsbendserapan = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
            {  
                this.SerapanDs = alfalah.core.newDataStore(
                    "{{ url('/newrapbsbend/2/0') }}", false,
                    {   s:"form", limit:this.page_limit, start:this.page_start });
                    this.SerapanDs.load();

                this.region_height = alfalah.newrapbsbend.centerPanel.container.dom.clientHeight;
                this.Columns = [
                   // store : this.SerapanDs,
                {   header: "Organisasi ", width : 150,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    // hidden : true,
                    tooltip:"Jenjang/Bidang/Departemen",
                },
                {   header: "Urusan", width : 200,
                    dataIndex : 'urusan_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Anggaran thn lalu", width : 110,
                    dataIndex : 'jumlahn', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya tahun lalu",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Usulan Thn ini", width : 110,
                    dataIndex : 'total_biaya', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        value = (record.data.total_biaya) - (record.data.total_perubahan);
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Perubahan Anggaran", width : 120,
                    dataIndex : 'total_perubahan', sortable: true,
                    tooltip:"Jumlah total perubahan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Total Anggaran", width : 120,
                    dataIndex : 'total_anggaran', sortable: true,
                    tooltip:"Jumlah total perubahan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        value = (record.data.total_biaya)
                        return Ext.util.Format.number(value, '0,000');
                    },
                },

                {   header: "% Thn Ini Vs Thn Lalu", width : 120,
                    dataIndex : 'j_persen', sortable: true,
                    tooltip:"Persentase naik / turun",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        t_pengurang = ((record.data.total_biaya)-(record.data.jumlahn));
                        value = (t_pengurang/(record.data.jumlahn)) * 100;
                        if ((value > 0.00) && (value < 100.00)) 
                            { metaData.attr = "style = background-color:cyan;bg-image:arrow-up.gif;"; }
                        else if (value < 0.00)
                        { metaData.attr = "style = background-color:lime;background-image:none;  text-align:right; "; }
                        else if (value = isFinite(value) ? value : 0.0) 
                        { metaData.attr = "style = background-color:magenta;background-image:none;  text-align:right;";}
                        else {  metaData.attr = "style = background-color:red;background-image:none;"; };
                        return Ext.util.Format.number(value, '000.00');

                    },
                },
                {   header: "Total Pencairan", width : 110,
                    dataIndex : 'total_pencairan', sortable: true,
                    tooltip:"Jumlah total uang yang telah dicairkan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },

                {   header: "Total Realisasi", width : 110,
                    dataIndex : 'total_realisasi', sortable: true,
                    tooltip:"Jumlah total uang muka",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },

                {   header: "% serapan ", width : 100,
                    dataIndex : 's_persen', sortable: true,
                    tooltip:"presentase serapan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        value = ((record.data.total_realisasi)/(record.data.total_biaya))*100;
                        if ((value >= 0.00) && (value <= 80.00)) 
                            { metaData.attr = "style = background-color:lime;background-image:none;"; }
                        else if (value > 80.00)
                        { metaData.attr = "style = background-color:yellow;background-image:none;"; }
                        else if (value = isFinite(value) ? value : 0.0) 
                        { metaData.attr = "style = background-color:magenta;background-image:none;";}
                        else {  metaData.attr = "style = background-color:red;background-image:none;"; };
                        return Ext.util.Format.number(value, '000.00');
                    },
                },
                ];
                this.Records = Ext.data.Record.create(
                [   {name: 'id', type: 'integer'},
                ]);
                this.Grid = new Ext.grid.EditorGridPanel(
                {   
                    store:  this.SerapanDs,
                    stripeRows : true,
                    columns: this.Columns,
                    enableColLock: false,
                    loadMask: true,
                    height : (this.region_height/2)-50,
                    anchor: '100%',
                    autoScroll  : true,
                    frame: true,
                    tbar: [
                        {   print_type : "xls",
                            text:'Print Excell',
                            tooltip:'Print to Excell SpreadSheet',
                            iconCls: 'silk-page-white-excel',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.SerapanDs); },
                            scope : this
                        }
                    ],
                }
            );
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_newrapbsbendserapanTab",
                jsId : tabId+"_newrapbsbendserapanTab",
                title:  "SERAPAN ANGGARAN",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
      
        },

        // newrapbsbend search button
        newrapbsbendserapan_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_newrapbsbendSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };
            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.newrapbsbend.task,
                        act: alfalah.newrapbsbend.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.newrapbsbend.spj = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SpjStore;
    this.SearchBtn_center;
    this.Searchs_center;

    this.SouthGrid;
    this.SouthDataStore;
    this.SearchBtn_south;
    this.Searchs_south;
    this.Records_south;
    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.newrapbsbend.centerPanel.container.dom.clientHeight;
            this.Columns = [
                {   header: "Nomor Uang Muka", width : 100,
                    dataIndex : 'uangmuka_no', sortable: true,
                    tooltip:"Nomor Uang muka",
                },
                {   header: "Keterangan", width : 150,
                    dataIndex : 'uangmuka_keterangan_id',
                    sortable: true,
                    tooltip:"Keterangan Pengajuan",
                },
                {   header: "Nomor RAPBS", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"Nomor Rapbs",
                },
                {   header: "Organisasi / Urusan", width : 200,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    hidden : true, 
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 200,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    hidden : true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 250,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Jumlah Uang Muka", width : 150,
                    // hidden:true,
                    dataIndex : 'total_uangmuka', sortable: true,
                    tooltip:"Jumlah total uang muka",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Realisasi", width : 150,
                    // hidden:true,
                    dataIndex : 'total_realisasi', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Lebih Kurang", width : 150,
                    // hidden:true,
                    dataIndex : 'lebih_kurang', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        //console.log(record);
                        value = record.data.total_uangmuka - record.data.total_realisasi 
                        return Ext.util.Format.number(value, '0,000');
                    },
                },

                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"newrapbsbenduser Created Date",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"newrapbsbenduser Last Updated",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }

            ];
            this.Searchs_center = [
                {   id: 'newrapbsbendum_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'newrapbsbendum_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'newrapbsbendum_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'newrapbsbendum_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub. Keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn_center = new Ext.Button(
                {   id : tabId+"_newrapbsbendumSearchBtn_center",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.newrapbsbendum_search_handler_center,
                    scope : this
                });
                this.Searchs_south = [
                {   id: 'spj_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'spj_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'spj_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'spj_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub. Keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'spj_status',
                    cid: 'approve_status_3',
                    fieldLabel: 'By Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['label','key'],
                        data : [["ALL", 0], ["RECEIVED", 1]]
                    }),
                    displayField:'label',
                    valueField :'key',    
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100
                    // value: 'Active'
                },
            ];

            /**
                * MAIN-GRID
            */
            this.SpjStore = alfalah.core.newDataStore(
                "{{ url('/newrapbsbend/3/0') }}", false, 
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );

            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.SpjStore,
                columns: this.Columns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   print_type : "xls",
                            text:'Print Excell',
                            // hidden : true,
                            tooltip:'Print to Excell SpreadSheet',
                            iconCls: 'silk-page-white-excel',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.SpjStore); },
                            scope : this
                        },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_newrapbsbendumGridBBar',
                    store: this.SpjStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_newrapbsbendumPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_newrapbsbendumPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_newrapbsbendumGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn_center.handler.call(this.SearchBtn_center.scope);
                                }
                            }
                        }),
                        'records at a time'
                    ]
                }),
            });

        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_spjTab", 
                jsId : tabId+"_spjTab",
                title:  "REALISASI SPJ UANG MUKA",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    title: 'SPJ UANG MUKA',
                    split: true,
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'ToolBox',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    { //  title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs_center,
                        tbar: [
                        {   text:'Search',
                            tooltip:'Search',
                            iconCls: 'silk-zoom',
                            handler : this.newrapbsbendum_search_handler_center,
                            scope : this,
                        }]
                        
                    },
                    { title : 'Setting'
                    }]
                },

                    ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
        },

        // spj search button
        newrapbsbendum_search_handler_center : function(button, event)
        {   var the_search_center = true;
            //console.log(this);
            if ( this.SpjStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_spjSaveBtn').handler.call();
                                    the_search_center = false;
                                } else the_search_center = true;
                            },
                        icon: Ext.MessageBox.WARNING
                        });
            };

            if (the_search_center == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs_center);
                this.SpjStore.baseParams = Ext.apply( the_parameter,
                {   task: alfalah.newrapbsbend.task,
                    act: alfalah.newrapbsbend.act,
                    s:"form",
                    limit:this.page_limit, start:this.page_start });
                    this.SpjStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
//create aplicatioan
alfalah.newrapbsbend.newrapbsbendcashflow = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.o_pass = '';
    this.n_pass = '';
    this.MyPr_Grid;
    this.MyPr_Columns;
    this.MyPr_Records;
    this.MyPr_DataStore;

    this.AngGridPanel;
    this.Ang_DataStore;
    this.anggaranTab;
    this.Revenue_grid;
    // private functions

    // public space
    return {
        // execute at the very last time
        //public variable
        refresh_time : ((1000*60)*15), // 1000 = 1 second
        centerPanel : 0,
        page_limit : 75,
        page_start : 0,
        tabId : '{{ $TABID }}',
        task : '',
        act : '',
        // public methods
        initialize: function()
        {   Ext.chart.Chart.CHART_URL = "asset('/js/ext3/resources/charts.swf') }}";
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.tools = [
            {   id:'gear',
               handler: function()
                   {   Ext.Msg.alert('Message', 'The Settings tool was clicked.'); }
            },
            {   id:'close',
                handler: function(e, target, panel)
                    {   panel.ownerCt.remove(panel, true);  }
            }];

            this.centerPanel = Ext.getCmp(this.tabId);

            this.Forecasting_Ds = alfalah.core.newDataStore(
                "{{ url('/newrapbsbend/2/5') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start });
                this.Forecasting_Ds.load();

            
                

            this.FsGridPanel = new Ext.grid.EditorGridPanel( 
            {  store : this.Forecasting_Ds,
                stripeRows : true,
                columns : [            
                {   header: "Kode Rek", width : 80,
                    dataIndex : 'coa_id',
                    sortable: true,
                    // hidden : true,
                    tooltip:"kode rekening",
                },
                {   header: "Nama Rekening", width : 150,
                    dataIndex : 'coa_name',
                    sortable: true,
                    tooltip:"Nama Rekening",
                },
                {   header: "Agustus", width : 70,
                    dataIndex : 'agustus_cost', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya yang telah dicairkan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        current_date = 8;
                        get_month = record.data.fs_month
                        //console.log(current_date);
                        if (current_date ==  get_month){
                            value = record.data.monthly_cost;
                        } else { value = 0 };

                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "September", width : 70,
                    dataIndex : 'september_cost', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya yang telah dicairkan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        current_date = 9;
                        get_month = record.data.fs_month
                        //console.log(current_date);
                        if (current_date ==  get_month){
                            value = record.data.monthly_cost;
                        } else { value = 0 };
                        
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Oktober", width : 70,
                    dataIndex : 'oktober_cost', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya yang telah dicairkan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        current_date = 10;
                        get_month = record.data.fs_month
                        //console.log(current_date);
                        if (current_date ==  get_month){
                            value = record.data.monthly_cost;
                        } else { value = 0 };
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "November", width : 70,
                    dataIndex : 'november_cost', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya yang telah dicairkan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        current_date = 11;
                        get_month = record.data.fs_month
                        //console.log(current_date);
                        if (current_date ==  get_month){
                            value = record.data.monthly_cost;
                        } else { value = 0 };
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Desember", width : 70,
                    dataIndex : 'desember_cost', sortable: true,
                    // hidden : true,
                    tooltip:"Jumlah total biaya yang telah dicairkan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        current_date = 12;
                        get_month = record.data.fs_month
                        //console.log(current_date);
                        if (current_date ==  get_month){
                            value = record.data.monthly_cost;
                        } else { value = 0 };
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                ],
                tbar: [
                    {   text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : this.FsGridPanel,
                        scope : this
                    },
                ]
            });
        },

        // build the layout
        build_layout: function()
        { 
            this.Tab = new Ext.Panel(
            {   region: 'center',
                title : 'REKAP RENCANA ANGGARAN DAN ANGGARAN ',
               // autoScroll  : true,
                items: [
                    {   xtype:'portal',
                        region:'center',
                        margins:'1 1 1 1',
                        autoScroll  : true,
                        items:[
                        {   columnWidth:.50,
                            style:'padding:10px 0 0px 19px ',
                            items:[
                            {   title: 'RENCANA ANGGARAN PENDAPATAN DAN BELANJA',
                                region : 'center', 
                                // autoScroll  : true,
                                // width:30,
                                // height:635,
                                items: [
                                    {   
                                        region: 'center',     // center region is required, no width/height specified
                                        width: 570,
                                        height : 570,
                                        autoScroll  : true,
                                        layout : 'fit',
                                        items: [{
                                                xtype: 'container',
                                                contentEl: 'rapby_view',
                                                
                                            },]

                                    },
                                ]
                            },  
                            ]
                        },
                        // {   columnWidth:.49,
                        //     style:'padding:10px 0 0px 19px ',
                        //     items:[
                        //     {   title: 'ANGGARAN PENDAPATAN DAN BELANJA',
                        //         region : 'east', 
                        //         // autoScroll  : true,
                        //         // width:30,
                        //         // height:635,
                        //         items: [
                        //             {   
                        //                 region: 'center',     // center region is required, no width/height specified
                        //                 width: 540,
                        //                 height : 540,
                        //                // autoScroll  : true,
                        //                 layout : 'fit',
                        //                 items: [{
                        //                         xtype: 'container',
                        //                        // autoLoad:"{{ url('/newrapbsbend/2/4') }}",
                        //                         contentEl: 'apby_view',
                        //                         //tplWriteMode : 'overwrite',
                                                
                        //                     },]

                        //             },
                        //         ]
                        //     },  
                        //     ]
                        // }
                        ]
                    },
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {  // this.centerPanel.add(this.Tab);
           // alfalah.core.viewport.doLayout();
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.newrapbsbend.initialize, alfalah.newrapbsbend);
// end of file
</script>
<div>&nbsp;</div>
<body>
<div id="rapby_view">
    <h2 style="text-align: center;">RENCANA ANGGARAN PENDAPATAN DAN BELANJA</h2>
    <h2 style="text-align: center;">TAHUN ANGGARAN : {{ $TAHUNANGGARAN }}</h2>
    <!-- <div class="table-responsive"> -->
    <table class="x-data-table">
        <tr>
            <button  style="position: right: 0;" class= "babale">Expand All</button>
            <button  style="position: right: 0;" class= "basambunyi">Collapse All</button>
            <button  style="position: right: 0;" ><a href="{{ url('newrapbsbend/4/3') }}">Print RAPBY</button>
            <button  style="position: right: 0;" ><a href="{{ url('newrapbsbend/4/4') }}">Print APBY</button>
        </tr>       
        
        <thead>
            <tr>
                <th style="text-align: center;" width="20%"><h3>Kode</h3></th>
                <th style="text-align: center;" width="50%"><h3>Keterangan</h3></th>
                <th style="text-align: center;" width="30%"><h3>Jumlah (Rp)</h3></th>
            </tr>
        </thead>
        <tbody>
            <tbody class="labels">
                <tr>
                    <td colspan="3">
                        <label for="income">PENDAPATAN TIDAK TERIKAT</label>
                        <input type="checkbox" name="income" id="income" data-toggle="toggle">
                    </td>
                </tr>
            </tbody>
            <tbody class="hide">
                <tr>
                    @php ($total_tdk_terikat = 0)
                    @foreach($RAPBYBUDGET as $ptt)
                    @if ($ptt->sumberdana_id == 1)
                    <tr>
                        <td style="background-color:#80ffff">{{$ptt->income_detail_id}}</td>
                        <td style="background-color:#80ffff">{{$ptt->income_name}}</td>
                        <td style="text-align: right; background-color:#80ffff">{{number_format($ptt->income_amount, 0, ',' , '.')}}&nbsp;</td>
                    </tr>
                    @php ($total_tdk_terikat = $total_tdk_terikat + $ptt->income_amount)
                    @endif
                    
                    @endforeach
                </tr>
            </tbody>
                <tr>
                    <td colspan="2" style = "background-color:#c0c0c0; font-weight: bold">TOTAL PENDAPATAN TIDAK TERIKAT</td>
                    <td style="text-align: right; font-weight: bold; background-color:#c0c0c0">{{number_format($total_tdk_terikat, 0, ',' , '.')}} &nbsp;</td>
                </tr>
            <tbody class="labels">
                <tr>
                    <td colspan="3">
                        <label for="cost">PENDAPATAN TERIKAT</label>
                        <input type="checkbox" name="cost" id="cost" data-toggle="toggle">
                    </td>
                </tr>
            </tbody>
            <tbody class="hide">
               @php ($total_terikat = 0)
                @foreach($RAPBYBUDGET as $pt)
                @if($pt->sumberdana_id == 2)
                <tr>
                    <td style="background-color:#80ff80">{{$pt->income_detail_id}}</td>
                    <td style="background-color:#80ff80">{{$pt->income_name}}</td>
                    <td style="text-align: right; background-color:#80ff80">{{number_format($pt->income_amount, 0, ',' , '.')}} &nbsp;</td>
                </tr>
                @php ($total_terikat = $total_terikat + $pt->income_amount)
                @endif
                @endforeach
            </tbody>
                <tr>
                    <td colspan="2" style = "background-color:#c0c0c0; font-weight: bold">TOTAL PENDAPATAN TERIKAT</td>
                    <td style="text-align: right; font-weight: bold; background-color:#c0c0c0">{{number_format($total_terikat, 0, ',' , '.')}} &nbsp;</td>
                </tr>
                @php ($total_pendapatan =$total_tdk_terikat+$total_terikat)
                <tr>
                    <td colspan="2" style = "background-color:#c0c0c0; font-weight: bold">TOTAL PENDAPATAN</td>
                    <td style="text-align: right; font-weight: bold; background-color:#c0c0c0">{{number_format($total_pendapatan, 0, ',' , '.')}} &nbsp;</td>
                </tr>
                @foreach($MASTERBUDGET as $presentage)
                @php ($total_digunakan =$total_pendapatan*($presentage->cost_presentage/100))
                <tr>
                    <td colspan="2" style = "background-color:#8080c0; font-weight: bold">PENDAPATAN YG BISA DIGUNAKAN {{$presentage->cost_presentage}} %</td>
                    <td style="text-align: right; font-weight: bold; background-color:#8080c0">{{number_format($total_digunakan, 0, ',' , '.')}} &nbsp;</td>
                </tr>
                @endforeach
            <tr>
            </tr> 
            <tbody class="labels">
                <tr>
                    <td colspan="3">
                        <label for="cost">RENCANA ANGGARAN BELANJA</label>
                        <input type="checkbox" name="cost" id="cost" data-toggle="toggle">
                    </td>
                </tr>
            </tbody>
            <tbody class="hide">
               @php ($total_urusan = 0)
                @foreach($URUSANBUDGET as $urusan)
                <tr>
                    <td style="background-color:#80ff80">{{$urusan->urusan_mrapbs_id}}</td>
                    <td style="background-color:#80ff80">{{$urusan->urusan_mrapbs_id_name}}</td>
                    <td style="text-align: right; background-color:#80ff80">{{number_format($urusan->total_biaya, 0, ',' , '.')}} &nbsp;</td>
                </tr>
                @php ($total_urusan = $total_urusan + $urusan->total_biaya)
                @endforeach
            </tbody>
                <tr>
                    <td colspan="2" style = "background-color:#c0c0c0; font-weight: bold">TOTAL RENCANA ANGGARAN BELANJA</td>
                    <td style="text-align: right; font-weight: bold; background-color:#c0c0c0">{{number_format($total_urusan, 0, ',' , '.')}} &nbsp;</td>
                </tr>
                @php ($surplusdefisit = $total_digunakan-$total_urusan)
                <tr>
                @if ($surplusdefisit < 0 )
                    <td colspan="2" style = "background-color:#ff0000; font-weight: bold">SURPLUS / DEFISIT ANGGARAN</td>
                    <td style="text-align: right; font-weight: bold; background-color:#ff0000">{{number_format($surplusdefisit, 0, ',' , '.')}} &nbsp;</td>
                @elseif ($surplusdefisit > 0 )
                    <td colspan="2" style = "background-color:#00ff00; font-weight: bold">SURPLUS / DEFISIT ANGGARAN</td>
                    <td style="text-align: right; font-weight: bold; background-color:#00ff00">{{number_format($surplusdefisit, 0, ',' , '.')}} &nbsp;</td>
                @endif
                </tr>
        </tbody>
    </table>
    <!-- </div> -->
    <script>
    $(document).ready(function() {
        $('[data-toggle="toggle"]').change(function(){
            $(this).parents().next('.hide').toggle();
        });

        var $childRows = $('.hide');
        $(this).find('button.basambunyi').click(function() {
            $childRows.hide();
        });

        $(this).find('button.babale').click(function() {
            $childRows.show();
        });
    });
    </script>
</div>

<div id="apby_view">
<h2 style="text-align: center;">ANGGARAN PENDAPATAN DAN BELANJA</h2>
<h2 style="text-align: center;">TAHUN ANGGARAN : {{ $TAHUNANGGARAN }}</h2>

<!-- <div class="table-responsive"> -->
    <table class="x-data-table">
    <tr>
            <button  style="position: right: 0;" class= "babale">Expand All</button>
            <button  style="position: right: 0;" class= "basambunyi">Collapse All</button>
            <button  style="position: right: 0;" >Print</button>
        </tr>       
        
        <thead>
            <tr>
                <th style="text-align: center;" width="20%"><h3>Kode</h3></th>
                <th style="text-align: center;" width="50%"><h3>Keterangan</h3></th>
                <th style="text-align: center;" width="30%"><h3>Jumlah (Rp)</h3></th>
            </tr>
        </thead>
        <tbody>
            <tbody class="labels">
                <tr>
                    <td colspan="3">
                        <label for="income">PENDAPATAN TIDAK TERIKAT</label>
                        <input type="checkbox" name="income" id="income" data-toggle="toggle">
                    </td>
                </tr>
            </tbody>
            <tbody class="hide">
                <tr>
                    @php ($total_income = 0)
                    @foreach($RAPBYBUDGET as $ptt)
                    @if ($ptt->sumberdana_id == 1)
                    <tr>
                        <td style="background-color:#80ffff">{{$ptt->income_detail_id}}</td>
                        <td style="background-color:#80ffff">{{$ptt->income_name}}</td>
                        <td style="text-align: right; background-color:#80ffff">{{number_format($ptt->income_amount, 0, ',' , '.')}}&nbsp;</td>
                    </tr>
                    @endif
                    @php ($total_income = $total_income + $ptt->income_amount)
                    @endforeach
                </tr>
            </tbody>
                <tr>
                    <td colspan="2" style = "background-color:#c0c0c0; font-weight: bold">TOTAL PENDAPATAN TIDAK TERIKAT</td>
                    <td style="text-align: right; font-weight: bold; background-color:#c0c0c0">{{number_format($total_income, 0, ',' , '.')}} &nbsp;</td>
                </tr>
            <tbody class="labels">
                <tr>
                    <td colspan="3">
                        <label for="cost">PENDAPATAN TERIKAT</label>
                        <input type="checkbox" name="cost" id="cost" data-toggle="toggle">
                    </td>
                </tr>
            </tbody>
            <tbody class="hide">
               @php ($total_terkait = 0)
                @foreach($RAPBYBUDGET as $pt)
                @if($pt->sumberdana_id == 2)
                <tr>
                    <td style="background-color:#80ff80">{{$pt->income_detail_id}}</td>
                    <td style="background-color:#80ff80">{{$pt->income_name}}</td>
                    <td style="text-align: right; background-color:#80ff80">{{number_format($pt->income_amount, 0, ',' , '.')}} &nbsp;</td>
                </tr>
                @php ($total_terkait = $total_terkait + $pt->income_amount)
                @endif
                @endforeach
            </tbody>
                <tr>
                    <td colspan="2" style = "background-color:#c0c0c0; font-weight: bold">TOTAL PENDAPATAN TERIKAT</td>
                    <td style="text-align: right; font-weight: bold; background-color:#c0c0c0">{{number_format($total_terkait, 0, ',' , '.')}} &nbsp;</td>
                </tr>
                @php ($total_pendapatan =$total_income+$total_terkait)
                <tr>
                    <td colspan="2" style = "background-color:#c0c0c0; font-weight: bold">TOTAL PENDAPATAN</td>
                    <td style="text-align: right; font-weight: bold; background-color:#c0c0c0">{{number_format($total_pendapatan, 0, ',' , '.')}} &nbsp;</td>
                </tr>
                @foreach($MASTERBUDGET as $presentage)
                @php ($total_digunakan =$total_pendapatan*($presentage->cost_presentage/100))
                <tr>
                    <td colspan="2" style = "background-color:#8080c0; font-weight: bold">PENDAPATAN YG BISA DIGUNAKAN {{$presentage->cost_presentage}} %</td>
                    <td style="text-align: right; font-weight: bold; background-color:#8080c0">{{number_format($total_digunakan, 0, ',' , '.')}} &nbsp;</td>
                </tr>
                @endforeach
            <tr>
            </tr> 
            <tbody class="labels">
                <tr>
                    <td colspan="3">
                        <label for="cost">RENCANA ANGGARAN BELANJA</label>
                        <input type="checkbox" name="cost" id="cost" data-toggle="toggle">
                    </td>
                </tr>
            </tbody>
            <tbody class="hide">
               @php ($total_urusan = 0)
                @foreach($URUSANBUDGET as $urusan)
                @if($urusan->approve_status_3 ==  1)
                <tr>
                    <td style="background-color:#80ff80">{{$urusan->urusan_mrapbs_id}}</td>
                    <td style="background-color:#80ff80">{{$urusan->urusan_mrapbs_id_name}}</td>
                    <td style="text-align: right; background-color:#80ff80">{{number_format($urusan->total_biaya, 0, ',' , '.')}} &nbsp;</td>
                </tr>
                @endif
                @php ($total_urusan = $total_urusan + $urusan->total_biaya)
                @endforeach
            </tbody>
                <tr>
                    <td colspan="2" style = "background-color:#c0c0c0; font-weight: bold">TOTAL RENCANA ANGGARAN BELANJA</td>
                    <td style="text-align: right; font-weight: bold; background-color:#c0c0c0">{{number_format($total_urusan, 0, ',' , '.')}} &nbsp;</td>
                </tr>
                @php ($surplusdefisit = $total_digunakan-$total_urusan)
                <tr>
                @if ($surplusdefisit < 0 )
                    <td colspan="2" style = "background-color:#ff0000; font-weight: bold">SURPLUS / DEFISIT ANGGARAN</td>
                    <td style="text-align: right; font-weight: bold; background-color:#ff0000">{{number_format($surplusdefisit, 0, ',' , '.')}} &nbsp;</td>
                @elseif ($surplusdefisit > 0 )
                    <td colspan="2" style = "background-color:#00ff00; font-weight: bold">SURPLUS / DEFISIT ANGGARAN</td>
                    <td style="text-align: right; font-weight: bold; background-color:#00ff00">{{number_format($surplusdefisit, 0, ',' , '.')}} &nbsp;</td>
                @endif
                </tr>
        </tbody>
    </table>
<!-- </div> -->
    <script>
    $(document).ready(function() {
        $('[data-toggle="toggle"]').change(function(){
            $(this).parents().next('.hide').toggle();
        });

        var $childRows = $('.hide');
        $(this).find('button.basambunyi').click(function() {
            $childRows.hide();
        });

        $(this).find('button.babale').click(function() {
            $childRows.show();
        });
    });
    </script>
    </script>
</div>
</body>

<style>
    .x-data-table {
        border-collapse: collapse;
        border-spacing: 0;
        min-width: 98%
    }

    .x-data-table th {
        text-align: left;

        background-color: #508abb;
        color: #FFFFFF;
        border-color: #6ea1cc !important;
        text-transform: uppercase;
       
    }

    .x-data-table td,
    th {
        padding: .5em;
        border: 2px solid #ffffff;
    }

    .labels tr td {
        background-color: #2cc16a;
        border: 2px solid #00ffff;
        font-weight: bold;
        color: #fff;
    }

    .label tr td label {
        display: block;
        
    }

    [data-toggle="toggle"] {
	display: none;
    }
   
</style>