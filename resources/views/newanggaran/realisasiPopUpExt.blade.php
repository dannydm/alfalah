<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.realisasi'); // daftar pengajuan
// create application
alfalah.realisasi = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',

        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.realisasi.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.realisasi.Tab);
            this.centerPanel.setActiveTab(this.realisasi.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.realisasi.realisasi= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
            {   this.region_height = alfalah.realisasi.centerPanel.container.dom.clientHeight;
                this.Columns = [
                    {   header: "Nomor Uang Muka", width : 100,
                        dataIndex : 'uangmuka_no', sortable: true,
                        tooltip:"Nomor Uang muka",
                    },
                    {   header: "Keterangan", width : 150,
                        dataIndex : 'uangmuka_keterangan_id',
                        sortable: true,
                        tooltip:"Keterangan Pengajuan",
                    },
                    {   header: "Organisasi / Urusan", width : 150,
                        dataIndex : 'organisasi_mrapbs_id_name',
                        sortable: true,
                        tooltip:"Jenjang/Bidang/Departemen",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                            return alfalah.core.gridColumnWrap(result);
                        }
                    },
                    {   header: "Program / Kegiatan", width : 250,
                        dataIndex : 'program_mrapbs_id_name', sortable: true,
                        tooltip:"Nama Program",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                            return alfalah.core.gridColumnWrap(result);
                        }
                    },
                    {   header: "Jumlah Biaya", width : 100,
                        dataIndex : 'total_biaya', sortable: true,
                        tooltip:"Jumlah total biaya",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                            return Ext.util.Format.number(value, '0,000');
                        },
                    },
                    {   header: "Pengajuan by", width : 150,
                        dataIndex : 'username', sortable: true,
                        tooltip:"Diajukan oleh",
                    },
                    {   header: "Status", width : 50,
                        dataIndex : 'status', sortable: true,
                        tooltip:" Status",
                    },
                    {   header: "Created", width : 50,
                        dataIndex : 'created', sortable: true,
                        tooltip:"realisasi Created Date",
                        css : "background-color: #DCFFDE;",
                        hidden :true,
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    },
                    {   header: "Updated", width : 50,
                        dataIndex : 'modified_date', sortable: true,
                        tooltip:"realisasi Last Updated",
                        css : "background-color: #DCFFDE;",
                        hidden :true,
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    }
                ];
                this.Records = Ext.data.Record.create(
                [   {name: 'id', type: 'integer'},
                    // {name: 'modified_date', type: 'date'},
                ]);
                this.Searchs = [
                    {   id: 'realisasi_organisasi',
                        cid: "organisasi_mrapbs_id_name",
                        fieldLabel: 'Organisasi',
                        labelSeparator : '',
                        xtype: 'textfield', 
                        width : 120
                    },
                    {   id: 'realisasi_urusan',
                        cid: 'urusan_mrapbs_id_name',
                        fieldLabel: 'Urusan',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'realisasi_kegiatan',
                        cid: 'kegiatan_mrapbs_id_name',
                        fieldLabel: 'Kegiatan',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'realisasi_status',
                        cid: 'status',
                        fieldLabel: 'Status',
                        labelSeparator : '',
                        xtype : 'combo',
                        store : new Ext.data.SimpleStore(
                        {   fields: ['status'],
                            data : [[''], ['Active'], ['Inactive']]
                        }),
                        displayField:'status',
                        valueField :'status',
                        mode : 'local',
                        triggerAction: 'all',
                        selectOnFocus:true,
                        editable: false,
                        width : 100,
                        value: 'Active'
                    },
                ];
                this.SearchBtn = new Ext.Button(
                {   id : tabId+"_realisasiSearchBtn",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.realisasi_search_handler,
                    scope : this
                });
                this.DataStore = alfalah.core.newDataStore(
                    "{{url('/realisasi/1/0')}}", false,
                    {   s:"form", limit:this.page_limit, start:this.page_start }
                );
                // this.DataStore.load();
                this.Grid = new Ext.grid.EditorGridPanel(
                {   
                    store:  this.DataStore,
                    columns: this.Columns,
                    enableColLock: false,
                    loadMask: true,
                    height : (this.region_height/2)-50,
                    anchor: '100%',
                    autoScroll  : true,
                    frame: true,
                    tbar: [
                        {  
                            text:'Add Realisasi', 
                            tooltip:'entry realisasi biaya',
                            iconCls: 'silk-add',
                            // handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            handler : this.Grid_detail,
                            scope : this
                        },
                        {   print_type : "pdf",
                            text:'Print PDF', 
                            tooltip:'Print to PDF',
                            iconCls: 'silk-page-white-acrobat',
                            handler : this.Grid_pdf,
                            scope : this
                        },
                        {   print_type : "xls",
                            text:'Print Excell',
                            tooltip:'Print to Excell SpreadSheet',
                            iconCls: 'silk-page-white-excel',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },'-',
                        {   text:'Approve Realisasi',
                            tooltip:'realisasi Anggaran',
                            style:'background-color:lime',
                            iconCls: 'silk-tick',
                            handler : this.Grid_approve,
                            scope : this
                        },'-',
                    ],
                    bbar: new Ext.PagingToolbar(
                    {   id : tabId+'_realisasiGridBBar',
                        store: this.DataStore,
                        pageSize: this.page_limit,
                        displayInfo: true,
                        emptyMsg: 'No data found',
                        items : [
                            '-',
                            'Displayed : ',
                            new Ext.form.ComboBox(
                            {   id : tabId+'_realisasiPageCombo',
                                store: new Ext.data.SimpleStore(
                                            {   fields: ['value'],
                                                data : [[50],[75],[100],[125],[150]]
                                            }),
                                displayField:'value',
                                valueField :'value',
                                value : 75,
                                editable: false,
                                mode: 'local',
                                triggerAction: 'all',
                                selectOnFocus:true,
                                hiddenName: 'pagesize',
                                width:50,
                                listeners : { scope : this,
                                    'select' : function (a,b,c)
                                    {   this.page_limit = Ext.get(tabId+'_realisasiPageCombo').getValue();
                                        bbar = Ext.getCmp(tabId+'_realisasiGridBBar');
                                        bbar.pageSize = parseInt(this.page_limit);
                                        this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                        this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    }
                                }
                            }),
                            ' records at a time'
                        ]
                    })
                    ,
                    listeners : { scope : this,
                            'cellclick': function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent)
                            {
                                console.log('cell click');
                                if (iColIdx == 0 ) // kolom pertama
                                {   the_record = this.Grid.getSelectionModel().selection.record.data.uangmuka_no
                                    console.log(the_record);
                                    // this.southdatastore.baseParam = {new param in here};
                                    alfalah.realisasi.realisasi.SouthDataStore.baseParams = {   task: alfalah.realisasi.task,
                                        act: alfalah.realisasi.act,
                                        a:2, b:0, s:"form", uangmuka_no : the_record,
                                        limit:this.page_limit, start:this.page_start 
                                    };
                                    console.log('alfalah.realisasi.realisasi.SouthDataStore')    
                                    console.log(alfalah.realisasi.realisasi.SouthDataStore)             
                                    alfalah.realisasi.realisasi.SouthDataStore.reload();
                                };
                            },
                            'rowselect' : function (a,b,c)
                            {   console.log('listener grid select');
                                console.log(a);
                                console.log(b);
                                console.log(c);
                            }
                        }
                });
                /**
                    * SOUTH-GRID
                    */
                this.SouthDataStore = alfalah.core.newDataStore(
                    "{{ url('/realisasi/1/10') }}", false,
                    {   s:"init", limit:this.page_limit, start:this.page_start }
                );
                // this.SouthDataStore.load();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
                {   store:  this.SouthDataStore,
                    columns: [ 
                    {   header: "Uang Muka No", width : 100,
                        dataIndex : 'uangmuka_no', sortable: true,
                        readonly : true,
                        hidden:true,
                        tooltip:"Uang Muka No",
                    },
                    {   header: "Pengajuan No", width : 100,
                        dataIndex : 'pengajuan_no', sortable: true,
                        readonly : true,
                        tooltip:"RAPBS No",
                    },
                    {   header: "RAPBS.No", width : 100,
                        dataIndex : 'rapbs_no', sortable: true,
                        readonly : true,
                        tooltip:"RAPBS No",
                    },
                    {   header: "Kode Rekening", width : 100,
                        dataIndex : 'coa_id', sortable: true,
                        tooltip:"Kode rekening",
                        readonly:true,
                    },
                    {   header: "Name", width : 200,
                        dataIndex : 'coa_name', sortable: true,
                        tooltip:"Pos Belanja Name",
                        readonly : true,
                    },
                    {   header: "Uraian", width : 150,
                        dataIndex : 'uraian', sortable: true,
                        tooltip:"Uraian",
                        hidden :true,
                        readonly : true,
                    },
                    {   header: "Volume", width : 50,
                        dataIndex : 'volume', sortable: true,
                        tooltip:"Volume",
                        readonly : true,
                        // editor : new Ext.form.TextField({allowBlank: false}),
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   
                            result = '<b>'+value+'</b><br>'+record.data.satuan;
                            record.data.volume = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Satuan", width : 100,
                        dataIndex : 'satuan', sortable: true,
                        tooltip:"Satuan",
                        hidden : true,
                        readonly : true,
                        // editor : new Ext.form.TextField({allowBlank: false}),
                    },

                    {   header: "Tarif", width : 100,
                        dataIndex : 'tarif', sortable: true,
                        tooltip:"Tarif",
                        readonly : true,
                        // editor : new Ext.form.TextField({allowBlank: false}),
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {    metaData.attr="style = text-align:right;";
                            record.data.tarif = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Uang Muka", width : 100,
                        id: 'jumlah_id',
                        dataIndex : 'jumlah', sortable: true,
                        tooltip:"Jumlah",
                        readonly : true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {    metaData.attr="style = text-align:right;";
                            value = parseInt(record.data.volume) * parseInt(record.data.tarif);
                            record.data.jumlah = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Realisasi", width : 100,
                        id : 'realisasi_biaya',
                        dataIndex : 'realisasi_biaya', sortable: true,
                        tooltip:"Jumlah realisasi biaya",
                        hidden :false,
                        editor : new Ext.form.TextField({allowBlank: false}),
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                            return Ext.util.Format.number(value, '0,000');
                        },
                    },
                    {   header: "Saldo", width : 100,
                        id: 'jumlah_id',
                        dataIndex : 'saldo', sortable: true,
                        tooltip:"Jumlah",
                        readonly : true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {    metaData.attr="style = text-align:right;";
                            value = parseInt(record.data.jumlah) - parseInt(record.data.realisasi_biaya);
                            record.data.saldo = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Ber-ulang", width : 60,
                        dataIndex : 'berulang', sortable: true,
                        hidden :true,
                        tooltip:"variabel pembagi anggaran yg berulang",
                        readonly : true,
                        // editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Keterangan", width : 150,
                        dataIndex : 'keterangan1', sortable: true,
                        tooltip:"keterangan anggaran",
                        readonly : true,
                        // editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    // {   header: "Status", width : 50,
                    //     dataIndex : 'status', sortable: true,
                    //     tooltip:" Status",
                    // },
                    // {   header: "Created", width : 50,
                    //     dataIndex : 'created', sortable: true,
                    //     tooltip:"realisasi Created Date",
                    //     css : "background-color: #DCFFDE;",
                    //     renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    // },
                    {   header: "Updated", width : 100,
                        dataIndex : 'modified_date', sortable: true,
                        tooltip:"realisasi Last Updated",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    }
                ],
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Save',
                        id : 'btnSave',
                        tooltip:'Save Record',
                        hidden :true,
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this,
                    },
                    {   text:'Entry Nota',
                        tooltip:'Add Nota',
                        hidden :true,
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Nota',
                        tooltip:'Buat Nota',
                        iconCls: 'silk-add',
                        handler : this.Grid_Popup,
                        scope : this
                    },
                  ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_southGridBBar',
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_southPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_southPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_southGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });

        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_realisasiTab",
                jsId : tabId+"_realisasiTab",
                title:  "REKAP REALISASI UANG MUKA",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.realisasi_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {
                    region: 'south',
                    title: 'DETAIL REALISASI UANG MUKA',
                    split: true,
                    height : this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    layout : 'border',
                    items :[ 
                    {   
                        region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.SouthGrid]
                    }]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},


        Grid_pdf : function(button, event)
        {     var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                console.log(the_record)
                alfalah.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        uangmuka_no : the_record.uangmuka_no });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };
        },

        Grid_add: function(button, event)
        {   
        this.NotaDataStore = alfalah.core.newDataStore(
                "{{ url('/realisasi/1/20') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );

        var nota = new Ext.grid.EditorGridPanel({
                loadMask: true,
                autoScroll  : true,
                store: this.NotaDataStore, 
                columns : [ 
                        {   header: "No. Nota", width : 100,
                            dataIndex : 'nota_no', sortable: true,
                            readonly : true,
                            tooltip:"Uang Muka No",
                        },
                        {   header: "Keterangan", width : 100,
                            dataIndex : 'ket', sortable: true,
                            readonly : true,
                            tooltip:"keterangan",
                        },
                        {   header: "Jumlah", width : 100,
                            dataIndex : 'jum_nota', sortable: true,
                            readonly : true,
                            tooltip:"jumlah dari nota",
                        }
                    ],
                tbar: [
                        {   text:'Save',
                            id : 'btnSave',
                            tooltip:'Save Record',
                            iconCls: 'icon-save',
                            handler : this.Grid_save,
                            scope : this,
                        },
                        {   text:'Entry Nota',
                            tooltip:'Add Nota',
                            iconCls: 'silk-add',
                            handler : this.Nota_add,
                            scope : this
                        },
                    ],
                    bbar : new Ext.PagingToolbar(
                    {   id : tabId+'_notaGridBBar',
                        pageSize: this.page_limit,
                        displayInfo: true,
                        emptyMsg: 'No data found',
                        items : [
                            '-',
                            'Displayed : ',
                            new Ext.form.ComboBox(
                            {   id : tabId+'_notaPageCombo',
                                store: new Ext.data.SimpleStore(
                                            {   fields: ['value'],
                                                data : [[50],[75],[100],[125],[150]]
                                            }),
                                displayField:'value',
                                valueField :'value',
                                value : 75,
                                editable: false,
                                mode: 'local',
                                triggerAction: 'all',
                                selectOnFocus:true,
                                hiddenName: 'pagesize',
                                width:50,
                                listeners : { scope : this,
                                    'select' : function (a,b,c)
                                    {   this.page_limit = Ext.get(tabId+'_notaPageCombo').getValue();
                                        bbar = Ext.getCmp(tabId+'_notaGridBBar');
                                        bbar.pageSize = parseInt(this.page_limit);
                                        this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                        this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    }
                                }
                            }),
                            ' records at a time'
                        ]
                    }),

            viewConfig: {
                forceFit: true
            },
            Nota_add: function(button, event)
                {   this.Grid.stopEditing();
                    this.Grid.store.insert( 0,
                        new this.Records (
                        {   
                            nota_no: "",
                            uraian: "New Uraian",
                            volume: 0,
                            tarif: "",
                            satuan: "unit",
                            jumlah: 0,
                            created_date: "",
                            modified_date: "",
                            action: "ADD",
                        }));
                    // placed the edit cursor on third-column
                    this.Grid.startEditing(0, 1);
                }


            });
                
             
            SubDetail = new Ext.Window({
                        height: 500,
                        width: 1000,
                        layout: 'fit',
                        title: 'DETAIL TRANSAKSI',
                        autoScroll  : false,
                        frame: true,
                        items: [nota],
                        modal : true,
                        })

            SubDetail.show();
        },

        Grid_Popup : function(button, event)
        {
            var SouthGridRecord = this.SouthGrid.getSelectionModel().selection.record.data;
            if ( SouthGridRecord )
                { 
                     var centerPanel = Ext.getCmp('center_panel');
                    alfalah.realisasi.nota.initialize(SouthGridRecord);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.realisasi.nota.Tab);
                    centerPanel.setActiveTab(alfalah.realisasi.nota.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                }
            
            else
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Data Selected ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });
                };
        },

        Grid_save : function(button, event)
        {   this.SouthGrid.stopEditing();
            var selection = this.SouthGrid.getSelectionModel().selection;
            // check data modification
            if (selection)
            {   var data = selection.record.data.rapbs_no
                    realisasi_data = selection.record.data.realisasi_biaya
                    coa_data = selection.record.data.coa_id
                alfalah.core.submitGrid(
                    alfalah.realisasi.realisasi.SouthDataStore.data.items,  
                    "{{ url('/realisasi/1/101') }}",
                    {   'x-csrf-token': alfalah.realisasi.sid }, 
                    {   rapbs_no: data,
                        realisasi : realisasi_data,
                        coa_id : coa_data
                    }
                );
            };
        },

        Grid_detail : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
                { 
                    var centerPanel = Ext.getCmp('center_panel');
                    alfalah.realisasi.forms.initialize(the_record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.realisasi.forms.Tab);
                    centerPanel.setActiveTab(alfalah.realiasi.forms.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                }
            
            else
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Data Selected ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });
                };
        },

        // realisasi search button
        realisasi_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_realisasiSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.realisasi.task,
                        act: alfalah.realisasi.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.realisasi.nota = function()
{
// do NOT access DOM from here; elements don't exist yet
// execute at the first time
    // private variables
    this.Tab;
    this.Searchs;
    this.SearchBtn;
    this.Grid;
    this.updateTab;
    this.updateForm;
    this.Records;
    this.DetailRecords;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        load_1     : true,
        // user_data : '',
        // public methods
        initialize: function(the_records)  
        {   console.log('initialize forms');
            if (the_records)
            {   console.log(the_records);
                this.Records = the_records; 
            };
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   console.log('prepare');
            console.log(this.Records);
            /************************************
                G R I D S
            ************************************/ 
            // var cbSelModel = new Ext.grid.CheckboxSelectionModel(); 

            this.Columns = [ 
                // cbSelModel,
                {   header: "ID", width : 50,
                    dataIndex : 'biaya_id', sortable: true,
                    tooltip:"ID",
                    hidden: true,
                },
                {   header: "Nota No.", width : 100,
                    dataIndex : 'nota_no', sortable: true,
                    tooltip:"nomor nota",
                    editor : new Ext.form.TextField({allowBlank: false}),

                },
                {   header: "Keterangan", width : 200,
                    dataIndex : 'keterangan_nota', sortable: true,
                    tooltip:"keterangan dari nota",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Volume", width : 100,
                    dataIndex : 'volume_nota', sortable: true,
                    tooltip:"Volume",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.volume = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Satuan", width : 100,
                    dataIndex : 'satuan_nota', sortable: true,
                    tooltip:"Satuan",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Tarif", width : 100,
                    dataIndex : 'tarif_nota', sortable: true,
                    tooltip:"Tarif",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.tarif = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Jumlah", width : 100,
                    dataIndex : 'jumlah_nota', sortable: true,
                    readonly:true,
                    tooltip:"Jumlah",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        value = parseInt(record.data.volume_nota) * parseInt(record.data.tarif_nota);
                        record.data.jumlah_nota = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                {name: 'nota_no', type: 'string'},
                {name: 'keterangan_nota', type: 'string'},
                {name: 'volume_nota', type: 'string'},
                {name: 'tarif_nota', type: 'string'},
                {name: 'jumlah_nota', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.NotaDataStore = alfalah.core.newDataStore(
                "{{ url('/realisasi/1/20') }}", false,
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );
            //  this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.NotaDataStore,
                columns: this.Columns,
                // selModel: cbSelModel,
                enableColLock: false,
                loadMask: true,
                height : alfalah.realisasi.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        // hidden :true,
                        scope : this
                    },
                    '-',
                    {   text:'Add nota',
                        tooltip:'Approve Record',
                        iconCls: 'silk-tick',
                        // hidden :true,
                        handler : this.Grid_add,
                        scope : this
                    },
                    '-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_notaGridBBar',
                    store: this.NotaDataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_notaPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_notaPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_notaGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.detailTab = new Ext.Panel(
            {   title:  "DETAIL NOTA",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
        },
        // build the layout
        build_layout: function()
        {   
            if (Ext.isObject(this.Records))
                {   the_title = "REALISASI NOTA BELANJA"; }
            else { the_title = "REALISASI NOTA BELANJA"; };

            this.Tab = new Ext.Panel(
            {   id : tabId+"_FormsTab",
                jsId : tabId+"_FormsTab",
                title:  '<span style="background-color: yellow;">'+the_title+'</span>',
                iconCls: 'silk-note_add',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region   : 'center', // center region is required, no width/height specified
                    xtype    : 'tabpanel',
                    activeTab: 0,
                    items    : [this.detailTab]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   
            // the_records = this.Records
            // if (the_records) {

            // this.NotaDataStore.baseParams = Ext.apply( the_parameter,    
            // {   s   :"forms", limit:this.page_limit, start:this.page_start,
            //     ID  : the_records.record.data.id
            // })
            // this.NotaDataStore.load();}    
        },
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   id: "",
                    nota_no: "",
                    keterangan_nota: "New nota",
                    volume_nota: 0,
                    tarif_nota: "",
                    satuan_nota: "unit",
                    jumlah_nota: 0,
                    created_date: "",
                    modified_date: "",
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
     }; // end of public space
}(); // end of app
// create application
alfalah.realisasi.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
// execute at the first time
    // private variables
    this.Tab;
    this.Searchs;
    this.SearchBtn;
    this.Form;
    this.updateTab;
    this.updateForm;
    this.Records;
    this.DetailRecords;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        load_1     : true,
        // user_data : '',
        // public methods
        initialize: function(the_records)  
        {   console.log('initialize forms');
            if (the_records)
            {   console.log(the_records);
                this.Records = the_records; 
            };
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   console.log('prepare');
            console.log(this.Records);

            /************************************
                G R I D S
            ************************************/ 
            // var cbSelModel = new Ext.grid.CheckboxSelectionModel(); 
            this.Columns = [ 
                // cbSelModel,
                {   header: "ID", width : 50,
                    dataIndex : 'rapbs_id', sortable: true,
                    tooltip:"ID",
                    hidden: true,
                },
                {   header: "No Kegiatan", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"Doc.No",
                },
                {   header: "Pos.Belanja.No", width : 100,
                    dataIndex : 'coa_id', sortable: true,
                    tooltip:"Pos Belanja",
                    readonly:true,
                },

                {   header: "Name", width : 200,
                    dataIndex : 'coa_name', sortable: true,
                    readonly:true,
                    tooltip:"Pos Belanja Name",
                    editor: new Ext.form.ComboBox(
                    {   store: this.coaDS_2,
                        typeAhead: false,
                        width: 250,
                        readonly:true,
                        displayField: 'display',
                        valueField: 'coa_name',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        // triggerAction: "All",
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.coa_id = record.data.mcoa_id;
                                    combo.gridEditor.record.data.coa_name = record.data.coa_name;
                                    alfalah.realisasi.forms.Grid.getView().refresh();
                                },
                        }
                    }),
                },
                {   header: "Uraian", width : 150,
                    dataIndex : 'uraian', sortable: true,
                    tooltip:"Uraian",
                    readonly:true,
                },
                {   header: "Volume", width : 100,
                    dataIndex : 'volume', sortable: true,
                    tooltip:"Volume",
                    readonly:true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.volume = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Satuan", width : 100,
                    dataIndex : 'satuan', sortable: true,
                    tooltip:"Satuan",
                    readonly:true,
                },
                {   header: "Tarif", width : 100,
                    dataIndex : 'tarif', sortable: true,
                    tooltip:"Tarif",
                    readonly:true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.tarif = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Jumlah", width : 100,
                    dataIndex : 'jumlah', sortable: true,
                    readonly:true,

                    tooltip:"Jumlah",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        value = parseInt(record.data.volume) * parseInt(record.data.tarif);
                        record.data.jumlah = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Ber-ulang", width : 60,
                    dataIndex : 'berulang', sortable: true,
                    tooltip:"variabel pembagi anggaran yg berulang",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Status", width : 100,
                    dataIndex : 'status_name', sortable: true,
                    readonly:true,

                    tooltip:"Status",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                            fields: ["label"],
                                            data : [["Active"], ["InActive"]]
                                        }),
                            displayField:"label",
                            valueField:"label",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
            ];
            
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/realisasi/1/10') }}", false,
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );
            //  this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                // selModel: cbSelModel,
                enableColLock: false,
                loadMask: true,
                height : alfalah.realisasi.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        hidden :true,
                        scope : this
                    },
                    '-',
                    {   text:'Approve',
                        tooltip:'Approve Record',
                        iconCls: 'silk-tick',
                        hidden :true,
                        handler : this.Grid_approve,
                        scope : this
                    },
                    '-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_formsGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_formsPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_formsPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_formsGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
        },
        // build the layout
        build_layout: function()
        {   
            if (Ext.isObject(this.Records))
                {   the_title = "DETAIL BIAYA"; }
            else { the_title = "DETAIL BIAYA"; };

            this.Tab = new Ext.Panel(
            {   id : tabId+"_FormsTab",
                jsId : tabId+"_FormsTab",
                title:  '<span style="background-color: yellow;">'+the_title+'</span>',
                iconCls: 'silk-note_add',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region   : 'center', // center region is required, no width/height specified
                    xtype    : 'tabpanel',
                    activeTab: 0,
                    items    : [this.detailTab]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   
            the_records = this.Records
            if (the_records) {

            this.DataStore.baseParams = Ext.apply( the_parameter,    
            {   s:"forms", limit:this.page_limit, start:this.page_start,
                uangmuka_no : the_records.record.data.uangmuka_no
            })
            this.DataStore.load();}    
        },

     }; // end of public space
}(); // end of app
//onready
Ext.onReady(alfalah.realisasi.initialize, alfalah.realisasi);
// end of file
</script>
<div>&nbsp;</div>