<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.approval');

// create application
alfalah.approval = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.approval.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.approval.Tab);
            this.centerPanel.setActiveTab(this.approval.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.approval.approval= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            // var cbSelModel = new Ext.grid.CheckboxSelectionModel();
            this.Columns = [
                // cbSelModel,
                {   header: "Nomor Pengajuan", width : 100,
                    dataIndex : 'pengajuan_no', sortable: true,
                    tooltip:"Nomor pengajuan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_2 == 1) { metaData.attr = "style = background-color:lime"; }
                        // not yet approve
                        else if ( record.data.approve_status_2 == null){ metaData.attr = "style = background-color:yellow;"; }
                        // rejected
                        else {  metaData.attr = "style = background-color:red;"; };
                        return value;
                    }
                },
                {   header: "Keterangan", width : 150,
                    dataIndex : 'pengajuan_keterangan_id',
                    sortable: true,
                    tooltip:"Keterangan Pengajuan",
                },
                {   header: "Organisasi", width : 150,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    // hidden :true,
                    tooltip:"Keterangan Pengajuan",
                },
                {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },

                {   header: "Jumlah Biaya", width : 100,
                    dataIndex : 'total_biaya', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Created by", width : 150,
                    dataIndex : 'created_by', sortable: true,
                    tooltip:"Diajukan oleh",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = ' ['+value+'] '+record.data.user;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    tooltip:" Status",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"approval Created Date",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"approval Last Updated",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                // {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'approval_organisasi_id',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Org.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'approval_keterangan_id',
                    cid: 'keterangan',
                    fieldLabel: 'ket.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'approval_status',
                    cid: 'status',
                    hidden : true,
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },

            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_approvalSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.approval_search_handler,
                scope : this 
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/approval/1/0')}}", false,
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   
                store:  this.DataStore,
                columns: this.Columns,
                // selModel: cbSelModel,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.approval.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Detail kegiatan',
                        tooltip:'view Pengajuan berdasar rapbs.no',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_pengajuan,
                        scope : this
                    },

                    {   text:'Detail Biaya',
                        tooltip:'view detail',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_edit,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF', 
                        iconCls: 'silk-page-white-acrobat',
                        // handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        hidden :true,
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                    // {   text:'Keterangan Warna',
                    //     iconCls:'silk-information ',
                    //     scope : this
                    // },'-',
                    {   text:'Approve',
                        tooltip:'Approval Anggaran',
                        // xType : 'dataview',
                        style:'background-color:lime',
                        iconCls: 'silk-tick',
                        handler : this.Grid_approve,
                        scope : this
                    },'-',
                    {   text:'Not Yet Approve',
                        tooltip:'Anggaran Belum di approve',
                        // xType : 'dataview',
                        style:'background-color:yellow',
                        hidden : true,
                        iconCls: 'silk-stop',
                        // handler : this.Grid_reject,
                        // scope : this
                    },'-',
                    {   text:'Reject',
                        tooltip:'Reject Pengajuan Anggaran bulanan',
                        // xType : 'dataview',
                        // hidden :true,
                        style:'background-color:red',
                        iconCls: 'silk-cancel',
                        handler : this.Grid_reject,
                        scope : this
                    },
                    '->',
                    '_T O T A L_',
                    new Ext.form.TextField(
                    {   id : 'grand_total_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        // listeners :
                        // {   "beforeedit" : this.Grid_grand_total },
                        // scope : this
                    }),



                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_approvalGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_approvalPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_approvalPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_approvalGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_approvalSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_approvalTab",
                jsId : tabId+"_approvalTab",
                title:  "Data Pengajuan approval",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.approval_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
            this.DataStore.on( 'load', function( store, records, options )
            {
                console.log(records);
                var total_data = 0;
                Ext.each(records,
                    function(the_record)
                    { 
                        total_data = total_data + parseInt(the_record.data.total_biaya);
                    });

                console.log("total jumlah = "+total_data);
//                console.log(Ext.getCmp('grand_total_id').setValue(total_data));
                Ext.getCmp('grand_total_id').setValue(Ext.util.Format.number(total_data, '0,000'));
            });
            // this.DataStore.load();
          
        },

        Grid_pengajuan: function(button, event)
        {  var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {
                var centerPanel = Ext.getCmp('center_panel');
                alfalah.approval.pengajuan.initialize(the_record);
                centerPanel.beginUpdate();
                centerPanel.add(alfalah.approval.pengajuan.Tab);
                centerPanel.setActiveTab(alfalah.approval.pengajuan.Tab);
                centerPanel.endUpdate();
                alfalah.core.viewport.doLayout();
                }
                else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },

        Grid_add: function(button, event)
        {   var centerPanel = Ext.getCmp('center_panel');
            alfalah.approval.forms.initialize();
            centerPanel.beginUpdate();
            centerPanel.add(alfalah.approval.forms.Tab);
            centerPanel.setActiveTab(alfalah.approval.forms.Tab);
            centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        Grid_edit : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
                { 
                     var centerPanel = Ext.getCmp('center_panel');
                    alfalah.approval.forms.initialize(the_record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.approval.forms.Tab);
                    centerPanel.setActiveTab(alfalah.approval.forms.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                }
            
            else
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Data Selected ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });
                };
        },
        Grid_detail : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var form_order = Ext.getCmp("form_order");
                if (form_order)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Order Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    alfalah.approval.detailFrm.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.approval.detailFrm.Tab);
                    centerPanel.setActiveTab(alfalah.approval.detailFrm.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        // approval grid save records

        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            } else
            {   var head_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/approval/1/1') }}",
                    {   'x-csrf-token': alfalah.approval.sid }, 
                    {   head : Ext.encode(head_data) }
                );
            };
        },
        // approval grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: "{{ url('/approval/1/2') }}",
                headers:
                {   'x-csrf-token': alfalah.approval.sid },
                params  :
                {   rapbs_no: this.Grid.getSelectionModel().selection.record.data.rapbs_no
                },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Server Message : '+'\n'+the_response.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
        
        Grid_approve : function(button, event) 
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   Ext.Msg.show(
                    {   title :'A P P R O V A L',
                        msg : 'APPROVE ?',
                        width:250,
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                        { if (buttonId =='yes')
                    {  
                        alfalah.core.submitGrid(
                            alfalah.approval.approval.DataStore, 
                        "{{ url('/approval/1/705') }}",
                        {   'x-csrf-token': alfalah.approval.sid }, 
                        {   pengajuan_no: alfalah.approval.approval.Grid.getSelectionModel().selection.record.data.pengajuan_no }
                    );
                } else
                            { this.DataStore.reload(); };
                        },
                        icon: Ext.MessageBox.WARNING
                    });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },

        Grid_reject: function(button, event)
        {   this.Grid.stopEditing();
            var selection_data = alfalah.approval.approval.Grid.getSelectionModel().selection;
            if (selection_data)
            {   var app_data = selection_data.record.data.approve_status_2;
                var data = selection_data.record.data.pengajuan_no;
                var head_data = [selection_data.record.data]; 
                get_head = Ext.apply(head_data);
                // console.log('ini get head_data');
                //     console.log(get_head);
                if (app_data == null) {
                    
                    Ext.MessageBox.show({
                        title: 'WARNING !!!',
                        msg: 'DATA WILL BE REJECT, ARE YOU SURE ??? <br /><br />Reject Reason : <input type="text"  style="width: 250px" id="catat_id"/>',
                        buttons: Ext.MessageBox.OKCANCEL,
                        grow: true,
                        width : 400,
                      //  activeItem : true,
                        fn: function (buttonId, catatan, head) {
                            if (buttonId =='ok' )
                                {   
                                    Ext.Ajax.request(
                                    {   method: 'POST',
                                        url: "{{ url('/approval/1/100') }}",
                                        headers: {   'x-csrf-token': alfalah.approval.sid },
                                        params: {   //uangmuka_no: data,
                                                    catatan : Ext.get('catat_id').getValue(),
                                                    head : Ext.encode(get_head),
                                                },
                                        success: function(response)
                                        {   var the_response = Ext.decode(response.responseText); 
                                            if (the_response.success == false)
                                            {   Ext.Msg.show(
                                                {   title :'E R R O R ',
                                                    msg : 'Server Message : '+'\n'+the_response.message,
                                                    buttons: Ext.Msg.OK,
                                                    icon: Ext.MessageBox.ERROR
                                                });
                                            }
                                            else
                                            {   //alfalah.pencairan.pencairan.DataStore.reload(); 
                                                Ext.MessageBox.show({
                                                    title: 'DATA HAS BEEN REJECTED',
                                                    buttons: Ext.Msg.OK,
                                                    closable:false,
                                                    width : 200,
                                                    fn : function (btn) { 
                                                        if (btn == "ok") 
                                                        {  
                                                            alfalah.core.submitGrid(
                                                                alfalah.approval.approval.DataStore,
                                                                "{{ url('/approval/1/2') }}",
                                                                {   'x-csrf-token': alfalah.approval.sid }, 
                                                                {   pengajuan_no: alfalah.approval.approval.Grid.getSelectionModel().selection.record.data.pengajuan_no }
                                                            );
                                                        } else
                                                        { this.DataStore.reload(); };
                                                    }
                                                });
                                                //console.log('test reject pengajuan');
                                            };
                                        },
                                        failure: function()
                                        {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                                        },
                                    });
                                       
                            }
                            
                        }
                    }
                ) 
                    
                }
                else
                {  
                    Ext.Msg.show(
                    {   title   :'I N F O ',
                        msg     : 'Data Has Been Verification',
                        buttons : Ext.Msg.OK,
                        icon    : Ext.MessageBox.INFO,
                        width   : 200
                    });
                   

                }    
            }
            else
            {   Ext.Msg.show(
                {   title   :'I N F O ',
                    msg     : 'No Selected Record ! ',
                    buttons : Ext.Msg.OK,
                    icon    : Ext.MessageBox.INFO,
                    width   : 200
                });
            };
        },

        // Grid_reject: function(button, event)
        // {   
        //     // the_rapbs_no = Ext.getCmp('rapbsFrm_rapbs_no').getValue();
        //     the_rapbs_no = this.Grid.getSelectionModel()
        //     if (the_rapbs_no == "")
        //     {
        //         Ext.Msg.show(
        //         {   title :'E R R O R ',
        //             msg : 'RAPBS not saved yet',
        //             buttons: Ext.Msg.OK,
        //             icon: Ext.MessageBox.ERROR
        //         });
        //     }
        //     else
        //     {
        //         this.Grid.stopEditing();
        //         Ext.Ajax.request(
        //         {   method: 'POST',
        //             url: "{{ url('/approval/1/100') }}",
        //             headers:
        //             {   'x-csrf-token': alfalah.approval.sid },
        //             params  :
        //             {   rapbs_no: the_rapbs_no },
        //             success: function(response)
        //             {   var the_response = Ext.decode(response.responseText);
        //                 if (the_response.success == false)
        //                 {   Ext.Msg.show(
        //                     {   title :'E R R O R ',
        //                         msg : 'Server Message : '+'\n'+the_response.message,
        //                         buttons: Ext.Msg.OK,
        //                         icon: Ext.MessageBox.ERROR
        //                     });
        //                 }
        //                 else
        //                 {   
        //                     Ext.getCmp(tabId+"_FormsTab").destroy();
        //                     alfalah.approval.approval.DataStore.reload();
        //                 };
        //             },
        //             failure: function()
        //             { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
        //             },
        //           scope: this
        //         });
        //     };
        // },

        // approval search button
        approval_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_approvalSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.approval.task,
                        act: alfalah.approval.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
// create application
alfalah.approval.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
// execute at the first time
    // private variables
    this.Tab;
    this.Searchs;
    this.SearchBtn;
    this.Form;
    this.updateTab;
    this.updateForm;
    this.Records;
    this.DetailRecords;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        load_1     : true,
        // user_data : '',
        // public methods
        initialize: function(the_records)  
        {   console.log('initialize forms');
            if (the_records)
            {   console.log(the_records);
                this.Records = the_records; 
            };
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   console.log('prepare');
            console.log(this.Records);

            /************************************
                G R I D S
            ************************************/ 
            // var cbSelModel = new Ext.grid.CheckboxSelectionModel(); 
            this.Columns = [ 
                // cbSelModel,
                {   header: "ID", width : 50,
                    dataIndex : 'rapbs_id', sortable: true,
                    tooltip:"ID",
                    hidden: true,
                },
                {   header: "No approval", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"Doc.No",
                },
                {   header: "Pos.Belanja.No", width : 100,
                    dataIndex : 'coa_id', sortable: true,
                    tooltip:"Pos Belanja",
                    readonly:true,
                },
                {   header: "Name", width : 200,
                    dataIndex : 'coa_name', sortable: true,
                    readonly:true,
                    tooltip:"Pos Belanja Name",
                    editor: new Ext.form.ComboBox(
                    {   store: this.coaDS_2,
                        typeAhead: false,
                        width: 250,
                        readonly:true,
                        displayField: 'display',
                        valueField: 'coa_name',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        // triggerAction: "All",
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.coa_id = record.data.mcoa_id;
                                    combo.gridEditor.record.data.coa_name = record.data.coa_name;
                                    alfalah.approval.forms.Grid.getView().refresh();
                                },
                        }
                    }),
                },
                {   header: "Uraian", width : 150,
                    dataIndex : 'uraian', sortable: true,
                    tooltip:"Uraian",
                    readonly:true,
                },
                {   header: "Volume Ajuan", width : 100,
                    dataIndex : 'vol_ajuan', sortable: true,
                    tooltip:"Volume",
                    readonly:true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.vol_ajuan = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Satuan", width : 100,
                    dataIndex : 'satuan', sortable: true,
                    tooltip:"Satuan",
                    readonly:true,
                },
                {   header: "Tarif", width : 100,
                    dataIndex : 'tarif', sortable: true,
                    tooltip:"Tarif",
                    readonly:true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.tarif = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Jumlah Ajuan", width : 100,
                    dataIndex : 'jum_ajuan', sortable: true,
                    readonly:true,

                    tooltip:"Jumlah",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        value = parseInt(record.data.vol_ajuan) * parseInt(record.data.tarif);
                        record.data.jum_ajuan = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Ber-ulang", width : 60,
                    dataIndex : 'berulang', sortable: true,
                    hidden : true,
                    tooltip:"variabel pembagi anggaran yg berulang",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Status", width : 100,
                    dataIndex : 'status_name', sortable: true,
                    readonly:true,

                    tooltip:"Status",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                            fields: ["label"],
                                            data : [["Active"], ["InActive"]]
                                        }),
                            displayField:"label",
                            valueField:"label",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
            ];
            
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/approval/1/10') }}", false,
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );
            //  this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                // selModel: cbSelModel,
                enableColLock: false,
                loadMask: true,
                height : alfalah.approval.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        hidden :true,
                        scope : this
                    },
                    '-',
                    {   text:'Approve',
                        tooltip:'Approve Record',
                        iconCls: 'silk-tick',
                        hidden :true,
                        handler : this.Grid_approve,
                        scope : this
                    },
                    '-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_newsGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_newsPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_newsPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_newsGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
        },
        // build the layout
        build_layout: function()
        {   
            if (Ext.isObject(this.Records))
                {   the_title = "DATA PENGAJUAN"; }
            else { the_title = "DATA PENGAJUAN"; };

            this.Tab = new Ext.Panel(
            {   id : tabId+"_FormsTab",
                jsId : tabId+"_FormsTab",
                title:  '<span style="background-color: yellow;">'+the_title+'</span>',
                iconCls: 'silk-note_add',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region   : 'center', // center region is required, no width/height specified
                    xtype    : 'tabpanel',
                    activeTab: 0,
                    items    : [this.detailTab]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   
            the_records = this.Records
            if (the_records) {

            this.DataStore.baseParams = Ext.apply( the_parameter,    
            {   s:"forms", limit:this.page_limit, start:this.page_start,
                pengajuan_no : the_records.record.data.pengajuan_no
            })
            this.DataStore.load();}    
        },
     }; // end of public space
}(); // end of app
// create application
alfalah.approval.pengajuan= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;
    this.updateTab;
    this.updateForm;
    this.DetailRecords;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        load_1 :true,
        // public methods
        initialize: function(the_records)  
        {   console.log('initialize approval.pengajuan');
            if (the_records)
            {   console.log(the_records);
                this.Records = the_records; 
            };
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },

        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "RAPBS.No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"RAPBS No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        console.log('test console di kegiatan');
                        console.log(record);
                        if (record.data.approve_status == 1) { metaData.attr = "style = background-color:lime"; }
                        // not yet approve
                        else if ( record.data.approve_status == 0){ metaData.attr = "style = background-color:yellow;"; }
                        // rejected
                        else {  metaData.attr = "style = background-color:red;"; };

                        return value;
                    }

                },
                {   header: "Urusan / Organisasi", width : 150,
                    dataIndex : 'urusan_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.organisasi_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 250,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Sumber Dana", width : 150,
                    dataIndex : 'sumberdana_id_name', sortable: true,
                    hidden :true,
                    tooltip:"sumber pendanaan",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Jumlah Biaya", width : 100,
                    id : 'total_biaya_urusan',
                    dataIndex : 'total', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    tooltip:" Status",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"approval Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"approval Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Searchs = [
                {   id: 'pengajuan_urusan',
                    cid: "urusan_mrapbs_id_name",
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengajuan_organisasi',
                    cid: 'organisasi_mrapbs_id_name',
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengajuan_approval',
                    cid: 'approval_mrapbs_id_name',
                    fieldLabel: 'approval',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengajuan_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_pengajuanSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                // handler : this.approval_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/approval/1/99')}}", false,
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.approval.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        hidden :true,
                        tooltip:'Print to Excell SpreadSheet', 
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                    {   text:'Action',
                        iconCls:'silk-information ',
                        hidden :true,
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_pengajuanGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_pengajuanPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_pengajuanPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_pengajuanGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_approvalSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time cek'
                    ]
                }),
            });

        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_pengajuanTab",
                jsId : tabId+"_pengajuanTab",
                title:  "DATA PENGAJUAN",
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   
            the_records = this.Records
            if (the_records) {

                console.log('records di finalize');
                console.log(the_records);

            this.DataStore.baseParams = Ext.apply( the_parameter,    
            {   s:"forms", limit:this.page_limit, start:this.page_start,
                pengajuan_no : the_records.record.data.pengajuan_no 
            })
            this.DataStore.load();}    
        },

        Grid_pdf : function(button, event)
        {     var the_records = this.Records;
            if ( the_records )
            {   the_record = the_records.record.data;
                console.log(the_record)
                alfalah.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        pengajuan_no : the_record.pengajuan_no });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };
        },


        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: "{{ url('/approval/1/2') }}",
                headers:
                {   'x-csrf-token': alfalah.approval.sid },
                params  :
                {   rapbs_no: this.Grid.getSelectionModel().selection.record.data.rapbs_no
                },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Server Message : '+'\n'+the_response.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.approval.initialize, alfalah.approval);
// end of file
</script>
<div>&nbsp;</div>