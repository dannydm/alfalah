<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.income');

// create application
alfalah.income = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.income.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.income.Tab);
            this.centerPanel.setActiveTab(this.income.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
    }; // end of public space
}(); // end of app
// create application
alfalah.income.income= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Tahun Pendapatan", width : 200,
                    dataIndex : 'income_master_no', sortable: true,
                    tooltip:"Tahun Pendapatan",
                },
                {   header: "Keterangan", width : 300,
                    dataIndex : 'income_description_name',
                    sortable: true,
                    tooltip:"Nama tahun pendapatan",
                },
                {   header: "Total Usulan", width : 200,
                    dataIndex : 'income_total_amount',
                    sortable: true,
                    tooltip:"total usulan pendapatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.income_total_amount = value;
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "% Terhadap Pendapatan", width : 300,
                    dataIndex : 'cost_presentage',
                    sortable: true,
                    tooltip:"presentase terhadap pendapatan untuk anggaran tahun ini",
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    hidden :true,
                    tooltip:" Income Status",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    hidden:true,
                    tooltip:"income Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    hidden:true,
                    tooltip:"income Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
            ]);
            this.Searchs = [
                {   id: 'income_name_id',
                    cid: 'tahun_id',
                    fieldLabel: 'Tahun',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'income_description_name_id',
                    cid: "income_description_name",
                    fieldLabel: 'Ket.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'income_status_id',
                    cid: 'status',
                    hidden :true,
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },

            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_incomeSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.income_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{url('/income/1/0')}}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.income.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        // hidden :true,
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Edit',
                        tooltip:'Edit Record',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_edit,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        id : 'btndelete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this,
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
//                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        handler : this.Grid_pdf,
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_incomeGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_incomePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_incomePageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_incomeGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_incomeSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_incomeTab",
                jsId : tabId+"_incomeTab",
                title:  "USULAN PENDAPATAN",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.income_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},

        Grid_add: function(button, event)
        {
            // var the_records = alfalah.income.income.DataStore.data.items;

            // Ext.each(the_records,
            //     function (the_record)
            //     {
            //         var check_year = the_record.data.tahun_ajaran_id;
            //         if (check_year == '{{ $TAHUNAJARAN_ID }}')
            //         {
            //             Ext.Msg.show(
            //             {   title :'E R R O R ',
            //                 msg : 'Year has been create',
            //                 buttons: Ext.Msg.OK,
            //                 icon: Ext.MessageBox.ERROR
            //             });
            //         }
            //         else 
            //         {
                        var centerPanel = Ext.getCmp('center_panel');
                        alfalah.income.forms.initialize();
                        centerPanel.beginUpdate();
                        centerPanel.add(alfalah.income.forms.Tab);
                        centerPanel.setActiveTab(alfalah.income.forms.Tab);
                        centerPanel.endUpdate();
                        alfalah.core.viewport.doLayout();
            //         }
            //     }
            // );
        },

        Grid_edit : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var the_form = Ext.getCmp("incomeFrm");
                if (the_form)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'RAPBS Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    alfalah.income.forms.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.income.forms.Tab);
                    centerPanel.setActiveTab(alfalah.income.forms.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            };
        },

        Grid_detail : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var form_order = Ext.getCmp("form_order");
                if (form_order)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Order Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    alfalah.income.detailFrm.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.income.detailFrm.Tab);
                    centerPanel.setActiveTab(alfalah.income.detailFrm.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },

        Grid_pdf : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                console.log(the_record)
                alfalah.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        income_master_no : the_record.income_master_no });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };
        },
        
        // income search button
        income_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_incomeSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.income.task,
                        act: alfalah.income.act,
                        a:3, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
// create application
alfalah.income.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
// execute at the first time
    // private variables
    this.Tab;
    this.Searchs;
    this.SearchBtn;
    this.Form;
    this.updateTab;
    this.updateForm;
    this.Records;
    this.DetailRecords;
    // private functions
    // public space
    return {
        // execute at the very last time 
        // public variable
        page_limit : 75,
        page_start : 0,
        load_1     : true,
        // user_data : '',
        // public methods
        initialize: function(the_records)  
        {   console.log('initialize');
            if (the_records)
            {   console.log(the_records);
                this.Records = the_records; };
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.total_income();
        },
        // prepare the component before layout drawing
            prepare_component: function()
        {   console.log('prepare');
            console.log(this.Records);
 
            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   id : 'incomeFrm',
                // width: '100%',
                frame: true,
                title : 'H E A D E R',
                // autoHeight: true,
                bodyStyle: 'padding: 10px 10px 0 10px;',
                labelWidth: 100,
                defaults: {
                    anchor: '100%',
                    allowBlank: false,
                    msgTarget: 'side'
                },
                items: [
                {   layout:'column',
                    border:false,
                    items:
                    [{   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'incomeFrm_id',
                            xtype:'textfield',
                            fieldLabel: 'ID',
                            name: 'id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            value : '',
                            hidden : true,
                        },
                        {   id : 'incomeFrm_created_date', 
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                            value : '',
                        },
                        {   id : 'incomeFrm_tahun_ajaran_id', 
                            xtype:'textfield',
                            fieldLabel: 'Tahun',
                            name: 'tahun_ajaran_id',
                            anchor:'100%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $TAHUNANGGARAN_ID }}',
                            hidden : false,
                        },
                        {   id : 'incomeFrm_income_master_no_id', 
                            xtype:'textfield',
                            fieldLabel: 'Tahun Anggaran',
                            name: 'income_master_no',
                            anchor:'100%',
                            allowBlank: false,
                            readOnly : true,
                            value : '{{ $TAHUNANGGARAN }}',
                            hidden : false,
                        },
                        {   id : 'incomeFrm_income_description_id', 
                            xtype:'textfield',
                            fieldLabel: 'keterangan',
                            name: 'income_description_name',
                            value: 'Income Year'+' '+ new Date().format('d/m/Y'), 
                            anchor:'100%',
                            // readOnly : true,
                            allowBlank: false,
                        },
                        {   id : 'incomeFrm_cost_presentage_id', 
                            xtype:'textfield',
                            fieldLabel: '% thp Pendapatan',
                            name: 'cost_presentage',
                            // value: 0, 
                            // anchor:'100%',
                            // readOnly : true,
                            allowBlank: false,
                        },
                        {   id : 'incomeFrm_status', 
                            xtype : 'combo',
                            fieldLabel: 'Status',
                            labelSeparator : '',
                            name: 'status',
                            anchor:'50%',
                            hidden:true,
                            allowBlank: false,
                            readOnly : false,
                            store : new Ext.data.SimpleStore(
                            {   fields: ["label", "key"],
                                data : [ ["Active", "0"], ["Inactive", "1"]]
                            }),
                            displayField:'label',
                            valueField :'key',
                            mode : 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            editable: false,
                            width : 100,
                            value: 0
                        }
                    ]
                }]
            }],
        });
 
        this.incomeDs = alfalah.core.newDataStore(
                "{{ url('/mcoa/3/9') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start, status : 0 }
            ); 
 
            /************************************
                G R I D S
            ************************************/ 
            this.Columns = [ 
                {   header: "ID", width : 50,
                    dataIndex : 'id', sortable: true,
                    tooltip:"ID",
                    hidden: true,
                },
                {   header: "Tahun Pendapatan", width : 150,
                    dataIndex : 'income_master_no', sortable: true,
                    tooltip:"tahun income",
//                    hidden: true,
                },
                {   header: "Kode Pendapatan", width : 200,
                    dataIndex : 'income_detail_id', sortable: true,
                    tooltip:"jenis income",
                    readonly:true,
                },
                {   header: "Nama Pendapatan", width : 300,
                    dataIndex : 'income_name', sortable: true,
                    tooltip:"nama jenis pendapatan",
                    editor: new Ext.form.ComboBox(
                    {   store: this.incomeDs,
                        typeAhead: false,
                        width: 250,
                        readonly:true,
                        displayField: 'display',
                        valueField: 'mincome_name',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        // triggerAction: "All",
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                { 
                                    combo.gridEditor.record.data.income_detail_id = record.data.mincome_id;
                                    combo.gridEditor.record.data.income_name = record.data.mincome_name;
                                    alfalah.income.forms.Grid.getView().refresh();
                                },
                        }
                    }),
                },
                {   header: "Uraian", width : 300,
                    dataIndex : 'income_uraian', sortable: true,
                    tooltip:"Uraian",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Jumlah Pendapatan", width : 200,
                    dataIndex : 'income_amount', sortable: true,
                    tooltip:"jumlah pendapatan tahun ini",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.income_amount = value;
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Status", width : 100,
                    dataIndex : 'status', sortable: true,
                    readonly:true,
                    hidden :true,
                    tooltip:"Status",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                            fields: ["label"],
                                            data : [["Active"], ["InActive"]]
                                        }),
                            displayField:"label",
                            valueField:"label",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
            ];
            
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/income/1/10') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
              this.DataStore.load();

            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                // selModel: cbSelModel,
                enableColLock: false,
                loadMask: true,
                height : alfalah.income.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        // hidden :true,
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Delete',
                        id : 'btndelete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this,
                    },
                    {   text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        hidden :false,
                        scope : this
                    },
                    '-',
                    {   text:'Approve',
                        tooltip:'Approve Record',
                        iconCls: 'silk-tick',
                        hidden :true,
                        handler : this.Grid_approve,
                        scope : this
                    },
                    '->',
                    '_TOTAL USULAN PENDAPATAN_',
                    new Ext.form.TextField(
                    {   id : 'total_amount_id',
                        displayField: 'total_income',
                        valueField: 'total_income',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                ],
                listeners :
                    {   "afteredit" : this.Gridafteredit },
                scope : this,


            });

            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
        },
        // build the layout
        build_layout: function()
        {   
            if (Ext.isObject(this.Records))
                {   the_title = "Usulan Pendapatan"; }
            else { the_title = "Edit Usulan Pendapatan"; };

            this.Tab = new Ext.Panel(
            {   id : tabId+"_incomeTab",
                jsId : tabId+"_incomeTab",
                title:  '<span style="background-color: yellow;">'+the_title+'</span>',
                iconCls: 'silk-note_add',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region   : 'center', // center region is required, no width/height specified
                    xtype    : 'tabpanel',
                    activeTab: 0,
                    items    : [this.Form, this.detailTab]
                }]
            });
        },
    // finalize the component and layout drawing
    finalize_comp_and_layout: function()

    {   console.log('prepare');
            console.log(this.Records);
            if (Ext.isObject(this.Records))
            {   Ext.each(this.Records.fields.items,
                function(the_field)
                {   switch (the_field.name)
                    {   case "id" :
                        case "tahun_ajaran_id" :
                        case "income_description_id" :
                        case "cost_presentage_id":
                        case "status" :
                        case "created_date" :
                        {   Ext.getCmp('incomeFrm_'+the_field.name).setValue(this.Records.data[the_field.name]);
                        };
                        break;
                        case "income_master_no_id" :
                        {   Ext.getCmp('incomeFrm_'+the_field.name).setValue(this.Records.data[the_field.name]);

                            this.DataStore.baseParams = {   
                                s:"form", limit:this.page_limit, start:this.page_start,
                                income_master_no_id: this.Records.data[the_field.name]
                            };
                            this.DataStore.reload();
                        };
                        break;
                    };
                }, this);
            };

            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                {name: 'modified_date', type: 'date'},
            ]);
        },

    Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   id:"",
                    income_master_no:"",
                    income_detail_id: "",
                    income_name: "",
                    income_uraian: "New Uraian",
                    income_amount: 0,
                    status_name: "Active",
                    created_date: "",
                    modified_date: "",
                    action: "ADD",
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
    },

    Form_save : function(button, event)
        {   var head_data = [];
            var json_data = [];
            var modi_data = [];
            var v_json = {}; 
            
            console.log('form save');
            // header validation
            if (alfalah.core.validateFields([
                'incomeFrm_tahun_ajaran_id','incomeFrm_income_description_id']))
            {        
                head_data = alfalah.core.getHeadData([
                    'incomeFrm_id','incomeFrm_income_master_no_id', 'incomeFrm_income_description_id', 'incomeFrm_tahun_ajaran_id', 
                    'incomeFrm_cost_presentage_id','incomeFrm_status','incomeFrm_created_date',
                        // 'rapbsFrm_modified_date',
                ]);
                //detail data
                console.log(head_data);
                
                json_data = alfalah.core.getDetailData(this.DataStore.getModifiedRecords());
                // submit data
                console.log(json_data);
                alfalah.core.submitForm(
                    tabId+"_incomeTab", 
                    alfalah.income.income.DataStore,
                    "{{ url('/income/1/1') }}",
                    {   'x-csrf-token': alfalah.income.sid },
                    {   task: 'save',
                        head : Ext.encode(head_data),
                        json : Ext.encode(json_data),
                });
            };
        },

    Grid_remove: function(button, event)
        {var the_record = this.Grid.getSelectionModel().selection.record.data;
        this.Grid.stopEditing();
        if ( the_record )
            {   console.log('record yg dihapus');
                console.log(the_record);
                Ext.Msg.show(
                    {   title :'D E L E T E',
                        msg : 'DELETE DATA ?',
                        width:250,
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                        { 
                            if (buttonId =='yes')
                            {  
                                alfalah.core.submitGrid(
                                    // tabId+"_FormsTab", 
                                    alfalah.income.forms.DataStore,
                                    "{{ url('/income/1/12') }}",
                                    {   'x-csrf-token': alfalah.income.sid }, 
                                    {   id: the_record.id
                                    });
                            } 
                        },

                        icon: Ext.MessageBox.WARNING
                    });
            }
        else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },


    Gridafteredit : function(the_cell)
        { switch (the_cell.field)
            {   case "income_amount":
                { console.log('test');
                    var the_data = alfalah.income.forms.DataStore.data.items;
                    var total_income = 0;
                    console.log(the_data);
                    Ext.each(the_data,
                    function(the_record)
                        { 
                            total_income = total_income + parseInt(the_record.data.income_amount);
                        });
                    Ext.getCmp('total_amount_id').setValue(Ext.util.Format.number(total_income, '0,000'));
                }; 
                break;
            };
        },

        total_income: function()
        { 
            alfalah.income.forms.DataStore.on( 'load', function( store, records, options )
            {   var total_income = 0;
                Ext.each(records,
                function(the_record)
                        { 
                            total_income = total_income + parseInt(the_record.data.income_amount);
                        });
                    Ext.getCmp('total_amount_id').setValue(Ext.util.Format.number(total_income, '0,000'));
            });
        },

    };
}(); // end of app

Ext.onReady(alfalah.income.initialize, alfalah.income);
// end of file
</script>
<div>&nbsp;</div>