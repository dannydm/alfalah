<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.monitoringuser');

// create application
alfalah.monitoringuser = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
 
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',
  
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.monitoringuser.initialize();
            this.monitoringusercair.initialize();
            this.allstatus.initialize();
            this.status.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.monitoringuser.Tab);
            this.centerPanel.add(this.monitoringusercair.Tab);
            this.centerPanel.add(this.allstatus.Tab);
            this.centerPanel.add(this.status.Tab);
            this.centerPanel.setActiveTab(this.monitoringuser.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {  },
    }; // end of public space
}(); // end of app
// create application
alfalah.monitoringuser.monitoringuser= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.monitoringuser.centerPanel.container.dom.clientHeight;
            this.Columns = [  
                
            {   header: "RAPBS.No", width : 100,
                dataIndex : 'rapbs_no', sortable: true,
                tooltip:"RAPBS No",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   //approve
                    if ( record.data.approve_status_1 == null ) { metaData.attr = "style = background-color:"; }
                    // not yet approve
                    else if ( record.data.approve_status_3 == 1 ){ metaData.attr = "style = background-color:;"; }
                    // rejected
                    else {  metaData.attr = "style = background-color:;"; };
                    return value;
                },
            },
            {   header: "Organisasi / Urusan", width : 150,
                dataIndex : 'organisasi_mrapbs_id_name',
                sortable: true,
                hidden :true,
                tooltip:"Jenjang/Bidang/Departemen",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                    return alfalah.core.gridColumnWrap(result);
                }
            },
            {   header: "Program / Kegiatan", width : 150,
                dataIndex : 'program_mrapbs_id_name', sortable: true,
                tooltip:"Nama Program",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                    return alfalah.core.gridColumnWrap(result);
                }
            },
            {   header: "Sub Kegiatan", width : 100,
                dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                tooltip:"nama sub kegiatan",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   return alfalah.core.gridColumnWrap(value);
                }
            },
            {   header: "Sumber Dana", width : 150,
                dataIndex : 'sumberdana_id_name', sortable: true,
                tooltip:"sumber pendanaan",
                // editor : new Ext.form.TextField({allowBlank: false}),
            },
            {   header: "Keluaran", width : 150,
                dataIndex : 'keluaran', sortable: true,
                tooltip:"keluaran",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value); }
            },
            {   header: "Hasil", width : 150,
                dataIndex : 'hasil', sortable: true,
                tooltip:"hasil",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value); }
            },
            {   header: "Sasaran", width : 100,
                dataIndex : 'sasaran', sortable: true,
                tooltip:"sasaran",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value); }
            },

            {   header: "Coa", width : 150,
                dataIndex : 'coa_id_name', sortable: true,
                tooltip:"kode rekening",
                hidden : true,
                // editor : new Ext.form.TextField({allowBlank: false}),
            },
            {   header: "Jumlah Biaya", width : 100,
                id : 'total_biaya_urusan',
                hidden:true,
                dataIndex : 'total_biaya', sortable: true,
                tooltip:"Jumlah total biaya",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   metaData.attr="style = text-align:right;";
                    return Ext.util.Format.number(value, '0,000');
                },
            },
            {   header: "Jumlah Tahun Lalu", width : 100,
                dataIndex : 'jumlahn', sortable: true,
                tooltip:"Jumlah Anggaran Tahun Sebelumnya",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   metaData.attr="style = text-align:right;";
                    return Ext.util.Format.number(value, '0,000');
                },
            },
            {   header: "Jumlah Tahun Ke N", width : 100,
                dataIndex : 'jumlahke_n', sortable: true,
                tooltip:"Jumlah Anggaran Sampai dengan Tahun Lalu",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {   value = (record.data.total_biaya);
                    metaData.attr="style = text-align:right;";
                    return Ext.util.Format.number(value, '0,000');
                },
            },
            {   header: "% perubahan", width : 100,
                dataIndex : 'j_persen', sortable: true,
                tooltip:"Persentase naik / turun",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {  
                    t_pengurang = ((record.data.total_biaya)-(record.data.jumlahn));
                    value = (t_pengurang/(record.data.jumlahn)) * 100;
                    
                    if (value < 100.00) 
                        { metaData.attr = "style = background-color:cyan; "; }
                    else {  metaData.attr = "style = background-color:red; "; };
                    return Ext.util.Format.number(value, '000.00');

                },
            },
            {   header: "Status Pengesahan", width : 150,
                dataIndex : 'status_pengesahan', sortable: true,
                tooltip:" Status",
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {  
                    
                    if (record.data.approve_status_3 == 1) { value = "Telah Disahkan ;" ; }
                    else { value = "Belum Disahkan ;" ; } ;
                    
                    return value;
                },
            },
            {   header: "Status", width : 50,
                dataIndex : 'status', sortable: true,
                hidden :true,
                tooltip:" Status",
            },
            {   header: "Created", width : 50,
                dataIndex : 'created', sortable: true,
                tooltip:"monitoringuser Created Date",
                css : "background-color: #DCFFDE;",
                hidden :true,
                renderer: function(value){  return alfalah.core.dateRenderer(value); }
            },
            {   header: "Updated", width : 50,
                dataIndex : 'modified_date', sortable: true,
                tooltip:"monitoringuser Last Updated",
                css : "background-color: #DCFFDE;",
                hidden :true,
                renderer: function(value){  return alfalah.core.dateRenderer(value); }
            }
        ];
            this.Searchs = [
                {   id: 'monitoringuser_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringuser_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringuser_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringuser_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub. Keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringuser_sumberdana',
                    cid: 'sumber_dana_id_name',
                    fieldLabel: 'Dana',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },


                {   id: 'monitoringuser_status',
                    cid: 'status',
                    hidden :true,
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_monitoringuserSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.monitoringuser_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore( // monitoring user tab pertama master
                "{{url('/monitoring/4/0')}}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                stripeRows :true,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [ 
                        'TOTAL RAPBY. THN LALU',
                        new Ext.form.TextField(
                        {   id : 'total_thn_lalu_id',
                        // store:this.DataStore,
                        displayField: 'total_thn_lalu',
                        valueField: 'total_thn_lalu',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),'-',

                    'PERSENTASE + -',
                    new Ext.form.TextField(
                    {   id : 't_persen_id',
                        // store:this.DataStore,
                        displayField: 't_persen',
                        valueField: 't_persen',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                    '->',
                    'TOTAL RAPBY. THN INI',
                    new Ext.form.TextField(
                    {   id : 'grand_total_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),

                    ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_monitoringuserGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_monitoringuserPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_monitoringuserPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_monitoringuserGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
                listeners : { scope : this,
                    'cellclick': function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent)
                    {
                        console.log('cell click');
                        if (iColIdx == 0 ) // collom pertama
                        {   the_record = this.Grid.getSelectionModel().selection.record.data.rapbs_no
                            console.log(the_record);
                            // this.southdatastore.baseParam = {new param in here};
                            alfalah.monitoringuser.monitoringuser.SouthDataStore.baseParams = {   task: alfalah.monitoringuser.task,
                                act: alfalah.monitoringuser.act,
                                a:2, b:0, s:"form", rapbs_no : the_record,
                                limit:this.page_limit, start:this.page_start 
                            };                 
                            alfalah.monitoringuser.monitoringuser.SouthDataStore.reload();
                        };
                    }
                    ,
                    'rowselect' : function (a,b,c)
                    {   console.log('listener grid select');
                        console.log(a);
                        console.log(b);
                        console.log(c);

                        // this.south_datastore.baseParam = {new param in here};
                        // this.south_datastrore.reload();
                        }
                }
            });
            /**
            * SOUTH-GRID
            */
            this.EastDataStore = alfalah.core.newDataStore(
                "{{ url('/monitoring/4/10') }}", false, // monitoring user detail tab pertama
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.EastDataStore.load();
            this.EastGrid =   new Ext.grid.EditorGridPanel( 
            {  store : this.EastDataStore,
                columns: [ 
                    {   header: "ID", width : 50,
                        dataIndex : 'organisasi_mrapbs_id', sortable: true,
                        hidden :true,
                        tooltip:"ID",
                    },
                    {   header: "Nama Organisasi", width : 250,
                        dataIndex : 'organisasi_mrapbs_id_name', sortable: true,
                        tooltip:"Doc.No",
                    },
                    {   header: "Jumlah Usulan Anggaran", width : 150,
                        dataIndex : 'total_biaya', sortable: true,
                        tooltip:"jumlah anggaran yang telah diusulkan",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                            {   metaData.attr="style = text-align:right;";
                                return Ext.util.Format.number(value, '0,000');
                            },
                    },
                    {   header: "Jumlah Ajuan Anggaran", width : 150,
                        hidden :true,
                        dataIndex : 'total_biaya_ajuan', sortable: true,
                        tooltip:"jumlah anggaran yang telah diusulkan",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                            {   metaData.attr="style = text-align:right;";
                                return Ext.util.Format.number(value, '0,000');
                            },
                    },

                    {   header: "Jumlah Pencairan", width : 100,
                        hidden :true,
                        dataIndex : 'rapbs_name', sortable: true,
                        tooltip:"Doc.No",
                    },
                ],

                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [  ],
                bbar: new Ext.PagingToolbar(
                {  
                    id : tabId+'_eastGridBBar',
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_eastPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 50,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_eastPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_eastGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                
                }),

            });

            this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/monitoring/4/10') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.SouthDataStore.load();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.SouthDataStore,
                columns: [ 
                    {   header: "ID", width : 50,
                        dataIndex : 'rapbs_id', sortable: true,
                        tooltip:"ID",
                        hidden: true,
                    },
                    {   header: "No", width : 100,
                        dataIndex : 'rapbs_no', sortable: true,
                        tooltip:"Doc.No",
                    },
                    {   header: "Pos.Belanja", width : 300,
                        dataIndex : 'coa_id', sortable: true,
                        tooltip:"Pos Belanja",
                        readonly:true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>['+value+']</b>'+'&ensp;'+record.data.coa_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                    },
                    {   header: "Name", width : 200,
                        dataIndex : 'coa_name', sortable: true,
                        readonly:true,
                        hidden :true,
                        tooltip:"Pos Belanja Name",
                    },
                    {   header: "Uraian", width : 300,
                        dataIndex : 'uraian', sortable: true,
                        tooltip:"Uraian",
                        readonly:true,
                    },
                    {   header: "Volume", width : 100,
                        dataIndex : 'volume', sortable: true,
                        tooltip:"Volume",
                        readonly:true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                            {   record.data.volume = value;
                                value1 = Ext.util.Format.number(value, '0,000');
                                result = '<b>'+value1+'</b>'+'&ensp;'+record.data.satuan;
                                return alfalah.core.gridColumnWrap(result);
                            }
                    },
                    {   header: "Satuan", width : 100,
                        dataIndex : 'satuan', sortable: true,
                        tooltip:"Satuan",
                        hidden :true,
                        readonly:true,
                    },
                    {   header: "Tarif", width : 100,
                        dataIndex : 'tarif', sortable: true,
                        tooltip:"Tarif",
                        readonly:true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                            record.data.tarif = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Jumlah", width : 100,
                        dataIndex : 'jumlah', sortable: true,
                        readonly:true,
                        tooltip:"Jumlah",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {    metaData.attr="style = text-align:right;";
                            value = parseInt(record.data.volume) * parseInt(record.data.tarif);
                            record.data.jumlah = value;
                            return Ext.util.Format.number(value, '0,000');
                        }
                    },
                    {   header: "Ber-ulang", width : 60,
                        dataIndex : 'berulang', sortable: true,
                        hidden :true,
                        tooltip:"variabel pembagi anggaran yg berulang",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Status", width : 100,
                        dataIndex : 'status_name', sortable: true,
                        readonly:true,
                        hidden :true,   
                        tooltip:"Status",
                    },
                ],
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [  ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_newsGridBBar',
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_newsPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_newsPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_newsGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });

    // *****************************************
        
    },
    // build the layout
    build_layout: function()
    {   this.Tab = new Ext.Panel(
        {   id : tabId+"_monitoringuserTab",
            jsId : tabId+"_monitoringuserTab",
            title:  "MONITORING USULAN ANGGARAN",
            region: 'center',
            layout: 'border',
            items: [
            {   region: 'center',     // center region is required, no width/height specified
                xtype: 'container',
                layout: 'fit',
                items:[this.Grid]
            },
            {   title: 'Parameters',
                region: 'east',     // position for region
                split:true,
                width: 200,
                minSize: 175,
                maxSize: 400,
                collapsible: true,
                layout : 'accordion',
                items:[
                {   //title: 'S E A R C H',
                    labelWidth: 50,
                    defaultType: 'textfield',
                    xtype: 'form',
                    frame: true,
                    autoScroll : true,
                    items : this.Searchs,
                    tbar: [
                        {   text:'Search',
                            tooltip:'Search',
                            iconCls: 'silk-zoom',
                            handler : this.monitoringuser_search_handler,
                            scope : this,
                        }]
                }]
            },
            {
                region: 'south',
                title: 'DETAIL ANGGARAN',
                split: true,
                height : this.region_height/2,
                collapsible: true,
                margins: '0 0 0 0',
                layout : 'border',
                items :[ 
                {   
                    region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.SouthGrid]
                },
                // {   region: 'east',     // position for region
                //     // store : this.EastDatastore,
                //     title: 'INFORMASI',
                //     split:true,
                //     width: 550,
                //     minSize: 175,
                //     maxSize: 400,
                //     collapsible: true,
                //     layout : 'fit',
                //     items:[this.EastGrid]
                // }
                ]
            }]
        });
    },
    // finalize the component and layout drawing
    finalize_comp_and_layout: function()
    {
        this.DataStore.on( 'load', function( store, records, options )
        {
            console.log(records);
            var total_data = 0;
            var total_thn_lalu=0;
            var t_persentase=0;

            Ext.each(records,
                function(the_record)
                {  // console.log('cari record');
                   // console.log(the_record.data.jumlahn);
                    total_data = total_data + parseInt(the_record.data.total_biaya);
                    total_thn_lalu = total_thn_lalu + parseInt(the_record.data.jumlahn);

                    // t_pengurang = ((record.data.total_biaya)-(record.data.jumlahn));
                    // value = (t_pengurang/(record.data.jumlahn)) * 100;
                    t_persentase = total_data-total_thn_lalu;
                    t_persen = (t_persentase/total_thn_lalu)*100;
                });

            console.log("total persentase = "+t_persen);
            console.log("total tahun lalu = "+total_thn_lalu);
    //                console.log(Ext.getCmp('grand_total_id').setValue(total_data));
            Ext.getCmp('grand_total_id').setValue(Ext.util.Format.number(total_data, '0,000'));
            Ext.getCmp('total_thn_lalu_id').setValue(Ext.util.Format.number(total_thn_lalu, '0,000'));
            Ext.getCmp('t_persen_id').setValue(Ext.util.Format.number(t_persen, '000.00'));

        });
        // this.DataStore.load();

    },
    // monitoringuser search button
    monitoringuser_search_handler : function(button, event)
    {   var the_search = true;
        if ( this.DataStore.getModifiedRecords().length > 0 )
        {   Ext.Msg.show(
                {   title:'W A R N I N G ',
                    msg: ' Modified Data Found, Do you want to save it before search process ? ',
                    buttons: Ext.Msg.YESNO,
                    fn: function(buttonId, text)
                        {   if (buttonId =='yes')
                            {   Ext.getCmp(tabId+'_monitoringuserSaveBtn').handler.call();
                                the_search = false;
                            } else the_search = true;
                        },
                    icon: Ext.MessageBox.WARNING
                    });
        };

        if (the_search == true) // no modification records flag then we can go to search
        {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
            this.DataStore.baseParams = Ext.apply( the_parameter,
                {   task: alfalah.monitoringuser.task,
                    act: alfalah.monitoringuser.act,
                    a:2, b:0, s:"form", 
                    limit:this.page_limit, start:this.page_start });
            this.DataStore.reload(); 
        };
    },
}; // end of public space
}(); // end of app
// create application
alfalah.monitoringuser.monitoringusercair = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn_center;
    this.Searchs_center;
    this.expander;
    this.ExpanderDs;
    this.SouthGrid;
    this.SouthDataStore;
    this.SearchBtn_south;
    this.Searchs_south;
    this.Records_south;
    // private functions
 
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;

        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.monitoringuser.centerPanel.container.dom.clientHeight;

            this.ExpanderDs = alfalah.core.newDataStore(
                "{{ url('/monitoring/4/10') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.ExpanderDs.load();

            this.expander = new Ext.ux.grid.RowExpander({
                //  autoLoad:"{{ url('/monitoringuser/2/10') }}",
                store :this.ExpanderDs,
                typeAhead: false,
                forceSelection: true,
                pageSize:25,
                hideTrigger:true,
                itemSelector: 'x-data-table',
                valueField: 'name',
                tpl : new Ext.XTemplate(
                        //start table
                        '<table class="x-data-table" width="60%" style="margin:5px;">',
                            //Header table
                            '<tr>',
                                '<th width="5%">No.</th>',
                                '<th width="5%">No.Pengajuan</th>',
                                '<th width="5%">No. Rapby</th>',
                                '<th width="10%">Kode Rekening</th>',
                                '<th width="20%">Uraian</th>',
                                '<th width="10%">Jumlah Pengajuan</th>',
                                '</tr>',
                            
                            '<tpl for=".">', //Start loop tpl
                            '<tr>',
                                '<td width="5%">{#}</td>',
                                '<td width="5%">{rapbs_no}</td>',
                                '<td width="5%">{rapbs_no}</td>',
                                '<td width="10%">{coa_id}</td>',
                                '<td width="20%">{uraian}</td>',
                                '<td width="10%">{jum_ajuan}</td>',
                            '</tr>',
                            '</tpl>', //End loop tpl
                        '</table>' //Close table
                    ),
                    listeners  : {
                    scope: this,

                    'expand' : function(rowNode, record, expandRow, eOpts){ 
                        if (record){
                            var the_record = record.data;
                            console.log('rowNode.store');
                            console.log(rowNode);
                            alfalah.monitoringuser.monitoringusercair.ExpanderDs,
                            // console.log(the_record);
                                    alfalah.monitoringuser.monitoringusercair.ExpanderDs.baseParams = { task: alfalah.monitoringuser.task,
                                    act: alfalah.monitoringuser.act,
                                    a:2, b:0, pengajuan_no : the_record.pengajuan_no,
                                    limit:this.page_limit, start:this.page_start 
                                };                 
                                alfalah.monitoringuser.monitoringusercair.ExpanderDs.reload();
                         }
  //                      Ext.get(expandRow).setHTML(alfalah.monitoringuser.monitoringusercair.ExpanderDs.baseParams);
                    }
                },

                    
            });
            this.Columns = [ 
                // this.expander,
                

                {   header: "Nomor Pengajuan", width : 100,
                    dataIndex : 'pengajuan_no', sortable: true,
                    tooltip:"Nomor Uang muka",
                },
                {   header: "Keterangan", width : 150,
                    dataIndex : 'pengajuan_keterangan_id',
                    sortable: true,
                    tooltip:"Keterangan Pengajuan",
                },
                {   header: "Organisasi / Urusan", width : 200,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    hidden : true, 
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 200,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    hidden : true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Jumlah Pengajuan", width : 100,
                    // hidden:true,
                    dataIndex : 'total_biaya', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Status Release", width : 100,
                    dataIndex : 'status_pengajuan_id',
                    sortable: true,
                    tooltip:"Status Pengajuan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_1 == 0) { value = 'Not Yet release'; }
                        // not yet approve
                        else if  ( record.data.approve_status_1 == 1){ value = 'Release'; };
                        
                        return value;
                    }
                },
                {   header: "Approve Kabid", width : 120,
                    dataIndex : 'status_approve_2',
                    sortable: true,
                    tooltip:"Status Pengajuan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_2 == null) { value = 'Not Yet Approve'; }
                        // not yet approve
                        else if  ( record.data.approve_status_2 == 1){ value = 'Approve by Kabid'; };
                        
                        return value;
                    }
                },
                {   header: "Approve Ketua", width : 120,
                    dataIndex : 'status_approve_3',
                    sortable: true,
                    tooltip:"Status Pengajuan anggaran",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   //approve
                        if ( record.data.approve_status_3 == null) { value = 'Not Yet Approve'; }
                        // not yet approve
                        else if  ( record.data.approve_status_3 == 1){ value = 'Approve by Ketua'; };
                        
                        return value;
                    }
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"monitoringuser Created Date",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"monitoringuser Last Updated",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Searchs_center = [
                {   id: 'monitoringuserum_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringuserum_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringuserum_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringuserum_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub. Keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn_center = new Ext.Button(
                {   id : tabId+"_monitoringuserumSearchBtn_center",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.monitoringuserum_search_handler_center,
                    scope : this
                });
                this.Searchs_south = [
                {   id: 'monitoringusercair_organisasi',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringusercair_urusan',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringusercair_program',
                    cid: 'program_mrapbs_id_name',
                    fieldLabel: 'Program',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringusercair_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub. Keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'monitoringusercair_status',
                    cid: 'approve_status_3',
                    fieldLabel: 'By Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['label','key'],
                        data : [["ALL", 0], ["RECEIVED", 1]]
                    }),
                    displayField:'label',
                    valueField :'key',    
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100
                    // value: 'Active'
                },

            ];
            this.SearchBtn_south = new Ext.Button(
                {   id : tabId+"_monitoringuserumSearchBtn_south",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.monitoringuser_search_handler_south,
                    scope : this
                });

            /**
                * MAIN-GRID
            */
            this.DataStore = alfalah.core.newDataStore( //monitoring user status pengajuan 4.1
                "{{ url('/monitoring/4/1') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                // plugins: this.expander,
                stripeRows :true,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    '->',
                    'TOTAL PENGAJUAN',
                    new Ext.form.TextField(
                    {   id : 'grand_pengajuanum_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),

                ],


                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_monitoringuserumGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_monitoringuserumPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_monitoringuserumPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_monitoringuserumGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn_center.handler.call(this.SearchBtn_center.scope);
                                }
                            }
                        }),
                        ' records at a time tes'
                    ]
                }),
                
            });

            /**
                * SOUTH-GRID
            */
            this.SouthDataStore = alfalah.core.newDataStore( //monitoring user status pencairan 4.2
                "{{ url('/monitoring/4/2') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   /*id : tabId+'_monitoringusercairSouthGrid',
                jsId : tabId+'_monitoringusercairSouthGrid',*/
                store:  this.SouthDataStore,
                columns: [ 
                    {   header: "Nomor Uang Muka", width : 100,
                        dataIndex : 'uangmuka_no', sortable: true,
                        tooltip:"Nomor Uang muka",
                    },
                    {   header: "RAPBS.No", width : 100,
                        dataIndex : 'rapbs_no', sortable: true,
                        tooltip:"RAPBS No",
                    },
                    {   header: "Keterangan", width : 150,
                        dataIndex : 'uangmuka_keterangan_id',
                        sortable: true,
                        tooltip:"Keterangan Pengajuan",
                    },
                    {   header: "Organisasi / Urusan", width : 200,
                        dataIndex : 'organisasi_mrapbs_id_name',
                        sortable: true,
                        hidden : true,
                        tooltip:"Jenjang/Bidang/Departemen",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                            return alfalah.core.gridColumnWrap(result);
                        }
                    },
                    {   header: "Program / Kegiatan", width : 200,
                        dataIndex : 'program_mrapbs_id_name', sortable: true,
                        tooltip:"Nama Program",
                        hidden : true,
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                            return alfalah.core.gridColumnWrap(result);
                        }
                    },
                    {   header: "Sub Kegiatan", width : 200,
                        dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                        tooltip:"nama sub kegiatan",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   return alfalah.core.gridColumnWrap(value);
                        }
                    },
                    {   header: "Jumlah Pengajuan", width : 100,
                        // hidden:true,
                        dataIndex : 'total_biaya', sortable: true,
                        tooltip:"Jumlah total biaya",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   metaData.attr="style = text-align:right;";
                            return Ext.util.Format.number(value, '0,000');
                        },
                    },
                    {   
                        header: "Jatuh Tempo", width : 150,
                        dataIndex : 'uangmuka_due_date',
                        sortable: true,
                        tooltip:"Status pengajuan uang muka",
                        renderer : function (value, metaData, record, rowIndex, colIndex, store)
                        {  
                                today = new Date().format("Y-m-d H:i:s");
                                start_date = record.data.bln_pengajuan;
                                end_date   = record.data.uangmuka_due_date ;
                                get_seven_day_before =  new Date(record.data.uangmuka_due_date); 
                                get_seven_day_before.setDate( new Date(get_seven_day_before.getDate()) - 7);
                                c = get_seven_day_before.format("Y-m-d H:i:s");

                                console.log(record.data.approve_status_3); 

                                if ((record.data.approve_status_3 == 1) && (today > end_date))
                                {  console.log('hihihiih');
                                    metaData.attr = "style = background-color:lime;" } 
                                else if ((record.data.approve_status_3 == 0)  && (today >= end_date))
                                {  console.log('hahahahha');
                                    metaData.attr = "style = background-color:red;" }
                                else 
                                { console.log('heheheheh');
                                    metaData.attr = "style = background-color:yellow;"; };
                                    return Ext.util.Format.date(value, "l, d F, Y");
                           // return value;
                        }
                    },
                    {   
                        header: "Status UangMuka", width : 100,
                        dataIndex : 'approve_status_1',
                        sortable: true,
                        tooltip:"Status pengajuan uang muka",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   //
                            if ( record.data.approve_status_1 == 0)  { value = 'Not Yet Approve' }
                            else  { value = 'Approve By Kabid'; };
                            return value;
                        }
                    },

                    {   id : 'status_pencairan_id',
                        header: "Status Pencairan", width : 150,
                        dataIndex : 'approve_status_3',
                        sortable: true,
                        tooltip:"Status Pencairan anggaran",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   //
                            if (( record.data.approve_status_2 == 1) && ( record.data.approve_status_3 == 0 )) { value = 'Please Contact Finance'; }
                            // not yet approve
                            else if  (( record.data.approve_status_2 == 1) && ( record.data.approve_status_3 == 1 )){ value = 'Received'; }
                            else { value = 'Not Yet Approved By Finance'; };
                            
                            return value;
                        }
                    },
                    {   header: "Created", width : 50,
                        dataIndex : 'created', sortable: true,
                        tooltip:"monitoringuser Created Date",
                        css : "background-color: #DCFFDE;",
                        hidden :true,
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    },
                    {   header: "Updated", width : 50,
                        dataIndex : 'modified_date', sortable: true,
                        tooltip:"monitoringuser Last Updated",
                        css : "background-color: #DCFFDE;",
                        hidden :true,
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    }
                ],

                // selModel: cbSelModel,
                //selModel: new Ext.grid.RowSelectionModel(),
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                stripeRows :true,
                frame: true,
                tbar: [
                    '->',
                    'TOTAL PENCAIRAN',
                    new Ext.form.TextField(
                    {   id : 'total_pencairan_id',
                        // store:this.DataStore,
                        displayField: 'total_pencairan',
                        valueField: 'total_pencairan',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_monitoringusercairSouthGridBBar',
                    store: this.SouthDataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_monitoringusercairSouthGridPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_monitoringusercairSouthGridPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_monitoringusercairSouthGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn_south.handler.call(this.SearchBtn_south.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),

            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_monitoringusercairTab", 
                jsId : tabId+"_monitoringusercairTab",
                title:  "MONITORING PENGAJUAN BULANAN",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    title: 'monitoring Pengajuan Bulanan',
                    split: true,
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'ToolBox',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    { //  title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs_center,
                        tbar: [
                        {   text:'Search',
                            tooltip:'Search',
                            iconCls: 'silk-zoom',
                            handler : this.monitoringuser_search_handler_center,
                            scope : this,
                        }]
                        
                    },
                    { title : 'Setting'
                    }]
                },
                {// lazily created panel (xtype:'panel' is default)
                    region: 'south',
                    title: 'Monitoring Status Uang Muka',
                    split: true,
                    height : this.region_height/2,
                    layout : 'border',
                    collapsible: true,
                    margins: '0 0 0 0',
                    items :[ 
                    {   
                        region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.SouthGrid]
                    },
                    {   region: 'east',     // position for region
                        title: 'ToolBox',
                        split:true,
                        width: 200,
                        minSize: 175,
                        maxSize: 400,
                        collapsible: true,
                        layout : 'accordion',
                        items:[
                        {   title: 'S E A R C H',
                            labelWidth: 50,
                            defaultType: 'textfield',
                            xtype: 'form',
                            frame: true,
                            autoScroll : true,
                            items : this.Searchs_south, 
                            tbar: [
                                {   text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.monitoringuser_search_handler_south,
                                    scope : this,
                                }] 
                            
                        },
                        { title : 'Setting'
                        }]
                    }]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
            this.DataStore.on( 'load', function( store, records, options )
                {
                    console.log(records);
                    var total_pengajuan = 0;
                    Ext.each(records,
                        function(the_record)
                        {  // console.log('cari record');
                            console.log(the_record.data.approve_status_3);
                            total_pengajuan = total_pengajuan + parseInt(the_record.data.total_biaya);
                        });
                    Ext.getCmp('grand_pengajuanum_id').setValue(Ext.util.Format.number(total_pengajuan, '0,000'));
                });

                this.SouthDataStore.on( 'load', function( store, records, options )
                {
                   // console.log(records);
                    var total_pencairan = 0;
                    Ext.each(records,
                        function(the_record)
                        {  // console.log('cari record');
                           // console.log(the_record.data.approve_status_3);
                            // if (the_record.data.approve_status_3==1)
                            total_pencairan = total_pencairan + parseInt(the_record.data.total_biaya);
                         });


                        Ext.getCmp('total_pencairan_id').setValue(Ext.util.Format.number(total_pencairan, '0,000'));
                });

        },

        // monitoringusercair search button
        monitoringuser_search_handler_center : function(button, event)
        {   var the_search_center = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_monitoringusercairSaveBtn').handler.call();
                                    the_search_center = false;
                                } else the_search_center = true;
                            },
                        icon: Ext.MessageBox.WARNING
                        });
            };

            if (the_search_center == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs_center);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.monitoringuser.task,
                        act: alfalah.monitoringuser.act,
                        s:"form", 
                        limit:this.page_limit, start:this.page_start });
                        this.DataStore.reload();
            };
        },

        monitoringuser_search_handler_south : function(button, event)
        {   var the_search_south = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_monitoringusercairSaveBtn').handler.call();
                                    the_search_south = false;
                                } else the_search_south = true;
                            },
                        icon: Ext.MessageBox.WARNING
                        });
            };

            if (the_search_south == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs_south);
                this.SouthDataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.monitoringuser.task,
                        act: alfalah.monitoringuser.act,
                        s:"form", 
                        limit:this.page_limit, start:this.page_start });
                this.SouthDataStore.reload();
            };
        },        
    }; // end of public space
}(); // end of app
alfalah.monitoringuser.allstatus= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
 
            this.Columns = [
              
                {   header: "RAPBS.No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"RAPBS No",
                },
                {   header: "Pengajuan.No", width : 100,
                    dataIndex : 'pengajuan_no', sortable: true,
                    tooltip:"PENGAJUAN No",
                },
                {   header: "Uang Muka .No", width : 100,
                    dataIndex : 'uangmuka_no', sortable: true,
                    tooltip:"UANG MUKA No",
                },
                {   header: "Organisasi / Urusan", width : 200,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    hidden : true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 200,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    hidden : true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Jumlah Tahun Ke N", width : 100,
                    dataIndex : 'jumlahke_n', sortable: true,
                    tooltip:"Jumlah Anggaran Sampai dengan Tahun Lalu",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   value = (record.data.total_biaya);
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Pengajuan", width : 100,
                    dataIndex : 'total_biaya_ajuan', sortable: true,
                    tooltip:"Jumlah pada waktu pengajuan biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   value = (record.data.total_biaya_ajuan);
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Jumlah Uang Muka", width : 100,
                    dataIndex : 'jumlah_uangmuka', sortable: true,
                    tooltip:"jumlah pada waktu pengajuan uang muka",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   value = (record.data.jumlah_uangmuka);
                        metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Tgl Pengesahan Ketua", width : 100,
                    dataIndex : 'ketua_approve_date', sortable: true,
                    tooltip:"tanggal pengesahan oleh ketua yayasan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return Ext.util.Format.date(value, "l, d F, Y"); },
                },
                {   header: "Tgl Pengajuan", width : 100,
                    dataIndex : 'pengajuan_approve_date', sortable: true,
                    tooltip:"tanggal pengajuan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return Ext.util.Format.date(value, "l, d F, Y"); },
                },
                {   header: "Tgl Pencairan", width : 100,
                    dataIndex : 'pencairan_approve_date', sortable: true,
                    tooltip:"tanggal pencairan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return Ext.util.Format.date(value, "l, d F, Y"); },
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    hidden:true,
                    tooltip:"allstatus Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    hidden:true,
                    tooltip:"allstatus Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                // {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'allstatus_organisasi',
                    cid: 'organisasi_mrapbs_id_name',
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'allstatus_urusan',
                    cid: "urusan_mrapbs_id_name",
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'allstatus_kegiatan',
                    cid: 'kegiatan_mrapbs_id_name',
                    fieldLabel: 'Kegiatan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'allstatus_sub_kegiatan',
                    cid: 'sub_kegiatan_mrapbs_id',
                    fieldLabel: 'sub keg.',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'allstatus_status',
                    cid: 'status',
                    hidden :true,
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },

            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_allstatusSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.allstatus_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore( // monitoring user untuk allstatus 4/15
                "{{url('/monitoring/4/15')}}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.monitoringuser.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    // '->',
                    // '_T O T A L_',
                    new Ext.form.TextField(
                    {   id : 'total_thnini_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        hidden : true,
                        readOnly : true,
                        // style: "text-align: right",
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        scope : this,
                    }),
                        ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_allstatusGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_allstatusPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_allstatusPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_allstatusGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_allstatusSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_allstatusTab",
                jsId : tabId+"_allstatusTab",
                title:  "ALL STATUS",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.allstatus_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        //  {},

        // Grid_grand_total : function(the_cell)
        {
            this.DataStore.on( 'load', function( store, records, options )
            {
                console.log(records);
                var total_data = 0;
                Ext.each(records,
                    function(the_record)
                    { 
                        total_data = total_data + parseInt(the_record.data.total_biaya);
                    });

                console.log("total jumlah = "+total_data);
                Ext.getCmp('total_thnini_id').setValue(Ext.util.Format.number(total_data, '0,000'));
            });
        },

        
        Gridafteredit : function(the_cell)
        { switch (the_cell.field)
            {   case "volume":
                case "tarif":
                {
                    var total_biaya = 0;
                    Ext.each(the_data,
                    function(the_record)
                    { 
                        total_biaya = total_biaya + parseInt(the_record.data.jumlah);
                    });
            
                    Ext.getCmp('total_biaya_id').setValue(Ext.util.Format.number(total_nota, '0,000'));

                }; 
                break; 
            };
        },

        // allstatus search button
        allstatus_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_allstatusSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                    });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.monitoringuser.task,
                        act: alfalah.monitoringuser.act,
                        a:3, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.monitoringuser.status = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.o_pass = '';
    this.n_pass = '';
    this.MyPr_Grid;
    this.MyPr_Columns;
    this.MyPr_Records;
    
    this.AngGridPanel;
    this.anggaranTab;
    this.Revenue_grid;
    // private functions

    // public space
    return {
        // execute at the very last time
        //public variable
        refresh_time : ((1000*60)*15), // 1000 = 1 second
        centerPanel : 0,
        page_limit : 75,
        page_start : 0,
        tabId : '{{ $TABID }}',
        task : '',
        act : '',
        // public methods
        initialize: function()
        {   Ext.chart.Chart.CHART_URL = "asset('/js/ext3/resources/charts.swf') }}";
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.tools = [
            {   id:'gear',
               handler: function()
                   {   Ext.Msg.alert('Message', 'The Settings tool was clicked.'); }
            },
            {   id:'close',
                handler: function(e, target, panel)
                    {   panel.ownerCt.remove(panel, true);  }
            }];

            this.centerPanel = Ext.getCmp(this.tabId);
        },

        // build the layout
        build_layout: function()
        { 
            this.Tab = new Ext.Panel(
            {   region: 'center',
                title : 'MONITORING STATUS APPROVAL',
               // autoScroll  : true,
                items: [
                    {   xtype:'portal',
                        region:'center',
                        margins:'1 1 1 1',
                        autoScroll  : true,
                        items:[
                            {   columnWidth:.98,
                                style:'padding: 10px 0 5px 10px ',
                                title: 'STATUS',
                                // xtype: 'container',
                                // height : 650,
                                // region: 'center',  
                                // autoScroll  : true,
                                // layout : 'fit',
                                // contentEl: 'test',
                                items: [
                                    {   title: 'HISTORY',
                                        region: 'center',     // center region is required, no width/height specified
                                        width: 800,
                                        height : 600,
                                        autoScroll  : true,
                                        layout : 'fit',
                                        items: [{
                                                xtype: 'container',
                                                contentEl: 'status_all_item',
                                                
                                            },
                                            ]

                                    }
                                ]

                            }
                        ]
                    },
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {  // this.centerPanel.add(this.Tab);
           // alfalah.core.viewport.doLayout();
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.monitoringuser.initialize, alfalah.monitoringuser);
// end of file
</script>

<body>
<div id="status_all_item">
<h2>TREE VIEW</h2>
<p>Click on item(s) to open or close the tree branches.</p>
<!-- <div class="table-responsive"> -->
    <table class="x-data-table">
        <thead>
            <tr>
                <th style="text-align: center;" width="10%"><h3>Rapbs No</h3></th>
                <th style="text-align: center;" width="20%"><h3>Kegiatan</h3></th>
                <th style="text-align: center;" width="40%"><h3>Sub Kegiatan</h3></th>
                <th style="text-align: center;" width="40%"><h3>Tgl Pengesahan</h3></th>
            </tr>
        </thead>
        <tbody>
            @foreach($ALLSTATUS as $keyfirst=>$status)
                <tr  class='expand' place = {{$keyfirst}}>
                    <td width="10%">{{$status->rapbs_no}}</td>  
                    <td width="20%">{{$status->kegiatan_mrapbs_id_name}}</td>
                    <td width="40%">{{$status->sub_kegiatan_mrapbs_id}}</td>
                    <td style="text-align: center;" width="40%">{{$status->ketua_approve_date}}</td> 
                </tr>
                <!-- tree pertama -->
                    <div class="container">
                        <td class='desc'  colspan ="6" style="padding:5;" id={{$keyfirst}} >
                            <table class ="x-head-hijau" width = "98%" style="padding-left:20px;">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;" width="10%"><h3>Pengajuan_no</h3></th>
                                        <th style="text-align: center;" width="20%"><h3>Kegiatan</h3></th>
                                        <th style="text-align: center;" width="40%"><h3>Sub Kegiatan</h3></th>
                                        <th style="text-align: center;" width="10%"><h3>Release Date</h3></th>
                                        <th style="text-align: center;" width="10%"><h3>Approval Kabid Date</h3></th>
                                        <th style="text-align: center;" width="10%"><h3>Approval Ketua Date</h3></th>

                                    </tr>
                                </thead>
                                <tbody>
                                
                                @foreach($PENGAJUANSTATUS as $keysecond=>$statuspengajuan)
                                    @if ( $status->rapbs_no == $statuspengajuan->pengajuan_rapbs) 
                                        <tr class='expand' place = r-{{$keysecond}}>
                                            <td>{{$statuspengajuan->pengajuan_no}}</td>  
                                            <td>{{$statuspengajuan->pengajuan_keterangan}} <p>{{$statuspengajuan->pengajuan_rapbs}}</p></td>
                                            <td>{{$statuspengajuan->pengajuan_sub_kegiatan}}</td>
                                            <td style="text-align: center;">{{$statuspengajuan->release_date}}</td>
                                            <td style="text-align: center;">{{$statuspengajuan->kabid_date}}</td>
                                            <td style="text-align: center;">{{$statuspengajuan->ketua_date}}</td>
                                        </tr>
                                        <!-- tree kedua -->
                                        <div class="container">
                                            <td class='desc'  colspan ="9" style="padding:2;" id=r-{{$keysecond}} >
                                                <table class ="x-head-merah" width = "100%" style="padding-left:10px;">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center;" width="9%"><h3>Uang Muka</h3></th>
                                                            <th style="text-align: center;" width="15%"><h3>Kegiatan</h3></th>
                                                            <th style="text-align: center;" width="25%"><h3>Sub Kegiatan</h3></th>
                                                            <th style="text-align: center;" width="10%"><h3>Release Date</h3></th>
                                                            <th style="text-align: center;" width="10%"><h3>Approval Kabid Date</h3></th>
                                                            <th style="text-align: center;" width="10%"><h3>Approval Finance Date</h3></th>
                                                            <th style="text-align: center;" width="10%"><h3>Receive Date</h3></th>
                                                            <th style="text-align: center;" width="10%"><h3>App Realisasi Kabid</h3></th>
                                                            <th style="text-align: center;" width="10%"><h3>App Realisasi Finance</h3></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($UANGMUKASTATUS as $statusuangmuka)
                                                        @if ( $statuspengajuan->pengajuan_no == $statusuangmuka->pengajuan_no) 
                                                            <tr>
                                                                <td>{{$statusuangmuka->uangmuka_no}}</td>  
                                                                <td>{{$statusuangmuka->uangmuka_keterangan}} <p>{{$statusuangmuka->uangmuka_rapbs}}</p></td>
                                                                <td>{{$statusuangmuka->uangmuka_sub_kegiatan}}</td>
                                                                <td style="text-align: center;">{{$statusuangmuka->release_date}}</td>
                                                                <td style="text-align: center;">{{$statusuangmuka->uangmuka_kabid_date}}</td>
                                                                <td style="text-align: center;">{{$statusuangmuka->finance_approve_date}}</td>
                                                                <td style="text-align: center;">{{$statusuangmuka->receive_date}}</td>
                                                                <td style="text-align: center;">{{$statusuangmuka->approve_realisasi_kabid}}</td>
                                                                <td style="text-align: center;">{{$statusuangmuka->approve_realisasi_finance}}</td>
                                                                
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </td>
                                        </div>
                                         <!-- batas tree kedua -->
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </td>
                </div>
                <!-- batas tree pertama -->
            @endforeach
        </tbody>
    </table>
<!-- </div> -->


    <script>
    $(document).ready(function() {
        $('tr.expand').on("click", function() {
            link = "td#" + $(this).attr("place");
            // $key =0;
            duration = 350;
            $(link).toggle(duration);
        });
    });
    </script>
</div>
</body>

<style>
    .x-data-table {
        border-collapse: collapse;
        font-size: 12px;
        max-width: 98%;
    }

    .x-data-table th {
        background-color: #508abb;
        color: #FFFFFF;
        border-color: #6ea1cc !important;
        text-transform: uppercase;
    }

        .x-data-table tbody tr:nth-child(odd) td {
    	background-color: #f4fbff;
    }

        .x-data-table tbody tr:hover td {
        background-color: #ffffa2;
        border-color: #ffff0f;
    }

    .x-data-table td,
        th {
            padding: .4em;
            border: 1px solid 000000#;
            color: #353535;
    }


    .x-head-hijau thead th {
        text-transform: uppercase;
        color: #FFFFFF;
        font-size: 12px;
        background-color: #00b050;
    }

    .x-head-hijau tbody td {
        color: #400040;
        background-color: #d6ffda;
        /* text-align: center; */
    }

    .x-head-hijau tbody tr:hover td {
        background-color: #ffffa2;
        border-color: #ffff0f;
    }

    .x-head-hijau tbody tr:nth-child(odd) td {
    	background-color: #f4fbff;
    }

    .x-head-merah thead th {
        text-transform: uppercase;
        background-color: #ff5f5f;
        color: #FFFFFF;
        font-size: 10px;
    }

    .x-head-merah tbody td,
    .x-head-merah tbody td:last-child {
        color: #353535;
        border-bottom: 1px solid #CCCCCC;
        font-size: 10px;
    }

    tr.expand td, th {border-style:solid none solid none;}
    tr.expand td:hover {cursor: pointer;}
    td.desc {display:none; background:lightblue; padding:3px; }
    
</style>
