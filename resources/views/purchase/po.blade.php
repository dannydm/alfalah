<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.purchase.po');

// create application
alfalah.purchase.po = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.po.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.po.Tab);
            this.centerPanel.setActiveTab(this.po.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.purchase.po.po= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Company", width : 100,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"Company ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.company_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "PO.No", width : 150,
                    dataIndex : 'po_no', sortable: true,
                    tooltip:"PO No",
                },
                {   header: "PO.Date", width : 100,
                    dataIndex : 'po_date', sortable: true,
                    tooltip:"PO Date",
                    // renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Supplier", width : 200,
                    dataIndex : 'supplier_name', sortable: true,
                    tooltip:"Supplier Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.supplier_id;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "PO.Type", width : 80,
                    dataIndex : 'po_type', sortable: true,
                    tooltip:"PO Type",
                },
                {   header: "Status", width : 80,
                    dataIndex : 'po_status_name', sortable: true,
                    tooltip:"PO Status",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.po_status == 1)
                        {   metaData.attr = "style = background-color:lime;";  }
                        else {   metaData.attr = "style = background-color:yellow;"; };
                        return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Approved", width : 100,
                    dataIndex : 'approved_status_name', sortable: true,
                    tooltip:"Approved",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( Ext.isEmpty(record.data.approved_date) )
                        {   metaData.attr = "style = background-color:red;";  }
                        else {   metaData.attr = "style = background-color:lime;"; };
                        return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"PO Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"pr Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Searchs = [
                {   id: 'po_po_no',
                    cid: 'po_no',
                    fieldLabel: 'PO.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'po_supplier_name',
                    cid: 'supplier_name',
                    fieldLabel: 'Supplier',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'po_po_status',
                    cid: 'po_status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['Draft'], ['Final']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Draft'
                },
                {   id: 'po_approved_status',
                    cid: 'approved_status',
                    fieldLabel: 'Approved',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['Approved'], ['Need Approval']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Need Approval'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_poSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.po_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                '/purchase/2/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.purchase.po.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    // {   text:'Copy',
                    //     tooltip:'Copy Record',
                    //     iconCls: 'silk-page-copy',
                    //     handler : this.Grid_copy,
                    //     scope : this
                    // },
                    {   text:'Edit',
                        tooltip:'Edit Record',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_edit,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : this.print_out,
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                    {   text:'Approve',
                        tooltip:'Approve Record',
                        iconCls: 'silk-tick',
                        handler : this.Grid_approve,
                        scope : this
                    },
                    '-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_poGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_poPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_poPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_poGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_poSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_poTab",
                jsId : tabId+"_poTab",
                title:  "Purchase Order",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 50,
                                defaultType: 'textfield',
                                items : this.Searchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   //id : 'search_btn_pritems',
                                    text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.po_search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        Grid_add : function(button, event)
        {   var po_form = Ext.getCmp("po_form");
            if (po_form)
            {   Ext.Msg.show(
                {   title :'E R R O R ',
                    msg : 'PO Form Available',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
            else
            {   var centerPanel = Ext.getCmp('center_panel');
                alfalah.purchase.po.forms.initialize(null);
                centerPanel.beginUpdate();
                centerPanel.add(alfalah.purchase.po.forms.Tab);
                centerPanel.setActiveTab(alfalah.purchase.po.forms.Tab);
                centerPanel.endUpdate();
                alfalah.core.viewport.doLayout();
            };
        },
        Grid_edit : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   if (the_record.record.data.grin_count > 0)
                {   Ext.Msg.show(
                    {   title :'F A I L E D ',
                        msg : 'Incoming Transaction Found.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   
                    centerPanel = Ext.getCmp('center-panel');
                    var po_form = Ext.getCmp("po_form");
                    if (po_form)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'PO Form Available',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                    else
                    {   var centerPanel = Ext.getCmp('center_panel');
                        alfalah.purchase.po.forms.initialize(the_record.record);
                        centerPanel.beginUpdate();
                        centerPanel.add(alfalah.purchase.po.forms.Tab);
                        centerPanel.setActiveTab(alfalah.purchase.po.forms.Tab);
                        centerPanel.endUpdate();
                        alfalah.core.viewport.doLayout();
                    };
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   if (the_record.record.data.grin_count > 0)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Incoming Transaction Found.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   alfalah.core.submitGrid(
                        this.DataStore, 
                        '/purchase/2/2',
                        {   'x-csrf-token': alfalah.purchase.po.sid }, 
                        {   id: the_record.record.data.id }
                    );
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_approve : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   if (the_record.record.data.po_status == 0)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Need Final PO',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   alfalah.core.submitGrid(
                        this.DataStore, 
                        '/purchase/2/8',
                        {   'x-csrf-token': alfalah.purchase.po.sid }, 
                        {   id: this.Grid.getSelectionModel().selection.record.data.id }
                    );
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        // pr search button
        po_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_poSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
        print_out: function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                alfalah.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        id : the_record.id });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };   
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.purchase.po.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        isRecalculate : false,
        // public methods
        initialize: function(the_record)
        {   this.Records = the_record;
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.isRecalculate = false;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.CurrencyDS = alfalah.core.newDataStore(
                '/country/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SupplierDS = alfalah.core.newDataStore(
                '/company/2/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.DeptDS = alfalah.core.newDataStore(
                '/company/6/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            
            this.ItemDS = alfalah.core.newDataStore(
                '/purchase/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.PaymentTermDS = alfalah.core.newDataStore(
                '/parameter/4/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   id : 'pof_form',
                title : 'H E A D E R',
                bodyStyle:'padding:5px',
                // autoScroll : true,
                // autoHeight : true,
                
                items: [
                {   layout:'column',
                    border:false,
                    items:[ // hidden columns
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'pof_id',
                            xtype:'textfield',
                            fieldLabel: 'ID',
                            name: 'id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'pof_company_id',
                            xtype:'textfield',
                            fieldLabel: 'Company.ID',
                            name: 'company_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'pof_supplier_id',
                            xtype:'textfield',
                            fieldLabel: 'Supplier.ID',
                            name: 'supplier_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'pof_po_status',
                            xtype:'textfield',
                            fieldLabel: 'PO.Status',
                            name: 'pof_status',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'pof_to_site_id',  
                            xtype:'textfield',
                            fieldLabel: 'Site.ID',
                            name: 'to_site_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }, 
                        {   id : 'pof_to_dept_id', 
                            xtype:'textfield',
                            fieldLabel: 'Dept.ID',
                            name: 'to_dept_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'pof_payment_term_id', 
                            xtype:'textfield',
                            fieldLabel: 'Payment.Term.ID',
                            name: 'payment_term_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'pof_created_date',  
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            hidden : true,
                        }]
                    }, 
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'pof_po_date', 
                            xtype:'datefield',
                            fieldLabel: 'PO Date',
                            name: 'po_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            value : new Date()
                        },
                        {   id : 'pof_po_no', 
                            xtype:'textfield',
                            fieldLabel: 'PO.No',
                            name: 'po_no',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                        },
                        {   id : 'pof_currency_id',
                            xtype:'combo',
                            fieldLabel: 'Currency',
                            name: 'currency_id',
                            anchor:'95%',
                            allowBlank: true,
                            store: this.CurrencyDS,
                            typeAhead: false,
                            width: 100,
                            displayField: 'display',
                            valueField: 'currency_id',
                            value : 'IDR',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form", t:"SP",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    // 'select' : function (combo, record, indexVal)
                                    // {   Ext.getCmp('pof_supplier_id').setValue(record.data.company_id);
                                    // },
                            }
                        },
                        {   id: 'pof_po_status_name',
                            xtype: 'combo',
                            fieldLabel: 'Status',
                            name: 'po_status_name',
                            store : new Ext.data.SimpleStore(
                            {   fields: ['status'],
                                data : [ ['Draft'], ['Final']]
                            }),
                            displayField:'status',
                            valueField :'status',
                            value : 'Draft',
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            width:100,
                        },
                        ]
                    }, 
                    {   columnWidth:.7, 
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'pof_company_name', 
                            xtype:'textfield',
                            fieldLabel: 'Company.Name',
                            name: 'company_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'pof_supplier_name',
                            xtype:'combo',
                            fieldLabel: 'Supplier',
                            name: 'supplier_name',
                            anchor:'95%',
                            allowBlank: true,
                            store: this.SupplierDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'name',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form", t:"SP",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('pof_supplier_id').setValue(record.data.company_id);
                                    },
                            }
                        },
                        {   id : 'pof_to_dept_name',
                            xtype:'combo',
                            fieldLabel: 'Dept.Name',
                            name: 'to_dept_name',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.DeptDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'display',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form", 
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('pof_to_site_id').setValue(record.data.site_id);
                                        Ext.getCmp('pof_to_dept_id').setValue(record.data.dept_id);
                                    },
                            }
                        },
                        {   id : 'pof_payment_term', 
                            xtype:'combo',
                            fieldLabel: 'Payment Term',
                            name: 'payment_term',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.PaymentTermDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'description',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('pof_payment_term_id').setValue(record.data.payment_term_id);
                                    },
                            }
                        }
                        ]
                    }]
                },  // description area
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.9,
                        layout: 'form',
                        border:false,
                        items: [ 
                        {   id : 'pof_description', 
                            xtype:'textarea',
                            fieldLabel: 'Description',
                            name: 'description',
                            anchor:'95%',
                            // allowBlank: false,
                            height : 100,
                        },
                        ]
                    }]
                }]
            });
            /************************************
                D E T A I L 
            ************************************/
            this.Columns = [
                {   header: "PR.No", width : 100,
                    dataIndex : 'pr_no', sortable: true,
                    tooltip:"PR No",
                },
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Items",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.item_id+' ';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "PR.Qty", width : 100,
                    dataIndex : 'pr_qty', sortable: true,
                    tooltip:"PR Quantity",
                },
                {   header: "Quantity", width : 100,
                    dataIndex : 'quantity_primary', sortable: true,
                    tooltip:"Quantity",
                    css : "background-color: lime;",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( parseInt(record.data.quantity_primary) > parseInt(record.data.pr_qty))
                        {   metaData.attr = "style = background-color:yellow;";  }
                        else {   metaData.attr = "style = background-color:lime;"; };
                        return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "UM", width : 100,
                    dataIndex : 'um_primary', sortable: true,
                    tooltip:"Unit Measurement",
                },
                {   header: "Price", width : 100,
                    dataIndex : 'unit_price', sortable: true,
                    tooltip:"Prices",
                    css : "background-color: lime;",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Amount", width : 100,
                    dataIndex : 'amount', sortable: true,
                    tooltip:"Amount",
                },
                {   header: "Remark", width : 300,
                    dataIndex : 'remark', sortable: true,
                    tooltip:"Item Remark",
                    css : "background-color: lime;",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
            ];
            this.DataStore = alfalah.core.newDataStore(
                '/purchase/2/5', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : 200, //alfalah.country.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   text:'Recalculate',
                        tooltip:'Recalculate Record',
                        iconCls: 'silk-calculator',
                        handler : this.Grid_recalculate,
                        scope : this
                    }
                ],
                listeners :
                {   "afteredit" : this.Grid_afteredit },
                scope : this
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
            /************************************
                S U M M A R Y 
            ************************************/
            this.summaryColumns = [
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Items",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.item_id+' ';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Quantity", width : 100,
                    dataIndex : 'quantity_primary', sortable: true,
                    tooltip:"Quantity",
                },
                {   header: "UM", width : 100,
                    dataIndex : 'um_primary', sortable: true,
                    tooltip:"Unit Measurement",
                },
                {   header: "Price", width : 100,
                    dataIndex : 'unit_price', sortable: true,
                    tooltip:"Prices",
                },
                {   header: "Amount", width : 100,
                    dataIndex : 'amount', sortable: true,
                    tooltip:"Amount",
                },
            ];
            this.summaryRecords = Ext.data.Record.create(
            [   {name: 'item_id', type: 'string'},
                {name: 'item_name', type: 'string'},
                {name: 'quantity_primary', type: 'string'},
                {name: 'um_primary', type: 'string'},
                {name: 'unit_price', type: 'string'},
                {name: 'amount', type: 'string'},
            ]);
            this.summaryDataStore = alfalah.core.newDataStore(
                '/purchase/2/6', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.summaryGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.summaryDataStore,
                columns: this.summaryColumns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                // enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : 200,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    '-',
                    {   text:'Save PO',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        scope : this
                    }, '-',
                    '_T O T A L_',
                    new Ext.form.TextField(
                    {   id : 'pof_total_amount',
                        allowBlank : false,
                        readOnly : true,
                        style: 'text-align: right',
                        value : 0,
                    }),
                    '-',
                    '__T O T A L__P O__A M O U N T_',
                    new Ext.form.TextField(
                    {   id : 'pof_po_amount',
                        allowBlank : false,
                        readOnly : true,
                        style: 'text-align: right',
                        value : 0,
                    })
                ],
                // bbar : ['-'],
                scope : this
            });
            this.chargesColumns = [
                {   header: "Name", width : 200,
                    dataIndex : 'charges_name', sortable: true,
                    tooltip:"Charges Name",
                },
                {   header: "Percentage", width : 80,
                    dataIndex : 'pct_amount', sortable: true,
                    tooltip:"Percentage",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Operation", width : 50,
                    dataIndex : 'plus_minus', sortable: true,
                    tooltip:"Plus Minus Operation",
                    editor :  new Ext.Editor(  new Ext.form.ComboBox(
                                                {   store: new Ext.data.SimpleStore({
                                                                       fields: ["label"],
                                                                       data : [["+Plus"], ["-Minus"]]
                                                                }),
                                                    displayField:"label",
                                                    valueField:"label",
                                                    mode: 'local',
                                                    typeAhead: true,
                                                    triggerAction: "all",
                                                    selectOnFocus:true,
                                                    forceSelection :true
                                               }),
                                                {autoSize:true}
                                            ),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == "-Minus" )
                        {   metaData.attr = "style = background-color:red;";  };
                        return value;
                    }
                },
                
                {   header: "Amount", width : 100,
                    dataIndex : 'total_amount', sortable: true,
                    tooltip:"Charges Amount",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Description", width : 200,
                    dataIndex : 'description', sortable: true,
                    tooltip:"Description",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
            ];
            this.chargesRecords = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'po_no', type: 'string'},
                {name: 'charges_id', type: 'string'},
                {name: 'percentage', type: 'string'},
                {name: 'plus_minus', type: 'string'},
                {name: 'amount', type: 'string'},
                {name: 'description', type: 'string'},
            ]);
            this.chargesDataStore = alfalah.core.newDataStore(
                '/purchase/2/6', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.chargesDataStore.load();
            this.region_height = alfalah.purchase.po.centerPanel.container.dom.clientHeight;
            this.chargesGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.chargesDataStore,
                columns: this.chargesColumns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                listeners :
                {   "beforeedit" : this.chargesGrid_beforeedit,
                    "afteredit" : this.chargesGrid_afteredit 
                },
                scope : this,
            });
            this.summaryTab = new Ext.Panel(
            {   title:  "S U M M A R Y",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.summaryGrid]
                },
                {   region: 'south',
                    title: 'C H A R G E S',
                    split: true,
                    height : this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    layout: 'border',
                    items:[
                    {   region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.chargesGrid]
                    }]
                }]
            });
            /************************************
                SEARCH ITEM TAB
            ************************************/
            this.pritemsColumns = [
                {   header: "Order.No", width : 100,
                    dataIndex : 'order_no', sortable: true,
                    tooltip:"Order No",
                },
                {   header: "PR.No", width : 100,
                    dataIndex : 'pr_no', sortable: true,
                    tooltip:"PR No",
                },
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Items",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.item_id+' ';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "PR.Qty", width : 100,
                    dataIndex : 'quantity_primary', sortable: true,
                    tooltip:"PR Quantity",
                },
                {   header: "PO.Qty", width : 100,
                    dataIndex : 'po_qty', sortable: true,
                    tooltip:"PO Quantity",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( parseInt(record.data.po_qty) > parseInt(record.data.quantity_primary))
                        {   metaData.attr = "style = background-color:red;";  }
                        else {   metaData.attr = "style = background-color:lime;"; };
                        return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "UM", width : 100,
                    dataIndex : 'um_primary', sortable: true,
                    tooltip:"Unit Measurement",
                },
                {   header: "Remark", width : 200,
                    dataIndex : 'remark', sortable: true,
                    tooltip:"Item Remark",
                },
            ];
            this.pritemsDataStore = alfalah.core.newDataStore(
                '/purchase/1/10', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.pritemsSearchs = [
                {   id: 'pof2_order_no',
                    cid: 'order_no',
                    fieldLabel: 'Order.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pof2_pr_no',
                    cid: 'pr_no',
                    fieldLabel: 'PR.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pof2_is_complete',
                    cid: 'is_complete',
                    fieldLabel: 'Complete',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['YES'], ['NO']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'NO'
                },
                {   id: 'pof2_pr_type',
                    cid: 'pr_type',
                    fieldLabel: 'Type',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['GPR'], ['OPR']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'ALL'
                },
            ];
            this.pritemsGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.pritemsDataStore,
                columns: this.pritemsColumns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : 200, //alfalah.country.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add Items',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add_items,
                        scope : this
                    },
                ],
                scope : this
            });
            this.pritemsTab = new Ext.Panel(
            {   title:  "F I N D  -  I T E M S",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 50,
                                defaultType: 'textfield',
                                items : this.pritemsSearchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   //id : 'search_btn_pritems',
                                    text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.pritems_search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.pritemsGrid]
                }],
            });
        },
        // build the layout
        build_layout: function()
        {   if (Ext.isObject(this.Records))
            {   the_title = "Edit PO"; }
            else { the_title = "New PO"; };

            this.Tab = new Ext.Panel(
            {   id : "po_form",
                jsId : "po_form",
                // title:  the_title,
                title : '<span style="background-color: yellow;">'+the_title+'</span>',
                region: 'center',
                iconCls: 'silk-grid',
                layout: 'border',
                closable : true,
                // autoScroll  : true,
                deferredRender : false,                                     
                items: [
                {   region: 'center',     
                    xtype: 'tabpanel',
                    activeTab: 0,
                    items:[this.Form, 
                        this.detailTab, 
                        // this.ChargesTab,
                        this.summaryTab, 
                        this.pritemsTab
                    ]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   if (Ext.isObject(this.Records))
            {   Ext.each(this.Records.fields.items,
                    function(the_field)
                    {   switch (the_field.name)
                        {   case "id" :
                            case "created_date" :
                            case "company_id" :
                            case "company_name" :
                            case "po_date" :
                            case "po_status" :
                            case "po_status_name" :
                            case "to_site_id" :
                            case "to_dept_id" :
                            case "to_dept_name" :
                            case "description" :
                            case "supplier_id" :
                            case "supplier_name" :
                            case "payment_term" :
                            case "payment_term_id" :
                            {   //console.log('pof_'+the_field.name);
                                Ext.getCmp('pof_'+the_field.name).setValue(this.Records.data[the_field.name]);
                            };
                            break;
                            case "po_no" :
                            {   Ext.getCmp('pof_'+the_field.name).setValue(this.Records.data[the_field.name]);
                                this.DataStore.baseParams = {   
                                    s:"form", po_no: this.Records.data["po_no"],
                                    limit:this.page_limit, start:this.page_start};
                                this.DataStore.reload();
                            };
                            break;
                        };
                    }, this);
            };
            Ext.getCmp('pof_total_amount').setValue(1234);
            this.Records = Ext.data.Record.create(
            [   {name: 'po_no', type: 'string'},
                {name: 'pr_no', type: 'string'},
                {name: 'item_id', type: 'string'},
                {name: 'item_name', type: 'string'},
                {name: 'quantity_primary', type: 'string'},
                {name: 'um_primary', type: 'string'},
                {name: 'unit_price', type: 'string'},
                {name: 'amount', type: 'string'},
                {name: 'remark', type: 'string'},
                {name: 'created_date', type: 'date'}
            ]);
        },
        // pritems search button
        pritems_search_handler : function(button, event)
        {   the_parameter = alfalah.core.getSearchParameter(this.pritemsSearchs);
            this.pritemsDataStore.removeAll();
            this.pritemsDataStore.baseParams = Ext.apply( the_parameter,
            {   s:"form",
                limit:this.page_limit, start:this.page_start });
            this.pritemsDataStore.reload();
        },
        Grid_afteredit : function(the_cell)
        {   switch (the_cell.field)
            {   case "quantity_primary":
                case "unit_price":
                {   var data = the_cell.record.data;
                    var value = parseInt(the_cell.value);
                    var unit_price = parseFloat(data.unit_price);
                    var pr_qty = parseInt(data.pr_qty);
                    var quantity_primary = parseInt(data.quantity_primary);
                    var is_calculate = true;
                    if (quantity_primary > pr_qty)
                    {   Ext.Msg.show(
                        {   title:'W A R N I N G ',
                            msg: ' PO.Quantity is higher than PR.Quantity <BR>Do you want to process it ? ',
                            buttons: Ext.Msg.YESNO,
                            fn: function(buttonId, text)
                                {   if (buttonId =='yes')
                                    {   is_calculate = true; } 
                                    else 
                                    {   is_calculate = false;
                                        data.quantity_primary = pr_qty;
                                        the_cell.grid.getView().refresh();
                                    };
                                },
                            icon: Ext.MessageBox.WARNING
                        });
                    };

                    if (is_calculate == true)
                    {   data.amount = unit_price * quantity_primary;
                        the_cell.grid.getView().refresh();
                        alfalah.purchase.po.forms.isRecalculate = false;
                    };
                };
                break;
            };
        },
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.Grid.getSelectionModel().selection;
            // b = alfalah.purchase.po.forms.DataStore.removeAt(a.cell[0])
            if ( the_record )
            {   if (Ext.isEmpty(the_record.record.data.po_no))
                {   this.Grid.getStore().removeAt(the_record.cell[0]);  }
                else
                {   the_record.record.set("remark", "REMOVED"); };
                this.isRecalculate = false;
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_add_items: function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.pritemsGrid.getSelectionModel().selection;
            if ( the_record )
            {   record = the_record.record.data;
                var pr_qty = parseInt(record.quantity_primary) - parseInt(record.po_qty);
                if (pr_qty < 0) { pr_qty = 0; };
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   po_no : "",
                        pr_no : record.pr_no,
                        item_id: record.item_id,
                        item_name: record.item_name,
                        pr_qty : pr_qty,
                        quantity_primary : pr_qty,
                        um_primary : record.um_primary,
                        unit_price : 0,
                        amount : 0,
                        remark : "",
                        created_date: "",
                    }));
                this.isRecalculate = false;
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_recalculate: function(button, event)
        {   this.Grid.stopEditing();
            this.summaryDataStore.removeAll();
            var the_records = this.DataStore.data.items;
            var json_data = [];
            var v_json = {};
            var v_data = {};
            var is_validate = true;
            Ext.each(the_records,
                function(the_record)
                {   the_data = the_record.data;
                    if (json_data.length == 0)
                    {   json_data.push({
                            item_id: the_data.item_id,
                            item_name: the_data.item_name,
                            quantity_primary : parseInt(the_data.quantity_primary),
                            um_primary : the_data.um_primary,
                            unit_price : parseFloat(the_data.unit_price),
                            amount : parseFloat(the_data.amount),
                        });
                    }
                    else
                    {   is_new_item = true;
                        Ext.each(json_data,
                            function(v_json)
                            {   if (v_json.item_id == the_data.item_id)
                                {   is_new_item = false;    };
                                
                            });
                        if (is_new_item == true)
                        {   //  new item
                            json_data.push({
                                item_id: the_data.item_id,
                                item_name: the_data.item_name,
                                quantity_primary : parseInt(the_data.quantity_primary),
                                um_primary : the_data.um_primary,
                                unit_price : parseFloat(the_data.unit_price),
                                amount : parseFloat(the_data.amount),
                        });
                        }
                        else
                        {   //  duplicate 
                            v_json.quantity_primary = v_json.quantity_primary + parseInt(the_data.quantity_primary);
                            v_json.amount = v_json.amount + parseFloat(the_data.amount);
                            if ( v_json.unit_price !== parseFloat(the_data.unit_price) )
                            {   is_validate = false;
                                Ext.Msg.show(
                                {   title:'E R R O R ',
                                    msg: 'Different Unit Price Found on Item '+v_json.item_name,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                            };
                        };
                    };
                });
            the_summaryGrid = this.summaryGrid;
            the_summaryRecords = this.summaryRecords;
            v_total_amount = 0;
            if (json_data.length > 0)
            {   this.summaryDataStore.removeAll();
                Ext.each(json_data,
                    function(v_json)
                    {   v_total_amount = v_total_amount + v_json.amount;
                        the_summaryGrid.store.insert( 0, new the_summaryRecords(v_json));
                    });
                this.isRecalculate = true;
            };
            Ext.getCmp('pof_total_amount').setValue(v_total_amount.toFixed(2));
            this.chargesGrid_recalculate();
        },
        chargesGrid_beforeedit : function(the_cell)
        {   summaryDataStore = alfalah.purchase.po.forms.summaryDataStore;
            if (summaryDataStore.data.length == 0)
            {   Ext.Msg.show(
                {   title:'W A R N I N G ',
                    msg: 'Need Recalculate Process',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });
                return false;
            }
            else return true;
        },
        chargesGrid_afteredit : function(the_cell)
        {   v_percentage = 0;
            v_pct_amount = 0;
            v_total_amount = parseFloat(Ext.getCmp('pof_total_amount').getValue());
            if (v_total_amount == 0)
            {   Ext.Msg.show(
                {   title:'E R R O R ',
                    msg: 'Total Amount is zero.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
            else 
            {   switch (the_cell.field)
                {   case "pct_amount"   :
                    {   v_percentage = the_cell.value;
                        v_pct_amount = v_total_amount * ( parseFloat(v_percentage) /100);
                        the_cell.record.data.total_amount = v_pct_amount.toFixed(2);
                        console.log(v_pct_amount);
                    };
                    break;
                    case "total_amount" :
                    {   v_pct_amount = the_cell.value;
                        v_percentage = (v_pct_amount / v_total_amount) * 100;    
                        the_cell.record.data.pct_amount = v_percentage.toFixed(2);
                        console.log(v_percentage);
                    };
                    break;
                };
                alfalah.purchase.po.forms.chargesGrid_recalculate();
                the_cell.grid.getView().refresh();  
            };
        },
        chargesGrid_recalculate: function()
        {   this.chargesGrid.stopEditing();
            the_charges = this.chargesGrid.store.data.items;
            v_po_amount = parseFloat(Ext.getCmp('pof_total_amount').getValue());
            Ext.each(the_charges,
                function(the_charge)
                {   switch (the_charge.data.plus_minus)
                    {   case "+" :
                        case "+Plus" :
                        {   v_po_amount = v_po_amount + parseFloat(the_charge.data.total_amount);   };
                        break;
                        case "-" :
                        case "-Minus" :
                        {   v_po_amount = v_po_amount - parseFloat(the_charge.data.total_amount);   };
                        break;
                    };   
                });
            Ext.getCmp('pof_po_amount').setValue(v_po_amount.toFixed(2));
        },
        Form_save : function(button, event)
        {   var head_data = [];
            var json_data = [];
            var modi_data = [];
            var v_json = {};

            if (this.isRecalculate == false)
            {   
                Ext.Msg.show(
                {   title:'E R R O R ',
                    msg: 'Need Recalculate Process',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERRROR
                }); 
            }
            else
            {   if (alfalah.core.validateFields([
                    'pof_po_status_name', 'pof_supplier_name', 'pof_to_dept_name']))
                {   // detail validation
                    var item_count = this.DataStore.getCount()
                    if ( item_count < 1 ) 
                    {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'Please register minimum 1 Item.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO,
                            width : 300
                        });
                    }
                    else if ( alfalah.core.validateGridQuantity(
                                this.DataStore.getModifiedRecords(),
                                ['quantity_primary', 'unit_price', 'amount']))
                    {   // header
                        head_data = alfalah.core.getHeadData([
                            'pof_id', 'pof_company_id', 'pof_company_name', 
                            'pof_po_no', 'pof_po_date', 'pof_currency_id', 
                            'pof_to_site_id', 'pof_to_dept_id', 'pof_to_dept_name', 
                            'pof_supplier_id', 'pof_supplier_name', 
                            'pof_description', 'pof_po_status_name',
                            'pof_payment_term', 'pof_payment_term_id',
                            'pof_created_date']);
                        // detail
                        json_data = alfalah.core.getDetailData(this.DataStore.getModifiedRecords());
                        // charges
                        chg_data = alfalah.core.getDetailData(this.chargesDataStore.getModifiedRecords());
                        // submit data
                        alfalah.core.submitForm(
                            'po_form', 
                            alfalah.purchase.po.po.DataStore,
                            '/purchase/2/1',
                            {   'x-csrf-token': alfalah.purchase.po.sid },
                            {   task: 'save',
                                head : Ext.encode(head_data),
                                json : Ext.encode(json_data), 
                                chg  : Ext.encode(chg_data),
                            });
                    };
                };
            };
            
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.purchase.po.initialize, alfalah.purchase.po);
// end of file
</script>
<div>&nbsp;</div>