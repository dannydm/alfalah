<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.purchase.invoice');

// create application
alfalah.purchase.invoice = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.invref.initialize();
            this.invoice.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.invref.Tab);
            this.centerPanel.add(this.invoice.Tab);
            this.centerPanel.setActiveTab(this.invref.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.purchase.invoice.invref = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            // this.WarehouseCDS = alfalah.core.newDataStore(
            //     '/company/9/9', false,
            //     {   s:"init", limit:this.page_limit, start:this.page_start,
            //         t:"COCO"
            //     }
            // ); 
            // this.WarehouseCDS.load();
            // this.SiteDeptCDS = alfalah.core.newDataStore(
            //     '/company/6/9', false,
            //     {   s:"init", limit:this.page_limit, start:this.page_start }
            // );
            // this.SiteDeptCDS.load();
            this.Columns = [
                {   header: "Company", width : 100,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"Company ID",
                    hidden : true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.company_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "PO.No", width : 150,
                    dataIndex : 'po_no', sortable: true,
                    tooltip:"Purchase Order No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.po_date;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Supplier", width : 200,
                    dataIndex : 'supplier_name', sortable: true,
                    tooltip:"Supplier Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.supplier_id;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Item Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.item_id;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "PO.Qty", width : 100,
                    dataIndex : 'po_qty', sortable: true,
                    tooltip:"PO Quantity",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == 0 )
                        {   metaData.attr = "style = background-color:red;";  };
                        result = value+'<br>'+record.data.um_primary;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "QC.Pass.Qty", width : 100,
                    dataIndex : 'qc_qty', sortable: true,
                    tooltip:"QC Pass Quantity",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == 0 )
                        {   metaData.attr = "style = background-color:red;";  };
                        result = value+'<br>'+record.data.um_primary;
                        return alfalah.core.gridColumnWrap(result);
                    }
                }
            ];
            this.Searchs = [
                {   id: 'invref_po_no',
                    cid: 'po_no',
                    fieldLabel: 'PO.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'invref_supplier_name',
                    cid: 'supplier_name',
                    fieldLabel: 'Buyer',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'invref_item_name',
                    cid: 'item_name',
                    fieldLabel: 'Buyer',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.DataStore = alfalah.core.newDataStore(
                '/purchase/3/11', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.purchase.invoice.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                //inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Invoice',
                        tooltip:'New invoice Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    // {   text:'Department',
                    //     tooltip:'New invoice Record',
                    //     iconCls: 'silk-add',
                    //     handler : this.Grid_add,
                    //     scope : this
                    // },
                    '-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_invrefGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_invrefPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_invrefPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_invrefGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    //this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_poSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_invrefTab",
                jsId : tabId+"_invrefTab",
                title:  "Search.New.invoice",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 220,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 70,
                                defaultType: 'textfield',
                                items : this.Searchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   //id : 'search_btn_pritems',
                                    text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        Grid_add : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            var centerPanel = Ext.getCmp('center_panel');
            console.log(the_record);
            alfalah.purchase.invoice.forms.mode_view = false;
            alfalah.purchase.invoice.forms.initialize(null, {
                company_id     : '',
                company_name   : '',
                ref_no         : '',
                ref_date       : '',
                party_id       : '',
                party_name     : '',
                item_id        : '',
                currency_id    : '',
                // remain_qty     : the_record.record.data.remain_qty,
                type           : button.type
            });
            centerPanel.beginUpdate();
            centerPanel.add(alfalah.purchase.invoice.forms.Tab);
            centerPanel.setActiveTab(alfalah.purchase.invoice.forms.Tab);
            centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();


            // if ( the_record )
            // {   var the_warehouse = Ext.getCmp('invref_warehouse_name').getValue();
            //     if ( the_warehouse )
            //     {   var po_inv_form = Ext.getCmp("po_inv_form");
            //         if (po_inv_form)
            //         {   Ext.Msg.show(
            //             {   title :'E R R O R ',
            //                 msg : 'invoice Form Available',
            //                 buttons: Ext.Msg.OK,
            //                 icon: Ext.MessageBox.ERROR
            //             });
            //         }
            //         else
            //         {   if (the_record.record.data.remain_qty == 0)
            //             {   Ext.Msg.show(
            //                 {   title :'W A R N I N G ',
            //                     msg : 'Remain Quantity is not available',
            //                     buttons: Ext.Msg.OK,
            //                     icon: Ext.MessageBox.WARNING
            //                 });
            //             };

            //             if (the_record.record.data.stock_qty == 0)
            //             {   Ext.Msg.show(
            //                 {   title :'W A R N I N G ',
            //                     msg : 'Stock Quantity is not available',
            //                     buttons: Ext.Msg.OK,
            //                     icon: Ext.MessageBox.WARNING
            //                 });
            //             };

                        
            //         };
            //     }
            //     else
            //     {   Ext.Msg.show(
            //         {   title:'E R R O R ',
            //             msg: 'No Warehouse Selected ! ',
            //             buttons: Ext.Msg.OK,
            //             icon: Ext.MessageBox.ERROR
            //         });

            //     };
            // }
            // else
            // {   Ext.Msg.show(
            //     {   title:'E R R O R ',
            //         msg: 'No Data Selected ! ',
            //         buttons: Ext.Msg.OK,
            //         icon: Ext.MessageBox.ERROR
            //     });
            // };
        },
        //  search button
        search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_invrefSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.purchase.invoice.invoice = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.WarehouseCDS = alfalah.core.newDataStore(
                '/company/9/9', false,
                {   s:"init", limit:this.page_limit, start:this.page_start,
                    t:"COCO"
                }
            ); 
            this.WarehouseCDS.load();
            this.Columns = [
                {   header: "Company", width : 150,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"Company ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.company_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Invoice.No", width : 100,
                    dataIndex : 'invoice_no', sortable: true,
                    tooltip:"Invoice No",
                },
                {   header: "Receive.Date", width : 100,
                    dataIndex : 'receive_date', sortable: true,
                    tooltip:"Receive Date",
                },
                {   header: "PO", width : 100,
                    dataIndex : 'ref_no', sortable: true,
                    tooltip:"Order Reference No",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.ref_type;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Suppliers", width : 200,
                    dataIndex : 'party_name', sortable: true,
                    tooltip:"Buyer Name",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.party_id;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Invoice Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:" Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Searchs = [
                {   id: 'inv_invoice_no',
                    cid: 'invoice_no',
                    fieldLabel: 'Invoice.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'inv_ref_no',
                    cid: 'ref_no',
                    fieldLabel: 'Ref.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'inv_party_id',
                    cid: 'party_id',
                    fieldLabel: 'Party.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'inv_party_name',
                    cid: 'party_name',
                    fieldLabel: 'Party.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'inv_company_id',
                    cid: 'company_id',
                    fieldLabel: 'Company',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120,
                    hidden : true
                },
                {   id: 'inv_site_id',
                    cid: 'site_id',
                    fieldLabel: 'Site',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120,
                    hidden : true
                },
                {   id: 'inv_warehouse_id',
                    cid: 'warehouse_id',
                    fieldLabel: 'Warehouse.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120,
                    hidden : true
                },
                new Ext.form.ComboBox(
                    {   store: this.WarehouseCDS,
                        typeAhead: true,
                        id: 'inv_warehouse_name',
                        cid: 'warehouse_name',
                        width: 120,
                        fieldLabel: 'Warehouse',
                        displayField: 'name',
                        valueField: 'name',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        listeners : { scope : this,
                            'select' : function (a,records,c)
                            {   Ext.getCmp("inv_company_id").setValue(records.data.company_id);
                                Ext.getCmp("inv_site_id").setValue(records.data.site_id);
                                Ext.getCmp("inv_warehouse_id").setValue(records.data.warehouse_id);
                            }
                        }
                    }),
                
            ];
            this.DataStore = alfalah.core.newDataStore(
                '/purchase/3/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.purchase.invoice.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'View',
                        tooltip:'View invoiceOrder',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_view,
                        scope : this
                    },
                    '-',
                    // {   text:'Delete',
                    //     tooltip:'Delete Record',
                    //     iconCls: 'silk-delete',
                    //     handler : this.Grid_remove,
                    //     scope : this
                    // },
                    // '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : this.print_out,
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_invoiceGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_invoicePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_invoicePageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_invoiceGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    //this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_invoiceSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_invoiceTab",
                jsId : tabId+"_invoiceTab",
                title:  "Purchase Invoice",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 220,
                    minSize: 220,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 70,
                                defaultType: 'textfield',
                                items : this.Searchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.invoice_search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        Grid_view : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   centerPanel = Ext.getCmp('center-panel');
                var po_inv_form = Ext.getCmp("po_inv_form");
                if (po_inv_form)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Invoice Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    // alfalah.purchase.invoice.forms.initialize(the_record.record);
                    alfalah.purchase.invoice.forms.mode_view = true;
                    alfalah.purchase.invoice.forms.initialize(the_record.record, {});
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.purchase.invoice.forms.Tab);
                    centerPanel.setActiveTab(alfalah.purchase.invoice.forms.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Data Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            };
        },
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            this.DataStore.removeAll();
            alfalah.core.submitGrid(
                this.DataStore, 
                '/purchase/3/2',
                {   'x-csrf-token': alfalah.purchase.invoice.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.id }
            );
        },
        //  search button
        invoice_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_invoiceSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
        print_out: function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                alfalah.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        id : the_record.id });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };   
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.purchase.invoice.forms = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;
    this.Headers;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit  : 75,
        page_start  : 0,
        mode_view   : false,
        seq_no      : 1,
        // public methods
        initialize: function(the_record, the_header)
        {   this.Records = the_record;
            this.Headers = the_header;
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.CurrencyDS = alfalah.core.newDataStore(
                '/country/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.PartyDS = alfalah.core.newDataStore(
                '/company/2/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.WarehouseDS = alfalah.core.newDataStore(
                '/company/9/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.ItemDS = alfalah.core.newDataStore(
                '/purchase/3/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   //id : 'out_form',
                title : 'H E A D E R',
                bodyStyle:'padding:5px',
                // autoScroll : true,
                // autoHeight : true,
                items: [
                {   layout:'column',
                    border:false,
                    items:[ // hidden columns
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'invfrm_id',
                            xtype:'textfield',
                            fieldLabel: 'ID',
                            name: 'id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }, 
                        {   id : 'invfrm_ref_type',
                            xtype:'textfield',
                            fieldLabel: 'Ref.Type',
                            name: 'ref_type',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'invfrm_company_id',
                            xtype:'textfield',
                            fieldLabel: 'Company.ID',
                            name: 'company_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'invfrm_party_id',
                            xtype:'textfield',
                            fieldLabel: 'Party.ID',
                            name: 'party_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'invfrm_invoice_status',
                            xtype:'textfield',
                            fieldLabel: 'Invoice.Status',
                            name: 'invoice_status',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'invfrm_site_id',  
                            xtype:'textfield',
                            fieldLabel: 'Site.ID',
                            name: 'site_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }, 
                        {   id : 'invfrm_warehouse_id',
                            xtype:'textfield',
                            fieldLabel: 'Warehouse.ID',
                            name: 'warehouse_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'invfrm_item_id',
                            xtype:'textfield',
                            fieldLabel: 'Item.ID',
                            name: 'item_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'invfrm_remain_qty',
                            xtype:'textfield',
                            fieldLabel: 'Remain.Qty',
                            name: 'remain_qty',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'invfrm_created_date',  
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            hidden : true,
                        }]
                    }, 
                    {   columnWidth:.4,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'invfrm_company_name', 
                            xtype:'textfield',
                            fieldLabel: 'Company.Name',
                            name: 'company_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'invfrm_invoice_no', 
                            xtype:'textfield',
                            fieldLabel: 'Invoice.No',
                            name: 'invoice_no',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly: true,
                        },
                        {   id : 'invfrm_receive_date', 
                            xtype:'datefield',
                            fieldLabel: 'Receive.Date',
                            name: 'receive_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            value : new Date(),
                            // readOnly : true,
                        },
                        {   id : 'invfrm_due_date', 
                            xtype:'datefield',
                            fieldLabel: 'Due.Date',
                            name: 'due_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            value : new Date(),
                            // readOnly : true,
                        },
                        {   id : 'invfrm_currency_id',
                            xtype:'combo',
                            fieldLabel: 'Currency',
                            name: 'currency_id',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.CurrencyDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'currency_id',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                        },
                        {   id : 'invfrm_xrate_value', 
                            xtype:'textfield',
                            fieldLabel: 'Xrate',
                            name: 'xrate_value',
                            anchor:'95%'
                        },
                        
                        ]
                    }, 
                    {   columnWidth:.6, 
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'invfrm_party_name',
                            xtype:'combo',
                            fieldLabel: 'Party',
                            name: 'party_name',
                            anchor:'95%',
                            allowBlank: true,
                            store: this.PartyDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'name',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form", t:"SP",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('invfrm_party_id').setValue(record.data.company_id);
                                    },
                            }
                        },
                        {   id : 'invfrm_party_inv_no', 
                            xtype:'textfield',
                            fieldLabel: 'Party.Inv.No',
                            name: 'party_inv_no',
                            anchor:'95%',
                            allowBlank: false,
                        },
                        {   id : 'invfrm_party_inv_date', 
                            xtype:'datefield',
                            fieldLabel: 'Party.Inv.Date',
                            name: 'party_inv_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            value : new Date(),
                            // readOnly : true,
                        },
                        
                        {   id : 'invfrm_ref_no', 
                            xtype:'textfield',
                            fieldLabel: 'REF.No',
                            name: 'ref_no',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                        },
                        {   id : 'invfrm_ref_date', 
                            xtype:'datefield',
                            fieldLabel: 'REF.Date',
                            name: 'ref_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            readOnly : true,
                        },
                        {   id : 'invfrm_ref_currency', 
                            xtype:'textfield',
                            fieldLabel: 'REF.Currency',
                            name: 'ref_currency',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        }
                        ]
                    }]  
                },  // description area
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.9,
                        layout: 'form',
                        border:false,
                        items: [ 
                        {   id : 'invfrm_description', 
                            xtype:'textarea',
                            fieldLabel: 'Description',
                            name: 'description',
                            anchor:'95%',
                            // allowBlank: false,
                            height : 100,
                        }]
                    }]
                }]
            });
            /************************************
                D E T A I L
            ************************************/
            this.Columns = [
                {   header: "REF.No", width : 150,
                    dataIndex : 'ref_no', sortable: true,
                    tooltip:"Reference Document",
                },
                
                {   header: "GRIN.No", width : 100,
                    dataIndex : 'grin_no', sortable: true,
                    tooltip:"GRIN Document",
                },
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Items",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.item_id+' ';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "QC", width : 80,
                    dataIndex : 'quantity_primary', sortable: true,
                    tooltip:"Quantity Primary",
                },
                {   header: "Remark", width : 300,
                    dataIndex : 'remark', sortable: true,
                    tooltip:"Item Remark",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
            ];
            if (Ext.isObject(this.Records))
            {   the_title = "View PO Invoice"; 
                the_save = '-';
                // load invoice detail
                this.DataStore = alfalah.core.newDataStore(
                    '/purchase/3/5', false,
                    {   s:"form", invoice_no : this.Records.data.invoice_no,
                        limit:this.page_limit, start:this.page_start }
                );
                this.DataStore.load();    
            }
            else
            {   the_title = "New PO Invoice"; 
                the_save = {   
                    text:'Save invoice',
                    tooltip:'Save Record',
                    iconCls: 'icon-save',
                    handler : this.Form_save,
                    scope : this
                };
                // load reference detail
                this.DataStore = alfalah.core.newDataStore(
                    '/purchase/3/12', false,
                    {   s:"form", ref_no: this.Headers.ref_no, qc:"YES",
                        limit:this.page_limit, start:this.page_start }
                );
                this.DataStore.load();
            };
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : 200, //alfalah.country.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   text:'Recalculate',
                        tooltip:'Recalculate Record',
                        iconCls: 'silk-calculator',
                        handler : this.Grid_recalculate,
                        scope : this
                    }
                ],
                listeners :
                {   //"beforeedit" : this.Grid_beforeedit,
                    "afteredit" : this.Grid_afteredit
                }
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
            /************************************
                S U M M A R Y 
            ************************************/
            this.summaryColumns = [
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Items",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.item_id+' ';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Quantity", width : 100,
                    dataIndex : 'quantity_primary', sortable: true,
                    tooltip:"Quantity",
                },
                {   header: "UM", width : 100,
                    dataIndex : 'um_primary', sortable: true,
                    tooltip:"Unit Measurement",
                },
                {   header: "Currency", width : 50,
                    dataIndex : 'currency_id', sortable: true,
                    tooltip:"Currency",
                },
                {   header: "Price", width : 100,
                    dataIndex : 'unit_price', sortable: true,
                    tooltip:"Price",
                },
                
            ];
            this.summaryRecords = Ext.data.Record.create(
            [   {name: 'item_id', type: 'string'},
                {name: 'item_name', type: 'string'},
                {name: 'quantity_primary', type: 'string'},
                {name: 'um_primary', type: 'string'},
            ]);
            this.summaryDataStore = alfalah.core.newDataStore(
                '/purchase/3/6', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.summaryGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.summaryDataStore,
                columns: this.summaryColumns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : 200, //alfalah.country.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    the_save,
                    '-',
                    '_T O T A L_',
                    new Ext.form.TextField(
                    {   id : 'invfrm_total_amount',
                        allowBlank : false,
                        readOnly : true,
                        style: 'text-align: right',
                        value : 0,
                    }),
                    '-',
                    '__T O T A L__INVOICE__A M O U N T_',
                    new Ext.form.TextField(
                    {   id : 'invfrm_invoice_amount',
                        allowBlank : false,
                        readOnly : true,
                        style: 'text-align: right',
                        value : 0,
                    }) 
                ],
                scope : this
            });
            this.chargesColumns = [
                {   header: "Name", width : 200,
                    dataIndex : 'charges_name', sortable: true,
                    tooltip:"Charges Name",
                },
                {   header: "Percentage", width : 80,
                    dataIndex : 'pct_amount', sortable: true,
                    tooltip:"Percentage",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Operation", width : 50,
                    dataIndex : 'plus_minus', sortable: true,
                    tooltip:"Plus Minus Operation",
                    editor :  new Ext.Editor(  new Ext.form.ComboBox(
                                                {   store: new Ext.data.SimpleStore({
                                                                       fields: ["label"],
                                                                       data : [["+Plus"], ["-Minus"]]
                                                                }),
                                                    displayField:"label",
                                                    valueField:"label",
                                                    mode: 'local',
                                                    typeAhead: true,
                                                    triggerAction: "all",
                                                    selectOnFocus:true,
                                                    forceSelection :true
                                               }),
                                                {autoSize:true}
                                            ),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value == "-Minus" )
                        {   metaData.attr = "style = background-color:red;";  };
                        return value;
                    }
                },
                
                {   header: "Amount", width : 100,
                    dataIndex : 'total_amount', sortable: true,
                    tooltip:"Charges Amount",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Description", width : 200,
                    dataIndex : 'description', sortable: true,
                    tooltip:"Description",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
            ];
            this.chargesRecords = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'invoice_no', type: 'string'},
                {name: 'charges_id', type: 'string'},
                {name: 'percentage', type: 'string'},
                {name: 'plus_minus', type: 'string'},
                {name: 'amount', type: 'string'},
                {name: 'description', type: 'string'},
            ]);
            this.chargesDataStore = alfalah.core.newDataStore(
                '/purchase/3/6', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.chargesDataStore.load();
            this.region_height = alfalah.purchase.invoice.centerPanel.container.dom.clientHeight;
            this.chargesGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.chargesDataStore,
                columns: this.chargesColumns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                listeners :
                {   "beforeedit" : this.chargesGrid_beforeedit,
                    "afteredit" : this.chargesGrid_afteredit 
                },
                scope : this,
            });            
            this.summaryTab = new Ext.Panel(
            {   title:  "S U M M A R Y",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.summaryGrid]
                },
                {   region: 'south',
                    title: 'C H A R G E S',
                    split: true,
                    height : this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    layout: 'border',
                    items:[
                    {   region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.chargesGrid]
                    }]
                }],
            });
            /************************************
                SEARCH QC ITEM TAB
            ************************************/
            this.qcColumns = [
                {   header: "REF.No", width : 150,
                    dataIndex : 'ref_no', sortable: true,
                    tooltip:"Reference Document",
                },
                
                {   header: "GRIN.No", width : 100,
                    dataIndex : 'grin_no', sortable: true,
                    tooltip:"GRIN Document",
                },
                {   header: "Items", width : 200,
                    dataIndex : 'item_name', sortable: true,
                    tooltip:"Items",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.item_id+' ';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "PO.Qty", width : 80,
                    dataIndex : 'po_qty_primary', sortable: true,
                    tooltip:"PO Quantity",
                },
                {   header: "QC", width : 80,
                    dataIndex : 'quantity_primary', sortable: true,
                    tooltip:"Quantity Primary",
                },
                {   header: "Reject", width : 80,
                    dataIndex : 'reject_primary', sortable: true,
                    tooltip:"Quantity Primary",
                },
                {   header: "Total", width : 80,
                    dataIndex : 'total_primary', sortable: true,
                    tooltip:"Total Primary",
                },
                {   header: "UM", width : 50,
                    dataIndex : 'um_primary', sortable: true,
                    tooltip:"Unit Measurement",
                },
            ];
            this.qcDataStore = alfalah.core.newDataStore(
                '/purchase/3/12', false,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            // this.qcDataStore.load();
            this.qcSearchs = [
                {   id: 'qc_ref_no',
                    cid: 'ref_no',
                    fieldLabel: 'REF.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120,
                    readOnly : true
                },
                {   id: 'qc_item_id',
                    cid: 'item_id',
                    fieldLabel: 'Item.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'qc_item_name',
                    cid: 'item_name',
                    fieldLabel: 'Item.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.qcGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.qcDataStore,
                columns: this.qcColumns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : 200, //alfalah.country.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add Items',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add_items,
                        scope : this
                    },
                    {   text:'Add ALL ITEMS',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add_all_items,
                        scope : this
                    },
                ],
                scope : this
            });
            this.qcTab = new Ext.Panel(
            {   title:  "F I N D  -  Q C ' s",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 210,
                    minSize: 210,
                    maxSize: 400,
                    collapsible: true,
                    hidden : true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 70,
                                defaultType: 'textfield',
                                items : this.qcSearchs,
                                frame: true,
                                autoScroll : true,
                                // tbar: [
                                // {   //id : 'search_btn_qc',
                                //     text:'Search',
                                //     tooltip:'Search',
                                //     iconCls: 'silk-zoom',
                                //     handler : this.qc_search_handler,
                                //     scope : this,
                                // }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.qcGrid]
                }],
            });
        },
        // build the layout
        build_layout: function()
        {   
            this.Tab = new Ext.Panel(
            {   id : "po_inv_form",
                jsId : "po_inv_form",
                // title:  the_title,
                title : '<span style="background-color: yellow;">'+the_title+'</span>',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region: 'center',     
                    xtype: 'tabpanel',
                    activeTab: 0,
                    items:[this.Form, this.detailTab, this.summaryTab, this.qcTab]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   // check headers
            // console.log(this.Headers);
            v_ref_no = "";
            v_item_id = "";
            if (Ext.isObject(this.Headers))
            {   console.log("Headers is object");
                Ext.each(this.Headers,
                    function(the_header)
                    {   console.log(the_header);
                        Ext.getCmp('invfrm_company_id').setValue(the_header.company_id);
                        Ext.getCmp('invfrm_company_name').setValue(the_header.company_name);
                        Ext.getCmp('invfrm_invoice_no').setValue(the_header.invoice_no);
                        v_ref_no = the_header.ref_no;
                        Ext.getCmp('invfrm_ref_no').setValue(v_ref_no);
                        Ext.getCmp('qc_ref_no').setValue(v_ref_no);
                        Ext.getCmp('invfrm_ref_type').setValue('O');
                        Ext.getCmp('invfrm_party_id').setValue(the_header.party_id);
                        Ext.getCmp('invfrm_party_name').setValue(the_header.party_name);
                        Ext.getCmp('invfrm_party_inv_no').setValue(the_header.supplier_invoice_no);
                        
                        Ext.getCmp('invfrm_ref_date').setValue(the_header.ref_date);
                        Ext.getCmp('invfrm_site_id').setValue(the_header.site_id);
                        Ext.getCmp('invfrm_warehouse_id').setValue(the_header.warehouse_id);
                        Ext.getCmp('invfrm_description').setValue(the_header.description);
                        Ext.getCmp('invfrm_item_id').setValue(the_header.item_id);
                        v_item_id = the_header.item_id;
                        Ext.getCmp('invfrm_ref_currency').setValue(the_header.currency_id);
                        Ext.getCmp('invfrm_remain_qty').setValue(the_header.remain_qty);
                        
                    }, this);
            };
            // check records
            if (Ext.isObject(this.Records))
            {   console.log("Records is object");
                the_record = this.Records;
                Ext.each(the_record.fields.items,
                    function(the_field)
                    {   console.log(the_field.name);
                        switch (the_field.name)
                        {   case "company_id" :
                            case "company_name" :
                            case "invoice_no" :
                            case "currency_id" :
                            case "ref_currency" :
                            case "ref_no" :
                            case "ref_type" :
                            case "ref_date" :
                            case "party_id" :
                            case "party_name" :
                            case "party_inv_no" :
                            case "xrate_value" :
                            case "description" :
                            {   Ext.getCmp('invfrm_'+the_field.name).setValue(the_record.data[the_field.name]);
                            };
                            break;
                            case "party_delivery_no" :
                            {   Ext.getCmp('invfrm_delivery_no').setValue(the_record.data[the_field.name]);
                            };
                            break;
                            case "due_date" :
                            {   Ext.getCmp('invfrm_due_date').setValue(alfalah.core.shortdateRenderer(the_record.data[the_field.name],'Y-m-d', 'd/m/Y'));
                            };
                            break;
                        };
                    });
            };

            this.qcDataStore.baseParams = {   
                s:"form",
                company_id : this.Headers.company_id,
                ref_no : v_ref_no,
                item_id : v_item_id };
            // load data if its not view mode
            if (this.mode_view == true ){} else { this.qcDataStore.load(); };

            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'invoice_no', type: 'string'},
                {name: 'seq_no', type: 'string'},
                {name: 'item_id', type: 'string'},
                {name: 'item_name', type: 'string'},
                {name: 'available_qty', type: 'string'},
                {name: 'quantity_primary', type: 'string'},
                {name: 'um_primary', type: 'string'},
                {name: 'unit_price', type: 'string'},
                {name: 'currency_id', type: 'string'},
                {name: 'stock_ref_no', type: 'string'},
                {name: 'stock_ref_type', type: 'string'},
                {name: 'stock_invoice_no', type: 'string'},
                {name: 'stock_party_name', type: 'string'},
                {name: 'remark', type: 'string'},
                {name: 'created_date', type: 'date'}
            ]);
        },
        // qc item search button
        qc_search_handler : function(button, event)
        {   the_parameter = alfalah.core.getSearchParameter(this.qcSearchs);
            this.qcDataStore.removeAll();
            this.qcDataStore.baseParams = Ext.apply( the_parameter,
            {   s:"form",
                company_id : this.Headers.company_id,
                site_id : this.Headers.site_id,
                warehouse_id : this.Headers.warehouse_id,
                limit:this.page_limit, start:this.page_start });
            this.qcDataStore.reload();
        },  
        Grid_add_items: function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.qcGrid.getSelectionModel().selection;
            var is_inserted = false;
            if ( the_record )
            {   this.Grid.store.insert( 0, the_record.record);
                is_inserted = true;
                the_qc = this.qcGrid.getStore();
                the_qc.removeAt(the_record.cell[0]);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_add_all_items: function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.qcDataStore.data.items;
            var is_inserted = false;
            if ( the_record )
            {   the_qc = this.qcGrid.getStore();
                for (var the_x = 0; the_x < the_record.length; the_x++)
                {   this.Grid.store.insert( 0, the_record[the_x]);  };
                this.qcDataStore.removeAll();
                is_inserted = true;
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_afteredit : function(the_cell)
        {   switch (the_cell.field)
            {   case "quantity_primary":
                {   var data = the_cell.record.data;
                    var value = parseInt(the_cell.value);
                    // var unit_price = parseInt(data.unit_price);
                    // var po_qty_primary = parseInt(data.po_qty_primary);
                    var quantity_primary = parseInt(data.quantity_primary);
                    var remain_qty = parseInt(data.remain_qty);
                    var stock_qty = parseInt(data.stock_qty);
                    if (quantity_primary > remain_qty)
                    {   Ext.Msg.show(
                        {   title:'W A R N I N G ',
                            msg: ' invoice.Qty is higher than Remain.Qty <BR>Do you want to process it ? ',
                            buttons: Ext.Msg.YESNO,
                            fn: function(buttonId, text)
                                {   if (buttonId =='yes')
                                    {   the_cell.grid.getView().refresh();
                                    } else 
                                    {   data.quantity_primary = 0;
                                        the_cell.grid.getView().refresh();
                                    };
                                },
                            icon: Ext.MessageBox.WARNING
                        });
                    };
                    if (quantity_primary > stock_qty)
                    {   Ext.Msg.show(
                        {   title:'E R R O R ',
                            msg: ' invoice.Qty is higher than Stock.Qty.',
                            buttons: Ext.Msg.OK,
                            fn: function(buttonId, text)
                                {   data.quantity_primary = 0;
                                    the_cell.grid.getView().refresh();
                                },
                            icon: Ext.MessageBox.WARNING
                        });
                    };
                };
                break;
            };
        },
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   
                if (Ext.isEmpty(the_record.record.data.id))
                {   the_store = this.Grid.getStore();
                    removed_record = the_record.cell[0];
                    the_modified = the_store.getModifiedRecords();
                    // delete the Grid_UI
                    the_store.removeAt(removed_record); 
                }
                else
                {   the_record.record.set("remark", "REMOVED"); };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_recalculate: function(button, event)
        {   this.Grid.stopEditing();
            var the_records = this.DataStore.data.items;
            var json_data = [];
            var v_json = {};
            var v_data = {};
            var is_new_item = false;
            Ext.each(the_records,
                function(the_record)
                {   is_new_item = false;
                    the_data = the_record.data;
                    if (json_data.length == 0)
                    {   is_new_item = true;
                        json_data.push({
                            id              : "",
                            invoice_no      : "",
                            item_id         : the_data.item_id,
                            item_name       : the_data.item_name,
                            quantity_primary: parseInt(the_data.quantity_primary),
                            um_primary      : the_data.um_primary,
                            currency_id     : the_data.currency_id,
                            po_price        : the_data.po_price,
                            remark          : "",
                            created_date    : "",
                            unit_price      : 0,    // default 0, for invoiceOrder order
                            amount          : 0,    // default 0, for invoiceOrder order
                            po_qty_primary  : 0,    // default 0, for invoiceOrder order
                            pcs_subdtl      : 0,    // default 0, for invoiceOrder order  
                            has_subdetail   : 1,    // default 0, for invoiceOrder order
                        });
                    }
                    else
                    {   is_new_item = true;
                        Ext.each(json_data,
                            function(v_json)
                            {   if (v_json.item_id == the_data.item_id)
                                {   v_json.quantity_primary = v_json.quantity_primary + parseInt(the_data.quantity_primary);
                                    is_new_item = false;
                                };
                            });
                        // add new items
                        if( is_new_item == true )
                        {   
                            json_data.push({
                                id              : "",
                                invoice_no      : "",
                                item_id         : the_data.item_id,
                                item_name       : the_data.item_name,
                                quantity_primary: parseInt(the_data.quantity_primary),
                                um_primary      : the_data.um_primary,
                                currency_id     : the_data.currency_id,
                                po_price        : the_data.po_price,
                                remark          : "",
                                created_date    : "",
                                unit_price      : 0,    // default 0, for invoiceOrder order
                                amount          : 0,    // default 0, for invoiceOrder order
                                po_qty_primary  : 0,    // default 0, for invoiceOrder order
                                pcs_subdtl      : 0,    // default 0, for invoiceOrder order  
                                has_subdetail   : 1,    // default 0, for invoiceOrder order
                            });
                        };
                    };
                });
            the_summaryGrid = this.summaryGrid;
            the_summaryRecords = this.summaryRecords;
            v_total_amount = 0;
            if (json_data.length > 0)
            {   this.summaryDataStore.rejectChanges();
                this.summaryDataStore.removeAll();
                Ext.each(json_data,
                    function(v_json)
                    {   v_total_amount = v_total_amount + v_json.amount;
                        the_summaryGrid.store.insert( 0, new the_summaryRecords(v_json)); 
                    });
                this.isRecalculate = true;
            };
            Ext.getCmp('invfrm_total_amount').setValue(v_total_amount.toFixed(2));
            this.chargesGrid_recalculate();
        },
        chargesGrid_beforeedit : function(the_cell)
        {   summaryDataStore = alfalah.purchase.invoice.forms.summaryDataStore;
            if (summaryDataStore.data.length == 0)
            {   Ext.Msg.show(
                {   title:'W A R N I N G ',
                    msg: 'Need Recalculate Process',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });
                return false;
            }
            else return true;
        },
        chargesGrid_afteredit : function(the_cell)
        {   v_percentage = 0;
            v_pct_amount = 0;
            v_total_amount = parseFloat(Ext.getCmp('invfrm_total_amount').getValue());
            if (v_total_amount == 0)
            {   Ext.Msg.show(
                {   title:'E R R O R ',
                    msg: 'Total Amount is zero.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
            else 
            {   switch (the_cell.field)
                {   case "pct_amount"   :
                    {   v_percentage = the_cell.value;
                        v_pct_amount = v_total_amount * ( parseFloat(v_percentage) /100);
                        the_cell.record.data.total_amount = v_pct_amount.toFixed(2);
                        console.log(v_pct_amount);
                    };
                    break;
                    case "total_amount" :
                    {   v_pct_amount = the_cell.value;
                        v_percentage = (v_pct_amount / v_total_amount) * 100;    
                        the_cell.record.data.pct_amount = v_percentage.toFixed(2);
                        console.log(v_percentage);
                    };
                    break;
                };
                alfalah.purchase.po.forms.chargesGrid_recalculate();
                the_cell.grid.getView().refresh();  
            };
        },
        chargesGrid_recalculate: function()
        {   this.chargesGrid.stopEditing();
            the_charges = this.chargesGrid.store.data.items;
            v_total_amount = parseFloat(Ext.getCmp('invfrm_total_amount').getValue());
            Ext.each(the_charges,
                function(the_charge)
                {   switch (the_charge.data.plus_minus)
                    {   case "+" :
                        case "+Plus" :
                        {   v_total_amount = v_total_amount + parseFloat(the_charge.data.total_amount);   };
                        break;
                        case "-" :
                        case "-Minus" :
                        {   v_total_amount = v_total_amount - parseFloat(the_charge.data.total_amount);   };
                        break;
                    };   
                });
            Ext.getCmp('invfrm_invoice_amount').setValue(v_total_amount.toFixed(2));
        },
        
        // forms grid save records
        Form_save : function(button, event)
        {   var head_data = [];
            var json_data = [];
            var modi_data = [];
            var v_json = {};
            var the_datastore = this.DataStore;
            if (alfalah.core.validateFields([
                'invfrm_receive_date', 'invfrm_due_date',
                'invfrm_currency_id', 'invfrm_xrate_value',
                'invfrm_party_inv_no', 'invfrm_party_inv_date']))
            {   console.log("detail validation");
                var item_count = this.DataStore.getCount()
                if ( item_count < 1 ) 
                {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'Please register minimum 1 Item.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO,
                        width : 300
                    });
                }
                else if ( alfalah.core.validateGridQuantity(
                            // this.DataStore.getModifiedRecords(),
                            the_datastore.data.items,
                            ['quantity_primary']))
                {   // header
                    head_data = alfalah.core.getHeadData([
                        'invfrm_id', 'invfrm_company_id', 'invfrm_party_id', 
                        'invfrm_invoice_status','invfrm_site_id', 'invfrm_currency_id',
                        'invfrm_warehouse_id', 'invfrm_created_date', 
                        'invfrm_party_inv_date', 'invfrm_invoice_no', 
                        'invfrm_ref_no', 'invfrm_ref_type', 'invfrm_ref_currency', 
                        'invfrm_receive_date', 'invfrm_due_date', 
                        'invfrm_company_name', 
                        'invfrm_party_name', 'invfrm_xrate_value', 
                        'invfrm_party_inv_no',  
                        'invfrm_description']);
                    // detail
                    json_data = alfalah.core.getDetailData(this.summaryDataStore.getModifiedRecords());
                    modi_data = alfalah.core.getDetailData(this.DataStore.getModifiedRecords());
                    
                    // submit data
                    alfalah.core.submitForm(
                        'po_inv_form', 
                        alfalah.purchase.invoice.invref.DataStore,
                        '/purchase/3/1',
                        {   'x-csrf-token': alfalah.purchase.invoice.sid },
                        {   task : 'save', st: 'O',
                            head : Ext.encode(head_data),
                            json : Ext.encode(json_data),
                            modi : Ext.encode(modi_data)
                        });
                };
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.purchase.invoice.initialize, alfalah.purchase.invoice);
// end of file
</script>
<div>&nbsp;</div>