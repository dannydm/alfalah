<script type="text/javascript">
// create namespace
Ext.namespace('saa.purchase.pr');

// create application
saa.purchase.pr = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.pr.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.pr.Tab);
            this.centerPanel.setActiveTab(this.pr.Tab);
            this.centerPanel.endUpdate();
            saa.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
saa.purchase.pr.pr= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Company", width : 100,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"Company ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> - <br> '+record.data.company_name;
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "PR.No", width : 100,
                    dataIndex : 'pr_no', sortable: true,
                    tooltip:"PR No",
                },
                {   header: "PR.Date", width : 100,
                    dataIndex : 'pr_date', sortable: true,
                    tooltip:"PR Date",
                    // renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Order.No", width : 100,
                    dataIndex : 'order_no', sortable: true,
                    tooltip:"Order No",
                },
                {   header: "PR.Type", width : 80,
                    dataIndex : 'pr_type', sortable: true,
                    tooltip:"PR Type",
                },
                {   header: "Status", width : 80,
                    dataIndex : 'pr_status_name', sortable: true,
                    tooltip:"PR Status",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.pr_status == 1)
                        {   metaData.attr = "style = background-color:lime;";  }
                        else {   metaData.attr = "style = background-color:yellow;"; };
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Approved", width : 100,
                    dataIndex : 'approved_status_name', sortable: true,
                    tooltip:"Approved",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( Ext.isEmpty(record.data.approved_date) )
                        {   metaData.attr = "style = background-color:red;";  }
                        else {   metaData.attr = "style = background-color:lime;"; };
                        return saa.core.gridColumnWrap(value);
                    }
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"pr Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"pr Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return saa.core.dateRenderer(value); }
                }
            ];
            this.Searchs = [
                {   id: 'pr_pr_no',
                    cid: 'pr_no',
                    fieldLabel: 'PR.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pr_order_no',
                    cid: 'order_no',
                    fieldLabel: 'Order.No',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pr_pr_type',
                    cid: 'pr_type',
                    fieldLabel: 'Type',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['GPR'], ['OPR']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'ALL'
                },
                {   id: 'pr_pr_status',
                    cid: 'pr_status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['Draft'], ['Final']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Draft'
                },
                {   id: 'pr_approved_status',
                    cid: 'approved_status',
                    fieldLabel: 'Approved',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ ['ALL'], ['Approved'], ['Need Approval']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Need Approval'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_prSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.pr_search_handler,
                scope : this
            });
            this.DataStore = saa.core.newDataStore(
                '/purchase/1/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : saa.purchase.pr.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    // {   text:'Copy',
                    //     tooltip:'Copy Record',
                    //     iconCls: 'silk-page-copy',
                    //     handler : this.Grid_copy,
                    //     scope : this
                    // },
                    {   text:'Edit',
                        tooltip:'Edit Record',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_edit,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : this.print_out,
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ saa.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                    {   text:'Approve',
                        tooltip:'Approve Record',
                        iconCls: 'silk-tick',
                        handler : this.Grid_approve,
                        scope : this
                    },
                    '-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_prGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_prPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_prPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_prGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_prSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_prTab",
                jsId : tabId+"_prTab",
                title:  "Purchase Requisition",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 50,
                                defaultType: 'textfield',
                                items : this.Searchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   //id : 'search_btn_pritems',
                                    text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.pr_search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        Grid_add : function(button, event)
        {   var pr_form = Ext.getCmp("pr_form");
            if (pr_form)
            {   Ext.Msg.show(
                {   title :'E R R O R ',
                    msg : 'PR Form Available',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
            else
            {   var centerPanel = Ext.getCmp('center_panel');
                saa.purchase.pr.forms.initialize(null);
                centerPanel.beginUpdate();
                centerPanel.add(saa.purchase.pr.forms.Tab);
                centerPanel.setActiveTab(saa.purchase.pr.forms.Tab);
                centerPanel.endUpdate();
                saa.core.viewport.doLayout();
            };
        },
        Grid_edit : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   centerPanel = Ext.getCmp('center-panel');
                var pr_form = Ext.getCmp("pr_form");
                if (pr_form)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'PR Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    saa.purchase.pr.forms.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(saa.purchase.pr.forms.Tab);
                    centerPanel.setActiveTab(saa.purchase.pr.forms.Tab);
                    centerPanel.endUpdate();
                    saa.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        // pr grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   if (the_record.record.data.po_count > 0)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Purchase Order Transaction Found.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   saa.core.submitGrid(
                        this.DataStore, 
                        '/purchase/1/2',
                        {   'x-csrf-token': saa.purchase.pr.sid }, 
                        {   id: the_record.record.data.id }
                    );
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_approve : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   if (the_record.record.data.pr_status == 0)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Need Final PR',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   saa.core.submitGrid(
                        this.DataStore, 
                        '/purchase/1/8',
                        {   'x-csrf-token': saa.purchase.pr.sid }, 
                        {   id: this.Grid.getSelectionModel().selection.record.data.id }
                    );
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        // pr search button
        pr_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_prSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = saa.core.getSearchParameter(this.Searchs);
                this.DataStore.removeAll();
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
        print_out: function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                saa.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        id : the_record.id });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };   
        },
    }; // end of public space
}(); // end of app
// create application
saa.purchase.pr.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        popSiteDepartment : '',
        // public methods
        initialize: function(the_record)
        {   this.Records = the_record;
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.DeptDS = saa.core.newDataStore(
                '/company/6/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.OrderDS = saa.core.newDataStore(
                '/sales/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.ItemDS = saa.core.newDataStore(
                '/administration/1/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Columns = [
                {   header: "Items", width : 250,
                    dataIndex : 'item_id', sortable: true,
                    tooltip:"Items",
                    editor: new Ext.form.ComboBox(
                    {   store: this.ItemDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'item_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.item_id = record.data.item_id;
                                    combo.gridEditor.record.data.item_name = record.data.name;
                                    combo.gridEditor.record.data.um_primary = record.data.um_id;
                                    // saa.assetITD.hardware.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.remark == "REMOVED" )
                        {   metaData.attr = "style = background-color:red;";  };
                        result = record.data.item_name+'<br> [ '+value+' ] ';
                        return saa.core.gridColumnWrap(result);
                    }
                },
                {   header: "Quantity", width : 100,
                    dataIndex : 'quantity_primary', sortable: true,
                    tooltip:"Quantity",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "UM", width : 100,
                    dataIndex : 'um_primary', sortable: true,
                    tooltip:"Unit Measurement",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Remark", width : 300,
                    dataIndex : 'remark', sortable: true,
                    tooltip:"Item Remark",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
            ];
            this.DataStore = saa.core.newDataStore(
                '/purchase/1/5', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : 200, //saa.country.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   text:'Save PR',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        scope : this
                    }
                ],
                listeners :
                {   //"beforeedit" : this.Grid_beforeedit,
                    // "afteredit" : this.Grid_afteredit
                }
            });
            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });

            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   bodyStyle:'padding:5px',
                // autoScroll : true,
                // autoHeight : true,
                title : 'H E A D E R',
                items: [
                {   layout:'column',
                    border:false,
                    items:[ // hidden columns
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'prf_company_id',
                            xtype:'textfield',
                            fieldLabel: 'Company.ID',
                            name: 'company_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'prf_pr_status',
                            xtype:'textfield',
                            fieldLabel: 'PR.Status',
                            name: 'pr_status',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'prf_to_site_id',  
                            xtype:'textfield',
                            fieldLabel: 'Site.ID',
                            name: 'to_site_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }, 
                        {   id : 'prf_to_dept_id', 
                            xtype:'textfield',
                            fieldLabel: 'Dept.ID',
                            name: 'to_dept_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'prf_created_date',  
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                            hidden : true,
                        }]
                    }, 
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'prf_pr_date', 
                            xtype:'datefield',
                            fieldLabel: 'PR Date',
                            name: 'pr_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            value : new Date()
                        },
                        {   id : 'prf_pr_no', 
                            xtype:'textfield',
                            fieldLabel: 'PR.No',
                            name: 'pr_no',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : true,
                        },
                        {   id: 'prf_pr_status_name',
                            xtype: 'combo',
                            fieldLabel: 'Status',
                            name: 'pr_status_name',
                            store : new Ext.data.SimpleStore(
                            {   fields: ['status'],
                                data : [ ['Draft'], ['Final']]
                            }),
                            displayField:'status',
                            valueField :'status',
                            value : 'Draft',
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            width:100,
                        },
                        ]
                    }, 
                    {   columnWidth:.7, 
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'prf_company_name', 
                            xtype:'textfield',
                            fieldLabel: 'Company.Name',
                            name: 'company_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        {   id : 'prf_order_no',
                            xtype:'combo',
                            fieldLabel: 'Order.No',
                            name: 'order_no',
                            anchor:'95%',
                            allowBlank: true,
                            store: this.OrderDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'order_no',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form",
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    // 'select' : function (combo, record, indexVal)
                                    // {   Ext.getCmp('prf_reporter_name').setValue(record.data.name);
                                    // },
                            }
                        },
                        {   id : 'prf_to_dept_name',
                            xtype:'combo',
                            fieldLabel: 'Dept.Name',
                            name: 'to_dept_name',
                            anchor:'95%',
                            allowBlank: false,
                            store: this.DeptDS,
                            typeAhead: false,
                            width: 250,
                            displayField: 'display',
                            valueField: 'display',
                            forceSelection: true,
                            loadingText: 'Searching...',
                            pageSize:25,
                            hideTrigger:true,
                            tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                                ),
                            itemSelector: 'div.search-item',
                            listeners : {
                                scope: this,
                                    'beforequery' : function (combo, query, forceAll, cancel)
                                    {   combo.combo.store.baseParams = {
                                            s:"form", 
                                            limit:this.page_limit, start:this.page_start };
                                    },
                                    'select' : function (combo, record, indexVal)
                                    {   Ext.getCmp('prf_to_site_id').setValue(record.data.site_id);
                                        Ext.getCmp('prf_to_dept_id').setValue(record.data.dept_id);
                                    },
                            }
                        },
                        ]
                    }]
                },  // description area
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.9,
                        layout: 'form',
                        border:false,
                        items: [ 
                        {   id : 'prf_description', 
                            xtype:'textarea',
                            fieldLabel: 'Description',
                            name: 'description',
                            anchor:'95%',
                            // allowBlank: false,
                            height : 100,
                        }]
                    }]
                }]
            });
        },
        // build the layout
        build_layout: function()
        {   if (Ext.isObject(this.Records))
            {   the_title = "Edit PR"; }
            else { the_title = "New PR"; };

            this.Tab = new Ext.Panel(
            {   id : "pr_form",
                jsId : "pr_form",
                // title:  the_title,
                title : '<span style="background-color: yellow;">'+the_title+'</span>',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region: 'center',     
                    xtype: 'tabpanel',
                    activeTab: 0,
                    items:[this.Form, this.detailTab]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   if (Ext.isObject(this.Records))
            {   Ext.each(this.Records.fields.items,
                    function(the_field)
                    {   switch (the_field.name)
                        {   case "created_date" :
                            case "company_id" :
                            case "company_name" :
                            case "pr_date" :
                            case "pr_status" :
                            case "pr_status_name" :
                            case "to_site_id" :
                            case "to_dept_id" :
                            case "to_dept_name" :
                            case "description" :
                            case "order_no" :
                            {   console.log('pr_'+the_field.name);
                                Ext.getCmp('prf_'+the_field.name).setValue(this.Records.data[the_field.name]);
                            };
                            break;
                            case "pr_no" :
                            {   Ext.getCmp('prf_'+the_field.name).setValue(this.Records.data[the_field.name]);
                                this.DataStore.baseParams = {   
                                    s:"form", pr_no: this.Records.data["pr_no"],
                                    limit:this.page_limit, start:this.page_start};
                                this.DataStore.reload();
                            };
                            break;
                        };
                    }, this);
            };
            this.Records = Ext.data.Record.create(
                [   {name: 'pr_no', type: 'string'},
                    {name: 'item_id', type: 'string'},
                    {name: 'quantity_primary', type: 'string'},
                    {name: 'um_primary', type: 'string'},
                    {name: 'remark', type: 'string'},
                    {name: 'created_date', type: 'date'}
                ]);
        },      
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   pr_no : "",
                    item_id: "New Item",
                    quantity_primary : 0,
                    um_primary : "",
                    remark : "",
                    created_date: "",
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 0);
        },
        // pr grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            var the_record = this.Grid.getSelectionModel().selection;
            // b = saa.purchase.pr.forms.DataStore.removeAt(a.cell[0])
            if ( the_record )
            {   if (Ext.isEmpty(the_record.record.data.pr_no))
                {   this.Grid.getStore().removeAt(the_record.cell[0]);
                }
                else
                {   the_record.record.set("remark", "REMOVED");
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        // forms grid save records
        Form_save : function(button, event)
        {   var head_data = [];
            var json_data = [];
            var modi_data = [];
            var v_json = {};
            if (saa.core.validateFields([
                'prf_pr_status_name', 'prf_to_dept_name']))
            {   // detail validation
                var item_count = this.DataStore.getCount()
                if ( item_count < 1 ) 
                {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'Please register minimum 1 Item.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO,
                        width : 300
                    });
                } 
                else if ( saa.core.validateGridQuantity(
                            this.DataStore.getModifiedRecords(),
                            ['quantity_primary']))
                {   // header 
                    head_data = saa.core.getHeadData([
                        'prf_company_id', 'prf_company_name', 
                        'prf_pr_no', 'prf_pr_date', 'prf_pr_status_name',
                        'prf_to_site_id', 'prf_to_dept_id', 'prf_to_dept_name', 
                        'prf_created_date', 
                        'prf_order_no', 'prf_description'
                        ]);
                    // detail
                    json_data = saa.core.getDetailData(this.DataStore.getModifiedRecords());
                    // submit data
                    saa.core.submitForm(
                        'pr_form', 
                        saa.purchase.pr.pr.DataStore,
                        '/purchase/1/1',
                        {   'x-csrf-token': saa.purchase.pr.sid },
                        {   task: 'save',
                            head : Ext.encode(head_data),
                            json : Ext.encode(json_data),
                        });
                };
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(saa.purchase.pr.initialize, saa.purchase.pr);
// end of file
</script>
<div>&nbsp;</div>