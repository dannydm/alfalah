<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.administrations.items');

// create application
alfalah.administrations.items = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.items.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.items.Tab);
            this.centerPanel.setActiveTab(this.items.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.administrations.items.items= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.UMDS = alfalah.core.newDataStore(
                '/parameter/2/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'item_id', sortable: true,
                    tooltip:"Item ID",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( record.data.status ==1 )
                        {   metaData.attr = "style = background-color:red;";  }
                        else {   metaData.attr = "style = background-color:lime;"; };
                        return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Parent.ID", width : 50,
                    dataIndex : 'parent_id', sortable: true,
                    tooltip:"items Parent ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Name", width : 200,
                    dataIndex : 'name', sortable: true,
                    tooltip:"items Name",
                },
                {   header: "Material", width : 100,
                    dataIndex : 'material_name', sortable: true,
                    tooltip:"Material Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Specification", width : 100,
                    dataIndex : 'spec_name', sortable: true,
                    tooltip:"Specification Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Type", width : 50,
                    dataIndex : 'item_type', sortable: true,
                    tooltip:"items Type",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if ( value =='B') 
                        {   metaData.attr = "style = background-color:yellow;";  }
                        else {   metaData.attr = "style = background-color:lime;"; };
                        return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "UM", width : 100,
                    dataIndex : 'um_id', sortable: true,
                    tooltip:"UM ID",
                    editor: new Ext.form.ComboBox(
                    {   store: this.UMDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'um_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.um_id = record.data.um_id;
                                    combo.gridEditor.record.data.um_name = record.data.name;
                                    alfalah.administrations.items.items.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.um_name+ ' ['+value+']';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "SubDetail", width : 100,
                    dataIndex : 'has_subdetail_name', sortable: true,
                    tooltip:"Need SubDetail",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["label"],
                                               data : [["NO"], ["YES"]]
                                        }),
                            displayField:"label",
                            valueField:"label",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true,
                            value : "NO"
                        }),
                        {autoSize:true}
                    )
                },
                {   header: "Status", width : 100,
                    dataIndex : 'status_name', sortable: true,
                    tooltip:"Status",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["label"],
                                               data : [["Active"], ["Inactive"]]
                                        }),
                            displayField:"label",
                            valueField:"label",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true,
                            value : "Active"
                        }),
                        {autoSize:true}
                    )
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"items Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_', sortable: true,
                    tooltip:"items Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'parent_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'material_name', type: 'string'},
                {name: 'spec_name', type: 'string'},
                {name: 'item_type', type: 'string'},
                {name: 'um_id', type: 'string'},
                {name: 'um_name', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'status_name', type: 'string'},
                {name: 'has_subdetail', type: 'string'},
                {name: 'has_subdetail_name', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'items_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'items_spec',
                    cid: 'spec',
                    fieldLabel: 'Spec',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'items_um',
                    cid: 'um_id',
                    fieldLabel: 'UM',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'items_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ [''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_itemsSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.items_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                '/administration/1/0', false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.administrations.items.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Buyer Item',
                        tooltip:'Add Buyer Item',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this,
                        type : 'B'
                    },
                    {   text:'PR Item',
                        tooltip:'Add PR Item',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this,
                        type : 'P'
                    },
                    // {   text:'Copy',
                    //     tooltip:'Copy Record',
                    //     iconCls: 'silk-page-copy',
                    //     handler : this.Grid_copy,
                    //     scope : this
                    // },
                    // {   text:'Edit',
                    //     tooltip:'Edit Record',
                    //     iconCls: 'silk-page-white-edit',
                    //     handler : this.Grid_edit,
                    //     scope : this
                    // },
                    {   id : tabId+'_itemsSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_itemsGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_itemsPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_itemsPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_itemsGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_itemsSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_itemsTab",
                jsId : tabId+"_itemsTab",
                title:  "Items",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   id: "New ID",
                    parent_id: "",
                    name: "New Name",
                    material_name : "New Material",
                    spec_name: "New Spec",
                    item_type: button.type,
                    um_id: "New UM",
                    status: 0,
                    status_name: "Active",
                    has_subdetail : 0,
                    has_subdetail_name : "NO",
                    created_date: "",
                    modified_date: ""
                }));
            this.Grid.startEditing(0, 3);
        },
        Grid_edit : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   centerPanel = Ext.getCmp('center-panel');
                var form_downtime = Ext.getCmp("form_downtime");
                if (form_downtime)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'DownTime Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   alfalah.administrations.items.forms.initialize(the_record.record);
                    alfalah.administrations.items.centerPanel.beginUpdate();
                    alfalah.administrations.items.centerPanel.add(alfalah.administrations.items.forms.Tab);
                    alfalah.administrations.items.centerPanel.setActiveTab(alfalah.administrations.items.forms.Tab);
                    alfalah.administrations.items.centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        // items grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   //console.log("Data changes, Do you want to save it before reloads ? ") ;
                var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        // this.store is equal with alfalah.administrations.items.items.DataStore
                        Ext.iterate(the_record.data, 
                            function(key, value) 
                            {   console.log(key + ' = ' + value); 
                                if (Ext.isDate(value))
                                {   v_data[ key ] = value.format("d/m/Y"); }
                                else { v_data [ key ] = value; };
                                v_json= Ext.apply( v_json, v_data);
                            });

                        
                        // Ext.each(this.DataStore.reader.meta.fields,
                        //     function(the_field)
                        //     {   v_data = {};
                        //         var the_data = the_record.data;
                        //         if (Ext.isDate( the_data[the_field["name"]] ) )
                        //         {   v_data[ the_field["name"] ] = the_data[the_field["name"]].format("d/m/Y");  }
                        //         else
                        //         {   v_data[ the_field["name"]] = the_data[the_field["name"]]; };
                        //         v_json= Ext.apply( v_json, v_data);
                        //     });
                        json_data.push(v_json);
                    }, this);
                Ext.Ajax.request(
                {   method: 'POST',
                    url: '/administration/1/1',
                    headers:
                    {   'x-csrf-token': alfalah.administrations.items.sid },
                    params  :
                    {   json: Ext.encode(json_data)
                    },
                    success : function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.message+'\n '+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   this.DataStore.reload();    };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    scope: this
                });
            };
        },
        // items grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            Ext.Ajax.request(
            {   method: 'POST',
                url: '/administration/1/2',
                headers:
                {   'x-csrf-token': alfalah.administrations.items.sid },
                params  :
                {   id: this.Grid.getSelectionModel().selection.record.data.id
                },
                success: function(response)
                {   var the_response = Ext.decode(response.responseText);
                    if (the_response.success == false)
                    {   Ext.Msg.show(
                        {   title :'E R R O R ',
                            msg : 'Server Message : '+'\n'+the_response.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                         });
                    }
                    else
                    {   this.DataStore.reload();
                        //datastore.remove(grid.getSelectionModel().selections.items[0]);
                    };
                },
                failure: function()
                { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                },
              scope: this
            });
        },
        // items search button
        items_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_itemsSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.administrations.items.task,
                        act: alfalah.administrations.items.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.administrations.items.initialize, alfalah.administrations.items);
// end of file
</script>
<div>&nbsp;</div>