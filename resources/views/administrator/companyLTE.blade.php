<!-- Using default Layout -->
@extends('layouts_backend._iframe_backend')
<!-- load your extry css styles -->
@section('extra_styles')
<link rel="stylesheet" href="../../w2ui/w2ui-1.5.rc1.min.css" />
<link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
<style>
.tab {
    width: 100%;
    height: 400px;
    border: 1px solid silver;
    border-top: 0px;
    display: none;
    padding: 10px;
    overflow: auto;
}
</style>
@endsection
<!-- load your main content page -->
@section('content')
<div id="countries_tab">
    <div id="countries_config_tab" style="width: 100%; height: 29px;"></div>
    <div id="currency_tab" class="tab">
        First Tab    
    </div>
    <!-- <div id="currency_layout" style="width: 100%; height: 100%;"> -->
    <div id="currency_layout" class="tab">
      <div id="currency_grid">
      </div>
    </div>
    <div id="country_tab" class="tab">
        Second tab, with some HTML in it. :)
        
        <div id="country_layout" style="width: 100%; height: 100%;">
        </div>
    </div>
    <div id="city_tab" class="tab">
        What did you expect, of course it is the third tab.
    </div>
</div>
@endsection
<!-- load your extra js scripts -->
@section('extra_scripts')
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- <script src="../../bower_components/fastclick/lib/fastclick.js"></script> -->
<!-- AdminLTE App -->
<!-- <script src="../../dist/js/adminlte.min.js"></script> -->
<!-- AdminLTE for demo purposes --><!-- 
<script src="dist/js/demo.js"></script> -->
<!--AdminLTE Iframe-->
<!-- <script src="dist/js/app_iframe.js"></script> -->
<script src="../../w2ui/w2ui-1.5.rc1.min.js"></script>
<script type="text/javascript">
    // Define namespace
    alfalah = parent.alfalah;
    alfalah.namespace('alfalah.countries');
    // create application
    alfalah.countries = function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        // this.tabId = '{{ $TABID }}';
        this.config;
        // private functions
        // public space
        return {
            centerPanel : 0,
            the_records : [],
            sid : '{{ csrf_token() }}',
            task : ' ',
            act : ' ',
            // public methods
            initialize: function()
            { 
                this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {   
                this.config = {
                    tabs: 
                    {   name: 'countries_config_tab',
                        active: 'currency_tab',
                        tabs: [
                            { id: 'currency_tab', caption: 'Curency' },
                            { id: 'country_tab', caption: 'Country' },
                            { id: 'city_tab', caption: 'City' },
                            { id: 'currency_layout', caption: 'Layout' },
                        ]  ,
                        onClick: function (event) {
                            // console.log('event.target');
                            // console.log(event.target);
                            $('#countries_tab .tab').hide();
                            $('#countries_tab #' + event.target).show();
                        }
                    },
                    currency_grid: 
                    {   name: 'currency_grid',
                        ref_url:  "{{ url('/country/1/0') }}",
                        method : 'GET',
                        recid : 'currency_id',
                        // selectType: 'cell',
                        show: { 
                            // toolbar: true,
                            footer: true,
                            // toolbarSave: true
                        },
                        columns: [
                            { field: 'currency_id', caption: 'ID', size: '5%', sortable: true, 
                            resizable: true, searchable: true, editable: { type: 'text' } },
                            { field: 'name', caption: 'Name', size: '33%', sortable: true, 
                            searchable: true, editable: { type: 'text' } },
                            { field: 'symbol', caption: 'Symbol', size: '10%', sortable: true,resizable: true, editable: { type: 'text' } },
                            { field: 'status', caption: 'Status', size: '10%', sortable: true,resizable: true, editable: { type: 'text' } },
                            { field: 'created_date', caption: 'Created', size: '10%' },
                        ],
                        newRecord : function()
                        {   console.log('add new record');
                            console.log(this);
                            this.add({
                                currency_id: "",
                                name: "New Currency",
                                recid: "",
                                status: "1",  // Inactive currency
                                symbol: "",
                                created_date: ""
                            }, true); // as fist record
                        },
                        onLoad: function(event)
                        {   console.log("currency_grid");
                            console.log(event);
                        }
                    },
                    currency_layout: 
                    {   name: 'currency_layout',
                        padding: 4,
                        panels: [
                        {   type: 'main', size: '100%', resizable: true, minSize: 300,
                            toolbar: 
                            {   items: [
                                {   type: 'button', text: 'Add', icon: 'fa fa-plus',
                                    onClick: function(event)
                                    {   console.log('add button loh');
                                        console.log(this);
                                        console.log(event);
                                        // w2ui.currency_grid.newRecord();
                                    } 
                                },
                                {   type: 'button', text: 'Save', icon: 'fa fa-save',
                                    // onClick: function(event)
                                    // { alfalah.core.submitGrid(
                                    //     w2ui.currency_grid, 
                                    //     '/country/1/1',
                                    //     alfalah.countries.sid
                                    //   );
                                    // }
                                },
                                {   type: 'break'},
                                {   type: 'button', text: 'Delete', icon: 'fa fa-remove',
                                    // onClick: function(event)
                                    // { alfalah.core.ajax(
                                    //     '/country/1/2',                          //the_url, 
                                    //     {                                 //the_parameters,
                                    //       '_token' : alfalah.countries.sid,
                                    //       json : JSON.stringify([
                                    //         { id : w2ui.currency_grid.getSelection()[0] 
                                    //         }])
                                    //     },                  
                                    //     "POST",                           //the_type, 
                                    //     function(response)                //fn_success
                                    //     { w2ui.currency_grid.reload(); 
                                    //     },
                                    //     function(response)                //fn_fail, 
                                    //     { console.log("FAILED");
                                    //       console.log(response);
                                    //     },
                                    //     null                              //fn_always
                                    //   );
                                    // } 
                                },
                                {   type: 'break'},
                                {   type: 'button', text: 'PDF', icon: 'fa fa-file-pdf-o' },
                                {   type: 'button', text: 'XLS', icon: 'fa fa-file-excel-o' },
                                ],
                            }
                          },
                          {     type: 'right', minSize: 200,
                                toolbar: 
                                { items: [
                                    {   type: 'button', text: 'Search', icon: 'fa fa-search',
                                        // onClick: function(event ){ 
                                        //   w2ui.currency_grid.load(w2ui.currency_grid.ref_url); }
                                    },
                                ],
                                onClick: function (event) {
                                    console.log(event);
                                }
                            } 
                          }
                        ]
                    },
                };
            },
            // build the layout
            build_layout: function()
            {
                $('#countries_config_tab').w2tabs(this.config.tabs);
                $('#currency_layout').w2layout(this.config.currency_layout);
                $('#currency_grid').w2grid(this.config.currency_grid);
                w2ui.currency_layout.content('main', w2ui.currency_grid);

                $('#currency_tab').show();
                },
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            { 
            },
        }; // end of public space
    }(); // end of app
    $(document).ready(alfalah.countries.initialize());
</script>

@endsection
