<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.permission');

// create application
alfalah.permission = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.menu.initialize();
            this.user.initialize();
            this.role.initialize();
            this.role_menu.initialize();
            this.role_user.initialize();
            this.role_page.initialize();
            this.user_page.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.menu.Tab);
            this.centerPanel.add(this.user.Tab);
            this.centerPanel.add(this.role.Tab);
            this.centerPanel.add(this.role_menu.Tab);
            this.centerPanel.add(this.role_user.Tab);
            this.centerPanel.add(this.role_page.Tab);
            this.centerPanel.add(this.user_page.Tab);
            this.centerPanel.setActiveTab(this.menu.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.permission.menu= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'menu_id', sortable: true,
                    tooltip:"Menu ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Parent.ID", width : 50,
                    dataIndex : 'parent_menu_id', sortable: true,
                    tooltip:"Parent Menu ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Name", width: 200,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Menu Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Link", width : 300,
                    dataIndex : 'link', sortable: true,
                    tooltip:"Menu Target Link",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);  }
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    tooltip:"Menu Status",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["label", "key"],
                                               data : [["Active", "0"], ["Inactive", "1"]]
                                        }),
                            displayField:"label",
                            valueField:"key",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
                {   header: "Level", width : 50,
                    dataIndex : 'level_degree', sortable: true,
                    tooltip:"Menu Degree Level",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Arrange.No", width : 50,
                    dataIndex : 'arrange_no', sortable: true,
                    tooltip:"Menu arrange no sequence",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Menu Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"Menu Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'menu_id', type: 'integer'},
                {name: 'parent_menu_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'object_id', type: 'integer'},
                {name: 'link', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'has_child', type: 'integer'},
                {name: 'level_degree', type: 'string'},
                {name: 'arrange_no', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'menu_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'menu_link',
                    cid: 'link',
                    fieldLabel: 'link',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'menu_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ [''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: ''
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_menuSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.menu_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/permission/1/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.permission.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Copy',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.Grid_copy,
                        scope : this
                    },
                    //{   text:'Edit',
                    //    tooltip:'Edit Record',
                    //    iconCls: 'silk-page-white-edit',
                    //    //handler : this.menu_grid_
                    //},
                    {   id : tabId+'_menuSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_menuGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_menuPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_menuPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_menuGridBBar').getPageData().activePage -1) * page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_menuSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_menuTab",
                jsId : tabId+"_menuTab",
                title:  "Menu",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // menu grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   menu_id: "",
                    parent_menu_id: "",
                    name: "New Name",
                    object_id : 0,
                    link: "New Link Url",
                    status: 0,
                    has_child : 0,
                    level_degree: 0,
                    arrange_no: 1,
                    created_date: "",
                    modified_date: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // menu grid copy and make it a new record
        Grid_copy: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection)
            {   var data = this.Grid.getSelectionModel().selection.record.data;
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   id: "",
                        parent_menu_id: data.parent_menu_id,
                        name: data.name,
                        object_id : data.object_id,
                        link: data.link,
                        status: data.status,
                        has_child : data.has_child,
                        level_degree: data.level_degree,
                        arrange_no: data.arange_no
                    }));
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },
        // menu grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/permission/1/1') }}",
                    {   'x-csrf-token': alfalah.permission.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // menu grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/permission/1/2') }}",
                {   'x-csrf-token': alfalah.permission.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.menu_id }
            );
        },
        // menu search button
        menu_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_menuSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter, {s:"form", limit:this.page_limit, start:this.page_start});
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.permission.user= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.UserDS = alfalah.core.newDataStore(
                "{{ url('/permission/2/10') }}", true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.DeptDS = alfalah.core.newDataStore(
                '/company/6/9', true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'user_id', sortable: true,
                    tooltip:"user ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Parent.ID", width : 50,
                    dataIndex : 'parent_user_id', sortable: true,
                    tooltip:"Parent user ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "UserName", width : 150,
                    dataIndex : 'username', sortable: true,
                    tooltip:"user Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Full.Name", width : 150,
                    dataIndex : 'name', sortable: true,
                    tooltip:"user Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Email", width : 150,
                    dataIndex : 'auth_email', sortable: true,
                    tooltip:"Authentication Email",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Laravel.ID", width : 120,
                    dataIndex : 'laravel_id', sortable: true,
                    tooltip:"Laravel.ID",
                    editor: new Ext.form.ComboBox(
                    {   store: this.UserDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'user_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.laravel_id = record.data.user_id;
                                    alfalah.permission.user.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if (value)
                        {   metaData.attr = "style = background-color:lime;"; } 
                        else { metaData.attr = "style = background-color:grey;"; };

                        result = record.data.laravel_username+ ' ['+value+']';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Department", width : 120,
                    dataIndex : 'dept_name', sortable: true,
                    tooltip:"Department.ID",
                    editor: new Ext.form.ComboBox(
                    {   store: this.DeptDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'display',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.company_id = record.data.company_id;
                                    combo.gridEditor.record.data.site_id = record.data.site_id;
                                    combo.gridEditor.record.data.dept_id = record.data.dept_id;
                                    alfalah.permission.user.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   if (value)
                        {   metaData.attr = "style = background-color:lime;"; } 
                        else { metaData.attr = "style = background-color:grey;"; };

                        result = value +'<BR>'+record.data.company_id+'-'+record.data.site_id;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    tooltip:"Menu Status",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["label", "key"],
                                               data : [["Active", "0"], ["Inactive", "1"]]
                                        }),
                            displayField:"label",
                            valueField:"key",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created', sortable: true,
                    tooltip:"user Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_', sortable: true,
                    tooltip:"user Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'user_id', type: 'integer'},
                {name: 'parent_user_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'object_id', type: 'integer'},
                {name: 'link', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'has_child', type: 'integer'},
                {name: 'level_degree', type: 'string'},
                {name: 'arrange_no', type: 'string'},
                {name: 'created', type: 'date'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'user_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'user_link',
                    cid: 'link',
                    fieldLabel: 'link',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'user_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ [''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_userSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.user_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/permission/2/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.permission.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Copy',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.Grid_copy,
                        scope : this
                    },
                    //{   text:'Edit',
                    //    tooltip:'Edit Record',
                    //    iconCls: 'silk-page-white-edit',
                    //    //handler : this.user_grid_
                    //},
                    {   id : tabId+'_userSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_userGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_userPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_userPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_userGridBBar').getPageData().activePage -1) * page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_userSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_userTab",
                jsId : tabId+"_userTab",
                title:  "User",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // user grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   user_id: "",
                    parent_user_id: "",
                    name: "New Name",
                    object_id : 0,
                    link: "New Link Url",
                    status: 0,
                    has_child : 0,
                    level_degree: 0,
                    arrange_no: 1,
                    created: "",
                    modified_date: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // user grid copy and make it a new record
        Grid_copy: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection)
            {   var data = this.Grid.getSelectionModel().selection.record.data;
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   id: "",
                        parent_user_id: data.parent_user_id,
                        name: data.name,
                        object_id : data.object_id,
                        link: data.link,
                        status: data.status,
                        has_child : data.has_child,
                        level_degree: data.level_degree,
                        arrange_no: data.arange_no
                    }));
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },
        // user grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/permission/2/1') }}",
                    {   'x-csrf-token': alfalah.permission.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // user grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/permission/2/2') }}",
                {   'x-csrf-token': alfalah.permission.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.user_id }
            );
        },
        // user search button
        user_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_userSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.permission.task,
                        act: alfalah.permission.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.permission.role = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'role_id', sortable: true,
                    tooltip:"Role ID"
                },
                {   header: "Parent.ID", width : 50,
                    dataIndex : 'parent_role_id', sortable: true,
                    tooltip:"Parent Role ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Name", width : 200,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Role Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    tooltip:"Role Status",
                    editor :  new Ext.Editor(  new Ext.form.ComboBox(
                                                {   store: new Ext.data.SimpleStore({
                                                                       fields: ["label", "key"],
                                                                       data : [["Active", "0"], ["Inactive", "1"]]
                                                                }),
                                                    displayField:"label",
                                                    valueField:"key",
                                                    mode: 'local',
                                                    typeAhead: true,
                                                    triggerAction: "all",
                                                    selectOnFocus:true,
                                                    forceSelection :true
                                               }),
                                                {autoSize:true}
                                            )
                },
                {   header: "Note", width : 200,
                    dataIndex : 'note', sortable: true,
                    tooltip:"Role Notes",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Role Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"Role Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'parent_role_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'note', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'role_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'role_note',
                    cid: 'note',
                    fieldLabel: 'note',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'role_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ [''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_roleSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.role_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/permission/3/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();

            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : alfalah.permission.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   id : tabId+'_roleSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Copy',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.MessageBox_copy,
                        scope : this
                    },
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_roleGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_rolePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_rolePageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_roleGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });

            },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_roleTab",
                jsId : tabId+"_roleTab",
                title:  "Role",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // role grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   id: 0,
                    parent_role_id: 0,
                    name: "New Name",
                    note: "New Note",
                    status: 0
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // role grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/permission/3/1') }}",
                    {   'x-csrf-token': alfalah.permission.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // role grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/permission/3/2') }}",
                {   'x-csrf-token': alfalah.permission.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.role_id  }
            );
        },
        // role message box copy
        MessageBox_copy: function(button, event)
        {   the_selection = this.Grid.getSelectionModel().selection;
            if (the_selection)
            {   Ext.MessageBox.prompt('Name', 'Please enter your name:', this.Grid_copy, this);
            } else
            {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Role Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });
            };
        },
        // role grid delete records
        Grid_copy: function(button, text)
        {   if (text)
            {   Ext.Ajax.request(
                {   method: 'POST',
                    url: 'saa/permission/menu/1/3',
                    params:
                    {   id: this.Grid.getSelectionModel().selection.record.data.id,
                        copyTo : text
                    },
                    success: function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.message+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   this.DataStore.reload();
                        };
                    },
                    failure: function()
                    { Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                  scope: this
                });
            };
        },
        // role search button
        role_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_roleSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.permission.task,
                        act: alfalah.permission.act,
                        a:3, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.permission.role_menu = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;
    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.permission.centerPanel.container.dom.clientHeight;
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'id', sortable: true,
                    tooltip:"role_menu ID"
                },
                {   header: "Role.ID", width : 50,
                    dataIndex : 'role_id', sortable: true,
                    tooltip:"Role ID"
                },
                {   header: "Role Name", //width : auto,
                    dataIndex : 'role_name', sortable: true,
                    tooltip:"Role Name"
                },
                {   header: "Menu.ID", width : 50,
                    dataIndex : 'menu_id', sortable: true,
                    tooltip:"Menu ID"
                },
                {   header: "Menu.Name", //width : auto,
                    dataIndex : 'menu_name', sortable: true,
                    tooltip:"Menu Name"
                },
                {   header: "Menu.Link", width : 150,
                    dataIndex : 'menu_link', sortable: true,
                    tooltip:"Menu Link"
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Role Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"Role Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'role_id', type: 'string'},
                {name: 'role_name', type: 'string'},
                {name: 'menu_id', type: 'string'},
                {name: 'menu_name', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'rm_role_name',
                    cid : 'role_name',
                    fieldLabel: 'Role.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'rm_menu_name',
                    cid : 'menu_name',
                    fieldLabel: 'Menu.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'rm_menu_link',
                    cid : 'menu_link',
                    fieldLabel: 'Menu.Link',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_role_menuSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.role_menu_search_handler,
                scope : this
            });
            /**
             * MAIN-GRID
            */
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/permission/4/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.RoleComboDataStore = alfalah.core.newDataStore(
                "{{ url('/permission/3/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.RoleComboDataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.DataStore.reload(); },
                        scope : this
                    },
                    //{   text:'Add',
                    //    tooltip:'Add Record',
                    //    iconCls: 'silk-add',
                    //    handler : this.Grid_add,
                    //    scope : this
                    //},
                    {   id : tabId+'_role_menuSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    '-','Selected Role ',
                    new Ext.form.ComboBox(
                    {   store: this.RoleComboDataStore,
                        typeAhead: true,
                        id: tabId+"_MenuRoleCombo",
                        width: 250,
                        displayField: 'name',
                        valueField: 'role_id',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        listeners : { scope : this,
                            'select' : function (a,b,c)
                            {   var role_value = Ext.getCmp(this.tabId+"_MenuRoleCombo").value;
                                this.DataStore.baseParams = {
                                    task   : alfalah.permission.task,
                                    act    : alfalah.permission.act,
                                    a      : 4, b:0, s:"combo",
                                    limit  : this.page_limit, start:this.page_start,
                                    role_id: role_value };
                                this.DataStore.reload();
                            }
                        }
                    }),
                    {   //text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.RoleComboDataStore.reload(); },
                        scope : this
                    },

                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_role_menuGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_role_menuPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_role_menuPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_role_menuGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });

            /**
             * SOUTH-GRID
            */
            this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/permission/1/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SouthDataStore.load();
            var cbSelModel = new Ext.grid.CheckboxSelectionModel();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.SouthDataStore,
                columns: [ cbSelModel,
                    {   header: "ID", width : 50,
                        dataIndex : 'menu_id', sortable: true,
                        tooltip:"Menu ID"
                    },
                    {   header: "Parent.ID", width : 50,
                        dataIndex : 'parent_menu_id', sortable: true,
                        tooltip:"Parent Menu ID",
                    },
                    {   header: "Name", width : 200,
                        dataIndex : 'name', sortable: true,
                        tooltip:"Menu Name",
                    },
                    {   header: "Link", width : 200,
                        dataIndex : 'link', sortable: true,
                        tooltip:"Menu Target Link",
                    },
                ],
                selModel: cbSelModel,
                //selModel: new Ext.grid.RowSelectionModel(),
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add Menu',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.SouthGrid_add,
                        scope : this
                    }
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_role_menuSouthGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_role_menuSouthGridPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_role_menuSouthGridPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_role_menuSouthGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_role_menuTab",
                jsId : tabId+"_role_menuTab",
                title:  "Role Menu",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'ToolBox',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //{   region: 'west',
                //    title: 'Roles',
                //    split: true,
                //    width: 200,
                //    minSize: 175,
                //    maxSize: 400,
                //    collapsible: true,
                //    margins: '0 0 0 5',
                //    items:[this.EastGrid]
                //},
                {// lazily created panel (xtype:'panel' is default)
                    region: 'south',
                    title: 'Menus',
                    split: true,
                    height : this.region_height/2,
                    minSize: this.region_height/4,
                    maxSize: this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    items:[this.SouthGrid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // role_menu grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/permission/4/1') }}",
                    {   'x-csrf-token': alfalah.permission.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // role_menu grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/permission/4/2') }}",
                {   'x-csrf-token': alfalah.permission.sid }, 
                {   role_id: this.Grid.getSelectionModel().selection.record.data.role_id,
                    menu_id: this.Grid.getSelectionModel().selection.record.data.menu_id
                }
            );
        },
        // south grid add new record
        SouthGrid_add: function(button, event)
        {   var the_role_id = Ext.getCmp(this.tabId+"_MenuRoleCombo").value;
            if (the_role_id)
            {   var the_records = this.SouthGrid.getSelectionModel().selections.items;
                Ext.each(the_records,
                    function(the_record)
                    {   this.Grid.store.insert( 0,
                        new this.Records (
                        {   id: "",
                            role_id: the_role_id,
                            role_name: "Assign by system",
                            menu_id: the_record.data.menu_id,
                            menu_name: the_record.data.name
                        }));
                    }, this
                );
            }
            else
            {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Role Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            };
        },
        // role_menu search button
        role_menu_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_role_menuSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.permission.task,
                        act: alfalah.permission.act,
                        a:4, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.permission.role_user = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;
    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.permission.centerPanel.container.dom.clientHeight;
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'id', sortable: true,
                    tooltip:"role_user ID"
                },
                {   header: "Role.ID", width : 50,
                    dataIndex : 'role_id', sortable: true,
                    tooltip:"Role ID"
                },
                {   header: "Role Name", //width : auto,
                    dataIndex : 'role_name', sortable: true,
                    tooltip:"Role Name"
                },
                {   header: "User.ID", width : 50,
                    dataIndex : 'user_id', sortable: true,
                    tooltip:"User ID"
                },
                {   header: "User.Name", //width : auto,
                    dataIndex : 'user_name', sortable: true,
                    tooltip:"User Name"
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Role Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"Role Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'role_id', type: 'string'},
                {name: 'role_name', type: 'string'},
                {name: 'user_id', type: 'string'},
                {name: 'user_name', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'ru_role_name',
                    cid : 'role_name',
                    fieldLabel: 'Role.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'ru_user_name',
                    cid : 'user_name',
                    fieldLabel: 'User.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_role_userSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.role_user_search_handler,
                scope : this
            });
            /**
             * MAIN-GRID
            */
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/permission/5/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.RoleComboDataStore = alfalah.core.newDataStore(
                "{{ url('/permission/3/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.RoleComboDataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.DataStore.reload(); },
                        scope : this
                    },
                    //{   text:'Add',
                    //    tooltip:'Add Record',
                    //    iconCls: 'silk-add',
                    //    handler : this.Grid_add,
                    //    scope : this
                    //},
                    {   id : tabId+'_role_userSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    '-','Selected Role ',
                    new Ext.form.ComboBox(
                    {   store: this.RoleComboDataStore,
                        typeAhead: true,
                        id: tabId+"_RoleUserCombo",
                        width: 250,
                        displayField: 'name',
                        valueField: 'role_id',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        listeners : { scope : this,
                            'select' : function (a,b,c)
                            {   var role_value = Ext.getCmp(this.tabId+"_RoleUserCombo").value;
                                this.DataStore.baseParams = {
                                    task   : alfalah.permission.task,
                                    act    : alfalah.permission.act,
                                    a      : 5, b:0, s:"combo",
                                    limit  : this.page_limit, start:this.page_start,
                                    role_id: role_value };
                                this.DataStore.reload();
                            }
                        }
                    }),
                    {   //text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.RoleComboDataStore.reload(); },
                        scope : this
                    },

                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_role_userGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_role_userPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_role_userPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_role_userGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });

            /**
             * SOUTH-GRID
            */
            this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/permission/2/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SouthDataStore.load();
            var cbSelModel = new Ext.grid.CheckboxSelectionModel();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   /*id : tabId+'_role_userSouthGrid',
                jsId : tabId+'_role_userSouthGrid',*/
                store:  this.SouthDataStore,
                columns: [ cbSelModel,
                    {   header: "User.ID", width : 50,
                        dataIndex : 'user_id', sortable: true,
                        tooltip:"User ID"
                    },
                    {   header: "Name", width : 200,
                        dataIndex : 'name', sortable: true,
                        tooltip:"Menu Name",
                    },
                    {   header: "Created", width : 150,
                        dataIndex : 'created', sortable: true,
                        tooltip:"User Created Date",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    },
                ],
                selModel: cbSelModel,
                //selModel: new Ext.grid.RowSelectionModel(),
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add User',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.SouthGrid_add,
                        scope : this
                    }
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_role_userSouthGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_role_userSouthGridPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_role_userSouthGridPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_role_userSouthGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_role_userTab",
                jsId : tabId+"_role_userTab",
                title:  "Role User",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'ToolBox',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //{   region: 'west',
                //    title: 'Roles',
                //    split: true,
                //    width: 200,
                //    minSize: 175,
                //    maxSize: 400,
                //    collapsible: true,
                //    margins: '0 0 0 5',
                //    items:[this.EastGrid]
                //},
                {// lazily created panel (xtype:'panel' is default)
                    region: 'south',
                    title: 'Users',
                    split: true,
                    height : this.region_height/2,
                    minSize: this.region_height/4,
                    maxSize: this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    items:[this.SouthGrid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // role_user grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/permission/5/1') }}",
                    {   'x-csrf-token': alfalah.permission.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // role_user grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/permission/5/2') }}",
                {   'x-csrf-token': alfalah.permission.sid }, 
                {   role_id: this.Grid.getSelectionModel().selection.record.data.role_id,
                    user_id: this.Grid.getSelectionModel().selection.record.data.user_id
                }
            );
        },
        // south grid add new record
        SouthGrid_add: function(button, event)
        {   var the_role_id = Ext.getCmp(this.tabId+"_RoleUserCombo").value;
            if (the_role_id)
            {   var the_records = this.SouthGrid.getSelectionModel().selections.items;
                Ext.each(the_records,
                    function(the_record)
                    {   this.Grid.store.insert( 0,
                        new this.Records (
                        {   id: "",
                            role_id: the_role_id,
                            role_name: "Assign by system",
                            user_id: the_record.data.user_id,
                            user_name: the_record.data.name
                        }));
                    }, this
                );
            }
            else
            {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Role Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            };

        },
        // role_user search button
        role_user_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_role_userSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.permission.task,
                        act: alfalah.permission.act,
                        a:5, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.permission.role_page = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;
    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.permission.centerPanel.container.dom.clientHeight;
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'id', sortable: true,
                    tooltip:"role_page ID"
                },
                {   header: "Role.ID", width : 50,
                    dataIndex : 'role_id', sortable: true,
                    tooltip:"Role ID"
                },
                {   header: "Role Name", //width : auto,
                    dataIndex : 'role_name', sortable: true,
                    tooltip:"Role Name"
                },
                {   header: "Page", width : 200,
                    dataIndex : 'pg', sortable: true,
                    tooltip:"Page"
                },
                {   header: "Event", //width : auto,
                    dataIndex : 'ev', sortable: true,
                    tooltip:"page Event"
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Role Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"Role Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'role_id', type: 'string'},
                {name: 'role_name', type: 'string'},
                {name: 'pg', type: 'string'},
                {name: 'ev', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'ru_role_name',
                    cid : 'role_name',
                    fieldLabel: 'Role.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'ru_page_name',
                    cid : 'page_name',
                    fieldLabel: 'page.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_role_pageSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.role_page_search_handler,
                scope : this
            });
            /**
             * MAIN-GRID
            */
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/permission/6/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.RoleComboDataStore = alfalah.core.newDataStore(
                "{{ url('/permission/3/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.RoleComboDataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.DataStore.reload(); },
                        scope : this
                    },
                    //{   text:'Add',
                    //    tooltip:'Add Record',
                    //    iconCls: 'silk-add',
                    //    handler : this.Grid_add,
                    //    scope : this
                    //},
                    {   id : tabId+'_role_pageSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    '-','Selected Role ',
                    new Ext.form.ComboBox(
                    {   store: this.RoleComboDataStore,
                        typeAhead: true,
                        id: tabId+"_RPCombo",
                        width: 250,
                        displayField: 'name',
                        valueField: 'role_id',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        listeners : { scope : this,
                            'select' : function (a,b,c)
                            {   var role_value = Ext.getCmp(this.tabId+"_RPCombo").value;
                                this.DataStore.baseParams = {
                                    task   : alfalah.permission.task,
                                    act    : alfalah.permission.act,
                                    a      : 6, b:0, s:"combo",
                                    limit  : this.page_limit, start:this.page_start,
                                    role_id: role_value };
                                this.DataStore.reload();
                            }
                        }
                    }),
                    {   //text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.RoleComboDataStore.reload(); },
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_role_pageGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_role_pagePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_role_pagePageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_role_pageGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });

            /**
             * SOUTH-GRID
            */
            this.SouthSearchs = [
                {   id: 'rps_event_name',
                    cid : 'event_name',
                    fieldLabel: 'Ev.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'rps_page_name',
                    cid : 'page_name',
                    fieldLabel: 'Pg.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SouthSearchBtn = new Ext.Button(
            {   id : tabId+"_role_pageSouthSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.role_page_south_search_handler,
                scope : this
            });
            this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/permission/0/10') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.SouthDataStore.load();
            var cbSelModel = new Ext.grid.CheckboxSelectionModel();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.SouthDataStore,
                columns: [ cbSelModel,
                    {   header: "Page", width : 100,
                        dataIndex : 'PG', sortable: true,
                        tooltip:"Pages"
                    },
                    {   header: "Event", width : 100,
                        dataIndex : 'EV', sortable: true,
                        tooltip:"Event",
                    },
                    {   header: "Handler", width : 350,
                        dataIndex : 'HANDLER_PAGE', sortable: true,
                        tooltip:"page Handler",
                    },
                ],
                selModel: cbSelModel,
                //selModel: new Ext.grid.RowSelectionModel(),
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add page',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.SouthGrid_add,
                        scope : this
                    }
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_RPSouthGridBBar',
                    store: this.SouthDataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_RPSouthGridPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_RPGridPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_RPGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SouthSearchBtn.handler.call(this.SouthSearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_role_pageTab",
                jsId : tabId+"_role_pageTab",
                title:  "Role page",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'S E AR C H',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    }]
                },
                {// lazily created panel (xtype:'panel' is default)
                    region: 'south',
                    title: 'pages',
                    split: true,
                    height : this.region_height/2,
                    // minSize: this.region_height/4,
                    // maxSize: this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    layout: 'border',
                    items:[
                    {   region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.SouthGrid]
                    },
                    {   region: 'east',     // position for region
                        title: 'S E A R C H',
                        split:true,
                        width: 200,
                        minSize: 175,
                        maxSize: 400,
                        collapsible: true,
                        layout : 'fit',
                        items:[
                        {   //title: 'S E A R C H',
                            labelWidth: 50,
                            defaultType: 'textfield',
                            xtype: 'form',
                            frame: true,
                            autoScroll : true,
                            items : [ this.SouthSearchs, this.SouthSearchBtn]
                        }]
                    }]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // role_page grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/permission/6/1') }}",
                    {   'x-csrf-token': alfalah.permission.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // role_page grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/permission/6/2') }}",
                {   'x-csrf-token': alfalah.permission.sid }, 
                {   role_id: this.Grid.getSelectionModel().selection.record.data.role_id,
                    pg: this.Grid.getSelectionModel().selection.record.data.pg,
                    ev: this.Grid.getSelectionModel().selection.record.data.ev
                }
            );
        },
        // south grid add new record
        SouthGrid_add: function(button, event)
        {   var the_role_id = Ext.getCmp(this.tabId+"_RPCombo").value;
            if (the_role_id)
            {   var the_records = this.SouthGrid.getSelectionModel().selections.items;
                Ext.each(the_records,
                    function(the_record)
                    {   this.Grid.store.insert( 0,
                        new this.Records (
                        {   id: "",
                            role_id: the_role_id,
                            role_name: "Assign by system",
                            pg: the_record.data.PG,
                            ev: the_record.data.EV
                        }));
                    }, this
                );
            }
            else
            {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Role Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            };

        },
        // role_page search button
        role_page_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_role_pageSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.permission.task,
                        act: alfalah.permission.act,
                        a:6, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
        // role_page_south search button
        role_page_south_search_handler : function(button, event)
        {   var the_search = true;

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.SouthSearchs);
                //console.log(this.SouthSearchs);
                //console.log(the_parameter);
                this.SouthDataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.permission.task,
                        act: alfalah.permission.act,
                        a:6, b:9, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.SouthDataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.permission.user_page = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;
    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.permission.centerPanel.container.dom.clientHeight;
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'id', sortable: true,
                    tooltip:"user_page ID"
                },
                {   header: "User.ID", width : 50,
                    dataIndex : 'user_id', sortable: true,
                    tooltip:"User ID"
                },
                {   header: "User Name", //width : auto,
                    dataIndex : 'username', sortable: true,
                    tooltip:"User Name"
                },
                {   header: "Role.ID", width : 50,
                    dataIndex : 'role_id', sortable: true,
                    tooltip:"Role ID"
                },
                {   header: "Role Name", //width : auto,
                    dataIndex : 'role_name', sortable: true,
                    tooltip:"Role Name"
                },
                {   header: "Page", width : 200,
                    dataIndex : 'pg', sortable: true,
                    tooltip:"Page"
                },
                {   header: "Event", //width : auto,
                    dataIndex : 'ev', sortable: true,
                    tooltip:"page Event"
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"user Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"user Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'user_id', type: 'string'},
                {name: 'user_name', type: 'string'},
                {name: 'role_id', type: 'string'},
                {name: 'role_name', type: 'string'},
                {name: 'pg', type: 'string'},
                {name: 'ev', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'up_user_name',
                    cid : 'user_name',
                    fieldLabel: 'user.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'up_page',
                    cid : 'page',
                    fieldLabel: 'Pg',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'up_event',
                    cid : 'event',
                    fieldLabel: 'Ev',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_user_pageSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.user_page_search_handler,
                scope : this
            });
            /**
             * MAIN-GRID
            */
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/permission/7/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.DataStore.reload(); },
                        scope : this
                    },
                    //{   text:'Add',
                    //    tooltip:'Add Record',
                    //    iconCls: 'silk-add',
                    //    handler : this.Grid_add,
                    //    scope : this
                    //},
                    {   id : tabId+'_user_pageSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_user_pageGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_user_pagePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_user_pagePageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_user_pageGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
            /**
             * SOUTH-GRID
            */
            this.SouthSearchs = [
                {   id: 'rp_south_event_name',
                    cid : 'event_name',
                    fieldLabel: 'Ev.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'rp_south_page_name',
                    cid : 'page_name',
                    fieldLabel: 'Pg.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SouthSearchBtn = new Ext.Button(
            {   id : tabId+"_user_pageSouthSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.user_page_south_search_handler,
                scope : this
            });
            this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/permission/6/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.SouthDataStore.load();
            this.RoleComboDataStore = alfalah.core.newDataStore(
                "{{ url('/permission/3/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.RoleComboDataStore.load();
            var cbSelModel = new Ext.grid.CheckboxSelectionModel();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   /*id : tabId+'_user_pageSouthGrid',
                jsId : tabId+'_user_pageSouthGrid',*/
                store:  this.SouthDataStore,
                columns: [ cbSelModel,
                    {   header: "Role.ID", width : 50,
                        dataIndex : 'role_id', sortable: true,
                        tooltip:"Role ID"
                    },
                    {   header: "Role Name", //width : auto,
                        dataIndex : 'role_name', sortable: true,
                        tooltip:"Role Name"
                    },
                    {   header: "Page", width : 200,
                        dataIndex : 'pg', sortable: true,
                        tooltip:"Page"
                    },
                    {   header: "Event", //width : auto,
                        dataIndex : 'ev', sortable: true,
                        tooltip:"page Event"
                    },
                    {   header: "Created", width : 150,
                        dataIndex : 'created_date', sortable: true,
                        tooltip:"Role Created Date",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    }
                ],
                selModel: cbSelModel,
                //selModel: new Ext.grid.RowSelectionModel(),
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add page',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.SouthGrid_add,
                        scope : this
                    },
                    '-','Selected Role ',
                    new Ext.form.ComboBox(
                    {   store: this.RoleComboDataStore,
                        typeAhead: true,
                        id: tabId+"_RolepageCombo",
                        width: 250,
                        displayField: 'name',
                        valueField: 'role_id',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        listeners : { scope : this,
                            'select' : function (a,b,c)
                            {   var role_value = Ext.getCmp(this.tabId+"_RolepageCombo").value;
                                this.SouthDataStore.baseParams = {
                                    task   : alfalah.permission.task,
                                    act    : alfalah.permission.act,
                                    a      : 6, b:0, s:"combo",
                                    limit  : this.page_limit, start:this.page_start,
                                    role_id: role_value };
                                this.SouthDataStore.reload();
                            }
                        }
                    }),
                    {   //text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.RoleComboDataStore.reload(); },
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_user_pageSouthGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_user_pageSouthGridPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_user_pageSouthGridPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_user_pageSouthGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_user_pageTab",
                jsId : tabId+"_user_pageTab",
                title:  "User Page",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'S E A R C H',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    }]
                },
                {// lazily created panel (xtype:'panel' is default)
                    region: 'south',
                    title: 'pages',
                    split: true,
                    height : this.region_height/2,
                    // minSize: this.region_height/4,
                    // maxSize: this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    layout: 'border',
                    items:[
                    {   region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.SouthGrid]
                    },
                    {   region: 'east',     // position for region
                        title: 'S E A R C H',
                        split:true,
                        width: 200,
                        minSize: 175,
                        maxSize: 400,
                        collapsible: true,
                        layout : 'fit',
                        items:[
                        {   //title: 'S E A R C H',
                            labelWidth: 50,
                            defaultType: 'textfield',
                            xtype: 'form',
                            frame: true,
                            autoScroll : true,
                            items : [ this.SouthSearchs, this.SouthSearchBtn]
                        }]
                    }]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // user_page grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/permission/7/1') }}",
                    {   'x-csrf-token': alfalah.permission.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // user_page grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            var json_data = alfalah.core.getDetailData(the_records);
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/permission/7/2') }}",
                {   'x-csrf-token': alfalah.permission.sid }, 
                {   user_id: this.Grid.getSelectionModel().selection.record.data.user_id,
                    pg: this.Grid.getSelectionModel().selection.record.data.pg,
                    ev: this.Grid.getSelectionModel().selection.record.data.ev
                }
            );
        },
        // south grid add new record
        SouthGrid_add: function(button, event)
        {   //var the_user_id = Ext.getCmp(this.tabId+"_userpageCombo").value;
            var the_user_id = this.Grid.getSelectionModel().selection.record.data.user_id;
            if (the_user_id)
            {   var the_records = this.SouthGrid.getSelectionModel().selections.items;
                Ext.each(the_records,
                    function(the_record)
                    {   this.Grid.store.insert( 0,
                        new this.Records (
                        {   id: "",
                            user_id: the_user_id,
                            username: "Assign by system",
                            role_id : the_record.data.role_id,
                            role_name : the_record.data.role_name,
                            pg: the_record.data.pg,
                            ev: the_record.data.ev
                        }));
                    }, this
                );
            }
            else
            {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No user Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            };

        },
        // user_page search button
        user_page_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_user_pageSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.permission.task,
                        act: alfalah.permission.act,
                        a:7, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
        // user_page_south search button
        user_page_south_search_handler : function(button, event)
        {   var the_search = true;
            var the_role_id = Ext.getCmp(this.tabId+"_RolepageCombo").value;

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.SouthSearchs);
                this.SouthDataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.permission.task,
                        act: alfalah.permission.act,
                        a:6, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start,
                        rid : the_role_id });
                this.SouthDataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
//Ext.reg('project', alfalah.project);
Ext.onReady(alfalah.permission.initialize, alfalah.permission);
// end of file
</script>
<div>&nbsp;</div>