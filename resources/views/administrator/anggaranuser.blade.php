<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.anggaranuser');

// create application
alfalah.anggaranuser = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',
        // public methods
        initialize: function()
        {  console.log("tabId");
            console.log(tabId);
             this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            console.log("centerPanel");
            console.log(this.centerPanel);
            this.anggaranuser.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.anggaranuser.Tab);
            this.centerPanel.setActiveTab(this.anggaranuser.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.anggaranuser.anggaranuser = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;
    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.anggaranuser.centerPanel.container.dom.clientHeight;
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'id', sortable: true,
                    tooltip:"ID"
                },
                {   header: "Role.ID", width : 50,
                    dataIndex : 'mrapbs_id', sortable: true,
                    tooltip:"Anggaran ID"
                },
                {   header: "Role Name", //width : auto,
                    dataIndex : 'note', sortable: true,
                    tooltip:"Role Name"
                },
                {   header: "User.ID", width : 50,
                    dataIndex : 'user_id', sortable: true,
                    tooltip:"User ID"
                },
                {   header: "User.Name", //width : auto,
                    dataIndex : 'user_name', sortable: true,
                    tooltip:"User Name"
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"Role Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"Role Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'mrapbs_id', type: 'string'},
                {name: 'note', type: 'string'},
                {name: 'user_id', type: 'string'},
                {name: 'user_name', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'ru_role_name',
                    cid : 'role_name',
                    fieldLabel: 'Role.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'ru_user_name',
                    cid : 'user_name',
                    fieldLabel: 'User.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_anggaranuserSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.anggaranuser_search_handler,
                scope : this
            });
            /**
             * MAIN-GRID
            */
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/anggaranuser/1/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.RoleComboDataStore = alfalah.core.newDataStore(
                "{{ url('/mrapbs/1/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start, type :"1" }
            );
            this.RoleComboDataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.DataStore.reload(); },
                        scope : this
                    },
                    //{   text:'Add',
                    //    tooltip:'Add Record',
                    //    iconCls: 'silk-add',
                    //    handler : this.Grid_add,
                    //    scope : this
                    //},
                    {   id : tabId+'_anggaranuserSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    '-','Selected Role ',
                    new Ext.form.ComboBox(
                    {   store: this.RoleComboDataStore,
                        typeAhead: true,
                        id: tabId+"_AnggaranUserCombo",
                        width: 250,
                        displayField: 'name',
                        valueField: 'mrapbs_id',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        listeners : { scope : this,
                            'select' : function (a,b,c)
                            {   var role_value = Ext.getCmp(this.tabId+"_AnggaranUserCombo").value;
                                this.DataStore.baseParams = {
                                    task   : alfalah.anggaranuser.task,
                                    act    : alfalah.anggaranuser.act,
                                    s:"combo",
                                    limit  : this.page_limit, start:this.page_start,
                                    mrapbs_id: role_value };
                                this.DataStore.reload();
                            }
                        }
                    }),
                    {   //text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.RoleComboDataStore.reload(); },
                        scope : this
                    },

                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_anggaranuserGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_anggaranuserPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_anggaranuserPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_anggaranuserGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });

            /**
             * SOUTH-GRID
            */
            this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/permission/2/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.SouthDataStore.load();
            var cbSelModel = new Ext.grid.CheckboxSelectionModel();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   /*id : tabId+'_anggaranuserSouthGrid',
                jsId : tabId+'_anggaranuserSouthGrid',*/
                store:  this.SouthDataStore,
                columns: [ cbSelModel,
                    {   header: "User.ID", width : 50,
                        dataIndex : 'user_id', sortable: true,
                        tooltip:"User ID"
                    },
                    {   header: "Name", width : 200,
                        dataIndex : 'name', sortable: true,
                        tooltip:"Menu Name",
                    },
                    {   header: "Created", width : 150,
                        dataIndex : 'created', sortable: true,
                        tooltip:"User Created Date",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    },
                ],
                selModel: cbSelModel,
                //selModel: new Ext.grid.RowSelectionModel(),
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add User',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.SouthGrid_add,
                        scope : this
                    }
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_anggaranuserSouthGridBBar',
                    store: this.SouthDataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_anggaranuserSouthGridPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   page_limit = Ext.get(tabId+'_anggaranuserSouthGridPageCombo').getValue();
                                    page_start = ( Ext.getCmp(tabId+'_anggaranuserSouthGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_anggaranuserTab",
                jsId : tabId+"_anggaranuserTab",
                title:  "Role User Anggaran",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'ToolBox',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //{   region: 'west',
                //    title: 'Roles',
                //    split: true,
                //    width: 200,
                //    minSize: 175,
                //    maxSize: 400,
                //    collapsible: true,
                //    margins: '0 0 0 5',
                //    items:[this.EastGrid]
                //},
                {// lazily created panel (xtype:'panel' is default)
                    region: 'south',
                    title: 'Users',
                    split: true,
                    height : this.region_height/2,
                    minSize: this.region_height/4,
                    maxSize: this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    items:[this.SouthGrid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // anggaranuser grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/anggaranuser/1/1') }}",
                    {   'x-csrf-token': alfalah.anggaranuser.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // anggaranuser grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/anggaranuser/1/2') }}",
                {   'x-csrf-token': alfalah.anggaranuser.sid }, 
                {   mrapbs_id: this.Grid.getSelectionModel().selection.record.data.mrapbs_id,
                    user_id: this.Grid.getSelectionModel().selection.record.data.user_id
                }
            );
        },
        // south grid add new record
        SouthGrid_add: function(button, event)
        {   var the_role_id = Ext.getCmp(this.tabId+"_AnggaranUserCombo").value;
            if (the_role_id)
            {   var the_records = this.SouthGrid.getSelectionModel().selections.items;
                Ext.each(the_records,
                    function(the_record)
                    {   this.Grid.store.insert( 0,
                        new this.Records (
                        {   id: "",
                            mrapbs_id: the_role_id,
                            role_name: "Assign by system",
                            user_id: the_record.data.user_id,
                            user_name: the_record.data.name
                        }));
                    }, this
                );
            }
            else
            {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Role Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            };

        },
        // anggaranuser search button
        anggaranuser_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_anggaranuserSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.anggaranuser.task,
                        act: alfalah.anggaranuser.act,
                        s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
//Ext.reg('project', alfalah.project);
Ext.onReady(alfalah.anggaranuser.initialize, alfalah.anggaranuser);
// end of file
</script>
<div>&nbsp;</div>