<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.school');

// create application
alfalah.school = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.periode.initialize();
            this.ppdb.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            // this.centerPanel.add(this.description.Tab);
            this.centerPanel.add(this.periode.Tab);
            this.centerPanel.add(this.ppdb.Tab);
            this.centerPanel.setActiveTab(this.periode.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.school.periode= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Name", width : 200,
                    dataIndex : 'param_name', sortable: true,
                    tooltip:"periode Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Value", width : 150,
                    dataIndex : 'param_value', sortable: true,
                    tooltip:"periode Value",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Description", width : 200,
                    dataIndex : 'description', sortable: true,
                    tooltip:"periode Description",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"periode Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_', sortable: true,
                    tooltip:"periode Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'param_name', type: 'integer'},
                {name: 'param_value', type: 'string'},
                {name: 'description', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'periode_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'periode_value',
                    cid: 'value',
                    fieldLabel: 'Value',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'periode_description',
                    cid: 'description',
                    fieldLabel: 'Description',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_periodeSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.periode_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/periode/1/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.school.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Copy',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.Grid_copy,
                        scope : this
                    },
                    //{   text:'Edit',
                    //    tooltip:'Edit Record',
                    //    iconCls: 'silk-page-white-edit',
                    //    //handler : this.periode_grid_
                    //},
                    {   id : tabId+'_periodeSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_periodeGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_periodePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_periodePageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_periodeGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_periodeSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_periodeTab",
                jsId : tabId+"_periodeTab",
                title:  "Periode",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // periode grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   param_name: "New Name",
                    param_value: "New Value",
                    description: "New Description",
                    created_date: "",
                    modified_date: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 0);
        },
        // periode grid copy and make it a new record
        Grid_copy: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection)
            {   var data = this.Grid.getSelectionModel().selection.record.data;
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   id: "",
                        parent_param_name: data.parent_param_name,
                        name: data.name,
                        object_id : data.object_id,
                        link: data.link,
                        status: data.status,
                        has_child : data.has_child,
                        level_degree: data.level_degree,
                        arrange_no: data.arange_no
                    }));
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },
        // periode grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/periode/1/1') }}",
                    {   'x-csrf-token': alfalah.school.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // periode grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/periode/1/2') }}",
                {   'x-csrf-token': alfalah.school.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.param_name }
            );
        },
        // periode search button
        periode_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_periodeSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_periode = alfalah.core.getSearchperiode(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_periode,
                    {   task: alfalah.school.task,
                        act: alfalah.school.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.school.ppdb = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.big_value;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/parameter/1/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start,
                    param_name: "ppdb_syarat",
                }
            );
            // this.DataStore.load();
            this.LoginForm = new Ext.FormPanel(
            {   bodyStyle:'padding:5px',
                tbar: [
                    {   id : tabId+'_periodeSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        scope : this
                    },
                    '-',
                ],
                items: [ 
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.9,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'ppdb_param_name', 
                            xtype:'textfield',
                            fieldLabel: 'Name',
                            name: 'param_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden:true,
                            value : 'ppdb_syarat'
                        },
                        {   id : 'ppdb_created_date', 
                            xtype:'textfield',
                            fieldLabel: 'Name',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden:true,
                            
                        },
                        {   id : 'ppdb_param_value', 
                            xtype:'textfield',
                            fieldLabel: 'Value',
                            name: 'param_value',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden:true,
                            
                        },
                        {   id : 'ppdb_description', 
                            xtype:'textfield',
                            fieldLabel: 'Desc',
                            name: 'description',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden:true,
                        },
                        {   id : 'ppdb_param_big_value', 
                            xtype:'htmleditor',
                            fieldLabel: '<strong><big> Persyaratan PPDB </big></strong>',
                            name: 'param_big_value',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : false,
                            height : 500,
                            // renderTo: "simpan_content",
                        }]
                    }]
                }]
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_ppdbTab",
                jsId : tabId+"_ppdbTab",
                title:  "PPDB",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.LoginForm]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
            this.DataStore.on( 'load', function( store, records, options )
            {   //check the fields
                Ext.each(records[0].store.fields.items,
                    function(the_field)
                    {   console.log(the_field.name);
                        switch (the_field.name)
                        {   case "created_date" :
                            {
                                Ext.getCmp('ppdb_'+the_field.name).setValue(alfalah.core.shortdateRenderer(records[0].data[the_field.name],'Y-m-d', 'd/m/Y'));
                            };
                            break;
                            default :
                            {
                                Ext.getCmp('ppdb_'+the_field.name).setValue(records[0].data[the_field.name]);
                            };
                            break;
                        };
                    });
            });
            this.DataStore.load();
        },
        Form_save : function(button, event)
        {   // header validation
            if (alfalah.core.validateFields(['ppdb_param_name', 'ppdb_param_value','ppdb_param_big_value', 'ppdb_created_date']))
            {   
                Ext.Ajax.request(
                {   method: 'POST',
                    url: "{{ url('/school/2/1') }}",
                    headers: {   'x-csrf-token': alfalah.school.sid }, 
                    params: {   
                        param_name: Ext.getCmp('ppdb_param_name').getValue(),
                        param_value: Ext.getCmp('ppdb_param_value').getValue(),
                        param_big_value: Ext.getCmp('ppdb_param_big_value').getValue(),
                        created_date: Ext.getCmp('ppdb_created_date').getValue()
                    },
                    success : function(response, options)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   alfalah.school.ppdb.DataStore.reload();
                        };
                    },
                    failure: function(response, options)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'S E R V E R    E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        };
                    },
                });
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.school.initialize, alfalah.school);
// end of file
</script>
<div>
    <div id="simpan_content">
       
    </div>
</div>