<script type="text/javascript">
    // create namespace
    Ext.namespace('alfalah.mcoa');
    
    // create application
    alfalah.mcoa = function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.tabId = '{{ $TABID }}';
        // private functions
        // public space
        return {
            centerPanel : 0,
            sid : '{{ csrf_token() }}',
            task : ' ',
            act : ' ',
            // public methods
            initialize: function()
            {   this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {   this.centerPanel = Ext.getCmp(tabId);
                this.mcoa.initialize();
                this.income.initialize();
                this.posbelanja.initialize();
                this.cash_bank_type.initialize();
            },
            // build the layout
            build_layout: function()
            {   this.centerPanel.beginUpdate();
                this.centerPanel.add(this.mcoa.Tab);
                this.centerPanel.setActiveTab(this.mcoa.Tab);
                this.centerPanel.add(this.income.Tab);
                this.centerPanel.add(this.posbelanja.Tab);
                this.centerPanel.add(this.cash_bank_type.Tab);
                this.centerPanel.endUpdate();
                alfalah.core.viewport.doLayout();
            },
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            {   console.log(4); },
        }; // end of public space
    }(); // end of app
    // create application
    alfalah.mcoa.mcoa= function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.Tab;
        this.Grid;
        this.Columns;
        this.Records;
        this.DataStore;
        this.Searchs;
        this.SearchBtn;
        // private functions
        // public space
        return {
            // execute at the very last time
            // public variable
            page_limit : 75,
            page_start : 0,
            // public methods
            initialize: function()
            {   this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {   this.SumberdanaDS = alfalah.core.newDataStore(
                "{{ url('/mrapbs/2/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start}
                );
                this.SumberdanaDS.load();

                this.Columns = [
                    {   header: "ID", width : 50,
                        dataIndex : 'mcoa_id', sortable: true,
                        tooltip:"mcoa ID",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Parent.ID", width : 100,
                        dataIndex : 'parent_mcoa_id', sortable: true,
                        tooltip:"Parent mcoa ID",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Name", width: 200,
                        dataIndex : 'name', sortable: true,
                        tooltip:"Name",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Name print", width: 200,
                        dataIndex : 'name_print', sortable: true,
                        hidden : true,
                        tooltip:"Name print",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Name Old", width: 200,
                        dataIndex : 'name_old', sortable: true,
                        hidden : true,
                        tooltip:"Name old",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Value", width : 50,
                        dataIndex : 'need_value', sortable: true,
                        tooltip:"Header / detail",
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                   fields: ["label", "key"],
                                                   data : [["Header", "0"], ["detail", "1"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },
                    {   header: "D/K", width : 50,
                        dataIndex : 'd_k', sortable: true,
                        tooltip:"Debet / Kredit",
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                   fields: ["label", "key"],
                                                   data : [["Debet", "0"], ["Kredit", "1"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },

                    {   header: "Type", width : 100,
                        dataIndex : 'type', sortable: true,
                        tooltip:"mcoa Type",
                        // editor : new Ext.form.TextField({allowBlank: false}),
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                   fields: ["label", "key"],
                                                   data : [["Asset", "0"], ["Kewajiban", "1"], ["Ekuitas Dana", "2"],["Pendapatan", "3"], ["Belanja", "4"], ["Pembiayaan", "5"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },
                    {   header: "Sumber Dana", width : 100,
                        dataIndex : 'sumberdana_id', sortable: true, 
                        tooltip:"jenis sumbangan",
                        editor : new Ext.form.ComboBox({
                        xtype : 'combo',
                        allowBlank: true,
                        store : this.SumberdanaDS,
                        displayField: 'name',
                        valueField: 'sumberdana_id',
                        mode : 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus:true,
                        editable: true,
                        typeAhead: true,
                        width : 100,
                        value: ''
                        }),
                    },
                    {   header: "Status", width : 50,
                        dataIndex : 'status', sortable: true,
                        tooltip:"mcoa Status",
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                   fields: ["label", "key"],
                                                   data : [["Active", "0"], ["Inactive", "1"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },
                    {   header: "Created", width : 150,
                        dataIndex : 'created_date', sortable: true,
                        tooltip:"mcoa Created Date",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    },
                    {   header: "Updated", width : 150,
                        dataIndex : 'modified_date', sortable: true,
                        tooltip:"mcoa Last Updated",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    }
                ];
                this.Records = Ext.data.Record.create(
                [   {name: 'mcoa_id', type: 'string'},
                    {name: 'parent_mcoa_id', type: 'string'},
                    {name: 'name', type: 'string'},
                    {name: 'name_print', type: 'string'},
                    {name: 'name_old', type: 'string'},
                    {name: 'need_value', type: 'integer'},
                    {name: 'd_k', type: 'integer'},
                    {name: 'type', type: 'integer'},
                    {name: 'status', type: 'string'},
                    {name: 'created_date', type: 'date'},
                    {name: 'modified_date', type: 'date'},
                ]);
                this.Searchs = [
                    {   id: 'mcoa_name',
                        cid: 'name',
                        fieldLabel: 'Name',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'mcoa_type',
                        cid: 'type',
                        fieldLabel: 'Type',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'mcoa_status',
                        cid: 'status',
                        fieldLabel: 'Status',
                        labelSeparator : '',
                        xtype : 'combo',
                        store : new Ext.data.SimpleStore(
                        {   fields: ['status'],
                            data : [ [''], ['Active'], ['Inactive']]
                        }),
                        displayField:'status',
                        valueField :'status',
                        mode : 'local',
                        triggerAction: 'all',
                        selectOnFocus:true,
                        editable: false,
                        width : 100,
                        value: ''
                    },
                ];
                this.SearchBtn = new Ext.Button(
                {   id : tabId+"_mcoaSearchBtn",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.mcoa_search_handler,
                    scope : this
                });
                this.DataStore = alfalah.core.newDataStore(
                    "{{url('/mcoa/1/0')}}", false,
                    {   s:"init", limit:this.page_limit, start:this.page_start }
                );
                // this.DataStore.load();
                this.Grid = new Ext.grid.EditorGridPanel(
                {   store:  this.DataStore,
                    columns: this.Columns,
                    //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                    enableColLock: false,
                    //trackMouseOver: true,
                    loadMask: true,
                    height : alfalah.mcoa.centerPanel.container.dom.clientHeight-50,
                    anchor: '100%',
                    autoScroll  : true,
                    frame: true,
                    //stripeRows : true,
                    // inline buttons
                    //buttons: [{text:'Save'},{text:'Cancel'}],
                    //buttonAlign:'center',
                    tbar: [
                        {   text:'Add',
                            tooltip:'Add Record',
                            iconCls: 'silk-add',
                            handler : this.Grid_add,
                            scope : this
                        },
                        {   text:'Copy',
                            tooltip:'Copy Record',
                            iconCls: 'silk-page-copy',
                            handler : this.Grid_copy,
                            scope : this
                        },
                        //{   text:'Edit',
                        //    tooltip:'Edit Record',
                        //    iconCls: 'silk-page-white-edit',
                        //    //handler : this.mcoa_grid_
                        //},
                        {   id : tabId+'_mcoaSaveBtn',
                            text:'Save',
                            tooltip:'Save Record',
                            iconCls: 'icon-save',
                            handler : this.Grid_save,
                            scope : this
                        },
                        '-',
                        {   text:'Delete',
                            tooltip:'Delete Record',
                            iconCls: 'silk-delete',
                            handler : this.Grid_remove,
                            scope : this
                        },
                        '-',
                        {   print_type : "pdf",
                            text:'Print PDF',
                            tooltip:'Print to PDF',
                            iconCls: 'silk-page-white-acrobat',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },
                        {   print_type : "xls",
                            text:'Print Excell',
                            tooltip:'Print to Excell SpreadSheet',
                            iconCls: 'silk-page-white-excel',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },'-',
                    ],
                    bbar: new Ext.PagingToolbar(
                    {   id : tabId+'_mcoaGridBBar',
                        store: this.DataStore,
                        pageSize: this.page_limit,
                        displayInfo: true,
                        emptyMsg: 'No data found',
                        items : [
                            '-',
                            'Displayed : ',
                            new Ext.form.ComboBox(
                            {   id : tabId+'_mcoaPageCombo',
                                store: new Ext.data.SimpleStore(
                                            {   fields: ['value'],
                                                data : [[50],[75],[100],[125],[150]]
                                            }),
                                displayField:'value',
                                valueField :'value',
                                value : 75,
                                editable: false,
                                mode: 'local',
                                triggerAction: 'all',
                                selectOnFocus:true,
                                hiddenName: 'pagesize',
                                width:50,
                                listeners : { scope : this,
                                    'select' : function (a,b,c)
                                    {   page_limit = Ext.get(tabId+'_mcoaPageCombo').getValue();
                                        page_start = ( Ext.getCmp(tabId+'_mcoaGridBBar').getPageData().activePage -1) * page_limit;
                                        this.SearchBtn.handler.call(this.SearchBtn.scope);
                                        //Ext.getCmp(tabId+"_mcoaSearchBtn").handler.call();
                                        //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                    }
                                }
                            }),
                            ' records at a time'
                        ]
                    }),
                });
            },
            // build the layout
            build_layout: function()
            {   this.Tab = new Ext.Panel(
                {   id : tabId+"_mcoaTab",
                    jsId : tabId+"_mcoaTab",
                    title:  "COA",
                    region: 'center',
                    layout: 'border',
                    items: [
                    {   title: 'ToolBox',
                        region: 'east',     // position for region
                        split:true,
                        width: 200,
                        minSize: 200,
                        maxSize: 400,
                        collapsible: true,
                        layout : 'accordion',
                        items:[
                        {   title: 'S E A R C H',
                            labelWidth: 50,
                            defaultType: 'textfield',
                            xtype: 'form',
                            frame: true,
                            autoScroll : true,
                            items : [ this.Searchs, this.SearchBtn]
                        },
                        { title : 'Setting'
                        }]
                    },
                    //new Ext.TabPanel(
                    //    { id:'center-panel',
                    //      region:'center'
                    //    })
                    {   region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.Grid]
                    }
                    ]
                });
            },
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            {},
            // mcoa grid add new record
            Grid_add: function(button, event)
            {   this.Grid.stopEditing();
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   mcoa_id: "",
                        parent_mcoa_id: "",
                        name: "New Name",
                        name_print: "",
                        name_old: "",
                        need_value: 0,
                        d_k: 0,
                        type: 0,
                        status: 0,
                        created_date: "",
                        modified_date: ""
                    }));
                // placed the edit cursor on third-column
                this.Grid.startEditing(0, 2);
            },
            // mcoa grid copy and make it a new record
            Grid_copy: function(button, event)
            {   this.Grid.stopEditing();
                var selection = this.Grid.getSelectionModel().selection;
                if (selection)
                {   var data = this.Grid.getSelectionModel().selection.record.data;
                    this.Grid.store.insert( 0,
                        new this.Records (
                        {   id: "",
                            parent_mcoa_id: data.parent_mcoa_id,
                            name: data.name,
                            type: data.type,
                            description: data.description,
                            status: data.status
                        }));
                    this.Grid.startEditing(0, 2);
                }
                else
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Selected Record ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                         });
                };
            },
            // mcoa grid save records
            Grid_save : function(button, event)
            {   var the_records = this.DataStore.getModifiedRecords();
                // check data modification
                if ( the_records.length == 0 )
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Data modified ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                         });
                } else
                {   var json_data = alfalah.core.getDetailData(the_records);
                    alfalah.core.submitGrid(
                        this.DataStore, 
                        "{{url('/mcoa/1/1')}}",
                        {   'x-csrf-token': alfalah.mcoa.sid }, 
                        {   json: Ext.encode(json_data) }
                    );
                };
            },
            // mcoa grid delete records
            Grid_remove: function(button, event)
            {   this.Grid.stopEditing();
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{url('/mcoa/1/2')}}",
                    {   'x-csrf-token': alfalah.mcoa.sid }, 
                    {   id: this.Grid.getSelectionModel().selection.record.data.mcoa_id }
                );
            },
            // mcoa search button
            mcoa_search_handler : function(button, event)
            {   var the_search = true;
                if ( this.DataStore.getModifiedRecords().length > 0 )
                {   Ext.Msg.show(
                        {   title:'W A R N I N G ',
                            msg: ' Modified Data Found, Do you want to save it before search process ? ',
                            buttons: Ext.Msg.YESNO,
                            fn: function(buttonId, text)
                                {   if (buttonId =='yes')
                                    {   Ext.getCmp(tabId+'_mcoaSaveBtn').handler.call();
                                        the_search = false;
                                    } else the_search = true;
                                },
                            icon: Ext.MessageBox.WARNING
                         });
                };
    
                if (the_search == true) // no modification records flag then we can go to search
                {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                    this.DataStore.baseParams = Ext.apply( the_parameter, {s:"form", limit:this.page_limit, start:this.page_start});
                    this.DataStore.reload();
                };
            },
        }; // end of public space
    }(); // end of app
alfalah.mcoa.posbelanja= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing

        prepare_component: function()
        { 

            this.coaDS_2 = alfalah.core.newDataStore(
                "{{ url('/mcoa/2/9') }}", true,
                {   s:"form", limit:this.page_limit, start:this.page_start,type : "5", need_value : 1 }
            );
            this.coaDS_2.load();

            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'posbelanja_id', sortable: true,
                    tooltip:"posbelanja ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "posbelanja.Name", width : 150,
                    dataIndex : 'posbelanja_name', sortable: true,
                    tooltip:"posbelanja Name",
                    editor : new Ext.form.TextField({allowBlank: false}), 
                },
                {   header: "Kode Rekening", width : 100,
                    dataIndex : 'coa_id', sortable: true,
                    tooltip:"Nomor rekening",
                    readonly :true,
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Nama Rekening", width : 200,
                    dataIndex : 'coa_name', sortable: true,
                    tooltip:"Account Name",
                    editor: new Ext.form.ComboBox(
                    {   store: this.coaDS_2,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'coa_name',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.coa_id = record.data.mcoa_id;
                                    combo.gridEditor.record.data.coa_name = record.data.coa_name;
                                    alfalah.mcoa.posbelanja.Grid.getView().refresh();
                                },
                        }
                    }),
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    tooltip:"currency Status",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["label", "key"],
                                               data : [["Active", "0"], ["Inactive", "1"]]
                                        }),
                            displayField:"label",
                            valueField:"key",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"posbelanja Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"posbelanja Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'posbelanja_id', type: 'string'},
                {name: 'posbelanja_name', type: 'string'},
                {name: 'coa_id', type: 'string'},
                {name: 'coa_name', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'posbelanja_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'posbelanja_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ [''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_posbelanjaSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.posbelanja_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/mcoa/2/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.mcoa.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Copy',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.Grid_copy,
                        scope : this
                    },
                    //{   text:'Edit',
                    //    tooltip:'Edit Record',
                    //    iconCls: 'silk-page-white-edit',
                    //    //handler : this.posbelanja_grid_
                    //},
                    {   id : tabId+'_posbelanjaSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_posbelanjaGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_posbelanjaPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_posbelanjaPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_posbelanjaGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_posbelanjaSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_posbelanjaTab",
                jsId : tabId+"_posbelanjaTab",
                title:  "COST TYPE",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // posbelanja grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   posbelanja_id: "",
                    posbelanja_name: "New Name",
                    coa_id: " ",
                    coa_name: " ",
                    status: 0,
                    created_date: "",
                    modified_date: "",
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // posbelanja grid copy and make it a new record
        Grid_copy: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection)
            {   var data = this.Grid.getSelectionModel().selection.record.data;
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   id: "",
                        name: data.posbelanja_name,
                        object_id : data.object_id,
                        link: data.link,
                        status: data.status,
                        has_child : data.has_child,
                        level_degree: data.level_degree,
                        arrange_no: data.arange_no
                    }));
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },
        // posbelanja grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/mcoa/2/1') }}",
                    {   'x-csrf-token': alfalah.mcoa.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // posbelanja grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/posbelanja/2/2') }}",
                {   'x-csrf-token': alfalah.posbelanja.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.posbelanja_id }
            );
        },
        // posbelanja search button
        posbelanja_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_posbelanjaSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {  s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.mcoa.income= function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.Tab;
        this.Grid;
        this.Columns;
        this.Records;
        this.DataStore;
        this.Searchs;
        this.SearchBtn;
        // private functions
        // public space
        return {
            // execute at the very last time
            // public variable
            page_limit : 75,
            page_start : 0,
            // public methods
            initialize: function()
            {   this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {  
                this.SumberdanaDS = alfalah.core.newDataStore(
                "{{ url('/mrapbs/2/0') }}", false,
                {   s:"form", limit:this.page_limit, start:this.page_start}
                );
                this.SumberdanaDS.load();

                this.Columns = [
                    {   header: "ID", width : 50,
                        dataIndex : 'mincome_id', sortable: true,
                        tooltip:"income ID",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Parent.ID", width : 100,
                        dataIndex : 'parent_mincome_id', sortable: true,
                        tooltip:"Parent income ID",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Description", width : 200,
                        dataIndex : 'mincome_name', sortable: true,
                        tooltip:"nama jenis pendapatan",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Value", width : 50,
                        dataIndex : 'mincome_value', sortable: true,
                        tooltip:"Header / detail",
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                   fields: ["label", "key"],
                                                   data : [["Header", "0"], ["detail", "1"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },
                    {   header: "Sumber Dana", width : 100,
                        dataIndex : 'sumberdana_id', sortable: true, 
                        tooltip:"jenis sumbangan",
                        editor : new Ext.form.ComboBox({
                            xtype : 'combo',
                            allowBlank: false,
                            store : this.SumberdanaDS,
                            displayField: 'name',
                            valueField: 'sumberdana_id',
                            mode : 'local',
                            forceSelection: true,
                            triggerAction: 'all',
                            selectOnFocus:true,
                            editable: true,
                            typeAhead: true,
                            width : 100,
                            value: 'Pilih'
                            }),
                    },

                    {   header: "Status", width : 50,
                        dataIndex : 'status', sortable: true,
                        tooltip:"jenis pendapatan Status",
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                   fields: ["label", "key"],
                                                   data : [["Active", "0"], ["Inactive", "1"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },
                    {   header: "Created", width : 150,
                        dataIndex : 'created_date', sortable: true,
                        tooltip:"jenis pendapatan Created Date",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    },
                    {   header: "Updated", width : 150,
                        dataIndex : 'modified_date', sortable: true,
                        tooltip:"jenis pendapatan Last Updated",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    }
                ];
                this.Records = Ext.data.Record.create(
                [   {name: 'mincome_id', type: 'string'},
                    {name: 'parent_mincome_id', type: 'string'},
                    {name: 'mincome_name', type: 'string'},
                    {name: 'mincome_value', type: 'integer'},
                    {name: 'status', type: 'string'},
                    {name: 'created_date', type: 'date'},
                    {name: 'modified_date', type: 'date'},
                ]);
                this.Searchs = [
                    {   id: 'mincome_name',
                        cid: 'mincome_name',
                        fieldLabel: 'income name',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'income_status',
                        cid: 'status',
                        fieldLabel: 'Status',
                        labelSeparator : '',
                        xtype : 'combo',
                        store : new Ext.data.SimpleStore(
                        {   fields: ['status'],
                            data : [ [''], ['Active'], ['Inactive']]
                        }),
                        displayField:'status',
                        valueField :'status',
                        mode : 'local',
                        triggerAction: 'all',
                        selectOnFocus:true,
                        editable: false,
                        width : 100,
                        value: ''
                    },
                ];
                this.SearchBtn = new Ext.Button(
                {   id : tabId+"_incomeSearchBtn",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.income_search_handler,
                    scope : this
                });
                this.DataStore = alfalah.core.newDataStore(
                    "{{ url('/mcoa/3/0') }}", false,
                    {   s:"init", limit:this.page_limit, start:this.page_start }
                );
                // this.DataStore.load();
                this.Grid = new Ext.grid.EditorGridPanel(
                {   store:  this.DataStore,
                    columns: this.Columns,
                    //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                    enableColLock: false,
                    //trackMouseOver: true,
                    loadMask: true,
                    height : alfalah.mcoa.centerPanel.container.dom.clientHeight-50,
                    anchor: '100%',
                    autoScroll  : true,
                    frame: true,
                    //stripeRows : true,
                    // inline buttons
                    //buttons: [{text:'Save'},{text:'Cancel'}],
                    //buttonAlign:'center',
                    tbar: [
                        {   text:'Add',
                            tooltip:'Add Record',
                            iconCls: 'silk-add',
                            handler : this.Grid_add,
                            scope : this
                        },
                        {   text:'Copy',
                            tooltip:'Copy Record',
                            iconCls: 'silk-page-copy',
                            handler : this.Grid_copy,
                            scope : this
                        },
                        //{   text:'Edit',
                        //    tooltip:'Edit Record',
                        //    iconCls: 'silk-page-white-edit',
                        //    //handler : this.income_grid_
                        //},
                        {   id : tabId+'_incomeSaveBtn',
                            text:'Save',
                            tooltip:'Save Record',
                            iconCls: 'icon-save',
                            handler : this.Grid_save,
                            scope : this
                        },
                        '-',
                        {   text:'Delete',
                            tooltip:'Delete Record',
                            iconCls: 'silk-delete',
                            handler : this.Grid_remove,
                            scope : this
                        },
                        '-',
                        {   print_type : "pdf",
                            text:'Print PDF',
                            hidden : true,
                            tooltip:'Print to PDF',
                            iconCls: 'silk-page-white-acrobat',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },
                        {   print_type : "xls",
                            text:'Print Excell',
                            tooltip:'Print to Excell SpreadSheet',
                            iconCls: 'silk-page-white-excel',
                            handler : function(button, event)
                            {   console.log(button) ;
                                console.log(event);
                                console.log(this.DataStore);
                                
                                alfalah.core.printButton(button, event, this.DataStore); 
                            },
                            scope : this
                        },'-',
                    ],
                    bbar: new Ext.PagingToolbar(
                    {   id : tabId+'_incomeGridBBar',
                        store: this.DataStore,
                        pageSize: this.page_limit,
                        displayInfo: true,
                        emptyMsg: 'No data found',
                        items : [
                            '-',
                            'Displayed : ',
                            new Ext.form.ComboBox(
                            {   id : tabId+'_incomePageCombo',
                                store: new Ext.data.SimpleStore(
                                            {   fields: ['value'],
                                                data : [[50],[75],[100],[125],[150]]
                                            }),
                                displayField:'value',
                                valueField :'value',
                                value : 75,
                                editable: false,
                                mode: 'local',
                                triggerAction: 'all',
                                selectOnFocus:true,
                                hiddenName: 'pagesize',
                                width:50,
                                listeners : { scope : this,
                                    'select' : function (a,b,c)
                                    {   this.page_limit = Ext.get(tabId+'_incomePageCombo').getValue();
                                        bbar = Ext.getCmp(tabId+'_incomeGridBBar');
                                        bbar.pageSize = parseInt(this.page_limit);
                                        this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                        this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    }
                                }
                            }),
                            ' records at a time'
                        ]
                    }),
                });
            },
            // build the layout
            build_layout: function()
            {   this.Tab = new Ext.Panel(
                {   id : tabId+"_incomeTab",
                    jsId : tabId+"_incomeTab",
                    title:  "TABEL JENIS PENDAPATAN",
                    region: 'center',
                    layout: 'border',
                    items: [
                    {   title: 'ToolBox',
                        region: 'east',     // position for region
                        split:true,
                        width: 200,
                        minSize: 200,
                        maxSize: 400,
                        collapsible: true,
                        layout : 'accordion',
                        items:[
                        {   title: 'S E A R C H',
                            labelWidth: 50,
                            defaultType: 'textfield',
                            xtype: 'form',
                            frame: true,
                            autoScroll : true,
                            items : [ this.Searchs, this.SearchBtn]
                        },
                        { title : 'Setting'
                        }]
                    },
                    //new Ext.TabPanel(
                    //    { id:'center-panel',
                    //      region:'center'
                    //    })
                    {   region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.Grid]
                    }
                    ]
                });
            }, 
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            {},
            // income grid add new record
            Grid_add: function(button, event)
            {   this.Grid.stopEditing();
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   mincome_id: "",
                        parent_mincome_id: "",
                        mincome_name: "income name",
                        mincome_value: 0,
                        status: 0,
                        created_date: "",
                        last_update: ""
                    }));
                // placed the edit cursor on third-column
                this.Grid.startEditing(0, 2);
            },
            // income grid copy and make it a new record
            Grid_copy: function(button, event)
            {   this.Grid.stopEditing();
                var selection = this.Grid.getSelectionModel().selection;
                if (selection)
                {   var data = this.Grid.getSelectionModel().selection.record.data;
                    this.Grid.store.insert( 0,
                        new this.Records (
                        {   income_id: "",
                            parent_income_id: data.parent_income_id,
                            income_name: data.income_name,
                            status: data.status
                    
                        }));
                    this.Grid.startEditing(0, 2);
                }
                else
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Selected Record ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                         });
                };
            },
            // income grid save records
            Grid_save : function(button, event)
            {   var the_records = this.DataStore.getModifiedRecords();
                // check data modification
                if ( the_records.length == 0 )
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Data modified ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                         });
                } else
                {   var json_data = alfalah.core.getDetailData(the_records);
                    alfalah.core.submitGrid(
                        this.DataStore, 
                        "{{ url('/mcoa/3/1') }}",
                        {   'x-csrf-token': alfalah.mcoa.sid }, 
                        {   json: Ext.encode(json_data) }
                    );
                };
            },
            // income grid delete records
            Grid_remove: function(button, event)
            {   this.Grid.stopEditing();
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/mcoa/3/2') }}",
                    {   'x-csrf-token': alfalah.mcoa.sid }, 
                    {   mincome_id: alfalah.mcoa.income.Grid.getSelectionModel().selection.record.data.mincome_id }
                );
            },
            // income search button
            income_search_handler : function(button, event)
            {   var the_search = true;
                if ( this.DataStore.getModifiedRecords().length > 0 )
                {   Ext.Msg.show(
                        {   title:'W A R N I N G ',
                            msg: ' Modified Data Found, Do you want to save it before search process ? ',
                            buttons: Ext.Msg.YESNO,
                            fn: function(buttonId, text)
                                {   if (buttonId =='yes')
                                    {   Ext.getCmp(tabId+'_incomeSaveBtn').handler.call();
                                        the_search = false;
                                    } else the_search = true;
                                },
                            icon: Ext.MessageBox.WARNING
                         });
                };
    
                if (the_search == true) // no modification records flag then we can go to search
                {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
    
                    this.DataStore.baseParams = Ext.apply( the_parameter, 
                        {s:"form", limit:this.page_limit, start:this.page_start, abc: 0});
                    this.DataStore.reload();
                };
        },
    }; // end of public space
    }(); // end of app

// On Ready
alfalah.mcoa.cash_bank_type= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing

        prepare_component: function()
        { 

            this.coaDS_2 = alfalah.core.newDataStore(
                "{{ url('/mcoa/4/9') }}", true,
                {   s:"form", limit:this.page_limit, start:this.page_start, type : "0", need_value : 1 }
            );
            this.coaDS_2.load();

            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'cash_bank_type_id', sortable: true,
                    tooltip:"cash_bank_type ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "cash_bank_type.Name", width : 150,
                    dataIndex : 'name', sortable: true,
                    tooltip:"cash_bank_type Name",
                    editor : new Ext.form.TextField({allowBlank: false}), 
                },
                {   header: "Account Number", width : 100,
                    dataIndex : 'coa_id', sortable: true,
                    tooltip:"Nomor rekening",
                    readonly :true,
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Nama Rekening", width : 200,
                    dataIndex : 'coa_name', sortable: true,
                    tooltip:"Account Name",
                    editor: new Ext.form.ComboBox(
                    {   store: this.coaDS_2,
                        typeAhead: false,
                        width: 250,
                      
                        displayField: 'display',
                        valueField: 'coa_name',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.coa_id = record.data.mcoa_id;
                                    combo.gridEditor.record.data.coa_name = record.data.coa_name;
                                    alfalah.mcoa.cash_bank_type.Grid.getView().refresh();
                                },
                        }
                    }),
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    tooltip:"cash type Status",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["label", "key"],
                                               data : [["Active", "0"], ["Inactive", "1"]]
                                        }),
                            displayField:"label",
                            valueField:"key",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"cash_bank_type Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"cash_bank_type Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'name', type: 'string'},
                // {name: 'dept_id', type: 'integer'},
                {name: 'coa_id', type: 'string'},
                {name: 'coa_name', type: 'string'},
                {name: 'status', type: 'integer'},
                {name: 'created_date', type: 'date'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'cash_bank_type_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'cash_bank_type_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ [''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_cash_bank_typeSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.cash_bank_type_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/mcoa/4/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.mcoa.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                stripeRows : true,
                
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Copy',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.Grid_copy,
                        scope : this
                    },
                    //{   text:'Edit',
                    //    tooltip:'Edit Record',
                    //    iconCls: 'silk-page-white-edit',
                    //    //handler : this.cash_bank_type_grid_
                    //},
                    {   id : tabId+'_cash_bank_typeSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_cash_bank_typeGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_cash_bank_typePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_cash_bank_typePageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_cash_bank_typeGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_cash_bank_typeSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_cash_bank_typeTab",
                jsId : tabId+"_cash_bank_typeTab",
                title:  "CASH / BANK TYPE",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // cash_bank_type grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   cash_bank_type_id: "",
                    cash_bank_type_name: "New Name",
                    // dept_id: "",
                    coa_id: " ",
                    coa_name: " ",
                    status: 0,
                    created_date: "",
                    modified_date: "",
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // cash_bank_type grid copy and make it a new record
        Grid_copy: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection)
            {   var data = this.Grid.getSelectionModel().selection.record.data;
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   id: "",
                        name: data.cash_bank_type_name,
                        status: data.status,
                    }));
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },
        // cash_bank_type grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/mcoa/4/1') }}",
                    {   'x-csrf-token': alfalah.mcoa.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // cash_bank_type grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/mcoa/4/2') }}",
                {   'x-csrf-token': alfalah.cash_bank_type.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.cash_bank_type_id }
            );
        },
        // cash_bank_type search button
        cash_bank_type_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_cash_bank_typeSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {  s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
//Ext.reg('project', alfalah.project);
Ext.onReady(alfalah.mcoa.initialize, alfalah.mcoa);
// end of file
</script>
<div>&nbsp;</div>