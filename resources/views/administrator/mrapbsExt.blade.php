<script type="text/javascript">
    // create namespace
    Ext.namespace('alfalah.mrapbs');
    
    // create application
    alfalah.mrapbs = function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.tabId = '{{ $TABID }}';
        // private functions
        // public space
        return {
            centerPanel : 0,
            sid : '{{ csrf_token() }}',
            task : ' ',
            act : ' ',
            // public methods
            initialize: function()
            {   this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {   this.centerPanel = Ext.getCmp(tabId);
                this.mrapbs.initialize();
                this.sumberdana.initialize();
                this.mprogram.initialize();
                // this.role.initialize();
                // this.role_mrapbs.initialize();
                // this.role_user.initialize();
                // this.role_page.initialize();
                // this.user_page.initialize();
            },
            // build the layout
            build_layout: function()
            {   this.centerPanel.beginUpdate();
                this.centerPanel.add(this.mrapbs.Tab);
                this.centerPanel.add(this.mprogram.Tab);
                this.centerPanel.add(this.sumberdana.Tab);
                this.centerPanel.setActiveTab(this.mrapbs.Tab);
                this.centerPanel.endUpdate();
                alfalah.core.viewport.doLayout();
            },
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            {   console.log(4); },
        }; // end of public space
    }(); // end of app
    // create application
    alfalah.mrapbs.mrapbs= function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.Tab;
        this.Grid;
        this.Columns;
        this.Records;
        this.DataStore;
        this.Searchs;
        this.SearchBtn;
        // private functions
        // public space
        return {
            // execute at the very last time
            // public variable
            page_limit : 75,
            page_start : 0,
            // public methods
            initialize: function()
            {   this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {
                this.DepartmentDS = alfalah.core.newDataStore(
                    "{{ url('/ppdb/0/9') }}", false,
                    {   s:"form", limit:this.page_limit, start:this.page_start,
                        type:"Section"}
                );
                this.DepartmentDS.load();
                this.Columns = [
                    {   header: "ID", width : 50,
                        dataIndex : 'mrapbs_id', sortable: true,
                        tooltip:"mrapbs ID",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Parent.ID", width : 100,
                        dataIndex : 'parent_mrapbs_id', sortable: true,
                        tooltip:"Parent mrapbs ID",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Name", width: 200,
                        dataIndex : 'name', sortable: true,
                        tooltip:"Name",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Type", width : 100,
                        dataIndex : 'type', sortable: true,
                        tooltip:"mrapbs Type",
                        // editor : new Ext.form.TextField({allowBlank: false}),
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                fields: ["label", "key"],
                                                data : [["ORGANISASI", "1"], ["URUSAN", "2"], ["PROGRAM", "3"], ["KEGIATAN", "4"],
                                                ["SUB KEGIATAN", "5"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },
                    {   header: "Description", width : 200,
                        dataIndex : 'description', sortable: true,
                        tooltip: "keterangan",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Jenjang", width : 100,
                        dataIndex : 'dept_id', sortable: true, 
                        tooltip:"Penomoran dokumen akan mengikuti jenjang yg diisi",
                        editor : new Ext.form.ComboBox({
                            xtype : 'combo',
                            allowBlank: false,
                            store : this.DepartmentDS,
                            displayField: 'name',
                            valueField: 'dept_id',
                            mode : 'local',
                            forceSelection: true,
                            triggerAction: 'all',
                            selectOnFocus:true,
                            editable: true,
                            typeAhead: true,
                            width : 100,
                            value: 'Pilih'
                            }),
                    },
                    {   header: "Status", width : 50,
                        dataIndex : 'status', sortable: true,
                        tooltip:"mrapbs Status",
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                fields: ["label", "key"],
                                                data : [["Active", "0"], ["Inactive", "1"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },
                    {   header: "Created", width : 150, 
                        dataIndex : 'created_date', sortable: true,
                        tooltip:"mrapbs Created Date",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    },
                    {   header: "Updated", width : 150,
                        dataIndex : 'modified_date', sortable: true,
                        tooltip:"mrapbs Last Updated",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    }
                ];
                this.Records = Ext.data.Record.create(
                [   {name: 'mrapbs_id', type: 'string'},
                    {name: 'parent_mrapbs_id', type: 'string'},
                    {name: 'name', type: 'string'},
                    {name: 'type', type: 'integer'},
                    {name: 'description', type: 'string'},
                    {name: 'dept_id', type: 'integer'},
                    {name: 'jenjang_Sekolah_name', type: 'string'},
                    {name: 'status', type: 'string'},
                    {name: 'created_date', type: 'date'},
                    {name: 'modified_date', type: 'date'},
                ]);
                this.Searchs = [
                    {   id: 'mrapbs_name',
                        cid: 'name',
                        fieldLabel: 'Name',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'mrapbs_type',
                        cid: 'type',
                        fieldLabel: 'Type',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'mrapbs_description',
                        cid: 'description',
                        fieldLabel: 'Desc.',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'mrapbs_status',
                        cid: 'status',
                        fieldLabel: 'Status',
                        labelSeparator : '',
                        xtype : 'combo',
                        store : new Ext.data.SimpleStore(
                        {   fields: ['status'],
                            data : [ [''], ['Active'], ['Inactive']]
                        }),
                        displayField:'status',
                        valueField :'status',
                        mode : 'local',
                        triggerAction: 'all',
                        selectOnFocus:true,
                        editable: false,
                        width : 100,
                        value: ''
                    },
                ];
                this.SearchBtn = new Ext.Button(
                {   id : tabId+"_mrapbsSearchBtn",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.mrapbs_search_handler,
                    scope : this
                });


                this.DataStore = alfalah.core.newDataStore(
                    "{{url('/mrapbs/1/0')}}", false,
                    {   s:"init", limit:this.page_limit, start:this.page_start }
                );
                // this.DataStore.load();
                this.Grid = new Ext.grid.EditorGridPanel(
                {   store:  this.DataStore,
                    columns: this.Columns,
                    //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                    enableColLock: false,
                    //trackMouseOver: true,
                    loadMask: true,
                    height : alfalah.mrapbs.centerPanel.container.dom.clientHeight-50,
                    anchor: '100%',
                    autoScroll  : true,
                    frame: true,
                    //stripeRows : true,
                    // inline buttons
                    //buttons: [{text:'Save'},{text:'Cancel'}],
                    //buttonAlign:'center',
                    tbar: [
                        {   text:'Add',
                            tooltip:'Add Record',
                            iconCls: 'silk-add',
                            handler : this.Grid_add,
                            scope : this
                        },
                        {   text:'Copy',
                            tooltip:'Copy Record',
                            iconCls: 'silk-page-copy',
                            handler : this.Grid_copy,
                            scope : this
                        },
                        //{   text:'Edit',
                        //    tooltip:'Edit Record',
                        //    iconCls: 'silk-page-white-edit',
                        //    //handler : this.mrapbs_grid_
                        //},
                        {   id : tabId+'_mrapbsSaveBtn',
                            text:'Save',
                            tooltip:'Save Record',
                            iconCls: 'icon-save',
                            handler : this.Grid_save,
                            scope : this
                        },
                        '-',
                        {   text:'Delete',
                            tooltip:'Delete Record',
                            iconCls: 'silk-delete',
                            handler : this.Grid_remove,
                            scope : this
                        },
                        '-',
                        {   print_type : "pdf",
                            text:'Print PDF',
                            tooltip:'Print to PDF',
                            iconCls: 'silk-page-white-acrobat',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },
                        {   print_type : "xls",
                            text:'Print Excell',
                            tooltip:'Print to Excell SpreadSheet',
                            iconCls: 'silk-page-white-excel',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },'-',
                    ],
                    bbar: new Ext.PagingToolbar(
                    {   id : tabId+'_mrapbsGridBBar',
                        store: this.DataStore,
                        pageSize: this.page_limit,
                        displayInfo: true,
                        emptyMsg: 'No data found',
                        items : [
                            '-',
                            'Displayed : ',
                            new Ext.form.ComboBox(
                            {   id : tabId+'_mrapbsPageCombo',
                                store: new Ext.data.SimpleStore(
                                            {   fields: ['value'],
                                                data : [[50],[75],[100],[125],[150]]
                                            }),
                                displayField:'value',
                                valueField :'value',
                                value : 75,
                                editable: false,
                                mode: 'local',
                                triggerAction: 'all',
                                selectOnFocus:true,
                                hiddenName: 'pagesize',
                                width:50,
                                listeners : { scope : this,
                                    'select' : function (a,b,c)
                                    {   page_limit = Ext.get(tabId+'_mrapbsPageCombo').getValue();
                                        page_start = ( Ext.getCmp(tabId+'_mrapbsGridBBar').getPageData().activePage -1) * page_limit;
                                        this.SearchBtn.handler.call(this.SearchBtn.scope);
                                        //Ext.getCmp(tabId+"_mrapbsSearchBtn").handler.call();
                                        //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                    }
                                }
                            }),
                            ' records at a time'
                        ]
                    }),
                });
            },
            // build the layout
            build_layout: function()
            {   this.Tab = new Ext.Panel(
                {   id : tabId+"_mrapbsTab",
                    jsId : tabId+"_mrapbsTab",
                    title:  "URUSAN/ORGANISASI",
                    region: 'center',
                    layout: 'border',
                    items: [
                    {   title: 'ToolBox',
                        region: 'east',     // position for region
                        split:true,
                        width: 200,
                        minSize: 200,
                        maxSize: 400,
                        collapsible: true,
                        layout : 'accordion',
                        items:[
                        {   title: 'S E A R C H',
                            items : [ this.Searchs, this.SearchBtn],
                            labelWidth: 50,
                            defaultType: 'textfield',
                            xtype: 'form',
                            frame: true,
                            autoScroll : true
                        },
                        { title : 'Setting'
                        }]
                    },
                    //new Ext.TabPanel(
                    //    { id:'center-panel',
                    //      region:'center'
                    //    })
                    {   region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.Grid]
                    }
                    ]
                });
            },
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            {},
            // mrapbs grid add new record
            Grid_add: function(button, event)
            {   this.Grid.stopEditing();
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   mrapbs_id: 0,
                        parent_mrapbs_id: 0,
                        name: "New Name",
                        type: 0,
                        description: "",
                        status: 0,
                        created_date: "",
                        modified_date: ""
                    }));
                // placed the edit cursor on third-column
                this.Grid.startEditing(0, 2);
            },
            // mrapbs grid copy and make it a new record
            Grid_copy: function(button, event)
            {   this.Grid.stopEditing();
                var selection = this.Grid.getSelectionModel().selection;
                if (selection)
                {   var data = this.Grid.getSelectionModel().selection.record.data;
                    this.Grid.store.insert( 0,
                        new this.Records (
                        {   id: "",
                            parent_mrapbs_id: data.parent_mrapbs_id,
                            name: data.name,
                            type: data.type,
                            description: data.description,
                            status: data.status
                        }));
                    this.Grid.startEditing(0, 2);
                }
                else
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Selected Record ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                         });
                };
            },
            // mrapbs grid save records
            Grid_save : function(button, event)
            {   var the_records = this.DataStore.getModifiedRecords();
                // check data modification
                if ( the_records.length == 0 )
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Data modified ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                         });
                } else
                {   var json_data = alfalah.core.getDetailData(the_records);
                    alfalah.core.submitGrid(
                        this.DataStore, 
                        "{{url('/mrapbs/1/1')}}",
                        {   'x-csrf-token': alfalah.mrapbs.sid }, 
                        {   json: Ext.encode(json_data) }
                    );
                };
            },
            // mrapbs grid delete records
            Grid_remove: function(button, event)
            {   this.Grid.stopEditing();
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{url('/mrapbs/1/2')}}",
                    {   'x-csrf-token': alfalah.mrapbs.sid }, 
                    {   id: this.Grid.getSelectionModel().selection.record.data.mrapbs_id }
                );
            },
            // mrapbs search button
            mrapbs_search_handler : function(button, event)
            {   var the_search = true;
                if ( this.DataStore.getModifiedRecords().length > 0 )
                {   Ext.Msg.show(
                        {   title:'W A R N I N G ',
                            msg: ' Modified Data Found, Do you want to save it before search process ? ',
                            buttons: Ext.Msg.YESNO,
                            fn: function(buttonId, text)
                                {   if (buttonId =='yes')
                                    {   Ext.getCmp(tabId+'_mrapbsSaveBtn').handler.call();
                                        the_search = false;
                                    } else the_search = true;
                                },
                            icon: Ext.MessageBox.WARNING
                         });
                };
    
                if (the_search == true) // no modification records flag then we can go to search
                {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                    this.DataStore.baseParams = Ext.apply( the_parameter, {s:"form", limit:this.page_limit, start:this.page_start});
                    this.DataStore.reload();
                };
            },
        }; // end of public space
    }(); // end of app
    // create application
    alfalah.mrapbs.sumberdana= function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.Tab;
        this.Grid;
        this.Columns;
        this.Records;
        this.DataStore;
        this.Searchs;
        this.SearchBtn;
        // private functions
        // public space
        return {
            // execute at the very last time
            // public variable
            page_limit : 75,
            page_start : 0,
            // public methods
            initialize: function()
            {   this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {
                this.Columns = [
                    {   header: "ID", width : 50,
                        dataIndex : 'sumberdana_id', sortable: true,
                        tooltip:"sumberdana ID",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Sumber Dana", width : 200,
                        dataIndex : 'name', sortable: true,
                        tooltip:"Sumber Dana",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Status", width : 50,
                        dataIndex : 'status', sortable: true,
                        tooltip:"sumberdana Status",
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                   fields: ["label", "key"],
                                                   data : [["Active", "0"], ["Inactive", "1"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },
                    {   header: "Created", width : 150,
                        dataIndex : 'created_date', sortable: true,
                        tooltip:"sumberdana Created Date",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    },
                    {   header: "Updated", width : 150,
                        dataIndex : 'modified_date', sortable: true,
                        tooltip:"sumberdana Last Updated",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    }
                ];
                this.Records = Ext.data.Record.create(
                [   {name: 'sumberdana_id', type: 'integer'},
                    {name: 'name', type: 'string'},
                    {name: 'status', type: 'string'},
                    {name: 'created_date', type: 'date'},
                    {name: 'modified_date', type: 'date'},
                ]);
                this.Searchs = [
                    {   id: 'sumberdana_name',
                        cid: 'name',
                        fieldLabel: 'Sumber Dana',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'sumberdana_status',
                        cid: 'status',
                        fieldLabel: 'Status',
                        labelSeparator : '',
                        xtype : 'combo',
                        store : new Ext.data.SimpleStore(
                        {   fields: ['status'],
                            data : [ [''], ['Active'], ['Inactive']]
                        }),
                        displayField:'status',
                        valueField :'status',
                        mode : 'local',
                        triggerAction: 'all',
                        selectOnFocus:true,
                        editable: false,
                        width : 100,
                        value: ''
                    },
                ];
                this.SearchBtn = new Ext.Button(
                {   id : tabId+"_sumberdanaSearchBtn",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.sumberdana_search_handler,
                    scope : this
                });
                this.DataStore = alfalah.core.newDataStore(
                    "{{ url('/mrapbs/2/0') }}", false,
                    {   s:"init", limit:this.page_limit, start:this.page_start }
                );
                // this.DataStore.load();
                this.Grid = new Ext.grid.EditorGridPanel(
                {   store:  this.DataStore,
                    columns: this.Columns,
                    //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                    enableColLock: false,
                    //trackMouseOver: true,
                    loadMask: true,
                    height : alfalah.mrapbs.centerPanel.container.dom.clientHeight-50,
                    anchor: '100%',
                    autoScroll  : true,
                    frame: true,
                    //stripeRows : true,
                    // inline buttons
                    //buttons: [{text:'Save'},{text:'Cancel'}],
                    //buttonAlign:'center',
                    tbar: [
                        {   text:'Add',
                            tooltip:'Add Record',
                            iconCls: 'silk-add',
                            handler : this.Grid_add,
                            scope : this
                        },
                        {   text:'Copy',
                            tooltip:'Copy Record',
                            iconCls: 'silk-page-copy',
                            handler : this.Grid_copy,
                            scope : this
                        },
                        //{   text:'Edit',
                        //    tooltip:'Edit Record',
                        //    iconCls: 'silk-page-white-edit',
                        //    //handler : this.sumberdana_grid_
                        //},
                        {   id : tabId+'_sumberdanaSaveBtn',
                            text:'Save',
                            tooltip:'Save Record',
                            iconCls: 'icon-save',
                            handler : this.Grid_save,
                            scope : this
                        },
                        '-',
                        {   text:'Delete',
                            tooltip:'Delete Record',
                            iconCls: 'silk-delete',
                            handler : this.Grid_remove,
                            scope : this
                        },
                        '-',
                        {   print_type : "pdf",
                            text:'Print PDF',
                            tooltip:'Print to PDF',
                            iconCls: 'silk-page-white-acrobat',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },
                        {   print_type : "xls",
                            text:'Print Excell',
                            tooltip:'Print to Excell SpreadSheet',
                            iconCls: 'silk-page-white-excel',
                            handler : function(button, event)
                            {   console.log(button) ;
                                console.log(event);
                                console.log(this.DataStore);
                                
                                alfalah.core.printButton(button, event, this.DataStore); 
                            },
                            scope : this
                        },'-',
                    ],
                    bbar: new Ext.PagingToolbar(
                    {   id : tabId+'_sumberdanaGridBBar',
                        store: this.DataStore,
                        pageSize: this.page_limit,
                        displayInfo: true,
                        emptyMsg: 'No data found',
                        items : [
                            '-',
                            'Displayed : ',
                            new Ext.form.ComboBox(
                            {   id : tabId+'_sumberdanaPageCombo',
                                store: new Ext.data.SimpleStore(
                                            {   fields: ['value'],
                                                data : [[50],[75],[100],[125],[150]]
                                            }),
                                displayField:'value',
                                valueField :'value',
                                value : 75,
                                editable: false,
                                mode: 'local',
                                triggerAction: 'all',
                                selectOnFocus:true,
                                hiddenName: 'pagesize',
                                width:50,
                                listeners : { scope : this,
                                    'select' : function (a,b,c)
                                    {   this.page_limit = Ext.get(tabId+'_sumberdanaPageCombo').getValue();
                                        bbar = Ext.getCmp(tabId+'_sumberdanaGridBBar');
                                        bbar.pageSize = parseInt(this.page_limit);
                                        this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                        this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    }
                                }
                            }),
                            ' records at a time'
                        ]
                    }),
                });
            },
            // build the layout
            build_layout: function()
            {   this.Tab = new Ext.Panel(
                {   id : tabId+"_sumberdanaTab",
                    jsId : tabId+"_sumberdanaTab",
                    title:  "Sumber Dana",
                    region: 'center',
                    layout: 'border',
                    items: [
                    {   title: 'ToolBox',
                        region: 'east',     // position for region
                        split:true,
                        width: 200,
                        minSize: 200,
                        maxSize: 400,
                        collapsible: true,
                        layout : 'accordion',
                        items:[
                        {   title: 'S E A R C H',
                            labelWidth: 50,
                            defaultType: 'textfield',
                            xtype: 'form',
                            frame: true,
                            autoScroll : true,
                            items : [ this.Searchs, this.SearchBtn]
                        },
                        { title : 'Setting'
                        }]
                    },
                    //new Ext.TabPanel(
                    //    { id:'center-panel',
                    //      region:'center'
                    //    })
                    {   region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.Grid]
                    }
                    ]
                });
            },
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            {},
            // sumberdana grid add new record
            Grid_add: function(button, event)
            {   this.Grid.stopEditing();
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   sumberdana_id: "",
                        name: "Sumber Dana",
                        status: 0,
                        created_date: "",
                        last_update: ""
                    }));
                // placed the edit cursor on third-column
                this.Grid.startEditing(0, 2);
            },
            // sumberdana grid copy and make it a new record
            Grid_copy: function(button, event)
            {   this.Grid.stopEditing();
                var selection = this.Grid.getSelectionModel().selection;
                if (selection)
                {   var data = this.Grid.getSelectionModel().selection.record.data;
                    this.Grid.store.insert( 0,
                        new this.Records (
                        {   id: "",
                            parent_sumberdana_id: data.parent_sumberdana_id,
                            name: data.name,
                            status: data.status
                    
                        }));
                    this.Grid.startEditing(0, 2);
                }
                else
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Selected Record ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                         });
                };
            },
            // sumberdana grid save records
            Grid_save : function(button, event)
            {   var the_records = this.DataStore.getModifiedRecords();
                // check data modification
                if ( the_records.length == 0 )
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Data modified ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                         });
                } else
                {   var json_data = alfalah.core.getDetailData(the_records);
                    alfalah.core.submitGrid(
                        this.DataStore, 
                        "{{ url('/mrapbs/2/1') }}",
                        {   'x-csrf-token': alfalah.mrapbs.sid }, 
                        {   json: Ext.encode(json_data) }
                    );
                };
            },
            // sumberdana grid delete records
            Grid_remove: function(button, event)
            {   this.Grid.stopEditing();
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/mrapbs/2/2') }}",
                    {   'x-csrf-token': alfalah.mrapbs.sid }, 
                    {   id: this.Grid.getSelectionModel().selection.record.data.sumberdana_id }
                );
            },
            // sumberdana search button
            sumberdana_search_handler : function(button, event)
            {   var the_search = true;
                if ( this.DataStore.getModifiedRecords().length > 0 )
                {   Ext.Msg.show(
                        {   title:'W A R N I N G ',
                            msg: ' Modified Data Found, Do you want to save it before search process ? ',
                            buttons: Ext.Msg.YESNO,
                            fn: function(buttonId, text)
                                {   if (buttonId =='yes')
                                    {   Ext.getCmp(tabId+'_sumberdanaSaveBtn').handler.call();
                                        the_search = false;
                                    } else the_search = true;
                                },
                            icon: Ext.MessageBox.WARNING
                         });
                };
    
                if (the_search == true) // no modification records flag then we can go to search
                {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
    
                    this.DataStore.baseParams = Ext.apply( the_parameter, 
                        {s:"form", limit:this.page_limit, start:this.page_start, abc: 0});
                    this.DataStore.reload();
                };
        },
    }; // end of public space
    }(); // end of app
    // create application
    alfalah.mrapbs.mprogram= function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.Tab;
        this.Grid;
        this.Columns;
        this.Records;
        this.DataStore;
        this.Searchs;
        this.SearchBtn;
        // private functions
        // public space
        return {
            // execute at the very last time
            // public variable
            page_limit : 75,
            page_start : 0,
            // public methods
            initialize: function()
            {   this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {
                this.Columns = [
                    {   header: "ID", width : 50,
                        dataIndex : 'mprogram_id', sortable: true,
                        tooltip:"mprogram ID",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Parent.ID", width : 100,
                        dataIndex : 'parent_mprogram_id', sortable: true,
                        tooltip:"Parent mprogram ID",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Name", width: 200,
                        dataIndex : 'name', sortable: true,
                        tooltip:"Name",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Type", width : 100,
                        dataIndex : 'type', sortable: true,
                        tooltip:"mprogram Type",
                        // editor : new Ext.form.TextField({allowBlank: false}),
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                            fields: ["label", "key"],
                                            data : [["PROGRAM", "3"], ["KEGIATAN", "4"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },
                    {   header: "Description", width : 200,
                        dataIndex : 'description', sortable: true,
                        tooltip:"mprogram arrange no sequence",
                        editor : new Ext.form.TextField({allowBlank: false}),
                    },
                    {   header: "Status", width : 50,
                        dataIndex : 'status', sortable: true,
                        tooltip:"mprogram Status",
                        editor :  new Ext.Editor(
                            new Ext.form.ComboBox(
                            {   store: new Ext.data.SimpleStore({
                                                   fields: ["label", "key"],
                                                   data : [["Active", "0"], ["Inactive", "1"]]
                                            }),
                                displayField:"label",
                                valueField:"key",
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: "all",
                                selectOnFocus:true,
                                forceSelection :true
                            }),
                            {autoSize:true}
                        )
                    },
                    {   header: "Created", width : 150,
                        dataIndex : 'created_date', sortable: true,
                        tooltip:"mprogram Created Date",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    },
                    {   header: "Updated", width : 150,
                        dataIndex : 'modified_date', sortable: true,
                        tooltip:"mprogram Last Updated",
                        css : "background-color: #DCFFDE;",
                        renderer: function(value){  return alfalah.core.dateRenderer(value); }
                    }
                ];
                this.Records = Ext.data.Record.create(
                [   {name: 'mprogram_id', type: 'string'},
                    {name: 'parent_mprogram_id', type: 'string'},
                    {name: 'name', type: 'string'},
                    {name: 'type', type: 'integer'},
                    {name: 'status', type: 'string'},
                    {name: 'description', type: 'string'},
                    {name: 'created_date', type: 'date'},
                    {name: 'modified_date', type: 'date'},
                ]);
                this.Searchs = [
                    {   id: 'mprogram_name',
                        cid: 'name',
                        fieldLabel: 'Name',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'mprogram_type',
                        cid: 'type',
                        fieldLabel: 'Type',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'mprogram_description',
                        cid: 'description',
                        fieldLabel: 'Desc.',
                        labelSeparator : '',
                        xtype: 'textfield',
                        width : 120
                    },
                    {   id: 'mprogram_status',
                        cid: 'status',
                        fieldLabel: 'Status',
                        labelSeparator : '',
                        xtype : 'combo',
                        store : new Ext.data.SimpleStore(
                        {   fields: ['status'],
                            data : [ [''], ['Active'], ['Inactive']]
                        }),
                        displayField:'status',
                        valueField :'status',
                        mode : 'local',
                        triggerAction: 'all',
                        selectOnFocus:true,
                        editable: false,
                        width : 100,
                        value: ''
                    },
                ];
                this.SearchBtn = new Ext.Button(
                {   id : tabId+"_mprogramSearchBtn",
                    fieldLabel: '',
                    text:'Search',
                    tooltip:'Search',
                    iconCls: 'silk-zoom',
                    xtype: 'button',
                    width : 120,
                    handler : this.mprogram_search_handler,
                    scope : this
                });
                this.DataStore = alfalah.core.newDataStore(
                    "{{url('/mrapbs/3/0')}}", false,
                    {   s:"init", limit:this.page_limit, start:this.page_start }
                );
                // this.DataStore.load();
                this.Grid = new Ext.grid.EditorGridPanel(
                {   store:  this.DataStore,
                    columns: this.Columns,
                    //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                    enableColLock: false,
                    //trackMouseOver: true,
                    loadMask: true,
                    height : alfalah.mrapbs.centerPanel.container.dom.clientHeight-50,
                    anchor: '100%',
                    autoScroll  : true,
                    frame: true,
                    //stripeRows : true,
                    // inline buttons
                    //buttons: [{text:'Save'},{text:'Cancel'}],
                    //buttonAlign:'center',
                    tbar: [
                        {   text:'Add',
                            tooltip:'Add Record',
                            iconCls: 'silk-add',
                            handler : this.Grid_add,
                            scope : this
                        },
                        {   text:'Copy',
                            tooltip:'Copy Record',
                            iconCls: 'silk-page-copy',
                            handler : this.Grid_copy,
                            scope : this
                        },
                        //{   text:'Edit',
                        //    tooltip:'Edit Record',
                        //    iconCls: 'silk-page-white-edit',
                        //    //handler : this.mprogram_grid_
                        //},
                        {   id : tabId+'_mprogramSaveBtn',
                            text:'Save',
                            tooltip:'Save Record',
                            iconCls: 'icon-save',
                            handler : this.Grid_save,
                            scope : this
                        },
                        '-',
                        {   text:'Delete',
                            tooltip:'Delete Record',
                            iconCls: 'silk-delete',
                            handler : this.Grid_remove,
                            scope : this
                        },
                        '-',
                        {   print_type : "pdf",
                            text:'Print PDF',
                            tooltip:'Print to PDF',
                            iconCls: 'silk-page-white-acrobat',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },
                        {   print_type : "xls",
                            text:'Print Excell',
                            tooltip:'Print to Excell SpreadSheet',
                            iconCls: 'silk-page-white-excel',
                            handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                            scope : this
                        },'-',
                    ],
                    bbar: new Ext.PagingToolbar(
                    {   id : tabId+'_mprogramGridBBar',
                        store: this.DataStore,
                        pageSize: this.page_limit,
                        displayInfo: true,
                        emptyMsg: 'No data found',
                        items : [
                            '-',
                            'Displayed : ',
                            new Ext.form.ComboBox(
                            {   id : tabId+'_mprogramPageCombo',
                                store: new Ext.data.SimpleStore(
                                            {   fields: ['value'],
                                                data : [[50],[75],[100],[125],[150]]
                                            }),
                                displayField:'value',
                                valueField :'value',
                                value : 75,
                                editable: false,
                                mode: 'local',
                                triggerAction: 'all',
                                selectOnFocus:true,
                                hiddenName: 'pagesize',
                                width:50,
                                listeners : { scope : this,
                                    'select' : function (a,b,c)
                                    {   page_limit = Ext.get(tabId+'_mprogramPageCombo').getValue();
                                        page_start = ( Ext.getCmp(tabId+'_mprogramGridBBar').getPageData().activePage -1) * page_limit;
                                        this.SearchBtn.handler.call(this.SearchBtn.scope);
                                        //Ext.getCmp(tabId+"_mprogramSearchBtn").handler.call();
                                        //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                    }
                                }
                            }),
                            ' records at a time'
                        ]
                    }),
                });
            },
            // build the layout
            build_layout: function()
            {   this.Tab = new Ext.Panel(
                {   id : tabId+"_mprogramTab",
                    jsId : tabId+"_mprogramTab",
                    title:  "PROGRAM/KEGIATAN",
                    region: 'center',
                    layout: 'border',
                    items: [
                    {   title: 'ToolBox',
                        region: 'east',     // position for region
                        split:true,
                        width: 200,
                        minSize: 200,
                        maxSize: 400,
                        collapsible: true,
                        layout : 'accordion',
                        items:[
                        {   title: 'S E A R C H',
                            items : [ this.Searchs, this.SearchBtn],
                            labelWidth: 50,
                            defaultType: 'textfield',
                            xtype: 'form',
                            frame: true,
                            autoScroll : true

                        },
                        { title : 'Setting'
                        }]
                    },
                    //new Ext.TabPanel(
                    //    { id:'center-panel',
                    //      region:'center'
                    //    })
                    {   region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.Grid]
                    }
                    ]
                });
            },
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            {},
            // mprogram grid add new record
            Grid_add: function(button, event)
            {   this.Grid.stopEditing();
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   mprogram_id: 0,
                        parent_mprogram_id: 0,
                        name: "New Name",
                        type: 0,
                        description: "",
                        status: 0,
                        created_date: "",
                        modified_date: ""
                    }));
                // placed the edit cursor on third-column
                this.Grid.startEditing(0, 2);
            },
            // mprogram grid copy and make it a new record
            Grid_copy: function(button, event)
            {   this.Grid.stopEditing();
                var selection = this.Grid.getSelectionModel().selection;
                if (selection)
                {   var data = this.Grid.getSelectionModel().selection.record.data;
                    this.Grid.store.insert( 0,
                        new this.Records (
                        {   id: "",
                            parent_mprogram_id: data.parent_mprogram_id,
                            name: data.name,
                            type: data.type,
                            description: data.description,
                            status: data.status
                        }));
                    this.Grid.startEditing(0, 2);
                }
                else
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Selected Record ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                         });
                };
            },
            // mprogram grid save records
            Grid_save : function(button, event)
            {   var the_records = this.DataStore.getModifiedRecords();
                // check data modification
                if ( the_records.length == 0 )
                {   Ext.Msg.show(
                        {   title:'I N F O ',
                            msg: 'No Data modified ! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                         });
                } else
                {   var json_data = alfalah.core.getDetailData(the_records);
                    alfalah.core.submitGrid(
                        this.DataStore, 
                        "{{url('/mrapbs/3/1')}}",
                        {   'x-csrf-token': alfalah.mrapbs.sid }, 
                        {   json: Ext.encode(json_data) }
                    );
                };
            },
            // mprogram grid delete records
            Grid_remove: function(button, event)
            {   this.Grid.stopEditing();
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{url('/mrapbs/3/2')}}",
                    {   'x-csrf-token': alfalah.mrapbs.sid }, 
                    {   id: this.Grid.getSelectionModel().selection.record.data.mprogram_id }
                );
            },
            // mprogram search button
            mprogram_search_handler : function(button, event)
            {   var the_search = true;
                if ( this.DataStore.getModifiedRecords().length > 0 )
                {   Ext.Msg.show(
                        {   title:'W A R N I N G ',
                            msg: ' Modified Data Found, Do you want to save it before search process ? ',
                            buttons: Ext.Msg.YESNO,
                            fn: function(buttonId, text)
                                {   if (buttonId =='yes')
                                    {   Ext.getCmp(tabId+'_mprogramSaveBtn').handler.call();
                                        the_search = false;
                                    } else the_search = true;
                                },
                            icon: Ext.MessageBox.WARNING
                         });
                };
    
                if (the_search == true) // no modification records flag then we can go to search
                {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                    this.DataStore.baseParams = Ext.apply( the_parameter, {s:"form", limit:this.page_limit, start:this.page_start});
                    this.DataStore.reload();
                };
            },
        }; // end of public space
    }(); // end of app
// On Ready
    //Ext.reg('project', alfalah.project);
    Ext.onReady(alfalah.mrapbs.initialize, alfalah.mrapbs);
    // end of file
    </script>
    <div>&nbsp;</div>