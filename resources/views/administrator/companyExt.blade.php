<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.company');

// create application
alfalah.company = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '{{ $TABID }}';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '{{ csrf_token() }}',
        task : ' ',
        act : ' ',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.company_type.initialize();
            this.company_class_type.initialize();
            this.company.initialize();
            this.site.initialize();
            this.department.initialize();
            this.site_department.initialize();
            this.site_building.initialize();
            this.documents.initialize();
            this.site_documents.initialize();
            this.warehouse.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.company_type.Tab);
            this.centerPanel.add(this.company.Tab);
            this.centerPanel.add(this.company_class_type.Tab);
            this.centerPanel.add(this.site.Tab);
            this.centerPanel.add(this.department.Tab);
            this.centerPanel.add(this.site_department.Tab);
            this.centerPanel.add(this.site_building.Tab);
            this.centerPanel.add(this.documents.Tab);
            this.centerPanel.add(this.site_documents.Tab);
            this.centerPanel.add(this.warehouse.Tab);
            this.centerPanel.setActiveTab(this.company_type.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {    },
    }; // end of public space
}(); // end of app
// create application
alfalah.company.company_type= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'company_type', sortable: true,
                    tooltip:"company_type ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Name", width : 250,
                    dataIndex : 'name', sortable: true,
                    tooltip:"company_type Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"company_type Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'company_type', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'created_date', type: 'date'}
            ]);
            this.Searchs = [
                {   id: 'comptype_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'comptype_type',
                    cid: 'type',
                    fieldLabel: 'Type',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_company_typeSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.company_type_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/company/1/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.company.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Copy',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.Grid_copy,
                        scope : this
                    },
                    //{   text:'Edit',
                    //    tooltip:'Edit Record',
                    //    iconCls: 'silk-page-white-edit',
                    //    //handler : this.company_type_grid_
                    //},
                    {   id : tabId+'_company_typeSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_company_typeGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_company_typePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_company_typePageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_company_typeGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_company_typeTab",
                jsId : tabId+"_company_typeTab",
                title:  "Company Type",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // company_type grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   company_type: "",
                    name: "New Name",
                    created_date: "",
                    modified_date: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // company_type grid copy and make it a new record
        Grid_copy: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection)
            {   var data = this.Grid.getSelectionModel().selection.record.data;
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   id: "",
                        parent_company_type_id: data.parent_company_type_id,
                        name: data.name,
                        object_id : data.object_id,
                        link: data.link,
                        status: data.status,
                        has_child : data.has_child,
                        level_degree: data.level_degree,
                        arrange_no: data.arange_no
                    }));
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },
        // company_type grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/company/1/1') }}",
                    {   'x-csrf-token': alfalah.company.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // company_type grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/company/1/2') }}",
                {   'x-csrf-token': alfalah.company.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.company_type }
            );
        },
        // company_type search button
        company_type_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_company_typeSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter, {s:"form", limit:this.page_limit, start:this.page_start});
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.company.company_class_type= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Company ID", width : 200,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"company ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.company_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Company Type", width : 150,
                    dataIndex : 'company_type', sortable: true,
                    tooltip:"company_type",
                    editor : new Ext.form.TextField({allowBlank: false}),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br> '+record.data.company_type_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Area", width : 100,
                    dataIndex : 'area_type_name', sortable: true,
                    tooltip:"company Status",
                    editor :  new Ext.Editor(  
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["key"],
                                               data : [["Local"], ["Foreign"]]
                                        }),
                            displayField:"key",
                            valueField:"key",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"company_class_type Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"company_class_type Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'company_id', type: 'string'},
                {name: 'company_type', type: 'string'},
                {name: 'area_type', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'cct_company_id',
                    cid: 'company_id',
                    fieldLabel: 'Company',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'cct_company_type',
                    cid: 'company_type',
                    fieldLabel: 'Type',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_company_class_typeSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.company_class_type_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/company/2/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.company.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Copy',
                        tooltip:'Copy Record',
                        iconCls: 'silk-page-copy',
                        handler : this.Grid_copy,
                        scope : this
                    },
                    //{   text:'Edit',
                    //    tooltip:'Edit Record',
                    //    iconCls: 'silk-page-white-edit',
                    //    //handler : this.company_class_type_grid_
                    //},
                    {   id : tabId+'_company_class_typeSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_company_class_typeGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_company_class_typePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_company_class_typePageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_company_class_typeGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_company_class_typeSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_cctTab",
                jsId : tabId+"_cctTab",
                title:  "Company Class Type",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // company_class_type grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   company_id: "New Company",
                    company_type: "New type",
                    area_type: "New Area",
                    created_date: "",
                    last_update: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // company_class_type grid copy and make it a new record
        Grid_copy: function(button, event)
        {   this.Grid.stopEditing();
            var selection = this.Grid.getSelectionModel().selection;
            if (selection)
            {   var data = this.Grid.getSelectionModel().selection.record.data;
                this.Grid.store.insert( 0,
                    new this.Records (
                    {   id: "",
                        parent_company_class_type_id: data.parent_company_class_type_id,
                        name: data.name,
                        object_id : data.object_id,
                        link: data.link,
                        status: data.status,
                        has_child : data.has_child,
                        level_degree: data.level_degree,
                        arrange_no: data.arange_no
                    }));
                this.Grid.startEditing(0, 2);
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Selected Record ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            };
        },
        // company_class_type grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/company/2/1') }}",
                    {   'x-csrf-token': alfalah.company.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // company_class_type grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/company/2/2') }}",
                {   'x-csrf-token': alfalah.company.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.company_class_type_id }
            );
        },
        // company_class_type search button
        company_class_type_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_company_class_typeSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.company.task,
                        act: alfalah.company.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.company.company = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.CityDS = alfalah.core.newDataStore(
                "{{ url('/country/3/9') }}", true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.CountryDS = alfalah.core.newDataStore(
                "{{ url('/country/2/9') }}", true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"company ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Parent.ID", width : 50,
                    dataIndex : 'parent_company_id', sortable: true,
                    tooltip:"Parent company ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Name", width : 200,
                    dataIndex : 'name', sortable: true,
                    tooltip:"company Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Address", width : 200,
                    dataIndex : 'address', sortable: true,
                    tooltip:"company Address",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "City", width : 120,
                    dataIndex : 'city_id', sortable: true,
                    tooltip:"City ID",
                    css : "background-color: #56FF00;",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store: this.CityDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'city_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.city_id = record.data.city_id;
                                    alfalah.company.company.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.city_name+ ' ['+value+']';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Country", width : 120,
                    dataIndex : 'country_id', sortable: true,
                    tooltip:"Country ID",
                    css : "background-color: #56FF00;",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store: this.CountryDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'country_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.country_id = record.data.country_id;
                                    alfalah.comany.company.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.country_name+' ['+value+']';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Telp", width : 200,
                    dataIndex : 'telp_no', sortable: true,
                    tooltip:"company Telphone",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Fax", width : 200,
                    dataIndex : 'fax_no', sortable: true,
                    tooltip:"company Telphone",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Email", width : 200,
                    dataIndex : 'email', sortable: true,
                    tooltip:"company Telphone",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Doc.Abvr", width : 50,
                    dataIndex : 'doc_abvr', sortable: true,
                    tooltip:"company Doc abreviation",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Doc.Code", width : 50,
                    dataIndex : 'doc_code', sortable: true,
                    tooltip:"company Doc.No",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Stage", width : 50,
                    dataIndex : 'stage_code', sortable: true,
                    tooltip:"company Stage Code",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status_name', sortable: true,
                    tooltip:"company Status",
                    editor :  new Ext.Editor(  new Ext.form.ComboBox(
                                                {   store: new Ext.data.SimpleStore({
                                                                       fields: ["label"],
                                                                       data : [["Active"], ["Inactive"]]
                                                                }),
                                                    displayField:"label",
                                                    valueField:"label",
                                                    mode: 'local',
                                                    typeAhead: true,
                                                    triggerAction: "all",
                                                    selectOnFocus:true,
                                                    forceSelection :true
                                               }),
                                                {autoSize:true}
                                            )
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"company Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"company Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'company_id', type: 'string'},
                {name: 'parent_company_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'doc_code', type: 'string'},
                {name: 'city_id', type: 'string'},
                {name: 'country_id', type: 'string'},
                {name: 'telp_no', type: 'string'},
                {name: 'fax_no', type: 'string'},
                {name: 'doc_abvr', type: 'string'},
                {name: 'stage_code', type: 'string'},
                {name: 'doc_code', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'company_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'company_doc_code',
                    cid: 'doc_code',
                    fieldLabel: 'Address',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'company_city',
                    cid: 'city',
                    fieldLabel: 'City',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'company_country',
                    cid: 'country',
                    fieldLabel: 'Country',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'company_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ [''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_companySearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.company_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/company/3/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();

            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : alfalah.company.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   id : tabId+'_companySaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_companyGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_companyPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_companyPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_companyGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });

            },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_companyTab",
                jsId : tabId+"_companyTab",
                title:  "Company",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // company grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   company_id: "New Company",
                    parent_company_id: "",
                    name: "New Name",
                    doc_code: "New Address",
                    city_id: "",
                    country_id: "",
                    telp_no: "",
                    fax_no: "",
                    doc_abvr: "",
                    doc_code: "",
                    status: 0,
                    created_date: "",
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // company grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/company/3/1') }}",
                    {   'x-csrf-token': alfalah.company.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // company grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/company/3/2') }}",
                {   'x-csrf-token': alfalah.company.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.company_id }
            );
        },
        // company search button
        company_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_companySaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.company.site= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.CompanyDS = alfalah.core.newDataStore(
                "{{ url('/company/2/9') }}", true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.CountryDS = alfalah.core.newDataStore(
                "{{ url('/country/2/9') }}", true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            
            this.Columns = [
                {   header: "Company", width : 120,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"Company ID",
                    css : "background-color: #56FF00;",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store: this.CompanyDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'city_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form", company_type:"EG",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.company_id = record.data.company_id;
                                    combo.gridEditor.record.data.company_name = record.data.company_name;
                                    combo.gridEditor.record.data.company_type = record.data.company_type;
                                    combo.gridEditor.record.data.name = "New Name";
                                    alfalah.company.site.Grid.getView().refresh();
                                },
                        }
                    }),
                },
                {   header: "Site.ID", width : 50,
                    dataIndex : 'site_id', sortable: true,
                    tooltip:"Site ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Site.Name", width : 150,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Site Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Country", width : 120,
                    dataIndex : 'country_id', sortable: true,
                    tooltip:"Country ID",
                    css : "background-color: #56FF00;",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store: this.CountryDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'country_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.country_id = record.data.country_id;
                                    alfalah.company.site.Grid.getView().refresh();
                                },
                        }
                    }),
                },
                {   header: "Company.Type", width : 150,
                    dataIndex : 'company_type', sortable: true,
                    tooltip:"Company Type",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.company_type_name+ ' ['+value+']';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Area", width : 100,
                    dataIndex : 'area_type', sortable: true,
                    tooltip:"company Status",
                    editor :  new Ext.Editor(  
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["Label", "key"],
                                               data : [["Local", 0], ["Foreign", 1]]
                                        }),
                            displayField:"Label",
                            valueField:"key",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    ),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.comboRenderer(this.editor.field, value); }
                },
                {   header: "Status", width : 100,
                    dataIndex : 'status', sortable: true,
                    tooltip:"company Status",
                    editor :  new Ext.Editor(  
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["label", "key"],
                                               data : [["Active", 0], ["Inactive", 1]]
                                        }),
                            displayField:"label",
                            valueField:"key",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    ),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.comboRenderer(this.editor.field, value); }
                },
                {   header: "Doc.Code", width : 150,
                    dataIndex : 'doc_code', sortable: true,
                    tooltip:"Site Doc.Code",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"site Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"site Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'company_id', type: 'string'},
                {name: 'site_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'country_id', type: 'string'},
                {name: 'company_type', type: 'string'},
                {name: 'area_type', type: 'string'},
                {name: 'status', type: 'string'},
                {name: 'doc_code', type: 'string'},
                {name: 'created_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'site_company_id',
                    cid: 'company_id',
                    fieldLabel: 'Company',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'site_site_id',
                    cid: 'site_id',
                    fieldLabel: 'Site.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'site_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_siteSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.site_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/company/4/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.company.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   id : tabId+'_siteSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_siteGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_sitePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_sitePageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_siteGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_siteTab",
                jsId : tabId+"_siteTab",
                title:  "Site",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                //new Ext.TabPanel(
                //    { id:'center-panel',
                //      region:'center'
                //    })
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // site grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   company_id: "New Company",
                    site_id: "New Site",
                    name : "New Name",
                    country_id : "",
                    company_type: "",
                    area_type: "",
                    status: 0,
                    doc_date : "",
                    created_date: "",
                    last_update: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // site grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/company/4/1') }}",
                    {   'x-csrf-token': alfalah.company.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // site grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/company/4/2') }}",
                {   'x-csrf-token': alfalah.company.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.id }
            );
        },
        // site search button
        site_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_siteSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.company.department = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'dept_id', sortable: true,
                    tooltip:"department ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Parent.ID", width : 50,
                    dataIndex : 'parent_dept_id', sortable: true,
                    tooltip:"Parent department ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Name", width : 200,
                    dataIndex : 'name', sortable: true,
                    tooltip:"department Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Abreviation", width : 100,
                    dataIndex : 'abreviation', sortable: true,
                    tooltip:"department Abreviation",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Type", width : 100,
                    dataIndex : 'type', sortable: true,
                    tooltip:"Department Type",
                    editor :  new Ext.Editor(  
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                            fields: ["Label", "key"],
                                            data : [
                                                ["Division", 0], 
                                                ["Department", 1], 
                                                ["Section", 2]]
                                        }),
                            displayField:"Label",
                            valueField:"key",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    ),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.comboRenderer(this.editor.field, value); }
                },
                {   header: "Description", width : 200,
                    dataIndex : 'description', sortable: true,
                    tooltip:"department Telphone",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Dept.Email", width : 200,
                    dataIndex : 'dept_email', sortable: true,
                    tooltip:"department Email",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Acct.Class", width : 50,
                    dataIndex : 'acct_class', sortable: true,
                    tooltip:"department Acct.Class",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"department Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"department Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'dept_id', type: 'string'},
                {name: 'parent_dept_id', type: 'string'}, 
                {name: 'name', type: 'string'},
                {name: 'abreviation', type: 'string'},
                {name: 'type', type: 'string'},
                {name: 'description', type: 'string'},
                {name: 'dept_email', type: 'string'},
                {name: 'acct_class', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'department_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'department_doc_code',
                    cid: 'doc_code',
                    fieldLabel: 'Address',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'department_type',
                    cid: 'type',
                    fieldLabel: 'Type',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['Label'],
                        data : [ [''], ['Division'], ['Department'], ['Section']]
                    }),
                    displayField:'Label',
                    valueField :'Label',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Department'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_departmentSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.department_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/company/5/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();

            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : alfalah.company.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   id : tabId+'_departmentSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_departmentGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_departmentPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_departmentPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_departmentGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });

            },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_departmentTab",
                jsId : tabId+"_departmentTab",
                title:  "Department",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // department grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   dept_id: 0,
                    parent_dept_id: 0,
                    name: "New Name",
                    abreviation: "",
                    type: "",
                    description: "",
                    dept_email: "",
                    acct_class: "",
                    created_date: "",
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // department grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/company/5/1') }}",
                    {   'x-csrf-token': alfalah.company.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // department grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/company/5/2') }}",
                {   'x-csrf-token': alfalah.company.sid }, 
                {   id: this.Grid.getSelectionModel().selection.record.data.dept_id  }
            );
        },
        // department search button
        department_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_departmentSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.company.site_department = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;
    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.company.centerPanel.container.dom.clientHeight;
            this.Columns = [
                {   header: "Comp.ID", width : 50,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"company ID"
                },
                {   header: "Site.ID", width : 80,
                    dataIndex : 'site_id', sortable: true,
                    tooltip:"Site.ID"
                },
                {   header: "Site.Name", width : 150,
                    dataIndex : 'site_name', sortable: true,
                    tooltip:"Site Name"
                },
                {   header: "Dept.ID", width : 80,
                    dataIndex : 'dept_id', sortable: true,
                    tooltip:"Dept.ID"
                },
                {   header: "Dept.Name", width : 150,
                    dataIndex : 'dept_name', sortable: true,
                    tooltip:"Dept Name"
                },
                {   header: "Email", width : 100,
                    dataIndex : 'site_dept_email', sortable: true,
                    tooltip:"Site.Dept Email",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"company Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"company Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'company_id', type: 'string'},
                {name: 'site_id', type: 'string'},
                {name: 'dept_id', type: 'string'},
                {name: 'site_dept_email', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'sd_company_id',
                    cid : 'company_id',
                    fieldLabel: 'Comp.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sd_site_id',
                    cid : 'site_id',
                    fieldLabel: 'Site.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sd_site_name',
                    cid : 'site_name',
                    fieldLabel: 'Site.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sd_dept_id',
                    cid : 'dept_id',
                    fieldLabel: 'Dept.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sd_dept_name',
                    cid : 'dept_name',
                    fieldLabel: 'Dept.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_SDSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.site_department_search_handler,
                scope : this
            });
            /**
             * MAIN-GRID
            */
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/company/6/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.siteComboDataStore = alfalah.core.newDataStore(
                "{{ url('/company/4/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.siteComboDataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.DataStore.reload(); },
                        scope : this
                    },
                    //{   text:'Add',
                    //    tooltip:'Add Record',
                    //    iconCls: 'silk-add',
                    //    handler : this.Grid_add,
                    //    scope : this
                    //},
                    {   id : tabId+'_SDSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    '-','Selected Site ',
                    new Ext.form.ComboBox(
                    {   store: this.siteComboDataStore,
                        typeAhead: true,
                        id: tabId+"_SDCombo",
                        width: 250,
                        displayField: 'name',
                        valueField: 'site_id',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        listeners : { scope : this,
                            'select' : function (a,records,c)
                            {   this.DataStore.baseParams = {
                                    s:"form",
                                    limit  : this.page_limit, start:this.page_start,
                                    company_id: records.data.company_id,
                                    site_id: records.data.site_id };
                                this.DataStore.reload();
                            }
                        }
                    }),
                    {   //text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.companyComboDataStore.reload(); },
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_SDGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_SDPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_SDPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_SDGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });

            /**
             * SOUTH-GRID
            */
            this.SouthSearchs = [
                {   id: 'sds_dept_name',
                    cid : 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sds_dept_desc',
                    cid : 'description',
                    fieldLabel: 'Description',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SouthSearchBtn = new Ext.Button(
            {   id : tabId+"_SDSouthSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.site_department_south_search_handler,
                scope : this
            });
            this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/company/5/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.SouthDataStore.load();
            var cbSelModel = new Ext.grid.CheckboxSelectionModel();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.SouthDataStore,
                columns: [ cbSelModel,
                    {   header: "ID", width : 100,
                        dataIndex : 'dept_id', sortable: true,
                        tooltip:"Department ID"
                    },
                    
                    {   header: "Name", width : 100,
                        dataIndex : 'name', sortable: true,
                        tooltip:"Department Name"
                    },
                    {   header: "Description", width : 350,
                        dataIndex : 'description', sortable: true,
                        tooltip:"Description",
                    },
                ],
                selModel: cbSelModel,
                //selModel: new Ext.grid.RowSelectionModel(),
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add Department',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.SouthGrid_add,
                        scope : this
                    }
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_SDSGridBBar',
                    store: this.SouthDataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_SDSGridPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_SDSGridPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_SDSGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SouthSearchBtn.handler.call(this.SouthSearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_SDTab",
                jsId : tabId+"_SDTab",
                title:  "Site Department",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'S E AR C H',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    }]
                },
                {// lazily created panel (xtype:'panel' is default)
                    region: 'south',
                    title: 'Department',
                    split: true,
                    height : this.region_height/2,
                    // minSize: this.region_height/4,
                    // maxSize: this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    layout: 'border',
                    items:[
                    {   region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.SouthGrid]
                    },
                    {   region: 'east',     // position for region
                        title: 'S E A R C H',
                        split:true,
                        width: 200,
                        minSize: 175,
                        maxSize: 400,
                        collapsible: true,
                        layout : 'fit',
                        items:[
                        {   //title: 'S E A R C H',
                            labelWidth: 50,
                            defaultType: 'textfield',
                            xtype: 'form',
                            frame: true,
                            autoScroll : true,
                            items : [ this.SouthSearchs, this.SouthSearchBtn]
                        }]
                    }]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // site_department grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/company/6/1') }}",
                    {   'x-csrf-token': alfalah.company.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // site_department grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/company/6/2') }}",
                {   'x-csrf-token': alfalah.company.sid }, 
                {   cid: this.Grid.getSelectionModel().selection.record.data.company_id,
                    sid: this.Grid.getSelectionModel().selection.record.data.site_id,
                    did: this.Grid.getSelectionModel().selection.record.data.dept_id
                }
            );
        },
        // south grid add new record
        SouthGrid_add: function(button, event)
        {   var the_combo = Ext.getCmp(this.tabId+"_SDCombo");
            var the_data = the_combo.store.getAt(the_combo.selectedIndex);
            if (the_data)
            {   var the_records = this.SouthGrid.getSelectionModel().selections.items;
                console.log(the_data);
                console.log(the_records);
                Ext.each(the_records,
                    function(the_record)
                    {   this.Grid.store.insert( 0,
                        new this.Records (
                        {   id: "",
                            company_id: the_data.data.company_id,
                            site_id : the_data.data.site_id,
                            dept_id : the_record.data.dept_id,
                            site_dept_email : 'New Site Email',
                            created_date : ''
                        }));
                    }, this
                );
            }
            else
            {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Site Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            };
        },
        // site_department search button
        site_department_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_SDSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.company.task,
                        act: alfalah.company.act,
                        a:6, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
        // site_department_south search button
        site_department_south_search_handler : function(button, event)
        {   var the_search = true;

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.SouthSearchs);
                //console.log(this.SouthSearchs);
                //console.log(the_parameter);
                this.SouthDataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.company.task,
                        act: alfalah.company.act,
                        a:6, b:9, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.SouthDataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.company.site_building = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;
    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.company.centerPanel.container.dom.clientHeight;
            this.SiteDS = alfalah.core.newDataStore(
                "{{ url('/company/4/9') }}", true,
                {   s:"form", limit:this.page_limit, start:this.page_start }
            );
            this.Columns = [
                {   header: "Comp.ID", width : 50,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"company ID"
                },
                {   header: "Site.ID", width : 120,
                    dataIndex : 'site_id', sortable: true,
                    tooltip:"Site ID",
                    css : "background-color: #56FF00;",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store: this.SiteDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'site_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.company_id = record.data.company_id;
                                    combo.gridEditor.record.data.site_id    = record.data.site_id;
                                    alfalah.company.site.Grid.getView().refresh();
                                },
                        }
                    }),
                },
                {   header: "Site.Name", width : 150,
                    dataIndex : 'site_name', sortable: true,
                    tooltip:"Site Name"
                },
                {   header: "Build.ID", width : 80,
                    dataIndex : 'building_id', sortable: true,
                    tooltip:"Building.ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Build.Name", width : 150,
                    dataIndex : 'name', sortable: true,
                    tooltip:"Building Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"company Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"company Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'company_id', type: 'string'},
                {name: 'site_id', type: 'string'},
                {name: 'site_name', type: 'string'},
                {name: 'building_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'sb_company_id',
                    cid : 'company_id',
                    fieldLabel: 'Comp.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sb_site_id',
                    cid : 'site_id',
                    fieldLabel: 'Site.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sb_site_name',
                    cid : 'site_name',
                    fieldLabel: 'Site.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sb_building_id',
                    cid : 'building_id',
                    fieldLabel: 'Build.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sb_name',
                    cid : 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_SBSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.site_building_search_handler,
                scope : this
            });
            /**
             * MAIN-GRID
            */
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/company/12/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    //{   text:'Add',
                    //    tooltip:'Add Record',
                    //    iconCls: 'silk-add',
                    //    handler : this.Grid_add,
                    //    scope : this
                    //},
                    {   id : tabId+'_SBSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   //text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.companyComboDataStore.reload(); },
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_SBGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_SBPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_SBPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_SBGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_SBTab",
                jsId : tabId+"_SBTab",
                title:  "Site Building",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'S E AR C H',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    }]
                },
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // site building grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   company_id: "New Company",
                    site_id: "New Site",
                    building_id : "New ID",
                    name : "New Name",
                    created_date: "",
                    last_update: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // site_building grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/company/12/1') }}",
                    {   'x-csrf-token': alfalah.company.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // site_building grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/company/12/2') }}",
                {   'x-csrf-token': alfalah.company.sid }, 
                {   cid: this.Grid.getSelectionModel().selection.record.data.company_id,
                    sid: this.Grid.getSelectionModel().selection.record.data.site_id,
                    did: this.Grid.getSelectionModel().selection.record.data.building_id
                }
            );
        },
        // south grid add new record
        SouthGrid_add: function(button, event)
        {   var the_combo = Ext.getCmp(this.tabId+"_SBCombo");
            var the_data = the_combo.store.getAt(the_combo.selectedIndex);
            if (the_data)
            {   var the_records = this.SouthGrid.getSelectionModel().selections.items;
                console.log(the_data);
                console.log(the_records);
                Ext.each(the_records,
                    function(the_record)
                    {   this.Grid.store.insert( 0,
                        new this.Records (
                        {   id: "",
                            company_id: the_data.data.company_id,
                            site_id : the_data.data.site_id,
                            building_id : the_record.data.building_id,
                            name : 'New Site Email',
                            created_date : ''
                        }));
                    }, this
                );
            }
            else
            {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Site Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            };
        },
        // site_building search button
        site_building_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_SBSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.company.task,
                        act: alfalah.company.act,
                        a:6, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
        // site_building_south search button
        site_building_south_search_handler : function(button, event)
        {   var the_search = true;

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.SouthSearchs);
                //console.log(this.SouthSearchs);
                //console.log(the_parameter);
                this.SouthDataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.company.task,
                        act: alfalah.company.act,
                        a:6, b:9, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.SouthDataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.company.documents = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.DeptDS = alfalah.core.newDataStore(
                "{{ url('/company/5/9') }}", true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Columns = [
                {   header: "ID", width : 50,
                    dataIndex : 'doc_id', sortable: true,
                    tooltip:"documents ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Code", width : 50,
                    dataIndex : 'doc_code', sortable: true,
                    tooltip:"Parent documents ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Name", width : 200,
                    dataIndex : 'name', sortable: true,
                    tooltip:"documents Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Description", width : 200,
                    dataIndex : 'description', sortable: true,
                    tooltip:"documents Telphone",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Dept", width : 120,
                    dataIndex : 'owner_dept_id', sortable: true,
                    tooltip:"Dept ID",
                    css : "background-color: #56FF00;",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store: this.DeptDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'dept_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.owner_dept_id = record.data.dept_id;
                                    combo.gridEditor.record.data.dept_name = record.data.name;
                                    alfalah.company.company.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.dept_name+ ' ['+value+']';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "format", width : 150,
                    dataIndex : 'format_doc_no', sortable: true,
                    tooltip:"documents format no",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Seq.Digit", width : 100,
                    dataIndex : 'seq_digit', sortable: true,
                    tooltip:"digit of sequence number",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"documents Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"documents Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'doc_id', type: 'string'},
                {name: 'doc_code', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'description', type: 'string'},
                {name: 'owner_dept_id', type: 'string'},
                {name: 'format_doc_no', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'documents_doc_id',
                    cid: 'doc_id',
                    fieldLabel: 'Doc.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'documents_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'documents_doc_code',
                    cid: 'doc_code',
                    fieldLabel: 'Doc.Code',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                }
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_documentsSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.documents_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/company/7/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();

            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : alfalah.company.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   id : tabId+'_documentsSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_documentsGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_documentsPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_documentsPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_documentsGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });

            },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_documentsTab",
                jsId : tabId+"_documentsTab",
                title:  "Documents",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // documents grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   doc_id: "New documents",
                    doc_code: "",
                    name: "New Name",
                    description: "New Desc",
                    owner_dept_id: "",
                    format_doc_no: "",
                    created_date: "",
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // documents grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/company/7/1') }}",
                    {   'x-csrf-token': alfalah.company.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // documents grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/company/7/2') }}",
                    {   'x-csrf-token': alfalah.company.sid }, 
                    {   id: this.Grid.getSelectionModel().selection.record.data.doc_id }
                );
        },
        // documents search button
        documents_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_documentsSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.company.site_documents = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;

    this.EastGrid;
    this.EastDataStore;
    this.SouthGrid;
    this.SouthDataStore;
    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        region_height : 0,
        tabId : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.tabId = tabId;
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.region_height = alfalah.company.centerPanel.container.dom.clientHeight;
            this.Columns = [
                {   header: "Comp.ID", width : 50,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"company ID"
                },
                {   header: "Site.ID", width : 80,
                    dataIndex : 'site_id', sortable: true,
                    tooltip:"Site.ID"
                },
                {   header: "Site.Name", width : 150,
                    dataIndex : 'site_name', sortable: true,
                    tooltip:"Site Name"
                },
                {   header: "Dept.ID", width : 80,
                    dataIndex : 'dept_id', sortable: true,
                    tooltip:"Dept.ID"
                },
                {   header: "Dept.Name", width : 100,
                    dataIndex : 'dept_name', sortable: true,
                    tooltip:"Dept Name"
                },
                {   header: "Doc.ID", width : 80,
                    dataIndex : 'doc_id', sortable: true,
                    tooltip:"Doc.ID"
                },
                {   header: "Doc.Name", width : 150,
                    dataIndex : 'doc_name', sortable: true,
                    tooltip:"Doc Name"
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"company Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'last_update', sortable: true,
                    tooltip:"company Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'string'},
                {name: 'company_id', type: 'string'},
                {name: 'site_id', type: 'string'},
                {name: 'doc_id', type: 'string'},
                {name: 'site_owner_dept_id', type: 'string'},
                {name: 'created_date', type: 'date'},
                {name: 'last_update', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'sdo_company_id',
                    cid : 'company_id',
                    fieldLabel: 'Comp.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sdo_site_id',
                    cid : 'site_id',
                    fieldLabel: 'Site.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sdo_site_name',
                    cid : 'site_name',
                    fieldLabel: 'Site.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sdo_doc_id',
                    cid : 'doc_id',
                    fieldLabel: 'Dept.ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sdo_dept_name',
                    cid : 'dept_name',
                    fieldLabel: 'Dept.Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_SDOSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.site_documents_search_handler,
                scope : this
            });
            /**
             * MAIN-GRID
            */
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/company/8/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.siteComboDataStore = alfalah.core.newDataStore(
                "{{ url('/company/6/9') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.siteComboDataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : (this.region_height/2)-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.DataStore.reload(); },
                        scope : this
                    },
                    //{   text:'Add',
                    //    tooltip:'Add Record',
                    //    iconCls: 'silk-add',
                    //    handler : this.Grid_add,
                    //    scope : this
                    //},
                    {   id : tabId+'_SDOSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    '-','Selected Site ',
                    new Ext.form.ComboBox(
                    {   store: this.siteComboDataStore,
                        typeAhead: true,
                        id: tabId+"_SDOCombo",
                        width: 250,
                        displayField: 'display',
                        valueField: 'display',
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        listeners : { scope : this,
                            'select' : function (a,records,c)
                            {   this.DataStore.baseParams = {
                                    s:"form",
                                    limit  : this.page_limit, start:this.page_start,
                                    company_id: records.data.company_id,
                                    site_id: records.data.site_id,
                                    dept_id: records.data.dept_id };
                                this.DataStore.removeAll();
                                this.DataStore.reload();
                            }
                        }
                    }),
                    {   //text:'Refresh',
                        tooltip:'Refresh Record',
                        iconCls: 'silk-arrow-refresh',
                        handler : function(){ this.companyComboDataStore.reload(); },
                        scope : this
                    },
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_SDOGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_SDOPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_SDOPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_SDOGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( Ext.getCmp(tabId+'_SDOGridBBar').getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });

            /**
             * SOUTH-GRID
            */
            this.SouthSearchs = [
                {   id: 'sdos_dept_name',
                    cid : 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'sdos_dept_desc',
                    cid : 'description',
                    fieldLabel: 'Description',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SouthSearchBtn = new Ext.Button(
            {   id : tabId+"_SDOSouthSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.site_documents_south_search_handler,
                scope : this
            });
            this.SouthDataStore = alfalah.core.newDataStore(
                "{{ url('/company/7/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.SouthDataStore.load();
            var cbSelModel = new Ext.grid.CheckboxSelectionModel();
            this.SouthGrid = new Ext.grid.EditorGridPanel(
            {   store:  this.SouthDataStore,
                columns: [ cbSelModel,
                    {   header: "ID", width : 100,
                        dataIndex : 'doc_id', sortable: true,
                        tooltip:"documents ID"
                    },
                    {   header: "Name", width : 100,
                        dataIndex : 'name', sortable: true,
                        tooltip:"documents Name"
                    },
                    {   header: "Description", width : 350,
                        dataIndex : 'description', sortable: true,
                        tooltip:"Description",
                    },
                    {   header: "Department", width : 100,
                        dataIndex : 'dept_name', sortable: true,
                        tooltip:"Department Name"
                    },
                ],
                selModel: cbSelModel,
                //selModel: new Ext.grid.RowSelectionModel(),
                loadMask: true,
                height : (this.region_height/2)-50,
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add documents',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.SouthGrid_add,
                        scope : this
                    }
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_SDOSGridBBar',
                    store: this.SouthDataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_SDOSGridPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_SDOSGridPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_SDOSBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SouthSearchBtn.handler.call(this.SouthSearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_SDOTab",
                jsId : tabId+"_SDOTab",
                title:  "Site Documents",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                },
                {   region: 'east',     // position for region
                    title: 'S E AR C H',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    }]
                },
                {// lazily created panel (xtype:'panel' is default)
                    region: 'south',
                    title: 'documents',
                    split: true,
                    height : this.region_height/2,
                    // minSize: this.region_height/4,
                    // maxSize: this.region_height/2,
                    collapsible: true,
                    margins: '0 0 0 0',
                    layout: 'border',
                    items:[
                    {   region: 'center',     // center region is required, no width/height specified
                        xtype: 'container',
                        layout: 'fit',
                        items:[this.SouthGrid]
                    },
                    {   region: 'east',     // position for region
                        title: 'S E A R C H',
                        split:true,
                        width: 200,
                        minSize: 175,
                        maxSize: 400,
                        collapsible: true,
                        layout : 'fit',
                        items:[
                        {   //title: 'S E A R C H',
                            labelWidth: 50,
                            defaultType: 'textfield',
                            xtype: 'form',
                            frame: true,
                            autoScroll : true,
                            items : [ this.SouthSearchs, this.SouthSearchBtn]
                        }]
                    }]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // site_documents grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data = alfalah.core.getDetailData(the_records);
                alfalah.core.submitGrid(
                    this.DataStore, 
                    "{{ url('/company/8/1') }}",
                    {   'x-csrf-token': alfalah.company.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
        // site_documents grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                this.DataStore, 
                "{{ url('/company/8/2') }}",
                {   'x-csrf-token': alfalah.company.sid }, 
                {   cid: this.Grid.getSelectionModel().selection.record.data.company_id,
                    sid: this.Grid.getSelectionModel().selection.record.data.site_id,
                    did: this.Grid.getSelectionModel().selection.record.data.doc_id
                }
            );
        },
        // south grid add new record
        SouthGrid_add: function(button, event)
        {   var the_combo = Ext.getCmp(this.tabId+"_SDOCombo");
            var the_data = the_combo.store.getAt(the_combo.selectedIndex);
            if (the_data)
            {   var the_records = this.SouthGrid.getSelectionModel().selections.items;
                Ext.each(the_records,
                    function(the_record)
                    {   if (the_data.data.dept_id == the_record.data.owner_dept_id)
                        {   this.Grid.store.insert( 0,
                                new this.Records (
                                {   id: "",
                                    company_id: the_data.data.company_id,
                                    site_id : the_data.data.site_id,
                                    dept_id : the_data.data.dept_id,
                                    dept_name : the_record.data.dept_name,
                                    doc_id : the_record.data.doc_id,
                                    doc_name : the_record.data.name,
                                    created_date : ''
                                }));
                        }
                        else
                        {   Ext.Msg.show(
                            {   title:'E R R O R ',
                                msg: 'Document belong to other department ! ',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });

                        };
                    }, this
                );
            }
            else
            {   Ext.Msg.show(
                {   title:'I N F O ',
                    msg: 'No Site Selected ! ',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            };
        },
        // site_documents search button
        site_documents_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_SDOSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.company.task,
                        act: alfalah.company.act,
                        a:6, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
        // site_documents_south search button
        site_documents_south_search_handler : function(button, event)
        {   var the_search = true;

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.SouthSearchs);
                //console.log(this.SouthSearchs);
                //console.log(the_parameter);
                this.SouthDataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.company.task,
                        act: alfalah.company.act,
                        a:6, b:9, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.SouthDataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
alfalah.company.warehouse = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.SearchBtn;
    this.Searchs;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            this.SiteDS = alfalah.core.newDataStore(
                "{{ url('/company/4/9') }}", true,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            this.Columns = [
                {   header: "Company", width : 50,
                    dataIndex : 'company_id', sortable: true,
                    tooltip:"Company ID"
                },
                {   header: "Site", width : 200,
                    dataIndex : 'site_id', sortable: true,
                    tooltip:"Site ID",
                    css : "background-color: #56FF00;",
                    // locked : true,
                    editor: new Ext.form.ComboBox(
                    {   store: this.SiteDS,
                        typeAhead: false,
                        width: 250,
                        displayField: 'display',
                        valueField: 'site_id',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'beforequery' : function (combo, query, forceAll, cancel)
                                {   combo.combo.store.baseParams = {
                                        s:"form",
                                        limit:this.page_limit, start:this.page_start };
                                },
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.company_id = record.data.company_id;
                                    combo.gridEditor.record.data.site_id = record.data.dept_id;
                                    combo.gridEditor.record.data.site_name = record.data.name;
                                    alfalah.company.company.Grid.getView().refresh();
                                },
                        }
                    }),
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = record.data.site_name+ ' ['+value+']';
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "WH.ID", width : 50,
                    dataIndex : 'warehouse_id', sortable: true,
                    tooltip:"Warehouse ID",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Name", width : 250,
                    dataIndex : 'name', sortable: true,
                    tooltip:"warehouse Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Flag", width : 50,
                    dataIndex : 'wh_flag', sortable: true,
                    tooltip:"warehouse Flag",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Type", width : 50,
                    dataIndex : 'type', sortable: true,
                    tooltip:"Warehouse Type",
                    editor :  new Ext.Editor(  
                                new Ext.form.ComboBox(
                                {   store: new Ext.data.SimpleStore({
                                       fields: ["label"],
                                       data : [ ["COCO"], ["GENERAL"]]
                                    }),
                                    displayField:"label",
                                    valueField:"label",
                                    mode: 'local',
                                    typeAhead: true,
                                    triggerAction: "all",
                                    selectOnFocus:true,
                                    forceSelection :true
                               }),
                                {autoSize:true}
                            )
                },
                {   header: "Level", width : 50,
                    dataIndex : 'level', sortable: true,
                    tooltip:"Warehouse Level",
                    editor :  new Ext.Editor(  
                                new Ext.form.ComboBox(
                                {   store: new Ext.data.SimpleStore({
                                       fields: ["label"],
                                       data : [ ["1st"],["2nd"]]
                                    }),
                                    displayField:"label",
                                    valueField:"label",
                                    mode: 'local',
                                    typeAhead: true,
                                    triggerAction: "all",
                                    selectOnFocus:true,
                                    forceSelection :true
                               }),
                                {autoSize:true}
                            )
                },
                {   header: "WH.Name", width : 200,
                    dataIndex : 'wh_name', sortable: true,
                    tooltip:"warehouse Name",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Created", width : 150,
                    dataIndex : 'created_date', sortable: true,
                    tooltip:"warehouse Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 150,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"warehouse Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'company_id', type: 'string'},
                {name: 'site_id', type: 'string'},
                {name: 'warehouse_id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'wh_flag', type: 'string'},
                {name: 'warehouse_type', type: 'string'},
                {name: 'wh_name', type: 'string'},
                {name: 'created_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'warehouse_company_id',
                    cid: 'company_id',
                    fieldLabel: 'Company',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'warehouse_site_id',
                    cid: 'site_id',
                    fieldLabel: 'Site',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'warehouse_name',
                    cid: 'name',
                    fieldLabel: 'Name',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_warehouseSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.warehouse_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "{{ url('/company/9/0') }}", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                loadMask: true,
                height : alfalah.company.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   id : tabId+'_warehouseSaveBtn',
                        text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },
                    '-',
                    {   text:'Delete',
                        tooltip:'Delete Record',
                        iconCls: 'silk-delete',
                        handler : this.Grid_remove,
                        scope : this
                    },
                    '-',
                    {   print_type : "pdf",
                        text:'Print PDF',
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },'-',
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_warehouseGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_warehousePageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_warehousePageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_warehouseGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });

            },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_warehouseTab",
                jsId : tabId+"_warehouseTab",
                title:  "Warehouse",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'ToolBox',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'accordion',
                    items:[
                    {   title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : [ this.Searchs, this.SearchBtn]
                    },
                    { title : 'Setting'
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
        // warehouse grid add new record
        Grid_add: function(button, event)
        {   this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   company_id: "New warehouse",
                    site_id: "",
                    name: "New Name",
                    wh_flag: "New Desc",
                    site_id: "",
                    warehouse_type: "",
                    created_date: "",
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 2);
        },
        // warehouse grid save records
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   var json_data =alfalah.core.getDetailData(this.DataStore.getModifiedRecords());
                alfalah.core.submitGrid(
                    alfalah.company.warehouse.DataStore,
                    "{{ url('/company/9/1') }}",
                    {   'x-csrf-token': alfalah.company.sid },
                    {   json : Ext.encode(json_data),
                });
            };
        },
        // warehouse grid delete records
        Grid_remove: function(button, event)
        {   this.Grid.stopEditing();
            alfalah.core.submitGrid(
                alfalah.company.warehouse.DataStore,
                "{{ url('/company/9/2') }}",
                {   'x-csrf-token': alfalah.company.sid },
                {   id: this.Grid.getSelectionModel().selection.record.data.company_id  }
            );
        },
        // warehouse search button
        warehouse_search_handler : function(button, event)
        {   var the_search = true;
            //console.log(this);
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_warehouseSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.company.initialize, alfalah.company);
// end of file
</script>
<div>&nbsp;</div>