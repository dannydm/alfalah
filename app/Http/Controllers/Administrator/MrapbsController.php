<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Administrator\Mrapbs;
use App\Models\Administrator\Sumberdana;
use App\Models\Administrator\Mprogram;
use App\Models\Administrator\Department;

use Maatwebsite\Excel\Facades\Excel;

class MrapbsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $records;
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
        // echo "HUAHAHAHAHA __construct<BR>";
        // print_r($request);
        //   $response = array(
        //     'status' => 'success',
        //     'msg' => $request->message,
        // );
        // return response()->json($response); 
    }

    public function index($task, $substask)
    {   // dd($user); exit;
        // dd($this->request->user()->get('user.id')); 
        // dd($this->request->session()); 
        // $session = $this->request->session();
        switch ($task) {   // master data
            case 0:
                return $this->master($substask);
                break;
                // menu tab
            case 1:
                return $this->crudMrapbs($substask);
                break;
            case 2:
                return $this->crudSumberdana($substask);
                break;
            case 3:
                return $this->crudMprogram($substask);
                break;
    
        };
    }
    /**
     * Create a new controller instance.
     */
    public function master($subtask)
    {
        $session = $this->request->session();
        switch ($subtask) {
            case 0:
                $result = view('administrator/mrapbs' . config('app.system_skin'))
                    ->with("USER_ID", $session->get("user_id"))
                    ->with("USERNAME", $session->get("username"))
                    ->with("USERSITE", $session->get("site"))
                    ->with("TABID", $this->request->get("tabId"))
                    ->with("COMPANY_CODE", $this->companies['company_code'])
                    ->with("COMPANY_NAME", $this->companies['company_name']);
                break;
            case 1:
                $result = Menu::ActiveMenu(null);
                break;
            case 10: // search page data
                $result = $this->rsExtJson(Page::SearchPage($this->request), 75, 1);
                break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Mrapbs
     */
    public function crudMrapbs($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = Mrapbs::mrapbsSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->crudMrapbs(3);
                        } else {
                            $this->crudMrapbs(4);
                        };
                    } else {
                        $result = $this->rsJson($this->records, $this->request->limit, $this->request->start);
                    };
                };
                break;
            case 1: // save data
                {
                    $session = $this->request->session();
                    $json_data = json_decode(stripslashes($this->request->json));
                    $result = Mrapbs::mrapbsSave(
                        $json_data,
                        $this->request->btn,
                        $session->get("user_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2: // delete data
                {
                    $result = Mrapbs::mrapbsDelete($this->request);
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                    $results = $this->records;
                    $data = array();
                    $data[] = array("MRAPBS_ID","PARENT_MRAPBS_ID", "NAME", "TYPE","DESCRIPTION"
                    , "STATUS", "CREATED","DEPT_ID");
                    foreach ($results as $result) {
                        $data[] = array(
                            $result->mrapbs_id,
                            $result->parent_mrapbs_id,
                            $result->name,
                            $result->type,
                            $result->description,
                            Mrapbs::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                            $result->dept_id,
                        );
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Master Organsasi dan Urusan',
                        'Mrapbs',
                        array("A" => 10,  "B" => 10,  "C" => 30, "D" => 10, "E" => 10,"F" => "10","G" => 10,"H" => 10),
                        $data,
                        'A',
                        'H'
                    );
                    $this->report->download('xls');
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        Mrapbs::mrapbsSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Msumberdana
     */
    public function crudSumberdana($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = Sumberdana::sumberdanaSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->crudSumberdana(3);
                        } else {
                            $this->crudSumberdana(4);
                        };
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
                break;
            case 1: // save data
                {
                    $session = $this->request->session();
                    $json_data = json_decode(stripslashes($this->request->json));
                    $result = Sumberdana::sumberdanaSave(
                        $json_data,
                        $this->request->btn,
                        $session->get("user_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2: // delete data
                {
                    $result = Sumberdana::sumberdanaDelete($this->request);
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                    $results = $this->records;
                    $data = array();
                    $data[] = array("SUMBERDANA_ID", "NAME", "STATUS", "CREATED");
                    foreach ($results as $result) {
                        $data[] = array(
                            $result->sumberdana_id,
                            $result->name,
                            Sumberdana::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                        );
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Master Sumberdana',
                        'm_sumberdana',
                        array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15),
                        $data,
                        'A',
                        'D'
                    );
                    $this->report->download('xls');
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        Sumberdana::sumberdanaSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        };
        return $result;
    }

    public function crudMprogram($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = Mprogram::mprogramSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->crudMprogram(3);
                        } else {
                            $this->crudMprogram(4);
                        };
                    } else {
                        $result = $this->rsJson($this->records, $this->request->limit, $this->request->start);
                    };
                };
                break;
            case 1: // save data
                {
                    $session = $this->request->session();
                    $json_data = json_decode(stripslashes($this->request->json));
                    $result = Mprogram::mprogramSave(
                        $json_data,
                        $this->request->btn,
                        $session->get("user_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2: // delete data
                {
                    $result = Mprogram::MprogramDelete($this->request);
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                    $results = $this->records;
                    $data = array();
                    $data[] = array("MPROGRAM_ID","PARENT_MPROGRAM_ID", "NAME","TYPE" 
                    ,"DESCRIPTION", "STATUS", "CREATED","DEPT_ID");
                    foreach ($results as $result) {
                        $data[] = array(
                            $result->mprogram_id,
                            $result->parent_mprogram_id,
                            $result->name,
                            $result->type,
                            $result->description,
                            Mprogram::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                            $result->dept_id,
                        );
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Master Program dan Kegiatan',
                        'Mprogram',
                        array("A" => 10,  "B" => 10,  "C" => 30, "D" => 10, "E" => 10,"F" => "10","G" => 10,"H" => 10),
                        $data,
                        'A',
                        'H'
                    );
                    $this->report->download('xls');
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        Mprogram::MprogramSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        };
        return $result;
    }
}
