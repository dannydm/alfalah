<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Administrator\Mcoa;
use App\Models\Administrator\Mposbelanja;
use App\Models\Administrator\Mcash_bank_type;
use App\Models\Administrator\Mincome;

class McoaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $records;
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
        // echo "HUAHAHAHAHA __construct<BR>";
        // print_r($request);
        //   $response = array(
        //     'status' => 'success',
        //     'msg' => $request->message,
        // );
        // return response()->json($response); 
    }

    public function index($task, $substask)
    {   // dd($user); exit;
        // dd($this->request->user()->get('user.id')); 
        // dd($this->request->session()); 
        // $session = $this->request->session();
        switch ($task) {   // master data
            case 0:
                return $this->master($substask);
                break;
                // menu tab
            case 1:
                return $this->crudMcoa($substask);
                break;
                // menu tab
            case 2:
                return $this->crudMposbelanja($substask);
                break;
            case 3:
                return $this->crudMincome($substask);
                break;
            case 4:
                return $this->crudMcashbanktype($substask);
                break;
                        
        };
        
    }
    /**
     * Create a new controller instance.
     */
    public function master($subtask)
    {
        $session = $this->request->session();
        switch ($subtask) {
            case 0:
                $result = view('administrator/mcoa' . config('app.system_skin'))
                    ->with("USER_ID", $session->get("user_id"))
                    ->with("USERNAME", $session->get("username"))
                    ->with("USERSITE", $session->get("site"))
                    ->with("TABID", $this->request->get("tabId"))
                    ->with("COMPANY_CODE", $this->companies['company_code'])
                    ->with("COMPANY_NAME", $this->companies['company_name']);
                break;
            case 1:
                $result = Menu::ActiveMenu(null);
                break;
            case 10: // search page data
                $result = $this->rsExtJson(Page::SearchPage($this->request), 75, 1);
                break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Mcoa
     */
    public function crudMcoa($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = Mcoa::mcoaSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->crudMcoa(3);
                        } else {
                            $this->crudMcoa(4);
                        };
                    } else {
                        $result = $this->rsJson($this->records, $this->request->limit, $this->request->start);
                    };
                };
                break;
            case 1: // save data
                {
                    $session = $this->request->session();
                    $json_data = json_decode(stripslashes($this->request->json));
                    $result = Mcoa::mcoaSave(
                        $json_data,
                        $this->request->btn,
                        $session->get("user_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2: // delete data
                {
                    $result = Mcoa::mcoaDelete($this->request);
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                    $results = $this->records;
                    $data = array();
                    $data[] = array("MCOA_ID", "PARENT_MCOA_ID","NAME", "D_K","TYPE" ,"STATUS", "CREATED");
                    foreach ($results as $result) {
                        $data[] = array(
                            $result->mcoa_id,
                            $result->parent_mcoa_id,
                            $result->name,
                            $result->d_k,
                            $result->type,
                            Mcoa::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                        );
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Master Coa',
                        'mcoa',
                        array("A" => 15,  "B" => 15,  "C" => 30, "D" => 10, "E" => 10, "F" => 10,"G" => 10),
                        $data,
                        'A',
                        'G'
                    );
                    $this->report->download('xls');
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        Mcoa::mcoaSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete MPosbelanja
     */

    public function crudMposbelanja($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = Mposbelanja::MposbelanjaSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->crudMposbelanja(3);
                        } else {
                            $this->crudMposbelanja(4);
                        };
                    } else {
                        $result = $this->rsJson($this->records, $this->request->limit, $this->request->start);
                    };
                };
                break;
            case 1: // save data
                {
                    $session = $this->request->session();
                    $json_data = json_decode(stripslashes($this->request->json));

                    $result = Mposbelanja::MposbelanjaSave(
                        $json_data,
                        $this->request->btn,
                        $session->get("user_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2: // delete data
                {
                    $result = Mposbelanja::MposbelanjaDelete($this->request);
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                    $results = $this->records;
                    $data = array();
                    $data[] = array("posbelanja_ID", "NAME", "STATUS", "CREATED");
                    foreach ($results as $result) {
                        $data[] = array(
                            $result->posbelanja_id,
                            $result->name,
                            $result->type,
                            Mcoa::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                        );
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Master Currencies',
                        'Currencies',
                        array("A" => 15,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15),
                        $data,
                        'A',
                        'E'
                    );
                    $this->report->download('xls');
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        Mposbelanja::MposbelanjaSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        };
        return $result;
    }

    public function crudMincome($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = Mincome::MincomeSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->crudMincome(3);
                        } else {
                            $this->crudMincome(4);
                        };
                    } else {
                        $result = $this->rsJson($this->records, $this->request->limit, $this->request->start);
                    };
                };
                break;
            case 1: // save data
                {
                    $session = $this->request->session();
                    $json_data = json_decode(stripslashes($this->request->json));
                    $result = Mincome::MincomeSave(
                        $json_data,
                        $this->request->btn,
                        $session->get("user_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2: // delete data
                {
                    $result = Mincome::MincomeDelete($this->request); 
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                    $results = $this->records;
                    $data = array();
                    $data[] = array("INCOME_ID", "PARENT_INCOME_ID","INCOME_NAME","STATUS", "CREATED");
                    foreach ($results as $result) {
                        $data[] = array(
                            $result->mincome_id,
                            $result->parent_mincome_id,
                            $result->mincome_name,
                            Mincome::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                        );
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Master Income Type',
                        'income',
                        array("A" => 15,  "B" => 15,  "C" => 30, "D" => 10, "E" => 10, "F" => 10),
                        $data,
                        'A',
                        'F'
                    );
                    $this->report->download('xls');
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        Mincome::MincomeSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        };
        return $result;
    }

    public function crudMcashbanktype($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = Mcash_bank_type::Mcash_bank_typeSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->crudMcash_bank_type(3);
                        } else {
                            $this->crudMcash_bank_type(4);
                        };
                    } else {
                        $result = $this->rsJson($this->records, $this->request->limit, $this->request->start);
                    };
                };
                break;
            case 1: // save data
                {
                    $session = $this->request->session();
                    $json_data = json_decode(stripslashes($this->request->json));
                    // $type = json_decode(stripslashes($this->request->type));

                    $result = Mcash_bank_type::Mcash_bank_typeSave(
                        $json_data,
                        $this->request->btn,
                        $session->get("user_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2: // delete data
                {
                    $result = Mcash_bank_type::Mcash_bank_typeDelete($this->request);
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                    $results = $this->records;
                    $data = array();
                    $data[] = array("posbelanja_ID", "NAME", "STATUS", "CREATED");
                    foreach ($results as $result) {
                        $data[] = array(
                            $result->posbelanja_id,
                            $result->name,
                            $result->type,
                            Mcoa::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                        );
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Master Currencies',
                        'Currencies',
                        array("A" => 15,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15),
                        $data,
                        'A',
                        'E'
                    );
                    $this->report->download('xls');
                };
                break;
            case 9: {
              //  dd($this->request->need_value);
                    $result = $this->rsExtJson(
                        Mcash_bank_type::Mcash_bank_typeSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        };
        return $result;
    }


}
