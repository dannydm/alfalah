<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Administrator\MUser;
use App\Models\Administrator\Mrapbs;
use App\Models\Administrator\AnggaranUser;

class AnggaranuserController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {   $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {   // dd($user); exit;
        // dd($this->request->user()->get('user.id')); 
        // dd($this->request->session()); 
        switch ($task)
        {   // master data
            case 0 : return $this->master($substask);       
            break;
            // anggaranuser tab
            case 1 : return $this->crudAnggaranuser($substask); 
            break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function master($subtask)
    {   switch ($subtask)
        {   case 0 : $result = view('administrator/anggaranuser')->with("TABID", $this->request->get("tabId")); break;
            case 1 : $result = Menu::ActiveMenu(null);
         break;
            case 10 : // search page data
                $result = $this->rsExtJson( Page::SearchPage($this->request), 
                    $this->request->limit, $this->request->start);
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Menu
     */

    /**
     * Create Replace Update Delete Role User
     */
    public function crudAnggaranuser($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( AnggaranUser::SearchAnggaranUser($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $json_data = json_decode(stripslashes($this->request->json));
                foreach ($json_data as $json) {
                    if (Empty($this->request->id))
                    {   $AnggaranUsers = new AnggaranUser(); }
                    else
                    {   $AnggaranUsers  = AnggaranUser::where('mrapbs_id', '=', $this->request->mrapbs_id)
                                    ->where('user_id', '=', $this->request->user_id)
                                    ->first();
                    };
                    
                    $AnggaranUsers->mrapbs_id = $json->mrapbs_id;
                    $AnggaranUsers->user_id = $json->user_id;

                    $result     = $AnggaranUsers->save();
                };
                $result = $this->jsonSuccess( $result );
                unset($AnggaranUsers);
                unset($json);
            };
            break;
            case 2 : // delete data
            {   $result = AnggaranUser::where('useranggaran_id', '=', $this->request->useranggaran_id)
                            ->where('user_id', '=', $this->request->user_id)
                            ->delete();
                $result = $this->jsonSuccess( $result );
            };
            break;
        };
        return $result;
    }

}
