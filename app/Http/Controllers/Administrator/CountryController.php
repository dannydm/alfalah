<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Administrator\Currency;
use App\Models\Administrator\Country;
use App\Models\Administrator\City;

class CountryController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $records;
    public function __construct(Request $request)
    {   $this->middleware('auth');
        $this->request = $request;
        // echo "HUAHAHAHAHA __construct<BR>";
        // print_r($request);
      //   $response = array(
      //     'status' => 'success',
      //     'msg' => $request->message,
      // );
      // return response()->json($response); 
    }

    public function index($task, $substask)
    {   // dd($user); exit;
        // dd($this->request->user()->get('user.id')); 
        // dd($this->request->session()); 
        // $session = $this->request->session();
        switch ($task)
        {   // master data
            case 0 : return $this->master($substask);       break;
            // menu tab
            case 1 : return $this->crudCurrency($substask); break;
            // user tab
            case 2 : return $this->crudCountry($substask);  break;
            // role tab
            case 3 : return $this->crudCity($substask);     break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function master($subtask)
    {   $session = $this->request->session();
        switch ($subtask)
        {   
            case 0 : $result = view('administrator/country'.config('app.system_skin'))
                        ->with("USER_ID", $session->get("user_id"))
                        ->with("USERNAME", $session->get("username"))
                        ->with("USERSITE", $session->get("site"))
                        ->with("TABID", $this->request->get("tabId"))
                        ->with("COMPANY_CODE", $this->companies['company_code'])
                        ->with("COMPANY_NAME", $this->companies['company_name']); 
            break;
            case 1 : $result = Menu::ActiveMenu(null); break;
            case 10 : // search page data
                $result = $this->rsExtJson( Page::SearchPage($this->request), 75, 1);
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Menu
     */
    public function crudCurrency($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   
                $this->records = Currency::currencySearch($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudCurrency(3); }
                    else {  $this->crudCurrency(4); };
                }
                else
                {   
                    $result = $this->rsJson(  $this->records, $this->request->limit, $this->request->start );
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = Currency::currencySave(
                                                $json_data, 
                                                $this->request->btn,
                                                $session->get("user_id")
                                            );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Currency::currencyDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array(  "ID", "NAME", "SYMBOL", "STATUS", "CREATED");
                foreach($results as $result) {
                        $data[] = array(
                            $result->currency_id, 
                            $result->name, 
                            $result->symbol, 
                            Currency::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Currencies',
                    'Currencies',
                    array("A" => 15,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15),
                    $data,
                    'A',
                    'E');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( Currency::currencySearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;

        };
        return $result;
    }
    /**
     * Create Replace Update Delete User
     */
    public function crudCountry($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = Country::countrySearch($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudCountry(3); }
                    else {  $this->crudCountry(4); };
                }
                else
                {   
                    $result = $this->rsJson(  $this->records,
                                $this->request->limit, $this->request->start);
                    
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = Country::countrySave(
                                                $json_data, 
                                                $this->request->btn,
                                                $session->get("user_id")
                                            );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Country::countryDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "NAME", "CAPITOL", "CURRENCY", "PHONE.AREA", "STATUS", "CREATED");
                foreach($results as $result) {
                        $data[] = array(
                            $result->country_id, 
                            $result->name, 
                            $result->capitol, 
                            $result->currency_id,
                            $result->phone_area,
                            Country::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Countries',
                    'Countries',
                    array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                    $data,
                    'A',
                    'G');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( Country::countrySearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Role
     */
    public function crudCity($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = City::citySearch($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudCity(3); }
                    else {  $this->crudCity(4); };
                }
                else
                {   $result = $this->rsJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = City::citySave(   $json_data, 
                                            $session->get("user_id")
                                        );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );

                $json_data = json_decode(stripslashes($this->request->json));
                
                $result = $this->jsonSuccess( $result );
                unset($city);
                unset($json);
            };
            break;
            case 2 : // delete data
            {   $result = City::cityDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "COUNTRY", "NAME", "PHONE.AREA", "STATUS", "CREATED");
                foreach($results as $result) {
                        $data[] = array(
                            $result->city_id, 
                            $result->country_id, 
                            $result->name, 
                            $result->phone_area,
                            City::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Cities',
                    'Cities',
                    array("A" => 10,  "B" => 10,  "C" => 50, "D" => 15, "E" => 15, "F" => 15),
                    $data,
                    'A',
                    'F');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( City::citySearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
}
