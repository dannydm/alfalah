<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Administrator\CompanyType;
use App\Models\Administrator\CompanyClassType;
use App\Models\Administrator\Company;
use App\Models\Administrator\Site;
use App\Models\Administrator\Department;
use App\Models\Administrator\SiteDepartment;
use App\Models\Administrator\SiteBuilding;
use App\Models\Administrator\Document;
use App\Models\Administrator\SiteDocument;
use App\Models\Administrator\Warehouse;

class CompanyController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {   $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {   // dd($companyclasstype); exit;
        // dd($this->request->CompanyClassType()->get('CompanyClassType.id')); 
        // dd($this->request->session()); 
        switch ($task)
        {   // master data
            case 0 : return $this->master($substask);               break;
            // CompanyType tab
            case 1 : return $this->crudCompanyType($substask);      break;
            // CompanyClassType tab
            case 2 : return $this->crudCompanyClassType($substask); break;
            // Company tab
            case 3 : return $this->crudCompany($substask);          break;
            // Site tab
            case 4 : return $this->crudSite($substask);             break;
            // Department tab
            case 5 : return $this->crudDepartment($substask);       break;
            // Site Department tab
            case 6 : return $this->crudSiteDepartment($substask);   break;
            // Document tab
            case 7 : return $this->crudDocument($substask);         break;
            // Site Document tab
            case 8 : return $this->crudSiteDocument($substask);     break;
            // Warehouse page tab
            case 9 : return $this->crudWarehouse($substask);        break;
            // Site Building page tab
            case 12 : return $this->crudSiteBuilding($substask);    break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function master($subtask)
    {   $session = $this->request->session();
        switch ($subtask)
        {   
            case 0 : $result = view('administrator/company'.config('app.system_skin'))
                        ->with("USER_ID", $session->get("user_id"))
                        ->with("USERNAME", $session->get("username"))
                        ->with("USERSITE", $session->get("site"))
                        ->with("TABID", $this->request->get("tabId"))
                        ->with("COMPANY_CODE", $this->companies['company_code'])
                        ->with("COMPANY_NAME", $this->companies['company_name']);
            break;
            case 1 : $result = CompanyType::ActiveCompanyType(null); break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete CompanyType
     */
    public function crudCompanyType($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = CompanyType::SearchCompanyType($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudCompanyType(3); }
                    else {  $this->crudCompanyType(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $json_data = json_decode(stripslashes($this->request->json));
                foreach ($json_data as $json) {
                    if ($json->created_date == '')
                    {   $companytype = new CompanyType();
                        $companytype->created_date  = date('Ymd');
                        $companytype->company_type  = $json->company_type;
                    }
                    else
                    {   $companytype = CompanyType::where('company_type', '=', $json->company_type)->first();
                    };

                    $companytype->name           = $json->name;
                    $result = $companytype->save();
                };
                $result = $this->jsonSuccess( $result );
                unset($companytype);
                unset($json);
            };
            break;
            case 2 : // delete data
            {   $result = CompanyType::where('company_type', '=', $this->request->id)->delete();
                $result = $this->jsonSuccess( $result );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "NAME", "CREATED");
                foreach($results as $result) {
                        $data[] = array(
                            $result->company_type, 
                            $result->name, 
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Company Types',
                    'Companytypes',
                    array("A" => 10, "B" => 25,  "C" => 15),
                    $data,
                    'A',
                    'C');
                $this->report->download('xls');
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete CompanyClassType
     */
    public function crudCompanyClassType($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = CompanyClassType::companyClassSearch($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudCompanyClassType(3); }
                    else {  $this->crudCompanyClassType(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = CompanyClassType::companyClassSave(
                                        $json_data, 
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Company::companyClassDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "Company", "Type", "Area",  "CREATED");
                foreach($results as $result) {
                        $data[] = array(
                            $result->company_id, 
                            $result->company_name, 
                            $result->company_type_name, 
                            $result->area_type_name, 
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Company Class Types',
                    'Companyclasstypes',
                    array("A" => 15, "B" => 50, "C" => 15, "D" => 15, "E" => 15),
                    $data,
                    'A',
                    'E');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( CompanyClassType::companyClassSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Company
     */
    public function crudCompany($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = Company::companySearch($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudCompany(3); }
                    else {  $this->crudCompany(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = Company::companySave(
                                        $json_data, 
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Company::companyDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "PARENT", "NAME", "ADDRESS", "CITY", "COUNTRY", 
                                "TELP", "FAX", "EMAIL", "DOC", "STATUS", "CREATED");
                foreach($results as $result) {
                        $data[] = array(
                            $result->company_id, 
                            $result->parent_company_id, 
                            $result->name, 
                            $result->address,
                            $result->city_name,
                            $result->country_name,
                            $result->telp_no, 
                            $result->fax_no, 
                            $result->email,
                            $result->doc_abvr,
                            $result->status_name,   
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Companies',
                    'Companies',
                    array("A" => 10, "B" => 10, "C" => 25, "D" => 25, "E" => 15, "F" => 15,
                                "G" => 15, "H" => 15, "I" => 50, "J" => 15, "K" => 15, "L" => 15),
                    $data,
                    'A',
                    'L');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( Company::companySearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Site
     */
    public function crudSite($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = Site::searchSite($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudSite(3); }
                    else {  $this->crudSite(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = Site::siteSave(
                                        $json_data, 
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Site::siteDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("COMPANY", "SITE", "NAME", "COUNTRY", "TYPE", "AREA", 
                                "DOC", "STATUS", "CREATED");
                foreach($results as $result) {
                        $data[] = array(
                            $result->company_id, 
                            $result->site_id, 
                            $result->name, 
                            $result->country_id,
                            $result->company_type_name,
                            $result->area_type,
                            $result->doc_code,
                            $result->status, 
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Sites',
                    'Sites',
                    array("A" => 10, "B" => 10, "C" => 25, "D" => 10, "E" => 25, "F" => 15,
                        "G" => 15, "H" => 15, "I" => 15),
                    $data,
                    'A',
                    'I');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( Site::siteSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Department
     */
    public function crudDepartment($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = Department::searchDepartment($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudDepartment(3); }
                    else {  $this->crudDepartment(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = Department::departmentSave(
                                        $json_data, 
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Department::departmentDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "PARENT", "NAME", "ABVR", "TYPE", "DESCRIPTION", 
                                "EMAIL", "ACCT.CLASS", "CREATED");
                foreach($results as $result) {
                     $data[] = array(
                            $result->dept_id, 
                            $result->parent_dept_id, 
                            $result->name, 
                            $result->abreviation,
                            $result->type,
                            $result->description,
                            $result->dept_email, 
                            $result->acct_class, 
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Departments',
                    'Departments',
                    array("A" => 10, "B" => 10, "C" => 25, "D" => 15, "E" => 15, "F" => 25,
                        "G" => 15, "H" => 15, "I" => 15),
                    $data,
                    'A',
                    'I');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( Department::departmentSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Site Department
     */
    public function crudSiteDepartment($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = SiteDepartment::searchSiteDepartment($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudSiteDepartment(3); }
                    else {  $this->crudSiteDepartment(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $json_data = json_decode(stripslashes($this->request->json));
                foreach ($json_data as $json) {
                    if (Empty($this->request->id))
                    {   $sitedepartment = new SiteDepartment(); 
                        $sitedepartment->company_id   = $json->company_id;
                        $sitedepartment->site_id      = $json->site_id;
                        $sitedepartment->dept_id      = $json->dept_id;
                        $sitedepartment->created_date = date('Ymd');
                    }
                    else
                    {   $sitedepartment  = SiteDepartment::where('company_id', '=', $this->request->company_id)
                                    ->where('site_id', '=', $this->request->site_id)
                                    ->where('dept_id', '=', $this->request->dept_id)
                                    ->first();
                    };
                    
                    $sitedepartment->site_dept_email = $json->site_dept_email;
                    
                    $result     = $sitedepartment->save();
                };
                $result = $this->jsonSuccess( $result );
                unset($sitedepartment);
                unset($json);
            };
            break;
            case 2 : // delete data
            {   $result = SiteDepartment::where('company_id', '=', $this->request->cid)
                            ->where('site_id', '=', $this->request->sid)
                            ->where('dept_id', '=', $this->request->did)
                            ->delete();
                $result = $this->jsonSuccess( $result );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("COMPANY", "SITE", "SITE.NAME", "DEPT", "DEPT.NAME",
                                "EMAIL", "CREATED");
                foreach($results as $result) {
                     $data[] = array(
                        $result->company_id, 
                        $result->site_id, 
                        $result->site_name, 
                        $result->dept_id, 
                        $result->dept_name, 
                        $result->site_dept_email,
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Site Departments',
                    'Sitedepts',
                    array("A" => 10, "B" => 10, "C" => 25, "D" => 10, "E" => 20,
                        "F" => 15, "G" => 15),
                    $data,
                    'A',
                    'G');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( SiteDepartment::searchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Document
     */
    public function crudDocument($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = Document::searchDocument($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudDocument(3); }
                    else {  $this->crudDocument(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = Document::documentSave(
                                        $json_data, 
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Document::documentDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "CODE", "NAME", "DESCRIPTION", "DEPT.ID", "DEPT.NAME",
                                "FORMAT", "CREATED");
                foreach($results as $result) {
                     $data[] = array(
                        $result->doc_id, 
                        $result->doc_code, 
                        $result->name, 
                        $result->description, 
                        $result->owner_dept_id, 
                        $result->dept_name, 
                        $result->format_doc_no, 
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Documents',
                    'Documents',
                    array("A" => 10, "B" => 10, "C" => 25, "D" => 25, "E" => 10,
                        "F" => 25, "G" => 15, "H" => 15),
                    $data,
                    'A',
                    'H');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( Document::documentSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Site Document
     */
    public function crudSiteDocument($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = SiteDocument::Search($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudSiteDocument(3); }
                    else {  $this->crudSiteDocument(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = SiteDocument::sitedocumentSave(
                                            $json_data, 
                                            $session->get("site"),
                                            $session->get("user_id")
                                        );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = SiteDocument::sitedocumentDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("COMPANY.ID", "SITE.ID", "SITE.NAME", "DEPT.ID", 
                                "DOC.ID", "DOC.NAME", "CREATED");
                foreach($results as $result) {
                     $data[] = array(
                        $result->company_id, 
                        $result->site_id, 
                        $result->site_name, 
                        $result->dept_id, 
                        $result->doc_id, 
                        $result->doc_name,
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Site Documents',
                    'Sitedocs',
                    array("A" => 10, "B" => 10, "C" => 25, "D" => 10, "E" => 10,
                        "F" => 25, "G" => 15),
                    $data,
                    'A',
                    'G');
                $this->report->download('xls');
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Warehouse Page
     */
    public function crudWarehouse($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = Warehouse::searchWarehouse($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudWarehouse(3); }
                    else {  $this->crudWarehouse(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $json_data = json_decode(stripslashes($this->request->json));
                foreach ($json_data as $json) {
                    if (Empty($json->created_date))
                    {   $Warehouse = new Warehouse(); 
                        $Warehouse->company_id   = $json->company_id;
                        $Warehouse->site_id      = $json->site_id;
                        $Warehouse->warehouse_id = $json->warehouse_id;
                        $Warehouse->created_date = date('Ymd');
                    }
                    else
                    {   $Warehouse  = Warehouse::where('company_id', '=', $json->company_id)
                                    ->where('site_id', '=', $json->site_id)
                                    ->where('warehouse_id', '=', $json->warehouse_id)
                                    ->first();
                    };
                    
                    $Warehouse->name = $json->name;
                    $Warehouse->status = Warehouse::setStatus($json->status_name);
                    $Warehouse->warehouse_type = Warehouse::setType($json->type);
                    $Warehouse->warehouse_level = Warehouse::setLevel($json->level);

                    $result     = $Warehouse->save();
                };
                $result = $this->jsonSuccess( $result );
                unset($Warehouse);
                unset($json);
            };
            break;
            case 2 : // delete data
            {   $result = Warehouse::where('company_id', '=', $this->request->company_id)
                            ->where('site_id', '=', $this->request->site_id)
                            ->where('warehouse_id', '=', $this->request->warehouse_id)
                            ->delete();
                $result = $this->jsonSuccess( $result );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("COMPANY.ID", "SITE.ID", "SITE.NAME", 
                                "ID", "NAME", "FLAG", "TYPE", "WH.NAME",
                                "CREATED");
                foreach($results as $result) {
                     $data[] = array(
                        $result->company_id, 
                        $result->site_id, 
                        $result->site_name, 
                        $result->warehouse_id, 
                        $result->name, 
                        $result->wh_flag,
                        $result->type,
                        $result->wh_name,
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Warehouses',
                    'Warehouses',
                    array("A" => 10, "B" => 10, "C" => 25, "D" => 10, "E" => 25,
                        "F" => 10, "G" => 10, "H" => 15, "I" => 15),
                    $data,
                    'A',
                    'I');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( Warehouse::SearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Site Building
     */
    public function crudSiteBuilding($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = SiteBuilding::sitebuildingSearch($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudSiteBuilding(3); }
                    else {  $this->crudSiteBuilding(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $json_data = json_decode(stripslashes($this->request->json));
                // dd($json_data);
                foreach ($json_data as $json) {
                    if (Empty($json->id))
                    {   $sitebuilding = new SiteBuilding(); 
                        $sitebuilding->company_id   = $json->company_id;
                        $sitebuilding->site_id      = $json->site_id;
                        $sitebuilding->building_id  = $json->building_id;
                        $sitebuilding->created_date = date('Ymd');
                    }
                    else 
                    {   $sitebuilding  = SiteBuilding::where('company_id', '=', $json->company_id)
                                    ->where('site_id', '=', $json->site_id)
                                    ->where('building_id', '=', $json->building_id)
                                    ->first();
                    };
                    
                    $sitebuilding->name = $json->name;
                    
                    $result     = $sitebuilding->save();
                };
                $result = $this->jsonSuccess( $result );
                unset($sitebuilding);
                unset($json);
            };
            break;
            case 2 : // delete data
            {   $result = SiteBuilding::where('company_id', '=', $this->request->cid)
                            ->where('site_id', '=', $this->request->sid)
                            ->where('building_id', '=', $this->request->did)
                            ->delete();
                $result = $this->jsonSuccess( $result );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("COMPANY", "SITE", "SITE.NAME", "BUILD.ID", "BUILD.NAME",
                                "CREATED");
                foreach($results as $result) {
                     $data[] = array(
                        $result->company_id, 
                        $result->site_id, 
                        $result->site_name, 
                        $result->building_id, 
                        $result->name,
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Site Buildings',
                    'Sitedepts',
                    array("A" => 10, "B" => 10, "C" => 25, "D" => 10, "E" => 20,
                        "F" => 15),
                    $data,
                    'A',
                    'G');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( SiteBuilding::sitebuildingSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    
}
