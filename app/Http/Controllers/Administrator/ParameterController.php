<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Administrator\Charges;
use App\Models\Administrator\ContainerType;
use App\Models\Administrator\Parameter;
use App\Models\Administrator\PaymentTerm;
use App\Models\Administrator\UMConversion;
use App\Models\Administrator\UnitMeasurement;

class ParameterController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $records;
    public function __construct(Request $request)
    {   $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {   // dd($user); exit;
        // dd($this->request->user()->get('user.id')); 
        // dd($this->request->session()); 
        switch ($task)
        {   // master data
            case 0 : return $this->master($substask);           break;
            // Parameter tab
            case 1 : return $this->crudParameter($substask);    break;
            // UnitMeasurements page tab
            case 2 : return $this->crudUM($substask);           break;
            // UM Conversions page tab
            case 3 : return $this->crudUMConversion($substask); break;
            // Payment Term page tab
            case 4 : return $this->crudPaymentTerm($substask);  break;
            // Container Type page tab
            case 5 : return $this->crudContainerType($substask);break;
            // Charges page tab
            case 6 : return $this->crudCharges($substask);      break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function master($subtask)
    {   switch ($subtask)
        {   case 0 : $result = view('administrator/parameter')->with("TABID", $this->request->get("tabId")); break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Parameter
     */
    public function crudParameter($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = Parameter::parameterSearch($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudParameter(3); }
                    else {  $this->crudParameter(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = Parameter::parameterSave(
                                                $json_data, 
                                                $session->get("user_id")
                                            );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Currency::currencyDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array(  "ID", "NAME", "SYMBOL", "STATUS", "CREATED");
                foreach($results as $result) {
                        $data[] = array(
                            $result->currency_id, 
                            $result->name, 
                            $result->symbol, 
                            Currency::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Currencies',
                    'Currencies',
                    array("A" => 15,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15),
                    $data,
                    'A',
                    'E');
                $this->report->download('xls');
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Unit Measurements Page
     */
    public function crudUM($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = UnitMeasurement::umSearch($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudUM(3); }
                    else {  $this->crudUM(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $json_data = json_decode(stripslashes($this->request->json));
                foreach ($json_data as $json) {
                    if (Empty($this->request->created_date))
                    {   $um = new UnitMeasurement(); 
                        $um->um_id        = $json->um_id;
                        $um->created_date = date('Ymd');
                    }
                    else
                    {   $um  = UnitMeasurement::where('um_id', '=', $this->request->um_id)
                                    ->first();
                    };
                    
                    $um->name = $json->name;
                    $um->description = $json->description;

                    $result     = $um->save();
                };
                $result = $this->jsonSuccess( $result );
                unset($um);
                unset($json);
            };
            break;
            case 2 : // delete data
            {   
                $result = UnitMeasurement::where('um_id', '=', $this->request->id)
                            ->delete();
                $result = $this->jsonSuccess( $result );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "NAME", "DESCRIPTION", "CREATED");
                foreach($results as $result) {
                     $data[] = array(
                        $result->um_id, 
                        $result->name, 
                        $result->description,
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Unit Measurements',
                    'um',
                    array("A" => 10, "B" => 25, "C" => 25, "D" => 15),
                    $data,
                    'A',
                    'D');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( UnitMeasurement::umSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete UM Conversion Page
     */
    public function crudUMConversion($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = UMConversion::umConvSearch($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudUMConversion(3); }
                    else {  $this->crudUMConversion(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $json_data = json_decode(stripslashes($this->request->json));
                foreach ($json_data as $json) {
                    if (Empty($this->request->id))
                    {   $umconv = new UMConversion(); 
                        $umconv->created_date = date('Ymd');
                    }
                    else
                    {   $umconv  = UMConversion::where('um_conv_id', '=', $this->request->um_conv_id)
                                    ->first();
                    };
                    
                    $umconv->first_um_id  = $json->first_um_id;
                    $umconv->first_value  = $json->first_value;
                    $umconv->second_um_id = $umconv->second_um_id;
                    $umconv->second_value = $json->second_value;

                    $result     = $umconv->save();
                };
                $result = $this->jsonSuccess( $result );
                unset($umconv);
                unset($json);
            };
            break;
            case 2 : // delete data
            {   $result = UMConversion::where('um_conv_id', '=', $this->request->um_conv_id)
                            ->delete();
                $result = $this->jsonSuccess( $result );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "FIRST", "VALUE", "SECOND", "VALUE", "CREATED");
                foreach($results as $result) {
                     $data[] = array(
                        $result->um_conv_id, 
                        $result->first_um_id, 
                        $result->first_value,
                        $result->second_um_id, 
                        $result->second_value,
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master UM Conversion',
                    'umconv',
                    array("A" => 10, "B" => 10, "C" => 10, "D" => 10, "E" => 10, "F" => 10),
                    $data,
                    'A',
                    'F');
                $this->report->download('xls');
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Payment Term
     */
    public function crudPaymentTerm($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = PaymentTerm::Search($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudPaymentTerm(3); }
                    else {  $this->crudPaymentTerm(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = PaymentTerm::paymenttermSave(
                                                $json_data, 
                                                $session->get("user_id")
                                            );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = PaymentTerm::paymenttermDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array(  "ID", "Description", "CREATED");
                foreach($results as $result) {
                        $data[] = array(
                            $result->payment_term_id, 
                            $result->description, 
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Payment Term',
                    'Payment Term',
                    array("A" => 15,  "B" => 25,  "C" => 15),
                    $data,
                    'A',
                    'C');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( PaymentTerm::SearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Container Type
     */
    public function crudContainerType($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = ContainerType::Search($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudContainerType(3); }
                    else {  $this->crudContainerType(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = ContainerType::containertypeSave(
                                                $json_data, 
                                                $session->get("user_id")
                                            );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = ContainerType::containertypeDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array(  "ID", "Description", "CREATED");
                foreach($results as $result) {
                        $data[] = array(
                            $result->container_type_id, 
                            $result->description, 
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Container Type',
                    'ContainerType',
                    array("A" => 15,  "B" => 25,  "C" => 15),
                    $data,
                    'A',
                    'C');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( ContainerType::SearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Charges
     */
    public function crudCharges($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = Charges::Search($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudCharges(3); }
                    else {  $this->crudCharges(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = Charges::chargesSave(
                                        $json_data, 
                                        $session->get("site"),
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Charges::chargesDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "NAME", "OPERATION", "STATUS", "IS_PO", "CREATED");
                foreach($results as $result) {
                        $data[] = array(
                            $result->id, 
                            $result->name, 
                            $result->plus_minus, 
                            $result->status_name, 
                            $result->is_po_name,
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Charges',
                    'Charges',
                    array("A" => 10, "B" => 25, "C" => 10, "D" => 10, "E" => 10, "F" => 15),
                    $data,
                    'A',
                    'E');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( Charges::SearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
}
