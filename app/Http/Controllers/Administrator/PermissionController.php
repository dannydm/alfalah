<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Administrator\Menu;
use App\Models\Administrator\MUser;
use App\Models\Administrator\Page;
use App\Models\Administrator\Role;
use App\Models\Administrator\RoleMenu;
use App\Models\Administrator\RoleUser;
use App\Models\Administrator\MenuTask;
use App\Models\Administrator\RoleTask;
use App\Models\Administrator\UserTask;

class PermissionController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {   $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {   // dd($user); exit;
        // dd($this->request->user()->get('user.id')); 
        // dd($this->request->session()); 
        switch ($task)
        {   // master data
            case 0 : return $this->master($substask);       break;
            // menu tab
            case 1 : return $this->crudMenu($substask);     break;
            // user tab
            case 2 : return $this->crudUser($substask);     break;
            // role tab
            case 3 : return $this->crudRole($substask);     break;
            // role menu tab
            case 4 : return $this->crudRoleMenu($substask); break;
            // role user tab
            case 5 : return $this->crudRoleUser($substask); break;
            // menu page tab
            case 6 : return $this->crudMenuTask($substask); break;
            // role page tab
            case 7 : return $this->crudRoleTask($substask); break;
            // user page tab
            case 8 : return $this->crudUserTask($substask); break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function master($subtask)
    {   switch ($subtask)
        {   case 0 : $result = view('administrator/permission')->with("TABID", $this->request->get("tabId")); break;
            case 1 : $result = Menu::ActiveMenu(null); break;
            case 10 : // search page data
                $result = $this->rsExtJson( Page::SearchPage($this->request), 
                    $this->request->limit, $this->request->start);
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Menu
     */
    public function crudMenu($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( Menu::SearchMenu($this->request), 
                    $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $json_data = json_decode(stripslashes($this->request->json));
                foreach ($json_data as $json) {
                    if ($json->created_date == '')
                    {   $menu = new Menu(); 
                        $menu->menu_id    = $json->menu_id;
                    }
                    else
                    {   $menu = Menu::where('menu_id', '=', $json->menu_id)->first();   };
                    
                    $menu->parent_menu_id = $json->parent_menu_id;
                    $menu->object_id      = $json->object_id;
                    $menu->name           = $json->name;
                    $menu->level_degree   = $json->level_degree;
                    $menu->arrange_no     = $json->arrange_no;
                    $menu->has_child      = $json->has_child;
                    $menu->link           = $json->link;
                    // $menu->created_date  = date('Ymd');
                    $menu->modified_date  = date('Ymd');
                    $menu->status         = $json->status;
                    $result = $menu->save();
                };
                $result = $this->jsonSuccess( $result );
                unset($menu);
                unset($json);
            };
            break;
            case 2 : // delete data
            {   $result = Menu::where('menu_id', '=', $this->request->id)->delete();
                $result = $this->jsonSuccess( $result );
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( Menu::SearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete User
     */
    public function crudUser($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( MUser::SearchUser($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = MUser::userSave(
                                        $json_data, 
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = MUser::where('user_id', '=', $this->request->id)->delete();
                $result = $this->jsonSuccess( $result );
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( MUser::userSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
            case 10 :
            {   $result = $this->rsExtJson( MUser::laraveluserSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
            case 91: {  $result = MUser::ResetPassword($this->request); };  break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Role
     */
    public function crudRole($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( Role::SearchRole($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $json_data = json_decode(stripslashes($this->request->json));
                foreach ($json_data as $json) {
                    $roles                       = Role::where('role_id', '=', $json->role_id)->first();
                    $roles->role_id              = $json->role_id;
                    $roles->parent_role_id       = $json->parent_role_id;
                    $roles->name                 = $json->name;
                    $roles->status               = $json->status;
                    $roles->note                 = $json->note;
                    $roles->created              = $json->created;
                    $roles->closed               = $json->closed;
                    $roles->created_date         = $json->created_date;

                    $result     = $roles->save();
                };
                $result = $this->jsonSuccess( $result );
                unset($roles);
                unset($json);
            };
            break;
            case 2 : // delete data
            {   $result = Role::where('role_id', '=', $this->request->id)->delete();
                $result = $this->jsonSuccess( $result );
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Role Menu
     */
    public function crudRoleMenu($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( RoleMenu::SearchRoleMenu($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $json_data = json_decode(stripslashes($this->request->json));
                foreach ($json_data as $json) {
                    if (Empty($this->request->id))
                    {   $RoleMenus = new RoleMenu(); }
                    else
                    {   $RoleMenus  = RoleMenu::where('role_id', '=', $this->request->role_id)
                                    ->where('menu_id', '=', $this->request->menu_id)
                                    ->first();
                    };
                    
                    $RoleMenus->role_id = $json->role_id;
                    $RoleMenus->menu_id = $json->menu_id;

                    $result     = $RoleMenus->save();
                };
                $result = $this->jsonSuccess( $result );
                unset($RoleMenus);
                unset($json);
            };
            break;
            case 2 : // delete data
            {   $result = RoleMenu::where('role_id', '=', $this->request->role_id)
                            ->where('menu_id', '=', $this->request->menu_id)
                            ->delete();
                $result = $this->jsonSuccess( $result );
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Role User
     */
    public function crudRoleUser($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( RoleUser::SearchRoleUser($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $json_data = json_decode(stripslashes($this->request->json));
                foreach ($json_data as $json) {
                    if (Empty($this->request->id))
                    {   $RoleUsers = new RoleUser(); }
                    else
                    {   $RoleUsers  = RoleUser::where('role_id', '=', $this->request->role_id)
                                    ->where('user_id', '=', $this->request->user_id)
                                    ->first();
                    };
                    
                    $RoleUsers->role_id = $json->role_id;
                    $RoleUsers->user_id = $json->user_id;

                    $result     = $RoleUsers->save();
                };
                $result = $this->jsonSuccess( $result );
                unset($RoleUsers);
                unset($json);
            };
            break;
            case 2 : // delete data
            {   $result = RoleUser::where('role_id', '=', $this->request->role_id)
                            ->where('user_id', '=', $this->request->user_id)
                            ->delete();
                $result = $this->jsonSuccess( $result );
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Menu Task
     */
    public function crudMenuTask($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( MenuTask::Search($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = MenuTask::taskSave(
                                        $json_data, 
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = MenuTask::taskDelete($json_data);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Role Page
     */
    public function crudRoleTask($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( RoleTask::Search($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = RoleTask::taskSave(
                                        $json_data, 
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = RoleTask::taskDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete User Page
     */
    public function crudUserTask($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( UserTask::Search($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = UserTask::taskSave(
                                        $json_data, 
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            {   
                // $json_data = json_decode(stripslashes($this->request->json));
                // foreach ($json_data as $json) {
                //     if (Empty($this->request->id))
                //     {   $UserTasks = new UserTask(); }
                //     else
                //     {   $UserTasks  = UserTask::where('role_id', '=', $this->request->role_id)
                //                     ->where('pg', '=', $this->request->pg)
                //                     ->where('ev', '=', $this->request->ev)
                //                     ->first();
                //     };
                    
                //     $UserTasks->role_id = $json->role_id;
                //     $UserTasks->pg = $json->pg;
                //     $UserTasks->ev = $json->ev;

                //     $result     = $UserTasks->save();
                // };
                // $result = $this->jsonSuccess( $result );
                // unset($UserTasks);
                // unset($json);
            };
            break;
            case 2 : // delete data
            {   $result = UserTask::where('id', '=', $this->request->id)->delete();
                $result = $this->jsonSuccess( $result );
            };
            break;
            case 20 ://generate user tasks
            {   $result = UserTask::generateUserTask(
                    $this->request->role_id, 
                    $this->request->user_id
                );
                $result = $this->jsonSuccess( $result );
            };
            break;
        };
        return $result;
    }
}
