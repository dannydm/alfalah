<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Administrator\Account;
// use App\Models\Administrator\Country;
// use App\Models\Administrator\City;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $records;
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
        // echo "HUAHAHAHAHA __construct<BR>";
        // print_r($request);
        //   $response = array(
        //     'status' => 'success',
        //     'msg' => $request->message,
        // );
        // return response()->json($response); 
    }

    public function index($task, $substask)
    {   // dd($user); exit;
        // dd($this->request->user()->get('user.id')); 
        // dd($this->request->session()); 
        // $session = $this->request->session();
        switch ($task) {   // master data
            case 0:
                return $this->master($substask);
                break;
                // menu tab
            case 1:
                return $this->crudAccount($substask);
                break;
                // // user tab
                // case 2 : return $this->crudCountry($substask);  break;
                // // role tab
                // case 3 : return $this->crudCity($substask);     break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function master($subtask)
    {
        $session = $this->request->session();
        switch ($subtask) {
            case 0:
                $result = view('administrator/account' . config('app.system_skin'))
                    ->with("USER_ID", $session->get("user_id"))
                    ->with("USERNAME", $session->get("username"))
                    ->with("USERSITE", $session->get("site"))
                    ->with("TABID", $this->request->get("tabId"))
                    ->with("COMPANY_CODE", $this->companies['company_code'])
                    ->with("COMPANY_NAME", $this->companies['company_name']);
                break;
            case 1:
                $result = Menu::ActiveMenu(null);
                break;
            case 10: // search page data
                $result = $this->rsExtJson(Page::SearchPage($this->request), 75, 1);
                break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete Menu
     */
    public function crudAccount($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = Account::accountSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->crudAccount(3);
                        } else {
                            $this->crudAccount(4);
                        };
                    } else {
                        $result = $this->rsJson($this->records, $this->request->limit, $this->request->start);
                    };
                };
                break;
            case 1: // save data
                {
                    $session = $this->request->session();
                    $json_data = json_decode(stripslashes($this->request->json));
                    $result = Account::accountSave(
                        $json_data,
                        $this->request->btn,
                        $session->get("user_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2: // delete data
                {
                    $result = Account::accountDelete($this->request);
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                    $results = $this->records;
                    $data = array();
                    $data[] = array("ID", "COA_NO", "PARENT_COA_NO", "STATUS", "CREATED");
                    foreach ($results as $result) {
                        $data[] = array(
                            $result->coa_no,
                            $result->parent_coa_no,
                            $result->name,
                            Account::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                        );
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Master Currencies',
                        'Currencies',
                        array("A" => 15,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15),
                        $data,
                        'A',
                        'E'
                    );
                    $this->report->download('xls');
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        Account::accountSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        };
        return $result;
    }
}
