<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PurchaseRequest;

class PurchaseRequestController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {   
        // switch ($task)
        // {   // master data
        //     case 0 : return $this->master($substask);       break;
        //     // menu tab
        //     case 1 : return $this->crudMenu($substask);     break;
        //     // user tab
        //     case 2 : return $this->crudUser($substask);     break;
        //     // role tab
        //     case 3 : return $this->crudRole($substask);     break;
        //     // role menu tab
        //     case 4 : return $this->crudRoleMenu($substask); break;
        //     // role user tab
        //     case 5 : return $this->crudRoleUser($substask); break;
        //     // role page tab
        //     case 6 : return $this->crudRolePage($substask); break;
        //     // user page tab
        //     case 7 : return $this->crudUserPage($substask); break;
        // };
        return true;
    }
    public function myPR($subtask)
    {   
        switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( PurchaseRequest::myPRSearch($this->request, 77), 75, 1);
            break;
        };
        return $result;
    }
}
