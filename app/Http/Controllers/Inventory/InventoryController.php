<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Inventory\Grin;
use App\Models\Inventory\GrinDetail;
use App\Models\Inventory\GrinSubDetail;
use App\Models\Inventory\SiteLaborCosts;

use DB;

class InventoryController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {   $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {   switch ($task)
        {   // routing data
            case 0 : return $this->routing($substask);      break;
            // Incoming data
            case 1 : return $this->crudIncoming($substask); break;
            // Outgoing data
            case 2 : return $this->crudOutgoing($substask); break;
            // Stocks data
            case 3 : return $this->crudStock($substask);    break;
            // Adjustment data
            case 4 : return $this->crudAdjustment($substask);    break;
            // Site labor costs data
            case 5 : return $this->crudSiteLaborCosts($substask);    break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {   switch ($subtask)
        {   case 0 : $result = view('inventory/incoming')->with("TABID", $this->request->get("tabId")); break;
            case 1 : $result = view('inventory/outgoingOrder')->with("TABID", $this->request->get("tabId")); break;
            case 2 : $result = view('inventory/outgoingGeneral')->with("TABID", $this->request->get("tabId")); break;
            case 4 : $result = view('inventory/adjustment')->with("TABID", $this->request->get("tabId")); break;
            case 5 : $result = view('inventory/sitelaborcost')->with("TABID", $this->request->get("tabId")); break;
            case 9 : $result = view('inventory/reports')->with("TABID", $this->request->get("tabId")); break;
        };
        return $result;
    }
    /**
     * CRUD Incoming
    */
    public function crudIncoming($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->records = Grin::headerPrintOut($this->request);
                        $this->crudIncoming(3); }
                    else 
                    {   $this->records = Grin::incomingSearch($this->request);
                        $this->crudIncoming(4); 
                    };
                }
                else
                {   $this->records = Grin::incomingSearch($this->request);
                    $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                $modi_data = json_decode(stripslashes($this->request->modi));
                $result = Grin::incomingSave(   $head_data, 
                                        $json_data, 
                                        $modi_data, 
                                        $session->get("site"),
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Grin::incomingDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {   //print_r($this->records);
                $data = $this->records;
                $title = "INCOMING DELIVERY";
                $filename = "grin_".$data->grin_no;

                $this->create_report_pdf($this->request->session());
                $pdf = $this->report;
                if ($data->grin_status == 0) // draft document
                {   $this->pdf_watermark(70, 110, " D R A F T ", 45); };
                ########## Document TITLE ##########
                $pdf->DocTitle($title, $data->grin_no, 30, 160);
                // $this->report->SetProtection(array('print'), '', null);
                ########## PAGE TITLE ##########
                $pdf->SetFont('Helvetica','B', 11);  
                $pdf->setRowXY(12, 40);  
                $pdf->Bookmark("Header", 1, -1);
                //$this->report->MultiCell(30,4,"Master",0,'L',0);
                $pdf->setRowXY(15, 40);
                $pdf->SetWidths(array(30, 70, 30, 60));
                $pdf->SetAligns(array("L", "L", "L", "L"));
                $pdf->SetFont('Times','', 10);
                $pdf->addRowHeight(5);
                $pdf->Row(array("Incoming.No", ": ".$data->grin_no, 
                                "Remarks", ": ".$data->description), false);
                $pdf->Row(array("Incoming.Date", ": ".$data->created_date, 
                                "WareHouse", ": ".$data->warehouse_name), false);
                $pdf->Row(array($data->ref_type.".No", ": ".$data->ref_no, "", ""), false);
                $pdf->Row(array("Supplier", ": ".$data->party_name, "", ""), false);
                $pdf->Row(array("Delivery.No", ": ".$data->party_delivery_no, "", ""), false);
                $pdf->Row(array("Delivery.Date", ": ".$data->party_delivery_date, "", ""), false);
                $pdf->Row(array("WareHouse", ": ".$data->warehouse_name, "", ""), false);
                $pdf->addRowHeight(5);
                // rectacngle boxs
                $y_current = $pdf->y_pos+5;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height ); 
                $pdf->Rect($pdf->x_pos+(100), $y_current, 85, $y_height ); 
                /* DETAIL ITEMS */
                $details = GrinDetail::detailPrintOut(trim($data->grin_no));
                $pdf->SetFont('Helvetica','B', 10);
                $pdf->addRowHeight(5);
                $pdf->SetWidths(array(10, 70, 30, 30, 30, 15));
                $pdf->Bookmark("Detail", 1, -1);
                $pdf->SetAligns(array("C", "C", "C", "C", "C", "C"));
                $pdf->Row(array("No", "Item Description","Passed", "Reject", "Total", "UM"), true);
                $pdf->SetAligns(array("L", "L", "R", "R", "R", "L"));
                $pdf->SetFont('Helvetica','', 10);
                $loop = 1;
                foreach ($details as $detail) 
                {   $pdf->Row(array($loop,
                                    $detail->item_name, 
                                    $detail->quantity_primary,
                                    $detail->reject_primary,
                                    $detail->total_primary,
                                    $detail->um_primary), true);
                    $loop += 1;
                };
                
                // $pdf->addRowHeight(5);
                // $pdf->SetWidths(array(40, 150));
                // $pdf->SetAligns(array("L", "L"));
                // $loop = 0;
                // foreach (explode(",", $data->payment_term) as $the_term)
                // {   if ($loop == 0) 
                //     {   $pdf->Row(array("Payment Term", ": ".$the_term), false); 
                //         $loop = 1;
                //     }
                //     else {  $pdf->Row(array("", " ".$the_term), false); }
                // };
                
                $pdf->Output($filename.".pdf", "I");
                exit;
            };
            break;
            case 4 : // print xls
            {   //print_r($this->records);
                $results = $this->records;
                $data = array();
                $data[] = array("NO", "COMPANY.ID", "COMPANY.NAME", "Incoming.NO", "Incoming.DATE",
                            "STATUS", "CREATED");
                $loop = 1;
                $z = 8;
                foreach($results as $result) 
                {   $z1 = $z + $loop;
                    $data[] = array( $loop, 
                        $result->company_id, $result->company_name, $result->grin_no, $result->grin_date,
                        $result->grin_status_name, 
                        $this->TimeStampToString($result->created_date,"d/m/Y H:i")
                    );
                    $loop = $loop + 1;
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'List Of Incoming Delivery',
                    'listID',
                    array( 
                        "A" => 3,                            "B" => 10,
                        "C" => 25,                           "D" => 15,
                        "E" => 10,                           "F" => 10,
                        "G" => 10),
                    $data,
                    'A',
                    'I');

                $y = $z + $loop +1;
                $sheet = $this->report->getActiveSheet();
                $this->report->download('xls');
            };
            break;
            case 5 : // search detail
            {   $this->records = GrinDetail::Search($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 9 : // searchExt Grin_NO
            {   $result = $this->rsExtJson( Grin::SearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
            case 10 : // searchExt Grin ITEMS
            {   $result = $this->rsExtJson( GrinDetail::itemsneedpoSearch($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
            case 11 : // search po reference
            {   $this->records = Grin::increfSearch($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 12 : // search po detail information
            {   $this->records = Grin::podetailSearch($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 13 : // search grin subdetail reference
            {   $this->records = GrinSubDetail::referenceSearch($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 14 : // search detail karung
            {   $this->records = GrinSubDetail::Search($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 15 : // save detail karung
            {   $session = $this->request->session();
                // $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                // $modi_data = json_decode(stripslashes($this->request->modi));
                $result = GrinSubDetail::incomingSave(
                                            $json_data, 
                                            $session->get("site"),
                                            $session->get("user_id")
                                        );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 16 : // remove detail karung
            {   $this->records = GrinSubDetail::subdetailRemove($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 17 : // sync subdetail with detail quantity
            {   $result = GrinDetail::subdetailSyncronize($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
        };
        return $result;
    }
    /**
    * CRUD Outgoing
    */
    public function crudOutgoing($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->records = Grin::headerPrintOut($this->request);
                        $this->crudOutgoing(3); }
                    else 
                    {   $this->records = Grin::outgoingSearch($this->request, "O");
                        $this->crudOutgoing(4); 
                    };
                }
                else
                {   $this->records = Grin::outgoingSearch($this->request, "O");
                    $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                $modi_data = json_decode(stripslashes($this->request->modi));
                $result = Grin::outgoingSave( $this->request->st,
                                        $head_data, 
                                        $json_data, 
                                        $modi_data, 
                                        $session->get("site"),
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Grin::outgoingDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {   //print_r($this->records);
                $data = $this->records;
                $title = "OUTGOING DELIVERY";
                $filename = "grin_".$data->grin_no;

                $this->create_report_pdf($this->request->session());
                $pdf = $this->report;
                if ($data->grin_status == 0) // draft document
                {   $this->pdf_watermark(70, 110, " D R A F T ", 45); };
                ########## Document TITLE ##########
                $pdf->DocTitle($title, $data->grin_no, 30, 160);
                // $this->report->SetProtection(array('print'), '', null);
                ########## PAGE TITLE ##########
                $pdf->SetFont('Helvetica','B', 11);  
                $pdf->setRowXY(12, 40);  
                $pdf->Bookmark("Header", 1, -1);
                //$this->report->MultiCell(30,4,"Master",0,'L',0);
                $pdf->setRowXY(15, 40);
                $pdf->SetWidths(array(30, 70, 30, 60));
                $pdf->SetAligns(array("L", "L", "L", "L"));
                $pdf->SetFont('Times','', 10);
                $pdf->addRowHeight(5);
                $pdf->Row(array("OUTGOING.No", ": ".$data->grin_no, 
                                "Currier.No", ": ".$data->curier_no), false);
                $pdf->Row(array("OUTGOING.Date", ": ".$this->TimeStampToString($data->created_date, "d/m/Y"),
                                "Currier.Name", ": ".$data->curier_name), false);
                $pdf->Row(array("Order.No", ": ".$data->ref_no, 
                                "Currier.PIC", ": ".$data->curier_pic), false);
                $pdf->Row(array("Party.Name", ": ".$data->party_name, 
                                "Currier.Phone", ": ".$data->curier_phone_no), false);
                $pdf->Row(array("Delivery.No", ": ".$data->party_delivery_no, 
                                "WareHouse", ": ".$data->warehouse_name), false);
                $pdf->Row(array("Delivery.Date", ": ".$this->TimeStampToString($data->party_delivery_date, "d/m/Y"), "", ""), false);
                // rectacngle boxs
                $y_current = $pdf->y_pos+5;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height ); 
                $pdf->Rect($pdf->x_pos+(100), $y_current, 85, $y_height ); 
                $pdf->SetWidths(array(30, 160));
                $pdf->SetAligns(array("L", "L"));
                $pdf->Row(array("Remarks", ": ".$data->description), false);
                $pdf->addRowHeight(5);
                $y_current = $pdf->y_pos+5;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $pdf->getY()-10, 185, 10); 
                /* DETAIL ITEMS */
                $details = GrinDetail::detailPrintOut(trim($data->grin_no));
                $pdf->SetFont('Helvetica','B', 10);
                // $pdf->addRowHeight(5);
                $pdf->SetWidths(array(10, 130, 30, 15));
                $pdf->Bookmark("Detail", 1, -1);
                $pdf->SetAligns(array("C", "C", "C", "C"));
                $pdf->Row(array("No", "Item Description","Quantity", "UM"), true);
                $pdf->SetAligns(array("L", "L", "R", "L"));
                $pdf->SetFont('Helvetica','', 10);
                $loop = 1;
                foreach ($details as $detail) 
                {   $pdf->Row(array($loop,
                                    $detail->item_name, 
                                    $detail->quantity_primary,
                                    $detail->um_primary), true);
                    $loop += 1;
                };
                
                $pdf->addRowHeight(5);
                $pdf->SetWidths(array(40, 150));
                $pdf->SetAligns(array("L", "L"));
                $loop = 0;
                // foreach (explode(",", $data->payment_term) as $the_term)
                // {   if ($loop == 0) 
                //     {   $pdf->Row(array("Payment Term", ": ".$the_term), false); 
                //         $loop = 1;
                //     }
                //     else {  $pdf->Row(array("", " ".$the_term), false); }
                // };
                
                $pdf->Output($filename.".pdf", "I");
                exit;
            };
            break;
            case 4 : // print xls
            {   //print_r($this->records);
                $results = $this->records;
                $data = array();
                $data[] = array("NO", "COMPANY.ID", "COMPANY.NAME", 
                            "Outgoing.NO", "Outgoing.DATE","Order.No",
                            "Buyer.ID", "Buyer.Name",
                            "CREATED");
                $loop = 1;
                $z = 8;
                foreach($results as $result) 
                {   $z1 = $z + $loop;
                    $data[] = array( $loop, 
                        $result->company_id, $result->company_name, 
                        $result->grin_no, $result->grin_date, $result->ref_no,
                        $result->party_id, $result->party_name,
                        $this->TimeStampToString($result->created_date,"d/m/Y H:i")
                    );
                    $loop = $loop + 1;
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'List Of Outgoing Delivery - Orders',
                    'listODOrders',
                    array( 
                        "A" => 3,       "B" => 10,
                        "C" => 25,      "D" => 15,
                        "E" => 15,      "F" => 15,
                        "G" => 10,      "H" => 10,
                        "I" => 15),
                    $data,
                    'A',
                    'I');

                $y = $z + $loop +1;
                $sheet = $this->report->getActiveSheet();
                $this->report->download('xls');
            };
            break;
            case 5 : // View Outgoing Order
            {   $this->records = GrinSubDetail::ViewOutgoingOrder($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 9 : // searchExt Grin_NO
            {   $result = $this->rsExtJson( Grin::SearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
            case 10 : // searchExt Grin ITEMS
            {   $result = $this->rsExtJson( GrinDetail::itemsneedpoSearch($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
            case 11 : // search reference
            {   //$this->records = Grin::outrefSearch($this->request);
                $this->records = Grin::outrefDetailSearch($this->request, "search_grid");
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 12 : // search detail information
            {   $this->records = Grin::outrefDetailSearch($this->request, " ");
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 13 : // search grin subdetail reference
            {   $this->records = GrinSubDetail::referenceSearch($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 14 : // search detail karung
            {   $this->records = GrinSubDetail::Search($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 15 : // save detail karung
            {   $session = $this->request->session();
                // $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                // $modi_data = json_decode(stripslashes($this->request->modi));
                $result = GrinSubDetail::incomingSave(
                                            $json_data, 
                                            $session->get("site"),
                                            $session->get("user_id")
                                        );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 16 : // remove detail karung
            {   $this->records = GrinSubDetail::subdetailRemove($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 17 : // sync subdetail with detail quantity
            {   $result = GrinDetail::subdetailSyncronize($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 18 : // get grinSubdetail per group_no
            {   $this->records = GrinSubDetail::searchPassedItemsPerGroup($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 20 : // search outgoing general data
            {   
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->records = Grin::headerPrintOut($this->request);
                        $this->crudOutgoing(3); }
                    else 
                    {   $this->records = Grin::outgoingSearch($this->request, "G");
                        $this->crudOutgoing(4); 
                    };
                }
                else
                {   $this->records = Grin::outgoingSearch($this->request, "G");
                    $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            
        };
        return $result;
    }
    /**
     * CRUD Stock
    */
    public function crudStock($subtask)
    {   switch ($subtask)
        {   case 0 : // search available stocks for outgoing orders
            {   if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->records = Grin::StockPrintOut($this->request);
                        $this->crudStock(3); }
                    else 
                    {   $this->records = Grin::StockOutgoingOrderFindItems($this->request);
                        $this->crudStock(4); 
                    };
                }
                else
                {   $this->records = Grin::StockOutgoingOrderFindItems($this->request);
                    $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // search available stocks for outgoing general
            {   if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->records = Grin::StockPrintOut($this->request);
                        $this->crudStock(3); }
                    else 
                    {   $this->records = Grin::StockOutgoingGeneralFindItems($this->request);
                        $this->crudStock(4); 
                    };
                }
                else
                {   $this->records = Grin::StockOutgoingGeneralFindItems($this->request);
                    $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
        };
        return $result;
    }
    /**
     * CRUD Site Labor Costs
    */
    public function crudSiteLaborCosts($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->records = SiteLaborCosts::headerPrintOut($this->request);
                        $this->crudSiteLaborCosts(3); }
                    else 
                    {   $this->records = SiteLaborCosts::Search($this->request);
                        $this->crudSiteLaborCosts(4); 
                    };
                }
                else
                {   $this->records = SiteLaborCosts::Search($this->request);
                    $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = SiteLaborCosts::sitelaborcostsSave(    
                                        $json_data, 
                                        $session->get("site"),
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = SiteLaborCosts::sitelaborcostsDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {   //print_r($this->records);
                $data = $this->records;
                $title = "Site Labor Costs";
                $filename = "sitelaborcosts";

                $this->create_report_pdf($this->request->session());
                $pdf = $this->report;
                if ($data->grin_status == 0) // draft document
                {   $this->pdf_watermark(70, 110, " D R A F T ", 45); };
                ########## Document TITLE ##########
                $pdf->DocTitle($title, $data->grin_no, 30, 160);
                // $this->report->SetProtection(array('print'), '', null);
                ########## PAGE TITLE ##########
                $pdf->SetFont('Helvetica','B', 11);  
                $pdf->setRowXY(12, 40);  
                $pdf->Bookmark("Header", 1, -1);
                //$this->report->MultiCell(30,4,"Master",0,'L',0);
                $pdf->setRowXY(15, 40);
                $pdf->SetWidths(array(30, 70, 30, 60));
                $pdf->SetAligns(array("L", "L", "L", "L"));
                $pdf->SetFont('Times','', 10);
                $pdf->addRowHeight(5);
                $pdf->Row(array("SiteLaborCost.No", ": ".$data->grin_no, 
                                "Remarks", ": ".$data->description), false);
                $pdf->Row(array("SiteLaborCost.Date", ": ".$data->created_date, 
                                "WareHouse", ": ".$data->warehouse_name), false);
                $pdf->Row(array($data->ref_type.".No", ": ".$data->ref_no, "", ""), false);
                $pdf->Row(array("Supplier", ": ".$data->party_name, "", ""), false);
                $pdf->Row(array("Delivery.No", ": ".$data->party_delivery_no, "", ""), false);
                $pdf->Row(array("Delivery.Date", ": ".$data->party_delivery_date, "", ""), false);
                $pdf->Row(array("WareHouse", ": ".$data->warehouse_name, "", ""), false);
                $pdf->addRowHeight(5);
                // rectacngle boxs
                $y_current = $pdf->y_pos+5;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height ); 
                $pdf->Rect($pdf->x_pos+(100), $y_current, 85, $y_height ); 
                /* DETAIL ITEMS */
                $details = SiteLaborCostsDetail::detailPrintOut(trim($data->grin_no));
                $pdf->SetFont('Helvetica','B', 10);
                $pdf->addRowHeight(5);
                $pdf->SetWidths(array(10, 70, 30, 30, 30, 15));
                $pdf->Bookmark("Detail", 1, -1);
                $pdf->SetAligns(array("C", "C", "C", "C", "C", "C"));
                $pdf->Row(array("No", "Item Description","Passed", "Reject", "Total", "UM"), true);
                $pdf->SetAligns(array("L", "L", "R", "R", "R", "L"));
                $pdf->SetFont('Helvetica','', 10);
                $loop = 1;
                foreach ($details as $detail) 
                {   $pdf->Row(array($loop,
                                    $detail->item_name, 
                                    $detail->quantity_primary,
                                    $detail->reject_primary,
                                    $detail->total_primary,
                                    $detail->um_primary), true);
                    $loop += 1;
                };
                
                $pdf->Output($filename.".pdf", "I");
                exit;
            };
            break;
            case 4 : // print xls
            {   //print_r($this->records);
                $results = $this->records;
                $data = array();
                $data[] = array("NO", "COMPANY.ID", "COMPANY.NAME", "SiteLaborCost.NO", "SiteLaborCost.DATE",
                            "STATUS", "CREATED");
                $loop = 1;
                $z = 8;
                foreach($results as $result) 
                {   $z1 = $z + $loop;
                    $data[] = array( $loop, 
                        $result->company_id, $result->company_name, $result->grin_no, $result->grin_date,
                        $result->grin_status_name, 
                        $this->TimeStampToString($result->created_date,"d/m/Y H:i")
                    );
                    $loop = $loop + 1;
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'List Of SiteLaborCost',
                    'listID',
                    array( 
                        "A" => 3,                            "B" => 10,
                        "C" => 25,                           "D" => 15,
                        "E" => 10,                           "F" => 10,
                        "G" => 10),
                    $data,
                    'A',
                    'I');

                $y = $z + $loop +1;
                $sheet = $this->report->getActiveSheet();
                $this->report->download('xls');
            };
            break;
            case 5 : // search detail
            {   $this->records = SiteLaborCostsDetail::Search($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
}