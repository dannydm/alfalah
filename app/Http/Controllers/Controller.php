<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
// use App\Http\Controllers\FpdfController;
use Fpdf;
use Excel;
// use PHPExcel;
// use PHPExcel_IOFactory; 

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /**
    * Public Properties
    */
    public $server_message;
    public $request;
    public $report;
    // public $skin = 'Ext'; // Ext or LTE
    public $companies = array
    (   "company_code" => "YMDT",
        "company_name" => "ALFALAH",
        "company_site" => "SBY",
        "company_logo" => "rpt_saa_logo.jpg",
        "company_jkt_address" => "Jalan Nama No",
        "company_jkt_info" => "* Telp Fax",
        "company_krw_address" => "Jalan Nama No",
        "company_krw_info" => "* Telp Fax",
        "company_prb_address" => "Jalan Nama No",
        "company_prb_info" => "* Telp Fax",
        "company_sby_address" => "Jl. Diamond Hill DR 6/23 Citraland, Surabaya 60213 – Indonesia",
        "company_sby_info" => "* Telp (+62-31) xxxx Fax",
    );
    /**
    * Public Functions
    */
    // clear single quotes
    public function clearQuotes($data)
    {   $replace_str = array("'", "\\");
        return str_replace($replace_str, "", $data);
    }
    // convert string date to DBF Format date
    public function makeDBFDate($the_date,$the_format)
    {   switch($the_format)
        {   case "d/m/Y":
            {   $day    = substr($the_date,0,2);
                $month  = substr($the_date,3,2);
                $year   = substr($the_date,6,4);
            };
            break;
            case "Y-m-d":
            {   $year   = substr($the_date,0,4);
                $month  = substr($the_date,5,2);
                $day    = substr($the_date,8,2);
            };
            break;
        };
        return date("Ymd", strtotime(strftime('%Y-%m-%d', mktime(0, 0, 0, $month, $day, $year))));
        // date('Ymd', strtotime(strftime('%Y-%m-%d',mktime(0, 0, 0, $m, $d, $y))));
    }
    // convert string date to timestamp format
    public function makeTimeStamp($the_date,$the_format)
    {   switch($the_format)
        {   case "d/m/Y":
            {   $day    = substr($the_date,0,2);
                $month  = substr($the_date,3,2);
                $year   = substr($the_date,6,4);
            };
            break;
            case "Y-m-d":
            {   $year   = substr($the_date,0,4);
                $month  = substr($the_date,5,2);
                $day    = substr($the_date,8,2);
            };
            break;
        };
        return date("Y-m-d H:i:s", mktime(0, 0, 0, $month, $day, $year)); //Y-m-d H:i:s
    }
    // convert timestamp to string date format
    public function TimeStampToString($the_date, $the_format)
    {   if (! empty($the_date))
        {   switch ($the_format)
            {   case "d/m/Y":
                {   return strftime("%d/%m/%Y", strtotime($the_date)); };
                break;
                case "d/m/Y H:i:s":
                {   return strftime("%d/%m/%Y %H:%M:%S", strtotime($the_date)); };
                break;
                case "d/m/Y H:i":
                {   return strftime("%d/%m/%Y %H:%M", strtotime($the_date)); };
                break;
                case "H:i":
                {   return strftime("%H:%M", strtotime($the_date)); };
                break;
            };
        } else return "";
    }
    // Generate Json data with Array Limit Capability.
    public function rsJson($results, $limit, $start)
    {   
        if ($results)
        {   $total = $results->count(); 
            $page = 1;
            if ($start > 0){ $page = $page + ($start / $limit); };
            
            $arr = array_values($results->forPage($page, $limit)->toArray());
            
        };
        $the_fields = array_keys(collect($arr[0])->toArray());
        $col_arr = array();
        foreach ($the_fields as $key => $value)
        {   $my_arr = array("name" => $value);
            array_push($col_arr, $my_arr);
        };

        switch (config('app.system_skin'))
        {   case "Ext":
            {   
                $json = array(
                    "metaData" => array(
                        "totalProperty" => "total_property",
                        "fields"        => $col_arr,
                        "root"          => "rows",
                        "id"            => "row_id"
                    ),
                    "rows"           => $arr,
                    "total_property" => $total,
                    "start"          => $start,
                    "limit"          => $limit,
                );
            };
            break;
            case "LTE":
            {
                $json = array(
                    "status"         => "success",
                    "total"          => $total,
                    "records"        => $arr,
                );
            };
            break;
        };    
        return json_encode($json);
    }
    public function rsExtJson($results, $limit, $start)
    {   
        if ($results)
        {   $total = $results->count(); 
            $page = 1;
            if ($start > 0){ $page = $page + ($start / $limit); };
            
            $arr = array_values($results->forPage($page, $limit)->toArray());
            
        };
        $the_fields = array_keys(collect($arr[0])->toArray());
        $col_arr = array();
        foreach ($the_fields as $key => $value)
        {   $my_arr = array("name" => $value);
            array_push($col_arr, $my_arr);
        };
        $json = array(
            "metaData" => array(
                "totalProperty" => "total_property",
                "fields"        => $col_arr,
                "root"          => "rows",
                "id"            => "row_id"
            ),
            "rows"           => $arr,
            "total_property" => $total,
            "start"          => $start,
            "limit"          => $limit,
            );
        return json_encode($json);
    }
    // Generate Json data success failed message.
    public function jsonSuccess($rs)
    {   if ($rs)
        {   $result= array("success" => true, "message" => "Succeed"); }
        else
        {
            $result= array("success" => false,
                           "message" => "Failed to save records.",
                           "server_message" => $this->server_message
                        );
        };
        return json_encode($result);
    }
    /**
    * Get Company Info 
    */
    public function getCompanyInfo($company_id, $site_id)
    { 
        $result = array();
        switch (trim($company_id))
        {   
            case "YMDT" :
                $result = array(
                    "company_id" => $company_id,
                    "name"       => "ALFALAH",
                    // "logo"       => "images/rpt_ertx_logo.jpeg"
                    "logo"       => "images/logo.jpeg"
                );
            break;
        };
        switch ($site_id)
        {   
            case "SBY" :
                $result = array_merge($result,
                    array( "site_id"    => $site_id,
                        "address" => "Jl. Raya , Surabaya 60213 - Indonesia",
                        "info"    => "* Telp (+62-31) xxx Fax",
                    ));
            break;
        };
        return $result;
    }
    public function pdf_watermark($x, $y, $txt, $angle)
    {   //Text rotated around its origin
        $this->report->SetFont('Helvetica','B', 70);
        $this->report->SetTextColor(255,192,203);
        $this->report->Rotate($angle,$x,$y);
        $this->report->Text($x,$y,$txt);
        $this->report->Rotate(0);
        $this->report->SetTextColor(0,0,0);
    }
    /*********************
    *** create report
    **********************/
    public function create_report_pdf($session)
    {   //$company_id, $site_id, $doc_no
        $result = $this->getCompanyInfo($session->get("company"),$session->get("site"));
        $this->report = new FpdfController('P','mm','A4');
        $this->report->setCompany($result);
        $this->report->set_User(null, null, $session->get("username"));
        $this->report->AddPage();
        
        $this->report->AliasNbPages();
        $this->report->SetFont('Times','',12);
        
    }
    public function create_report_pdf_kwitansi($session)
    {   //$company_id, $site_id, $doc_no
        $result = $this->getCompanyInfo($session->get("company"),$session->get("site"));
        $this->report = new FpdfController('L','mm','A4');
        $this->report->setCompany($result);
        $this->report->set_User(null, null, $session->get("username"));
        $this->report->AddPage();
        
        $this->report->AliasNbPages();
        $this->report->SetFont('Times','',12);
        
    }

    public function create_report_xls($company_id, $site_id, $username, $filename, $title)
    {   // need memory arrangement for processing big big data
        $company_info = $this->getCompanyInfo($company_id, $site_id);
         
        $this->report = Excel::create($filename, 
            function($excel) use ($company_info, $title) 
            {   
                $excel->setTitle($title)
                    ->setCreator($company_info["name"])
                    ->setSubject($title)
                    ->setCompany($company_info["name"])
                    ->setLastModifiedBy($company_info["name"])
                    ->setKeywords("")
                    ->setDescription($title);

                $excel->sheet('Sheet 1', function ($sheet) use ($company_info) {
                    $sheet->setOrientation('landscape');
                    $sheet->getRowDimension(2)->setRowHeight(15);
                    $sheet->setCellValue('A2', $company_info["name"])
                        ->mergeCells('A2:G2')
                        ->getStyle('A2')->applyFromArray(array('font' => array( 'bold' => true),));
                    $sheet->setCellValue('A3', $company_info["address"])
                           ->mergeCells('A3:G3');
                    $sheet->setCellValue('A4', $company_info["info"])
                           ->mergeCells('A4:G4')
                           ->getStyle('A3:A4')->applyFromArray(array('font' => array( 'size' => 8),));
                });
            });
        
        // $this->report = new PHPExcel();

        // $this->report->setActiveSheetIndex(0);
        // header('Content-Type: application/vnd.ms-excel');
        // header('Content-Disposition: attachment;filename="customcaturwulan.xls"');
        // header('Cache-Control: max-age=0');
        // header("Pragma: no-cache");
        // header("Expires: 0");
        // //$result = PHPExcel_IOFactory::createWriter($this->report, 'Excel5');
        // $result = \PHPExcel_IOFactory::createWriter($this->report, 'Excel2007');
        // //new PHPExcel_Writer_Excel2007($objPHPExcel);
        // $result->save('php://output');
        // // required exit command to replace "break" command
        // exit;
    }
    public function create_report_xls_master($rec_count, $session, $title, $filename, 
                        $widths, $data, $border_start, $border_end)
    {   
        $this->create_report_xls(
            $session->get("company"),
            $session->get("site"), 
            $session->get("username"), 
            $filename, 
            $title);
        $sheet = $this->report->getActiveSheet();
        $sheet->getRowDimension(6)->setRowHeight(20);
        $sheet->setCellValue('B6', $title)
            ->mergeCells('B6:E6')
            ->getStyle('B6')
            ->applyFromArray(array('font' => array( 'bold' => true,'size'=>18),));

        $w = 10;
        if ( $rec_count > 0 )
        {   // Columns Headers
            $w = 8;
            $sheet->getRowDimension($w)->setRowHeight(15);
            $loop = $w + $rec_count;
            // $set_memory = false;
            // ini_set('memory_limit', '512M'); 
            // apply the data
            $sheet->fromArray($data, null, 'A'.$w, false, false);
            // formating worksheet
            foreach ($widths as $key => $value)
            {   switch ($value)
                {   case 0:
                    {   $sheet->getColumnDimension($key)->setAutoSize(true); 
                    };
                    break;
                    default :
                    {   $sheet->setWidth($key, $value); };
                    break;
                };
            };
            $sheet->getStyle($border_start.$w.':'.$border_end.$loop)
                ->applyFromArray(array(
                      'font' => array( 'size'  => 10),
                      'borders' => array(
                          'allborders' => array(
                              'style' => \PHPExcel_Style_Border::BORDER_THIN,
                ),),));
	    $sheet->setCellValue('H4', "printed : " . date('d/m/Y H:i:s'))
                ->getStyle('H4')->applyFromArray(['font' => ['size' => 8]]);
	    unset($results);
            unset($session);
            unset($data);
        };
    } 
}
