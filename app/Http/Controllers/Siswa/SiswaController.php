<?php

namespace App\Http\Controllers\Siswa;

use App\Http\Controllers\Controller;
use App\Models\Ppdb\CalonSiswa;
use App\Models\Ppdb\CalonSiswaFile;
use App\Models\Ppdb\CalonSiswaDetail;
use App\Models\Siswa\BankTransfer;
use App\Models\Siswa\Siswa;
use App\Models\Siswa\SiswaReceivable;
use Illuminate\Http\Request;
use Excel;

class SiswaController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask) {
        switch ($task) {
            // routing data
            case 0:
                return $this->routing($substask);
                break;
            // Profile tab
            case 1:
                return $this->crudProfile($substask);
                break;
            // Siswa tab
            case 2:
                return $this->crudSiswa($substask);
                break;
            // Siswa Receivable tab
            case 3:
                return $this->crudSiswaReceivable($substask);
                break;
            // Sync Table
            case 4:
                return $this->crudSync($substask);
                break;
            case 5:
                return $this->crudCalonSiswa($substask);
                break;
        };
    }

    /**
     * Create a new controller instance.
     */
    public function routing($subtask) {
        $session       = $this->request->session();
        $this->myTasks = array_flip(
            Siswa::myTasks(
                $session->get("user_id"),
                $this->request->m)
        );
        switch ($subtask) {
            case 0:
                // profile page
                {
                    $siswa  = Siswa::Profile($session->get("username"));
                    $result = view('siswa/profile' . config('app.system_skin'))
                        ->with("TABID", $this->request->get("tabId"))
                        ->with("KEYID", $siswa->keyid)
                        ->with("NIS", $siswa->nis)
                        ->with("JENJANG", $siswa->nama_jenjang_sekolah)
                        ->with("KELAS", $siswa->nama_kelas)
                        ->with("FULLNAME", $siswa->nama_lengkap)
                        ->with("BIRTHPLACE", $siswa->tempat_lahir)
                        ->with("ADDRESS", $siswa->alamat_rumah)
                        ->with("GENDER", $siswa->jenis_kelamin)
                        ->with("NICKNAME", $siswa->nama_panggilan)
                        ->with("BIRTHDATE", $siswa->tanggal_lahir);
                };
                break;
            case 1:
                // siswa page
                {
                    $result = view('siswa/siswa' . config('app.system_skin'))
                        ->with("TABID", $this->request->get("tabId"))
                        ->with("MyTasks", $this->myTasks);
                };
                break;
            case 2:
                // Profile receivable landing page
                {
                    $siswa       = Siswa::Profile($session->get("username"));
                    $receivables = SiswaReceivable::Receivable($siswa->keyid);
                    //print_r($receivable); exit;
                    $result = view('siswa/receivable' . config('app.system_skin'))
                        ->with("TABID", $this->request->get("tabId"))
                        ->with("KEYID", $siswa->keyid)
                        ->with("NIS", $siswa->nis)
                        ->with("NOPEN", $siswa->no_pendaftaran)
                        ->with("FULLNAME", $siswa->nama_lengkap)
                        ->with("BIRTHPLACE", $siswa->tempat_lahir)
                        ->with("ADDRESS", $siswa->alamat_rumah)
                        ->with("GENDER", $siswa->jenis_kelamin)
                        ->with("NICKNAME", $siswa->nama_panggilan)
                        ->with("BIRTHDATE", $siswa->tanggal_lahir)
                        ->with("RECEIVABLES", $receivables);
                };
                break;
            case 3:
                // siswa receivable landing page
                {
                    $result = view('siswa/siswareceivable' . config('app.system_skin'))
                        ->with("TABID", $this->request->get("tabId"))
                        ->with("MyTasks", $this->myTasks);
                };
                break;
            case 5:
                // calon siswa PPDB page
                {
                    $result = view('siswa/siswappdb' . config('app.system_skin'))
                        ->with("TABID", $this->request->get("tabId"))
                        ->with("MyTasks", $this->myTasks);
                };
                break;
        };
        return $result;
    }

    /**
     * CRUD Profile
     */
    public function crudProfile($subtask) {
        switch ($subtask) {
            case 0:
                // search data
                {
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->records = Siswa::headerPrintOut($this->request);
                            $this->crudSiswa(3);
                        } else {
                            $this->records = Siswa::Search($this->request);
                            $this->crudSiswa(4);
                        };
                    } else {
                        $this->records = Siswa::Search($this->request);
                        $result        = $this->rsExtJson($this->records,
                            $this->request->limit, $this->request->start);
                    };
                };
                break;
            case 1:
                // save data
                {
                    $session   = $this->request->session();
                    $head_data = json_decode(stripslashes($this->request->head));
                    $json_data = json_decode(stripslashes($this->request->json));
                    $modi_data = json_decode(stripslashes($this->request->modi));
                    $result    = Siswa::Siswaave($head_data,
                        $json_data,
                        $modi_data,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
                    if ($result[0] == false) {$this->server_message = $result[1];};
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2:
                // delete data
                {
                    $result = Siswa::SiswaDelete($this->request);
                    if ($result[0] == false) {$this->server_message = $result[1];};
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3:
                // print pdf
                {
                    //print_r($this->records);
                    $data     = $this->records;
                    $title    = "PROFORMA INVOICE";
                    $filename = "pi_" . $data->pi_no;

                    $this->create_report_pdf($this->request->session());
                    $pdf = $this->report;
                    ########## Document TITLE ##########
                    $pdf->DocTitle($title, $data->pi_no, 30, 160);
                    // $this->report->SetProtection(array('print'), '', null);
                    ########## PAGE TITLE ##########
                    $pdf->SetFont('Helvetica', 'B', 11);
                    $pdf->setRowXY(12, 40);
                    $pdf->Bookmark("Header", 1, -1);
                    //$this->report->MultiCell(30,4,"Master",0,'L',0);
                    $pdf->setRowXY(15, 40);
                    $pdf->SetWidths([40, 60, 30, 60]);
                    $pdf->SetAligns(["L", "L", "L", "L"]);
                    $pdf->SetFont('Times', '', 10);
                    $pdf->addRowHeight(5);
                    $pdf->Row(["PO.No", ": " . $data->Siswa_no,
                        "PO.Date", ": " . $data->Siswa_date], false);
                    $pdf->Row(["PI.No", ": " . $data->pi_no,
                        "PI.Date", ": " . $data->pi_date], false);
                    $pdf->addRowHeight(1);
                    // rectangle boxs
                    $y_current = $pdf->y_pos + 5;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height);
                    $pdf->Rect($pdf->x_pos + (100), $y_current, 85, $y_height);

                    $pdf->addRowHeight(1);
                    $pdf->SetWidths([40, 150]);
                    $pdf->SetAligns(["L", "L"]);
                    $pdf->Row(["Shipper ", ": " . strtoupper($data->company_name)], false);
                    $pdf->Row(["", "  " . strtoupper($data->company_address)], false);
                    // rectangle boxs
                    $y_current = $y_current + $y_height;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 185, $y_height);

                    $pdf->addRowHeight(1);
                    $pdf->Row(["Consignee", ": " . strtoupper($data->buyer_name)], false);
                    $pdf->Row(["", "  " . strtoupper($data->buyer_address)], false);
                    // rectangle boxs
                    $y_current = $y_current + $y_height;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 185, $y_height);

                    $pdf->addRowHeight(5);
                    $pdf->SetWidths([40, 150]);
                    $pdf->Row(["Beneficiary Name", ": " . strtoupper($data->company_name)], false);
                    $pdf->Row(["Beneficiaty Bank", ": " . strtoupper($data->bank_name)], false);
                    $pdf->Row(["Bank Account (USD)", ": " . strtoupper($data->bank_account)], false);
                    $pdf->Row(["Swift Code", ": " . strtoupper($data->swift_code)], false);
                    $pdf->Row(["Bank Address", ": " . strtoupper($data->bank_address)], false);
                    // rectangle boxs
                    $y_current = $y_current + $y_height;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 185, $y_height);

                    $pdf->addRowHeight(5);
                    $pdf->SetWidths([40, 60, 30, 60]);
                    $pdf->Row(["Container / Seal Vessel", ": " . $data->container_type_name,
                        "ETD", ": " . $data->etd], false);
                    $pdf->Row(["Date of Shipment", ": " . $data->shipment_date,
                        "ETA", ": " . $data->eta], false);
                    $pdf->Row(["Port Of Loading ", ": " . $data->port_loading_name,
                        "Gross Weight", ": " . $data->weight_gross], false);
                    $pdf->Row(["Port Of Discharge ", ": " . $data->port_discharge_name,
                        "Nett Weight", ": " . $data->weight_nett], false);
                    $pdf->Row(["", "", "CBM", ": " . $data->cbm], false);
                    // rectangle boxs
                    $y_current = $y_current + $y_height;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height);
                    $pdf->Rect($pdf->x_pos + (100), $y_current, 85, $y_height);
                    /* DETAIL ITEMS */
                    $details = SiswaDetail::detailPrintOut(trim($data->Siswa_no));
                    $pdf->SetFont('Times', 'B', 10);
                    $pdf->addRowHeight(2);
                    $pdf->SetWidths([65, 20, 20, 20, 20, 20, 20]);
                    $pdf->SetAligns(["C", "C", "C", "C", "C", "C", "C"]);
                    $pdf->Row(["COMODITY", "QTY (Fcl)", "QTY (Bags)", "NUT.QTY (Pcs)", "NUT.QTY (Kgs)", "PRICE (USD)(Kgs)", "AMOUNT (USD)"], true);
                    $pdf->SetFont('Times', '', 10);
                    $pdf->SetAligns(["L", "R", "R", "R", "R", "R", "R"]);
                    foreach ($details as $detail) {
                        $pdf->Row([$detail->item_name,
                            $detail->quantity_fcl,
                            $detail->quantity_bags,
                            $detail->quantity_nut_pcs,
                            $detail->quantity_nut_kgs,
                            $detail->unit_price,
                            $detail->amount], true);
                    };
                    $pdf->addRowHeight(5);
                    $pdf->SetWidths([40, 150]);
                    $pdf->SetAligns(["L", "L"]);
                    $loop = 0;
                    foreach (explode(",", $data->payment_term) as $the_term) {
                        if ($loop == 0) {
                            $pdf->Row(["Payment Term", ": " . $the_term], false);
                            $loop = 1;
                        } else { $pdf->Row(["", " " . $the_term], false);}
                    };

                    $pdf->Output($filename . ".pdf", "I");
                    exit;
                };
                break;
            case 4:
                // print xls
                {
                    $results = $this->records;
                    $data    = [];
                    $data[]  = ["NO", "COMPANY.ID", "COMPANY.NAME", "Siswa.NO", "Siswa.DATE",
                        "BUYER.ID", "BUYER.NAME", "PI.NO", "PI.DATE", "CREATED"];
                    $loop = 1;
                    $z    = 8;
                    foreach ($results as $result) {
                        $z1     = $z + $loop;
                        $data[] = [$loop,
                            $result->company_id, $result->company_name, $result->Siswa_no, $result->Siswa_date,
                            $result->buyer_id, $result->buyer_name, $result->pi_no, $result->pi_date,
                            $this->TimeStampToString($result->created_date, "d/m/Y H:i"),
                        ];
                        $loop = $loop + 1;
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'List Of Siswa',
                        'listSiswa',
                        [
                            "A" => 3, "B"  => 8,
                            "C" => 25, "D" => 15,
                            "E" => 10, "F" => 10,
                            "G" => 10, "H" => 20,
                            "I" => 10, "J" => 15],
                        $data,
                        'A',
                        'J');

                    $y     = $z + $loop + 1;
                    $sheet = $this->report->getActiveSheet();
                    $this->report->download('xls');
                };
                break;
            case 5:
                // search Siswa detail
                {
                    $this->records = SiswaDetail::Search($this->request);
                    $result        = $this->rsExtJson($this->records,
                        $this->request->limit, $this->request->start);
                };
                break;
            case 9:
                {
                    $result = $this->rsExtJson(Siswa::SearchExt($this->request),
                        $this->request->limit, $this->request->start);
                };
                break;
        };
        return $result;
    }

    /**
     * CRUD Siswa
     */
    public function crudSiswa($subtask) {
        switch ($subtask) {
            case 0:
                // search data
                {
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->records = Siswa::headerPrintOut($this->request);
                            $this->crudSiswa(3);
                        } else {
                            $this->records = Siswa::Search($this->request);
                            $this->crudSiswa(4);
                        };
                    } else {
                        $this->records = Siswa::Search($this->request);
                        $result        = $this->rsJson($this->records,
                            $this->request->limit, $this->request->start);
                    };
                };
                break;
            case 1:
                // save data
                {
                    $session   = $this->request->session();
                    $head_data = json_decode(stripslashes($this->request->head));
                    $json_data = json_decode(stripslashes($this->request->json));
                    $modi_data = json_decode(stripslashes($this->request->modi));
                    $result    = Siswa::Siswasave($head_data,
                        $json_data,
                        $modi_data,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
                    if ($result[0] == false) {$this->server_message = $result[1];};
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2:
                // delete data
                {
                    $result = Siswa::SiswaDelete($this->request);
                    if ($result[0] == false) {$this->server_message = $result[1];};
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3:
                // print pdf
                {
                    //print_r($this->records);
                    $data     = $this->records;
                    $title    = "PROFORMA INVOICE";
                    $filename = "pi_" . $data->pi_no;

                    $this->create_report_pdf($this->request->session());
                    $pdf = $this->report;
                    ########## Document TITLE ##########
                    $pdf->DocTitle($title, $data->pi_no, 30, 160);
                    // $this->report->SetProtection(array('print'), '', null);
                    ########## PAGE TITLE ##########
                    $pdf->SetFont('Helvetica', 'B', 11);
                    $pdf->setRowXY(12, 40);
                    $pdf->Bookmark("Header", 1, -1);
                    //$this->report->MultiCell(30,4,"Master",0,'L',0);
                    $pdf->setRowXY(15, 40);
                    $pdf->SetWidths([40, 60, 30, 60]);
                    $pdf->SetAligns(["L", "L", "L", "L"]);
                    $pdf->SetFont('Times', '', 10);
                    $pdf->addRowHeight(5);
                    $pdf->Row(["PO.No", ": " . $data->Siswa_no,
                        "PO.Date", ": " . $data->Siswa_date], false);
                    $pdf->Row(["PI.No", ": " . $data->pi_no,
                        "PI.Date", ": " . $data->pi_date], false);
                    $pdf->addRowHeight(1);
                    // rectangle boxs
                    $y_current = $pdf->y_pos + 5;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height);
                    $pdf->Rect($pdf->x_pos + (100), $y_current, 85, $y_height);

                    $pdf->addRowHeight(1);
                    $pdf->SetWidths([40, 150]);
                    $pdf->SetAligns(["L", "L"]);
                    $pdf->Row(["Shipper ", ": " . strtoupper($data->company_name)], false);
                    $pdf->Row(["", "  " . strtoupper($data->company_address)], false);
                    // rectangle boxs
                    $y_current = $y_current + $y_height;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 185, $y_height);

                    $pdf->addRowHeight(1);
                    $pdf->Row(["Consignee", ": " . strtoupper($data->buyer_name)], false);
                    $pdf->Row(["", "  " . strtoupper($data->buyer_address)], false);
                    // rectangle boxs
                    $y_current = $y_current + $y_height;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 185, $y_height);

                    $pdf->addRowHeight(5);
                    $pdf->SetWidths([40, 150]);
                    $pdf->Row(["Beneficiary Name", ": " . strtoupper($data->company_name)], false);
                    $pdf->Row(["Beneficiaty Bank", ": " . strtoupper($data->bank_name)], false);
                    $pdf->Row(["Bank Account (USD)", ": " . strtoupper($data->bank_account)], false);
                    $pdf->Row(["Swift Code", ": " . strtoupper($data->swift_code)], false);
                    $pdf->Row(["Bank Address", ": " . strtoupper($data->bank_address)], false);
                    // rectangle boxs
                    $y_current = $y_current + $y_height;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 185, $y_height);

                    $pdf->addRowHeight(5);
                    $pdf->SetWidths([40, 60, 30, 60]);
                    $pdf->Row(["Container / Seal Vessel", ": " . $data->container_type_name,
                        "ETD", ": " . $data->etd], false);
                    $pdf->Row(["Date of Shipment", ": " . $data->shipment_date,
                        "ETA", ": " . $data->eta], false);
                    $pdf->Row(["Port Of Loading ", ": " . $data->port_loading_name,
                        "Gross Weight", ": " . $data->weight_gross], false);
                    $pdf->Row(["Port Of Discharge ", ": " . $data->port_discharge_name,
                        "Nett Weight", ": " . $data->weight_nett], false);
                    $pdf->Row(["", "", "CBM", ": " . $data->cbm], false);
                    // rectangle boxs
                    $y_current = $y_current + $y_height;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height);
                    $pdf->Rect($pdf->x_pos + (100), $y_current, 85, $y_height);
                    /* DETAIL ITEMS */
                    $details = SiswaDetail::detailPrintOut(trim($data->Siswa_no));
                    $pdf->SetFont('Times', 'B', 10);
                    $pdf->addRowHeight(2);
                    $pdf->SetWidths([65, 20, 20, 20, 20, 20, 20]);
                    $pdf->SetAligns(["C", "C", "C", "C", "C", "C", "C"]);
                    $pdf->Row(["COMODITY", "QTY (Fcl)", "QTY (Bags)", "NUT.QTY (Pcs)", "NUT.QTY (Kgs)", "PRICE (USD)(Kgs)", "AMOUNT (USD)"], true);
                    $pdf->SetFont('Times', '', 10);
                    $pdf->SetAligns(["L", "R", "R", "R", "R", "R", "R"]);
                    foreach ($details as $detail) {
                        $pdf->Row([$detail->item_name,
                            $detail->quantity_fcl,
                            $detail->quantity_bags,
                            $detail->quantity_nut_pcs,
                            $detail->quantity_nut_kgs,
                            $detail->unit_price,
                            $detail->amount], true);
                    };
                    $pdf->addRowHeight(5);
                    $pdf->SetWidths([40, 150]);
                    $pdf->SetAligns(["L", "L"]);
                    $loop = 0;
                    foreach (explode(",", $data->payment_term) as $the_term) {
                        if ($loop == 0) {
                            $pdf->Row(["Payment Term", ": " . $the_term], false);
                            $loop = 1;
                        } else { $pdf->Row(["", " " . $the_term], false);}
                    };

                    $pdf->Output($filename . ".pdf", "I");
                    exit;
                };
                break;
            case 4:
                // print xls
                {
                    $results = $this->records;
                    // print_r($results); exit;

                    $data   = [];
                    $data[] = ["ID", "No.Pen", "NIS", "Nama", "Panggilan",
                        "Gender", "Tempat.Lahir", "Tanggal.Lahir",
                        "Alamat", "No.Telp", "Keterangan", "Status", "CREATED"];
                    $loop = 1;
                    $z    = 8;
                    foreach ($results as $result) {
                        $z1 = $z + $loop;

                        // print_r($result); exit;
                        $data[] = [
                            $result->keyid, $result->no_pendaftaran, $result->nis,
                            $result->nama_lengkap, $result->nama_panggilan,
                            $result->jenis_kelamin, $result->tempat_lahir, $result->tanggal_lahir,
                            $result->alamat_rumah, $result->no_telpon1,
                            $result->keterangan_no_telpon1, "", "",
                        ];
                        $loop = $loop + 1;
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'List Of Siswa',
                        'listSiswa',
                        [
                            "A" => 8, "B"  => 8,
                            "C" => 8, "D"  => 25,
                            "E" => 10, "F" => 8,
                            "G" => 10, "H" => 10,
                            "I" => 30, "J" => 10,
                            "K" => 10, "L" => 10,
                            "M" => 10],
                        $data,
                        'A',
                        'M');

                    $y     = $z + $loop + 1;
                    $sheet = $this->report->getActiveSheet();
                    $this->report->download('xls');
                };
                break;
            case 5:
                // search Siswa detail
                {
                    $this->records = SiswaDetail::Search($this->request);
                    $result        = $this->rsExtJson($this->records,
                        $this->request->limit, $this->request->start);
                };
                break;
            case 9:
                {
                    $result = $this->rsExtJson(Siswa::SearchExt($this->request),
                        $this->request->limit, $this->request->start);
                };
                break;
            case 70:
                // load chart by jenjang
                {
                    $this->records = Siswa::ChartbyJenjang();
                    $result        = $this->rsJson($this->records, 75, 0);
                };
                break;
            case 71:
                // load chart by gender
                {
                    $this->records = Siswa::ChartbyGender();
                    $result        = $this->rsJson($this->records, 75, 0);
                };
                break;
            case 80:
                // list jenjang sekolah
                {
                    $result = $this->rsExtJson(Siswa::ListJenjangSekolah($this->request),
                        $this->request->limit, $this->request->start);
                };
                break;
            case 81:
                // list nama kelas
                {
                    $result = $this->rsExtJson(Siswa::ListNamaKelas($this->request),
                        $this->request->limit, $this->request->start);
                };
                break;
            case 82:
                // list ruang kelas
                {
                    $result = $this->rsExtJson(Siswa::ListRuangKelas($this->request),
                        $this->request->limit, $this->request->start);
                };
                break;
            case 90:
                {
                    switch ($this->request->t) {
                        case 0:
                            // per user
                            { $result = Siswa::RegisterUser($this->request);};
                            break;
                        case 1:
                            // all user
                            { $result = Siswa::RegisterAllUsers($this->request);};
                            break;
                    };
                };
                break;
            case 91:
                { $result = Siswa::ResetPassword($this->request);};
                break;
        };
        return $result;
    }

    /**
     * CRUD Siswa Receivable
     */
    public function crudSiswaReceivable($subtask) {
        switch ($subtask) {
            case 0:
                // search pembayaran data
                {
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->records = SiswaReceivable::headerPrintOut($this->request);
                            $this->crudSiswaReceivable(3);
                        } else {
                            $this->records = SiswaReceivable::Search($this->request);
                            $this->crudSiswaReceivable(4);
                        };
                    } else {
                        $this->records = SiswaReceivable::Search($this->request);
                        $result        = $this->rsJson($this->records,
                            $this->request->limit, $this->request->start);
                    };
                };
                break;
            case 1:
                // save data
                {
                    $session   = $this->request->session();
                    $head_data = json_decode(stripslashes($this->request->head));
                    $json_data = json_decode(stripslashes($this->request->json));
                    $modi_data = json_decode(stripslashes($this->request->modi));
                    $result    = Siswa::Siswasave($head_data,
                        $json_data,
                        $modi_data,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
                    if ($result[0] == false) {$this->server_message = $result[1];};
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2:
                // delete data
                {
                    $result = Siswa::SiswaDelete($this->request);
                    if ($result[0] == false) {$this->server_message = $result[1];};
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3:
                // print pdf
                {
                    //print_r($this->records);
                    $data     = $this->records;
                    $title    = "PROFORMA INVOICE";
                    $filename = "pi_" . $data->pi_no;

                    $this->create_report_pdf($this->request->session());
                    $pdf = $this->report;
                    ########## Document TITLE ##########
                    $pdf->DocTitle($title, $data->pi_no, 30, 160);
                    // $this->report->SetProtection(array('print'), '', null);
                    ########## PAGE TITLE ##########
                    $pdf->SetFont('Helvetica', 'B', 11);
                    $pdf->setRowXY(12, 40);
                    $pdf->Bookmark("Header", 1, -1);
                    //$this->report->MultiCell(30,4,"Master",0,'L',0);
                    $pdf->setRowXY(15, 40);
                    $pdf->SetWidths([40, 60, 30, 60]);
                    $pdf->SetAligns(["L", "L", "L", "L"]);
                    $pdf->SetFont('Times', '', 10);
                    $pdf->addRowHeight(5);
                    $pdf->Row(["PO.No", ": " . $data->Siswa_no,
                        "PO.Date", ": " . $data->Siswa_date], false);
                    $pdf->Row(["PI.No", ": " . $data->pi_no,
                        "PI.Date", ": " . $data->pi_date], false);
                    $pdf->addRowHeight(1);
                    // rectangle boxs
                    $y_current = $pdf->y_pos + 5;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height);
                    $pdf->Rect($pdf->x_pos + (100), $y_current, 85, $y_height);

                    $pdf->addRowHeight(1);
                    $pdf->SetWidths([40, 150]);
                    $pdf->SetAligns(["L", "L"]);
                    $pdf->Row(["Shipper ", ": " . strtoupper($data->company_name)], false);
                    $pdf->Row(["", "  " . strtoupper($data->company_address)], false);
                    // rectangle boxs
                    $y_current = $y_current + $y_height;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 185, $y_height);

                    $pdf->addRowHeight(1);
                    $pdf->Row(["Consignee", ": " . strtoupper($data->buyer_name)], false);
                    $pdf->Row(["", "  " . strtoupper($data->buyer_address)], false);
                    // rectangle boxs
                    $y_current = $y_current + $y_height;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 185, $y_height);

                    $pdf->addRowHeight(5);
                    $pdf->SetWidths([40, 150]);
                    $pdf->Row(["Beneficiary Name", ": " . strtoupper($data->company_name)], false);
                    $pdf->Row(["Beneficiaty Bank", ": " . strtoupper($data->bank_name)], false);
                    $pdf->Row(["Bank Account (USD)", ": " . strtoupper($data->bank_account)], false);
                    $pdf->Row(["Swift Code", ": " . strtoupper($data->swift_code)], false);
                    $pdf->Row(["Bank Address", ": " . strtoupper($data->bank_address)], false);
                    // rectangle boxs
                    $y_current = $y_current + $y_height;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 185, $y_height);

                    $pdf->addRowHeight(5);
                    $pdf->SetWidths([40, 60, 30, 60]);
                    $pdf->Row(["Container / Seal Vessel", ": " . $data->container_type_name,
                        "ETD", ": " . $data->etd], false);
                    $pdf->Row(["Date of Shipment", ": " . $data->shipment_date,
                        "ETA", ": " . $data->eta], false);
                    $pdf->Row(["Port Of Loading ", ": " . $data->port_loading_name,
                        "Gross Weight", ": " . $data->weight_gross], false);
                    $pdf->Row(["Port Of Discharge ", ": " . $data->port_discharge_name,
                        "Nett Weight", ": " . $data->weight_nett], false);
                    $pdf->Row(["", "", "CBM", ": " . $data->cbm], false);
                    // rectangle boxs
                    $y_current = $y_current + $y_height;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height);
                    $pdf->Rect($pdf->x_pos + (100), $y_current, 85, $y_height);
                    /* DETAIL ITEMS */
                    $details = SiswaDetail::detailPrintOut(trim($data->Siswa_no));
                    $pdf->SetFont('Times', 'B', 10);
                    $pdf->addRowHeight(2);
                    $pdf->SetWidths([65, 20, 20, 20, 20, 20, 20]);
                    $pdf->SetAligns(["C", "C", "C", "C", "C", "C", "C"]);
                    $pdf->Row(["COMODITY", "QTY (Fcl)", "QTY (Bags)", "NUT.QTY (Pcs)", "NUT.QTY (Kgs)", "PRICE (USD)(Kgs)", "AMOUNT (USD)"], true);
                    $pdf->SetFont('Times', '', 10);
                    $pdf->SetAligns(["L", "R", "R", "R", "R", "R", "R"]);
                    foreach ($details as $detail) {
                        $pdf->Row([$detail->item_name,
                            $detail->quantity_fcl,
                            $detail->quantity_bags,
                            $detail->quantity_nut_pcs,
                            $detail->quantity_nut_kgs,
                            $detail->unit_price,
                            $detail->amount], true);
                    };
                    $pdf->addRowHeight(5);
                    $pdf->SetWidths([40, 150]);
                    $pdf->SetAligns(["L", "L"]);
                    $loop = 0;
                    foreach (explode(",", $data->payment_term) as $the_term) {
                        if ($loop == 0) {
                            $pdf->Row(["Payment Term", ": " . $the_term], false);
                            $loop = 1;
                        } else { $pdf->Row(["", " " . $the_term], false);}
                    };

                    $pdf->Output($filename . ".pdf", "I");
                    exit;
                };
                break;
            case 4:
                // print xls
                {
                    $results = $this->records;
                    $data    = [];
                    $data[]  = ["NO", "Grade", "Siswa", "Tahun/ Bulan", "Biaya",
                        "Bulan", "Piutang", "Potongan", "Pembayaran"];
                    $loop = 1;
                    $z    = 8;
                    foreach ($results as $result) {
                        $z1     = $z + $loop;
                        $data[] = [$loop,
                            $result->nama_jenjang_siswa, $result->nama_siswa, $result->bultah, $result->nama_biaya_sekolah,
                            $result->bulan, $result->piutang, $result->potongan, $result->pembayaran,
                        ];
                        $loop = $loop + 1;
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'List Of Pembayaran Siswa',
                        'listPembayaranSiswa',
                        [
                            "A" => 3, "B"  => 5,
                            "C" => 25, "D" => 8,
                            "E" => 8, "F"  => 5,
                            "G" => 15, "H" => 15,
                            "I" => 15],
                        $data,
                        'A',
                        'I');

                    $y     = $z + $loop + 1;
                    $sheet = $this->report->getActiveSheet();
                    $this->report->download('xls');
                };
                break;
            case 5:
                // search Siswa detail
                {
                    $this->records = SiswaDetail::Search($this->request);
                    $result        = $this->rsExtJson($this->records,
                        $this->request->limit, $this->request->start);
                };
                break;
            case 9:
                {
                    $result = $this->rsExtJson(Siswa::SearchExt($this->request),
                        $this->request->limit, $this->request->start);
                };
                break;
            case 10:
                // search bank transfer data
                {
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->records = BankTransfer::headerPrintOut($this->request);
                            $this->crudSiswaReceivable(13);
                        } else {
                            $this->records = BankTransfer::Search($this->request);
                            $this->crudSiswaReceivable(14);
                        };
                    } else {
                        $this->records = BankTransfer::Search($this->request);
                        $result        = $this->rsExtJson($this->records,
                            $this->request->limit, $this->request->start);
                    };
                };
                break;
        };
        return $result;
    }

    /**
     * CRUD Syncronize Data
     */
    public function crudSync($subtask) {
        switch ($subtask) {
            case 0:
                // syncronize Siswa data
                {
                    $session = $this->request->session();
                    $result  = Siswa::SyncTable();
                };
                break;
            case 1:
                // syncronize Bank Transfer data
                {
                    $session = $this->request->session();
                    $result  = BankTransfer::SyncTable();
                };
                break;
            case 2:
                // syncronize Siswa Receivable data
                {
                    $session = $this->request->session();
                    $result  = SiswaReceivable::SyncTable();
                };
                break;
        };
        return $result;
    }

    /**
     * CRUD Calon Siswa
     */
    public function crudCalonSiswa($subtask) {
        switch ($subtask) {
            case 0:
                // search data
                {
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->records = CalonSiswa::StudentProfile($this->request->no_pendaftaran);
                            $this->crudCalonSiswa(5);
			} else if ($this->request->pt == 'xls') {
                            $this->records = CalonSiswa::Search($this->request);
                            $this->crudCalonSiswa(6);
                        } else if ($this->request->pt == 'xls1') {
                            $this->records = CalonSiswa::RegistrationReport($this->request);
                            $this->crudCalonSiswa(11);
                        } else if ($this->request->pt == 'xls2') {
                            $this->records = CalonSiswa::SummaryReport($this->request);
                            $this->crudCalonSiswa(12);
                        };
                    } else {
                        $this->records = CalonSiswa::Search($this->request);
                        $result        = $this->rsJson($this->records,
                            $this->request->limit, $this->request->start);
                    };
                };
                break;
            case 1:
                // save registration data
                {
                    $session   = $this->request->session();
                    $head_data = json_decode(stripslashes($this->request->head));
                    $json_data = json_decode(stripslashes($this->request->json));
                    $modi_data = json_decode(stripslashes($this->request->modi));
                    $result    = CalonSiswa::Registrationsave($head_data[0],
                        $json_data,
                        $modi_data,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );

                    if ($result[0] == false) {
                        $result = ["success" => false,
                            "message"            => "Failed to save records.",
                            "server_message"     => $result[1],
                        ];
                    } else {
                        $result = ["success" => true,
                            "message"            => "Succeed",
                            "no_registration"    => $result[1],
                            "nama_lengkap"       => $result[2],
                            "no_pendaftaran"     => $result[3],
                        ];
                    };
                    return json_encode($result);
                };
                break;
            case 3:
                // load Siswa Profile
                {
                    $this->records = CalonSiswa::StudentProfile($this->request->get("nopen"));
                    $result        = $this->rsJson($this->records, 75, 0);
                };
                break;
            case 4:
                // load Orangtua Profile
                {
                    $this->records = CalonSiswa::ParentsProfile($this->request->get("nopen"));
                    $result        = $this->rsJson($this->records, 75, 0);
                };
                break;
            case 5:
		// print pdf
		{
                    // print_r($this->records);exit;
                    $data     = $this->records[0];
                    $title    = "Calon Siswa Photo";
                    $filename = "photo_" . $data->no_registration;

                    $this->create_report_pdf($this->request->session());
                    $pdf = $this->report;
                    ########## Document TITLE ##########
                    $pdf->DocTitle($title, $data->no_registration, 30, 160);
                    // $this->report->SetProtection(array('print'), '', null);
                    ########## PAGE TITLE ##########
                    $pdf->SetFont('Helvetica', 'B', 11);
                    $pdf->setRowXY(12, 40);
                    $pdf->Bookmark("Header", 1, -1);
                    //$this->report->MultiCell(30,4,"Master",0,'L',0);
                    $pdf->setRowXY(15, 40);
                    $pdf->SetWidths([40, 60, 30, 60]);
                    $pdf->SetAligns(["L", "L", "L", "L"]);
                    $pdf->SetFont('Times', '', 10);
                    $pdf->addRowHeight(5);
                    $pdf->Row(["No.Pendaftaran", ": " . $data->no_pendaftaran,
                        "Nama.Siswa", ": " . $data->nama_lengkap], false);
                    $pdf->Row(["No.Registrasi", ": " . $data->no_registration,
                        "Jenjang.Sekolah", ": " . $data->jenjang_sekolah_name], false);
                    $pdf->addRowHeight(1);
                    // rectangle boxs
                    $y_current = $pdf->y_pos + 5;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height);
                    $pdf->Rect($pdf->x_pos + (100), $y_current, 85, $y_height);

                    $pdf->addRowHeight(1);
                    $pdf->SetWidths([40, 150]);
                    $pdf->SetAligns(["L", "L"]);

                    $Files = CalonSiswaFile::Search($data->no_pendaftaran);
                    // print_r($Files);exit;
                    foreach ($Files as $file) {
                        // print_r($file);
                        switch ($file->type) {
                            case 'ktp':
                                { $pdf->Row(["Foto Akte Lahir Siswa ", ": "], false);};
                                break;
                            case 'kk':
                                { $pdf->Row(["Foto Kartu Keluarga ", ": "], false);};
                                break;
                            case 'siswa':
                                {
                                    $pdf->AddPage();
                                    $pdf->setRowXY(15, 40);
                                    $pdf->Row(["Foto Siswa ", ": "], false);};
                                break;
                            case 'bukti':
                                { $pdf->Row(["Foto Bukti Pembayaran Registrasi ", ": "], false);};
                                break;
                        };
                        // $pdf->Row(["X ", ": " . $pdf->x_pos], false);
                        // $pdf->Row(["Y ", ": " . ($pdf->getY())], false);
                        $y_current = $pdf->y_pos + 5;
                        $y_height  = 100;
                        $pdf->Image($file->file_path, $pdf->x_pos, $pdf->getY(), $y_height);
                        // $this->SetXY(32, 10);
                        $pdf->addRowHeight(($y_current + $y_height));
                        $pdf->setRowXY($pdf->x_pos, ($y_current + $y_height));
                    };

                    $pdf->Output($filename . ".pdf", "I");
		    exit;
		};
		break;
            case 6:
                // print xls
                {
                    $results = $this->records;
                    $data    = [];
                    $data[]  = ["No.Pendaftaran","Tgl Daftar", "No.Registrasi",
                     "Jenjang", "Nama", 
                     "Panggilan", "Jenis Kelamin", 
                     "Tempat.Lahir", "Tanggal.Lahir", 
                     "Alamat", "Kecamatan", 
                     "Kota", "agama","warga negara", "keadaan siswa",
                     "tinggi badan","berat badan","golongan darah","penyakit bawaan", "Tinggal Dengan",
                     "anak Ke","dari berapa saudara","Nis Saudara","Kelas Saudara","Nama saudara",
                     "Jenis Bayar", "dibayar oleh","besar pembayara", "dikonfirmasi oleh",
                     "Internal/External","Dari", "Kelas pindahan",
                     "observation date",
                     "observation result",
                    
                    "nama ayah", "alamat ayah", "tempat lahir ayah","tanggal lahir ayah",
                    "telp ayah","hp ayah", "hubungan keluarga","agama ayah", "pendidikan ayah", "pekerjaan ayah",
                    "jenis pekerjaan ayah", "jabatan ayah", "institusi ayah","alamat institusi ayah","telp institusi ayah",
                    "penghasilan ayah","tanggungan ayah",
                    
                    "nama ibu","alamat ibu", "tempat lahir ibu", "tanggal lahir ibu",
                    "telp rumah ibu", "hp ibu", "hubungan ibu","agama ibu", "pendidikan ibu",
                    "pekerjaan ibu","jenis pekerjaan ibu", "jabatan ibu", "institusi ibu","institusi alamat ibu",
                    "institiusi telp ibu","penghasilan  ibu","pengeluaran ibu",

                    "nama wali", "alamat wali","tempat lahir wali","tanggal lahir wali","telpon wali",
                    "hp wali","hubungan wali"," agama wali","pendidikan wali", "pekerjaan wali","jenis pekerjaan wali",
                    "jabatan wali", "institusi wali","alamat institusi wali","telp intitusi wali",
                    "penghasilan wali", "tanggungan wali"
                    ];
                    $loop    = 1;
                    $z       = 18;

                    

                    foreach ($results as $result) { 
                       
                        $z1 = $z + $loop;
                        
                       
                        // print_r($result); exit;
                        $data[] = [
                            $result->no_pendaftaran, $this->TimeStampToString($result->created_date, "d/m/Y H:i"),
                            $result->no_registration,
                            $result->jenjang_sekolah_name, $result->nama_lengkap,
                            $result->nama_panggilan, $result->gender,
                            $result->tempat_lahir, $this->TimeStampToString($result->tanggal_lahir, "d/m/Y"),
                            $result->alamat_rumah, $result->kecamatan, $result->kota,
                            $result->agama, $result->warganegara,
                            $result->keadaan_siswa, 
                            $result->tinggi_badan, $result->berat_badan,
                            $result->bloodtype, $result->penyakit_bawaan,
                            $result->tinggaldengan, $result->anakke,
                            $result->brpsaudara, $result->saudara_nis,
                            $result->saudara_kelas, $result->saudara_nama,
                            $result->jenis_bayar,
                            $result->payment_by, $result->payment_amount,
                            $result->payment_confirm_by,
                            $result->sekolah_asal_internal, $result->sekolah_asal_nama,
                            $result->kelas_pindahan,
                            $result->observation_date, $result->observation_result, 
         
                            $result->nama_ayah, $result->alamat_ayah,
                            $result->tempat_lahir_ayah, $result->tanggal_lahir_ayah,
                            $result->telp_rumah_ayah, $result->telp_hp_ayah, 
                            $result->hubungan_ayah, $result->agama_ayah,
                            $result->pendidikan_ayah, $result->pekerjaan_ayah,
                            $result->jenis_pekerjaan_ayah, $result->jabatan_ayah,
                            $result->institusi_ayah, $result->institusi_alamat_ayah,
                            $result->institusi_telp_ayah,
                            $result->penghasilan_ayah, $result->tanggungan_ayah,

                            $result->nama_ibu, $result->alamat_ibu,
                            $result->tempat_lahir_ibu, $result->tanggal_lahir_ibu,
                            $result->telp_rumah_ibu, $result->telp_hp_ibu, 
                            $result->hubungan_ibu, $result->agama_ibu,
                            $result->pendidikan_ibu, $result->pekerjaan_ibu,
                            $result->jenis_pekerjaan_ibu, $result->jabatan_ibu,
                            $result->institusi_ibu, $result->institusi_alamat_ibu,
                            $result->institusi_telp_ibu,
                            $result->penghasilan_ibu, $result->pengeluaran_ibu,
                            
                            $result->nama_wali_ayah, $result->alamat_wali_ayah,
                            $result->tempat_lahir_wali_ayah, $result->tanggal_lahir_wali_ayah,
                            $result->telp_rumah_wali_ayah, $result->telp_hp_wali_ayah, 
                            $result->hubungan_wali_ayah, $result->agama_wali_ayah,
                            $result->pendidikan_wali_ayah, $result->pekerjaan_wali_ayah,
                            $result->jenis_pekerjaan_wali_ayah, $result->jabatan_wali_ayah,
                            $result->institusi_wali_ayah, $result->institusi_alamat_wali_ayah,
                            $result->institusi_telp_wali_ayah,
                            $result->penghasilan_wali_ayah, $result->tanggungan_wali_ayah,
                            
                        ];
                        $loop = $loop + 1;
                    };

                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'List Of Calon Siswa',
                        'listCalonSiswa',
                        [
                            "A" => 8, "B"  => 8,
                            "C" => 8, "D"  => 25,
                            "E" => 20, "F" => 8,
                            "G" => 20, "H" => 20,
                            "I" => 30, "J" => 20,
                           
                            "K" => 20, "L" => 20,
                            "M" => 20, "N" => 20,
                            "O" => 20, "P" => 20,
                            "Q" => 20, "R" => 20,
                            "S" => 8, "T"  => 20,

                            "U" => 20, "V"  => 25,
                            "W" => 20, "X" => 8,
                            "Y" => 20, "Z" => 20,
                            "AA" => 30, "AB" => 20,
                            "AC" => 20, "AD" => 20,

                            "AE" => 20, "AF" => 20,
                            "AG" => 20, "AH" => 20,
                            "AI" => 20, "AJ" => 20,
                            "AK" => 20, "AL" => 20,

                            "AM" => 20, "AN" => 20,
                            "AO" => 20, "AP" => 20,
                            "AQ" => 20, "AR" => 20,
                            "AS" => 20, "AT" => 20,

                            "AU" => 20, "AV" => 20,
                            "AW" => 20, "AX" => 20,
                            "AY" => 20, "AZ" => 20,
                            "BA" => 20, "BB" => 20,

                            "BC" => 20, "BD" => 20,
                            "BE" => 20, "BF" => 20,
                            "BG" => 20, "BH" => 20,
                            "BI" => 20, "BJ" => 20,

                            "BK" => 20, "BL" => 20,
                            "BM" => 20, "BN" => 20,
                            "BO" => 20, "BP" => 20,
                            "BQ" => 20, "BR" => 20,

                            "BS" => 20, "BT" => 20,
                            "BU" => 20, "BV" => 20,
                            "BW" => 20, "BX" => 20,
                            "BY" => 20, "BZ" => 20,

                            "CA" => 20, "CB" => 20,
                            "CC" => 20, "CD" => 20,
                            "CE" => 20, "CF" => 20,
                            "CG" => 20, "CH" => 20,

                        ],
                        $data,
                        'A',
                        'CH');

                    $y     = $z + $loop + 1;
                    $sheet = $this->report->getActiveSheet();
                    $this->report->download('xls');
                };
                break;
            case 7:
                // save observation & commitment
                {
                    $session   = $this->request->session();
                    $json_data = json_decode(stripslashes($this->request->json));
                    $result    = CalonSiswa::Observationsave(
                        $json_data,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );

                    if ($result[0] == false) {
                        $result = ["success" => false,
                            "message"            => "Failed to save records.",
                            "server_message"     => $result[1],
                        ];
                    } else {
                        $result = ["success" => true,
                            "message"            => "Succeed",
                            // "no_registration" => $result[1],
                            // "nama_lengkap" => $result[2],
                            // "no_pendaftaran" => $result[3]
                        ];
                    };
                    return json_encode($result);
                };
                break;
            case 10:
                // load chart by jenjang
                {
                    $this->records = CalonSiswa::ChartbyJenjang();
                    $result        = $this->rsJson($this->records, 75, 0);
                };
                break;
	    case 11:
                // print xls list laporan registrasi
                {
                    $results   = $this->records;
                    $data      = [];
                    $data[]    = ["No", "No.Registrasi", "Nama", "Gender", "Tanggal.Daftar", "Tunai", "Non.Tunai", "Tgl.Observasi", "Tgl.Komitment", "Hasil.Observasi", "Internal", "Eksternal", "Nama.Sekolah", "Mutasi"];
                    $loop      = 1;
                    $z         = 8;
                    $tunai     = 0;
                    $non_tunai = 0;
                    foreach ($results as $result) {
                        $z1        = $z + $loop;
                        $tunai     = $tunai + $result->tunai_amount;
                        $non_tunai = $non_tunai + $result->non_tunai_amount;
                        // print_r($result);
                        $data[] = [" " . $z1,
                            $result->no_registration,
                            $result->nama_lengkap,
                            $result->gender_name,
                            $result->created_date,
                            "",
                            "",
                            $result->observation_date,
                            $result->commitment_date,
                            $result->observation_result,
                            $result->sekolah_asal_internal,
                            $result->sekolah_asal_external,
                            $result->sekolah_asal_nama,
                            $result->kelas_pindahan,
                        ];
                        $loop = $loop + 1;
                    };
                    // echo "<BR> Tunai = " . $tunai;
                    // echo "<BR> Non Tunai = " . $non_tunai;exit;
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Laporan Harian',
                        'dailyReport',
                        [
                            "A" => 8, "B"  => 10,
                            "C" => 30, "D" => 10,
                            "E" => 10, "F" => 10,
                            "G" => 10, "H" => 10,
                            "I" => 10, "J" => 10,
                            "K" => 10, "L" => 10,
                            "M" => 30, "N" => 10,
                        ],
                        $data,
                        'A',
                        'N');

                    $y     = $z + $loop + 1;
                    $sheet = $this->report->getActiveSheet();
                    $loop  = 1;
                    $z     = 8;
                    foreach ($results as $result) {
                        $z1 = $z + $loop;
                        $sheet->setCellValue('F' . $z1, "" . $result->tunai_amount);
                        $sheet->setCellValue('G' . $z1, "" . $result->non_tunai_amount);
                        $loop = $loop + 1;
                    };

                    $sheet->setCellValue('F' . ($z - 1), 'PEMBAYARAN DAFTAR ')
                        ->mergeCells('F' . ($z - 1) . ':G' . ($z - 1))
                        ->setCellValue('K' . ($z - 1), 'SEKOLAH ASAL ')
                        ->mergeCells('K' . ($z - 1) . ':M' . ($z - 1))
                        ->getStyle('F' . ($z - 1) . ':K' . ($z - 1))->applyFromArray(['font' => ['size' => 9]]);

                    $sheet->setCellValue('D' . $y, 'TOTAL BAYAR PENDAFTARAN ')
                        ->setCellValue('D' . ($y + 1), '1. TUNAI ')
                        ->setCellValue('D' . ($y + 2), '2. NON-TUNAI')
                        ->setCellValue('E' . ($y + 1), '=SUM(F' . ($z + 1) . ':F' . ($y - 1) . ')')
                        ->setCellValue('E' . ($y + 2), '=SUM(G' . ($z + 1) . ':G' . ($y - 1) . ')')
                        ->setCellValue('E' . ($y + 3), '------------------------')
                        ->setCellValue('D' . ($y + 4), 'TOTAL')
                        ->setCellValue('E' . ($y + 4), '=SUM(E' . ($y + 1) . ':E' . ($y + 2) . ')')
                        ->setCellValue('C' . ($y + 6), '------------------------')
                        ->setCellValue('C' . ($y + 7), 'ADMIN PPDB')
                        ->setCellValue('G' . ($y + 6), '------------------------')
                        ->setCellValue('G' . ($y + 7), 'PANITIA PPDB')
                        ->getStyle('D' . $y . ':E' . ($y + 4))->applyFromArray(['font' => ['size' => 9]]);

                    $this->report->download('xls');
                };
                break;
            case 12:
                // print xls list laporan rekapitulasi
                {
                    $results   = $this->records;
                    $data      = [];
                    $data[]    = ["No", "Jenjang Sekolah", "Laki Laki", "Perempuan", "Total", "Internal", "External", "Total", "Bergabung", "Tidak Bergabung", "Pindahan/Mutasi", "Total", "Daftar Ulang", "Belum Daftar Ulang"];
                    $loop      = 1;
                    $z         = 8;
                    $tunai     = 0;
                    $non_tunai = 0;
                    foreach ($results as $result) {
                        $z1 = $z + $loop;
                        // $tunai     = $tunai + $result->tunai_amount;
                        // $non_tunai = $non_tunai + $result->non_tunai_amount;
                        // print_r($result);exit;
                        $data[] = [" " . $z1,
                            $result->jenjang_sekolah_name,
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                        ];
                        $loop = $loop + 1;
                    };
                    // echo "<BR> Tunai = " . $tunai;
                    // echo "<BR> Non Tunai = " . $non_tunai;exit;
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Laporan Rekapitulasi',
                        'summaryReport',
                        [
                            "A" => 8, "B"  => 30,
                            "C" => 10, "D" => 10,
                            "E" => 10, "F" => 10,
                            "G" => 10, "H" => 10,
                            "I" => 10, "J" => 10,
                            "K" => 10, "L" => 10,
                            "M" => 10, "N" => 10,
                        ],
                        $data,
                        'A',
                        'N');

                    $y     = $z + $loop;
                    $sheet = $this->report->getActiveSheet();
                    $loop  = 1;
                    $z     = 8;
                    foreach ($results as $result) {
                        $z1 = $z + $loop;
                        $sheet->setCellValue('C' . $z1, "" . $result->total_lakilaki);
                        $sheet->setCellValue('D' . $z1, "" . $result->total_perempuan);
                        $sheet->setCellValue('E' . $z1, '=SUM(C' . $z1 . ':D' . $z1 . ')');
                        $sheet->setCellValue('F' . $z1, "" . $result->total_internal);
                        $sheet->setCellValue('G' . $z1, "" . $result->total_external);
                        $sheet->setCellValue('H' . $z1, '=SUM(F' . $z1 . ':G' . $z1 . ')');
                        $sheet->setCellValue('I' . $z1, "" . $result->total_bergabung);
                        $sheet->setCellValue('J' . $z1, "" . $result->total_tidak_bergabung);
                        $sheet->setCellValue('K' . $z1, "0");
                        $sheet->setCellValue('L' . $z1, '=sum(I' . $z1 . ':K' . $z1 . ')');
                        $sheet->setCellValue('M' . $z1, "" . $result->total_daftar);
                        $sheet->setCellValue('N' . $z1, "" . $result->belum_daftar);
                        $loop = $loop + 1;
                    };

                    $sheet->setCellValue('C' . ($z - 1), 'GENDER ')
                        ->mergeCells('C' . ($z - 1) . ':E' . ($z - 1))
                        ->setCellValue('F' . ($z - 1), 'ASAL SEKOLAH')
                        ->mergeCells('F' . ($z - 1) . ':H' . ($z - 1))
                        ->setCellValue('I' . ($z - 1), 'TOTAL PENDAFTARAN')
                        ->mergeCells('I' . ($z - 1) . ':L' . ($z - 1))
                        ->getStyle('C' . ($z - 1) . ':K' . ($z - 1))->applyFromArray(['font' => ['size' => 9]]);

                    $sheet->setCellValue('B' . $y, 'TOTAL ')
                        ->setCellValue('C' . $y, '=SUM(C' . ($z + 1) . ':C' . ($y - 1) . ')')
                        ->setCellValue('D' . $y, '=SUM(D' . ($z + 1) . ':D' . ($y - 1) . ')')
                        ->setCellValue('E' . $y, '=SUM(E' . ($z + 1) . ':E' . ($y - 1) . ')')
                        ->setCellValue('F' . $y, '=SUM(F' . ($z + 1) . ':F' . ($y - 1) . ')')
                        ->setCellValue('G' . $y, '=SUM(G' . ($z + 1) . ':G' . ($y - 1) . ')')
                        ->setCellValue('H' . $y, '=SUM(H' . ($z + 1) . ':H' . ($y - 1) . ')')
                        ->setCellValue('I' . $y, '=SUM(I' . ($z + 1) . ':I' . ($y - 1) . ')')
                        ->setCellValue('J' . $y, '=SUM(J' . ($z + 1) . ':J' . ($y - 1) . ')')
                        ->setCellValue('K' . $y, '=SUM(K' . ($z + 1) . ':K' . ($y - 1) . ')')
                        ->setCellValue('L' . $y, '=SUM(L' . ($z + 1) . ':L' . ($y - 1) . ')')
                        ->setCellValue('M' . $y, '=SUM(M' . ($z + 1) . ':M' . ($y - 1) . ')')
                        ->setCellValue('N' . $y, '=SUM(N' . ($z + 1) . ':N' . ($y - 1) . ')')
                        ->getStyle('B' . $y . ':N' . ($y + 1))->applyFromArray(['font' => ['size' => 9]]);

                    $sheet->getStyle("C" . ($z - 1) . ':' . 'N' . $y)
                        ->applyFromArray([
                            'font'    => ['size' => 10],
                            'borders' => [
                                'allborders' => [
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                ]]]);

                    $this->report->download('xls');
                };
                break;
            case 100:
                // print xls observation
                {
                    $results =  CalonSiswa::Search($this->request);
                    $data    = [];
                    $data[]  = [
                    "No.Pendaftaran",
                    "No.Registrasi",
                    "Jenjang", 
                    "Nama", 
                    "jenis kelamin",
                    "observation date",
                    "observation result",
                    "commitment_date"];
                    $loop    = 1;
                    $z       = 8;

                    foreach ($results as $result) { 
                        $z1 = $z + $loop;
                        // print_r($result); exit;
                        $data[] = [
                            $result->no_pendaftaran,
                            $result->no_registration,
                            $result->jenjang_sekolah_name, 
                            $result->nama_lengkap,
                            $result->gender,
                            $result->observation_date, 
                            $result->observation_result,
                            $result->commitment_date
                        ];
                        $loop = $loop + 1;
                    };

                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'List Of Calon Siswa Observation & commitment',
                        'listCalonSiswa',
                        [
                            "A" => 8, "B"  => 8,
                            "C" => 8, "D"  => 50,
                            "E" => 10, "F" => 20,
                            "G" => 10, "H" => 10,
                            ], 
                        $data,
                        'A',
                        'H');

                    $y     = $z + $loop + 1;
                    $sheet = $this->report->getActiveSheet();
                    $this->report->download('xls');
                };
                break;
                case 705:
                    // save observation & commitment
                    {
                        $session   = $this->request->session();
                        $json_data = json_decode(stripslashes($this->request->json));
                        $result    = CalonSiswa::Statussave(
                            $json_data,
                            $session->get("site"),
                            $session->get("user_id"),
                            $session->get("company_id")
                        );
    
                        if ($result[0] == false) {
                            $result = ["success" => false,
                                "message"            => "Failed to save records.",
                                "server_message"     => $result[1],
                            ];
                        } else {
                            $result = ["success" => true,
                                "message"            => "Succeed",
                                // "no_registration" => $result[1],
                                // "nama_lengkap" => $result[2],
                                // "no_pendaftaran" => $result[3]
                            ];
                        };
                        return json_encode($result);
                    };
                    break;
    

                // case 705: // set status calon siswa
                //     { //dd($this->request->no_pendaftaran);
                //             $session = $this->request->session();
                //             // $json_data = json_decode(stripslashes($this->request->status));
                //             $result = calonsiswa::setStatusCalonsiswa(
                //             $this->request->pendaftaran_no,
                //             $this->request->status,
                //             $session->get("site"),
                //             $session->get("user_id"),
                //             $session->get("company_id")
                //         );
        
                //         if ($result[0] == false) {
                //             $this->server_message = $result[1];
                //         };
                //         $result = $this->jsonSuccess($result[0]);
                //     };
                // break;
        };
        return $result;
    }
}
