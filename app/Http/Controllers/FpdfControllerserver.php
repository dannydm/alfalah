<?php

namespace App\Http\Controllers;

use Codedge\Fpdf\Fpdf\Fpdf;

class FpdfController extends Fpdf
{   //define private property;
    private $company_arr,
            $user_arr,
            $doc_no,
            $outlines = array(),
            $OutlineRoot,
            $widths,
            $aligns,
            $borders, 
            $border_widths, 
            $border_fonts,
            $encrypted,          //whether document is protected
            $Uvalue,             //U entry in pdf document
            $Ovalue,             //O entry in pdf document
            $Pvalue,             //P entry in pdf document
            $enc_obj_id,         //encryption object id
            $last_rc4_key,       //last RC4 key encrypted (cached for optimisation)
            $last_rc4_key_c,         //last RC4 computed key
            $row_height,
            $print_header = true,
            $print_footer = true;          // Cell's Row Height
    //define public property;
    public  $x_pos = 0,
            $y_pos = 0,
            $height = 0;
    /****************
    ** SET COMPANY **
    ****************/
    public function setCompany($company)
    {   $this->company_arr = $company;
        $this->SetAuthor('Danny');
        $this->SetCreator('Matulessy');
        $this->encrypted=false;
        $this->last_rc4_key='';
        $this->padding="\x28\xBF\x4E\x5E\x4E\x75\x8A\x41\x64\x00\x4E\x56\xFF\xFA\x01\x08".
            "\x2E\x2E\x00\xB6\xD0\x68\x3E\x80\x2F\x0C\xA9\xFE\x64\x53\x69\x7A";
    }
    // Set Current Cell's Row Height
    public function SetRowHeight($value) 
    {
        $this->row_height = $value;     
    }
    /****************
    ** SET USERS **
    ****************/
    public function set_User($the_preparator, $the_authenticator, $the_publisher)
    {
        $this->user_arr = array($the_preparator, $the_authenticator, $the_publisher);
    }
    /****************
    ** SET DOCUMENT NUMBER **
    ****************/
    public function setDocNumber($doc_no)
    {
        $this->doc_no=$doc_no;
    }
    /*******************
    ** SET PAGE HEADER **
    ********************/
    function Header()
    {
        if ($this->print_header) 
        {   //Logo x=12, y=12
            // $this->Image($this->company_arr["logo"],12,10,18);
            $this->SetXY(32,10);
            $this->SetFont('Arial','B',10);
            $this->Cell(0,8,'YAYASAN MASJID DARUSSALAM TROPODO',0,0,'L');
            $this->SetXY(32,16);
            $this->Cell(0,0,'',1,0,'L');
            $this->SetXY(32,16);
            $this->SetFont('Arial','',7);
            $this->Cell(0,4, 'Jl. Nusa Indah D-1 Wisma Tropodo, Waru, Sidoarjo (031) 8672828, 8664323',0,0,'L');
            $this->SetXY(32,20);
            $this->SetFont('Arial','',7);
            $this->MultiCell(0,3,'Di Sidoarjo',0,'L',0);
            $this->SetXY(32,23);
            $this->SetFont('Arial','B',8);
            // $this->MultiCell(0,3, $this->company_arr["doc_no"] ,0,'R',0);
            $this->Ln(8);
        }
    }
    /********************
    ** SET PAGE FOOTER **
    ********************/
    function Footer()
    {   global $my;

        if ($this->print_footer) 
        {   //Position at 1.5 cm from bottom
            $this->SetY(-31);
            $this->SetY(-30);
            $this->SetXY(10, -18);
            $this->SetFont('Arial','B',7);
            $message = "This is computer generated documents, ".
                " prepared by ".$this->user_arr[0].
                " approved by ".$this->user_arr[1].
                " printed by ".$this->user_arr[2].
                " printed date ".date("d M Y");
            $page_width = $this->w-$this->lMargin-$this->rMargin;                  
            $ln = $this->NbLines($page_width, $message);
            $this->SetXY(10, -18);
            $this->MultiCell($page_width, 3,$message,0,'R',0);
            $this->Cell(0,0,'',1,0,'L');  ## Garis footer bawah
            $this->SetY(-18 + 3*$ln + 1);
            $this->SetFont('Arial','I',8);
            //Page number
            $this->Cell(0,3,'Page '.$this->PageNo().'/{nb}',0,0,'C');
        }
    }
    /**********************
    ** SET DOCUMENT TITLE **
    **********************/
    public function DocTitle($title, $doc_no, $xy='', $multi='')
    {   $this->doc_no=$doc_no;
        if ( $xy == '' ) {  $this->SetXY(50, 30);   }
        else { $this->SetXY($xy, 30);   }

        $this->SetFont('Arial','B',16);

        if ( $multi == '' ) {   $this->MultiCell(80, 5, $title, 0, 'C', 0); }
        else {  $this->MultiCell($multi, 5, $title, 0, 'C', 0); };

        $this->Bookmark($title);
        $this->SetTitle($title);
        $this->SetSubject($title." ".$doc_no);
        //Line break
        $this->Ln(4);
    }
    /**********************
    ** SET XY Axis of Row **
    **********************/
    public function setRowXY($x, $y)
    {   //Set line height and XY positions
        $this->height = $y;
        $this->y_pos  = $y;
        $this->x_pos  = $x;
        $this->SetXY($x,$y);
    }
    /**********************
    ** SET Row Height **
    **********************/
    public function addRowHeight($h)
    {   //Set line height and Y positions
        $this->height += $h;
        $this->SetXY($this->x_pos, $this->height);
    }
    /**********************
    ** SET Column Widths **
    **********************/
    function SetWidths($w)
    {   //Set the array of column widths
        $this->widths = $w;
    }
    /**********************
    ** SET Column Aligment **
    **********************/
    function SetAligns($a) 
    {   //Set the array of column alignments
        $this->aligns = $a;
    }
    /********************************
    ** SET Borders of each Columns **
    *********************************/
    function SetBorders($a) 
    {   /*  This parameter is an array of string. 
            The following characters are recognized by the function     
            
            Char                    Description
            A                           All border will be drawn equals to LRTB
            L                           Left border will be drawn
            R                           Right border will be drawn
            T                           Top border will be drawn
            B                           Bottom border will be drawn
            
            Example:
            if table contains 3 columns :
            $pdf = new PDF_SAA();
            $pdf->SetBorder(array("A", "LR", "TB"));
            column 1 : A
            column 2 : LR
            column 3 : TB
        */

        //Sey the array of column borders
        $this->borders = $a;
    }
    /**********************
    ** SET Border Fonts **
    **********************/
    function SetBorderFonts($v) 
    {   $this->border_fonts = $v;
    }
    /************
    ** SET Row **
    ************/
    function Row($data, $border=false)
    {   //Calculate the height of the row
        $nb =  0 ; 
        for($i=0; $i<count($data); $i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));

        if ($this->row_height) 
        {   $xHeight = $this->row_height;   } 
        else {  $xHeight = 5;   };

        $h = $xHeight * $nb; # tinggi cell nya

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for($i=0; $i<count($data); $i++) 
        {   $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetXY($x,$y);

            //Draw the border
            if ($border !== false)
            { $this->Rect($x,$y,$w,$h); };

            //Print the text
            $this->MultiCell($w, $xHeight, $data[$i],0,$a);

            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
        $this->height += $h;
        $this->SetX($this->x_pos);
    }
    /************
    ** SET Row **
    ************/
    /*
        ## Write row with optional border ##
        
        To use this function you must set the column borders by using setBorders() function.
        Please check setBorders() function for detail.
                  
        Example :
        
        $pdf = new PDF_SAA;
        
        // Set column border
            // First column --> show All borders
            // Second column --> show Left border
            // Third column --> show Left, Right, and Top border
            
        $pdf->SetBorders(array("A", "L", "LRT")); 
                
        $arr_data = array();
        $pdf->WriteBorder(array("A1", "B1", "C1")); // Write the row with border
    */
    function WriteBorder($data)
    {   //Calculate the height of the row
        $nb = 0;
        for($i=0; $i<count($data); $i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));

        if ($this->row_height) 
        {   $xHeight = $this->row_height;   } 
        else {  $xHeight = 5;   }

        $h = $xHeight * $nb; # tinggi cell nya

        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        // $curr_font = $this->F;
    
        //Draw the cells of the row
        for($i=0; $i<count($data); $i++) 
        {   $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetXY($x,$y);
            //Draw the border
            $b = $this->borders[$i];
            // Get Border Width
            $bw = $this->border_widths[$i];
            if ($b) 
            {   if (trim(strtoupper($b))=="A") 
                {   $this->Rect($x,$y,$w,$h);   } 
                else 
                {   // Check Left Border
                    $wL = stripos($b, "L");
                    $wR = stripos($b, "R");
                    $wT = stripos($b, "T");
                    $wB = stripos($b, "B");
                    if ($wL!==false) 
                    {   $this->SetLineWidth($this->defaultBorderWidth($bw[$wL]));
                        $this->Rect($x,$y,0,$h);        
                    };
                    if ($wR!==false) 
                    {   $this->SetLineWidth($this->defaultBorderWidth($bw[$wR]));
                        $this->Rect($x+$w,$y,0,$h);         
                    };
                    if ($wT!==false) 
                    {   $this->SetLineWidth($this->defaultBorderWidth($bw[$wT]));
                        $this->Rect($x,$y,$w,0);        
                    };
                    if ($wB!==false) 
                    {   $this->SetLineWidth($this->defaultBorderWidth($bw[$wB]));
                        $this->Rect($x,$y+$h,$w,0);         
                    };
                };
            };      

            if ($this->border_fonts[$i]) 
            {   
                $this->SetFont($this->border_fonts[$i][0], $this->border_fonts[$i][1], $this->border_fonts[$i][2]); 
            };

            //Print the text
            $this->MultiCell($w, $xHeight,$data[$i],0,$a);

            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        };
        //Go to the next line
        $this->Ln($h);
        $this->height += $h;
        $this->SetX($this->x_pos);
    }
    /*******************
    ** SET Page Break **
    *******************/
    function CheckPageBreak($h)
    {   //If the height h would cause an overflow, add a new page immediately
        if($this->GetY() + $h > $this->PageBreakTrigger)
        {   $this->AddPage($this->CurOrientation);
            $this->setRowXY($this->GetX(), $this->GetY());
        };
    }
    /************************
    ** SET New Break Lines **
    *************************/
    function NbLines($w,$txt)
    {   //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w == 0)
            $w = $this->w - $this->rMargin - $this->x;

        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s    =str_replace("\r",'',$txt);
        $nb   =strlen($s);

        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;

        $sep = -1;
        $i   = 0;
        $j   = 0;
        $l   = 0;
        $nl  = 1;

        while($i<$nb) 
        {   $c=$s[$i];
            if($c=="\n") 
            {   $i++;
                $sep =-1;
                $j   =$i;
                $l   =0;
                $nl++;
                continue;
            };

            if($c==' ')
                $sep=$i;

            $l+=$cw[$c];
            if($l>$wmax) 
            {   if($sep==-1) 
                {   if($i==$j)
                    $i++;
                }
                else
                    $i = $sep+1;
                
                $sep =-1;
                $j   =$i;
                $l   =0;
                $nl++;
            }
            else $i++;
        };
        return $nl;
    }
    ## AKHIR PENGATURAN POSISI LAYOUT SECARA OTOMATIS ##
    /******************
    ** SET BOOKMARK  **
    ******************/
    public function Bookmark($txt,$level=0,$y=0)
    {   if ($y==-1) { $y=$this->GetY(); };
        $this->outlines[]=array('t'=>$txt,'l'=>$level,'y'=>$y,'p'=>$this->PageNo());
    }
    /******************
    ** PUT BOOKMARK  **
    ******************/
    private function _putbookmarks()
    {   $nb=count($this->outlines);
        if($nb==0) { return; };
        $lru   = array();
        $level = 0;
        foreach($this->outlines as $i=>$o)
        {   if($o['l']>0)
            {   $parent=$lru[$o['l']-1];
                //Set parent and last pointers
                $this->outlines[$i]['parent']=$parent;
                $this->outlines[$parent]['last']=$i;
                if($o['l']>$level)
                {   //Level increasing: set first pointer
                    $this->outlines[$parent]['first']=$i;
                };
            }
            else { $this->outlines[$i]['parent']=$nb; };
  
            if($o['l']<=$level and $i>0)
            {   //Set prev and next pointers
                $prev=$lru[$o['l']];
                $this->outlines[$prev]['next']=$i;
                $this->outlines[$i]['prev']=$prev;
            };
            $lru[$o['l']]=$i;
            $level=$o['l'];
        };
        //Outline items
        $n=$this->n+1;
        foreach($this->outlines as $i=>$o)
        {   $this->_newobj();
            $this->_out('<</Title '.$this->_textstring($o['t']));
            $this->_out('/Parent '.($n+$o['parent']).' 0 R');
            if(isset($o['prev']))
                $this->_out('/Prev '.($n+$o['prev']).' 0 R');
            if(isset($o['next']))
                $this->_out('/Next '.($n+$o['next']).' 0 R');
            if(isset($o['first']))
                $this->_out('/First '.($n+$o['first']).' 0 R');
            if(isset($o['last']))
                $this->_out('/Last '.($n+$o['last']).' 0 R');
            $this->_out(sprintf('/Dest [%d 0 R /XYZ 0 %.2f null]',1+2*$o['p'],($this->h-$o['y'])*$this->k));
            $this->_out('/Count 0>>');
            $this->_out('endobj');
        };
        //Outline root
        $this->_newobj();
        $this->OutlineRoot=$this->n;
        $this->_out('<</Type /Outlines /First '.$n.' 0 R');
        $this->_out('/Last '.($n+$lru[0]).' 0 R>>');
        $this->_out('endobj');
    }
    /******************
    ** PUT RESOURCES **
    ******************/
    public function _putresources()
    {   parent::_putresources();
        //$this->_putresources();
        $this->_putbookmarks();
        if ($this->encrypted)
        {   $this->_newobj();
            $this->enc_obj_id = $this->n;
            $this->_out('<<');
            $this->_putencryption();
            $this->_out('>>');
            $this->_out('endobj');
        };
    }
    /******************
    ** PUT CATALOG   **
    ******************/
    public function _putcatalog()
    {   parent::_putcatalog();
        //$this->_putcatalog();
        if(count($this->outlines)>0)
        {   $this->_out('/Outlines '.$this->OutlineRoot.' 0 R');
            $this->_out('/PageMode /UseOutlines');
        };
    }
    /***********************************************************************
    **                SET PROTECTION                                      **
    ** copy: copy text and images to the clipboard                        **
    ** print: print the document                                          **
    ** modify: modify it (except for annotations and forms)               **
    ** annot-forms: add annotations and forms                             **
    ***********************************************************************/
    public function SetProtection($permissions=array(),$user_pass='',$owner_pass=null)
    {   $options = array('print' => 4, 'modify' => 8, 'copy' => 16, 'annot-forms' => 32 );
        $protection = 192;
        foreach($permissions as $permission)
        {   if (!isset($options[$permission]))
                $this->Error('Incorrect permission: '.$permission);
            $protection += $options[$permission];
        };
        if ($owner_pass === null) $owner_pass = uniqid(rand());
        $this->encrypted = true;
        $this->_generateencryptionkey($user_pass, $owner_pass, $protection);
    }
    public function _putstream($s)
    {   
        if ($this->encrypted)
            { $s = $this->_RC4($this->_objectkey($this->n), $s); }
        parent::_putstream($s);
    }
    public function _textstring($s)
    {
        if ($this->encrypted)
            { $s = $this->_RC4($this->_objectkey($this->n), $s); }
        return parent::_textstring($s);
    }



    /**
    * Compute key depending on object number where the encrypted data is stored
    */
    private function _objectkey($n)
    {
        return substr($this->_md5_16($this->encryption_key.pack('VXxx',$n)),0,10);
    }
    /**
    * Escape special characters
    */
    public function _escape($s)
    {   $s=str_replace('\\','\\\\',$s);
        $s=str_replace(')','\\)',$s);
        $s=str_replace('(','\\(',$s);
        $s=str_replace("\r",'\\r',$s);
        return $s;
    }
    private function _putencryption()
    {
        $this->_out('/Filter /Standard');
        $this->_out('/V 1');
        $this->_out('/R 2');
        $this->_out('/O ('.$this->_escape($this->Ovalue).')');
        $this->_out('/U ('.$this->_escape($this->Uvalue).')');
        $this->_out('/P '.$this->Pvalue);
    }
    public function _puttrailer()
    {
        parent::_puttrailer();
        if ($this->encrypted)
        {
            $this->_out('/Encrypt '.$this->enc_obj_id.' 0 R');
            $this->_out('/ID [()()]');
        };
    }
    /**
    * RC4 is the standard encryption algorithm used in PDF format
    */
    private function _RC4($key, $text)
    {   if ($this->last_rc4_key != $key)
        {   $k = str_repeat($key, 256/strlen($key)+1);
            $rc4 = range(0,255);
            $j = 0;
            for ($i=0; $i<256; $i++)
            {   $t       = $rc4[$i];
                $j       = ($j + $t + ord($k{$i})) % 256;
                $rc4[$i] = $rc4[$j];
                $rc4[$j] = $t;
            };
            $this->last_rc4_key = $key;
            $this->last_rc4_key_c = $rc4;
        } else { $rc4 = $this->last_rc4_key_c; };

        $len = strlen($text);
        $a   = 0;
        $b   = 0;
        $out = '';
        for ($i=0; $i<$len; $i++)
        {   
            $a       = ($a+1)%256;
            $t       = $rc4[$a];
            $b       = ($b+$t)%256;
            $rc4[$a] = $rc4[$b];
            $rc4[$b] = $t;
            $k       = $rc4[($rc4[$a]+$rc4[$b])%256];
            $out     .=chr(ord($text{$i}) ^ $k);
        };
        return $out;
    }
    /**
    * Get MD5 as binary string
    */
    private function _md5_16($string)
    {   
        return pack('H*',md5($string));
    }
    /**
    * Compute O value
    */
    private function _Ovalue($user_pass, $owner_pass)
    {
        $tmp = $this->_md5_16($owner_pass);
        $owner_RC4_key = substr($tmp,0,5);
        return $this->_RC4($owner_RC4_key, $user_pass);
    }
    /**
    * Compute U value
    */
    private function _Uvalue()
    {
        return $this->_RC4($this->encryption_key, $this->padding);
    }
    /**
    * Compute encryption key
    */
    private function _generateencryptionkey($user_pass, $owner_pass, $protection)
    {
        // Pad passwords
        $user_pass = substr($user_pass.$this->padding,0,32);
        $owner_pass = substr($owner_pass.$this->padding,0,32);
        // Compute O value
        $this->Ovalue = $this->_Ovalue($user_pass,$owner_pass);
        // Compute encyption key
        $tmp = $this->_md5_16($user_pass.$this->Ovalue.chr($protection)."\xFF\xFF\xFF");
        $this->encryption_key = substr($tmp,0,5);
        // Compute U value
        $this->Uvalue = $this->_Uvalue();
        // Compute P value
        $this->Pvalue = -(($protection^255)+1);
    }

    // New Function
    function defaultBorderWidth($v) 
    {   return $v?$v:0.2;   }

    public function SetPrintHeader($value) 
    {   $this->print_header = $value;   }

    public function SetPrintFooter($value) 
    {   $this->print_footer = $value;   }

    public function SetBorderWidths($a) 
    {   $this->border_widths = $a;  }

    public function ClearBorderWidhts() 
    {   $this->border_widths = null;    }

    public function ClearBorderFonts() 
    {   $this->border_fonts = null; }

// End of Class
}