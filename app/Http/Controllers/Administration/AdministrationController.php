<?php

namespace App\Http\Controllers\Administration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Administration\Item;
use DB;

class AdministrationController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {   $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {   switch ($task)
        {   // routing data
            case 0 : return $this->routing($substask);       break;
            // ITD register tab
            case 1 : return $this->crudItems($substask);      break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {   switch ($subtask)
        {   case 0 : $result = view('administration/items')->with("TABID", $this->request->get("tabId")); break;
        };
        return $result;
    }
    /**
     * CRUD Items
     */
    public function crudItems($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = Item::Search($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudDownTime(3); }
                    else {  $this->crudDownTime(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = Item::itemSave(   $json_data, 
                                            $session->get("site"),
                                            $session->get("user_id")
                                        );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Item::itemDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("NO", "STATUS", "TAG", "ASSET.ID", "ASSET.NAME", "USER", 
                            "OS.TYPE", "OS.NAME", "OS.KEY", "OS.KEY.TYPE","OS.LIC.TYPE",
                            "IS_LINUX", "IS_WINDOWS", "IS_OEM", "IS_OLP", "No_License",
                            "8.1.Pro", "7.Home", "7.Pro", "7.Utm", "XP.Pro", "XP.Home",
                            "2000.Pro", "Svr.2000", "Svr.2003", "Svr.2008", "SITE",
                            "OEM", "OLP", "OEM", "OLP",
                            "OEM", "OLP", "OEM", "OLP",
                            "OEM", "OLP", "OEM", "OLP", "Processor");
                $loop = 1;
                $z = 8;
                foreach($results as $result) {
                        $z1 = $z + $loop;
                        $data[] = array( $loop, 
                            $result->status, $result->tag, $result->name, $result->asset_id,
                            $result->userid, $result->os_type, $result->os_name, 
                            $result->os_key, $result->os_key_type, $result->os_license_type,
                            '=if(G'.$z1.'="LINUX",1,0)',
                            '=if(G'.$z1.'="WINDOWS",1,0)',
                            '=if(J'.$z1.'="OEM",1,0)',
                            '=if(J'.$z1.'="OLP",1,0)',
                            '=if(G'.$z1.'="WINDOWS",(IF(K'.$z1.'="NA",1,0)),0)',
                            '=if(H'.$z1.'="Microsoft Windows 8.1 Pro",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 7 Home Premium",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 7 Professional",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 7 Ultimate",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows XP Professional",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows XP Home Edition",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 2000 Professional",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 2000 Server",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows Server 2003 R2 Standard Edition",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows Server 2008 R2 Enterprise",1,0)',
                            $result->site_id,
                            '=IF(AA'.$z1.'="JKT",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OEM",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="JKT",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OLP",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="JKT",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OEM",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="JKT",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OLP",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="SBY",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OEM",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="SBY",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OLP",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="SBY",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OEM",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="SBY",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OLP",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="PRB",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OEM",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="PRB",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OLP",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="PRB",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OEM",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="PRB",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OLP",1,0)),0)),0)',
                            $result->processort
                    );
                    $loop = $loop + 1;
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'ITD Asset License',
                    'ITDAssetLicense',
                    array( 
                        "A" => 3,                            "B" => 8,
                        "C" => 20,                           "D" => 15,
                        "E" => 15,                           "F" => 15,
                        "G" => 10,                           "H" => 30,
                        "I" => 30,                           "J" => 10,
                        "K" => 10,                           "L" => 10,
                        "M" => 10,                           "N" => 10,
                        "O" => 10,                           "P" => 10,
                        "Q" => 10,                           "R" => 10,
                        "S" => 10,                           "T" => 10,
                        "U" => 10,                           "V" => 10,
                        "W" => 10,                           "X" => 10,
                        "Y" => 10,                           "Z" => 10,
                        "AA" => 10,                          "AB" => 10,
                        "AC" => 10,                          "AD" => 10,
                        "AE" => 10,                          "AF" => 10,
                        "AG" => 10,                          "AH" => 10,
                        "AI" => 10,                          "AJ" => 10,
                        "AK" => 10,                          "AL" => 10,
                        "AM" => 10,                          "AN" => 25 ),
                    $data,
                    'A',
                    'AN');

                $y = $z + $loop +1;
                $sheet = $this->report->getActiveSheet();
                $sheet->setCellValue('I'.$y, 'TOTAL')
                    ->setCellValue('L'.$y, '=SUM(L'.($z+1).':L'.($y-1).')')
                    ->setCellValue('M'.$y, '=SUM(M'.($z+1).':M'.($y-1).')')
                    ->setCellValue('N'.$y, '=SUM(N'.($z+1).':N'.($y-1).')')
                    ->setCellValue('O'.$y, '=SUM(O'.($z+1).':O'.($y-1).')')
                    ->setCellValue('P'.$y, '=SUM(P'.($z+1).':P'.($y-1).')')
                    ->setCellValue('Q'.$y, '=SUM(Q'.($z+1).':Q'.($y-1).')')
                    ->setCellValue('R'.$y, '=SUM(R'.($z+1).':R'.($y-1).')')
                    ->setCellValue('S'.$y, '=SUM(S'.($z+1).':S'.($y-1).')')
                    ->setCellValue('T'.$y, '=SUM(T'.($z+1).':T'.($y-1).')')
                    ->setCellValue('U'.$y, '=SUM(U'.($z+1).':U'.($y-1).')')
                    ->setCellValue('V'.$y, '=SUM(V'.($z+1).':V'.($y-1).')')
                    ->setCellValue('W'.$y, '=SUM(W'.($z+1).':W'.($y-1).')')
                    ->setCellValue('X'.$y, '=SUM(X'.($z+1).':X'.($y-1).')')
                    ->setCellValue('Y'.$y, '=SUM(Y'.($z+1).':Y'.($y-1).')')
                    ->setCellValue('Z'.$y, '=SUM(Z'.($z+1).':Z'.($y-1).')')
                    ->setCellValue('AB'.$y, '=SUM(AB'.($z+1).':AB'.($y-1).')')
                    ->setCellValue('AC'.$y, '=SUM(AC'.($z+1).':AC'.($y-1).')')
                    ->setCellValue('AD'.$y, '=SUM(AD'.($z+1).':AD'.($y-1).')')
                    ->setCellValue('AE'.$y, '=SUM(AE'.($z+1).':AE'.($y-1).')')
                    ->setCellValue('AF'.$y, '=SUM(AF'.($z+1).':AF'.($y-1).')')
                    ->setCellValue('AG'.$y, '=SUM(AG'.($z+1).':AG'.($y-1).')')
                    ->setCellValue('AH'.$y, '=SUM(AH'.($z+1).':AH'.($y-1).')')
                    ->setCellValue('AI'.$y, '=SUM(AI'.($z+1).':AI'.($y-1).')')
                    ->setCellValue('AJ'.$y, '=SUM(AJ'.($z+1).':AJ'.($y-1).')')
                    ->setCellValue('AK'.$y, '=SUM(AK'.($z+1).':AK'.($y-1).')')
                    ->setCellValue('AL'.$y, '=SUM(AL'.($z+1).':AL'.($y-1).')')
                    ->setCellValue('AM'.$y, '=SUM(AM'.($z+1).':AM'.($y-1).')')
                    ->setCellValue('C'.($y+2), 'Total Computer')
                        ->setCellValue('D'.($y+2), $loop-1)
                    ->setCellValue('C'.($y+3), 'Total Linux')
                        ->setCellValue('D'.($y+3), '=L'.$y)
                    ->setCellValue('C'.($y+4), 'NO.OS')
                        ->setCellValue('D'.($y+4), '=D'.($y+2).'-D'.($y+3).'-D'.($y+5))
                        ->setCellValue('F'.($y+4), 'CHART')
                    ->setCellValue('C'.($y+5), 'Total Windows')
                        ->setCellValue('D'.($y+5), '=M'.$y)
                        ->setCellValue('E'.($y+5), 'Unversioned')
                        ->setCellValue('F'.($y+5), '=D'.($y+5).'- SUM(Q'.$y.':Z'.$y.')')
                    ->setCellValue('E'.($y+6), 'Total OEM')
                        ->setCellValue('F'.($y+6), '=N'.$y)
                    ->setCellValue('E'.($y+7), 'Total OLP')
                        ->setCellValue('F'.($y+7), '=O'.$y)
                    ->setCellValue('E'.($y+8), 'No.License')
                        ->setCellValue('F'.($y+8), '=P'.$y)
                    ->setCellValue('E'.($y+9), 'Yet.License')
                        ->setCellValue('F'.($y+9), '=D'.($y+5).'-F'.($y+6).'-F'.($y+7).'-F'.($y+8));

                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( Item::itemSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    
    /**
     * CRUD Asset ITD Hardware
     */
    public function crudITDHardware($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = AssetITDHardware::hardwareSearch($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudITDHardware(3); }
                    else {  $this->crudITDHardware(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = AssetITDHardware::hardwareSave(
                                                $json_data, 
                                                $this->request->btn,
                                                $session->get("user_id")
                                            );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = AssetITDHardware::hardwareDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("No", "Status","Asset.ID", "TAG", "NAME", "OCS.ID", 
                            "Class", "OS", "Processor", "RAM", "IP.Address", 
                            "MAC.Address", "Remark", "Site", "Dept", "Building",
                            "Win.Svr", "Win.Client", "VMs", "Parent.Asset",
                            "Reported", "CREATED");
                $loop = 1;
                foreach($results as $result) {
                        $data[] = array( $loop,
                            $result->status, 
                            $result->asset_id, 
                            $result->tag, 
                            $result->name, 
                            $result->id, 
                            $result->comp_type, 
                            $result->osname,
                            $result->processort,
                            $result->memory, 
                            $result->ipaddr, 
                            $result->ori_macaddr, 
                            $result->remark, 
                            $result->site_id, 
                            $result->dept_name, 
                            $result->building_name, 
                            $result->is_server,
                            $result->is_client,
                            $result->is_vm,   
                            $result->parent_asset_id,
                            $result->is_asset_reported,  
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                    $loop = $loop + 1;
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'ITD Asset Hardware',
                    'ITDAssetHardware',
                    array( "A" => 5, "B" => 10, "C" => 15, "D" => 20, "E" => 25, 
                          "F" => 10, "G" => 10, "H" => 30, "I" => 30, "J" => 10, 
                          "K" => 15, "L" => 30, "M" => 15, "N" => 15, "O" => 20, 
                          "P" => 15, "Q" => 15, "R" => 15, "S" => 15, "T" => 15, 
                          "U" => 15, "V" => 15 ),
                    $data,
                    'A',
                    'V');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( AssetITDHardware::hardwareSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * CRUD Asset ITD Software
     */
    public function crudITDSoftware($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( AssetITDHardware::softwareSearch($this->request), 
                            $this->request->limit, $this->request->start);
            break;
        };
        return $result;
    }
    /**
     * CRUD Asset ITD Asset Software
     */
    public function crudITDAssetSoftware($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( AssetITDHardware::assetSoftwareSearch($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {  };
            break;
            case 2 : // delete data
            {  };
            break;
            case 9 : // searchExt
                $result = $this->rsExtJson( AssetITDHardware::assetSoftwareSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            break;
        };
        return $result;
    }
    /**
     * CRUD Asset ITD License
     */
    public function crudITDLicense($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = AssetITDLicense::licenseSearch($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudITDLicense(3); }
                    else {  $this->crudITDLicense(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = AssetITDLicense::licenseSave(
                                                $json_data, 
                                                $session->get("user_id")
                                            );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = AssetITDLicense::licenseDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("NO", "STATUS", "TAG", "ASSET.ID", "ASSET.NAME", "USER", 
                            "OS.TYPE", "OS.NAME", "OS.KEY", "OS.KEY.TYPE","OS.LIC.TYPE",
                            "IS_LINUX", "IS_WINDOWS", "IS_OEM", "IS_OLP", "No_License",
                            "8.1.Pro", "7.Home", "7.Pro", "7.Utm", "XP.Pro", "XP.Home",
                            "2000.Pro", "Svr.2000", "Svr.2003", "Svr.2008", "SITE",
                            "OEM", "OLP", "OEM", "OLP",
                            "OEM", "OLP", "OEM", "OLP",
                            "OEM", "OLP", "OEM", "OLP", "Processor");
                $loop = 1;
                $z = 8;
                foreach($results as $result) {
                        $z1 = $z + $loop;
                        $data[] = array( $loop, 
                            $result->status, $result->tag, $result->name, $result->asset_id,
                            $result->userid, $result->os_type, $result->os_name, 
                            $result->os_key, $result->os_key_type, $result->os_license_type,
                            '=if(G'.$z1.'="LINUX",1,0)',
                            '=if(G'.$z1.'="WINDOWS",1,0)',
                            '=if(J'.$z1.'="OEM",1,0)',
                            '=if(J'.$z1.'="OLP",1,0)',
                            '=if(G'.$z1.'="WINDOWS",(IF(K'.$z1.'="NA",1,0)),0)',
                            '=if(H'.$z1.'="Microsoft Windows 8.1 Pro",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 7 Home Premium",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 7 Professional",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 7 Ultimate",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows XP Professional",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows XP Home Edition",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 2000 Professional",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 2000 Server",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows Server 2003 R2 Standard Edition",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows Server 2008 R2 Enterprise",1,0)',
                            $result->site_id,
                            '=IF(AA'.$z1.'="JKT",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OEM",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="JKT",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OLP",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="JKT",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OEM",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="JKT",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OLP",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="SBY",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OEM",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="SBY",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OLP",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="SBY",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OEM",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="SBY",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OLP",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="PRB",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OEM",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="PRB",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OLP",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="PRB",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OEM",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="PRB",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OLP",1,0)),0)),0)',
                            $result->processort
                    );
                    $loop = $loop + 1;
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'ITD Asset License',
                    'ITDAssetLicense',
                    array( 
                        "A" => 3,                            "B" => 8,
                        "C" => 20,                           "D" => 15,
                        "E" => 15,                           "F" => 15,
                        "G" => 10,                           "H" => 30,
                        "I" => 30,                           "J" => 10,
                        "K" => 10,                           "L" => 10,
                        "M" => 10,                           "N" => 10,
                        "O" => 10,                           "P" => 10,
                        "Q" => 10,                           "R" => 10,
                        "S" => 10,                           "T" => 10,
                        "U" => 10,                           "V" => 10,
                        "W" => 10,                           "X" => 10,
                        "Y" => 10,                           "Z" => 10,
                        "AA" => 10,                          "AB" => 10,
                        "AC" => 10,                          "AD" => 10,
                        "AE" => 10,                          "AF" => 10,
                        "AG" => 10,                          "AH" => 10,
                        "AI" => 10,                          "AJ" => 10,
                        "AK" => 10,                          "AL" => 10,
                        "AM" => 10,                          "AN" => 25 ),
                    $data,
                    'A',
                    'AN');

                $y = $z + $loop +1;
                $sheet = $this->report->getActiveSheet();
                $sheet->setCellValue('I'.$y, 'TOTAL')
                    ->setCellValue('L'.$y, '=SUM(L'.($z+1).':L'.($y-1).')')
                    ->setCellValue('M'.$y, '=SUM(M'.($z+1).':M'.($y-1).')')
                    ->setCellValue('N'.$y, '=SUM(N'.($z+1).':N'.($y-1).')')
                    ->setCellValue('O'.$y, '=SUM(O'.($z+1).':O'.($y-1).')')
                    ->setCellValue('P'.$y, '=SUM(P'.($z+1).':P'.($y-1).')')
                    ->setCellValue('Q'.$y, '=SUM(Q'.($z+1).':Q'.($y-1).')')
                    ->setCellValue('R'.$y, '=SUM(R'.($z+1).':R'.($y-1).')')
                    ->setCellValue('S'.$y, '=SUM(S'.($z+1).':S'.($y-1).')')
                    ->setCellValue('T'.$y, '=SUM(T'.($z+1).':T'.($y-1).')')
                    ->setCellValue('U'.$y, '=SUM(U'.($z+1).':U'.($y-1).')')
                    ->setCellValue('V'.$y, '=SUM(V'.($z+1).':V'.($y-1).')')
                    ->setCellValue('W'.$y, '=SUM(W'.($z+1).':W'.($y-1).')')
                    ->setCellValue('X'.$y, '=SUM(X'.($z+1).':X'.($y-1).')')
                    ->setCellValue('Y'.$y, '=SUM(Y'.($z+1).':Y'.($y-1).')')
                    ->setCellValue('Z'.$y, '=SUM(Z'.($z+1).':Z'.($y-1).')')
                    ->setCellValue('AB'.$y, '=SUM(AB'.($z+1).':AB'.($y-1).')')
                    ->setCellValue('AC'.$y, '=SUM(AC'.($z+1).':AC'.($y-1).')')
                    ->setCellValue('AD'.$y, '=SUM(AD'.($z+1).':AD'.($y-1).')')
                    ->setCellValue('AE'.$y, '=SUM(AE'.($z+1).':AE'.($y-1).')')
                    ->setCellValue('AF'.$y, '=SUM(AF'.($z+1).':AF'.($y-1).')')
                    ->setCellValue('AG'.$y, '=SUM(AG'.($z+1).':AG'.($y-1).')')
                    ->setCellValue('AH'.$y, '=SUM(AH'.($z+1).':AH'.($y-1).')')
                    ->setCellValue('AI'.$y, '=SUM(AI'.($z+1).':AI'.($y-1).')')
                    ->setCellValue('AJ'.$y, '=SUM(AJ'.($z+1).':AJ'.($y-1).')')
                    ->setCellValue('AK'.$y, '=SUM(AK'.($z+1).':AK'.($y-1).')')
                    ->setCellValue('AL'.$y, '=SUM(AL'.($z+1).':AL'.($y-1).')')
                    ->setCellValue('AM'.$y, '=SUM(AM'.($z+1).':AM'.($y-1).')')
                    ->setCellValue('C'.($y+2), 'Total Computer')
                        ->setCellValue('D'.($y+2), $loop-1)
                    ->setCellValue('C'.($y+3), 'Total Linux')
                        ->setCellValue('D'.($y+3), '=L'.$y)
                    ->setCellValue('C'.($y+4), 'NO.OS')
                        ->setCellValue('D'.($y+4), '=D'.($y+2).'-D'.($y+3).'-D'.($y+5))
                        ->setCellValue('F'.($y+4), 'CHART')
                    ->setCellValue('C'.($y+5), 'Total Windows')
                        ->setCellValue('D'.($y+5), '=M'.$y)
                        ->setCellValue('E'.($y+5), 'Unversioned')
                        ->setCellValue('F'.($y+5), '=D'.($y+5).'- SUM(Q'.$y.':Z'.$y.')')
                    ->setCellValue('E'.($y+6), 'Total OEM')
                        ->setCellValue('F'.($y+6), '=N'.$y)
                    ->setCellValue('E'.($y+7), 'Total OLP')
                        ->setCellValue('F'.($y+7), '=O'.$y)
                    ->setCellValue('E'.($y+8), 'No.License')
                        ->setCellValue('F'.($y+8), '=P'.$y)
                    ->setCellValue('E'.($y+9), 'Yet.License')
                        ->setCellValue('F'.($y+9), '=D'.($y+5).'-F'.($y+6).'-F'.($y+7).'-F'.($y+8));

                $this->report->download('xls');
            };
            break;
        };
        return $result;
    }
    /**
     * CRUD Asset ITD Non Computer
     */
    public function crudITDNonComputer($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = AssetITDNonComputer::Search($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudITDNonComputer(3); }
                    else {  $this->crudITDNonComputer(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = AssetITDNonComputer::noncomputerSave(
                                                $json_data, 
                                                $this->request->btn,
                                                $session->get("user_id")
                                            );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = AssetITDNonComputer::noncomputerDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("No", "Status","Asset.ID", "NAME", "Class", 
                            "Remark", "Site", "Dept", "Building", "CREATED");
                $loop = 1;
                foreach($results as $result) {
                        $data[] = array( $loop,
                            $result->status, 
                            $result->asset_id, 
                            $result->name, 
                            $result->comp_type, 
                            $result->remark, 
                            $result->site_id, 
                            $result->dept_name, 
                            $result->building_name, 
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                    $loop = $loop + 1;
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'ITD Asset Non Computer',
                    'ITDAssetNonComputer',
                    array( "A" => 5, "B" => 10, "C" => 15, "D" => 30, "E" => 10, 
                          "F" => 30, "G" => 5, "H" => 10, "I" => 10, "J" => 10 ),
                    $data,
                    'A',
                    'J');
                $this->report->download('xls');
            };
            break;
        };
        return $result;
    }
    /**
     * CRUD Asset Master DocCode
     */
    public function crudDoccode($subtask)
    {   
        switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( AssetDocCode::docCodeSearch($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $json_data = json_decode(stripslashes($this->request->json));
                $result = AssetDocCode::docCodeSave($json_data);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = AssetDocCode::docCodeDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
        };
        return $result;
    }
    /**
     * CRUD Asset Master Machine Type
     */
    public function crudMCType($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( MachineType::machinetypeSearch($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = MachineType::machinetypeSave($json_data, $session->get("site"));
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = MachineType::machinetypeDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 9 : // searchExt
                $result = $this->rsExtJson( MachineType::machinetypeSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            break;
        };
        return $result;
    }
    /**
     * CRUD Asset Master Machine Brand
     */
    public function crudMCBrand($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( MachineBrand::machinebrandSearch($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = MachineBrand::machinebrandSave($json_data, $session->get("site"));
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = MachineBrand::machinebrandDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 9 : // searchExt
                $result = $this->rsExtJson( MachineBrand::machinebrandSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            break;
        };
        return $result;
    }
    /**
     * CRUD Asset Machine Register
     */
    public function crudMCRegister($subtask)
    {   
        switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( Machine::registerSearch($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $json_data = json_decode(stripslashes($this->request->json));
                $result = Machine::machinetypeSave($json_data);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Machine::machinetypeDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
        };
        return $result;
    }
    /**
     * CRUD Asset Machine Hardware
     */
    public function crudMCHardware($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
                $result = $this->rsExtJson( Machine::machineSearch($this->request), 
                            $this->request->limit, $this->request->start);
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = Machine::machineSave($json_data);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Machine::machineDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( Machine::machineSearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
     * CRUD Asset ITD License
     */
    public function crudDownTime($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = AssetDownTime::Search($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudDownTime(3); }
                    else {  $this->crudDownTime(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $result = AssetDownTime::downtimeSave(
                                                $json_data, 
                                                $session->get("site"),
                                                $session->get("user_id")
                                            );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = AssetDownTime::licenseDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("NO", "STATUS", "TAG", "ASSET.ID", "ASSET.NAME", "USER", 
                            "OS.TYPE", "OS.NAME", "OS.KEY", "OS.KEY.TYPE","OS.LIC.TYPE",
                            "IS_LINUX", "IS_WINDOWS", "IS_OEM", "IS_OLP", "No_License",
                            "8.1.Pro", "7.Home", "7.Pro", "7.Utm", "XP.Pro", "XP.Home",
                            "2000.Pro", "Svr.2000", "Svr.2003", "Svr.2008", "SITE",
                            "OEM", "OLP", "OEM", "OLP",
                            "OEM", "OLP", "OEM", "OLP",
                            "OEM", "OLP", "OEM", "OLP", "Processor");
                $loop = 1;
                $z = 8;
                foreach($results as $result) {
                        $z1 = $z + $loop;
                        $data[] = array( $loop, 
                            $result->status, $result->tag, $result->name, $result->asset_id,
                            $result->userid, $result->os_type, $result->os_name, 
                            $result->os_key, $result->os_key_type, $result->os_license_type,
                            '=if(G'.$z1.'="LINUX",1,0)',
                            '=if(G'.$z1.'="WINDOWS",1,0)',
                            '=if(J'.$z1.'="OEM",1,0)',
                            '=if(J'.$z1.'="OLP",1,0)',
                            '=if(G'.$z1.'="WINDOWS",(IF(K'.$z1.'="NA",1,0)),0)',
                            '=if(H'.$z1.'="Microsoft Windows 8.1 Pro",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 7 Home Premium",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 7 Professional",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 7 Ultimate",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows XP Professional",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows XP Home Edition",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 2000 Professional",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows 2000 Server",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows Server 2003 R2 Standard Edition",1,0)',
                            '=if(H'.$z1.'="Microsoft Windows Server 2008 R2 Enterprise",1,0)',
                            $result->site_id,
                            '=IF(AA'.$z1.'="JKT",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OEM",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="JKT",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OLP",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="JKT",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OEM",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="JKT",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OLP",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="SBY",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OEM",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="SBY",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OLP",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="SBY",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OEM",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="SBY",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OLP",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="PRB",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OEM",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="PRB",(IF(J'.$z1.'="NA",0,(IF(K'.$z1.'="OLP",(IF(J'.$z1.'=K'.$z1.',1,0)),0)))),0)',
                            '=IF(AA'.$z1.'="PRB",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OEM",1,0)),0)),0)',
                            '=IF(AA'.$z1.'="PRB",(IF(J'.$z1.'="NA",(IF(K'.$z1.'="OLP",1,0)),0)),0)',
                            $result->processort
                    );
                    $loop = $loop + 1;
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'ITD Asset License',
                    'ITDAssetLicense',
                    array( 
                        "A" => 3,                            "B" => 8,
                        "C" => 20,                           "D" => 15,
                        "E" => 15,                           "F" => 15,
                        "G" => 10,                           "H" => 30,
                        "I" => 30,                           "J" => 10,
                        "K" => 10,                           "L" => 10,
                        "M" => 10,                           "N" => 10,
                        "O" => 10,                           "P" => 10,
                        "Q" => 10,                           "R" => 10,
                        "S" => 10,                           "T" => 10,
                        "U" => 10,                           "V" => 10,
                        "W" => 10,                           "X" => 10,
                        "Y" => 10,                           "Z" => 10,
                        "AA" => 10,                          "AB" => 10,
                        "AC" => 10,                          "AD" => 10,
                        "AE" => 10,                          "AF" => 10,
                        "AG" => 10,                          "AH" => 10,
                        "AI" => 10,                          "AJ" => 10,
                        "AK" => 10,                          "AL" => 10,
                        "AM" => 10,                          "AN" => 25 ),
                    $data,
                    'A',
                    'AN');

                $y = $z + $loop +1;
                $sheet = $this->report->getActiveSheet();
                $sheet->setCellValue('I'.$y, 'TOTAL')
                    ->setCellValue('L'.$y, '=SUM(L'.($z+1).':L'.($y-1).')')
                    ->setCellValue('M'.$y, '=SUM(M'.($z+1).':M'.($y-1).')')
                    ->setCellValue('N'.$y, '=SUM(N'.($z+1).':N'.($y-1).')')
                    ->setCellValue('O'.$y, '=SUM(O'.($z+1).':O'.($y-1).')')
                    ->setCellValue('P'.$y, '=SUM(P'.($z+1).':P'.($y-1).')')
                    ->setCellValue('Q'.$y, '=SUM(Q'.($z+1).':Q'.($y-1).')')
                    ->setCellValue('R'.$y, '=SUM(R'.($z+1).':R'.($y-1).')')
                    ->setCellValue('S'.$y, '=SUM(S'.($z+1).':S'.($y-1).')')
                    ->setCellValue('T'.$y, '=SUM(T'.($z+1).':T'.($y-1).')')
                    ->setCellValue('U'.$y, '=SUM(U'.($z+1).':U'.($y-1).')')
                    ->setCellValue('V'.$y, '=SUM(V'.($z+1).':V'.($y-1).')')
                    ->setCellValue('W'.$y, '=SUM(W'.($z+1).':W'.($y-1).')')
                    ->setCellValue('X'.$y, '=SUM(X'.($z+1).':X'.($y-1).')')
                    ->setCellValue('Y'.$y, '=SUM(Y'.($z+1).':Y'.($y-1).')')
                    ->setCellValue('Z'.$y, '=SUM(Z'.($z+1).':Z'.($y-1).')')
                    ->setCellValue('AB'.$y, '=SUM(AB'.($z+1).':AB'.($y-1).')')
                    ->setCellValue('AC'.$y, '=SUM(AC'.($z+1).':AC'.($y-1).')')
                    ->setCellValue('AD'.$y, '=SUM(AD'.($z+1).':AD'.($y-1).')')
                    ->setCellValue('AE'.$y, '=SUM(AE'.($z+1).':AE'.($y-1).')')
                    ->setCellValue('AF'.$y, '=SUM(AF'.($z+1).':AF'.($y-1).')')
                    ->setCellValue('AG'.$y, '=SUM(AG'.($z+1).':AG'.($y-1).')')
                    ->setCellValue('AH'.$y, '=SUM(AH'.($z+1).':AH'.($y-1).')')
                    ->setCellValue('AI'.$y, '=SUM(AI'.($z+1).':AI'.($y-1).')')
                    ->setCellValue('AJ'.$y, '=SUM(AJ'.($z+1).':AJ'.($y-1).')')
                    ->setCellValue('AK'.$y, '=SUM(AK'.($z+1).':AK'.($y-1).')')
                    ->setCellValue('AL'.$y, '=SUM(AL'.($z+1).':AL'.($y-1).')')
                    ->setCellValue('AM'.$y, '=SUM(AM'.($z+1).':AM'.($y-1).')')
                    ->setCellValue('C'.($y+2), 'Total Computer')
                        ->setCellValue('D'.($y+2), $loop-1)
                    ->setCellValue('C'.($y+3), 'Total Linux')
                        ->setCellValue('D'.($y+3), '=L'.$y)
                    ->setCellValue('C'.($y+4), 'NO.OS')
                        ->setCellValue('D'.($y+4), '=D'.($y+2).'-D'.($y+3).'-D'.($y+5))
                        ->setCellValue('F'.($y+4), 'CHART')
                    ->setCellValue('C'.($y+5), 'Total Windows')
                        ->setCellValue('D'.($y+5), '=M'.$y)
                        ->setCellValue('E'.($y+5), 'Unversioned')
                        ->setCellValue('F'.($y+5), '=D'.($y+5).'- SUM(Q'.$y.':Z'.$y.')')
                    ->setCellValue('E'.($y+6), 'Total OEM')
                        ->setCellValue('F'.($y+6), '=N'.$y)
                    ->setCellValue('E'.($y+7), 'Total OLP')
                        ->setCellValue('F'.($y+7), '=O'.$y)
                    ->setCellValue('E'.($y+8), 'No.License')
                        ->setCellValue('F'.($y+8), '=P'.$y)
                    ->setCellValue('E'.($y+9), 'Yet.License')
                        ->setCellValue('F'.($y+9), '=D'.($y+5).'-F'.($y+6).'-F'.($y+7).'-F'.($y+8));

                $this->report->download('xls');
            };
            break;
        };
        return $result;
    }

}
