<?php

namespace App\Http\Controllers\Ppdb;

use App\Http\Controllers\Controller;
use App\Models\Administrator\Department;
use App\Models\Administrator\Parameter;
use App\Models\Ppdb\CalonSiswa;
use App\Models\Ppdb\CalonSiswaFile;
use App\Models\Ppdb\TahunAjaran;
use Illuminate\Http\Request;

class PpdbController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        //$this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask) {
        switch ($task) {
            // registering data
            case 0:
                {
                    return $this->registering($substask);
                };
                break;
            case 1:
                {
                    return $this->completing($substask);
                };
                break;
        };
    }
    
    public function upload(request $request) {
        $session = $this->request->session();

        $the_filename_1 = null;
        $the_filename_2 = null;
        $the_filename_3 = null;
        $the_filename_4 = null;
        $the_file_1     = null;
        $the_file_2     = null;
        $the_file_3     = null;
        $the_file_4     = null;
        $result         = false;

        if ($request->no_registration) {
            $Calon = CalonSiswa::where('no_registration', '=', $request->no_registration)->first();
            // get photo ktp
            $file = $request->file('photo_ktp');
            if ($file) {
                // $the_file_1 = $this->TimeStampToString(date('Ymd'), 'Ymd') . ;
                $the_filename_1 = $Calon->id . $this->TimeStampToString(date('Ymd'), 'Ymd') . '_ktp' . $file->getClientOriginalName();
                $the_file_1     = $file->move(storage_path("app/ppdb_upload"), $the_filename_1);
                $result         = true;
            };
            // get photo bukti kk
            $file = $request->file('photo_kk');
            if ($file) {
                $the_filename_2 = $Calon->id . $this->TimeStampToString(date('Ymd'), 'Ymd') . '_kk' . $file->getClientOriginalName();
                $the_file_2     = $file->move(storage_path("app/ppdb_upload"), $the_filename_2);
                $result         = true;
            };
            // get photo siswa
            $file = $request->file('photo_siswa');
            if ($file) {
                $the_filename_3 = $Calon->id . $this->TimeStampToString(date('Ymd'), 'Ymd') . '_siswa' . $file->getClientOriginalName();
                $the_file_3     = $file->move(storage_path("app/ppdb_upload"), $the_filename_3);
                $result         = true;
            };
            // get photo bukti bayar
            $file = $request->file('photo_bayar');
            if ($file) {
                $the_filename_4 = $Calon->id . $this->TimeStampToString(date('Ymd'), 'Ymd') . '_bayar' . $file->getClientOriginalName();
                $the_file_4     = $file->move(storage_path("app/ppdb_upload"), $the_filename_4);
                $result         = true;
            };

            if ($result) {
                $result = CalonSiswa::FileSave(
                    $request->no_registration,
                    $the_filename_1,
                    $the_filename_2,
                    $the_filename_3,
                    $the_filename_4,
                    $session->get("user_id")
                );

                if ($result[0] == false) {
                    $result = ["success" => false,
                        "message"            => "Failed to save records.",
                        "server_message"     => $result[1],
                    ];
                } else {
                    $result = ["success" => true,
                        "message"            => "Succeed",
                        "server_message"     => $result[1],
                    ];
                };
            };
        };
        return json_encode($result);
    }
    /**
     * First registering data
     */
    public function registering($subtask) {
        switch ($subtask) {
            case 0:
                // ppdb form page
                {
                    $session     = $this->request->session();
                    $tahunajaran = TahunAjaran::getPeriode($this->request);
                    $prasyarat   = Parameter::getPPDB($this->request);

                    $result = view('ppdb/ppdb' . config('app.system_skin'))
                        ->with("TABID", $this->request->get("tabId"))
                        ->with("TAHUNAJARAN", $tahunajaran->name)
                        ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id)
                        ->with("PRASYARAT", $prasyarat->param_big_value);
                };
                break;
            case 1:
                // save ppdb form page
                {
                    $session   = $this->request->session();
                    $head_data = json_decode(stripslashes($this->request->head));
                    $json_data = json_decode(stripslashes($this->request->json));
                    $modi_data = json_decode(stripslashes($this->request->modi));
                    $result    = CalonSiswa::CalonSave($head_data,
                        $json_data,
                        $modi_data,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id"),
                        "registering"
                    );
                    if ($result[0] == false) {
                        $result = ["success" => false,
                            "message"            => "Failed to save records.",
                            "server_message"     => $result[1],
                        ];
                    } else {
                        $result = ["success" => true,
                            "message"            => "Succeed",
                            "server_message"     => $result[1],
                        ];
                    };
                    return json_encode($result);
                };
                break;
            case 2:
                // acceptance
                {
                    //echo "HUAHAHAHAHA";
                    $session     = $this->request->session();
                    $tahunajaran = TahunAjaran::getPeriode($this->request);
                    $calonsiswa  = CalonSiswa::StudentProfile($this->request->get("id"));
                    //echo "<BR>HUAHAHAHAHA"; exit;
                    if ($calonsiswa) {
                        $result = view('ppdb/ppdbAccept' . config('app.system_skin'))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id)
                            ->with("PPDB_ID", $this->request->get("id"))
                            ->with("NAMA_LENGKAP", $calonsiswa[0]->nama_lengkap);
                    };
                    // else redirect to home page;
                };
                break;
            case 3:
                // age limit
                {
                    $this->records = Parameter::getPPDBAgeLimit();
                    // $this->records = Parameter::parameterSearch($this->request);

                    // print_r($this->records);exit;
                    $result = $this->rsExtJson($this->records,
                        $this->request->limit, $this->request->start);
                };
                break;
	    case 4:
                // acceptance 2
                {
                    //echo "HUAHAHAHAHA";
                    $session     = $this->request->session();
                    $tahunajaran = TahunAjaran::getPeriode($this->request);
                    $calonsiswa  = CalonSiswa::StudentProfile($this->request->get("id"));
                    //echo "<BR>HUAHAHAHAHA"; exit;
                    if ($calonsiswa) {
                        $result = view('ppdb/ppdbAccept_2' . config('app.system_skin'))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id)
                            ->with("PPDB_ID", $this->request->get("id"))
                            ->with("NAMA_LENGKAP", $calonsiswa[0]->nama_lengkap);
                    };
                    // else redirect to home page;
                };
                break;
	    case 9:
                // Department Data
                {
                    $this->records = Department::searchDepartment($this->request);
                    $result        = $this->rsExtJson($this->records,
                        $this->request->limit, $this->request->start);
                };
                break;
        };
        return $result;
    }

    /**
     * Completing data
     */
    public function completing($subtask) {
        switch ($subtask) {
            case 0:
                // load completing form page
                {
                    $session     = $this->request->session();
                    $tahunajaran = TahunAjaran::getPeriode($this->request);

                    $result = view('ppdb/ppdbComplete' . config('app.system_skin'))
                        ->with("TABID", $this->request->get("tabId"))
                        ->with("NOPEN", $this->request->get("id"))
                        ->with("TAHUNAJARAN", $tahunajaran->name)
                        ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                };
                break;
            case 1:
                // Login ppdb form page
                {
                    $session   = $this->request->session();
                    $head_data = json_decode(stripslashes($this->request->head));
                    $json_data = json_decode(stripslashes($this->request->json));
                    $modi_data = json_decode(stripslashes($this->request->modi));
                    $result    = CalonSiswa::Login($head_data[0],
                        $json_data,
                        $modi_data,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
                    if ($result[0] == false) {
                        $result = ["success" => false,
                            "message"            => "Failed to Login",
                            "server_message"     => $result[1],
                        ];
                    } else {
                        $result = ["success" => true,
                            "message"            => "Succeed",
                            "server_message"     => $result[1],
                        ];
                    };
                    return json_encode($result);
                };
                break;
            case 2:
                // save calon siswa data
                {
                    // echo "HUAHAHAHAHA"; exit;
                    $session      = $this->request->session();
                    $head_data    = json_decode(stripslashes($this->request->head));
                    $detail1_data = json_decode(stripslashes($this->request->dtl1));
                    $detail2_data = json_decode(stripslashes($this->request->dtl2));
                    $result       = CalonSiswa::CalonSave($head_data,
                        $detail1_data,
                        $detail2_data,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id"),
                        "completing"
                    );
                    if ($result[0] == false) {
                        $result = ["success" => false,
                            "message"            => "Failed to save records.",
                            "server_message"     => $result[1],
                        ];
                    } else {
                        $result = ["success" => true,
                            "message"            => "Succeed",
                            "server_message"     => $result[1],
                        ];
                    };
                    return json_encode($result);
                };
                break;
            case 3:
                // load Siswa Profile
                {
                    $this->records = CalonSiswa::StudentProfile($this->request->get("id"));
                    $result        = $this->rsExtJson($this->records, 75, 0);
                };
                break;
            case 4:
                // load Orangtua Profile
                {
                    $this->records = CalonSiswa::ParentsProfile($this->request->get("id"));
                    $result        = $this->rsExtJson($this->records, 75, 0);
                };
                break;
            case 9:
                // Load Upload Files
                {
                    $this->records = CalonSiswaFile::Search($this->request->get("id"));
                    $result        = $this->rsExtJson($this->records, 75, 0);
	        };
                break;
        };
        return $result;
    }
}
