<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\Administrator\MUser;
use App\Models\Siswa\Siswa;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
    protected function authenticated(Request $request, $user)
    {   //echo "<BR>authenticated";
        // update last login users
        $user->updated_at = date("Y-m-d H:i:s");
        $user->save();
        //echo "<BR>get muser data";
        $muser = MUser::where('laravel_id', '=', $user->id)->first();

        // $request->session()->put('key_satu', 'value_satu');
        $request->session()->put('company', $this->companies['company_code']);
        $request->session()->put('site', $this->companies['company_site']);

        if($muser)
        {   //echo "<BR>set session muser";
            $request->session()->put('user_id', $muser->user_id);
            $request->session()->put('username', $muser->username);
            $request->session()->put('name', $muser->name);
            $request->session()->put('dept', $muser->dept_id);
            $userrole = $muser->userrole()->first();
            if ($userrole->role_id == 2) // admin level
            {   $request->session()->put('level', $userrole->role_id);  }
            else
            {   $request->session()->put('level', 1); }; // user level
        }
        else
        {   //echo "<BR>get siswa data";
            $muser = Siswa::where('nis', '=', $user->name)->first();
            if ($muser)
            {   //echo "<BR>set session siswa";
                $request->session()->put('user_id', $muser->nis);
                $request->session()->put('username', $muser->nama_lengkap);
                $request->session()->put('level', 1); // user level
                $request->session()->put('jenjang', $muser->mapJenjang($muser->keyid_jenjang_sekolah) ); // user level
            }
        };
        // echo "<BR>return to home";
        // echo $request->session()->get('key_satu');
        // exit;
        return redirect('/home');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    // logout function
    public function logout(Request $request) 
    {   Auth::logout();
        return redirect('/');
    }
}
