<?php

namespace App\Http\Controllers\Sales;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Sales\Order;
use App\Models\Sales\OrderDetail;
use DB;

class SalesController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {   $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {   switch ($task)
        {   // routing data
            case 0 : return $this->routing($substask);      break;
            // Orders tab
            case 1 : return $this->crudOrders($substask);   break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {   switch ($subtask)
        {   case 0 : $result = view('sales/orders')->with("TABID", $this->request->get("tabId")); break;
            case 1 : $result = view('sales/proforma')->with("TABID", $this->request->get("tabId")); break;
            case 2 : $result = view('sales/reports')->with("TABID", $this->request->get("tabId")); break;
        };
        return $result;
    }
    /**
     * CRUD Orders
     */
    public function crudOrders($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->records = Order::headerPrintOut($this->request);
                        $this->crudOrders(3); 
                    }
                    else 
                    {   $this->records = Order::Search($this->request); 
                        $this->crudOrders(4); 
                    };
                }
                else
                {   $this->records = Order::Search($this->request);
                    $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                $modi_data = json_decode(stripslashes($this->request->modi));
                $result = Order::orderSave( $head_data, 
                                            $json_data, 
                                            $modi_data, 
                                            $session->get("site"),
                                            $session->get("user_id"),
                                            $session->get("company_id")
                                        );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Order::orderDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {   //print_r($this->records);
                $data = $this->records;
                $title = "PROFORMA INVOICE";
                $filename = "pi_".$data->pi_no;

                $this->create_report_pdf($this->request->session());
                $pdf = $this->report;
                ########## Document TITLE ##########
                $pdf->DocTitle($title, $data->pi_no, 30, 160);
                // $this->report->SetProtection(array('print'), '', null);
                ########## PAGE TITLE ##########
                $pdf->SetFont('Helvetica','B', 11);  
                $pdf->setRowXY(12, 40);  
                $pdf->Bookmark("Header", 1, -1);
                //$this->report->MultiCell(30,4,"Master",0,'L',0);
                $pdf->setRowXY(15, 40);
                $pdf->SetWidths(array(40, 60, 30, 60));
                $pdf->SetAligns(array("L", "L", "L", "L"));
                $pdf->SetFont('Times','', 10);
                $pdf->addRowHeight(5);
                $pdf->Row(array("PO.No", ": ".$data->order_no, 
                                "PO.Date", ": ".$data->order_date), false);
                $pdf->Row(array("PI.No", ": ".$data->pi_no, 
                                "PI.Date", ": ".$data->pi_date), false);
                $pdf->addRowHeight(1);
                // rectangle boxs
                $y_current = $pdf->y_pos+5;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height ); 
                $pdf->Rect($pdf->x_pos+(100), $y_current, 85, $y_height ); 

                $pdf->addRowHeight(1);
                $pdf->SetWidths(array(40, 150));
                $pdf->SetAligns(array("L", "L"));
                $pdf->Row(array("Shipper ", ": ".strtoupper($data->company_name)), false);
                $pdf->Row(array("", "  ".strtoupper($data->company_address)), false);
                // rectangle boxs
                $y_current = $y_current+$y_height;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current, 185, $y_height ); 
                
                $pdf->addRowHeight(1);
                $pdf->Row(array("Consignee", ": ".strtoupper($data->buyer_name)), false);
                $pdf->Row(array("", "  ".strtoupper($data->buyer_address)), false);
                // rectangle boxs
                $y_current = $y_current+$y_height;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current, 185, $y_height ); 

                $pdf->addRowHeight(5);
                $pdf->SetWidths(array(40, 150));
                $pdf->Row(array("Beneficiary Name", ": ".strtoupper($data->company_name)), false);
                $pdf->Row(array("Beneficiaty Bank", ": ".strtoupper($data->bank_name)), false);
                $pdf->Row(array("Bank Account (USD)", ": ".strtoupper($data->bank_account)), false);
                $pdf->Row(array("Swift Code", ": ".strtoupper($data->swift_code)), false);
                $pdf->Row(array("Bank Address", ": ".strtoupper($data->bank_address)), false);
                // rectangle boxs
                $y_current = $y_current+$y_height;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current, 185, $y_height ); 

                $pdf->addRowHeight(5);
                $pdf->SetWidths(array(40, 60, 30, 60));
                $pdf->Row(array("Container / Seal Vessel", ": ".$data->container_type_name,
                                "ETD", ": ".$data->etd ), false);
                $pdf->Row(array("Date of Shipment", ": ".$data->shipment_date,
                                "ETA", ": ".$data->eta), false);
                $pdf->Row(array("Port Of Loading ", ": ".$data->port_loading_name,
                                "Gross Weight", ": ".$data->weight_gross), false);
                $pdf->Row(array("Port Of Discharge ", ": ".$data->port_discharge_name,
                                "Nett Weight", ": ".$data->weight_nett), false);
                $pdf->Row(array("", "", "CBM", ": ".$data->cbm), false);
                // rectangle boxs
                $y_current = $y_current+$y_height;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height ); 
                $pdf->Rect($pdf->x_pos+(100), $y_current, 85, $y_height ); 
                /* DETAIL ITEMS */
                $details = OrderDetail::detailPrintOut(trim($data->order_no));
                $pdf->SetFont('Times','B', 10);
                $pdf->addRowHeight(2);
                $pdf->SetWidths(array(65, 20, 20, 20, 20, 20, 20));
                $pdf->SetAligns(array("C", "C", "C", "C", "C", "C", "C"));
                $pdf->Row(array("COMODITY", "QTY (Fcl)","QTY (Bags)", "NUT.QTY (Pcs)", "NUT.QTY (Kgs)", "PRICE (USD)(Kgs)", "AMOUNT (USD)"), true);
                $pdf->SetFont('Times','', 10);
                $pdf->SetAligns(array("L", "R", "R", "R", "R", "R", "R"));
                foreach ($details as $detail) 
                {   $pdf->Row(array($detail->item_name, 
                                    $detail->quantity_fcl,
                                    $detail->quantity_bags,
                                    $detail->quantity_nut_pcs,
                                    $detail->quantity_nut_kgs,
                                    $detail->unit_price,
                                    $detail->amount), true);
                };
                $pdf->addRowHeight(5);
                $pdf->SetWidths(array(40, 150));
                $pdf->SetAligns(array("L", "L"));
                $loop = 0;
                foreach (explode(",", $data->payment_term) as $the_term)
                {   if ($loop == 0) 
                    {   $pdf->Row(array("Payment Term", ": ".$the_term), false); 
                        $loop = 1;
                    }
                    else {  $pdf->Row(array("", " ".$the_term), false); }
                };
                
                $pdf->Output($filename.".pdf", "I");
                exit;
            };
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("NO", "COMPANY.ID", "COMPANY.NAME", "ORDER.NO", "ORDER.DATE", 
                            "BUYER.ID", "BUYER.NAME", "PI.NO", "PI.DATE","CREATED");
                $loop = 1;
                $z = 8;
                foreach($results as $result) 
                {   $z1 = $z + $loop;
                    $data[] = array( $loop, 
                        $result->company_id, $result->company_name, $result->order_no, $result->order_date,
                        $result->buyer_id, $result->buyer_name, $result->pi_no, $result->pi_date, 
                        $this->TimeStampToString($result->created_date,"d/m/Y H:i")
                    );
                    $loop = $loop + 1;
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'List Of Order',
                    'listorder',
                    array( 
                        "A" => 3,                            "B" => 8,
                        "C" => 25,                           "D" => 15,
                        "E" => 10,                           "F" => 10,
                        "G" => 10,                           "H" => 20,
                        "I" => 10,                           "J" => 15),
                    $data,
                    'A',
                    'J');

                $y = $z + $loop +1;
                $sheet = $this->report->getActiveSheet();
                $this->report->download('xls');
            };
            break;
            case 5 : // search order detail
            {   $this->records = OrderDetail::Search($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( Order::SearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }

}
