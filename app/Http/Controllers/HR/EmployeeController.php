<?php

namespace App\Http\Controllers\HR;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HR\Employee;

class EmployeeController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $records;
    public function __construct(Request $request)
    {   $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {   switch ($task)
        {   // master data
            case 0 : return $this->master($substask);       
            break;
            // employee tab
            case 1 : return $this->crudEmployee($substask);  
            break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function master($subtask)
    {   switch ($subtask)
        {   case 0 : $result = view('hr/employee')->with("TABID", $this->request->get("tabId")); break;
        };
        return $result;
    }
    /**
     * Create Replace Update Delete User
     */
    public function crudEmployee($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   $this->records = Employee::employeeSearch($this->request);
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->crudEmployee(3); }
                    else {  $this->crudEmployee(4); };
                }
                else
                {   $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $type = json_decode(stripslashes($this->request->type));
                $result = Employee::employeeSave(
                $json_data,
                $type, 
                $this->request->btn,
                $session->get("user_id")
            );
            if ( $result[0] == false ){ 
                $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = Employee::employeeDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {};
            break;
            case 4 : // print xls
            {   
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "NAME", "CAPITOL", "CURRENCY", "PHONE.AREA", "STATUS", "CREATED");
                foreach($results as $result) {
                        $data[] = array(
                            $result->country_id, 
                            $result->name, 
                            $result->capitol, 
                            $result->currency_id,
                            $result->phone_area,
                            Employee::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Countries',
                    'Countries',
                    array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                    $data,
                    'A',
                    'G');
                $this->report->download('xls');
            };
            break;
            case 9 :
            {   $result = $this->rsExtJson( Employee::employeeSearchExt($this->request), 
                    $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }

}
