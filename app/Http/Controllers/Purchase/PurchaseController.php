<?php

namespace App\Http\Controllers\Purchase;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Purchase\PR;
use App\Models\Purchase\PRDetail;
use App\Models\Purchase\PO;
use App\Models\Purchase\PODetail;
use App\Models\Purchase\POCharges;
use App\Models\Purchase\POInv;
use App\Models\Purchase\POInvDetail;
use App\Models\Purchase\POInvCharges;

use DB;

class PurchaseController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {   $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {   switch ($task)
        {   // routing data
            case 0 : return $this->routing($substask);  break;
            // PR tab
            case 1 : return $this->crudPR($substask);   break;
            // PO tab
            case 2 : return $this->crudPO($substask);   break;
            // Invoices tab
            case 3 : return $this->crudInvoice($substask);   break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {   switch ($subtask)
        {   case 0 : $result = view('purchase/pr')->with("TABID", $this->request->get("tabId")); break;
            case 1 : $result = view('purchase/po')->with("TABID", $this->request->get("tabId")); break;
            case 2 : $result = view('purchase/poinvoice')->with("TABID", $this->request->get("tabId")); break;
            case 3 : $result = view('purchase/reports')->with("TABID", $this->request->get("tabId")); break;
        };
        return $result;
    }
    /**
     * CRUD PR
    */
    public function crudPR($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->records = PR::headerPrintOut($this->request);  
                        $this->crudPR(3); }
                    else 
                    {   $this->records = PR::Search($this->request);
                        $this->crudPR(4); };
                }
                else
                {   $this->records = PR::Search($this->request);   
                    $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                $modi_data = json_decode(stripslashes($this->request->modi));
                $result = PR::prSave(   $head_data, 
                                        $json_data, 
                                        $modi_data, 
                                        $session->get("site"),
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = PR::prDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {   //print_r($this->records);
                $data = $this->records;
                $title = "PURCHASE REQUEST";
                $filename = "pr_".$data->pr_no;

                $this->create_report_pdf($this->request->session());
                $pdf = $this->report;
                if ($data->pr_status == 0) // draft document
                {   $this->pdf_watermark(70, 110, " D R A F T ", 45); };
                ########## Document TITLE ##########
                $pdf->DocTitle($title, $data->pr_no, 30, 160);
                // $this->report->SetProtection(array('print'), '', null);
                ########## PAGE TITLE ##########
                $pdf->SetFont('Helvetica','B', 11);  
                $pdf->setRowXY(12, 40);  
                $pdf->Bookmark("Header", 1, -1);
                //$this->report->MultiCell(30,4,"Master",0,'L',0);
                $pdf->setRowXY(15, 40);
                $pdf->SetWidths(array(40, 60, 30, 60));
                $pdf->SetAligns(array("L", "L", "L", "L"));
                $pdf->SetFont('Times','', 10);
                $pdf->addRowHeight(5);
                $pdf->Row(array("PR.No", ": ".$data->pr_no, 
                                "Remarks", ": ".$data->description), false);
                $pdf->Row(array("PR.Date ", ": ".$this->TimeStampToString($data->created_date,"d/m/Y H:i"), "", ""), false);
                $pdf->Row(array("Approved.Date ", ": ".$this->TimeStampToString($data->approved_date,"d/m/Y H:i"), "", ""), false);
                $pdf->addRowHeight(5);
                // rectacngle boxs
                $y_current = $pdf->y_pos+5;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height ); 
                $pdf->Rect($pdf->x_pos+(100), $y_current, 85, $y_height ); 
                /* DETAIL ITEMS */
                $details = PRDetail::detailPrintOut(trim($data->pr_no));
                $pdf->SetFont('Helvetica','B', 10);
                $pdf->addRowHeight(5);
                $pdf->SetWidths(array(10, 130, 30, 15));
                $pdf->Bookmark("Detail", 1, -1);
                $pdf->SetAligns(array("C", "C", "C", "C"));
                $pdf->Row(array("No", "Item Description","Quantity", "UM"), true);
                $pdf->SetAligns(array("L", "L", "R", "L"));
                $pdf->SetFont('Helvetica','', 10);
                $loop = 1;
                foreach ($details as $detail) 
                {   $pdf->Row(array($loop,
                                    $detail->item_name, 
                                    $detail->quantity_primary,
                                    $detail->um_primary), true);
                    $loop += 1;
                };
                // $pdf->Rect($pdf->x_pos-3, $pdf->y_pos, 185, ($pdf->height += 2)-$pdf->y_pos);
                // $pdf->Cell(50, 25, 'Hello World ! '.rand(2,5000));

                $pdf->Output($filename.".pdf", "I");
                exit;
            };
            break;
            case 4 : // print xls
            {   //print_r($this->records);
                $results = $this->records;
                $data = array();
                $data[] = array("NO", "COMPANY.ID", "COMPANY.NAME", "PR.NO", "PR.DATE",
                            "ORDER.NO", "PR.TYPE", "STATUS", 
                            "APPROVED.BY", "APPROVED.DATE", "CREATED");
                $loop = 1;
                $z = 8;
                foreach($results as $result) 
                {   $z1 = $z + $loop;
                    $data[] = array( $loop, 
                        $result->company_id, $result->company_name, $result->pr_no, $result->pr_date,
                        $result->order_no, $result->pr_type, $result->pr_status_name, 
                        $result->approved_by_name, 
                        $this->TimeStampToString($result->approved_date,"d/m/Y H:i"),
                        $this->TimeStampToString($result->created_date,"d/m/Y H:i")
                    );
                    $loop = $loop + 1;
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'List Of Purchase Requisitions',
                    'listpr',
                    array( 
                        "A" => 3,                            "B" => 10,
                        "C" => 25,                           "D" => 15,
                        "E" => 10,                           "F" => 10,
                        "G" => 10,                           "H" => 10,
                        "I" => 10,                           "J" => 15,
                        "K" => 15),
                    $data,
                    'A',
                    'K');

                $y = $z + $loop +1;
                $sheet = $this->report->getActiveSheet();
                $this->report->download('xls');
            };
            break;
            case 5 : // search order detail
            {   $this->records = PRDetail::Search($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 8 : // approve data
            {   $session = $this->request->session();
                $result = PR::prApprove($this->request, $session->get("user_id"));
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 9 : // searchExt PR_NO
            {   $result = $this->rsExtJson( PR::SearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
            case 10 : // searchExt PR ITEMS
            {   $result = $this->rsExtJson( PRDetail::itemsneedpoSearch($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
    * CRUD PO
    */
    public function crudPO($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->records = PO::headerPrintOut($this->request);
                        $this->crudPO(3); }
                    else 
                    {   $this->records = PO::Search($this->request);
                        $this->crudPO(4); };
                }
                else
                {   $this->records = PO::Search($this->request);
                    $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                $chg_data = json_decode(stripslashes($this->request->chg));
                $result = PO::poSave(   $head_data, 
                                        $json_data, 
                                        $ch_data, 
                                        $session->get("site"),
                                        $session->get("user_id")
                                    );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = PO::poDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {   //print_r($this->records);
                $data = $this->records;
                $title = "PURCHASE ORDER";
                $filename = "po_".$data->po_no;

                $this->create_report_pdf($this->request->session());
                $pdf = $this->report;
                if ($data->po_status == 0) // draft document
                {   $this->pdf_watermark(70, 110, " D R A F T ", 45); };
                ########## Document TITLE ##########
                $pdf->DocTitle($title, $data->po_no, 30, 160);
                // $this->report->SetProtection(array('print'), '', null);
                ########## PAGE TITLE ##########
                $pdf->SetFont('Helvetica','B', 11);  
                $pdf->setRowXY(12, 40);  
                $pdf->Bookmark("Header", 1, -1);
                //$this->report->MultiCell(30,4,"Master",0,'L',0);
                $pdf->setRowXY(15, 40);
                $pdf->SetWidths(array(30, 70, 20, 70));
                $pdf->SetAligns(array("L", "L", "L", "L"));
                $pdf->SetFont('Times','', 10);
                $pdf->addRowHeight(5);
                $pdf->Row(array("PO.No", ": ".$data->po_no, 
                                "Supplier", ": ".$data->supplier_name), false);
                $pdf->Row(array("PO.Date", ": ".$this->TimeStampToString($data->created_date,"d/m/Y"), "", ""), false);
                $pdf->Row(array("Approved.Date", ": ".$this->TimeStampToString($data->approved_date,"d/m/Y H:i"), "", ""), false);
                
                
                $pdf->addRowHeight(5);
                // rectacngle boxs
                $y_current = $pdf->y_pos+5;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height ); 
                $pdf->Rect($pdf->x_pos+(100), $y_current, 85, $y_height ); 
                $pdf->SetWidths(array(30, 155));
                $pdf->SetAligns(array("L", "L"));
                $pdf->Row(array("Remarks", $data->description), true);
                /* DETAIL ITEMS */
                $details = PODetail::detailPrintOut(trim($data->po_no));
                $pdf->SetFont('Helvetica','B', 10);
                $pdf->addRowHeight(5);
                $pdf->SetWidths(array(10, 90, 20, 10, 20, 35));
                $pdf->Bookmark("Detail", 1, -1);
                $pdf->SetAligns(array("C", "C", "C", "C", "C", "C"));
                $pdf->Row(array("No", "Item Description","Quantity", "UM", "Price", "Amount"), true);
                $pdf->SetAligns(array("L", "L", "R", "L", "R", "R"));
                $pdf->SetFont('Times','', 10);
                $loop = 1;
                $total = 0;
                foreach ($details as $detail) 
                {   $total += $detail->amount;
                    $pdf->Row(array($loop,
                                    $detail->item_name, 
                                    number_format($detail->quantity_primary, 0, '.', ','),
                                    $detail->um_primary,
                                    number_format($detail->unit_price, 2, '.', ',')." ".$data->currency_id,
                                    number_format($detail->amount, 2, '.', ',')." ".$data->currency_id,
                                    ), true);
                    $loop += 1;
                };
                $pdf->SetWidths(array(150, 35));
                $pdf->SetAligns(array("R", "R"));
                $pdf->Row(array("TOTAL ", number_format($total, 2, '.', ',')." ".$data->currency_id), true);

                if ($data->currency_id == "IDR"){ $lang_id = "id"; } else { $lang_id = "en"; };
                $spell_total = new \NumberFormatter($lang_id, \NumberFormatter::SPELLOUT);
                $pdf->SetWidths(array(185));
                $pdf->SetAligns(array("R"));
                $pdf->Row(array("TERBILANG : ".$spell_total->format($total)." ".$data->currency_id), true);    
                
                $pdf->addRowHeight(5);
                $pdf->SetWidths(array(30, 160));
                $pdf->SetAligns(array("L", "L"));
                $loop = 0;
                foreach (explode(",", $data->payment_term) as $the_term)
                {   if ($loop == 0) 
                    {   $pdf->Row(array("Payment Term", ": ".$the_term), false); 
                        $loop = 1;
                    }
                    else {  $pdf->Row(array("", " ".$the_term), false); }
                };
                
                $pdf->Output($filename.".pdf", "I");
                exit;
            };
            break;
            case 4 : // print xls
            {   //print_r($this->records);
                $results = $this->records;
                $data = array();
                $data[] = array("NO", "COMPANY.ID", "COMPANY.NAME", "PO.NO", "PO.DATE",
                            "STATUS", 
                            "APPROVED.BY", "APPROVED.DATE", "CREATED");
                $loop = 1;
                $z = 8;
                foreach($results as $result) 
                {   $z1 = $z + $loop;
                    $data[] = array( $loop, 
                        $result->company_id, $result->company_name, $result->po_no, $result->po_date,
                        $result->po_status_name, 
                        $result->approved_by_name, 
                        $this->TimeStampToString($result->approved_date,"d/m/Y H:i"),
                        $this->TimeStampToString($result->created_date,"d/m/Y H:i")
                    );
                    $loop = $loop + 1;
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'List Of Purchase Orders',
                    'listpo',
                    array( 
                        "A" => 3,                            "B" => 10,
                        "C" => 25,                           "D" => 15,
                        "E" => 10,                           "F" => 10,
                        "G" => 10,                           "H" => 15,
                        "I" => 15),
                    $data,
                    'A',
                    'I');

                $y = $z + $loop +1;
                $sheet = $this->report->getActiveSheet();
                $this->report->download('xls');
            };
            break;
            case 5 : // search detail
            {   $this->records = PODetail::Search($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 6 : // search charges
            {   $this->records = POCharges::Search($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 8 : // approve data
            {   $session = $this->request->session();
                $result = PO::poApprove($this->request, $session->get("user_id"));
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 9 :
            {   
                $result = $this->rsExtJson( PO::SearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
        };
        return $result;
    }
    /**
    * CRUD Invoice
    */
    public function crudInvoice($subtask)
    {   switch ($subtask)
        {   case 0 : // search data
            {   
                if ($this->request->pt)
                {   if ($this->request->pt == 'pdf')
                    {   $this->records = POInv::headerPrintOut($this->request);
                        $this->crudPO(3); }
                    else 
                    {   $this->records = POInv::Search($this->request);
                        $this->crudPO(4); };
                }
                else
                {   $this->records = POInv::Search($this->request);
                    $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
                };
            };
            break;
            case 1 : // save data
            {   $session = $this->request->session();
                $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                $modi_data = json_decode(stripslashes($this->request->modi));
                $result = POInv::poinvSave(    $head_data, 
                                            $json_data, 
                                            $modi_data, 
                                            $session->get("site"),
                                            $session->get("user_id")
                                        );
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 2 : // delete data
            {   $result = POInv::poDelete($this->request);
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 3 : // print pdf
            {   //print_r($this->records);
                $data = $this->records;
                $title = "PURCHASE Invoices";
                $filename = "po_".$data->po_no;

                $this->create_report_pdf($this->request->session());
                $pdf = $this->report;
                if ($data->po_status == 0) // draft document
                {   $this->pdf_watermark(70, 110, " D R A F T ", 45); };
                ########## Document TITLE ##########
                $pdf->DocTitle($title, $data->po_no, 30, 160);
                // $this->report->SetProtection(array('print'), '', null);
                ########## PAGE TITLE ##########
                $pdf->SetFont('Helvetica','B', 11);  
                $pdf->setRowXY(12, 40);  
                $pdf->Bookmark("Header", 1, -1);
                //$this->report->MultiCell(30,4,"Master",0,'L',0);
                $pdf->setRowXY(15, 40);
                $pdf->SetWidths(array(30, 70, 20, 70));
                $pdf->SetAligns(array("L", "L", "L", "L"));
                $pdf->SetFont('Times','', 10);
                $pdf->addRowHeight(5);
                $pdf->Row(array("PO.No", ": ".$data->po_no, 
                                "Supplier", ": ".$data->supplier_name), false);
                $pdf->Row(array("PO.Date", ": ".$this->TimeStampToString($data->created_date,"d/m/Y"), "", ""), false);
                $pdf->Row(array("Approved.Date", ": ".$this->TimeStampToString($data->approved_date,"d/m/Y H:i"), "", ""), false);
                
                
                $pdf->addRowHeight(5);
                // rectacngle boxs
                $y_current = $pdf->y_pos+5;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current, 100, $y_height ); 
                $pdf->Rect($pdf->x_pos+(100), $y_current, 85, $y_height ); 
                $pdf->SetWidths(array(30, 155));
                $pdf->SetAligns(array("L", "L"));
                $pdf->Row(array("Remarks", $data->description), true);
                /* DETAIL ITEMS */
                $details = POInvDetail::detailPrintOut(trim($data->po_no));
                $pdf->SetFont('Helvetica','B', 10);
                $pdf->addRowHeight(5);
                $pdf->SetWidths(array(10, 90, 20, 10, 20, 35));
                $pdf->Bookmark("Detail", 1, -1);
                $pdf->SetAligns(array("C", "C", "C", "C", "C", "C"));
                $pdf->Row(array("No", "Item Description","Quantity", "UM", "Price", "Amount"), true);
                $pdf->SetAligns(array("L", "L", "R", "L", "R", "R"));
                $pdf->SetFont('Times','', 10);
                $loop = 1;
                $total = 0;
                foreach ($details as $detail) 
                {   $total += $detail->amount;
                    $pdf->Row(array($loop,
                                    $detail->item_name, 
                                    number_format($detail->quantity_primary, 0, '.', ','),
                                    $detail->um_primary,
                                    number_format($detail->unit_price, 2, '.', ',')." ".$data->currency_id,
                                    number_format($detail->amount, 2, '.', ',')." ".$data->currency_id,
                                    ), true);
                    $loop += 1;
                };
                $pdf->SetWidths(array(150, 35));
                $pdf->SetAligns(array("R", "R"));
                $pdf->Row(array("TOTAL ", number_format($total, 2, '.', ',')." ".$data->currency_id), true);

                if ($data->currency_id == "IDR"){ $lang_id = "id"; } else { $lang_id = "en"; };
                $spell_total = new \NumberFormatter($lang_id, \NumberFormatter::SPELLOUT);
                $pdf->SetWidths(array(185));
                $pdf->SetAligns(array("R"));
                $pdf->Row(array("TERBILANG : ".$spell_total->format($total)." ".$data->currency_id), true);    
                
                $pdf->addRowHeight(5);
                $pdf->SetWidths(array(30, 160));
                $pdf->SetAligns(array("L", "L"));
                $loop = 0;
                foreach (explode(",", $data->payment_term) as $the_term)
                {   if ($loop == 0) 
                    {   $pdf->Row(array("Payment Term", ": ".$the_term), false); 
                        $loop = 1;
                    }
                    else {  $pdf->Row(array("", " ".$the_term), false); }
                };
                
                $pdf->Output($filename.".pdf", "I");
                exit;
            };
            break;
            case 4 : // print xls
            {   //print_r($this->records);
                $results = $this->records;
                $data = array();
                $data[] = array("NO", "COMPANY.ID", "COMPANY.NAME", "PO.NO", "PO.DATE",
                            "STATUS", 
                            "APPROVED.BY", "APPROVED.DATE", "CREATED");
                $loop = 1;
                $z = 8;
                foreach($results as $result) 
                {   $z1 = $z + $loop;
                    $data[] = array( $loop, 
                        $result->company_id, $result->company_name, $result->po_no, $result->po_date,
                        $result->po_status_name, 
                        $result->approved_by_name, 
                        $this->TimeStampToString($result->approved_date,"d/m/Y H:i"),
                        $this->TimeStampToString($result->created_date,"d/m/Y H:i")
                    );
                    $loop = $loop + 1;
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'List Of Purchase Orders',
                    'listpo',
                    array( 
                        "A" => 3,                            "B" => 10,
                        "C" => 25,                           "D" => 15,
                        "E" => 10,                           "F" => 10,
                        "G" => 10,                           "H" => 15,
                        "I" => 15),
                    $data,
                    'A',
                    'I');

                $y = $z + $loop +1;
                $sheet = $this->report->getActiveSheet();
                $this->report->download('xls');
            };
            break;
            case 5 : // search detail
            {   $this->records = POInvDetail::Search($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 6 : // search charges
            {   $this->records = POInvCharges::Search($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 8 : // approve data
            {   $session = $this->request->session();
                $result = POInv::poApprove($this->request, $session->get("user_id"));
                if ( $result[0] == false ){ $this->server_message = $result[1]; };
                $result = $this->jsonSuccess( $result[0] );
            };
            break;
            case 9 :
            {   
                $result = $this->rsExtJson( POInv::SearchExt($this->request), 
                            $this->request->limit, $this->request->start);
            };
            break;
            case 11 : // search reference
            {   $this->records = POInv::incomingRefSearch($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;
            case 12 : // search passed qc items
            {   $this->records = POInv::incomingRefItems($this->request);
                $result = $this->rsExtJson(  $this->records,
                                $this->request->limit, $this->request->start);
            };
            break;       
        };
        return $result;
    }

}
