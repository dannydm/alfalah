<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Siswa\Siswa;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // Get the currently authenticated user...
        $user = Auth::user();
        // dd($user);
        // echo "<br>abcde";
        // // Get the currently authenticated user's ID...
        $id = Auth::id();
        // dd($id);
        $session = $this->request->session();
        // print_r($session); exit;
        // dd($this->request->session()); 
        // dd($this->request->session()->all());
        // dd($this->request->session()->get("user_id"));
        // $company_name = $this->companies['company_name'];
        // return view('homeExt');
        $skin = config('app.system_skin');
        switch ( $skin )
        {   case 'LTE' :
            {   $fullname = '';
                $nis = '';
                $nopen = '';

                if ( $session->get("level") == 1 )
                {   $siswa = Siswa::Profile($session->get("username"));
                    $fullname = $siswa->nama_lengkap;
                    $nis = $siswa->nis;
                    $nopen = $siswa->no_pendaftaran;
                }
                else
                {   $fullname = 'Administrator';
                    $nis = '...';
                    $nopen = '...';
                };

                return view('home'.$skin)
                    ->with("USER_ID", $session->get("user_id"))
                    ->with("USERNAME", $session->get("username"))
                    ->with("FULLNAME", $fullname )
                    ->with("NIS", $nis)
                    ->with("NOPEN", $nopen)
                    ->with("USERSITE", $session->get("site"))
                    ->with("COMPANY_CODE", $this->companies['company_code'])
                    ->with("COMPANY_NAME", $this->companies['company_name']);
            }
            break;
            default :
            // case 'Ext' :
            {
                return view('home'.$skin)
                    ->with("USER_ID", $session->get("user_id"))
                    ->with("USERNAME", $session->get("username"))
                    ->with("USERSITE", $session->get("site"))
                    ->with("COMPANY_CODE", $this->companies['company_code'])
                    ->with("COMPANY_NAME", $this->companies['company_name']);
            }
            break;
        };
    }
}
