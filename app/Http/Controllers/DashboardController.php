<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\PurchaseRequestController;
use App\Models\Administrator\Menu;
use App\Models\Anggaran\Monitoring;

class DashboardController extends Controller
{   
    private $PR;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {   $this->middleware('auth');
        $this->request = $request;
        $this->PR = new PurchaseRequestController($request);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {   
        switch ($task)
        {   case 0 : // master data
                return $this->master($substask);
            break;
            case 1 : // myPR data
                return $this->myPR($substask);
            break;
        };
    }

    public function master($subtask)
    {   switch ($subtask)
        {   case 0 : 
            $urusanbudget = Monitoring::UrusanChart($this->request);
                return view('dashboard')->with("TABID", $this->request->get("tabId"))
                 ->with("URUSAN", $urusanbudget)
                ;
            break;
            case 1 : 
                return Menu::ActiveMenu($this->request->session()->get("user_id")); 
            break;
        };
    }
    public function myPR($subtask)
    {   $result = null;
        // $result =  $this->PR->myPR($subtask);
        return $result;
    }
}
