<?php

namespace App\Http\Controllers\Newanggaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Anggaran\RapbsMaster;
use App\Models\Anggaran\RapbsDetail;
use App\Models\Anggaran\UangmukaMaster;
use App\Models\Anggaran\UangmukaDetail;
use App\Models\Anggaran\PengajuanDetail;
use App\Models\Anggaran\PengajuanMaster;
use App\Models\Anggaran\Monitoring;
use App\Models\Ppdb\TahunAjaran; 


class MonitoringController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {
        switch ($task) {   // routing data
            case 0: return $this->routing($substask);   break;
            // RAPBS
            case 1: return $this->crudMonitoring($substask);  break;
            case 2: return $this->crudMonitoringCair($substask);    break;
            case 3: return $this->crudRapbsDetail($substask);    break;
            case 4: return $this->crudMonitoringUser($substask);    break;

        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {
        switch ($subtask) {
            case 0: // monitoring page 
                {
                $session     = $this->request->session();
                $tahunajaran = TahunAjaran::getPeriode($this->request);
                $cost        = Monitoring::CostMonitoringSearch($this->request);
                $income      = Monitoring::IncomeMonitoring($this->request);
                // print_r($cost);exit; 
                $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
                     
            //    print_r($income);exit;
                $result  = view('newanggaran/monitoring' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
     
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id)
                            ->with("INCOME", $income)
                            ->with("COST", $cost)
                            ;

                            
                        };
                
            break;
            case 1: // monitoring user page 
                {   

                $session = $this->request->session();
                $tahunajaran= TahunAjaran::getPeriode($this->request);
                $allstatus        = Monitoring::statusMonitoring($this->request, $session->get("user_id") );
                $allpengajuan     = Monitoring::historyPengajuan($this->request);
                $alluangmuka      = Monitoring::historyUangmuka($this->request);

                $result  = view('newanggaran/monitoringuser' . config('app.system_skin'))
                    ->with("USER_ID", $session->get("user_id"))
                    ->with("USERNAME", $session->get("username"))
                    ->with("USERSITE", $session->get("site"))
                    ->with("TABID", $this->request->get("tabId"))
                    ->with("TAHUNAJARAN", $tahunajaran->name)
                    ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id)
                    ->with("ALLSTATUS", $allstatus)
                    ->with("PENGAJUANSTATUS", $allpengajuan)
                    ->with("UANGMUKASTATUS", $alluangmuka)
                    ;
                };
            break;
        };
        return $result;
    }

    public function crudMonitoring($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = RapbsMaster::MonitoringSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->records = RapbsMaster::headerusulanPrintOut($this->request); 
                    //  print_r($this->records);exit; 
                        $this->crudNewrapbs(3);
                    } else {
                        $this->crudNewrapbs(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
            {
                $this->records = RapbsMaster::MonitoringSearch($this->request);
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->RapbsMaster(3);
                    } else {
                        $this->RapbsMaster(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
        };
            break;
            case 2: // delete data
            {
                $result = RapbsMaster::RapbsMasterDelete($this->request);
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 3: // print pdf
            {};
            break;
            case 4: // print xls
            {
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                foreach ($results as $result) {
                    $data[] = array(
                        $result->RapbsMaster_key_id,
                        $result->name,
                        Newrapbs::getStatus($result->status),
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Countries',
                    'Countries',
                    array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                    $data,
                    'A',
                    'G'
                );
                $this->report->download('xls');
            };
            break;
            case 10: // search detail data
            {   $this->records = RapbsDetail::RapbsDetailSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
            };
            break;
            case 11: // Rekap data
                {   $this->records = RapbsMaster::RapbsRekapSearch($this->request);
                    $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                };
                break;
            case 15: // search data
            {
                $session = $this->request->session();
                $this->records = Monitoring::MonitoringAllStatus($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->records = RapbsMaster::headerusulanPrintOut($this->request); 
                    //  print_r($this->records);exit; 
                        $this->crudNewrapbs(3);
                    } else {
                        $this->crudNewrapbs(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 705: // approve data
            {   
                $session = $this->request->session();
                $result = RapbsMaster::RapbsMasterApproval2(
                    $this->request,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);

            };
            break;
            case 707: // reject data
                {   
                    $session = $this->request->session();
                    $result = RapbsMaster::RapbsMasterReject2(
                        $this->request,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
    
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
    
                };
                break;
            };
        return $result;
    }

    public function crudMonitoringCair($subtask)
    {
        switch ($subtask) {
            case 0: // search data pengajuan
                {
                    $session = $this->request->session(); 
                    $this->records = Monitoring::MonitoringPengajuanSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                
                );
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->RapbsMaster(3);
                        } else {
                            $this->RapbsMaster(4);
                        };
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
                break;
            case 1: // search data pencairan
                {
                    {
                        $session = $this->request->session(); 
                        $this->records = Monitoring::MonitoringcairSearch($this->request,  
                        $session->get("user_id"),
                        $session->get("level")
                    
                    );
                        if ($this->request->pt) {
                            if ($this->request->pt == 'pdf') {
                                $this->RapbsMaster(3);
                            } else {
                                $this->RapbsMaster(4);
                            };
                        } else {
                            $result = $this->rsJson(
                                $this->records,
                                $this->request->limit,
                                $this->request->start
                            );
                        };
                    };
    
                };
                break;
            case 2: // monitoring serapan anggaran 
                {   $this->records = Monitoring::SerapanSearch($this->request);
                    $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                };
                break;
            case 3: // detail monitoring serapan anggaran / kegiatan
                {
                    {   $this->records = Monitoring::SerapanKegiatanSearch($this->request);
                        $result = $this->rsJson(
                                $this->records,
                                $this->request->limit,
                                $this->request->start 
                            );
                    };
                };
                break;
            case 4: // Cost Monitoring
                {  
             
                };
                break;
                case 5: // FORECASTING MONITORING
                    {   $this->records = Monitoring::ForecastingSearch($this->request);
                        $result = $this->rsJson(
                                $this->records,
                                $this->request->limit,
                                $this->request->start 
                            );
                    };

                break;
            case 9: {
                    $result = $this->rsExtJson(
                        RapbsMaster::RapbsMasterSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
            break;
            case 10: { 
                $result = $this->rsExtJson(
                    Monitoring::MonitoringPengajuanDetailSearch($this->request),
                    
                    $this->request->limit,
                    $this->request->start
                );
            };
        break;
        };
        return $result;
    }

    public function crudRapbsDetail($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = RapbsDetail::MonitoringSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->RapbsMaster(3);
                        } else {
                            $this->RapbsMaster(4);
                        };
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
                break;
            };
        return $result;
    }

    public function crudMonitoringUser($subtask)
    {
        switch ($subtask) {
            case 0: // // monitoring user tab pertama master 4.0
                {
                    $session = $this->request->session(); 
                    $this->records = Monitoring::MonitoringrapbyuserSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->RapbsMaster(3);
                        } else {
                            $this->RapbsMaster(4);
                        };
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
                break;
            case 1: // ////monitoring user status pengajuan 4.1
                {
                        $session = $this->request->session(); 
                        $this->records = Monitoring::MonitoringUserPengajuanSearch($this->request, 
                        $session->get("user_id"),
                        $session->get("level")
                    
                    );
                            $result = $this->rsJson(
                                $this->records,
                                $this->request->limit,
                                $this->request->start
                            );
    
                };
                break;
            case 2: //searching usulan
                {
                    {
                        $session = $this->request->session();
                        $this->records = Monitoring::MonitoringusercairSearch($this->request, 
                            $session->get("user_id"),
                            $session->get("level")
                        );
                        if ($this->request->pt) {
                            if ($this->request->pt == 'pdf') {
                                $this->records = RapbsMaster::headerusulanPrintOut($this->request); 
                            //  print_r($this->records);exit; 
                                $this->crudNewrapbs(3);
                            } else {
                                $this->crudNewrapbs(4);
                            };
                        } else {
                            $result = $this->rsJson(
                                $this->records,
                                $this->request->limit,
                                $this->request->start
                            );
                        };
                    };                    
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        RapbsMaster::RapbsMasterSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
            break;
            case 10: // monitoring detail user tab pertama 4.10
            {   $this->records = Monitoring::MonitoringRapbsDetailSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
            };
            break;
            case 11: {
                
                $result = $this->rsExtJson(
                    Monitoring::MonitoringuserPengajuanDetailSearch($this->request->pengajuan_no),
                    
                    $this->request->limit,
                    $this->request->start
                );
            };
        break;
        case 15: // monitoring user untuk allstatus 4/15
        {
            $session = $this->request->session(); 
            $this->records = Monitoring::MonitoringuserAllStatus($this->request, 
                $session->get("user_id"),
                $session->get("level")
            );
            if ($this->request->pt) {
                if ($this->request->pt == 'pdf') {
                    $this->records = RapbsMaster::headerusulanPrintOut($this->request); 
                //  print_r($this->records);exit; 
                    $this->crudNewrapbs(3);
                } else {
                    $this->crudNewrapbs(4);
                };
            } else {
                $result = $this->rsJson(
                    $this->records,
                    $this->request->limit,
                    $this->request->start
                );
            };
        };


        };
        return $result;
    }
}
