<?php

namespace App\Http\Controllers\Newanggaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Anggaran\UangmukaMaster;
use App\Models\Anggaran\UangmukaDetail;
use App\Models\Anggaran\UangmukaReject;
use App\Models\Anggaran\PengajuanDetail;
use App\Models\Anggaran\PengajuanMaster;
use App\Models\Ppdb\TahunAjaran;
use Fpdf;

use DB;
class ApprovalcairController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {
        switch ($task) {   // routing data
            case 0: return $this->routing($substask);   break;
            // RAPBS
            case 1: return $this->crudApprovalcair($substask);  break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {
        switch ($subtask) {
            case 0: // anggaran page 
                {
                    $session = $this->request->session();
                    $tahunajaran= TahunAjaran::getPeriode($this->request);
                    $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
            
                    $result  = view('newanggaran/approvalcair' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                };
                break;
        };
        return $result;
    }

    public function crudApprovalcair($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = UangmukaMaster::AppcairSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->records = UangmukaMaster::headerPrintOut($this->request);
                        $this->crudAppum(3);
                    } else {
                        $this->crudAppum(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
            {
                $session = $this->request->session();
                $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                $modi_data = json_decode(stripslashes($this->request->modi));

                $result = PengajuanMaster::PengajuanMasterSave(
                    $head_data[0],
                    $json_data,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 2: // delete data
            {
                $result =  UangMukaMaster::UangMukaMasterDelete($this->request);
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 3: // print pdf
            {
                    // print_r($this->records);exit;
                    $data = $this->records;
                    // $title = "RENCANA KERJA DAN ANGGARAN YAYASAN MASJID DARUSALLAM";
                    $filename = "U_M".$data->uangmuka_no;
    
                    $this->create_report_pdf($this->request->session());
                    $pdf = $this->report;
                    ########## Document TITLE ##########
    
                //    $pdf->DocTitle("UM cek", $filename, 60, 100);
                    // $pdf->DocTitle("TAHUN AJARAN", $data->tahun_ajaran_id, 60, 100);
                    // $pdf->DocTitle("YAYASAN MASJID DARUSSALAM TROPODO", "", 30, 100);

                    ########## PAGE TITLE ##########
                    $pdf->setRowXY(15, 10);
 
                    // $pdf->SetWidths(array(155));
                    $pdf->SetFont('Times','B', 15);
                    $pdf->SetAligns(array("C"));
                    $pdf->Row(array("YAYASAN MASJID DARUSSALAM TROPODO"), false);
                    $pdf->Row(array("LEMBAGA PENDIDIKAN AL FALAH DARUSSALAM TROPODO"), false);
                    $pdf->SetFont('Times','B', 9);
                    $pdf->SetAligns(array("C"));
                    $pdf->Row(array("Jl. Nusa Indah D-1 Wisma Tropodo, Waru, Sidoarjo (031) 8672828, 8664323"), false);

                    $pdf->addRowHeight(10);
                    $pdf->SetFont('Times','U', 12);
                    $pdf->SetAligns(array("C"));
                    $pdf->Row(array("PENGAJUAN UANG MUKA ANGGARAN"), false);
                    $pdf->SetFont('Times','B', 12);
                    $pdf->Row(array("NO. ".$data->uangmuka_no), false);
                    // $y_current = $pdf->y_pos-5;
                    // $y_height  = $pdf->getY() - $y_current;
                    // $pdf->Rect($pdf->x_pos, $y_current+1, 150, $y_height ); 
                    // $pdf->Rect($pdf->x_pos+(150), $y_current+1, 30, $y_height ); 

                    $pdf->addRowHeight(10);
                    $pdf->SetFont('Times','', 10);
                    $pdf->SetWidths(array(20, 5, 60));
                    $pdf->SetAligns(array("L","L","L"));
                    $pdf->Row(array("Kepada Yth ",":","BENDAHARA YMDT"), false);
                    $pdf->Row(array("Organisasi", ": ",$data->organisasi_mrapbs_id_name."/".$data->urusan_mrapbs_id_name), false);
                    $pdf->Row(array("Kegiatan", ": ",$data->kegiatan_mrapbs_id_name), false);


                    ########## HEADER ##########
                    $pdf->setRowXY(15, 80);
                    $pdf->SetWidths(array(40, 110, 30));
                    $pdf->SetAligns(array("L", "L", "L"));
                    $pdf->SetFont('Times','', 9);
                    $pdf->addRowHeight(1);


                    /* DETAIL ITEMS */
                    $details = uangmukadetail::detailPrintOut(trim($data->uangmuka_no));
                    $pdf->SetFont('Times','B', 8);                    
                    $pdf->SetWidths(array(20, 30, 15 ,50 ,10 ,15, 15 ,25));
                    $pdf->SetAligns(array("C", "C", "C","C","C","C","C","C"));
                    $pdf->Row(array("NO PENGAJUAN", "NO. RAB","KODE REK","RINCIAN ANGGARAN","VOL","SATUAN","TARIF","JUMLAH"), true);
                    $pdf->SetFont('Times','', 8);
                    $pdf->SetAligns(array("L", "L", "L", "L", "R", "R", "C", "R", "R"));
                    foreach ($details as $detail) 
                    {   $pdf->Row(array($detail->pengajuan_no, 
                                        $detail->rapbs_no, 
                                        $detail->coa_id, 
                                        $detail->coa_name,
                                        $detail->volume,
                                        $detail->satuan,
                                        number_format($detail->tarif, 0, ",", "."),
                                        number_format($detail->jumlah, 0, ",", ".")), true);
                    };
                    $pdf->SetWidths(array(155, 25));
                    $pdf->SetAligns(array("C", "R"));
                    $pdf->Row(array("T O T A L",number_format($data->total_biaya, 0, ",", ".")), true);

                    // TERBILANG
                    $pdf->addRowHeight(2);
                    $pdf->SetFont('Times','I', 9);
                    $pdf->SetWidths(array(25, 100));
                    $pdf->SetAligns(array("L", "L"));
                    $nilai = $data->total_biaya;
                    $lang_id = "id";
                    $spell_total = new \NumberFormatter($lang_id, \NumberFormatter::SPELLOUT);
                    $pdf->Row(array("TERBILANG : ","(".$spell_total->format($nilai)." rupiah )"), false);
               

                    /* TANDA TANGAN */
                    /* TANDA TANGAN */
                    $pdf->setRowXY(15, 187);
                    $pdf->SetFont('Times','B', 10);
                    $pdf->addRowHeight(20);
                    $pdf->SetWidths(array(60,60,60));
                    $pdf->SetAligns(array("C","C","C"));                    
                    $pdf->Row(array("Bendahara,","Kabid/Wakabid,","Penanggung Jawab,"), false);
                    $pdf->addRowHeight(20);
                    $pdf->Row(array("________________","_________________","__________________"), false);

                    /* NB */
                    $pdf->addRowHeight(20);
                    $pdf->SetWidths(array(180));
                    $pdf->SetAligns(array("L"));
                    $pdf->SetFont('Times','I', 9);
                    $pdf->Row(array("NB : "), false); 
                    $pdf->Row(array("Pengajuan Uang Muka Anggaran selambat-lambatnya 5(lima) hari sebelum pelaksanaan kegiatan."), false); 
                    $pdf->SetFont('Times','I', 8);
                    $pdf->SetWidths(array(180));
                    $pdf->SetAligns(array("R"));                    
                    $pdf->Row(array("Form KEU 2.02"), false);
                    $pdf->Output($filename.".pdf", "I");
                    exit;
            };
            break;
            case 4: // print xls
            {
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                foreach ($results as $result) {
                    $data[] = array(
                        $result->RapbsMaster_key_id,
                        $result->name,
                        Newrapbs::getStatus($result->status),
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Countries',
                    'Countries',
                    array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                    $data,
                    'A',
                    'G'
                );
                $this->report->download('xls');
            };
            break;
            case 10: // search detail data
            {   $this->records = Uangmukadetail::UangmukaRinciSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
            };
            break;
            case 705: // approve data dari menu approval pencairan
            {   
                $session = $this->request->session();
                $result = UangmukaMaster::uangmukaMasterApproval2(
                    $this->request,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);

            };
            break;
            case 707: // reject data
                {   
                    $session = $this->request->session();
                    $result = RapbsMaster::RapbsMasterReject2(
                        $this->request,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
    
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
    
                };
            break;

            case 99: // search detail data
                { 
                    $this->records = PengajuanMaster::ApprovalPengajuanDetailSearch($this->request);
                    if ($this->request->pt == 'pdf') {
                            $this->detail = PengajuanMaster::appumPrintOut($this->request);
                                // print_r($this->detail);exit;
                            $this->crudNewrapbs(3);
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
            break;
            case 100: // reject uang muka by kabid
                {
                        $session = $this->request->session();
                       
                        $head_data = json_decode(stripslashes($this->request->head));
                        //$catat = json_decode(stripslashes($this->request->catatan));
                        //print_r($catat);exit;
                        $result = UangmukaReject::RejectUangMuka(
                        
                        $this->request->catatan,
                        $head_data,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
    
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
            break;

        }
        return $result;
    }
}
