<?php

namespace App\Http\Controllers\Newanggaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Anggaran\UangmukaMaster;
use App\Models\Anggaran\UangmukaDetail;
use App\Models\Anggaran\PengajuanDetail;
use App\Models\Anggaran\PengajuanMaster;
use App\Models\Anggaran\UangmukaRealisasi;
use App\Models\Ppdb\TahunAjaran;
use Fpdf;

use DB;
class RealisasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {
        switch ($task) {   // routing data
            case 0: return $this->routing($substask);   break;
            // Realisasi
            case 1: return $this->crudRealisasi($substask);  break;
            // Realisasi App by Kabid
            case 2: return $this->crudAppRealisasi($substask);  break;
            // Realisasi App by Keuangan
            case 3: return $this->crudAppRealisasiKeu($substask);  break;

        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {
        switch ($subtask) {
            case 0: // realisasi page 
                {
                    $session = $this->request->session();
                    $tahunajaran= TahunAjaran::getPeriode($this->request);
                    $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
  
                    $result  = view('newanggaran/realisasi' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
        
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                };
            break;
            case 1: // approval realisasi page   
                {
                    $session = $this->request->session();
                    $tahunajaran= TahunAjaran::getPeriode($this->request);
                    $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
  
                    $result  = view('newanggaran/apprealisasi' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
        
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                };
            break;
            case 2: // approval realisasi page   
                {
                    $session = $this->request->session();
                    $tahunajaran= TahunAjaran::getPeriode($this->request);
                    $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
  
                    $result  = view('newanggaran/apprealisasikeu' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
        
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                };
            break;
        };
        return $result;
    }

    public function crudRealisasi($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = UangmukaRealisasi::UmRealisasiSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->records = UangmukaMaster::headerPrintOut($this->request);
                        $this->crudRealisasi(3);
                    } else {
                        $this->crudRealisasi(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
            {
                $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                $head_data = json_decode(stripslashes($this->request->head));
                $total = json_decode(stripslashes($this->request->total));
                $modi_data = json_decode(stripslashes($this->request->modi));
               // print_r($head_data);exit;
                $result = UangmukaRealisasi::UangmukaRealisasiSave(
                    $json_data,
                    $head_data,
                    $total,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 2: // delete data
            {
                $result = UangmukaRealisasi::uangmukarealisasiDelete($this->request->id); 
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 3: // print pdf
            {
                //  print_r($this->records);exit;
                $data = $this->records;
                // print_r($data);exit;
                $filename = "U_M";

                $this->create_report_pdf($this->request->session());
                $pdf = $this->report;
                ########## Document TITLE ##########

                ########## PAGE TITLE ##########
                $pdf->setRowXY(15, 10);

                $pdf->SetWidths(array(170));
                $pdf->SetFont('Times','B', 15);
                $pdf->SetAligns(array("C"));
                $pdf->addRowHeight(20);
                $pdf->Row(array("YAYASAN MASJID DARUSSALAM TROPODO"), false);
                $pdf->Row(array("LEMBAGA PENDIDIKAN AL FALAH DARUSSALAM TROPODO"), false);

                $pdf->Image('academy/img/core-img/logountuklaporan.png', 20, 40, 29, 24);
                $pdf->addRowHeight(10);
                $pdf->SetWidths(array(180));
                $pdf->SetFont('Times','U', 12);
                $pdf->SetAligns(array("C"));
                $pdf->Row(array("PERTANGGUNG JAWABAN UANG MUKA ANGGARAN"), false);
                $pdf->SetFont('Times','B', 12);
                $pdf->Row(array("NO. ".$data->uangmuka_no), false);
                $details = uangmukadetail::realisasidetailPrintOut(trim($data->uangmuka_no));
                $pdf->addRowHeight(10);
                $pdf->SetFont('Times','', 10);
                $pdf->SetWidths(array(30, 5, 80));
                $pdf->SetAligns(array("L","L","L"));
                $pdf->Row(array("Kepada Yth ",":","BENDAHARA YMDT"), false);
                $pdf->Row(array("Organisasi", ": ",$data->organisasi_mrapbs_id_name."/".$data->urusan_mrapbs_id_name), false);
                $pdf->Row(array("Sub.Keg", ": ",$data->sub_kegiatan_mrapbs_id), false);
                $pdf->Row(array("Realisasi Hasil", ": ",$data->realisasi_hasil), false);
             //   print_r($data->realisasi_hasil);exit;
                ########## HEADER ##########

                /* DETAIL ITEMS */
                
                $pdf->SetFont('Times','B', 8);
                $pdf->SetWidths(array( 65,30 ,50, 40));
                $pdf->SetAligns(array("C","C","C","C"));
                $pdf->Row(array("K E T E R A N G A N","VOLUME","JUMLAH","SELISIH"), true);                    
                $pdf->SetWidths(array(15 ,50 ,15 ,15, 25 ,25, 15, 25));
                $pdf->SetAligns(array("C","C","C","C","C","C","C","C"));
                $pdf->Row(array("Kd Rek","Rincian Anggaran","Rencana","Realisasi","Um","Realisasi","Vol","Um"), true);
                $pdf->SetFont('Times','', 8);
                $pdf->SetAligns(array("L", "L", "R", "R", "R", "R", "R", "R"));
                  
                // print_r($details);exit;
                foreach ($details as $detail) 
                {  
                    $acc_baru = $detail->correction_acc;
                    $acc_lama = $detail->coa_id;
                    if ($acc_baru == '') {$coa = $acc_lama ; } else {$coa = $acc_baru. "/"."P" ;};
                    $pdf->Row(array(
                        $coa, 
                        $detail->uraian,
                        number_format($detail->vol_ajuan, 0, ",", "."),
                        number_format($detail->total_vol, 0, ",", "."),
                        number_format($detail->vol_ajuan*$detail->tarif, 0, ",", "."),
                        number_format($detail->total_nota, 0, ",", "."),
                        number_format($detail->vol_ajuan-$detail->total_vol, 0, ",", "."),
                        number_format($detail->jum_ajuan-$detail->total_nota, 0, ",", "."),
                    ), true);
                };
                $pdf->SetWidths(array(95,25 ,25, 40));
                $pdf->SetAligns(array("C", "R", "R", "R"));
                $pdf->Row(array("T O T A L",number_format($data->total_biaya, 0, ",", "."),
                                            number_format($data->total_realisasi, 0, ",", "."),
                                            number_format($data->total_biaya-$data->total_realisasi, 0, ",", ".")
                                        ), true);

                // TERBILANG
                $pdf->addRowHeight(2);
                $pdf->SetFont('Times','I', 9);
                $pdf->SetWidths(array(25, 100));
                $pdf->SetAligns(array("L", "L"));
                $nilai = $data->total_realisasi;
                $lang_id = "id";
                $spell_total = new \NumberFormatter($lang_id, \NumberFormatter::SPELLOUT);
                $pdf->Row(array("Terbilang : ","(".$spell_total->format($nilai)." rupiah )"), false);
            
                /* TANDA TANGAN */
                $pdf->setRowXY(15, 187);
                $pdf->SetFont('Times','B', 10);
                $pdf->addRowHeight(20);
                $pdf->SetWidths(array(60,60,60));
                $pdf->SetAligns(array("C","C","C"));                    
                $pdf->Row(array("Bendahara,","KPA,","PJ.PK,"), false);
                $pdf->addRowHeight(20);
                $pdf->Row(array("________________","_________________","__________________"), false);
                $pdf->SetFont('Times','I', 8);
                $pdf->SetAligns(array("C","C","C"));
                $pdf->Row(array(" ,"," ,","( Paraf )"), false);
                /* NB */
                $pdf->addRowHeight(20);
                $pdf->SetWidths(array(180));
                $pdf->SetAligns(array("L"));
                $pdf->SetFont('Times','I', 9);
                $pdf->Row(array("NB : "), false); 
                $pdf->Row(array("Pertanggung Jawaban Uang Muka Anggaran selambat-lambatnya 7(tujuh) hari setelah pelaksanaan kegiatan."), false); 
                $pdf->SetFont('Times','I', 8);
                $pdf->SetWidths(array(180));
                $pdf->SetAligns(array("R"));                    
                $pdf->Row(array("Form KEU 2.03"), false);
                $pdf->Output($filename.".pdf", "I");
                exit;
            };
            break;
            case 4: // print xls
            {
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                foreach ($results as $result) {
                    $data[] = array(
                        $result->RapbsMaster_key_id,
                        $result->name,
                        Newrapbs::getStatus($result->status),
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Countries',
                    'Countries',
                    array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                    $data,
                    'A',
                    'G'
                );
                $this->report->download('xls');
            };
            break;
            case 10: // search detail data
            {   $this->records = Uangmukadetail::UangmukaRinciSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
            };
            break;
            case 11: // search detail data nota
                {   $this->records = UangmukaRealisasi::UangmukaNotaSearch1($this->request);
                    $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                };
            break;
            case 705: // approval data realisasi
            {   
            };
            break;
            case 707: // reject data
                {   
                };
            break;
            case 99: // search detail data
                { 
                $this->records = PengajuanMaster::ApprovalPengajuanDetailSearch($this->request);
                if ($this->request->pt == 'pdf') {
                            $this->detail = PengajuanMaster::appumPrintOut($this->request);
                            // print_r($this->detail);exit;
                        $this->crudRealisasi(3);
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 101: // input realisasi biaya
                {
                    $session = $this->request->session();
                    $result = UangmukaMaster::RealisasiHasil(
                    $this->request->uangmuka_no, 
                    $this->request->realisasi_hasil,
                    
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
        break;
        };
        return $result;
    }

    public function crudAppRealisasi($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = UangmukaRealisasi::UmAppRealisasiSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->records = UangmukaMaster::headerPrintOut($this->request);
                        $this->crudRealisasi(3);
                    } else {
                        $this->crudRealisasi(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
            {
            };
            break;
            case 2: // delete data
            {
            };
            break;
            case 3: // print pdf
            {

            };
            break;
            case 4: // print xls
            {
            };
            break;
            case 10: // search detail data
            {   $this->records = Uangmukadetail::UangmukaRinciSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                );
            };
            break;
            case 11: // search detail data nota
                {   $this->records = UangmukaRealisasi::UangmukaNotaSearch1($this->request);
                    $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                };
            break;
            case 12: // approval realisasi
                {   
                    $session = $this->request->session();
                    $result = UangmukaDetail::RealisasiTotalSave(
                        $this->request->uangmuka_no, 
                        $this->request->biaya_id, 
                        $this->request->total_nota,
                        $this->request,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 705: // approval realisasi kabid
            {   
                $session = $this->request->session();
                $result = UangmukaMaster::approvalRealisasi(
                    $this->request,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 707: // reject data
            {   
            };
            break;
            case 99: // search detail data
            { 
            };
            break;
            case 101: // input realisasi biaya
            {
            };
            break;
            };
        return $result;
    }
    
    public function crudAppRealisasiKeu($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = UangmukaRealisasi::UmAppRealisasiKeuSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->records = UangmukaMaster::headerPrintOut($this->request);
                        $this->crudRealisasi(3);
                    } else {
                        $this->crudRealisasi(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
            {
            };
            break;
            case 2: // delete data
            {
            };
            break;
            case 3: // print pdf
            {

            };
            break;
            case 4: // print xls
            {
            };
            break;
            case 10: // search detail data
            {   $this->records = Uangmukadetail::UangmukaRinciSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                );
            };
            break;
            case 11: // search detail data nota
                {   $this->records = UangmukaRealisasi::UangmukaNotaSearch1($this->request);
                    $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                };
            break;
            case 12: // approval realisasi
                {   
                    $session = $this->request->session();
                    $result = UangmukaDetail::RealisasiTotalSave(
                        $this->request->uangmuka_no, 
                        $this->request->biaya_id, 
                        $this->request->total_nota,
                        $this->request,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 705: // approval realisasi keuangan
            {   
                $session = $this->request->session();
                $result = UangmukaMaster::approvalRealisasiKeu(
                    $this->request,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 707: // reject data
            {   
            };
            break;
            case 99: // search detail data
            { 
            };
            break;
            case 101: // input realisasi biaya
                {
                    $session = $this->request->session();
                    $result = UangmukaDetail::KoreksiAcc(
                    // $this->request->uangmuka_no,    
                    $this->request->id, 
                    $this->request->koreksiacc,
                    $this->request->koreksiname,
                        
                    
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
        };
        return $result;
    }

}
