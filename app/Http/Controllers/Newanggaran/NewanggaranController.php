<?php

namespace App\Http\Controllers\Newanggaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Anggaran\RapbsMaster;
use App\Models\Anggaran\RapbsDetail;
use App\Models\Ppdb\TahunAjaran;
use Fpdf;




class NewanggaranController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {
        switch ($task) {   // routing data
            case 0: return $this->routing($substask);   break;
            // RAPBS
            case 1: return $this->crudNewrapbs($substask);  break;
            // Detail biaya RAPBS
            case 2: return $this->crudRapbsMaster($substask);    break;

        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {
        switch ($subtask) {
            case 0: // anggaran page 
                {
                    $session = $this->request->session();
                    $tahunajaran= TahunAjaran::getPeriode($this->request);
                    $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
                  //  print_r($tahunanggaran->name);exit;
                    $result  = view('newanggaran/newrapbs' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                };
                break;
        };
        return $result;
    }

    public function crudNewrapbs($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            { 
                $session = $this->request->session();
                $this->records = RapbsMaster::RapbsMasterSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                       $this->records = RapbsMaster::headerusulanPrintOut($this->request); 
                    //  print_r($this->records);exit; 
                        $this->crudNewrapbs(3);
                    } else {
                        $this->crudNewrapbs(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
            {
                $session = $this->request->session();

                $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                $modi_data = json_decode(stripslashes($this->request->modi));

                // ($data, $json_data, $site, $user_id, $company_id)
                $result = RapbsMaster::RapbsMasterSave(
                    $head_data[0],
                    $json_data,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company")
                );
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 2: // delete data master
            {
                $result = RapbsMaster::RapbsMasterDelete($this->request);

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 3: // print pdf
            {
                //print_r($this->request);exit;
                // $data = RapbsMaster::headerOut($this->request);

                // print_r($data);exit;
                $data = $this->records;
                if ($data->approve_status_2 == 1) { $title =  "RENCANA KERJA DAN ANGGARAN "; } 
                else {$title ="USULAN RENCANA KERJA DAN ANGGARAN ";};
                $filename = "RENCANA".$data->rapbs_no;

                $this->create_report_pdf($this->request->session());
                $pdf = $this->report;
                ########## Document TITLE ##########

//                    $pdf->DocTitle($title, $filename, 60, 100);
                // $pdf->DocTitle("TAHUN AJARAN", $data->tahun_ajaran_id, 60, 100);

                ########## PAGE TITLE ##########
                $pdf->setRowXY(15, 30);
                $pdf->SetWidths(array(140, 40));
                $pdf->SetFont('Times','B', 10);
                $pdf->SetAligns(array("C", "R"));
                $pdf->Row(array($title,""), false);
                $pdf->Row(array(" YAYASAN MASJID DARUSALLAM","".$data->rapbs_no), false);
                $pdf->Row(array("Tahun : ".$data->tahun_ajaran_id," "), false);
                $y_current = $pdf->y_pos-5;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current+1, 145, $y_height ); 
                $pdf->Rect($pdf->x_pos+(145), $y_current+1, 35, $y_height ); 

                ########## HEADER ##########
                $pdf->setRowXY(15, 46);
                $pdf->SetWidths(array(40, 105, 30));
                $pdf->SetAligns(array("L", "L", "L"));
                $pdf->SetFont('Times','', 9);
                $pdf->addRowHeight(1);
                $pdf->Row(array("ORGANISASI", ": ".strtoupper($data->organisasi_mrapbs_id_name)), false);
                $pdf->Row(array("URUSAN", ": ".$data->urusan_mrapbs_id_name), false);
                $pdf->Row(array("PROGRAM", ": ".$data->program_mrapbs_id_name ), false);
                $pdf->Row(array("KEGIATAN", ": ".$data->kegiatan_mrapbs_id_name), false);
                $pdf->Row(array("SUB KEGIATAN", ": ".$data->sub_kegiatan_mrapbs_id), false);
                $pdf->Row(array("JUMLAH Tahun n-1", ": ".number_format($data->jumlahn, 0, ",", ".")), false); 
                $pdf->Row(array("JUMLAH Tahun n", ": ".number_format($data->total_biaya, 0, ",", ".")), false);
                $pdf->SetAligns(array("L", "L", "L", "L"));
                $pdf->Row(array("SUMBER DANA", ": ".$data->sumberdana_id_name,"Bln : ".$data->tgl_mulai), false);
                // rectangle boxs
                $y_current = $pdf->y_pos+1;
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current, 35, $y_height ); 
                $pdf->Rect($pdf->x_pos+(35), $y_current, 145, $y_height ); 

                $pdf->SetWidths(array(190));
                $pdf->SetAligns(array("C")); 
                $pdf->SetFont('Times','', 8);                   
                $pdf->Row(array("INDIKATOR DAN TOLAK UKUR KINERNA BELANJA LANGSUNG"), false);
                // rectangle boxs
                $y_height  = $pdf->getY() - $y_current;
                $pdf->Rect($pdf->x_pos, $y_current, 180, $y_height );                     
                $hasil_data = ($data->tgt_masukan +$data->tgt_keluaran)/2;
                $pdf->SetWidths(array(35, 110, 35));
                $pdf->SetAligns(array("C", "C", "C"));
                $pdf->SetFont('Times','B', 8);
                $pdf->Row(array("INDIKATOR", "TOLOK UKUR","TARGET KINERJA"), true);
                $pdf->SetFont('Times','', 8);
                $pdf->SetAligns(array("L", "C", "R"));
                $pdf->Row(array("MASUKAN",$data->masukan,number_format($data->tgt_masukan, 0, ",", ".")), true);
                $pdf->Row(array("KELUARAN",$data->keluaran,number_format($data->tgt_keluaran, 0, ",", ".")), true);
                $pdf->Row(array("HASIL",$data->hasil,$hasil_data), true);

                $pdf->SetWidths(array(35, 145));
                $pdf->SetAligns(array("L", "C"));
                $pdf->Row(array("SASARAN KEGIATAN",$data->sasaran), true);

                $pdf->SetWidths(array(180));
                $pdf->SetAligns(array("C"));                    
                $pdf->Row(array("RINCIAN ANGGARAN BELANJA LANGSUNG MENURUT PROGRAM DAN PER KEGIATAN ORGANISASI"), true);

                /* DETAIL ITEMS */
                $details = rapbsdetail::detailPrintOut(trim($data->rapbs_no));
                $pdf->SetFont('Times','B', 8);                    
                $pdf->SetWidths(array(35, 60, 15,15,20,35));
                $pdf->SetAligns(array("C", "C", "C","C","C","C"));
                $pdf->Row(array("KODE REKENING", "URAIAN","VOLUME","SATUAN","TARIF","JUMLAH"), true);
                $pdf->SetFont('Times','', 8);
                $pdf->SetAligns(array("L", "L", "R", "C", "R", "R", "R"));
                foreach ($details as $detail) 
                {   $pdf->Row(array($detail->coa_id, 
                                    $detail->uraian,
                                    number_format($detail->volume, 0, ",", "."),
                                    $detail->satuan,
                                    number_format($detail->tarif, 0, ",", "."),
                                    number_format($detail->jumlah, 0, ",", ".")), true);
                };
                $pdf->SetWidths(array(145, 35));
                $pdf->SetAligns(array("C", "R"));
                $pdf->Row(array("JUMLAH",number_format($data->total_biaya, 0, ",", ".")), true);

                /* TANDA TANGAN */
                $pdf->SetFont('Times','B', 10);
                $pdf->addRowHeight(15);
                $pdf->SetWidths(array(180));
                $pdf->SetAligns(array("R"));                    
                $pdf->Row(array("KETUA BIDANG"), false);
                $pdf->addRowHeight(20);
                // $pdf->Row(array($data->pic_id), false);

                $pdf->addRowHeight(75);
                $pdf->SetFont('Times','I', 8);
                $pdf->SetWidths(array(180));
                $pdf->SetAligns(array("L"));                    
                $pdf->Row(array("Form RKA 2.1"), false);
                $pdf->Output($filename.".pdf", "I");
                exit;
            };
            break;
            case 4: // print xls
            {
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                foreach ($results as $result) {
                    $data[] = array(
                        $result->RapbsMaster_key_id,
                        $result->name,
                        Newrapbs::getStatus($result->status),
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Countries',
                    'Countries',
                    array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                    $data,
                    'A',
                    'G'
                );
                $this->report->download('xls');
            };
            break;
            case 10: // search detail data
            {   $this->records = RapbsDetail::RapbsDetailSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
            };
            break;
            case 12: // delete data master
            {
                $result = RapbsDetail::RapbsDetailBiayaDelete($this->request);

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
    
            case 705: // approve data
            {   
                $session = $this->request->session();
                $result = RapbsMaster::RapbsMasterApproval(
                    $this->request,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);

            };
            break;
            
        };
        return $result;
    }

    public function crudRapbsMaster($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = RapbsMaster::RapbsMasterSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->crudNewrapbs(3);
                        } else {
                            $this->RapbsMaster(4);
                        };
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
                break;
            case 1: // save data
                {
                    $session = $this->request->session();
                     $head_data = json_decode(stripslashes($this->request->head));
                    $json_data = json_decode(stripslashes($this->request->json));
                    // $type = json_decode(stripslashes($this->request->type));
                    $result = RapbsMaster::RapbsMasterSave(
                         $head_data,
                        $json_data,
                         $type,
                        // $session->get("site"),
                        $session->get("user_id")
                        // $session->get("company_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2: // delete data
                {
                    $result = RapbsMaster::RapbsMasterDelete($this->request);
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                    $results = $this->records;
                    $data = array();
                    $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                    foreach ($results as $result) {
                        $data[] = array(
                            $result->RapbsMaster_key_id,
                            $result->name,
                            Newrapbs::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                        );
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Master Countries',
                        'Countries',
                        array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                        $data,
                        'A',
                        'G'
                    );
                    $this->report->download('xls');
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        RapbsMaster::RapbsMasterSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        };
        return $result;
    }

}
