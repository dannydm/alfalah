<?php

namespace App\Http\Controllers\Newanggaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Anggaran\RapbsMaster;
use App\Models\Anggaran\RapbsDetail;
use App\Models\Anggaran\IncomeMaster;
use App\Models\Anggaran\IncomeDetail;
use App\Models\Anggaran\Monitoring;
use App\Models\Ppdb\TahunAjaran;


class NewrapbskeuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {
        switch ($task) {   // routing data
            case 0: return $this->routing($substask);   break;
            // RAPBS
            case 1: return $this->crudNewrapbskeu($substask);  break;
            // Detail biaya RAPBS
            case 2: return $this->crudRapbsMaster($substask);    break;
             // Detail biaya RAPBS
            case 3: return $this->crudSahRapbs($substask);    break;
             // Detail Pendapatan
            case 4: return $this->crudBudgeting($substask);    break;
            // BENDAHARA RAPBS VIEW
            case 5: return $this->crudNewrapbsbend($substask);  break;
            // Detail biaya RAPBS

        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {
        switch ($subtask) {
            case 0: // anggaran page 
                {
                    $session = $this->request->session();
                    $tahunajaran= TahunAjaran::getPeriode($this->request);
                    $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
         
                    $result  = view('newanggaran/newrapbskeu' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
   
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                };
                break;

                case 1: // bendahara page 
                    {
                        $session = $this->request->session();
                        $tahunajaran= TahunAjaran::getPeriode($this->request);
                        $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
             
                        $result  = view('newanggaran/newrapbsbend' . config('app.system_skin'))
                                ->with("USER_ID", $session->get("user_id"))
                                ->with("USERNAME", $session->get("username"))
                                ->with("USERSITE", $session->get("site"))
                                ->with("TABID", $this->request->get("tabId"))
                                ->with("TAHUNANGGARAN", $tahunanggaran->name)
                                ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
       
                                ->with("TAHUNAJARAN", $tahunajaran->name)
                                ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                    };
                    break;
        };
        return $result;
    }
 
    public function crudNewrapbskeu($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = RapbsMaster::PenetapanSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->crudNewrapbskeu(3);
                    } else {
                        $this->crudNewrapbskeu(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
            {
                $session = $this->request->session();
                $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                $modi_data = json_decode(stripslashes($this->request->modi));

                $result = RapbsMaster::RapbsMasterSave(
                    $head_data[0],
                    $json_data,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 2: // delete data
            {
                $result = RapbsMaster::RapbsMasterDelete($this->request);
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 3: // print pdf
            {};
            break;
            case 4: // print xls
            {
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                foreach ($results as $result) {
                    $data[] = array(
                        $result->RapbsMaster_key_id,
                        $result->name,
                        Newrapbs::getStatus($result->status),
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Countries',
                    'Countries',
                    array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                    $data,
                    'A',
                    'G'
                );
                $this->report->download('xls');
            };
            break;
            case 10: // search detail data
            {   $this->records = RapbsDetail::RapbsDetailSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
            };
            break;
            case 705: // approve data
            {   
                $session = $this->request->session();
                $result = RapbsMaster::PenetapanApprove(
                    $this->request,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);

            };
            break;
            
        };
        return $result;
    }

    public function crudRapbsMaster($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = RapbsMaster::RapbsMasterSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->RapbsMaster(3);
                        } else {
                            $this->RapbsMaster(4);
                        };
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
                break;
            case 1: // save data
                {
                    $session = $this->request->session();
                     $head_data = json_decode(stripslashes($this->request->head));
                    $json_data = json_decode(stripslashes($this->request->json));
                    // $type = json_decode(stripslashes($this->request->type));
                    $result = RapbsMaster::RapbsMasterSave(
                         $head_data,
                        $json_data,
                         $type,
                        // $session->get("site"),
                        $session->get("user_id")
                        // $session->get("company_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2: // delete data
                {
                    $result = RapbsMaster::RapbsMasterDelete($this->request);
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                    $results = $this->records;
                    $data = array();
                    $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                    foreach ($results as $result) {
                        $data[] = array(
                            $result->RapbsMaster_key_id,
                            $result->name,
                            Newrapbs::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                        );
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Master Countries',
                        'Countries',
                        array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                        $data,
                        'A',
                        'G'
                    );
                    $this->report->download('xls');
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        RapbsMaster::RapbsMasterSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        };
        return $result;
    }

    public function crudSahRapbs($subtask)
    {
        switch ($subtask) {
            case 0: // search data pengesahan
                { 
                    $session = $this->request->session(); 
                    $this->records = RapbsMaster::SahRapbsSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                
                );
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->crudSahRapbs(3);
                        } else {
                            $this->crudSahRapbs(4);
                        };
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
                break;
            case 1: // search data pencairan
                {
                    {
                        $session = $this->request->session(); 
                        $this->records = UangmukaMaster::MonitoringcairSearch($this->request, 
                        $session->get("user_id"),
                        $session->get("level")
                    
                    );
                        if ($this->request->pt) {
                            if ($this->request->pt == 'pdf') {
                                $this->RapbsMaster(3);
                            } else {
                                $this->RapbsMaster(4);
                            };
                        } else {
                            $result = $this->rsJson(
                                $this->records,
                                $this->request->limit,
                                $this->request->start
                            );
                        };
                    };
    
                };
                break;
            case 2: // delete data
                {
                };
                break;
            case 3: // print pdf
                {
                    $data = RapbsMaster::SahRapbsPrint($this->request);
                    $filename = "U_M";

                    $this->create_report_pdf_kwitansi($this->request->session());
                    $pdf = $this->report;
                    $pdf->setRowXY(50, 25);

                    $pdf->SetWidths(array(200));
                    $pdf->SetFont('Times','B', 12);
                    $pdf->SetAligns(array("C"));
                    $pdf->Row(array("YAYASAN MASJID DARUSSALAM TROPODO"), false);
                    $pdf->Row(array("LEMBAGA PENDIDIKAN AL FALAH DARUSSALAM TROPODO"), false);
                    $pdf->Row(array($data[0]->urusan_mrapbs_id_name), false);
                    $pdf->Output($filename.".pdf", "I");
                    exit;
                };
                break;
            case 4: // print xls
                {
                    $results = $this->records;
                    $data = array();
                    $data[] = array("NO APBY", "URUSAN", "PROGRAM", "KEGIATAN", "SUB KEGIATAN", "SUMBER DANA", 
                                    "KELUARAN", "HASIL", "SASARAN", "JUMLAH THN N", "JUMLAH TAHUN KE N", "PERSENTASE");
                    foreach($results as $result) {
                            $data[] = array(
                                $result->rapbs_no, 
                                $result->urusan_mrapbs_id_name, 
                                $result->program_mrapbs_id_name, 
                                $result->kegiatan_mrapbs_id_name,
                                $result->sub_kegiatan_mrapbs_id,
                                $result->sumberdana_id_name,
                                $result->keluaran, 
                                $result->hasil, 
                                $result->sasaran,
                                $result->jumlahn,
                                // $result->total_biaya,
                                $result->
                                setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER)                                  
                                
                        );
                    };
                            
                        $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'REKAP APBY',
                        'BERDASAR',
                        array("A" => 10, "B" => 10, "C" => 25, "D" => 25, "E" => 15, "F" => 15,
                            "G" => 15, "H" => 15, "I" => 50, "J" => 15, "K" => 15, "L" => 15),
                        $data,
                        'A',
                        'L'
                    );
                    $this->report->download('xls');  
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        RapbsMaster::RapbsMasterSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        }; 
        return $result;
    }

    public function crudBudgeting($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = BudgetingMaster::BudgetingSearch(
                $this->request, 
                $session->get("user_id"),
                $session->get("level")
            );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->crudBudgeting(3);
                    } else {
                        $this->crudBudgeting(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
                {
                    $session = $this->request->session();
                    $head_data = json_decode(stripslashes($this->request->head));
                    $json_data = json_decode(stripslashes($this->request->json));
                    $modi_data = json_decode(stripslashes($this->request->modi));
                    // print_r($head_data);exit;
                    $result = BudgetingMaster::BudgetingMasterSave(
                        $head_data[0],
                        $json_data,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                    };
                break;
            case 2: // delete data
            {
                $result = BudgetingMaster::BudgetingMasterDelete($this->request);
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 3: // print pdf
            {};
            break;
            case 4: // print xls
            {
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                foreach ($results as $result) {
                    $data[] = array(
                        $result->RapbsMaster_key_id,
                        $result->name,
                        Newrapbs::getStatus($result->status),
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Countries',
                    'Countries',
                    array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                    $data,
                    'A',
                    'G'
                );
                $this->report->download('xls');
            };
            break;
            case 9: // search detail data
                {   $this->records = IncomeDetail::BudgetingAppSearch($this->request);
                    $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                };
            break;
            case 10: // search detail data
                {   $this->records = IncomeDetail::IncomeAppSearch($this->request);
                    $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                };
            break;
            case 705: // approve data kabid
            {   

            };
            break;
            case 99: // search detail data
                { 
                };
            break;
        };
        return $result;
    }

    public function crudNewrapbsbend($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = Monitoring::BendaharaSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->crudNewrapbsbend(3);
                    } else {
                        $this->crudNewrapbsbend(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 3: // print pdf
            {};
            break;
            case 4: // print xls
            {
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                foreach ($results as $result) {
                    $data[] = array(
                        $result->RapbsMaster_key_id,
                        $result->name,
                        Newrapbs::getStatus($result->status),
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Countries',
                    'Countries',
                    array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                    $data,
                    'A',
                    'G'
                );
                $this->report->download('xls');
            };
            break;
            case 10: // search detail data
            {   $this->records = RapbsDetail::RapbsDetailSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
            };
            break;
            case 705: // approve data
            {   
                $session = $this->request->session();
                $result = RapbsMaster::PenetapanApprove(
                    $this->request,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);

            };
            break;
            
        };
        return $result;
    }

}
