<?php

namespace App\Http\Controllers\Newanggaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Anggaran\UangmukaMaster;
use App\Models\Anggaran\UangmukaDetail;
use App\Models\Anggaran\PengajuanDetail;
use App\Models\Anggaran\PengajuanMaster;
use App\Models\Ppdb\TahunAjaran;
use Fpdf;
use DB;

class PencairanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {
        switch ($task) {   // routing data
            case 0: return $this->routing($substask);   break;
            // RAPBS
            case 1: return $this->crudPencairan($substask);  break;
            // monitoring biaya
            case 2: return $this->crudPencairanmonitoring_biaya($substask);  break;

        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {
        switch ($subtask) {
            case 0: // anggaran page 
                {
                    $session = $this->request->session();
                    $tahunajaran= TahunAjaran::getPeriode($this->request);
                    $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
         
                    $result  = view('newanggaran/pencairan' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
        
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                };
                break;
        };
        return $result;
    }

    public function crudPencairan($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = UangmukaMaster::PencairanSearch($this->request,  
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        // $this->records = UangmukaMaster::headerPrintOut($this->request);
                        $this->crudPencairan(3);
                    } else {
                        $this->crudPencairan(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
            {
                $session = $this->request->session();
                $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                $modi_data = json_decode(stripslashes($this->request->modi));

                $result = PengajuanMaster::PengajuanMasterSave(
                    $head_data[0],
                    $json_data,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 2: // delete data
            {
                $result = PengajuanMaster::PengajuanMasterDelete($this->request);
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 3: // print pdf
            {

                    // print_r($this->records);exit;
                    $data = UangmukaMaster::hduangmukaPrintOut($this->request);
                    // print_r($data);exit;
                    // $data = PengajuanMaster::ApprovalPengajuanDetailSearch($this->request);
                    
                    // $title = "RENCANA KERJA DAN ANGGARAN YAYASAN MASJID DARUSALLAM";
                    $filename = "U_M";
    
                    $this->create_report_pdf($this->request->session());
                    // $this->create_report_pdf_kwitansi($this->request->session());
                    $pdf = $this->report;
                    ########## Document TITLE ##########

                    // $pdf->addRowHeight(15);
                    // $pdf->DocTitle("UM", $filename, 60, 100);
                    // $pdf->DocTitle("TAHUN AJARAN", $data->tahun_ajaran_id, 60, 100);
                    // $pdf->DocTitle("YAYASAN MASJID DARUSSALAM TROPODO", "", 30, 100);

                    ########## PAGE TITLE ##########
                    $pdf->setRowXY(40, 25);
 
                    $pdf->SetWidths(array(155));
                    $pdf->SetFont('Times','B', 12);
                    $pdf->SetAligns(array("C"));
                    $pdf->Row(array("LEMBAGA PENDIDIKAN AL FALAH DARUSSALAM TROPODO"), false);
                    $pdf->SetFont('Times','B', 9);
                    $pdf->SetAligns(array("C"));
                    $pdf->Row(array("TANDA TERIMA"), false);

                    $pdf->setRowXY(15, 35);
                    // $pdf->addRowHeight(5);
                    $pdf->SetFont('Times','', 10);
                    $pdf->SetWidths(array(22, 5, 100));
                    $pdf->SetAligns(array("L","L","L"));
                    $pdf->Row(array("Organisasi", ": ",$data->organisasi_mrapbs_id_name."/".$data->urusan_mrapbs_id_name), false);
                    $pdf->Row(array("Sub.Kegiatan", ": ",$data->sub_kegiatan_mrapbs_id), false);

                    ########## HEADER ##########
                    $tgl_cetak = strtotime($data->approve_date_3);
                    $pdf->setRowXY(145, 35);
                    $pdf->SetFont('Times','', 10);
                    $pdf->SetWidths(array(20, 5, 100));
                    $pdf->SetAligns(array("L","L","L"));
                    $pdf->Row(array("No. Piutang", ": ",$data->uangmuka_no), false);
                    $pdf->Row(array("Tanggal",":",date("m-F-Y",$tgl_cetak)),false);

                    // $pdf->Row(array("No. Nota", ": "," "), false);

                    /* DETAIL ITEMS */
                    
                    $pdf->setRowXY(15, 50);
                    $details = uangmukadetail::detailPrintOut(trim($data->uangmuka_no));
                    $pdf->SetFont('Times','B', 8);                    
                    $pdf->SetWidths(array(20, 30, 15 ,50 ,10 ,15, 15 ,25));
                    $pdf->SetAligns(array("C", "C", "C","C","C","C","C","C"));
                    $pdf->Row(array("NO PENGAJUAN", "NO. RAB","KODE REK","KETERANGAN","VOL","SATUAN","TARIF","JUMLAH"), true);
                    $pdf->SetFont('Times','', 8);
                    $pdf->SetAligns(array("L", "L", "L", "L", "R", "R", "C", "R", "R"));
                    foreach ($details as $detail) 
                    {   $pdf->Row(array($detail->pengajuan_no, 
                                        $detail->rapbs_no, 
                                        $detail->coa_id, 
                                        $detail->uraian,
                                        $detail->vol_ajuan,
                                        $detail->satuan,
                                        number_format($detail->tarif, 0, ",", "."),
                                        number_format($detail->jum_ajuan, 0, ",", ".")), true);
                    };
                    $pdf->SetWidths(array(155, 25));
                    $pdf->SetAligns(array("C", "R"));
                    $pdf->Row(array("T O T A L",number_format($data->total_biaya, 0, ",", ".")), true);

                    // TERBILANG
                    $pdf->addRowHeight(2);
                    $pdf->SetFont('Times','I', 9);
                    $pdf->SetWidths(array(25, 100));
                    $pdf->SetAligns(array("L", "L"));
                    $nilai = $data->total_biaya;
                    $lang_id = "id";
                    $spell_total = new \NumberFormatter($lang_id, \NumberFormatter::SPELLOUT);
                    $pdf->Row(array("Terbilang : ","(".$spell_total->format($nilai)." rupiah )"), false);
               
                    /* TANDA TANGAN */
                     $pdf->addRowHeight(5);
                    //  $y_current = $pdf->y_pos+60;
                    $y_height  = $pdf->getY();
                    $pdf->Rect($pdf->x_pos,$y_height, 130, 30 ); // letak horisontal ,letak vertical , lebar ,tinggi, kotak kiri
                    $pdf->Rect($pdf->x_pos+(130), $y_height, 50, 30 ); //kotak kanan

                    // $pdf->setRowXY(17, 100);
                    $pdf->SetFont('Times','', 10);
                    $pdf->SetWidths(array(20, 5, 100));
                    $pdf->SetAligns(array("L","L","L"));
                    if ($data->jenis_byr == 1) { $jenis_byr ="TUNAI"; } 
                    else {$jenis_byr = "TRANSFER";};
    
                    $pdf->Row(array("Jenis Bayar", ": ",$jenis_byr), false); 
                    $pdf->Row(array("Catatan", ": ",$data->catatan), false);

                    

                    // $pdf->setRowXY(145, 100);
                    $x_height  = $pdf->getX();
                    $pdf->setRowXY($pdf->x_pos+(130), $y_height);
                    $pdf->SetFont('Times','', 10);
                    $pdf->SetWidths(array(50));
                    $pdf->SetAligns(array("C"));
                    $pdf->Row(array("Penerima"),true);
                    $tgl_cetak = strtotime($data->approve_date_3);
                    // $pdf->Row(array(date("m-F-Y",$tgl_cetak)),false);
                       
                   
    //                    $pdf->Row(array("Form KEU 2.02"), false);
                    $pdf->Output($filename.".pdf", "I");
                    exit;

    
            };
            break;
            case 4: // print xls
            {
               
            };
            break;
                break;
            case 10: // search detail data
            {   $this->records = Uangmukadetail::UangmukaRinciSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
            };
            break;
            case 100: // reject kegiatan kabid
                {
                        $session = $this->request->session();
                        // $result= PengajuanDetail::KegiatanReject(
                        $result = UangmukaMaster::pencairanApproval(
                        $this->request->uangmuka_no, 
                        $this->request->jenis_byr,
                        $this->request->catatan,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
    
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
            break;
            case 705: // approve data dari menu pencairan
            {   
                $session = $this->request->session();
                $result = UangmukaMaster::uangmukaMasterApproval3(
                    $this->request,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);

            };
            break;
            case 707: // reject data
                {   
                    $session = $this->request->session();
                    $result = RapbsMaster::RapbsMasterReject2(
                        $this->request,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
    
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
    
                };
                break;

                case 99: // search detail data
                        { 
                       $this->records = PengajuanMaster::ApprovalPengajuanDetailSearch($this->request);
                        if ($this->request->pt == 'pdf') {
                                 $this->detail = PengajuanMaster::appumPrintOut($this->request);
                                    // print_r($this->detail);exit;
                                $this->crudNewrapbs(3);
                        } else {
                            $result = $this->rsJson(
                                $this->records,
                                $this->request->limit,
                                $this->request->start
                            );
                        };
                    };
                    break;
        }
        return $result;
    }

    public function crudPencairanmonitoring_biaya($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $session = $this->request->session(); 
                    $this->records = UangmukaMaster::Pencairan_MonitoringbiayaSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                
                );
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->RapbsMaster(3);
                        } else {
                            $this->RapbsMaster(4);
                        };
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
                break;
            case 1: // save data
                {
                    {   $this->records = Uangmukadetail::UangmukaRinciMonitoringSearch($this->request);
                        $result = $this->rsJson(
                                $this->records,
                                $this->request->limit,
                                $this->request->start 
                            );
                    };
            
                };
                break;
            case 2: // delete data
                {
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        RapbsMaster::RapbsMasterSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        };
        return $result;
    }
}
