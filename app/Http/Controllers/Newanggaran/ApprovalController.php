<?php

namespace App\Http\Controllers\Newanggaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Anggaran\PengajuanMaster;
use App\Models\Anggaran\PengajuanDetail;
use App\Models\Anggaran\PengajuanReject;

use App\Models\Ppdb\TahunAjaran;


class ApprovalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {
        switch ($task) {   // routing data
            case 0: return $this->routing($substask);   break;
            // APPROVAL KABID
            case 1: return $this->crudNewrapbs($substask);  break;
            // Detail PENGAJUAN
            case 2: return $this->crudRapbsMaster($substask);    break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {
        switch ($subtask) {
            case 0: // anggaran page 
                {
                    $session = $this->request->session();
                    $tahunajaran= TahunAjaran::getPeriode($this->request);
                    $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
          
                    $result  = view('newanggaran/approval' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                };
                break;
        };
        return $result;
    }

    public function crudNewrapbs($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = PengajuanMaster::ApprovalKabidSearch(
                $this->request, 
                $session->get("user_id"),
                $session->get("level")
            );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->crudNewrapbs(3);
                    } else {
                        $this->crudNewrapbs(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
            {
                $session = $this->request->session();
                $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                $modi_data = json_decode(stripslashes($this->request->modi));

                $result = RapbsMaster::RapbsMasterSave(
                    $head_data[0],
                    $json_data,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 2: // reject data
            {
                $result = PengajuanMaster::ApprovalMasterDelete($this->request);
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 3: // print pdf
            {};
            break;
            case 4: // print xls
            {
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                foreach ($results as $result) {
                    $data[] = array(
                        $result->RapbsMaster_key_id,
                        $result->name,
                        Newrapbs::getStatus($result->status),
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Countries',
                    'Countries',
                    array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                    $data,
                    'A',
                    'G'
                );
                $this->report->download('xls');
            };
            break;
            case 10: // search detail data
            {   $this->records = PengajuanDetail::KegiatanDetailSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
            };
            break;
            case 705: // approve data kabid
            {   
                $session = $this->request->session();
                $result = PengajuanMaster::PengajuanMasterApprovalkabid( 
                    $this->request,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);

            };
            break;
            case 707: // reject data 
                {   
                    $session = $this->request->session();
                    $result = PengajuanMaster::PengajuanMasterReject2(
                        $this->request,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
    
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
    
                };
                break;
                case 99: // search detail data
                    {
                        $this->records = PengajuanMaster::ApprovalPengajuanDetailSearch($this->request);
                        if ($this->request->pt == 'pdf') {
                                 $this->detail = PengajuanMaster::KegiatanPrintOut($this->request);
  //                                  print_r($this->records);exit;
                                $this->crudNewrapbs(3);
                        } else {
                            $result = $this->rsJson(
                                $this->records,
                                $this->request->limit,
                                $this->request->start
                            );
                        };
                        // $this->records = PengajuanMaster::ApprovalPengajuanDetailSearch($this->request); 
                        // $result = $this->rsJson(
                        //         $this->records,
                        //         $this->request->limit,
                        //         $this->request->start 
                        //     );
                    };
                    break;

            case 100: // reject pengajuan by kabid
                {
                        $session = $this->request->session();
                        $head_data = json_decode(stripslashes($this->request->head));
                        //$catat = json_decode(stripslashes($this->request->catatan));
                        //print_r($catat);exit;
                        $result = PengajuanReject::RejectPengajuan(
                        $this->request->catatan,
                        $head_data,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
            break;
            case 102: // entry realisasi hasil
                {   
                    $session = $this->request->session();
                    $result = UangmukaMaster::UangmukaRealisasiHasil( 
                        $this->request,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
    
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
    
                };
            break;
                    
        };
        return $result;
    }

    public function crudRapbsMaster($subtask)
    {
    }
}
