<?php

namespace App\Http\Controllers\Newanggaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Anggaran\RapbsMaster;
use App\Models\Anggaran\RapbsDetail;
use App\Models\Anggaran\UangmukaMaster;
use App\Models\Anggaran\UangmukaDetail;
use App\Models\Anggaran\PengajuanDetail;
use App\Models\Anggaran\PengajuanMaster;
use App\Models\Anggaran\Monitoring;
use App\Models\Ppdb\TahunAjaran; 
use Excel;


class NewrapbsbendController extends Controller
{ 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {
        switch ($task) {   // routing data
            case 0: return $this->routing($substask);   break;
            // bendahara Rekap
            case 1: return $this->crudNewrapbsbend($substask);  break;
            // bendahara serapan
            case 2: return $this->crudSerapanbend($substask);  break;
            // bendahara spj
            case 3: return $this->crudSpjbend($substask);  break;
            // bendahara RAPBY
            case 4: return $this->crudRapbybend($substask);  break;
                };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {
        switch ($subtask) {
            case 0: // Bendahara Page
                {// print_r('wakakakakaak');exit;
                $session     = $this->request->session();
                $tahunajaran = TahunAjaran::getPeriode($this->request);
                $rapbybudget = Monitoring::RapbyMonitoringSearch($this->request);
                $masterbudget = Monitoring::masterdetailincomeMonitoring($this->request);
                $urusanbudget = Monitoring::UrusanSearch($this->request);
                $apbybudget  = Monitoring::ApbyMonitoring($this->request);
                $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);

                $result  = view('newanggaran/newrapbsbend' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id)
                            ->with("RAPBYBUDGET", $rapbybudget)
                            ->with("MASTERBUDGET", $masterbudget)
                            ->with("URUSANBUDGET", $urusanbudget)
                            
                            ->with("APBYBUDGET", $apbybudget);

                        };
            break;
        };
        return $result;
    }

    public function crudNewrapbsbend($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = Monitoring::BendaharaSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->records = RapbsMaster::headerusulanPrintOut($this->request); 
                    //  print_r($this->records);exit; 
                        $this->crudNewrapbsbend(3);
                    } else {
                        $this->crudNewrapbsbend(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 3: // print pdf
            {};
            break;
            case 4: // print xls
                {
                    $results   = $this->records;
                    $data      = [];
                    $data[]    = [ 
                        "NO",
                        "RAPBS", 
                        "PROGRAM", 
                        "KEGIATAN", 
                        "SUB KEGIATAN", 
                        "KELUARAN", 
                        "HASIL",
                        "KOLOM APA ?", 
                        "WAKTU", 
                        "JUMLAH N-1", 
                        "JUMLAH THN", 
                        "KET ??"
    
                                    ];
                    $loop      = 1;
                    $z         = 8;

                    foreach ($results as $result) {
                        $z1 = $z + $loop;
        
                        $data[] = [" " . $z1,
                        $result->rapbs_no,
                        $result->program_mrapbs_id_name,
                        $result->kegiatan_mrapbs_id_name,
                        $result->sub_kegiatan_mrapbs_id,
                        $result->keluaran,
                        $result->hasil,
                        "",
                        "",
                        "",
                        "",
                        "",

                        ];
                        $loop = $loop + 1;
                    };
                    // echo "<BR> Tunai = " . $tunai;
                    // echo "<BR> Non Tunai = " . $non_tunai;exit;
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'REKAP RENCANA KERJA TAHUNAN',
                        'summaryRekapitulasi',
                        [
                            "A" => 8, "B"  => 30,
                            "C" => 30, "D" => 30,
                            "E" => 30, "F" => 30,
                            "G" => 30, "H" => 30,
                            "I" => 30, "J" => 30,
                            "K" => 30, "L" => 30,

                        ],
                        $data,
                        'A',
                        'L');

                    $y     = $z + $loop;
                    $sheet = $this->report->getActiveSheet();
                    $loop  = 1;
                    $z     = 8;
                    foreach ($results as $result) {
                        $z1 = $z + $loop;
                       // $sheet->setColumnFormat(array(
                        $sheet->setCellValue('H' . $z1, "" . $result->tgt_masukan);
                        $sheet->setCellValue('I' . $z1, "" . $result->tgl_mulai);
                        $sheet->setCellValue('J' . $z1, "" . $result->jumlahn);
                        $sheet->setCellValue('K' . $z1, "" . $result->total_biaya);
                        $sheet->setCellValue('L' . $z1, "" . $result->tgt_keluaran);
                        
                        $loop = $loop + 1;
                    };



                    //TOTAL DIBUAT SEPARATOR
                    $sheet->setColumnFormat(array(
                        'J' => '0,000', 'K' => '0,000'));

                        $sheet->setColumnFormat(array(
                            "J" . ($z - 1) . ':' . 'K' . $y =>'0,000' ));

                    $sheet->setCellValue('B' . $y, 'TOTAL ')
                        ->setCellValue('J' . $y, '=SUM(J' . ($z + 1) . ':J' . ($y - 1) . ')')
                        ->setCellValue('K' . $y, '=SUM(K' . ($z + 1) . ':K' . ($y - 1) . ')')
                        ->getStyle('B' . $y . ':L' . $y)->applyFromArray(['font' => ['size' => 9],
                        'fill' => array(
                            'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'C0C0C0')
                        )                                                                              
                        
                        ]);

                    $sheet->getStyle("H" . ($z - 1) . ':' . 'L' . $y)
                        ->applyFromArray([
                            'font'    => ['size' => 10],
                            'borders' => [
                                'allborders' => [
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                ]]]);

                    $sheet->getStyle('A'. $z . ':L' . $z)->applyFromArray(array(
                        'fill' => array(
                            'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'C0C0C0')
                        )
                    ));

                    $this->report->download('xls');
                };
            break;
            case 10: // search detail data
            {
            };
            break;
            case 11: // Rekap data
                {   $this->records = RapbsMaster::RapbsRekapSearch($this->request);
                    $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                };
                break;

        };
        return $result;
    }

    public function crudSerapanbend($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = Monitoring::SerapanSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->records = RapbsMaster::headerusulanPrintOut($this->request); 
                    //  print_r($this->records);exit; 
                        $this->crudSerapanbend(3);
                    } else {
                        $this->crudSerapanbend(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 3: // print pdf
            {};
            break;
            case 4: // print xls
                {
                    $results   = $this->records;
                    $data      = [];
                    $data[]    = [  "NO", 
                                    "URUSAN", 
                                    "ANGGARAN TAHUN LALU", 
                                    "ANGGARAN TAHUN INI", 
                                    "PERUBAHAN ANGGARAN", 
                                    "ANGGARAN SETELAH PERUBAHAN", 
                                    "UANGMUKA ANGGARAN", 
                                    "%SERAPAN"
                                    ];
                    $loop      = 1;
                    $z         = 8;

                    foreach ($results as $result) {
                        $z1 = $z + $loop;
        
                        $data[] = [" " . $z1,
                            $result->urusan_mrapbs_id_name,
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",

                        ];
                        $loop = $loop + 1;
                    };
                    // echo "<BR> Tunai = " . $tunai;
                    // echo "<BR> Non Tunai = " . $non_tunai;exit;
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'SERAPAN ANGGARAN',
                        'summaryReportSerapanAnggaran',
                        [
                            "A" => 8, "B"  => 30,
                            "C" => 25, "D" => 25,
                            "E" => 25, "F" => 30,
                            "G" => 22, "H" => 15,
                        ],
                        $data,
                        'A',
                        'H');

                    $y     = $z + $loop;
                    $sheet = $this->report->getActiveSheet();
                    $loop  = 1;
                    $z     = 8;
                    foreach ($results as $result) {
                        $z1 = $z + $loop;
                        $sheet->setCellValue('C'. $z1, "" . $result->jumlahn);
                        $sheet->setCellValue('D' . $z1, "" . $result->total_biaya);
                        $sheet->setCellValue('E' . $z1, "" . $result->total_perubahan);
                        $sheet->setCellValue('F' . $z1, '=D' . $z1 . '-E' . $z1);
                        $sheet->setCellValue('G' . $z1, "" . $result->total_pencairan );
                        $sheet->setCellValue('H' . $z1, '=SUM(G' .$z1.'/F'.$z1.')*100');
    
                        
                        $loop = $loop + 1;
                    };

                    // SEPARATOR PER KOLOM
                    $sheet->setColumnFormat(array(
                        "C" . ($z - 1) . ':' . 'G' . $y =>'0,000' ));

                    // SEPARATOR UNTUK KOLOM PERSENTASE    
                    $sheet->setColumnFormat(array(
                            "H" . ($z - 1) . ':' . 'H' . $y =>'0.00' ));

                    //TOTAL DIBUAT SEPARATOR
                    $sheet->setColumnFormat(array(
                        'C' => '0,000', 'D' => '0,000', 'E' => '0,000', 'F' => '0,000', 'G' => '0,000', 'H' => '0,000' ));

                    $sheet->setCellValue('B' . $y, 'TOTAL ')
                        ->setCellValue('C' . $y, '=SUM(C' . ($z + 1) . ':C' . ($y - 1) . ')')
                        ->setCellValue('D' . $y, '=SUM(D' . ($z + 1) . ':D' . ($y - 1) . ')')
                        ->setCellValue('E' . $y, '=SUM(E' . ($z + 1) . ':E' . ($y - 1) . ')')
                        ->setCellValue('F' . $y, '=SUM(F' . ($z + 1) . ':F' . ($y - 1) . ')')
                        ->setCellValue('G' . $y, '=SUM(G' . ($z + 1) . ':G' . ($y - 1) . ')')
                        ->setCellValue('H' . $y, '=SUM(H' . ($z + 1) . ':H' . ($y - 1) . ')')
                        ->getStyle('B' . $y . ':H' . $y)->applyFromArray(['font' => ['size' => 9],
                        'fill' => array(
                            'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'C0C0C0')
                        )                                                                              
                        
                        ]);

                    $sheet->getStyle("C" . ($z - 1) . ':' . 'H' . $y)
                        ->applyFromArray([
                            'font'    => ['size' => 10],
                            'borders' => [
                                'allborders' => [
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                ]]]);

                    // WARNA UNTUK BARIS TOTAL
                    $sheet->getStyle('A'. $z . ':H' . $z)->applyFromArray(array(
                        'fill' => array(
                            'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'C0C0C0')
                        )
                    ));

                    $this->report->download('xls');
                };

            break;
            case 10: // search detail data
            {
            };
            break;
        };
        return $result;
    }

    public function crudSpjbend($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = Monitoring::Monitoringspj($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->crudSpjbend(3);
                    } else {
                        $this->crudSpjbend(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 3: // print pdf
            {};
            break;
            case 4: // print xls
                {
                    $results   = $this->records;
                    // print_r($results->rapbs_no);exit;
                    $data      = [];
                    $data[]    = [ 
                                    "NO", 
                                    "NO RAPBS", 
                                    "SUB KEGIATAN", 
                                    "UANG MUKA", 
                                    "REALISASI", 
                                    "LEBIH (KURANG)" 
                                    ];
                    $loop      = 1;
                    $z         = 7;

                    foreach ($results as $result) {
                        $z1 = $z + $loop;
        
                        $data[] = [
                            " " . $z1,
                            $result->rapbs_no,
                            $result->sub_kegiatan_mrapbs_id,
                            "",
                            "",
                            "",

                        ];
                        $loop = $loop + 1;
                    };

                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'REALISASI SPJ UANG MUKA',
                        'summaryReportRealisasiSpj',
                        [
                            "A" => 8, "B"  => 20,
                            "C" => 60, "D" => 20,
                            "E" => 20, "F" => 20,
                        ],
                        $data,
                        'A',
                        'F');

                        $y     = $z + $loop;
                        $sheet = $this->report->getActiveSheet();
                        $loop  = 1;
                        $z     = 8;
                        foreach ($results as $result) {
                            $z1 = $z + $loop;
                            $sheet->setCellValue('D'. $z1, "" . $result->total_uangmuka);
                            $sheet->setCellValue('E' . $z1, "" . $result->total_realisasi);
                            $sheet->setCellValue('F' . $z1, '=D' . $z1 . '-E' . $z1);
                            
                            $loop = $loop + 1;
                        };
    
                        // SEPARATOR PER KOLOM
                        $sheet->setColumnFormat(array(
                            "D" . ($z - 1) . ':' . 'F' . $y =>'0,000' ));
    
                        // SEPARATOR UNTUK KOLOM PERSENTASE    
                        // $sheet->setColumnFormat(array(
                        //         "H" . ($z - 1) . ':' . 'H' . $y =>'0.00' ));
    
                        //TOTAL DIBUAT SEPARATOR
                        $sheet->setColumnFormat(array(
                            'D' => '0,000', 'E' => '0,000', 'F' => '0,000'));
    
                        $sheet->setCellValue('B' . ($y+1), 'TOTAL ')
                            ->setCellValue('D' . ($y+1), '=SUM(D' . ($z + 1) . ':D' . ($y - 1) . ')')
                            ->setCellValue('E' . ($y+1), '=SUM(E' . ($z + 1) . ':E' . ($y - 1) . ')')
                            ->setCellValue('F' . ($y+1), '=SUM(F' . ($z + 1) . ':F' . ($y - 1) . ')')
                            ->getStyle('B' . ($y+1) . ':F' . $y)->applyFromArray(['font' => ['size' => 9],
                            'fill' => array(
                                'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => 'C0C0C0')
                            )                                                                              
                            
                            ]);
    
                        $sheet->getStyle("C" . ($z - 1) . ':' . 'F' . ($y+1))
                            ->applyFromArray([
                                'font'    => ['size' => 10],
                                'borders' => [
                                    'allborders' => [
                                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    ]]]);
    
                        // WARNA UNTUK BARIS TOTAL
                        $sheet->getStyle('A'. $z . ':F' . $z)->applyFromArray(array(
                            'fill' => array(
                                'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => 'C0C0C0')
                            )
                        ));                        
                    $this->report->download('xls');
                };

            break;
            case 10: // search detail data
            {
            };
            break;
        };
        return $result;
    }

    public function crudRapbybend($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = Monitoring::RapbyMonitoringSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->records = RapbsMaster::headerusulanPrintOut($this->request); 
                    //  print_r($this->records);exit; 
                        $this->crudSpjbend(3);
                    } else {
                        $this->crudSpjbend(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 3: // print rapby
            {
                $rapbybudget = Monitoring::RapbyMonitoringSearch($this->request);
                $masterbudget = Monitoring::masterdetailincomeMonitoring($this->request);
                $urusanbudget = Monitoring::UrusanSearch($this->request);
                $apbybudget  = Monitoring::ApbyMonitoring($this->request);
                $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);

                $filename = "rapbysummaryreport";
    
                $this->create_report_pdf($this->request->session());
                $pdf = $this->report;

                $pdf->setRowXY(25, 25);

                $pdf->SetWidths(array(155));
                $pdf->SetFont('Times','B', 12);
                $pdf->SetAligns(array("C"));
                $pdf->Row(array("RENCANA ANGGARAN PENDAPATAN DAN BELANJA"), false);
                $pdf->Row(array("TAHUN ANGGARAN : ".$tahunanggaran->name), false);

                $pdf->addRowHeight(6);
                $pdf->SetFont('Times','B', 10);
                $pdf->SetAligns(array("L"));
                $pdf->SetFillColor(210,221,242);
                $pdf->Cell(0,5,"RENCANA ANGGARAN PENDAPATAN",0,1,'L',true);

                // PENDAPATAN TIDAK TERIKAT 
                $pdf->addRowHeight(6);
                // $pdf->setRowXY(25,60 );
                $pdf->SetFont('Times','B', 10);
                $pdf->SetWidths(array(70));
                $pdf->Row(array("1. PENDAPATAN TIDAK TERIKAT"), false);
                $pdf->SetWidths(array(70, 30));
                // $pdf->Row(array("", ""), false);
                $pdf->SetFont('Times','I', 10);
                $pdf->SetAligns(array("L", "R"));
                $ttp = 0;

                foreach ($rapbybudget as $ptt)
                {  
                    if ($ptt->sumberdana_id == 1 ) {
                      $pdf->Row(array($ptt->income_name, 
                      number_format($ptt->income_amount, 0, ",", ".")), false);
                      $ttp = $ttp + $ptt->income_amount ;
                    }
                };
                $pdf->Row(array("", "_______________"), false);
                $pdf->SetWidths(array(70,40, 30));
                $pdf->Row(array("TOTAL PENDAPATAN TIDAK TERIKAT :","","Rp. ".number_format($ttp, 0, ",", ".")), false);

                // BATAS PENDAPATAN TIDAK TERIKAT ---------------------

                // PENDAPATAN TERIKAT ---------------------------------
                $pdf->addRowHeight(4);
                $pdf->SetFont('Times','B', 10);
                // $pdf->setRowXY(30,200 );
                $pdf->SetWidths(array(70));
                $pdf->Row(array("2. PENDAPATAN TERIKAT"), false);

                $pdf->addRowHeight(2);
                // $pdf->setRowXY(35,202 );
                $pdf->SetWidths(array(70, 30));
                $pdf->SetFont('Times','I', 10);
                $pdf->SetAligns(array("L", "R"));
                $tp = 0;

                foreach ($rapbybudget as $pt)
                {  
                    if ($pt->sumberdana_id == 2 ) {
                      $pdf->Row(array($pt->income_name, 
                      number_format($pt->income_amount, 0, ",", ".")), false);
                      $tp = $tp + $pt->income_amount ;
                    }
                };
                $pdf->Row(array("", "_______________"), false);
                $pdf->SetWidths(array(70,40, 30));
                $pdf->SetAligns(array("L","L", "R"));
                $pdf->Row(array("TOTAL PENDAPATAN TERIKAT :",""," ".number_format($tp, 0, ",", ".")), false);
                // BATAS PENDAPATAN TERIKAT ---------------------------
                $total_pendapatan = $ttp+$tp;
                $pdf->Row(array("TOTAL PENDAPATAN  :",""," ".number_format($total_pendapatan, 0, ",", ".")), false);

                $pdf->SetWidths(array(80,30, 30));
                $persentage_digunakan = 0;
                foreach ($masterbudget as $presentage)
                {
                $persentage_digunakan = $total_pendapatan*($presentage->cost_presentage/100);
                $pdf->Row(array("PENDAPATAN YANG BISA DIGUNAKAN  : ",
                "",number_format($persentage_digunakan, 0, ",", "."))
                , false);
                }


                $pdf->addRowHeight(6);
                $pdf->SetFont('Times','B', 10);
                $pdf->SetAligns(array("L"));
                $pdf->SetFillColor(210,221,242);
                $pdf->Cell(0,5,"ANGGARAN BELANJA",0,1,'L',true);

                $pdf->SetWidths(array(70, 30));
                $pdf->SetFont('Times','I', 10);
                $pdf->SetAligns(array("L", "R"));
                $total_urusan = 0;
                foreach ($urusanbudget as $urusan)
                {  
                      $pdf->Row(array($urusan->urusan_mrapbs_id_name, 
                      number_format($urusan->total_biaya, 0, ",", ".")), false);
                      $total_urusan = $total_urusan +$urusan->total_biaya ;
                };
                $pdf->Row(array("", "_______________"), false);
                $pdf->SetWidths(array(70,40, 30));
                $pdf->SetAligns(array("L","L", "R"));
                $pdf->Row(array("TOTAL ANGGARAN BELANJA :",""," ".number_format($total_urusan, 0, ",", ".")), false);
                $pdf->Row(array("","", "_______________"), false);
                $surplus_defisit = $persentage_digunakan - $total_urusan;
                $pdf->SetFont('Times','B', 10);

                if ($surplus_defisit > 0 ) {
                    $fill = 1;
                    $pdf->Cell(0, 5, 'SURPLUS', 0, 0, 'L', true);
                    $pdf->Cell(0, 5, " ".number_format($surplus_defisit, 0, ',', '.'), 0, 1, 'R', $fill);
                } else {
                    $fill = 1;
                    $pdf->Cell(0, 5, 'DEFISIT', 0, 0, 'L', true);
                    $pdf->Cell(0, 5, " ".number_format($surplus_defisit, 0, ',', '.'), 0, 1, 'R', $fill);
                }
                $pdf->addRowHeight(20);
                $pdf->SetFont('Times','B', 12);
                $pdf->SetWidths(array(40,40, 40));
                $pdf->SetAligns(array("C","C","C"));
                $pdf->Row(array("Ketua","", "Bendahara"), false);

                $pdf->Output($filename.".pdf", "I");
                exit;
            };
            break;
            case 4: // print xls
                {
                    $rapbybudget = Monitoring::RapbyMonitoringSearch($this->request);
                    $masterbudget = Monitoring::masterdetailincomeMonitoring($this->request);
                    $urusanbudget = Monitoring::UrusanSearch($this->request);
                    $apbybudget  = Monitoring::ApbyMonitoring($this->request);
                    $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
    
                    $filename = "rapbysummaryreport";
        
                    $this->create_report_pdf($this->request->session());
                    $pdf = $this->report;
    
                    $pdf->setRowXY(25, 25);
    
                    $pdf->SetWidths(array(155));
                    $pdf->SetFont('Times','B', 12);
                    $pdf->SetAligns(array("C"));
                    $pdf->Row(array("ANGGARAN PENDAPATAN DAN BELANJA"), false);
                    $pdf->Row(array("TAHUN ANGGARAN : ".$tahunanggaran->name), false);
    
                    $pdf->addRowHeight(6);
                    $pdf->SetFont('Times','B', 10);
                    $pdf->SetAligns(array("L"));
                    $pdf->SetFillColor(210,221,242);
                    $pdf->Cell(0,5,"RENCANA ANGGARAN PENDAPATAN",0,1,'L',true);
    
                    // PENDAPATAN TIDAK TERIKAT 
                    $pdf->addRowHeight(6);
                    // $pdf->setRowXY(25,60 );
                    $pdf->SetFont('Times','B', 10);
                    $pdf->SetWidths(array(70));
                    $pdf->Row(array("1. PENDAPATAN TIDAK TERIKAT"), false);
                    $pdf->SetWidths(array(70, 30));
                    // $pdf->Row(array("", ""), false);
                    $pdf->SetFont('Times','I', 10);
                    $pdf->SetAligns(array("L", "R"));
                    $ttp = 0;
    
                    foreach ($rapbybudget as $ptt)
                    {  
                        if ($ptt->sumberdana_id == 1 ) {
                          $pdf->Row(array($ptt->income_name, 
                          number_format($ptt->income_amount, 0, ",", ".")), false);
                          $ttp = $ttp + $ptt->income_amount ;
                        }
                    };
                    $pdf->Row(array("", "_______________"), false);
                    $pdf->SetWidths(array(70,40, 30));
                    $pdf->Row(array("TOTAL PENDAPATAN TIDAK TERIKAT :","","Rp. ".number_format($ttp, 0, ",", ".")), false);
    
                    // BATAS PENDAPATAN TIDAK TERIKAT ---------------------
    
                    // PENDAPATAN TERIKAT ---------------------------------
                    $pdf->addRowHeight(4);
                    $pdf->SetFont('Times','B', 10);
                    // $pdf->setRowXY(30,200 );
                    $pdf->SetWidths(array(70));
                    $pdf->Row(array("2. PENDAPATAN TERIKAT"), false);
    
                    $pdf->addRowHeight(2);
                    // $pdf->setRowXY(35,202 );
                    $pdf->SetWidths(array(70, 30));
                    $pdf->SetFont('Times','I', 10);
                    $pdf->SetAligns(array("L", "R"));
                    $tp = 0;
    
                    foreach ($rapbybudget as $pt)
                    {  
                        if ($pt->sumberdana_id == 2 ) {
                          $pdf->Row(array($pt->income_name, 
                          number_format($pt->income_amount, 0, ",", ".")), false);
                          $tp = $tp + $pt->income_amount ;
                        }
                    };
                    $pdf->Row(array("", "_______________"), false);
                    $pdf->SetWidths(array(70,40, 30));
                    $pdf->SetAligns(array("L","L", "R"));
                    $pdf->Row(array("TOTAL PENDAPATAN TERIKAT :",""," ".number_format($tp, 0, ",", ".")), false);
                    // BATAS PENDAPATAN TERIKAT ---------------------------
                    $total_pendapatan = $ttp+$tp;
                    $pdf->Row(array("TOTAL PENDAPATAN  :",""," ".number_format($total_pendapatan, 0, ",", ".")), false);
    
                    $pdf->SetWidths(array(80,30, 30));
                    $persentage_digunakan = 0;
                    foreach ($masterbudget as $presentage)
                    {
                    $persentage_digunakan = $total_pendapatan*($presentage->cost_presentage/100);
                    $pdf->Row(array("PENDAPATAN YANG BISA DIGUNAKAN  : ",
                    "",number_format($persentage_digunakan, 0, ",", "."))
                    , false);
                    }
    
    
                    $pdf->addRowHeight(6);
                    $pdf->SetFont('Times','B', 10);
                    $pdf->SetAligns(array("L"));
                    $pdf->SetFillColor(210,221,242);
                    $pdf->Cell(0,5,"ANGGARAN BELANJA",0,1,'L',true);
    
                    $pdf->SetWidths(array(70, 30));
                    $pdf->SetFont('Times','I', 10);
                    $pdf->SetAligns(array("L", "R"));
                    $total_urusan = 0;
                    foreach ($urusanbudget as $urusan)
                    {  
                          $pdf->Row(array($urusan->urusan_mrapbs_id_name, 
                          number_format($urusan->total_biaya, 0, ",", ".")), false);
                          $total_urusan = $total_urusan +$urusan->total_biaya ;
                    };
                    $pdf->Row(array("", "_______________"), false);
                    $pdf->SetWidths(array(70,40, 30));
                    $pdf->SetAligns(array("L","L", "R"));
                    $pdf->Row(array("TOTAL ANGGARAN BELANJA :",""," ".number_format($total_urusan, 0, ",", ".")), false);
                    $pdf->Row(array("","", "_______________"), false);
                    $pdf->SetFont('Times','B', 10);
                    // $pdf->SetFillColor(210,221,242);
                    $surplus_defisit = $persentage_digunakan - $total_urusan;
                    if ($surplus_defisit > 0 ) {
                        $fill = 1;
                        $pdf->Cell(0, 5, 'SURPLUS', 0, 0, 'L', true);
                        $pdf->Cell(0, 5, " ".number_format($surplus_defisit, 0, ',', '.'), 0, 1, 'R', $fill);
                    } else {
                        $fill = 1;
                        $pdf->Cell(0, 5, 'DEFISIT', 0, 0, 'L', true);
                        $pdf->Cell(0, 5, " ".number_format($surplus_defisit, 0, ',', '.'), 0, 1, 'R', $fill);
                    }
    

                    $pdf->addRowHeight(20);
                    $pdf->SetFont('Times','B', 12);
                    $pdf->SetWidths(array(40,40, 40));
                    $pdf->SetAligns(array("C","C","C"));
                    $pdf->Row(array("Pembina","", "Ketua"), false);
    
                    $pdf->Output($filename.".pdf", "I");
                    exit;
                };

            break;
            case 10: // search detail data
            {
            };
            break;
        };
        return $result;
    }
}
