<?php

namespace App\Http\Controllers\Newanggaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Anggaran\RapbsMaster;
use App\Models\Anggaran\RapbsDetail;
use App\Models\Ppdb\TahunAjaran;
//use Codedge\Fpdf\Fpdf\Fpdf;
use Fpdf;


class PembinaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {
        switch ($task) {   // routing data
            case 0: return $this->routing($substask);   break;
            // Pembina
            case 1: return $this->crudPembina($substask);  break;
            // Detail biaya RAPBS
            case 2: return $this->crudRapbsMaster($substask);    break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {   $session = $this->request->session();
        $this->myTasks = array_flip(
                                    RapbsMaster::myTasks(
                                        $session->get("user_id"), 
                                        $this->request->m)
                                );
        switch ($subtask) {
            case 0: // anggaran page 
                {
                    $session = $this->request->session();
                    $tahunajaran= TahunAjaran::getPeriode($this->request);
                    $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
         
                    $result  = view('newanggaran/pembina' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("MyTasks", $this->myTasks)
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
        
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                };
                break;
                
        };
        return $result;
    }

    public function crudPembina($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = RapbsMaster::PembinaSearch($this->request,  
                    $session->get("user_id"), 
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->records = RapbsMaster::headerusulanPrintOut($this->request); 
                    //  print_r($this->records);exit; 
                        $this->crudPembina(3);
                    } else {
                        $this->crudPembina(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
            {
            };
            break;
            case 2: // delete data
            {
            };
            break;
            case 3: // print pdf
            { 

            };
            break;
            case 4: // print xls
            {
                $results = $this->records;
                $data = array();
                $data[] = array("NO_RAPBY", "URUSAN","ORGANISASI","PROGRAM","KEGIATAN", "STATUS", "CREATED");
                foreach ($results as $result) {
                    $data[] = array(
                        $result->rapbs_no,
                        $result->urusan_mrapbs_id_name,
                        $result->organisasi_mrapbs_id_name,
                        $result->program_mrapbs_id_name,
                        $result->kegiatan_mrapbs_id_name,
                        rapbsmaster::getStatus($result->status),
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Rekapitulasi Anggaran',
                    'Anggaran',
                    array("A" => 10,  "B" => 25,  "C" => 25, "D" => 25, "E" => 55, "F" => 5, "G" => 10),
                    $data,
                    'A',
                    'G'
                );
                $this->report->download('xls');
            };
            break;
            case 9: // search detail data

                { $session = $this->request->session();
                    $this->records = RapbsMaster::PembinaPanitiaSearch(
                    $this->request,  
                    $session->get("user_id"), 
                    $session->get("level")
                
                );
                    $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                };
                break;
    
            case 10: // search detail data
            {   $this->records = RapbsDetail::RapbsDetailSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
            };
            break;
            case 705: // approve data
            {   
                $session = $this->request->session();
                $result = RapbsMaster::Pembinakabid(
                    $this->request,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);

            };
            break;
            case 706: // approve data anggaran by ketua
            {   
                $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                // print_r($json_data);exit;
                $result = RapbsMaster::Pembinaketua(
                    
                    $json_data,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);

            };
            break;
            case 707: // approve data anggaran by pembina
            {   
                $session = $this->request->session();
                $json_data = json_decode(stripslashes($this->request->json));
                // print_r($json_data);exit;
                $result = RapbsMaster::Pembinapembina(
                    
                    $json_data,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);

            };
            break;
    
            
        };
        return $result;
    }

    public function crudRapbsMaster($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = RapbsMaster::RapbsMasterSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->rapbsmaster(3);
                        } else {
                            $this->RapbsMaster(4);
                        };
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
                break;
            case 1: // save data
                {
                    $session = $this->request->session();
                     $head_data = json_decode(stripslashes($this->request->head));
                    $json_data = json_decode(stripslashes($this->request->json));
                    // $type = json_decode(stripslashes($this->request->type));
                    $result = RapbsMaster::RapbsMasterSave(
                         $head_data,
                        $json_data,
                         $type,
                        // $session->get("site"),
                        $session->get("user_id")
                        // $session->get("company_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2: // delete data
                {
                    $result = RapbsMaster::RapbsMasterDelete($this->request);
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3: // print pdf
                {

                };
                break;
            case 4: // print xls
                {
                    $results = $this->records;
                    $data = array();
                    $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                    foreach ($results as $result) {
                        $data[] = array(
                            $result->RapbsMaster_key_id,
                            $result->name,
                            Newrapbs::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                        );
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Master Countries',
                        'Countries',
                        array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                        $data,
                        'A',
                        'G'
                    );
                    $this->report->download('xls');
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        RapbsMaster::RapbsMasterSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        };
        return $result;
    }

}
