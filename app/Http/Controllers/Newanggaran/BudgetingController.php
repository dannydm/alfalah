<?php

namespace App\Http\Controllers\Newanggaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Anggaran\IncomeMaster;
use App\Models\Anggaran\IncomeDetail;
use App\Models\Anggaran\RapbsMaster;
use App\Models\Anggaran\RapbsDetail;

use App\Models\Ppdb\TahunAjaran; 


class BudgetingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {
        switch ($task) {   // routing data
            case 0: return $this->routing($substask);   break;
            // Budgeting
            case 1: return $this->crudBudgeting($substask);  break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {    $session = $this->request->session();
        $this->myTasks = array_flip(
                                    RapbsMaster::myTasks(
                                        $session->get("user_id"), 
                                        $this->request->m)
                                );
        switch ($subtask) {
            case 0: // anggaran page 
                {
                    $session = $this->request->session();
                    $tahunajaran= TahunAjaran::getPeriode($this->request);
                    $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
    
                    $result  = view('newanggaran/budgeting' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("MyTasks", $this->myTasks)
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
            
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                };
                break;
        };
        return $result;
    }

    public function crudBudgeting($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = BudgetingMaster::BudgetingSearch(
                $this->request, 
                $session->get("user_id"),
                $session->get("level")
            );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->crudBudgeting(3);
                    } else {
                        $this->crudBudgeting(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
                {
                    $session = $this->request->session();
                    $head_data = json_decode(stripslashes($this->request->head));
                    $json_data = json_decode(stripslashes($this->request->json));
                    $modi_data = json_decode(stripslashes($this->request->modi));
                    // print_r($head_data);exit;
                    $result = BudgetingMaster::BudgetingMasterSave(
                        $head_data[0],
                        $json_data,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                    };
                break;
            case 2: // delete data
            {
                $result = BudgetingMaster::BudgetingMasterDelete($this->request);
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 3: // print pdf
            {};
            break;
            case 4: // print xls
            {
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                foreach ($results as $result) {
                    $data[] = array(
                        $result->RapbsMaster_key_id,
                        $result->name,
                        Newrapbs::getStatus($result->status),
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Countries',
                    'Countries',
                    array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                    $data,
                    'A',
                    'G'
                );
                $this->report->download('xls');
            };
            break;
            case 9: // search detail data
                {   $this->records = IncomeDetail::BudgetingSearch($this->request);
                    $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                };
            break;
            case 10: // search detail data
                {   $this->records = IncomeDetail::IncomeDetailSearch($this->request);
                    $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                };
            break;
            // kepake di dashboard
            case 15: // search detail data
                {   $this->records = IncomeDetail::DashboardAngSearch($this->request);
                    $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                };
            break;


            case 705: // approve data kabid
            {   
                $session = $this->request->session();
                $result = BudgetingMaster::BudgetingMasterBudgetingkabid( 
                    $this->request,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);

            };
            break;
            case 99: // search detail data
                { 
                    $this->records = BudgetingMaster::PengajuanBudgetingDetailSearch($this->request);
                    if ($this->request->pt == 'pdf') {
                            $this->detail = BudgetingMaster::KegiatanPrintOut($this->request);
                            $this->crudNewrapbs(3);
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };

                };
            break;
        };
        return $result;
    }

    public function crudRapbsMaster($subtask)
    {
    }
}
