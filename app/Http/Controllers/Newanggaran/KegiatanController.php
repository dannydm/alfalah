<?php

namespace App\Http\Controllers\Newanggaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Anggaran\PengajuanMaster;
use App\Models\Anggaran\PengajuanDetail;
use App\Models\Ppdb\TahunAjaran;
use Fpdf;


class KegiatanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {
        switch ($task) {   // routing data
            case 0: return $this->routing($substask);   break;
            // RAPBS
            case 1: return $this->crudKegiatan($substask);  break;
            // Detail biaya RAPBS
            case 2: return $this->crudRapbsMaster($substask);    break;
        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {
        switch ($subtask) {
            case 0: // anggaran page 
                {
                    $session = $this->request->session();
                    $tahunajaran= TahunAjaran::getPeriode($this->request);
                    $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
  
                    $result  = view('newanggaran/kegiatan' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
     
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                };
                break;
        };
        return $result;
    }

    public function crudKegiatan($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = PengajuanMaster::KegiatanSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        // $this->records = PengajuanMaster::headerajuanPrintOut($this->request);
                        $this->crudKegiatan(3);
                    } else {
                        $this->crudNewrapbs(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
            {
                $session = $this->request->session();
                $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                $modi_data = json_decode(stripslashes($this->request->modi));

                $result = PengajuanMaster::PengajuanMasterSave(
                    $head_data[0],
                    $json_data,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 2: // delete data
            {
                $result = PengajuanMaster::PengajuanMasterDelete($this->request);
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 3: // print pdf
            {
                    
                    // $data =  PengajuanMaster::headerajuanPrintOut($this->request);
                    $data =  PengajuanDetail::kegiatanprint($this->request);
                    
                    $title = "RENCANA KERJA DAN ANGGARAN YAYASAN MASJID DARUSALLAM";
                    $filename = "PENG".$data->pengajuan_no;
    
                    $this->create_report_pdf($this->request->session());
                    $pdf = $this->report;
                    ########## PAGE TITLE ##########    
                    /*KOTAK HEADER DAN LOGO */

                    $pdf->setRowXY(15, 30);
                    $pdf->Image('academy/img/core-img/logoalfalah.png', 15, 26, 29, 24); 
                    $pdf->SetWidths(array(50, 120));
                    $pdf->SetFont('Times','B', 10);
                    $pdf->SetAligns(array("C", "C"));
                    $pdf->Row(array("","YAYASAN MASJID DARUSSALAM TROPODO"), false);
                    $pdf->Row(array("","LEMBAGA PENDIDIKAN AL FALAH DARUSSALAM TROPODO"), false);
                    $pdf->Row(array("","RENCANA KEGIATAN : ".$data->organisasi_mrapbs_id_name),false);
                    
                    $pdf->Row(array("NO. ",$data->pengajuan_no),false);
                    $y_current = $pdf->y_pos-5;
                    $y_height  = $pdf->getY() - $y_current;
                    $pdf->Rect($pdf->x_pos, $y_current+1, 30, $y_height ); 
                    $pdf->Rect($pdf->x_pos+(30), $y_current+1, 150, $y_height ); 
                    /* DETAIL ITEMS */
                    // $details = PengajuanDetail::pengajuandetailPrintOut(trim($data->pengajuan_no));
                    // print_r($details);exit;
                    // $detail = PengajuanMaster::KegiatanPrintOut($this->request);
                    // print_r($detail);exit;
                    
                    $pdf->setRowXY(15, 52);
                    $pdf->SetFont('Times','B', 8);                    
                    $pdf->SetWidths(array(7,30,70,20,53));
                    $pdf->SetAligns(array("C","C","C","C","C"));
                    $pdf->Row(array("No.","No. Kegiatan", "Sub Kegiatan","Jumlah","Hasil Yg diharapkan"), true);
                    $pdf->SetFont('Times','', 8);
                    $pdf->SetAligns(array("C","L","L","R","C"));

                    
//                   print_r($data);exit;
                    $i=1;
            //       print_r($data);exit;
                     $details = PengajuanDetail::kegiatandetailPrintOut(trim($data->pengajuan_no));
 //                     print_r($details);exit;
                    foreach ($details as $item )
                    {// print_r($item);exit; 
                       
                        $pdf->Row(array(
                                    $i++,
                                    $item->rapbs_no,
                                    $item->sub_kegiatan,
                                    number_format($item->total_biaya, 0, ",", "."),
                                    $item->hasil_ang), true);
                    };
                    $pdf->SetFont('Times','', 8);
                    $pdf->SetAligns(array("L", "L"));
                    $pdf->SetWidths(array(107, 20));
                    $pdf->SetAligns(array("C", "R"));
                    // $details = $this->detail;
                   $pdf->Row(array("T O T A L",number_format($data->total_pengajuan, 0, ",", ".")), true);

                    /* TANDA TANGAN */
                    $pdf->setRowXY(15, 187);
                    $pdf->SetFont('Times','B', 10);
                    $pdf->addRowHeight(15);
                    $pdf->SetWidths(array(60,60,60));
                    $pdf->SetAligns(array("C","C","C"));                    
                    $pdf->Row(array("Ketua Yayasan,","Kabid/Wakabid,","Penanggung Jawab,"), false);
                    $pdf->addRowHeight(20);
                    $pdf->Row(array("________________","_________________","__________________"), false);

                    /* KOTAK DISPOSISI */
                    $pdf->setRowXY(15, 236);
                    $pdf->SetFont('Times','I', 10);
                    $pdf->SetWidths(array(60));
                    $pdf->SetAligns(array("L"));                    
                    $pdf->Row(array("Disposisi Ketua Yayasan :"), false); 
                    $pdf->Rect(15, 235, 180,25 );
                    
                    /* NB */
                    $pdf->addRowHeight(20);
                    $pdf->SetWidths(array(180));
                    $pdf->SetAligns(array("L"));
                    $pdf->SetFont('Times','I', 9);
                    $pdf->Row(array("NB : "), false); 
                    $pdf->Row(array("Pengajuan rencana kegiatan selambat-lambatnya tanggal 20 sebelum bulan pelaksanaan kegiatan."), false); 
                    $pdf->SetWidths(array(180));
                    $pdf->SetAligns(array("R"));                    
                    $pdf->SetFont('Times','I', 8);

                    $pdf->Row(array("Form KEU 2.01"), false);


                    $pdf->Output($filename.".pdf", "I");
                    exit;
            };
            break;
            case 4: // print xls
            {
                $results = $this->records;
                $data = array();
                $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                foreach ($results as $result) {
                    $data[] = array(
                        $result->RapbsMaster_key_id,
                        $result->name,
                        Newrapbs::getStatus($result->status),
                        $this->TimeStampToString($result->created_date, 'd/m/Y'),
                    );
                };
                $this->create_report_xls_master(
                    $results->count(),
                    $this->request->session(),
                    'Master Countries',
                    'Countries',
                    array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                    $data,
                    'A',
                    'G'
                );
                $this->report->download('xls');
            };
            break;
            case 10: // search detail data
            {   $this->records = PengajuanDetail::KegiatanDetailSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
            };
            break;
            case 705: // approve data dari daftar pengajuan
            {   
                $session = $this->request->session();
                $result = PengajuanMaster::PengajuanMasterApproval(
                    $this->request,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company_id")
                );

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);

            };
            break;
            case 707: // reject data
                {   
                    $session = $this->request->session();
                    $result = RapbsMaster::RapbsMasterReject2(
                        $this->request,
                        $session->get("site"),
                        $session->get("user_id"),
                        $session->get("company_id")
                    );
    
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
    
                };
                break;

                case 99: // search detail data
                        { 
                       $this->records = PengajuanMaster::ApprovalPengajuanDetailSearch($this->request);
                        if ($this->request->pt == 'pdf') {
                                 $this->detail = PengajuanMaster::KegiatanPrintOut($this->request);
                                    //  print_r($this->detail);exit;
                                $this->crudKegiatan(3);
                        } else {
                            $result = $this->rsJson(
                                $this->records,
                                $this->request->limit,
                                $this->request->start
                            );
                        };
                    };
                    break;

        }
        return $result;
    }

    public function crudRapbsMaster($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = PengajuanMaster::KegiatanSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->RapbsMaster(3);
                        } else {
                            $this->RapbsMaster(4);
                        };
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
                break;
            case 1: // save data
                {
                    $session = $this->request->session();
                     $head_data = json_decode(stripslashes($this->request->head));
                    $json_data = json_decode(stripslashes($this->request->json));
                    // $type = json_decode(stripslashes($this->request->type));
                    $result = RapbsMaster::RapbsMasterSave(
                         $head_data,
                        $json_data,
                         $type,
                        // $session->get("site"),
                        $session->get("user_id")
                        // $session->get("company_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2: // delete data
                {
                    $result = RapbsMaster::RapbsMasterDelete($this->request);
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                    $results = $this->records;
                    $data = array();
                    $data[] = array("ID", "ANGGARAN_ID", "STATUS", "CREATED");
                    foreach ($results as $result) {
                        $data[] = array(
                            $result->RapbsMaster_key_id,
                            $result->name,
                            Newrapbs::getStatus($result->status),
                            $this->TimeStampToString($result->created_date, 'd/m/Y'),
                        );
                    };
                    $this->create_report_xls_master(
                        $results->count(),
                        $this->request->session(),
                        'Master Countries',
                        'Countries',
                        array("A" => 10,  "B" => 25,  "C" => 15, "D" => 15, "E" => 15, "F" => 15, "G" => 15),
                        $data,
                        'A',
                        'G'
                    );
                    $this->report->download('xls');
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        PengajuanMaster::PengajuanMasterSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
                break;
        };
        return $result;
    }

}
