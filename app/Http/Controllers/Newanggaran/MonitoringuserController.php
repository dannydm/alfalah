<?php

namespace App\Http\Controllers\Newanggaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Anggaran\RapbsMaster;
use App\Models\Anggaran\RapbsDetail;
use App\Models\Anggaran\UangmukaMaster;
use App\Models\Anggaran\UangmukaDetail;
use App\Models\Anggaran\PengajuanDetail;
use App\Models\Anggaran\PengajuanMaster;
use App\Models\Anggaran\Monitoring;
use App\Models\Ppdb\TahunAjaran; 


class MonitoringuserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {
        switch ($task) {   // routing data
            case 0: return $this->routing($substask);   break;
            // RAPBS
            case 1: return $this->crudMonitoringuser($substask);  break;
            case 2: return $this->crudMonitoringuserCair($substask);    break;
            case 3: return $this->crudRapbsDetail($substask);    break;

        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {
        switch ($subtask) {
            case 0: // Monitoringuser page 
                {
                    $session = $this->request->session();
                    $tahunajaran= TahunAjaran::getPeriode($this->request);
                    $allstatus        = Monitoring::statusMonitoring($this->request);
                    $tahunanggaran = TahunAjaran::getPeriodeAng($this->request);
        
                    // print_r($tahunajaran);exit;
                    $result  = view('newanggaran/Monitoringuser' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNANGGARAN", $tahunanggaran->name)
                            ->with("TAHUNANGGARAN_ID", $tahunanggaran->tahunajaran_id)
   
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id)
                            ->with("ALLSTATUS", $allstatus)
                            ;
                };
                break;
        };
        return $result;
    }

    public function crudMonitoringuser($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            {
                $session = $this->request->session();
                $this->records = RapbsMaster::MonitoringuserSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->records = RapbsMaster::headerusulanPrintOut($this->request); 
                    //  print_r($this->records);exit; 
                        $this->crudNewrapbs(3);
                    } else {
                        $this->crudNewrapbs(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
            {
                $this->records = RapbsMaster::MonitoringuserSearch($this->request);
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->RapbsMaster(3);
                    } else {
                        $this->RapbsMaster(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
        };
            break;
            case 2: // delete data
            {};
            break;
            case 3: // print pdf
            {};
            break;
            case 4: // print xls
            {
            };
            break;
            case 10: // search detail data
            {   $this->records = RapbsDetail::RapbsDetailSearch($this->request);
                $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
            };
            break;
            case 11: // Rekap data
                {   $this->records = RapbsMaster::RapbsRekapSearch($this->request);
                    $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                };
                break;
            case 15: // search data
            {
                $session = $this->request->session();
                $this->records = Monitoringuser::MonitoringuserAllStatus($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                        $this->records = RapbsMaster::headerusulanPrintOut($this->request); 
                    //  print_r($this->records);exit; 
                        $this->crudNewrapbs(3);
                    } else {
                        $this->crudNewrapbs(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
        };
        return $result;
    }

    public function crudMonitoringuserCair($subtask)
    {
        switch ($subtask) {
            case 0: // search data pengajuan
                {
                    $session = $this->request->session(); 
                    $this->records = Monitoringuser::MonitoringuserPengajuanSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                
                );
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->RapbsMaster(3);
                        } else {
                            $this->RapbsMaster(4);
                        };
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
                break;
            case 1: // search data pencairan
                {
                    {
                        $session = $this->request->session(); 
                        $this->records = UangmukaMaster::MonitoringusercairSearch($this->request, 
                        $session->get("user_id"),
                        $session->get("level")
                    
                    );
                        if ($this->request->pt) {
                            if ($this->request->pt == 'pdf') {
                                $this->RapbsMaster(3);
                            } else {
                                $this->RapbsMaster(4);
                            };
                        } else {
                            $result = $this->rsJson(
                                $this->records,
                                $this->request->limit,
                                $this->request->start
                            );
                        };
                    };
    
                };
                break;
            case 2: // delete data
                {
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                };
                break;
            case 9: {
                    $result = $this->rsExtJson(
                        RapbsMaster::RapbsMasterSearchExt($this->request),
                        $this->request->limit,
                        $this->request->start
                    );
                };
            break;
            case 10: {
                
                $result = $this->rsExtJson(
                    Monitoringuser::MonitoringuserPengajuanDetailSearch($this->request->pengajuan_no),
                    
                    $this->request->limit,
                    $this->request->start
                );
            };
        break;
        };
        return $result;
    }

    public function crudRapbsDetail($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = RapbsDetail::MonitoringuserSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->RapbsMaster(3);
                        } else {
                            $this->RapbsMaster(4);
                        };
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
                break;
            };
            return $result;
        }

}
