<?php

namespace App\Http\Controllers\Accounting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ppdb\TahunAjaran;
use Fpdf;




class CashController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($task, $substask)
    {
        switch ($task) {   // routing data
            case 0: return $this->routing($substask);   break;
            // RAPBS
            case 1: return $this->crudCashin($substask);  break;
            // Detail biaya RAPBS
            case 2: return $this->crudCashout($substask);    break;

        };
    }
    /**
     * Create a new controller instance.
     */
    public function routing($subtask)
    {
        switch ($subtask) {
            case 0: // cashin page
                {
                    $session = $this->request->session();
                    $tahunajaran= TahunAjaran::getPeriode($this->request);
                    

                    $result  = view('accounting/cashin' . config('app.system_skin'))
                            ->with("USER_ID", $session->get("user_id"))
                            ->with("USERNAME", $session->get("username"))
                            ->with("USERSITE", $session->get("site"))
                            ->with("TABID", $this->request->get("tabId"))
                            ->with("TAHUNAJARAN", $tahunajaran->name)
                            ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                };
                break;
                case 1: // cashout page
                    {
                        $session = $this->request->session();
                        $tahunajaran= TahunAjaran::getPeriode($this->request);
                        
    
                        $result  = view('accounting/cashout' . config('app.system_skin'))
                                ->with("USER_ID", $session->get("user_id"))
                                ->with("USERNAME", $session->get("username"))
                                ->with("USERSITE", $session->get("site"))
                                ->with("TABID", $this->request->get("tabId"))
                                ->with("TAHUNAJARAN", $tahunajaran->name)
                                ->with("TAHUNAJARAN_ID", $tahunajaran->tahunajaran_id);
                    };
                    break;
        };
        return $result;
    }

    public function crudCashin($subtask)
    {
        switch ($subtask) {
            case 0: // search data
            { 
                $session = $this->request->session();
                $this->records = CashinMaster::CashinMasterSearch($this->request, 
                    $session->get("user_id"),
                    $session->get("level")
                );
                if ($this->request->pt) {
                    if ($this->request->pt == 'pdf') {
                       $this->records = CashinMaster::headerusulanPrintOut($this->request); 
                    //  print_r($this->records);exit; 
                        $this->crudNewCashin(3);
                    } else {
                        $this->crudNewCashin(4);
                    };
                } else {
                    $result = $this->rsJson(
                        $this->records,
                        $this->request->limit,
                        $this->request->start
                    );
                };
            };
            break;
            case 1: // save data
            {
                $session = $this->request->session();

                $head_data = json_decode(stripslashes($this->request->head));
                $json_data = json_decode(stripslashes($this->request->json));
                $modi_data = json_decode(stripslashes($this->request->modi));

                // ($data, $json_data, $site, $user_id, $company_id)
                $result = CashinMaster::CashinMasterSave(
                    $head_data[0],
                    $json_data,
                    $session->get("site"),
                    $session->get("user_id"),
                    $session->get("company")
                );
                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 2: // delete data master
            {
                $result = CashinMaster::CashinMasterDelete($this->request);

                if ($result[0] == false) {
                    $this->server_message = $result[1];
                };
                $result = $this->jsonSuccess($result[0]);
            };
            break;
            case 3: // print pdf
            {

            };
            break;
            case 4: // print xls
            {
            };
            break;
            
        };
        return $result;
    }

    public function crudCashout($subtask)
    {
        switch ($subtask) {
            case 0: // search data
                {
                    $this->records = CashoutMaster::cashoutMasterSearch($this->request);
                    if ($this->request->pt) {
                        if ($this->request->pt == 'pdf') {
                            $this->crudCashout(3);
                        } else {
                            $this->crudCashout(4);
                        };
                    } else {
                        $result = $this->rsJson(
                            $this->records,
                            $this->request->limit,
                            $this->request->start
                        );
                    };
                };
                break;
            case 1: // save data
                {
                    $session = $this->request->session();
                     $head_data = json_decode(stripslashes($this->request->head));
                    $json_data = json_decode(stripslashes($this->request->json));
                    // $type = json_decode(stripslashes($this->request->type));
                    $result = CashoutMaster::CashoutMasterSave(
                         $head_data,
                        $json_data,
                         $type,
                        // $session->get("site"),
                        $session->get("user_id")
                        // $session->get("company_id")
                    );
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 2: // delete data
                {
                    $result = CashoutMaster::CashoutMasterDelete($this->request);
                    if ($result[0] == false) {
                        $this->server_message = $result[1];
                    };
                    $result = $this->jsonSuccess($result[0]);
                };
                break;
            case 3: // print pdf
                {
                };
                break;
            case 4: // print xls
                {
                };
                break;
        };
        return $result;
    }
}
