<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;
use DB;

class Employee extends Model
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_emp';
	protected $primaryKey = 'emp_id';
    // public $timestamps = false;    // disable auto generate created value
    // public $incrementing = false;  // disable auto generate primary key value
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getStatus($status)
    {   switch($status)
        {   case 0: $result = "Active";  break;
            case 1: $result = "InActive";  break;
        };
        return $result;
    }
    public static function setStatus($status)
    {   switch($status)
        {   case "Active": $result = 0; break;
            case "InActive": $result = 1; break;
        };
        return $result;
    }
    public static function employeeSearch($request)
    {
    $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY emp_id)) as record_id, * ")->orderBy('emp_id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->status)
            {   $results->where("status", "=", Employee::setStatus($request->status));   };
        };
        return $results->get();
    }
    public static function employeeSearchExt($request)
    { 
        $subQuery = DB::table('m_emp as a')
            ->selectRaw("a.emp_id,
                                (' [' || a.emp_id || '] ' || a.name) as display")
            ->where('a.status', '=', 0);

        $results = Employee::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.emp_id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
        }
    public static function employeeSave($json_data, $user_id, $type)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {
                    if ($json->created_at == '') {   
                        $employee = new Employee();
                        // $employee->created_date  = date('Ymd');
                        $employee->emp_id         = $json->emp_id;
                    }  else {   
                        $employee = Employee::where('emp_id', '=', $json->emp_id)->first();
                        // $employee->modified_date  = date('Ymd');
                    };
                    // $employee->emp_id           = $json->emp_id;
                    $employee->name           = $json->name;
                    $employee->address        = $json->address;
                    $employee->mobile_phone   = $json->mobile_phone;
                    $employee->place_of_birth = $json->place_of_birth;
                    $employee->date_of_birth  = $json->date_of_birth;
                    $employee->height         = $json->height;
                    $employee->weight         = $json->weight;
                    $employee->sex            = $json->sex;
                    $employee->marital_status = $json->marital_status;
                    $employee->jabatan        = $json->jabatan;
                    $employee->dept_id        = $json->dept_id;
                    $employee->status         = $json->status;
                    $result                   = $employee->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function employeeDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('emp_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Pk_m_emp()
    {
        return $this->hasMany('App\Models\HR\Employee', 'emp_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
