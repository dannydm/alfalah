<?php

namespace App\Models\Inventory;

use App\Models\AppModel;
use DB;

class GrinDetail extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'grin_detail';
	protected $primaryKey = 'id';
    public $sequence_name = 'grin_detail_';
    
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   //$results = static::orderby('id', 'asc');
        $results = DB::table('grin_detail as a')
            ->leftjoin('m_item as x1', 'x1.item_id', '=', 'a.item_id')
            ->select(DB::raw("a.*, 0 as incoming_qty,
                (a.quantity_primary + a.reject_primary) as total_primary,
                x1.name as item_name, x1.has_subdetail
                "))
            ->orderby('a.id', 'asc');

        $where_clause = "";
        if ($request->s == "form")
        {   if ( $request->grin_no )
            {   $results->where("grin_no", "ilike", "%".$request->grin_no."%"); 
                // $where_clause .= "and a.grin_no ilike '%".$request->grin_no."%' ";
            };
        };
        return $results->get();
    }
    // public static function SearchOrder($request)
    // {   //$results = static::orderby('id', 'asc');
    //     $results = DB::table('grin_detail as a')
    //         ->leftjoin('m_item as x1', 'x1.item_id', '=', 'a.item_id')
    //         ->select(DB::raw("a.*, 0 as incoming_qty,
    //             (a.quantity_primary + a.reject_primary) as total_primary,
    //             x1.name as item_name, x1.has_subdetail
    //             "))
    //         ->orderby('a.id', 'asc');

    //     $where_clause = "";
    //     if ($request->s == "form")
    //     {   if ( $request->grin_no )
    //         {   $results->where("grin_no", "ilike", "%".$request->grin_no."%"); 
    //             // $where_clause .= "and a.grin_no ilike '%".$request->grin_no."%' ";
    //         };
    //     };
    //     return $results->get();
    // }
    public static function detailPrintOut($grin_no)
    {   
        $results = DB::table('grin_detail as a')
            ->leftjoin('m_item as x1', 'x1.item_id', '=', 'a.item_id')
            ->select(DB::raw("
                a.grin_no, a.item_id, 
                sum(a.quantity_primary) as quantity_primary, 
                sum(a.reject_primary) as reject_primary, 
                sum(a.quantity_primary + a.reject_primary) as total_primary, 
                a.um_primary, a.unit_price, sum(a.amount), x1.name as item_name
                "))
            ->where("a.grin_no", "=", $grin_no)
            ->groupby('a.grin_no', 'a.item_id', 'a.um_primary',
                    'a.unit_price', 'x1.name')
            ->orderby('x1.name');
        return $results->get();
    }
    public static function grindetailSave($json_data, $site, $user_id, $grin_no)
    {   if (count($json_data) > 0)
        {   // transaction control ikut parent orders
            foreach ($json_data as $json) 
            {   // save hardware
                if (trim($json->remark) == 'REMOVED')
                {   // remove record
                    // dd($json);
                    $result = static::where('id', '=', $json->id)->delete();
                    $result = [true, "Save succcefully"];
                }
                else
                {   // save record
                    if ($json->created_date == '')  
                    {   $GrinDetail = new GrinDetail();
                        $GrinDetail->id         = $GrinDetail->getNextID($GrinDetail->sequence_name, $site);
                        $GrinDetail->grin_no    = trim($grin_no);
                        $GrinDetail->item_id    = trim($json->item_id);
                        $GrinDetail->created_by = $user_id;
                    }
                    else 
                    {   // update hardware
                        $GrinDetail = GrinDetail::where('id', '=', $json->id)->first();
                        $GrinDetail->modified_by   = $user_id;
                        $GrinDetail->modified_date = Date('d/m/Y');
                    };

                    $GrinDetail->unit_price       = $json->unit_price;
                    $GrinDetail->po_qty_primary   = $json->po_qty_primary;
                    $GrinDetail->quantity_primary = $json->quantity_primary;
                    $GrinDetail->um_primary       = $json->um_primary;
                    $GrinDetail->unit_price       = $json->unit_price;
                    $GrinDetail->pcs_subdtl       = $json->pcs_subdtl;
                    $GrinDetail->remark           = $json->remark;
                    if ($json->has_subdetail == 1) 
                        {   $GrinDetail->reject_primary = 0; }
                    else 
                        { $GrinDetail->reject_primary   = $json->reject_primary;    };
                    
                    $result = $GrinDetail->save();
                };
            };
        };
        return $result;
    }
    public static function grindetailDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function subdetailSyncronize($request)
    {   DB::beginTransaction();
        try 
        {   $result = DB::statement("
                update grin_detail
                set quantity_primary = b.total_qty,
                    reject_pecah = b.reject_pecah,
                    reject_tunas = b.reject_tunas,
                    reject_muda = b.reject_muda,
                    reject_retak = b.reject_retak,
                    reject_basah = b.reject_basah,
                    reject_busuk = b.reject_busuk,
                    reject_primary = (b.reject_pecah + b.reject_tunas + b.reject_muda + b.reject_retak + b.reject_basah + b.reject_busuk)
                from (
                    select x.grin_no, 
                        sum(x.quantity_primary) as total_qty,
                        sum(x.reject_pecah) as reject_pecah,
                        sum(x.reject_tunas) as reject_tunas,
                        sum(x.reject_muda) as reject_muda,
                        sum(x.reject_retak) as reject_retak,
                        sum(x.reject_basah) as reject_basah,
                        sum(x.reject_busuk) as reject_busuk
                    from  grin_subdetail as x
                    where x.grin_no = '".$request->grin_no."'
                    group by x.grin_no
                ) as b
                where grin_detail.grin_no = b.grin_no
            ");
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentGrin()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Inventory\Grin', 'grin_no', 'grin_no');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
