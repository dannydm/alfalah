<?php

namespace App\Models\Inventory;

use App\Models\AppModel;
use DB;

class SiteLaborCosts extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'site_labor_costs';
	protected $primaryKey = 'id';
    public $sequence_name = 'site_labor_costs_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getStatus($status)
    {   switch($status)
        {   case 0: $result = "Active";  break;
            case 1: $result = "InActive";  break;
        };
        return $result;
    }
    public static function setStatus($status)
    {   switch($status)
        {   case "Active": $result = 0; break;
            case "InActive": $result = 1; break;
        };
        return $result;
    }
    public static function Search($request)
    {   //$results = static::orderBy('id', 'asc');
        $results = DB::table('site_labor_costs as a')
                ->join('m_site as b', function($join)
                    {   $join->on('a.company_id', '=', 'b.company_id')
                             ->on('a.site_id', '=', 'b.site_id');
                    })
                ->join('m_department as c', 'a.dept_id', '=', 'c.dept_id')
                ->select('a.*', 'b.name as site_name', 'c.name as dept_name');
        
        
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->symbol)
            {   $results->where("symbol", "ilike", "%".$request->symbol."%");   };
            if ($request->status)
            {   $results->where("status", "=", SiteLaborCosts::setStatus($request->status));   };
        };
        return $results->get();
    }
    public static function sitelaborcostsSave($json_data, $site, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {   if ($json->created_date == '')
                    {   
                        $sitelaborcosts               = new SiteLaborCosts();
                        $sitelaborcosts->id           = self::getNextID(self::sequence_name, $site);
                        $sitelaborcosts->company_id   = $json->company_id;
                        $sitelaborcosts->site_id      = $json->site_id;
                        $sitelaborcosts->dept_id      = $json->dept_id;
                        $sitelaborcosts->created_date = date('Ymd');

                    }
                    else
                    {   $sitelaborcosts = SiteLaborCosts::where('_id', '=', $json->id)->first();
                        $sitelaborcosts->modified_date  = date('Ymd');
                    };
                    
                    $sitelaborcosts->unloading_cost = $json->unloading_cost;
                    $sitelaborcosts->sorting_cost = trim($json->sorting_cost);
                    $sitelaborcosts->packing_cost= $json->packing_cost;
                    $sitelaborcosts->loading_cost= $json->loading_cost;
                    
                    $result = $sitelaborcosts->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function sitelaborcostsDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function countries()
    {   
        return $this->hasMany('App\Models\Country', 'sitelaborcosts_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
