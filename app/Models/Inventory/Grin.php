<?php

namespace App\Models\Inventory;

use App\Models\AppModel;
use App\Models\Inventory\GrinDetail;
use App\Models\Inventory\GrinSubDetail;
use App\Models\Administrator\SiteDocument;
use DB;

class Grin extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'grin_master';
	protected $primaryKey = 'grin_no';
    public $sequence_name = 'grin_master_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function headerPrintOut($request)
    {   
        $results = DB::table('grin_master as a')
            ->leftjoin('m_company as x1', 'x1.company_id', '=', 'a.company_id')
            ->leftjoin('m_users as x2', 'x2.user_id', '=', 'a.created_by')
            ->leftjoin('m_warehouse as x3', 'x3.warehouse_id', '=', 'a.warehouse_id')
            ->select(DB::raw("a.*,
                x1.name as company_name, 
                x2.name as created_by_name,
                x3.name as warehouse_name
                "))
            ->orderby('a.grin_no');

        if ($request->s == "form")
        {   
            if ( $request->id )
            {   $results->where("a.id", "=", $request->id); };
        };

        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };
    }
    
    /******************************************************
    **
    ** I N C O M I N G
    **
    *******************************************************/
    public static function increfSearch($request)
    {   
        if ($request->s == "form")
        {   //$results = static::orderBy('grin_no', 'asc');
            $results = DB::table('po_master as a')
                ->join('po_detail as b', 'a.po_no','=','b.po_no')
                ->leftjoin('m_company as c', 'a.supplier_id','=','c.company_id')
                ->leftjoin('m_company as c1', 'a.company_id','=','c1.company_id')
                ->leftjoin('m_item as d', 'b.item_id','=','d.item_id')
                ->select(DB::raw(" a.company_id, c1.name as company_name,
                    a.po_no as ref_no, 'PO' as ref_type, a.currency_id,
                    to_char(a.created_date, 'dd/mm/yyyy') as ref_date,
                    a.supplier_id as party_id, c.name as party_name,
                    b.item_id, d.name as item_name, '' as grin_no,
                    sum(b.quantity_primary) as quantity_primary,
                    b.um_primary,
                    (   select coalesce(sum(e2.quantity_primary + e2.reject_primary),0)
                        from grin_master e1 
                        join grin_detail e2 on (e1.grin_no = e2.grin_no)
                        where e1.ref_no = a.po_no and e2.item_id = b.item_id
                    ) as incoming_qty
                    "))
                ->where("a.approved_status", "=", 1)
                ->groupby('a.company_id', 'a.po_no', 'a.created_date', 'a.supplier_id',
                    'b.item_id', 'c.name', 'c1.name', 'd.name', 'b.um_primary')
                ->orderby('a.created_date', 'desc');
        };
        return $results->get();
    }
    public static function podetailSearch($request)
    {   
        if ($request->s == "form")
        {   //$results = static::orderBy('grin_no', 'asc');
            $subQuery = DB::table('po_detail as a')
                ->leftjoin('m_item as b', 'a.item_id','=','b.item_id')
                ->select(DB::raw("a.po_no as ref_no, 'PO' as ref_type, 
                    a.item_id, a.um_primary, a.unit_price,
                    sum(a.quantity_primary) as po_qty_primary, 
                    b.name as item_name, b.has_subdetail,
                    0 as quantity_primary, 
                    0 as reject_primary, 
                    0 as total_primary, 
                    0 as pcs_subdtl,
                    '' as remark, '' as created_date
                    "))
                // ->where("a.approved_status", "=", 1)
                ->groupby('a.po_no', 'a.item_id', 'a.um_primary', 'a.unit_price', 'b.name', 'b.has_subdetail');

                if ( $request->po_no )
                {   $subQuery->where("a.po_no", "=", trim($request->po_no)); };

            $results = Grin::selectRaw("x.*,
                    (   select coalesce(sum(e2.quantity_primary + e2.reject_primary),0)
                        from grin_master e1 
                        join grin_detail e2 on (e1.grin_no = e2.grin_no)
                        where e1.ref_no = x.ref_no and e2.item_id = x.item_id
                    ) as incoming_qty
                    ")
                ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                ->mergeBindings($subQuery)
                ->orderby('x.item_id', 'asc')
                ->orderby('x.ref_no', 'asc');
        };
        return $results->get();
    }
    public static function incomingSearch($request)
    {   
        if ($request->s == "form")
        {   //$results = static::orderBy('grin_no', 'asc');
            $results = DB::table('grin_master as a')
                ->join('m_company as b', 'a.company_id','=','b.company_id')
                ->leftjoin('m_company as d', 'a.party_id','=','d.company_id')
                ->leftjoin('m_warehouse as e', function($join)
                    {   $join->on('a.company_id', '=', 'e.company_id')
                             ->on('a.site_id', '=', 'e.site_id')
                             ->on('a.warehouse_id', '=', 'e.warehouse_id');
                    })
                ->select(DB::raw(" a.id,
                    a.company_id, a.grin_no, a.grin_type, 
                    a.party_id, a.party_delivery_no as delivery_no,
                    to_char(a.party_delivery_date, 'dd/mm/yyyy') as delivery_date,
                    (   case a.grin_status 
                            when 0 then 'Draft'
                            when 1 then 'Final'
                        end
                    ) as grin_status_name, a.grin_status,
                    to_char(a.created_date, 'dd/mm/yyyy') as grin_date,
                    a.site_id, a.warehouse_id, a.ref_no, a.ref_type,
                    a.curier_no, a.curier_name, a.curier_pic, a.curier_phone_no,
                    a.description, a.created_by, a.created_date,
                    a.modified_by, a.modified_date,
                    b.name as company_name, 
                    d.name as party_name, e.name as warehouse_name,
                    (   select max(x1.seq_no)
                        from grin_subdetail as x1
                        where x1.grin_no = a.grin_no
                    ) as max_seq_no
                    "))
                ->where("a.grin_type", "=", "ID")
                ->orderby('a.created_date', 'desc');

            if ( $request->grin_no )
            {   $results->where("a.grin_no", "ilike", "%".$request->grin_no."%");     };
            if ( $request->ref_no )
            {   $results->where("a.ref_no", "ilike", "%".$request->ref_no."%");     };
            if ( $request->party_name )
            {   $results->where("d.name", "ilike", "%".$request->party_name."%");     };
            // if ( $request->grin_date_1 && $request->grin_date_2)
            // {   $results->where("a.grin_date", "ilike", "%".$request->ref_no."%");     };
        };
        
        // dd($results->get());
        return $results->get();
    }
    public static function incomingSave($head_data, $json_data, $modi_data, $site, $user_id)
    {   
        // echo "<BR>sequence_name = ".$self->sequence_name;
        // echo "<BR>site = ".$site;
        // exit;
        
        if (count($head_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($head_data as $json) 
                {   // save  
                    if ($json->created_date == '')  
                    {   $grin             = new Grin();
                        // echo "<BR>sequence_name = ".$grin->sequence_name;
                        // echo "<BR>site = ".$site;
                        // exit;
                        $grin->id         = $grin->getNextID($grin->sequence_name, $site);
                        $grin->company_id = config('app.system_company');
                        $grin->ref_no     = trim($json->ref_no);
                        $grin->ref_type   = trim($json->ref_type);
                        $grin->created_by = $user_id;
                        $sitedocument     = new SiteDocument();
                        $grin->grin_no    = $sitedocument->getDocNum(config('app.system_company'),                  $site, 30, 3001, "", "");
                        $json->grin_no    = $grin->grin_no; 
                        $grin_detail = new GrinDetail();
                    }
                    else 
                    {   // update  
                        $grin = Grin::where('grin_no', '=', $json->grin_no)->first();
                        $grin->modified_by = $user_id;
                        
                        $grin_detail = GrinDetail::where('grin_no', '=', $json->grin_no)->first();
                        if (Empty($grin_detail)) { $grin_detail = new GrinDetail(); };
                    };

                    $grin->site_id             = $json->site_id;
                    $grin->warehouse_id        = $json->warehouse_id;
                    $grin->currency_id         = $json->currency_id;
                    $grin->party_id            = $json->party_id;
                    $grin->party_name          = $json->party_name;
                    $grin->party_delivery_no   = $json->delivery_no;
                    $grin->party_delivery_date = $json->delivery_date;
                    $grin->curier_no           = $json->curier_no;
                    $grin->curier_name         = $json->curier_name;
                    $grin->curier_pic          = $json->curier_pic;
                    $grin->curier_phone_no     = $json->curier_phone_no;
                    $grin->grin_status         = 0;
                    $grin->description         = $json->description;
                    
                    $result = $grin->save();

                    // save details
                    if ($result)
                    {   $result = $grin_detail->grindetailSave( $json_data, 
                                            $site, $user_id, $json->grin_no); 
                    };
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function incomingDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /******************************************************
    **
    ** O U T G O I N G      
    **
    *******************************************************/
    public static function outrefSearch($request)
    {   
        if ($request->s == "form")
        {   $results = DB::table('orders as a')
                ->join('order_detail as b', 'a.order_no','=','b.order_no')
                ->join('m_company as x', 'a.buyer_id','=','x.company_id')
                ->join('m_company as x1', 'a.company_id','=','x1.company_id')
                ->join('m_item as x2', 'b.item_id','=','x2.item_id')
                ->select(DB::raw(" a.company_id, a.buyer_id, a.order_no,
                    to_char(a.order_date, 'dd/mm/yyyy') as order_date,
                    b.item_id, 
                    b.quantity_nut_kgs as order_qty,
                    coalesce((   
                        select sum(y1.quantity_primary)
                        from grin_master y
                        join grin_detail y1 on y.grin_no = y1.grin_no
                        where y.ref_no = a.order_no 
                          and y.ref_type = 'OD'
                          and y1.item_id = b.item_id
                    ), 0) as outgoing_qty,
                    x.name as buyer_name, x1.name as company_name,
                    x2.name as item_name, x2.um_id as um_primary
                    "))
                ->orderby('a.created_date', 'desc');

                // quantity_order (b.quantity_nut_kgs) dikurangi quantity_pengeluaran_ke_order_tersebut
        };
        return $results->get();
    }
    public static function outrefDetailSearch($request, $mode)
    {    
        if ($request->s == "form")
        {   $subQuery_1_clause_1 = "";
            $subQuery_1_clause_2 = "";
            if ($mode == "search_grid")
            {   
                $subQuery_1_clause_1 = "a.company_id, a.buyer_id, 
                    a.order_no, 
                    to_char(a.order_date, 'dd/mm/yyyy') as order_date, ";

                if ( $request->company_id )
                {   $subQuery_1_clause_2 = $subQuery_1_clause_2." and y.company_id = '".$request->company_id."' "; };
                if ( $request->site_id )
                {   $subQuery_1_clause_2 = $subQuery_1_clause_2." and y.site_id = '".$request->site_id."' "; };
                if ( $request->warehouse_id )
                {   $subQuery_1_clause_2 = $subQuery_1_clause_2." and y.warehouse_id = '".$request->warehouse_id."' "; };
            };
            $subQuery_1 = DB::table('orders as a')
                ->join('order_detail as b', 'a.order_no','=','b.order_no')
                ->join('m_item as x', 'b.item_id','=','x.item_id')
                ->select(DB::raw($subQuery_1_clause_1."
                    a.order_no as ref_no, b.item_id, x.name as item_name,
                    b.quantity_nut_kgs as order_qty, 
                    coalesce((   
                        select sum(y1.quantity_primary)
                        from grin_master y
                        join grin_detail y1 on y.grin_no = y1.grin_no
                        where trim(y.ref_no) = trim(a.order_no) 
                          and y.grin_type = 'OD'
                          and y.grin_subtype = 'O'
                          and y1.item_id = b.item_id ".$subQuery_1_clause_2."
                    ), 0) as orderout_qty,
                    x.um_id as um_primary, x.has_subdetail
                    "));

                if ( $request->ref_no )
                {   $subQuery_1->where("a.order_no", "=", trim($request->ref_no)); };
                // if ( $request->warehouse_id )
                // {   $subQuery_1->where("a.company_id", "=", trim($request->company_id))
                //         ->where("a.site_id", "=", trim($request->site_id))
                //         ->where("a.warehouse_id", "=", trim($request->warehouse_id)); 
                // };
        };


        $subQuery_2_clause = "";
        if ($mode == "search_grid")
        {   if ( $request->company_id )
                {   $subQuery_2_clause = $subQuery_2_clause." and e1.company_id = '".$request->company_id."' "; };
                if ( $request->site_id )
                {   $subQuery_1_clause_2 = $subQuery_2_clause." and e1.site_id = '".$request->site_id."' "; };
                if ( $request->warehouse_id )
                {   $subQuery_1_clause_2 = $subQuery_2_clause." and e1.warehouse_id = '".$request->warehouse_id."' "; };
        };
        $subQuery_2 = Grin::selectRaw("x.*, 
                coalesce((   
                    select coalesce(sum(e2.quantity_primary),0)
                    from grin_master e1 
                    join grin_detail e2 on (e1.grin_no = e2.grin_no)
                    where e2.item_id = x.item_id
                      and e1.grin_type = 'ID' ".$subQuery_2_clause."
                ),0) as incoming_qty,
                coalesce((   
                    select coalesce(sum(e2.quantity_primary),0)
                    from grin_master e1 
                    join grin_detail e2 on (e1.grin_no = e2.grin_no)
                    where e2.item_id = x.item_id
                      and e1.grin_type = 'OD' 
                      and e1.grin_subtype = 'O' ".$subQuery_2_clause."
                ),0) as outgoing_qty
                ")
            ->from(\DB::raw(' ( '.$subQuery_1->toSql().' ) as x'));

        $subQuery_3_clause = "";
        if ($mode == "search_grid")
        {   $subQuery_3_clause = ", y1.name as buyer_name, y2.name as company_name "; };
        $subQuery_3 = Grin::selectRaw("y.*, 
                (y.order_qty - y.orderout_qty) as remain_qty,
                (y.incoming_qty - y.outgoing_qty) as stock_qty,
                0 as quantity_primary, 0 as unit_price, 0 as amount, 0 as po_qty_primary,
                '' as remark, '' as created_date
                ".$subQuery_3_clause)
            ->from(\DB::raw(' ( '.$subQuery_2->toSql().' ) as y'));

        if ($mode == "search_grid")
        {   $subQuery_3->join('m_company as y1', 'y.buyer_id','=','y1.company_id')
                ->join('m_company as y2', 'y.company_id','=','y2.company_id');
        };

        $select_clause = "";
        $results = Grin::selectRaw("z.*")
            ->from(\DB::raw(' ( '.$subQuery_3->toSql().' ) as z'))
            ->mergeBindings($subQuery_1)
            // ->where('z.stock_qty', '>', 0)
            ->orderby('z.item_id', 'asc')
            ->orderby('z.ref_no', 'asc');
        
        if ( $request->stocks == "YES")
        {   $results->where("z.stock_qty", ">", 0); };
        if ( $request->stocks == "NO")
        {   $results->where("z.stock_qty", "<", 1); };

        return $results->get();
    }
    public static function outdetailSearch($request)
    {   
        if ($request->s == "form")
        {   //$results = static::orderBy('grin_no', 'asc');
            $results = DB::table('grin_detail as a')
                ->leftjoin('m_item as b', 'a.item_id','=','b.item_id')
                ->select(DB::raw("a.grin_no as ref_no, grin_type as ref_type, 
                    a.item_id, a.um_primary, 
                    sum(a.quantity_primary) as ref_qty_primary, 
                    b.name as item_name, 0 as quantity_primary, 
                    '' as remark, '' as created_date
                    "))
                ->groupby('a.grin_no', 'a.item_id', 'a.um_primary', 'b.name')
                ->orderby('a.item_id', 'desc');

                if ( $request->grin_no )
                {   $results->where("a.grin_no", "=", trim($request->grin_no)); };
        };
        return $results->get();
    }
    // subtype  = I = Incoming
    //          = O = Order
    //          = G = General
    public static function outgoingSearch($request, $subtype)
    {   
        if ($request->s == "form")
        {   //$results = static::orderBy('grin_no', 'asc');
            $results = DB::table('grin_master as a')
                ->join('m_company as b', 'a.company_id','=','b.company_id')
                ->join('m_site as c', 'a.site_id','=','c.site_id')
                ->leftjoin('m_warehouse as d', 'a.warehouse_id','=','d.warehouse_id')
                ->select(DB::raw(" a.id,
                    a.company_id, a.grin_no, a.grin_type, 
                    a.grin_status, a.ref_no, a.ref_type, a.currency_id,
                    a.party_id, a.party_name, a.party_delivery_no, a.party_delivery_date,
                    to_char(a.created_date, 'dd/mm/yyyy') as grin_date,
                    a.description, a.site_id, a.warehouse_id, 
                    a.curier_no, a.curier_name, a.curier_pic, a.curier_phone_no,
                    a.created_by, a.created_date,
                    a.modified_by, a.modified_date,
                    b.name as company_name, c.name as site_name,
                    d.name as warehouse_name
                    "))
                ->where("a.grin_type", "=", "OD")
                ->where("a.grin_subtype", "=", $subtype)
                ->orderby('a.created_date', 'desc');

            if ( $request->company_id )
            {   $results->where("a.company_id", "=", trim($request->company_id)); };
            if ( $request->site_id )
            {   $results->where("a.site_id", "=", trim($request->site_id)); };
            if ( $request->warehouse_id )
            {   $results->where("a.warehouse_id", "=", trim($request->warehouse_id)); };
        };
        
        // dd($results->get());
        return $results->get();
    }
    public static function outgoingSave(
        $subtype, $head_data, $json_data, $modi_data, $site, $user_id)
    {   if (count($head_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($head_data as $json) 
                {   // save  
                    if ($json->created_date == '')  
                    {   
                        $grin               = new Grin();
                        $grin->id           = self::getNextID(self::sequence_name, $site);
                        $sitedocument       = new SiteDocument();
                        $grin->grin_no      = $sitedocument->getDocNum(config('app.system_company'), $site, 30, 3002, "", "");
                        $grin->company_id   = config('app.system_company');
                        $grin->created_by   = $user_id;
                        $json->grin_no      = $grin->grin_no;
                        $grin_detail        = new GrinDetail();
                        $grin_subdetail     = new GrinSubDetail();
                    }
                    else 
                    {   // update  
                        $grin               = Grin::where('grin_no', '=', $json->grin_no)->first();
                        $grin->modified_by  = $user_id;
                        $grin->modified_date= new date();
                        
                        $grin_detail        = GrinDetail::where('grin_no', '=', $json->grin_no)->first();
                        $grin_subdetail        = GrinSubDetail::where('grin_no', '=', $json->grin_no)->first();
                        if (Empty($grin_detail)) 
                            {   $grin_detail    = new GrinDetail(); 
                                $grin_subdetail = new GrinSubDetail();
                            };
                    };
                    $grin->grin_type           = 'OD';
                    $grin->grin_subtype        = $subtype;
                    $grin->ref_no              = trim($json->ref_no);
                    $grin->ref_type            = $json->ref_type;
                    $grin->currency_id         = $json->currency_id;
                    $grin->party_id            = $json->party_id;
                    $grin->party_name          = $json->party_name;
                    $grin->party_delivery_no   = $json->delivery_no;
                    $grin->party_delivery_date = $json->delivery_date;
                    $grin->site_id             = $json->site_id;
                    $grin->warehouse_id        = $json->warehouse_id;
                    $grin->description         = $json->description;   
                    $grin->curier_no           = $json->curier_no;
                    $grin->curier_name         = $json->curier_name;
                    $grin->curier_pic          = $json->curier_pic;
                    $grin->curier_phone_no     = $json->curier_phone_no;
                    
                    $result = $grin->save();

                    //save order details
                    if ($result)
                    {   $result = $grin_detail->grindetailSave( $json_data,
                                            $site, $user_id, $json->grin_no); 
                        if ($subtype == 'O') // outgoing order has subdetail transactions
                        {   $result = $grin_subdetail->outgoingSave( $modi_data,
                                            $site, $user_id, $json->grin_no);
                        };
                        
                    };
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function outgoingDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /******************************************************
    **
    ** S T O C K S
    **
    *******************************************************/
    public static function StockOutgoingOrderFindItems($request)
    {
        if ($request->s == "form")
        {   $subQuery_1 = DB::table('grin_master as a')
                        ->join('grin_detail as b', 'a.grin_no','=','b.grin_no')
                        ->join('grin_subdetail as c', function($join)
                            {   $join->on('c.grin_no',   '=', 'b.grin_no')
                                     ->on('c.item_id',   '=', 'b.item_id');
                            })
                        ->selectRaw(" '' as id, a.company_id, a.site_id, a.warehouse_id, 
                            a.grin_no as ref_no, a.grin_type as ref_type, 
                            a.party_name, a.party_id,
                            c.group_no, c.item_id, 
                            sum(c.quantity_primary) as incoming_qty")
                        ->where("a.grin_type", "=", "ID")
                        ->groupby('a.company_id', 'a.site_id', 'a.warehouse_id',
                            'a.grin_no','a.grin_type', 'a.party_name', 'a.party_id',
                            'c.group_no','c.item_id');

            if ($request->company_id)
            {   $subQuery_1->where("a.company_id", "=", $request->company_id); };
            if ($request->site_id)
            {   $subQuery_1->where("a.site_id", "=", $request->site_id);    };
            if ($request->warehouse_id)
            {   $subQuery_1->where("a.warehouse_id", "=", $request->warehouse_id);  };
            if ($request->item_id)
            {   $subQuery_1->where("b.item_id", "=", $request->item_id);  };
            // main query
            $subQuery_2 = Grin::selectRaw("x1.*, 
                        coalesce((   
                            select sum(y2.quantity_primary)
                            from grin_master y
                            join grin_detail y1 on y.grin_no = y1.grin_no
                            join grin_subdetail y2 on y2.grin_no = y1.grin_no and y2.item_id = y1.item_id
                            where y.grin_type = 'OD'
                              and y.grin_subtype = 'O' 
                              and trim(y1.item_id) = trim(x1.item_id)
                              and trim(y2.stock_ref_no) = trim(x1.ref_no)
                              and y2.group_no = x1.group_no
                        ), 0) as outgoing_qty,
                        x2.name as item_name, x2.um_id as um_primary,
                        x3.name as warehouse_name
                        ")
                ->from(\DB::raw(' ( '.$subQuery_1->toSql().' ) as x1'))
                ->join('m_item as x2', 'x2.item_id','=','x1.item_id')
                ->leftjoin('m_warehouse as x3', function($join)
                    {   $join->on('x3.company_id',   '=', 'x1.company_id')
                             ->on('x3.site_id',      '=', 'x1.site_id')
                             ->on('x3.warehouse_id', '=', 'x1.warehouse_id');
                    });
            
            $subQuery_3 = Grin::selectRaw("x4.*, 
                            (x4.incoming_qty - x4.outgoing_qty) as available_qty") 
                ->from(\DB::raw(' ( '.$subQuery_2->toSql().' ) as x4'));
            
            $results = Grin::selectRaw("x5.*") 
                ->from(\DB::raw(' ( '.$subQuery_3->toSql().' ) as x5'))
                ->mergeBindings($subQuery_1) // because where paramater only on subQuery_1
                ->where('x5.available_qty', '>', 0)
                ->orderby('x5.company_id', 'asc')
                ->orderby('x5.site_id', 'asc')
                ->orderby('x5.warehouse_id', 'asc')
                ->orderby('x5.item_id', 'asc')
                ->orderby('x5.group_no', 'asc');
        };
        return $results->get();
    }
    public static function StockOutgoingGeneralFindItems($request)
    {
        // select x.*,
            // (   select coalesce(sum(z1.quantity_primary),0)
            //     from grin_detail as z1
            //     where z1.stock_ref_no = x.ref_no and z1.item_id = x.item_id
            // ) as outgoing, x2.name as item_name
        // from
        //     (   select a.company_id, a.site_id, a.warehouse_id,
        //                 a.grin_no as ref_no, a.grin_type as ref_type,
        //                 b.item_id, sum(quantity_primary) as incoming
        //             from grin_master a
        //             join grin_detail b on (a.grin_no = b.grin_no)
        //             where a.grin_type ='ID'
        //             group by a.company_id, a.site_id, a.warehouse_id, 
        //                 a.grin_no, a.grin_type, b.item_id
        //     ) as x
        //     join m_item x2 on (x.item_id = x2.item_id)
        if ($request->s == "form")
        {   $subQuery_1 = DB::table('grin_master as a')
                        ->join('grin_detail as b', 'a.grin_no','=','b.grin_no')
                        ->selectRaw("a.company_id, a.site_id, a.warehouse_id, 
                            a.grin_no as ref_no, a.grin_type as ref_type, b.item_id,
                            sum(b.quantity_primary) as inc_qty")
                        ->where("a.grin_type", "=", "ID")
                        ->groupby('a.company_id', 'a.site_id', 'a.warehouse_id',
                            'a.grin_no','a.grin_type', 'b.item_id');

            if ($request->company_id)
            {   $subQuery_1->where("a.company_id", "=", $request->company_id); };
            if ($request->site_id)
            {   $subQuery_1->where("a.site_id", "=", $request->site_id);    };
            if ($request->warehouse_id)
            {   $subQuery_1->where("a.warehouse_id", "=", $request->warehouse_id);  };


            // main query
            $subQuery_2 = Grin::selectRaw("x1.*, 
                        (   select coalesce(sum(z1.quantity_primary),0)
                            from grin_detail as z1
                            where z1.stock_ref_no = x1.ref_no and z1.item_id = x1.item_id
                        ) as out_qty, 
                        x2.name as item_name, x2.um_id as um_primary,
                        x3.name as warehouse_name
                        ")
                ->from(\DB::raw(' ( '.$subQuery_1->toSql().' ) as x1'))
                ->join('m_item as x2', 'x2.item_id','=','x1.item_id')
                ->leftjoin('m_warehouse as x3', function($join)
                    {   $join->on('x3.company_id',   '=', 'x1.company_id')
                             ->on('x3.site_id',      '=', 'x1.site_id')
                             ->on('x3.warehouse_id', '=', 'x1.warehouse_id');
                    });
            
            $results = Grin::selectRaw("x2.*, (inc_qty - out_qty) as available_qty")
                ->from(\DB::raw(' ( '.$subQuery_2->toSql().' ) as x2'))
                ->mergeBindings($subQuery_1) // because where paramater only on subQuery_1
                ->orderby('x2.company_id', 'asc')
                ->orderby('x2.site_id', 'asc')
                ->orderby('x2.warehouse_id', 'asc')
                ->orderby('x2.item_id', 'asc');

            
        };
        return $results->get();
    }
    public static function StockAvailable($request)
    {   
        // select x1.*, x2.name as item_name
        // from (
        //     select b.item_id, sum(quantity_primary) as incoming, 0 as outgoing
        //     from grin_master a
        //     join grin_detail b on (a.grin_no = b.grin_no)
        //     where a.grin_type ='ID'
        //     group by b.item_id
        //     union all
        //     select b.item_id, 0 as incoming , sum(quantity_primary) as outgoing
        //     from grin_master a
        //     join grin_detail b on (a.grin_no = b.grin_no)
        //     where a.grin_type ='OD'
        //     and a.grin_subtype = 'O'
        //     group by b.item_id
        // ) as x1
        // join m_item as x2 on (x2.item_id = x1.item_id)
        // order by x1.item_id

        if ($request->s == "form")
        {   
            $subQuery_2 = DB::table('grin_master as a')
                    ->join('grin_detail as b', 'a.grin_no','=','b.grin_no')
                    ->selectRaw("a.company_id, a.site_id, a.warehouse_id, 
                        b.stock_grin_no as ref_no, b.item_id, 
                        0 as inc_qty, sum(b.quantity_primary) as out_qty")
                    ->where("a.grin_type", "=", "OD")
                    ->where("a.grin_subtype", '=', "O")
                    ->groupby('a.company_id', 'a.site_id', 'a.warehouse_id',
                        'b.stock_grin_no', 'b.item_id');
            // union query
            $subQuery_1 = DB::table('grin_master as a')
                        ->join('grin_detail as b', 'a.grin_no','=','b.grin_no')
                        ->selectRaw("a.company_id, a.site_id, a.warehouse_id, 
                            a.grin_no as ref_no, b.item_id,
                            sum(b.quantity_primary) as inc_qty, 0 as out_qty")
                        ->where("a.grin_type", "=", "ID")
                        ->groupby('a.company_id', 'a.site_id', 'a.warehouse_id',
                            'a.grin_no', 'b.item_id')
                        ->union($subQuery_2);
            // main query
            $results = Grin::selectRaw("x1.*, (x1.inc_qty - x1.out_qty) as available_qty,
                            x2.name as item_name, x2.um_id as um_primary,
                            x3.name as warehouse_name ")
                    ->from(\DB::raw(' ( '.$subQuery_1->toSql().' ) as x1'))
                    ->mergeBindings($subQuery_1)
                    ->join('m_item as x2', 'x2.item_id','=','x1.item_id')
                    ->leftjoin('m_warehouse as x3', function($join)
                    {   $join->on('x3.company_id',   '=', 'x1.company_id')
                             ->on('x3.site_id',      '=', 'x1.site_id')
                             ->on('x3.warehouse_id', '=', 'x1.warehouse_id');
                    })
                    ->orderby('x1.company_id', 'asc')
                    ->orderby('x1.site_id', 'asc')
                    ->orderby('x1.warehouse_id', 'asc')
                    ->orderby('x1.item_id', 'asc');

                if ($request->company_id)
                {   $subQuery_1->where("a.company_id", "=", $request->company_id);
                    $subQuery_2->where("a.company_id", "=", $request->company_id);
                };
                if ($request->site_id)
                {   $subQuery_1->where("a.site_id", "=", $request->site_id);
                    $subQuery_2->where("a.site_id", "=", $request->site_id);
                };
                if ($request->warehouse_id)
                {   $subQuery_1->where("a.warehouse_id", "=", $request->warehouse_id);
                    $subQuery_2->where("a.warehouse_id", "=", $request->warehouse_id);
                };
        };
        return $results->get();
    }
    
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
