<?php

namespace App\Models\Inventory;

use App\Models\AppModel;
use DB;

class GrinSubDetail extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'grin_subdetail';
	protected $primaryKey = 'id';
    public $sequence_name = 'grin_subdetail_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function referenceSearch($request)
    {   
        $results = DB::table('grin_master as a')
            ->join('m_company as b', 'a.company_id','=','b.company_id')
            ->join('grin_detail as c', 'a.grin_no','=','c.grin_no')
            ->join('m_item as x1', 'x1.item_id', '=', 'c.item_id')
            ->leftjoin('m_company as d', 'a.party_id','=','d.company_id')
            ->leftjoin('m_warehouse as e', function($join)
                {   $join->on('a.company_id', '=', 'e.company_id')
                         ->on('a.site_id', '=', 'e.site_id')
                         ->on('a.warehouse_id', '=', 'e.warehouse_id');
                })
            ->select(DB::raw(" a.id,
                a.company_id, a.grin_no, a.grin_type, 
                a.party_id, a.party_delivery_no as delivery_no,
                to_char(a.party_delivery_date, 'dd/mm/yyyy') as delivery_date,
                (   case a.grin_status 
                        when 0 then 'Draft'
                        when 1 then 'Final'
                    end
                ) as grin_status_name, a.grin_status,
                to_char(a.created_date, 'dd/mm/yyyy') as grin_date,
                a.site_id, a.warehouse_id, a.ref_no, a.ref_type,
                a.curier_no, a.curier_name, a.curier_pic, a.curier_phone_no,
                a.description, a.created_by, a.created_date,
                a.modified_by, a.modified_date,
                b.name as company_name, 
                d.name as party_name, e.name as warehouse_name,
                c.item_id, c.um_primary, 
                (c.quantity_primary + c.reject_primary) as quantity_primary, 
                c.reject_primary,
                x1.name as item_name,
                (   select sum(z.quantity_primary)
                    from grin_subdetail as z
                    where z.grin_no = a.grin_no and z.item_id = c.item_id
                ) as subdetail_qty,
                (   select max(z.seq_no)
                    from grin_subdetail as z
                    where z.grin_no = a.grin_no and z.item_id = c.item_id
                ) as max_seq_no
                "))
            ->where("a.grin_type", "=", "ID")
            ->where("x1.has_subdetail", "=", 1)
            ->orderby('a.grin_no', 'desc');


        $where_clause = "";
        if ($request->s == "form")
        {   if ( $request->grin_no )
            {   $results->where("a.grin_no", "ilike", "%".$request->grin_no."%"); 
                // $where_clause .= "and a.grin_no ilike '%".$request->grin_no."%' ";
            };
        };
        return $results->get();
    }
    public static function Search($request)
    {   //$results = static::orderby('id', 'asc');
        $results = DB::table('grin_detail as a')
            ->leftjoin('grin_subdetail as b', function($join)
                    {   $join->on('a.grin_no', '=', 'b.grin_no')
                             ->on('a.item_id', '=', 'b.item_id');
                    })
            ->leftjoin('m_item as x1', 'x1.item_id', '=', 'a.item_id')
            ->selectRaw("
                a.grin_no, a.item_id, 
                b.id, coalesce(b.seq_no,0) as seq_no, 
                a.um_primary, 
                coalesce(b.quantity_primary,0) as quantity_primary, 
                coalesce(b.reject_pecah,0) as reject_pecah, 
                coalesce(b.reject_tunas,0) as reject_tunas, 
                coalesce(b.reject_muda,0) as reject_muda, 
                coalesce(b.reject_retak,0) as reject_retak, 
                coalesce(b.reject_basah,0) as reject_basah, 
                coalesce(b.reject_busuk,0) as reject_busuk,
                coalesce(b.group_no,0) as group_no,
                b.remark, b.created_by, b.created_date,
                x1.name as item_name")
            ->orderby('b.seq_no', 'desc');

        $where_clause = "";
        if ($request->s == "form")
        {   if ( $request->grin_no )
            {   $results->where("a.grin_no", "ilike", "%".$request->grin_no."%"); 
                // $where_clause .= "and a.grin_no ilike '%".$request->grin_no."%' ";
            };
        };
        return $results->get();
    }
    public static function ViewOutgoingOrder($request)
    {   //$results = static::orderby('id', 'asc');
        $results = DB::table('grin_subdetail as a')
            ->leftjoin('grin_master as b', 'b.grin_no', '=', 'a.stock_ref_no')
            ->leftjoin('m_item as x1', 'x1.item_id', '=', 'a.item_id')
            ->selectRaw("a.*, b.party_name as stock_party_name,
                x1.name as item_name")
            ->orderby('a.seq_no', 'desc');

        $where_clause = "";
        if ($request->s == "form")
        {   if ( $request->grin_no )
            {   $results->where("a.grin_no", "ilike", "%".$request->grin_no."%"); 
                // $where_clause .= "and a.grin_no ilike '%".$request->grin_no."%' ";
            };
        };
        return $results->get();
    }
    public static function searchPassedItemsPerGroup($request)
    {   //$results = static::orderby('id', 'asc');
        if ($request->s == "form")
        {   
            $results = DB::table('grin_detail as a')
                ->leftjoin('grin_subdetail as b', function($join)
                        {   $join->on('a.grin_no', '=', 'b.grin_no')
                                 ->on('a.item_id', '=', 'b.item_id');
                        })
                ->leftjoin('grin_subdetail as c', function($join)
                        {   $join->on('c.stock_ref_no', '=', 'b.grin_no')
                                 ->on('c.item_id', '=', 'b.item_id')
                                 ->on('c.group_no', '=', 'b.group_no')
                                 ->on('c.seq_no', '=', 'b.seq_no');
                        })
                ->leftjoin('m_item as x1', 'x1.item_id', '=', 'a.item_id')
                ->selectRaw("
                    a.grin_no, a.item_id,  
                    b.id, coalesce(b.seq_no,0) as seq_no, 
                    a.um_primary, 
                    coalesce(b.quantity_primary,0) as quantity_primary,
                    coalesce(b.group_no,0) as group_no,
                    b.remark, b.created_by, b.created_date,
                    c.seq_no as od_seq_no,
                    x1.name as item_name")
                ->where("b.quantity_primary", ">", 0) //passed_items
                ->whereNull("c.seq_no")
                ->orderby('b.id', 'desc');

            $where_clause = "";
            
            if ( $request->grin_no )
            {   $results->where("a.grin_no", "ilike", "%".$request->grin_no."%"); };
            if ( $request->item_id )
            {   $results->where("a.item_id", "ilike", "%".$request->item_id."%"); };
            if ( $request->group_no )
            {   $results->where("b.group_no", "=", $request->group_no); };
        };
        return $results->get();
    }
    public static function subdetailPrintOut($grin_no)
    {   
        $results = DB::table('grin_subdetail as a')
            ->leftjoin('m_item as x1', 'x1.item_id', '=', 'a.item_id')
            ->select(DB::raw("
                a.grin_no, a.item_id, 
                sum(a.quantity_primary) as quantity_primary, 
                sum(a.reject_pecah) as reject_pecah, 
                sum(a.reject_tunas) as reject_tunas, 
                sum(a.reject_muda) as reject_muda, 
                sum(a.reject_retak) as reject_retak, 
                sum(a.reject_basah) as reject_basah, 
                sum(a.reject_busuk) as reject_busuk, 
                a.um_primary, x1.name as item_name
                "))
            ->where("a.grin_no", "=", $grin_no)
            ->groupby('a.grin_no', 'a.item_id', 'a.um_primary', 'x1.name')
            ->orderby('x1.name');

        return $results->get();
    }
    public static function incomingSave($json_data, $site, $user_id)
    {   if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {   // save 
                    if (trim($json->remark) == 'REMOVED')
                    {   // remove record
                        $result = static::where('id', '=', $json->id)->delete();
                        $result = [true, "Save succcefully"];
                    }
                    else
                    {   // save new record
                        if ($json->created_date == '')  
                        {   $GrinSubDetail             = new GrinSubDetail();
                            $GrinSubDetail->id         = self::getNextID(self::sequence_name, $site);
                            $GrinSubDetail->grin_no    = trim($json->grin_no);
                            $GrinSubDetail->item_id    = trim($json->item_id);
                            $GrinSubDetail->seq_no     = $json->seq_no;
                            $GrinSubDetail->created_by = $user_id;
                        }
                        else 
                        {   // update record
                            $GrinSubDetail = GrinSubDetail::where('id', '=', $json->id)->first();
                            $GrinSubDetail->modified_by   = $user_id;
                            $GrinSubDetail->modified_date = Date('d/m/Y');
                        };

                        $GrinSubDetail->quantity_primary = $json->quantity_primary;
                        $GrinSubDetail->reject_pecah     = $json->reject_pecah;
                        $GrinSubDetail->reject_tunas     = $json->reject_tunas;
                        $GrinSubDetail->reject_muda      = $json->reject_muda;
                        $GrinSubDetail->reject_retak     = $json->reject_retak;
                        $GrinSubDetail->reject_basah     = $json->reject_basah;
                        $GrinSubDetail->reject_busuk     = $json->reject_busuk;
                        $GrinSubDetail->group_no         = $json->group_no;
                        $GrinSubDetail->um_primary       = $json->um_primary;
                        $GrinSubDetail->remark           = $json->remark;
                        
                        $result = $GrinSubDetail->save();
                    };
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function outgoingSave($json_data, $site, $user_id, $grin_no)
    {   if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {   // save
                    if ($grin_no) { $json->grin_no = $grin_no; };
                     
                    if (trim($json->remark) == 'REMOVED')
                    {   // remove record
                        $result = static::where('id', '=', $json->id)->delete();
                        $result = [true, "Save succcefully"];
                    }
                    else
                    {   // save new record
                        if ($json->created_date == '')  
                        {   $GrinSubDetail             = new GrinSubDetail();
                            $GrinSubDetail->id         = self::getNextID(self::sequence_name, $site);
                            $GrinSubDetail->grin_no    = trim($json->grin_no);
                            $GrinSubDetail->item_id    = trim($json->item_id);
                            $GrinSubDetail->seq_no     = $json->seq_no;
                            $GrinSubDetail->created_by = $user_id;
                        }
                        else 
                        {   // update record
                            $GrinSubDetail = GrinSubDetail::where('id', '=', $json->id)->first();
                            $GrinSubDetail->modified_by   = $user_id;
                            $GrinSubDetail->modified_date = Date('d/m/Y');
                        };

                        $GrinSubDetail->quantity_primary = $json->quantity_primary;
                        $GrinSubDetail->reject_pecah     = $json->reject_pecah;
                        $GrinSubDetail->reject_tunas     = $json->reject_tunas;
                        $GrinSubDetail->reject_muda      = $json->reject_muda;
                        $GrinSubDetail->reject_retak     = $json->reject_retak;
                        $GrinSubDetail->reject_basah     = $json->reject_basah;
                        $GrinSubDetail->reject_busuk     = $json->reject_busuk;
                        $GrinSubDetail->group_no         = $json->group_no;
                        $GrinSubDetail->um_primary       = $json->um_primary;
                        $GrinSubDetail->remark           = $json->remark;
                        
                        if (isset($json->stock_ref_no)) //check if stock_ref_no set
                        {   $GrinSubDetail->stock_ref_no  = $json->stock_ref_no;
                            $GrinSubDetail->stock_ref_type= $json->stock_ref_type;
                            $GrinSubDetail->stock_grin_no = $json->stock_grin_no;
                        };

                        $result = $GrinSubDetail->save();
                    };
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function grinsubdetailDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentGrinDetail()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Inventory\GrinDetail', 'grin_no', 'grin_no');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
