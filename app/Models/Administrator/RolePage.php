<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class RolePage extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'role_page';
	protected $primaryKey = 'id';
    // public $sequence_name = 'role_page_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function SearchRolePage($request)
    {   $arr = array();
        $my_arr = array();
        // $results = DB::select('
        //     select a.*, b.name as role_name, c.name as user_name, c.link as user_link
        //     from role_users a
        //     join m_role b on (a.role_id = b.role_id)
        //     join m_users c on (a.user_id = c.user_id)
        //     ', []);
        $results = DB::table('role_page as a')
                        ->join('m_role as b', 'a.role_id', '=', 'b.role_id')
                        ->select('a.*', 'b.name as role_name');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->role_name)
            {   $results->where("b.name", "ilike", "%".$request->role_name."%");   };
            if ($request->page_name)
            {   $results->where("a.pg", "ilike", "%".$request->page_name."%");   };
        }
        elseif ($request->s == "combo")
        {   if ($request->role_id)
            {   $results->where("a.role_id", "=", $request->role_id);   };
        };
        return $results->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function role()
    {
        return $this->belongTo('Role');
    }

    public function user()
    {
        return $this->belongTo('MPage');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
