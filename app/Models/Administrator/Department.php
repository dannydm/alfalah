<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Department extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_department';
	protected $primaryKey = 'dept_id';
    // public $sequence_name = 'm_department_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getType($status)
    {   switch($status)
        {   case 0: $result = "Division";  break;
            case 1: $result = "Department";  break;
            case 2: $result = "Section";  break;
        };
        return $result;
    }
    public static function setType($status)
    {   switch($status)
        {   case "Division":   $result = 0; break;
            case "Department": $result = 1; break;
            case "Section":    $result = 2; break;
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function searchDepartment($request)
    {   $results = static::orderBy('dept_id', 'asc');

        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->dept_id)
            {   $results->where("dept_id", "=", $request->dept_id);   };
            if ($request->parent_dept_id)
            {   $results->where("parent_dept_id", "=", $request->parent_dept_id);   };
            if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->description)
            {   $results->where("description", "ilike", "%".$request->description."%");   };
            if ($request->type)
            {   $results->where("type", "=", self::setType($request->type));   };
        };
        return $results->get();
    }
    public static function departmentSearchExt($request)
    {   
        $subQuery = DB::table('m_department as a')
                    ->selectRaw("a.dept_id, a.name,
                                (a.name || ' [' || a.dept_id || '] ') as display");

        $results = Company::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.dept_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function departmentSave($json_data, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {
                    if (Empty($json->created_date))
                    {   $department = new Department(); 
                        $department->created_date = date('Ymd');
                        $department->dept_id = $json->dept_id;
                        
                    }
                    else
                    {   $department  = Department::where('dept_id', '=', $json->dept_id)
                                    ->first();
                    };
                    $department->parent_dept_id = $json->parent_dept_id;
                    $department->name = $json->name;
                    $department->abreviation = $json->abreviation;
                    $department->type = $json->type;
                    $department->description = $json->description;
                    $department->dept_email = $json->dept_email;
                    $department->acct_class = $json->acct_class;

                    $result     = $department->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function departmentDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function Childs() //circular reference
    {
        return $this->hasMany('App\Models\Department', 'dept_id', 'parent_dept_id');
    }
    public function SiteDepartment()
    {
        return $this->hasMany('App\Models\SiteDepartment', 'dept_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
