<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Sumberdana extends AppModel
{    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_sumberdana';
    protected $primaryKey = 'sumberdana_id';
    // public $sequence_name = 'm_sumberdana_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function sumberdanaSearch($request)
    {
        $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY sumberdana_id)) as record_id, * ")->orderBy('sumberdana_id', 'asc');
        // searchForm parameter
        if ($request->s == "form") {
            if ($request->name) {
                $results->where("name", "ilike", "%" . $request->name . "%");
            };
            if ($request->status) {
                $results->where("status", "=", self::setStatus($request->status));
            };
        };
        return $results->get();
    }
    public static function sumberdanaSearchExt($request)
    {
        $subQuery = DB::table('m_sumberdana as a')
            ->selectRaw("a.sumberdana_id,
                                (' [' || a.sumberdana_id || '] ' || a.name) as display")
            ->where('a.status', '=', 0);

        $results = sumberdana::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.sumberdana_id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }
    public static function sumberdanaSave($json_data, $type, $user_id)
    {
        $result = false;
        if (count($json_data) > 0) {   // begin transaction
            DB::beginTransaction();
            try {
                foreach ($json_data as $json) {
                    if ($json->created_date == '') {
                        $sumberdana = new Sumberdana();
                        $sumberdana->created_date  = date('Ymd');
                    } else {
                        $sumberdana = Sumberdana::where('sumberdana_id', '=', $json->sumberdana_id)->first();
                        $sumberdana->modified_date  = date('Ymd');
                    };
                    $sumberdana->sumberdana_id   = $json->sumberdana_id;
                    $sumberdana->name   = $json->name;
                    $sumberdana->status = $json->status;
                    $result = $sumberdana->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function sumberdanaDelete($request)
    {
        DB::beginTransaction();
        try {
            $result = static::where('sumberdana_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Pk_sumberdana()
    {
        return $this->hasMany('App\Models\Sumberdana', 'sumberdana_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
