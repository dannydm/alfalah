<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Document extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_documents';
	protected $primaryKey = 'doc_id';
    // public $sequence_name = 'm_documents_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function searchDocument($request)
    {   //$results = static::orderBy('doc_id', 'asc');
        $results = DB::table('m_documents as a')
                ->join('m_department as b', 'a.owner_dept_id', '=', 'b.dept_id')
                ->select('a.*', 'b.name as dept_name');

        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->doc_id)
            {   $results->where("doc_id", "=", $request->doc_id);   };
            if ($request->doc_code)
            {   $results->where("doc_code", "=", $request->doc_code);   };
            if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->description)
            {   $results->where("description", "ilike", "%".$request->description."%");   };
        };
        return $results->get();
    }
    public static function documentSearchExt($request)
    {   
        $subQuery = DB::table('m_document as a')
                    ->selectRaw("a.doc_id, a.doc_code, a.name,
                                (a.name || ' [' || a.doc_id || '] ') as display");

        $results = Company::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.doc_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function documentSave($json_data, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) {
                    if (Empty($json->created_date))
                    {   $document = new Document(); 
                        $document->doc_id = $json->doc_id;
                        $document->created_date = date('Ymd');
                    }
                    else
                    {   $document  = Document::where('doc_id', '=', $json->doc_id)
                                    ->first();
                    };

                    $document->doc_code = $json->doc_code;
                    $document->name = $json->name;
                    $document->description = $json->description;
                    $document->owner_dept_id = $json->owner_dept_id;
                    $document->format_doc_no = $json->format_doc_no;
                    $document->seq_digit = $json->seq_digit;

                    $result     = $document->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function documentDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('doc_id', '=', $request->doc_id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function SiteDocument()
    {
        return $this->hasMany('App\Models\SiteDocument', 'doc_id');
    }
    public function parentDepartment()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Department', 'owner_dept_id', 'dept_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
