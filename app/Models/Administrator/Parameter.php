<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Parameter extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_parameters';
	protected $primaryKey = 'param_name';
    // public $sequence_name = 'm_parameters_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function parameterSearch($request)
    {   

        $results = static::orderBy('param_name', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->param_name)
            {   $results->where("param_name", "ilike", "%".$request->param_name."%");   };
            if ($request->param_value)
            {   $results->where("param_value", "ilike", "%".$request->param_value."%");   };
        };
        return $results->get();
    }
    public static function parameterSave($json_data, $user_id)
    {   
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {
                    if ($json->created_date == '')
                    {   $parameter = new Parameter();
                        $parameter->param_name   = $json->param_name;
                        $parameter->created_date  = date('Ymd');
                    }
                    else
                    {   $parameter = Parameter::where('param_name', '=', $json->param_name)->first();
                    };
                    
                    $parameter->param_value = $json->param_value;
                    $parameter->description = $json->description;
                    // if ($json->param_big_value)
                    // {   
                    //     // $parameter->param_big_value = base64_encode($json->param_big_value);
                    //     $parameter->param_big_value = $json->param_big_value;
                    // };
                    
                    $result = $parameter->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function parameterDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('param_name', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function BigValueSave($data, $user_id)
    {   
        if ($data->param_name)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   
                $parameter = Parameter::where('param_name', '=', $data->param_name)->first();
                $parameter->param_big_value = $data->param_big_value;
                $result = $parameter->save();
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function getPPDB($request) 
    {   $result = static::select("param_big_value")
                    ->where('param_name', '=', 'ppdb_syarat')->first();
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
