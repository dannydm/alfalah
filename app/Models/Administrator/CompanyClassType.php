<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class CompanyClassType extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'company_class_type';
	protected $primaryKey = 'id';
    // public $sequence_name = 'company_class_type_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getArea($status)
    {   switch($status)
        {   case 0: $result = "Local";  break;
            case 1: $result = "Foreign";  break;
        };
        return $result;
    }
    public static function setArea($status)
    {   switch($status)
        {   case "Local": $result = 0; break;
            case "Foreign": $result = 1; break;
        };
        return $result;
    }
    public static function companyClassSearch($request)
    {   $arr = array();
        $my_arr = array();
        $results = DB::table('company_class_type as a')
                ->join('m_company as b', 'a.company_id', '=', 'b.company_id')
                ->join('m_company_type as c', 'a.company_type', '=', 'c.company_type')
                ->select(DB::raw("
                    a.*, b.name as company_name, c.name as company_type_name,
                    ( case a.area_type when 0 then 'Local' 
                        else 'Foreign' 
                        end ) as area_type_name
                    "));

        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->company_id)
            {   $results->where("a.company_id", "ilike", "%".$request->company_id."%");   };
            if ($request->company_type)
            {   $results->where("c.name", "ilike", "%".$request->company_type."%");   };
            if ($request->area_type)
            {   $results->where("a.area_type", "ilike", "%".$request->area_type."%");   };    
        };
        return $results->get();
    }
    public static function companyClassSearchExt($request)
    {   
        $subQuery = DB::table('company_class_type as a')
                    ->join('m_company as b', 'a.company_id', '=', 'b.company_id')
                    ->selectRaw("a.company_id, a.company_type, b.name, a.area_type,
                                (b.name || ' [' || a.company_id || '] ') as display")
                    ->where('b.status', '=', 0);
        if ($request->s == "form")
        {   if ($request->company_type)
            {   $subQuery->where("a.company_type", "=", $request->company_type);   };
        };

        $results = Company::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.company_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
            if ($request->t)
            {   $results->where("x.company_type", "=", $request->t);   };
        };
        return $results->get();
    }
    public static function companyClassSave($json_data, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) {
                    if ($json->created_date == '')
                    {   $companyclasstype = new CompanyClassType();
                        
                        $companyclasstype->company_id    = $json->company_id;
                        $companyclasstype->created_date  = date('Ymd');
                    }
                    else
                    {   $companyclasstype = CompanyClassType::where('company_id', '=', $json->company_id)
                                                ->where('company_type', '=', $json->company_type)
                                                ->where('area_type', '=', $json->area_type)
                                                ->first();
                    };
                    $companyclasstype->company_type        = $json->company_type;
                    $companyclasstype->area_type           = CompanyClassType::setArea($json->area_type_name);
                    $companyclasstype->last_update  = date('Ymd');

                    $result     = $companyclasstype->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function companyClassDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function parentCompany()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Company', 'company_id', 'company_id');
    }
    public function parentCompanyType()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\CompanyType', 'company_type', 'company_type');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
