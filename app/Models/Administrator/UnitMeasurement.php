<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class UnitMeasurement extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_um';
	protected $primaryKey = 'um_id';
    // public $sequence_name = 'm_um_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function umSearch($request)
    {   $results = static::orderBy('um_id', 'asc');

        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->um_id)
            {   $results->where("um_id", "=", $request->um_id);   };
            if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->description)
            {   $results->where("description", "ilike", "%".$request->description."%");   };
        };
        return $results->get();
    }
    public static function umSearchExt($request)
    {   
        $subQuery = DB::table('m_um as a')
                    ->selectRaw("a.um_id, a.name,
                                (a.description || ' [' || a.um_id || '] ') as display");

        $results = Company::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.um_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
