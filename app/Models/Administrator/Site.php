<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Site extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_site';
	protected $primaryKey = 'id';
    // public $sequence_name = 'm_site_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getArea($status)
    {   switch($status)
        {   case 0: $result = "Local";  break;
            case 1: $result = "Foreign";  break;
        };
        return $result;
    }
    public static function setArea($status)
    {   switch($status)
        {   case "Local": $result   = 0; break;
            case "Foreign": $result = 1; break;
        };
        return $result;
    }
    public static function getStatus($status)
    {   switch($status)
        {   case 0: $result = "Active";  break;
            case 1: $result = "Inactive";  break;
        };
        return $result;
    }
    public static function setStatus($status)
    {   switch($status)
        {   case "Active": $result   = 0; break;
            case "Inactive": $result = 1; break;
        };
        return $result;
    }
    public static function searchSite($request)
    {   $results = DB::table('m_site as a')
                ->join('m_company as b', 'a.company_id', '=', 'b.company_id')
                ->join('m_company_type as c', 'a.company_type', '=', 'c.company_type')
                ->select(DB::raw("
                    a.*, b.name as company_name, c.name as company_type_name
                    "));
        // $results = static::orderBy('site_id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->company_id)
            {   $results->where("a.company_id", "ilike", "%".$request->company_id."%");   };
            if ($request->site_id)
            {   $results->where("a.site_id", "ilike", "%".$request->site_id."%");   };
            if ($request->company_type)
            {   $results->where("c.name", "ilike", "%".$request->company_type."%");   };
            if ($request->area_type)
            {   $results->where("a.area_type", "ilike", "%".$request->area_type."%");   };
            // if ($request->status)
            // {   $results->where("status", "=", Company::setStatus($request->status));   };
        };
        return $results->get();
    }
    public static function siteSearchExt($request)
    {   $subQuery = DB::table('m_site as a')
                    ->selectRaw("a.country_id, a.company_id, a.site_id,
                                (a.company_id || '-' || a.site_id || '-'||a.name) as display");

        $results = Site::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.company_id', 'asc')
                    ->orderby('x.site_id', 'asc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };

        return $results->get();
    }
    public static function siteSave($json_data, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) {
                    if (Empty($json->created_date))
                    {   $sites = new Site(); 
                        $sites->company_id   = $json->company_id;
                        $sites->site_id      = $json->site_id;
                        $sites->area_type    = $json->area_type;
                        $sites->created_date = date('Ymd');
                    }
                    else
                    {   
                        $sites  = Site::where('company_id', '=', $json->company_id)
                                    ->where('site_id', '=', $json->site_id)
                                    ->first();
                    };

                    $sites->name         = $json->name;
                    $sites->country_id   = $json->country_id;
                    $sites->company_type = $json->company_type;
                    $sites->status       = $json->status;
                    $sites->doc_code     = $json->doc_code;

                    $result     = $sites->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function siteDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentCompany()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Company', 'company_id', 'company_id');
    }
    public function parentCountry()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Country', 'country_id', 'country_id');
    }
    public function parentCompanyType()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\CompanyType', 'company_type', 'company_type');
    }
    public function SiteDepartment()
    {   return $this->hasMany('App\Models\SiteDepartment', 'site_id');    }
    public function SiteDocument()
    {   return $this->hasMany('App\Models\SiteDocument', 'site_id');    }
    public function SiteBuilding()
    {   return $this->hasMany('App\Models\SiteBuilding', 'site_id');    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
