<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use App\Models\Administrator\SiteDocument;
use App\Models\Administrator\SiteDocumentBuyer;
use App\Models\Administrator\SiteDocumentVendor;
use DB;

class Periodeak extends AppModel
{    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_periodeak';
    protected $primaryKey = 'periodeak_id';
    // public $sequence_name = 'm_periodeak_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function periodeakSearch($request)
    {
        $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY periodeak_id)) as record_id, * ")->orderBy('periodeak_id', 'asc');
        // searchForm parameter
        if ($request->s == "form") {
            if ($request->name) {
                $results->where("name", "ilike", "%" . $request->name . "%");
            };
            if ($request->status) {
                $results->where("status", "=", self::setStatus($request->status));
            };
        };
        return $results->get();
    }
    public static function periodeakSearchExt($request)
    {
        $subQuery = DB::table('m_periodeak as a')
            ->selectRaw("a.periodeak_id,
                                (' [' || a.periodeak_id || '] ' || a.name) as display")
            ->where('a.status', '=', 0);

        $results = Periodeak::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.periodeak_id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }
    public static function periodeakSave($json_data, $type, $user_id)
    {
        if (count($json_data) > 0) {   // begin transaction
            DB::beginTransaction();
            try {
                foreach ($json_data as $json) {
                    if ($json->created_date == '') {
                        $periodeak = new Periodeak();
                        $periodeak->created_date  = date('Ymd');
                    } else {
                        $periodeak = Periodeak::where('periodeak_id', '=', $json->periodeak_id)->first();
                        $periodeak->last_update  = date('Ymd');
                    };
                    $periodeak->periodeak_id   = $json->periodeak_id;
                    $periodeak->bulan_id   = $json->bulan_id;
                    $periodeak->name   = $json->name;
                    $periodeak->tahun  = $json->tahun;
                    $periodeak->status = $json->status;
                    $result = $periodeak->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function periodeakDelete($request)
    {
        DB::beginTransaction();
        try {
            $result = static::where('periodeak_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function documentSearch($request)
    {   DB::enableQueryLog();
        $results = DB::table('site_documents as a')
        // ->join('m_rapbs as x1', 'x1.dept_id', '=', 'a.dept_id')
        ->leftjoin('m_rapbs as x2', 'x2.dept_id', '=', 'a.dept_id')
        ->select(DB::raw("a.id, x2.dept_id, x2.name as urusan_mrapbs_name, 
        a.dept_id, a.last_doc_no , a.um_last_doc , a.doc_id,a.created_date, a.last_generate_date
        "))
        ->where('x2.type','=',2) 
 //       ->groupby('a.doc_id','a.dept_id','x2.dept_id','x2.name','a.last_doc_no','a.um_last_doc')
       
        ->orderby('a.dept_id');

        if ($request->s == "form")
        {   
            if ( $request->dept_id )
            {   $results->where("a.dept_id", "=", $request->dept_id); };
            if ($request->urusan_mrapbs_name)
            {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_name."%");  
            };
        };
        $x = $results->get();
        // print_r(DB::getQueryLog()); exit;
        return $x;       
    }

    public static function saveDocumentNumber($json_data, $site, $user_id, $company_id) //inject site document number
    { 
        DB::beginTransaction();
        try { 
            foreach ($json_data as $data) 
            { // dd($data->last_doc_no);exit;
                if ( $data->created_date == '')
                    {  }
                else {
                    $sitedocument = SiteDocument::where('dept_id', '=', $data->dept_id)->first();
                    $sitedocument->last_doc_no = $data->last_doc_no; 
                    $sitedocument->um_last_doc = $data->um_last_doc;
                    $sitedocument->last_generate_date  = date("Y-m-d H:i:s");
                    $result =  $sitedocument->save();
                };
            };
            // commit transaction
            $result = [true, "Save succcefully"];
            DB::commit();
            }
            catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Pk_periodeak()
    {
        return $this->hasMany('App\Models\periodeak', 'periodeak_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
