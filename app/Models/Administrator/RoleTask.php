<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class RoleTask extends AppModel
{   /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------
    */
    // CREATE SEQUENCE role_task_seq;
    // CREATE TABLE public.role_task
    // (
    //   id numeric(18,0) NOT NULL DEFAULT nextval('role_task_seq'::regclass),
    //   role_id numeric(18,0) NOT NULL,
    //   menu_id numeric(18,0) NOT NULL,
    //   task_action varchar(32) NOT NULL,
    //   created_date timestamp without time zone DEFAULT now(),
    //   modified_date timestamp without time zone DEFAULT now(),
    //   CONSTRAINT role_task_pk PRIMARY KEY (role_id, menu_id, task_action),
    //   CONSTRAINT role_task_ux UNIQUE (id),
    //   CONSTRAINT m_menus2_fk FOREIGN KEY (menu_id) references m_menus(menu_id),
    //   CONSTRAINT m_roles1_fk FOREIGN KEY (role_id) references m_role(role_id)
    // );
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'role_task';
    protected $primaryKey = 'id';
    // public $sequence_name = 'role_task_seq';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   
        $results = DB::table('role_task as a')
                    ->selectRaw("a.*, b.name as menu_name, c.name as role_name ")
                    ->join('m_menus as b', 'a.menu_id', '=', 'b.menu_id')
                    ->join('m_role as c', 'a.role_id', '=', 'c.role_id')
                    ->orderby('a.id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->menu_id)
            {   $results->where("a.menu_id", "ilike", "%".$request->menu_id."%");   };
            if ($request->menu_name)
            {   $results->where("b.name", "ilike", "%".$request->menu_name."%");   };
            if ($request->task_action)
            {   $results->where("a.task_action", "ilike", "%".$request->task_action."%");   };
            if ($request->role_id)
            {   $results->where("a.role_id", "=", $request->role_id);   };
            if ($request->role_name)
            {   $results->where("b.name", "ilike", "%".$request->role_name."%");   };
        };
        return $results->get();
    }
    public static function SearchExt($request)
    {   
        $subQuery = DB::table('m_menu_task as a')
                    ->selectRaw("a._id, a.name,
                                (a.name || ' [' || a.company_id || '] ') as display")
                    ->where('a.status', '=', 0);

        $results = Menu::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.company_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function taskSave($json_data, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   
                foreach ($json_data as $json) 
                {
                    $RoleTask = RoleTask::where('role_id', '=', $json->role_id)
                                    ->where('menu_id', '=', $json->menu_id)
                                    ->where('task_action', '=', $json->task_action)
                                    ->first();
                    if ($RoleTask){}
                    else
                    {
                        $RoleTask = new RoleTask();
                        $RoleTask->role_id      = $json->role_id;
                        $RoleTask->menu_id      = $json->menu_id;
                        $RoleTask->task_action  =  strtoupper($json->task_action);

                        $result = $RoleTask->save();
                    };
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function taskDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
