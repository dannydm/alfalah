<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Country extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_country';
	protected $primaryKey = 'country_id';
    // public $sequence_name = 'm_country_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function countrySearch($request)
    {   $arr = array();
        $my_arr = array();
        $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY country_id)) as recid, * ")->orderBy('country_id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->capitol)
            {   $results->where("capitol", "ilike", "%".$request->capitol."%");   };
            if ($request->currency)
            {   $results->where("currency_id", "ilike", "%".$request->currency."%");   };
            if ($request->status)
            {   $results->where("status", "=", self::setStatus($request->status));   };
        };
        return $results->get();
    }
    public static function countrySearchExt($request)
    {   
        $subQuery = DB::table('m_country as a')
                    ->selectRaw("a.country_id,
                                (a.name || ' [' || a.country_id || '] ') as display")
                    ->where('a.status', '=', 0);

        $results = Country::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.country_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function countrySave($json_data, $type, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) {
                    if ($json->created_date == '')
                    {   $country = new Country();
                        $country->created_date  = date('Ymd');
                    }
                    else
                    {   $country = Country::where('country_id', '=', $json->country_id)->first();
                    };

                    $country->name        = $json->name;
                    $country->capitol     = $json->capitol;
                    $country->currency_id = $json->currency_id;
                    $country->phone_area  = $json->phone_area;
                    $country->status      = $json->status;
                    $result     = $country->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function countryDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('country_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function City()
    {
        return $this->hasMany('App\Models\City', 'country_id');
    }
    public function Company()
    {
        return $this->hasMany('App\Models\Company', 'country_id');
    }
    public function Site()
    {
        return $this->hasMany('App\Models\Site', 'country_id');
    }
    public function parentCurrency()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsToMany('App\Models\Currency', 'currency_id', 'currency_id');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
