<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class UserPage extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'user_page';
	protected $primaryKey = 'id';
    // public $sequence_name = 'user_page_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function SearchUserPage($request)
    {   $arr = array();
        $my_arr = array();
        // $results = DB::select('
        //     select a.*, b.name as user_name, c.name as user_name, c.link as user_link
        //     from user_users a
        //     join m_user b on (a.user_id = b.user_id)
        //     join m_users c on (a.user_id = c.user_id)
        //     ', []);
        $results = DB::table('user_page as a')
                        ->join('m_users as b', 'a.user_id', '=', 'b.user_id')
                        ->join('m_role as c', 'a.role_id', '=', 'c.role_id')
                        ->select('a.*', 'b.username', 'c.name as role_name');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->user_name)
            {   $results->where("b.name", "ilike", "%".$request->user_name."%");   };
            if ($request->page)
            {   $results->where("a.pgg", "ilike", "%".$request->page."%");   };
            if ($request->event)
            {   $results->where("a.evv", "ilike", "%".$request->event."%");   };
        }
        elseif ($request->s == "combo")
        {   if ($request->user_id)
            {   $results->where("a.user_id", "=", $request->user_id);   };
        };
        return $results->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    // public function role()
    // {
    //     return $this->belongTo('User');
    // }

    // public function user()
    // {
    //     return $this->belongTo('MPage');
    // }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
