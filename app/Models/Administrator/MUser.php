<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use App\User;
use Illuminate\Support\Facades\Hash;
use DB;

class MUser extends AppModel
{   /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_users';
    protected $primaryKey = 'user_id';
    // public $sequence_name = 'm_users_';
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function userrole()
    {
        return $this->hasMany('App\Models\Administrator\RoleUser', 'user_id');
    }
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function SearchUser($request)
    {   $arr = array();
        $my_arr = array();
        $results = DB::table('m_users as a')
                ->leftjoin('users as b', 'a.laravel_id', '=', 'b.id')
                ->leftjoin('m_department as d', 'a.dept_id', '=', 'd.dept_id')
                ->select(DB::raw("
                    a.*, 
                    b.email as laravel_email,
                    b.name as laravel_username, 
                    d.name as dept_name"))
                ->orderby("a.user_id");

        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("a.name", "ilike", "%".$request->name."%");   };
            if ($request->status)
            {   $results->where("a.status", "=", self::setStatus($request->status));   };
            if ($request->type == "User")
            {   $results->where("a.note", "=", null);   }
            else { $results->where("a.note", "=", $request->type); };
        };
        return $results->get();
    }
    public static function userSearchExt($request)
    {   $subQuery = DB::table('m_users as a')
                    ->selectRaw("a.user_id, a.username,
                                (a.user_id || ' [' || a.username || '] ') as display")
                    ->where('a.status', '=', 0);

        $results = MUser::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.user_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function laraveluserSearchExt($request)
    {   $subQuery = DB::table('users as a')
                    ->selectRaw("a.id as user_id, a.name as username,
                                (a.id || ' [' || a.name || '] ') as display");

        $results = MUser::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.user_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function userSave($json_data, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {   $v_name        = $json->name;
                    $v_auth_email  = $json->auth_email;
                    $v_laravel_id  = $json->laravel_id;

                    if ($json->created == '')
                    {   $user = new MUser();
                        $user->user_id  = $json->user_id;
                    }
                    else
                    {   $user = MUser::where('user_id', '=', $json->user_id)->first();  };
                    
                    // $user->parent_user_id       = $json->parent_user_id;
                    $user->username    = $json->username;
                    $user->name        = $v_name;
                    $user->password    = $json->password;
                    $user->auth_email  = $v_auth_email;
                    $user->laravel_id  = $v_laravel_id;
                    $user->status      = $json->status;
                    // $user->created  = $json->created;
                    $user->company_id  = $json->company_id;
                    $user->site_id     = $json->site_id;
                    $user->dept_id     = $json->dept_id;
                    
                    $result     = $user->save();
                    
                    if ($v_laravel_id == '')
                    {}
                    else
                    {   $larauser  = User::where('id', '=', $json->laravel_id)->first();
                        $larauser->email = $json->auth_email;
                        $larauser->name  = $json->name;

                        $result  = $larauser->save();
                    };
                    // echo "<BR> lanjut terus sss ";
                    // exit;
                    // $user->approved = $json->approved;
                    // $user->closed               = $json->closed;
                    // $user->last_login           = $json->last_login;
                    // $user->last_notify          = $json->last_notify;
                    // $user->last_fail            = $json->last_fail;
                    // $user->failure_count        = $json->failure_count;
                    // $user->max_failures         = $json->max_failures;
                    // $user->failure_audit_period = $json->failure_audit_period;
                    // $user->note                 = $json->note;
                    // $user->is_crypted           = $json->is_crypted;
                    // $user->auth_answer          = $json->auth_answer;
                    // $user->auth_question        = $json->auth_question;
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }

    public static function ResetPassword($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('user_id', '=', $request->id)->first();
            if ($result)
            {   $user = User::where('id', '=', $result->laravel_id)->first();
                if ($user)
                {   $user->password = Hash::make($request->txt);
                    $user->save();
                    
                    if ($user)
                    {   $result = [true, "Save succcefully"];
                        DB::commit();
                    }
                    else
                    {   $result = [false, "Failed to Save on User"];
                        DB::rollback();
                    };
                }
                else
                {   $result = [false, "Users not found "];
                    DB::rollback();
                };
            }
            else
            {   $result = [false, "User not found "];
                DB::rollback();
            };          
            
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
