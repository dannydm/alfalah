<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class MenuTask extends AppModel
{   /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------
    */
    // CREATE SEQUENCE m_menutask_seq;
    // CREATE TABLE public.m_menu_task
    // (
    //   id numeric(18,0) NOT NULL DEFAULT nextval('m_menutask_seq'::regclass),
    //   menu_id numeric(18,0) NOT NULL,
    //   task_action varchar(32) NOT NULL,
    //   created_date timestamp without time zone DEFAULT now(),
    //   modified_date timestamp without time zone DEFAULT now(),
    //   CONSTRAINT m_menutask_pk PRIMARY KEY (menu_id, task_action),
    //   CONSTRAINT m_menutask_ux UNIQUE (id),
    //   CONSTRAINT n_menus1_fk FOREIGN KEY (menu_id) references m_menus(menu_id)
    // );
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_menu_task';
    protected $primaryKey = 'id';
    // public $sequence_name = 'm_menu_task_seq';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   
        $results = DB::table('m_menu_task as a')
                    ->selectRaw("a.*, b.name as menu_name ")
                    ->join('m_menus as b', 'a.menu_id', '=', 'b.menu_id')
                    ->orderby('a.id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->menu_id)
            {   $results->where("a.menu_id", "ilike", "%".$request->menu_id."%");   };
            if ($request->menu_name)
            {   $results->where("b.name", "ilike", "%".$request->menu_name."%");   };
            if ($request->task_action)
            {   $results->where("a.task_action", "ilike", "%".$request->task_action."%");   };
        };
        return $results->get();
    }
    public static function SearchExt($request)
    {   
        $subQuery = DB::table('m_menu_task as a')
                    ->selectRaw("a._id, a.name,
                                (a.name || ' [' || a.company_id || '] ') as display")
                    ->where('a.status', '=', 0);

        $results = Menu::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.company_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function taskSave($json_data, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   
                foreach ($json_data as $json) 
                {
                    if ($json->created_date == '')
                    {   $MenuTask = new MenuTask();  } 
                    else
                    {   $MenuTask = MenuTask::where('id', '=', $json->id)->first();
                    };
                    
                    $MenuTask->menu_id      = $json->menu_id;
                    $MenuTask->task_action  =  strtoupper($json->task_action);

                    $result = $MenuTask->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function taskDelete($json_data)
    {   if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {
                    $result = static::where('id', '=', $json->id)->delete();
                };
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
