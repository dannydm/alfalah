<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class ContainerType extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_container_type';
	protected $primaryKey = 'container_type_id';
    public $sequence_name = 'm_container_type_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function Search($request)
    {   $arr = array();
        $my_arr = array();
        $results = static::orderBy('container_type_id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("description", "ilike", "%".$request->description."%");   };
            if ($request->container_type_id)
            {   $results->where("container_type_id", "ilike", "%".$request->container_type_id."%");   };
        };
        return $results->get();
    }
    public static function SearchExt($request)
    {   
        $subQuery = DB::table('m_container_type as a')
                    ->selectRaw("a.container_type_id, a.description,
                                (a.container_type_id|| ' [' || a.description|| '] ') as display");

        $results = ContainerType::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.container_type_id', 'asc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function containertypeSave($json_data, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) {
                    if ($json->created_date == '')
                    {   $container_type = new ContainerType();
                        $container_type->container_type_id = $json->container_type_id;
                    }
                    else
                    {   $container_type = ContainerType::where('container_type_id', '=', $json->container_type_id)->first();
                    };
                    
                    $container_type->description = $json->description;
                    $result     = $container_type->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function containertypeDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('container_type_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
