<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class RoleMenu extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'role_menus';
	protected $primaryKey = 'rolemenu_id';
    // public $sequence_name = 'role_menus_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function SearchRoleMenu($request)
    {   $arr = array();
        $my_arr = array();
        // $results = DB::select('
        //     select a.*, b.name as role_name, c.name as menu_name, c.link as menu_link
        //     from role_menus a
        //     join m_role b on (a.role_id = b.role_id)
        //     join m_menus c on (a.menu_id = c.menu_id)
        //     ', []);

        $results = DB::table('role_menus as a')
                        ->join('m_role as b', 'a.role_id', '=', 'b.role_id')
                        ->join('m_menus as c', 'a.menu_id', '=', 'c.menu_id')
                        ->select('a.*', 'b.name as role_name', 'c.name as menu_name', 'c.link as menu_link');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->role_name)
            {   $results->where("b.name", "ilike", "%".$request->role_name."%");   };
            if ($request->menu_name)
            {   $results->where("c.name", "ilike", "%".$request->menu_name."%");   };
            if ($request->menu_link)
            {   $results->where("c.link", "ilike", "%".$request->menu_link."%");   };
        }
        elseif ($request->s == "combo")
        {   if ($request->role_id)
            {   $results->where("a.role_id", "=", $request->role_id);   };
        };
        return $results->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function role()
    {
        return $this->belongTo('Role');
    }

    public function menu()
    {
        return $this->belongTo('Menu');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
