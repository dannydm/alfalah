<?php

namespace App\Models\Administrator;

use App\Models\AppModel; 
use DB;

class Mincome extends AppModel
{    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_income';
    protected $primaryKey = 'mincome_id';
    // public $sequence_name = 'm_income_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function MincomeSearch($request)
    {
        $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY mincome_id)) as record_id, * ")->orderBy('mincome_id', 'asc');
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->mincome_name) 
            {   $results->where("mincome_name", "ilike", "%" . $request->mincome_name . "%");   };
            if ($request->status) 
            {   $results->where("status", "=", self::setStatus($request->status));  };
        };
        return $results->get();
    }
    public static function MincomeSearchExt($request)
    {
        $subQuery = DB::table('m_income as a')
            ->selectRaw("a.mincome_id, a.mincome_name as mincome_name,
                                (' [' || a.mincome_id || '] ' || a.mincome_name) as display")
            ->where('a.status', '=', 0);

        $results = mincome::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.mincome_id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }
    public static function MincomeSave($json_data, $type, $user_id)
    {
        if (count($json_data) > 0) {   // begin transaction
            DB::beginTransaction();
            try {
                foreach ($json_data as $json) {
                    if ($json->created_date == '') {
                        $mincome = new Mincome();
                        $mincome->created_date  = date('Ymd');
                    } else {
                        $mincome = Mincome::where('mincome_id', '=', $json->mincome_id)->first();
                        $mincome->modified_date  = date('Ymd');
                    };
                    $mincome->mincome_id   = $json->mincome_id;
                    $mincome->parent_mincome_id   = $json->parent_mincome_id;
                    $mincome->mincome_name   = $json->mincome_name;
                    $mincome->mincome_value   = $json->mincome_value;
                    $mincome->sumberdana_id   = $json->sumberdana_id;
                    $mincome->status = $json->status;
                    $result = $mincome->save();
                };
                // commit transaction
                $result = [true, "Save succesfully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function mincomeDelete($request)
    {
        DB::beginTransaction();
        try {
            $result = static::where('mincome_id', '=', $request->mincome_id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Pk_Mincome()
    {
        return $this->hasMany('App\Models\Mincome', 'mincome_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
