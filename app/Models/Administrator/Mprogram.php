<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Mprogram extends AppModel
{    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_program';
    protected $primaryKey = 'mprogram_id';
    // public $sequence_name = 'm_mprogram_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function mprogramSearch($request)
    {
        $results = static::selectRaw(" * ")->orderBy('mprogram_id', 'asc');
        // searchForm parameter
        if ($request->s == "form") {
            if ($request->name) 
            {   $results->where("name", "ilike", "%" . $request->name . "%");   };

            if ($request->status) 
            {   $results->where("status", "=", self::setStatus($request->status));  };
            
            if ($request->type) 
            {   $results->where("type", "ilike", "%" . $request->type . "%");   };

            if ($request->parent) 
            {   $results->where("parent_mprogram_id", "=", $request->parent);   };
        };
        return $results->get();
    }
    public static function mprogramSearchExt($request)
    {
        $subQuery = DB::table('m_mprogram as a')
            ->selectRaw("a.mprogram_id,
                                (' [' || a.mprogram_id || '] ' || a.name) as display")
            ->where('a.status', '=', 0);

        $results = mprogram::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.mprogram_id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }
    public static function mprogramSave($json_data, $type, $user_id)
    {
        // print_r($json_data); exit;
        if (count($json_data) > 0) {   // begin transaction
            DB::beginTransaction();
            try {
                foreach ($json_data as $json) {
                    if ($json->created_date == '') {
                        $mprogram = new mprogram();
                        $mprogram->created_date  = date('Ymd');
                        $mprogram->mprogram_id   = $json->mprogram_id;
                    } else {
                        $mprogram = mprogram::where('mprogram_id', '=', $json->mprogram_id)->first();
                        $mprogram->modified_date  = date('Ymd');
                    };
                    $mprogram->parent_mprogram_id   = $json->parent_mprogram_id;
                    $mprogram->name   = $json->name;
                    $mprogram->type  = $json->type;
                    $mprogram->description  = $json->description;
                    $mprogram->status = $json->status;
                    $result = $mprogram->save();
                };
                // commit transaction
                $result = [true, "Save succesfully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function mprogramDelete($request)
    {
        DB::beginTransaction();
        try {
            $result = static::where('mprogram_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Pk_mprogram()
    {
        return $this->hasMany('App\Models\mprogram', 'mprogram_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
