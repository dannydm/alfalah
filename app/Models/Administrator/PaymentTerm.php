<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class PaymentTerm extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_payment_term';
	protected $primaryKey = 'payment_term_id';
    // public $sequence_name = 'm_payment_term_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   $arr = array();
        $my_arr = array();
        $results = static::orderBy('payment_term_id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("description", "ilike", "%".$request->description."%");   };
            if ($request->payment_term_id)
            {   $results->where("payment_term_id", "ilike", "%".$request->payment_term_id."%");   };
        };
        return $results->get();
    }
    public static function SearchExt($request)
    {   
        $subQuery = DB::table('m_payment_term as a')
                    ->selectRaw("a.payment_term_id, a.description,
                                (a.payment_term_id|| ' [' || a.description|| '] ') as display");

        $results = PaymentTerm::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.payment_term_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function paymenttermSave($json_data, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) {
                    if ($json->created_date == '')
                    {   $payment_term = new PaymentTerm();
                        $payment_term->payment_term_id = $json->payment_term_id;
                    }
                    else
                    {   $payment_term = PaymentTerm::where('payment_term_id', '=', $json->payment_term_id)->first();
                    };
                    
                    $payment_term->description = $json->description;
                    $result     = $payment_term->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function paymenttermDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('payment_term_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
