<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Mcoa extends AppModel
{    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_coa';
    protected $primaryKey = 'mcoa_id';
    // public $sequence_name = 'm_coa_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function McoaSearch($request)
    {
        $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY mcoa_id)) as record_id, * ")->orderBy('mcoa_id', 'asc');
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->name) 
            {   $results->where("name", "ilike", "%" . $request->name . "%");   };
            if ($request->status) 
            {   $results->where("status", "=", self::setStatus($request->status));  };
            if ($request->type) 
            {   $results->where("type", "=", $request->type);  };
        };
        return $results->get();
    }
    public static function McoaSearchExt($request)
    {
        $subQuery = DB::table('m_coa as a')
            ->selectRaw("a.mcoa_id, a.name as coa_name,
                                (' [' || a.mcoa_id || '] ' || a.name) as display")
            ->where('a.status', '=', 0);

        $results = mcoa::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.mcoa_id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }
    public static function McoaSave($json_data, $type, $user_id)
    {
        if (count($json_data) > 0) {   // begin transaction
            DB::beginTransaction();
            try {
                foreach ($json_data as $json) {
                    if ($json->created_date == '') {
                        $mcoa = new Mcoa();
                        $mcoa->created_date  = date('Ymd');
                    } else {
                        $mcoa = Mcoa::where('mcoa_id', '=', $json->mcoa_id)->first(); 
                        $mcoa->modified_date  = date('Ymd');
                    };
                    $mcoa->mcoa_id   = $json->mcoa_id;
                    $mcoa->parent_mcoa_id   = $json->parent_mcoa_id;
                    $mcoa->name   = $json->name;
                    $mcoa->name_print   = $json->name_print;
                    $mcoa->name_old   = $json->name_old;
                    $mcoa->need_value   = $json->need_value;
                    $mcoa->d_k   = $json->d_k;
                    $mcoa->type  = $json->type;
                    $mcoa->sumberdana_id   = $json->sumberdana_id;
                    $mcoa->status = $json->status;
                    $result = $mcoa->save();
                };
                // commit transaction
                $result = [true, "Save succesfully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function mcoaDelete($request)
    {
        DB::beginTransaction();
        try {
            $result = static::where('mcoa_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Pk_Mcoa()
    {
        return $this->hasMany('App\Models\Mcoa', 'mcoa_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
