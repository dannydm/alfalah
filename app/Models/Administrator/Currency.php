<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Currency extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_currency';
	protected $primaryKey = 'currency_id';
    // public $sequence_name = 'm_currency_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function currencySearch($request)
    {   $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY currency_id)) as record_id, * ")->orderBy('currency_id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->symbol)
            {   $results->where("symbol", "ilike", "%".$request->symbol."%");   };
            if ($request->status)
            {   $results->where("status", "=", self::setStatus($request->status));   };
        };
        return $results->get();
    }
    public static function currencySearchExt($request)
    {   
        $subQuery = DB::table('m_currency as a')
                    ->selectRaw("a.currency_id,
                                (' [' || a.currency_id || '] ' || a.name) as display")
                    ->where('a.status', '=', 0);

        $results = Country::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.currency_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function currencySave($json_data, $type, $user_id)
    {   
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {
                    if ($json->created_date == '')
                    {   $currency = new Currency();
                        $currency->created_date  = date('Ymd');
                    }
                    else
                    {   $currency = Currency::where('currency_id', '=', $json->currency_id)->first();
                        $currency->last_update  = date('Ymd');
                    };
                    
                    $currency->name   = $json->name;
                    $currency->symbol = trim($json->symbol);
                    $currency->status = $json->status;
                    $result = $currency->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function currencyDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('currency_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function countries()
    {   
        return $this->hasMany('App\Models\Country', 'currency_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
