<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Warehouse extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_warehouse';
	protected $primaryKey = 'id';
    // public $sequence_name = 'm_warehouse_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getType($status)
    {   switch($status)
        {   case 1: $result = "COCO";      break;
            case 2: $result = "GENERAL";   break;
              
        };
        return $result;
    }
    public static function setType($status)
    {   switch($status)
        {   case "COCO"     : $result = 1; break;
            case "GENERAL"  : $result = 2; break;
        };
        return $result;
    }
    public static function getLevel($status)
    {   switch($status)
        {   case 1: $result = "1st";   break;
            case 2: $result = "2nd";   break;
              
        };
        return $result;
    }
    public static function setLevel($status)
    {   switch($status)
        {   case "1st"  : $result = 1; break;
            case "2nd"  : $result = 2; break;
        };
        return $result;
    }
    public static function searchWarehouse($request)
    {   $results = DB::table('m_warehouse as a')
                ->join('m_site as b', function($join)
                    {   $join->on('a.company_id', '=', 'b.company_id')
                             ->on('a.site_id', '=', 'b.site_id');
                    })
                ->select(DB::raw("
                    a.*, b.name as site_name,
                    ( case warehouse_type 
                        when 1 then 'COCO'
                        when 2 then 'GENERAL'
                     end ) as type,
                    ( case warehouse_level 
                        when 1 then '1st'
                        when 2 then '2nd'
                     end ) as level,
                    ( case a.status 
                        when 0 then 'Active'
                        when 1 then 'Inactive'
                     end ) as status_name
                    "))
                ->orderBy('a.company_id', 'asc')
                ->orderBy('a.site_id', 'asc')
                ->orderBy('a.warehouse_id', 'asc');
        // $results = static::orderBy('site_id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->company_id)
            {   $results->where("a.company_id", "ilike", "%".$request->company_id."%");   };
            if ($request->site_id)
            {   $results->where("a.site_id", "ilike", "%".$request->site_id."%");   };
            if ($request->type)
            {   $results->where("a.warehouse_type", "=", Warehouse::setType($request->type));   };
        };
        return $results->get();
    }
    public static function SearchExt($request)
    {   $subQuery = DB::table('m_warehouse as a')
                    ->selectRaw("a.company_id, a.site_id, a.warehouse_id, a.name,
                                (a.company_id || '-' || a.site_id || '-'||a.name) as display");
        if ($request->s == "init")
        {   if ($request->t)
            {   $subQuery->where("a.warehouse_type", "=", Warehouse::setType($request->t));   
            };
        };
        $results = Site::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.site_id', 'asc')
                    ->orderby('x.warehouse_id', 'asc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };

        return $results->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentCompany()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Company', 'company_id', 'company_id');
    }
    public function parentSite()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Site', 'site_id', 'site_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
