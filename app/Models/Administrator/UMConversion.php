<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class UMConversion extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_um_conversion';
	protected $primaryKey = 'um_conv_id';
    // public $sequence_name = 'm_um_conversion_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function umConvSearch($request)
    {   $results = static::orderBy('um_conv_id', 'asc');

        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->um_conv_id)
            {   $results->where("um_conv_id", "=", $request->um_conv_id);   };
            if ($request->first_um_id)
            {   $results->where("first_um_id", "=", $request->first_um_id);   };
            if ($request->second_um_id)
            {   $results->where("second_um_id", "ilike", "%".$request->second_um_id."%");   };
        };
        return $results->get();
    }
    public static function umConvSearchExt($request)
    {   
        $subQuery = DB::table('m_um_conversion as a')
                    ->selectRaw("a.um_conv_id, a.first_um_id, a.second_um_id,
                                (a.second_um_id || ' [' || a.um_conv_id || '] ') as display");

        $results = Company::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.um_conv_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentFirstUM()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\UnitMeasurement', 'first_um_id', 'um_id');
    }
    public function parentSecondUM()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\UnitMeasurement', 'second_um_id', 'um_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
