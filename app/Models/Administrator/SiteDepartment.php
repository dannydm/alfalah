<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class SiteDepartment extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'site_department';
	protected $primaryKey = 'id';
    // public $sequence_name = 'site_department_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function searchSiteDepartment($request)
    {   
        $results = DB::table('site_department as a')
                ->join('m_site as b', function($join)
                    {   $join->on('a.company_id', '=', 'b.company_id')
                             ->on('a.site_id', '=', 'b.site_id');
                    })
                ->join('m_department as c', 'a.dept_id', '=', 'c.dept_id')
                ->select('a.*', 'b.name as site_name', 'c.name as dept_name');
        
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->company_id)
            {   $results->where("a.company_id", "ilike", "%".$request->company_id."%");   };
            if ($request->site_id)
            {   $results->where("a.site_id", "ilike", "%".$request->site_id."%");   };
            if ($request->site_name)
            {   $results->where("b.name", "ilike", "%".$request->site_name."%");   };
            if ($request->dept_name)
            {   $results->where("c.name", "ilike", "%".$request->dept_name."%");   };
        };
        return $results->get();
    }
    public static function searchExt($request)
    {   $subQuery = DB::table('site_department as a')
                    ->leftjoin('m_department as b', 'a.dept_id', '=', 'b.dept_id')
                    ->selectRaw("a.company_id, a.site_id, a.dept_id,
                                (a.company_id || '-' || a.site_id || '-'||b.name) as display");

        $results = static::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.dept_id', 'asc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };

        return $results->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentCompany()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Company', 'company_id', 'company_id');
    }
    public function parentSite()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Site', 'site_id', 'site_id');
    }
    public function parentDepartment()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Department', 'dept_id', 'dept_id');
    }
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
