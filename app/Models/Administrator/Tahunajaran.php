<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Tahunajaran extends AppModel
{    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_tahun_ajaran';
    protected $primaryKey = 'tahunajaran_id';
    // public $sequence_name = 'm_tahunajaran_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function tahunajaranSearch($request)
    {
        $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY tahunajaran_id)) as record_id, * ")->orderBy('tahunajaran_id', 'asc');
        // searchForm parameter
        if ($request->s == "form") {
            if ($request->name) {
                $results->where("name", "ilike", "%" . $request->name . "%");
            };
            if ($request->status) {
                $results->where("status", "=", self::setStatus($request->status));
            };
        };
        return $results->get();
    }
    public static function tahunajaranSearchExt($request)
    {
        $subQuery = DB::table('m_tahunajaran as a')
            ->selectRaw("a.tahunajaran_id,
                                (' [' || a.tahunajaran_id || '] ' || a.name) as display")
            ->where('a.status', '=', 0);

        $results = tahunajaran::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.tahunajaran_id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }
    public static function tahunajaranSave($json_data, $type, $user_id)
    {
        if (count($json_data) > 0) {   // begin transaction
            DB::beginTransaction();
            try {
                foreach ($json_data as $json) {
                    if ($json->created_date == '') {
                        $tahunajaran = new tahunajaran();
                        $tahunajaran->created_date  = date('Ymd');
                    } else {
                        $tahunajaran = tahunajaran::where('tahunajaran_id', '=', $json->tahunajaran_id)->first();
                        $tahunajaran->last_update  = date('Ymd');
                    };
                    $tahunajaran->tahunajaran_id   = $json->tahunajaran_id;
                    $tahunajaran->name   = $json->name;
                    $tahunajaran->status = $json->status;
                    $result = $tahunajaran->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function tahunajaranDelete($request)
    {
        DB::beginTransaction();
        try {
            $result = static::where('tahunajaran_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Pk_tahunajaran()
    {
        return $this->hasMany('App\Models\Tahunajaran', 'tahunajaran_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
