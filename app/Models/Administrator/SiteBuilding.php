<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class SiteBuilding extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'site_building';
	protected $primaryKey = 'building_id';
    // public $sequence_name = 'site_building_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function sitebuildingSearch($request)
    {   
        $results = DB::table('site_building as a')
                ->join('m_site as b', function($join)
                    {   $join->on('a.company_id', '=', 'b.company_id')
                             ->on('a.site_id', '=', 'b.site_id');
                    })
                ->select('a.*', 'b.name as site_name');
        
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->company_id)
            {   $results->where("a.company_id", "ilike", "%".$request->company_id."%");   };
            if ($request->site_id)
            {   $results->where("a.site_id", "ilike", "%".$request->site_id."%");   };
            if ($request->site_name)
            {   $results->where("b.name", "ilike", "%".$request->site_name."%");   };
            if ($request->name)
            {   $results->where("a.name", "ilike", "%".$request->name."%");   };
        };
        return $results->get();
    }
    public static function sitebuildingSearchExt($request)
    {   $subQuery = DB::table('site_building as a')
                    ->selectRaw("a.company_id, a.site_id, a.name, a.building_id,
                                (a.company_id || '-' || a.site_id || '-'||a.name) as display");

        $results = static::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.building_id', 'asc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };

        return $results->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentCompany()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Company', 'company_id', 'company_id');
    }
    public function parentSite()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Site', 'site_id', 'site_id');
    }

    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
