<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class AnggaranUser extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'user_anggaran';
	protected $primaryKey = 'useranggaran_id';
    // public $sequence_name = 'user_anggaran_usermrapbs_id_seq';
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function Mrapbs()
    {
        return $this->belongTo('Mrapbs');
    }

    public function user()
    {
        return $this->belongTo('MUser');
    }


    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function SearchAnggaranUser($request)
    {   $arr = array();
        $my_arr = array();
        // $results = DB::select('
        //     select a.*, b.name as role_name, c.name as user_name, c.link as user_link
        //     from role_users a
        //     join m_role b on (a.role_id = b.role_id)
        //     join m_users c on (a.user_id = c.user_id)
        //     ', []);
        $results = DB::table('user_anggaran as a')
                        ->join('m_rapbs as b', 'a.mrapbs_id', '=', 'b.mrapbs_id')
                        ->join('m_users as c', 'a.user_id', '=', 'c.user_id')
                        ->select('a.*', 'b.name as role_name', 'c.name as user_name');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->role_name)
            {   $results->where("b.name", "ilike", "%".$request->role_name."%");   };
            if ($request->user_name)
            {   $results->where("c.name", "ilike", "%".$request->user_name."%");   };
            if ($request->user_link)
            {   $results->where("c.link", "ilike", "%".$request->user_link."%");   };
        }
        elseif ($request->s == "combo")
        {   if ($request->mrapbs_id)
            {   $results->where("a.mrapbs_id", "=", $request->mrapbs_id);   };
        };
        return $results->get();
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
