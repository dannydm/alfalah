<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class City extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_city';
	protected $primaryKey = 'city_id';
    // public $sequence_name = 'm_city_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function citySearch($request)
    {   $arr = array();
        $my_arr = array();
        $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY city_id)) as record_id, * ")
                    ->orderBy('city_id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->phone_area)
            {   $results->where("phone_area", "ilike", "%".$request->phone_area."%");   };
            if ($request->country_id)
            {   $results->where("country_id", "ilike", "%".$request->country_id."%");   };
            if ($request->status)
            {   $results->where("status", "=", self::setStatus($request->status));   };
        };
        return $results->get();
    }
    public static function citySearchExt($request)
    {   
        $subQuery = DB::table('m_city as a')
                    ->leftjoin('m_country as b', 'a.country_id', '=', 'b.country_id')
                    ->selectRaw("a.city_id, a.country_id,
                                (a.name || ' [' || a.country_id|| '] ') as display")
                    ->where('a.status', '=', 0);

        $results = City::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.city_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function citySave($json_data, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) {
                    if ($json->created_date == '')
                    {   $city = new City();
                        $city->created_date = date('Ymd');
                    }
                    else
                    {   $city = City::where('city_id', '=', $json->city_id)->first();
                    };
                    
                    $city->name         = $json->name;
                    $city->phone_area   = $json->phone_area;
                    $city->country_id   = $json->country_id;
                    $city->status       = $json->status;

                    $result     = $city->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function cityDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('city_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function parentCountry()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Country', 'country_id', 'country_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
