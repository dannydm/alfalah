<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class RoleUser extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'user_role';
	protected $primaryKey = 'userrole_id';
    // public $sequence_name = 'user_role_';
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function role()
    {
        return $this->belongTo('Role');
    }

    public function user()
    {
        return $this->belongTo('MUser');
    }


    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function SearchRoleUser($request)
    {   $arr = array();
        $my_arr = array();
        // $results = DB::select('
        //     select a.*, b.name as role_name, c.name as user_name, c.link as user_link
        //     from role_users a
        //     join m_role b on (a.role_id = b.role_id)
        //     join m_users c on (a.user_id = c.user_id)
        //     ', []);
        $results = DB::table('user_role as a')
                        ->join('m_role as b', 'a.role_id', '=', 'b.role_id')
                        ->join('m_users as c', 'a.user_id', '=', 'c.user_id')
                        ->select('a.*', 'b.name as role_name', 'c.name as user_name');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->role_name)
            {   $results->where("b.name", "ilike", "%".$request->role_name."%");   };
            if ($request->user_name)
            {   $results->where("c.name", "ilike", "%".$request->user_name."%");   };
            if ($request->user_link)
            {   $results->where("c.link", "ilike", "%".$request->user_link."%");   };
        }
        elseif ($request->s == "combo")
        {   if ($request->role_id)
            {   $results->where("a.role_id", "=", $request->role_id);   };
        };
        return $results->get();
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
