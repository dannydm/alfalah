<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Role extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_role';
	protected $primaryKey = 'role_id';
    // public $sequence_name = 'm_role_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function SearchRole($request)
    {   $arr = array();
        $my_arr = array();
        $results = static::orderBy('role_id', 'asc')->get();
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->note)
            {   $results->where("note", "ilike", "%".$request->note."%");   };
            if ($request->status)
            {   $results->where("status", "=", $request->status);   };
        };
        
        return $results;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function rolemenu()
    {
        return $this->hasMany('App\Models\RoleMenu', 'role_id');
    }

    // public function menu()
    // {
    //     return $this->hasManyThrough(
    //             'App\Models\Menu', 'App\Models\RoleMenu',
    //             'role_id', 'menu_id', 'role_id');
    // }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
