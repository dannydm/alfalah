<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use App\Models\Administrator\SiteDocumentBuyer;
use App\Models\Administrator\SiteDocumentVendor;
use DB;

class SiteDocument extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'site_documents';
	protected $primaryKey = 'id';
    public $sequence_name = 'site_documents_';
    protected $variables = [
        "company_id"    => "",
        "site_id"       => "",
        "dept_id"       => "",
        "doc_id"        => "",
        "party_id"      => "",
        "revision_no"   => ""
        ];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    protected function checkVariables()
    {   $is_valid = true;
        if ($this->variables["company_id"] == "")   {  $is_valid = false; };
        if ($this->variables["site_id"] == "")      {  $is_valid = false; };
        if ($this->variables["dept_id"] == "")      {  $is_valid = false; };
        if ($this->variables["doc_id"] == "")       {  $is_valid = false; };
        
        return $is_valid;
    }
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   
        $results = DB::table('site_documents as a')
                ->join('m_site as b', function($join)
                    {   $join->on('a.company_id', '=', 'b.company_id')
                             ->on('a.site_id', '=', 'b.site_id');
                    })
                ->join('m_documents as c', 'a.doc_id', '=', 'c.doc_id')
                ->join('m_department as d', 'a.dept_id', '=', 'd.dept_id')
                ->select('a.*', 'b.name as site_name', 'c.name as doc_name', 'd.name as dept_name');
        
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->company_id)
            {   $results->where("a.company_id", "ilike", "%".$request->company_id."%");   };
            if ($request->site_id)
            {   $results->where("a.site_id", "ilike", "%".$request->site_id."%");   };
            if ($request->dept_id)
            {   $results->where("a.dept_id", "=", $request->dept_id);   };
            if ($request->site_name)
            {   $results->where("b.name", "ilike", "%".$request->site_name."%");   };
            if ($request->dept_name)
            {   $results->where("c.name", "ilike", "%".$request->dept_name."%");   };
        };
        return $results->get();
    }
    public static function SearchExt($request)
    {   
        $subQuery = DB::table('site_document as a')
                    ->leftjoin('m_document as b', 'a.doc_id', '=', 'b.doc_id')
                    ->selectRaw("a.company_id, a.site_id, a.doc_id,
                                (a.company_id || '-' || a.site_id || '-'||b.name) as display");

        $results = AssetITDHardware::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.doc_id', 'asc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };

        return $results->get();
    }
    /*
    * the_variables function getDocNum is array of :
    *   company_id
    *   site_id
    *   dept_id
    *   doc_id
    *   party_id
    *   revision_no
    */
    public function getDOCNum($company_id="", $site_id="", $dept_id="",
                            $doc_id="", $party_id="", $revision_no="")
    {   
        $this->variables["company_id"]  = $company_id;
        $this->variables["site_id"]     = $site_id;
        $this->variables["dept_id"]     = $dept_id;
        $this->variables["doc_id"]      = $doc_id;
        $this->variables["party_id"]    = $party_id;
        $this->variables["revision_no"] = $revision_no;

        if ($this->checkVariables() == false)
        {   return "Invalid Parameters";  }
        else
        {   //print_r($this->variables); exit;
            $mode = 'Normal';

            $result = DB::table('site_documents as a')
                ->join('m_site as b', function($join)
                    {   $join->on('a.company_id', '=', 'b.company_id')
                             ->on('a.site_id', '=', 'b.site_id');
                    })
                ->join('m_documents as c', 'a.doc_id', '=', 'c.doc_id')
                ->join('m_company as d', 'a.company_id', '=', 'd.company_id')
                ->selectRaw("a.company_id, a.last_doc_no, a.last_rev_no,
                    b.doc_code as site_doc_code,
                    c.doc_code as doc_doc_code, c.format_doc_no, c.seq_digit,
                    d.doc_abvr as doc_abvr,
                    extract(year from a.last_generate_date) as last_year,
                    extract(month from a.last_generate_date) as last_month,
                    extract(year from now() ) as current_year,
                    extract(month from now() ) as current_month
                    ")
                ->where("a.company_id", "=", $this->variables["company_id"])
                ->where("a.site_id", "=", $this->variables["site_id"])
                ->where("a.dept_id", "=", $this->variables["dept_id"])
                ->where("a.doc_id", "=", $this->variables["doc_id"])
                ->first();
            
            if ($result)
            {   //echo "format ".$result->format_doc_no."<BR>";
                //print_r(explode("$", $result->format_doc_no)); 
                $doc_no = "";
                foreach (explode("$", $result->format_doc_no) as $the_format) 
                {   switch (strtoupper(trim($the_format)))
                    {   case "B":
                        {   $mode = 'Buyer';
                            $doc_no .= strtoupper(trim(substr($this->variables["party_id"],0,5))) . "/";
                        };
                        break;
                        case "C":
                            // $doc_no .= strtoupper(trim(substr($result->company_id,0,1))) . "/";
                            $doc_no .= strtoupper(trim($result->company_id)) . "/";
                        break;
                        case "D":
                            $doc_no .= strtoupper(trim(substr($result->doc_doc_code,0,4))) . "/";
                        break;
                        case "S":
                        {   //Reset Seq Num at New Year For Last Document
                            $last_doc_no = 0;
                            $sitedocument = SiteDocument::where("company_id", "=", $this->variables["company_id"])
                                            ->where("site_id", "=", $this->variables["site_id"])
                                            ->where("dept_id", "=", $this->variables["dept_id"])
                                            ->where("doc_id", "=", $this->variables["doc_id"])
                                            ->first();
                            if ($sitedocument)
                            {   $sitedocument->last_generate_date = date("Y-m-d H:i:s");
                                switch ($mode) 
                                {   case 'Normal':
                                    {   //echo "<BR>Normal";
                                        if ($result->last_year !== $result->current_year)
                                        {   $last_doc_no = 1;   }
                                        else
                                        {   $last_doc_no = $sitedocument->last_doc_no + 1;  };

                                        $sitedocument->last_doc_no = $last_doc_no; 
                                        $sitedocument = $sitedocument->save();

                                        if ($sitedocument)
                                        {   
                                            //$rest = 20 - strlen($doc_no);
                                            $seq = str_pad($last_doc_no, $result->seq_digit, "0", STR_PAD_LEFT);
                                            $doc_no .= trim(strtoupper($seq)); //. "/";
                                        };
                                    };
                                    break;
                                    case 'Buyer':
                                    {   //echo "<BR>Buyer";
                                        $sitedoc_id = $sitedocument->id;
                                        $buyer_id = trim($this->variables["party_id"]);
                                        if ($result->last_year !== $result->current_year)
                                        {   $last_doc_no = 1;   }
                                        else
                                        {   $last_doc_no = $sitedocument->last_doc_no + 1;  };

                                        $sitedocument->last_doc_no = $last_doc_no; 
                                        $sitedocument = $sitedocument->save();

                                        if ($sitedocument)
                                        {   
                                            $sitedocbuyer = SiteDocumentBuyer::where("company_id", "=", $this->variables["company_id"])
                                                                ->where("site_id", "=", $this->variables["site_id"])
                                                                ->where("dept_id", "=", $this->variables["dept_id"])
                                                                ->where("doc_id", "=", $this->variables["doc_id"])
                                                                ->where("buyer_id", "=", $buyer_id)
                                                                ->first();
                                            
                                            if ($sitedocbuyer) // update sequence
                                            {   if ($result->last_year !== $result->current_year)
                                                {   $last_doc_no = 1;   }
                                                else
                                                {   $last_doc_no = $sitedocbuyer->last_doc_no + 1;  };

                                            }
                                            else // new buyer, insert sequence
                                            {   $sitedocbuyer = new SiteDocumentBuyer();
                                                $sitedocbuyer->id = $sitedocbuyer->getNextID($sitedocbuyer->sequence_name, $site_id);
                                                $sitedocbuyer->site_documents_id = $sitedoc_id;
                                                $sitedocbuyer->company_id = $this->variables["company_id"];
                                                $sitedocbuyer->site_id = $this->variables["site_id"];
                                                $sitedocbuyer->dept_id = $this->variables["dept_id"];
                                                $sitedocbuyer->doc_id = $this->variables["doc_id"];
                                                $sitedocbuyer->buyer_id = $buyer_id;
                                                $last_doc_no = 1;
                                            };
                                            // print_r($sitedocbuyer);
                                            $sitedocbuyer->last_generate_date = date("Y-m-d H:i:s");
                                            $sitedocbuyer->last_doc_no = $last_doc_no;
                                            $sitedocbuyer = $sitedocbuyer->save();

                                            if ($sitedocbuyer)
                                            {   //$rest = 20 - strlen($doc_no);
                                                $seq = str_pad($last_doc_no, $result->seq_digit, "0", STR_PAD_LEFT);
                                                $doc_no .= trim(strtoupper($seq)); // . "/";
                                            };
                                        };
                                    };
                                    break;
                                    case 'Vendor':
                                    {   //echo "<BR>Vendor";
                                        $sitedoc_id = $sitedocument->id;
                                        $vendor_id = trim($this->variables["party_id"]);
                                        if ($result->last_month !== $result->current_month)
                                        {   $last_doc_no = 1;   }
                                        else
                                        {   $last_doc_no = $sitedocument->last_doc_no + 1;  };

                                        $sitedocument->last_doc_no = $last_doc_no; 
                                        $sitedocument = $sitedocument->save();

                                        if ($sitedocument)
                                        {   
                                            $sitedocvendor = SiteDocumentVendor::where("company_id", "=", $this->variables["company_id"])
                                                                ->where("site_id", "=", $this->variables["site_id"])
                                                                ->where("dept_id", "=", $this->variables["dept_id"])
                                                                ->where("doc_id", "=", $this->variables["doc_id"])
                                                                ->where("vendor_id", "=", $vendor_id)
                                                                ->first();
                                            
                                            if ($sitedocvendor) // update sequence
                                            {   if ($result->last_year !== $result->current_year)
                                                {   $last_doc_no = 1;   }
                                                else
                                                {   $last_doc_no = $sitedocvendor->last_doc_no + 1;  };

                                            }
                                            else // new vendor, insert sequence
                                            {   $sitedocvendor = new SiteDocumentVendor();
                                                $sitedocvendor->id = $sitedocvendor->getNextID($sitedocvendor->sequence_name, $site_id);
                                                $sitedocvendor->site_documents_id = $sitedoc_id;
                                                $sitedocvendor->company_id = $this->variables["company_id"];
                                                $sitedocvendor->site_id = $this->variables["site_id"];
                                                $sitedocvendor->dept_id = $this->variables["dept_id"];
                                                $sitedocvendor->doc_id = $this->variables["doc_id"];
                                                $sitedocvendor->vendor_id = $vendor_id;
                                                $last_doc_no = 1;
                                            };
                                            // print_r($sitedocvendor);
                                            $sitedocvendor->last_generate_date = date("Y-m-d H:i:s");
                                            $sitedocvendor->last_doc_no = $last_doc_no;
                                            $sitedocvendor = $sitedocvendor->save();

                                            if ($sitedocvendor)
                                            {   //$rest = 20 - strlen($doc_no);
                                                $seq = str_pad($last_doc_no, $result->seq_digit, "0", STR_PAD_LEFT);
                                                if ($result->current_month < 10) { $month = '0'.$result->current_month; }
                                                else { $month = $result->current_month; }
                                                $doc_no .= $month.'/'. trim(strtoupper($seq)); // . "/";
                                            };
                                        };
                                    };
                                    break;
                                };
                            };
                        };
                        break;
                        case "T":
                            $doc_no .= strtoupper(trim(substr($result->site_doc_code,0,2))) . "/";
                        break;
                        case "V":
                        {   $mode = 'Vendor';
                            //$doc_no .= strtoupper(trim($result->doc_abvr)) . "/";
                            $doc_no .= strtoupper(trim(substr($this->variables["party_id"],0,5))) . "/";
                        };
                        break;
                        case "Y":
                            $doc_no .= strtoupper(trim(substr($result->current_year,2,2))) . "/";
                        break;
                    };
                };
                return $doc_no;
            }
            else 
            {  return "number generator failed";    };
        };
    }
    public static function sitedocumentSave($json_data, $site, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   
                foreach ($json_data as $json)  
                {   if (Empty($json->id))
                    {   $sitedocument               = new SiteDocument(); 
                        $sitedocument->id           = self::getNextID($sitedocument->sequence_name, $site);
                        $sitedocument->company_id   = $json->company_id;
                        $sitedocument->site_id      = $json->site_id;
                        $sitedocument->dept_id      = $json->dept_id;
                        $sitedocument->doc_id       = $json->doc_id;
                        // $sitedocument->created_date = date('Ymd');
                    }
                    else
                    {   $sitedocument  = SiteDocument::where('company_id', '=', $json->company_id)
                                    ->where('site_id', '=', $json->site_id)
                                    ->where('dept_id', '=', $json->dept_id)
                                    ->where('doc_id', '=', $json->doc_id)
                                    ->first();
                    };
                    
                    $sitedocument->last_doc_no = 0;
                    $sitedocument->last_generate_date = date('Ymd');

                    $result     = $sitedocument->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function sitedocumentDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('company_id', '=', $request->cid)
                            ->where('site_id', '=', $request->sid)
                            ->where('dept_id', '=', $request->did)
                            ->where('doc_id', '=', $request->cid)
                            ->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentDocument()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Document', 'doc_id', 'doc_id');
    }
    public function parentCompany()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Company', 'company_id', 'company_id');
    }
    public function parentSite()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Site', 'site_id', 'site_id');
    }
    public function parentDepartment()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Department', 'doc_id', 'doc_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
