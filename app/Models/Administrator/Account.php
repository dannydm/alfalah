<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Account extends AppModel
{    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_coa';
    protected $primaryKey = 'coa_no';
    // public $sequence_name = 'm_coa_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function accountSearch($request)
    {
        $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY coa_no)) as record_id, * ")->orderBy('coa_no', 'asc');
        // searchForm parameter
        if ($request->s == "form") {
            if ($request->name) {
                $results->where("name", "ilike", "%" . $request->name . "%");
            };
            if ($request->status) {
                $results->where("status", "=", self::setStatus($request->status));
            };
        };
        return $results->get();
    }
    public static function accountSearchExt($request)
    {
        $subQuery = DB::table('m_coa as a')
            ->selectRaw("a.coa_no,
                                (' [' || a.coa_no|| '] ' || a.name) as display")
            ->where('a.status', '=', 0);

        $results = account::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.coa_no', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }
    public static function accountSave($json_data, $type, $user_id)
    {
        if (count($json_data) > 0) {   // begin transaction
            DB::beginTransaction();
            try {
                foreach ($json_data as $json) {
                    if ($json->created_date == '') {
                        $account = new Account();
                        $account->created_date  = date('Ymd');
                    } else {
                        $account = Account::where('coa_no', '=', $json->coa_no)->first();
                        $account->modified_date  = date('Ymd');
                    };
                    $account->coa_no  = $json->coa_no;
                    $account->parent_coa_no   = $json->parent_coa_no;
                    $account->name   = $json->name;
                    $account->name_print   = $json->name_print;
                    $account->name_old   = $json->name_old;
                    $account->need_value   = $json->need_value;
                    $account->d_k   = $json->d_k;
                    $account->type  = $json->type;
                    $account->status = $json->status;
                    $account->created_by = $json->created_by;
                    $account->modified_by = $json->modified_by;
                    $result = $account->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function accountDelete($request)
    {
        DB::beginTransaction();
        try {
            $result = static::where('coa_no', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Pk_account()
    {
        return $this->hasMany('App\Models\Account', 'coa_no');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
