<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Charges extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_charges';
	protected $primaryKey = 'id';
    public $sequence_name = 'm_charges_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    protected static function getPlusMinus($plus_minus)
    {   switch($plus_minus)
        {   case '+': $result = "-Plus";   break;
            case '-': $result = "-Minus";  break;
        };
        return $result;
    }
    protected static function setPlusMinus($plus_minus)
    {   switch($plus_minus)
        {   case "+Plus" : $result = '+'; break;
            case "-Minus": $result = '-'; break;
        };
        return $result;
    }
    
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   $results = static::selectRaw("*,
                            (   case status 
                                    when 0 then 'Active'
                                    when 1 then 'Inactive'
                                end
                            ) as status_name,
                            (   case is_sales_invoice
                                    when 0 then 'Yes'
                                    when 1 then 'No'
                                end
                            ) as is_sales_invoice_name,
                            (   case is_po
                                    when 0 then 'Yes'
                                    when 1 then 'No'
                                end
                            ) as is_po_name,
                            (   case is_po_invoice
                                    when 0 then 'Yes'
                                    when 1 then 'No'
                                end
                            ) as is_po_invoice_name
                        ")
                    ->orderBy('id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->status)
            {   $results->where("status", "=", self::setStatus($request->status));   };
        };
        return $results->get();
    }
    public static function SearchExt($request)
    {   
        $subQuery = DB::table('m_charges as a')
                    ->selectRaw("a.id,
                                (' [' || a.id || '] ' || a.name) as display")
                    ->where('a.status', '=', 0);

        if ($request->s == "form")
        {   if ($request['t'])
            {   switch ($request['t']) 
                {   case 'po' : $field_name = "a.is_po"; break;
                    default   : $field_name = "a.is_po"; break;
                };
                $subQuery->where($field_name, "=", 0);   };
        };

        $results = Country::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function chargesSave($json_data, $site, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {   
                    if ($json->created_date == '')
                    {   $charges = new Charges();
                        $charges->id = self::getNextID(self::sequence_name, $site);
                        $charges->created_date  = date('Ymd');
                        $charges->created_by = $user_id;
                    }
                    else
                    {   $charges = Charges::where('id', '=', $json->id)->first();
                        // $charges->last_update  = date('Ymd');
                    };
                    
                    $charges->name             = $json->name;
                    $charges->plus_minus       = self::setPlusMinus($json->plus_minus);
                    $charges->status           = self::setStatus($json->status_name);
                    $charges->is_sales_invoice = self::setYesNo($json->is_sales_invoice_name);
                    $charges->is_po            = self::setYesNo($json->is_po_name);
                    $charges->is_po_invoice    = self::setYesNo($json->is_po_invoice_name);
                    $result = $charges->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function chargesDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
