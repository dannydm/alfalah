<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Mposbelanja extends AppModel
{    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_posbelanja';
    protected $primaryKey = 'posbelanja_id';
    // public $sequence_name = 'm_coa_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function MposbelanjaSearch($request)
    {
        $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY posbelanja_id)) as record_id, * ")->orderBy('posbelanja_id', 'asc');
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->posbelanja_name) 
            {   $results->where("posbelanja_name", "ilike", "%" . $request->posbelanja_name . "%");   };
            if ($request->status) 
            {   $results->where("status", "=", self::setStatus($request->status));  };
        };
        return $results->get();
    }
    public static function MposbelanjaSearchExt($request)
    {
        $subQuery = DB::table('m_coa as a')
            ->selectRaw("a.mcoa_id, a.name as coa_name,
                                (' [' || a.mcoa_id|| '] ' || a.name) as display")
            ->where('a.type','=',$request->type)
            ->where('a.need_value','=',$request->need_value)
            ->where('a.status', '=', 0);

        $results = mcash_bank_type::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.mcoa_id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();

    }
    public static function MposbelanjaSave($json_data,$user_id)
    {
        if (count($json_data) > 0) {   // begin transaction
            DB::beginTransaction();
            try {
                foreach ($json_data as $json) {
                    if ($json->created_date == '') {
                        $posbelanja = new Mposbelanja();
                        $posbelanja->created_date  = date('Ymd');
                    } else {
                        $posbelanja = Mposbelanja::where('posbelanja_id', '=', $json->posbelanja_id)->first();
                        $posbelanja->modified_date  = date('Ymd');
                    };
                    $posbelanja->posbelanja_id   = $json->posbelanja_id;
                    $posbelanja->posbelanja_name   = $json->posbelanja_name;
                    $posbelanja->coa_id   = $json->coa_id;
                    $posbelanja->coa_name   = $json->coa_name;
                    $posbelanja->status = $json->status;
                    $result = $posbelanja->save();
                };
                // commit transaction
                $result = [true, "Save succesfully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function MposbelanjaDelete($request)
    {
        DB::beginTransaction();
        try {
            $result = static::where('posbelanja_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Pk_posbelanja()
    {
        return $this->hasMany('App\Models\Mposbelanja', 'posbelanja_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
