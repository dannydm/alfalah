<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Company extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_company';
	protected $primaryKey = 'company_id';
    // public $sequence_name = 'm_company_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function companySearch($request)
    {   $arr = array();
        $my_arr = array();
        $results = DB::table('m_company as a')
                ->leftjoin('m_country as b', 'a.country_id', '=', 'b.country_id')
                ->leftjoin('m_city as c', 'a.city_id', '=', 'c.city_id')
                ->select(DB::raw("(ROW_NUMBER () OVER (ORDER BY a.company_id)) as recid,
                    a.*, b.name as country_name, c.name as city_name,
                    (   case a.status when 0 then 'Active' 
                        else 'Inactive' 
                        end 
                    ) as status_name
                    "))
                ->orderBy('a.company_id', 'asc');;

        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("a.name", "ilike", "%".$request->name."%");   };
            if ($request->address)
            {   $results->where("a.address", "ilike", "%".$request->address."%");   };
            if ($request->city)
            {   $results->where("c.name", "ilike", "%".$request->city."%");   };
            if ($request->country)
            {   $results->where("b.name", "ilike", "%".$request->country."%");   };
            if ($request->status)
            {   $results->where("a.status", "=", self::setStatus($request->status));   };
        };
        return $results->get();
    }
    public static function companySearchExt($request)
    {   
        $subQuery = DB::table('m_company as a')
                    ->selectRaw("a.company_id, a.name,
                                (a.name || ' [' || a.company_id || '] ') as display")
                    ->where('a.status', '=', 0);

        $results = Company::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.company_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function companySave($json_data, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) {
                    if ($json->created_date == '')
                    {   $Company = new Company();
                        $Company->company_id   = $json->company_id;
                    }
                    else
                    {   $Company = Company::where('company_id', '=', $json->company_id)->first();
                    };
                    
                    $Company->parent_company_id = $json->parent_company_id;
                    $Company->name              = $json->name;
                    $Company->address           = $json->address;
                    $Company->city_id           = $json->city_id;
                    $Company->country_id        = $json->country_id;
                    $Company->telp_no           = $json->telp_no;
                    $Company->fax_no            = $json->fax_no;
                    $Company->email             = $json->email;
                    $Company->doc_abvr          = $json->doc_abvr;
                    $Company->status            = $json->status;

                    $result = $Company->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function companyDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('company_id', '=', $request->company_id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function parentCountry()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Country', 'country_id', 'country_id');
    }
    public function parentCity()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\City', 'city_id', 'city_id');
    }
    public function Site()
    {   return $this->hasMany('App\Models\Site', 'company_id');    }
    
    public function SiteDepartment()
    {   return $this->hasMany('App\Models\SiteDepartment', 'company_id');    }
    
    public function SiteDocument()
    {   return $this->hasMany('App\Models\SiteDocument', 'company_id');    }

    public function SiteBuilding()
    {   return $this->hasMany('App\Models\SiteBuilding', 'company_id');    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
