<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class CompanyType extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_company_type';
	protected $primaryKey = 'company_type';
    public $sequence_name = 'm_company_type_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function SearchCompanyType($request)
    {   $arr = array();
        $my_arr = array();
        $results = static::orderBy('company_type', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->phone_area)
            {   $results->where("company_type", "ilike", "%".$request->company_type."%");   };
        };
        return $results->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function companyClassType()
    {
        return $this->hasMany('App\Models\companyClassType', 'company_type');
    }
    public function Site()
    {
        return $this->hasMany('App\Models\Site', 'company_type');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
