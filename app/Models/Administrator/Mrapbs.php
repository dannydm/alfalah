<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;


class Mrapbs extends AppModel
{    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_rapbs';
    protected $primaryKey = 'mrapbs_id';
    // public $sequence_name = 'm_mrapbs_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function mrapbsSearch($request)
    {
        $results = static::selectRaw(" * ")->orderBy('mrapbs_id', 'asc');
        // searchForm parameter
        if ($request->s == "form") {
            if ($request->name) 
            {   $results->where("name", "ilike", "%" . $request->name . "%");   };

            if ($request->status) 
            {   $results->where("status", "=", self::setStatus($request->status));  };
            
            if ($request->type) 
            {   $results->where("type", "ilike", "%" . $request->type . "%");   };

            if ($request->parent) 
            {   $results->where("parent_mrapbs_id", "=", $request->parent);   };
        };
        return $results->get();
    }
    public static function mrapbsSearchExt($request)
    {
        $subQuery = DB::table('m_mrapbs as a')
            ->selectRaw("a.mrapbs_id,
                                (' [' || a.mrapbs_id || '] ' || a.name) as display")
            ->where('a.status', '=', 0);

        $results = Mrapbs::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.mrapbs_id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }
    public static function mrapbsSave($json_data, $type, $user_id)
    {
        // print_r($json_data); exit;
        if (count($json_data) > 0) {   // begin transaction
            DB::beginTransaction();
            try {
                foreach ($json_data as $json) {
                    if ($json->created_date == '') {
                        $mrapbs = new Mrapbs();
                        $mrapbs->created_date  = date('Ymd');
                        $mrapbs->mrapbs_id   = $json->mrapbs_id;
                    } else {
                        $mrapbs = Mrapbs::where('mrapbs_id', '=', $json->mrapbs_id)->first();
                        $mrapbs->modified_date  = date('Ymd');
                    };
                    $mrapbs->parent_mrapbs_id   = $json->parent_mrapbs_id;
                    $mrapbs->name   = $json->name;
                    $mrapbs->type  = $json->type;
                    $mrapbs->description  = $json->description;
                    $mrapbs->dept_id   = $json->dept_id;
                    // $mrapbs->jenjang_sekolah_name   = $json->jenjang_sekolah_name;

                    $mrapbs->status = $json->status;
                    $result = $mrapbs->save();
                };
                // commit transaction
                $result = [true, "Save succesfully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function mrapbsDelete($request)
    {
        DB::beginTransaction();
        try {
            $result = static::where('mrapbs_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Pk_Mrapbs()
    {
        return $this->hasMany('App\Models\Mrapbs', 'mrapbs_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
