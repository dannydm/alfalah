<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Menu extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_menus';
	protected $primaryKey = 'menu_id';
    // public $sequence_name = 'm_menus_';
    
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getMenuChild($user_id, $childs)
    {   $arr = array();
    	foreach ($childs as $child)
        {   //$children= static::where('parent_menu_id', '=', $child->menu_id)->orderBy('arrange_no', 'asc')->get();
            if(Empty($user_id))
            {   $children = static::where('parent_menu_id', '=', $child->menu_id)
                            ->orderBy('arrange_no', 'asc')
                            ->get();
            }
            else
            {   $children = DB::table('m_users as a')
                        ->join('user_role as b', 'a.user_id', '=', 'b.user_id')
                        ->join('role_menus as c', 'b.role_id', '=', 'c.role_id')
                        ->join('m_menus as d', 'c.menu_id', '=', 'd.menu_id')
                        ->select('d.menu_id','d.name', 'd.link')
                        ->where("a.user_id", "=", $user_id)
                        ->where("d.parent_menu_id", "=", $child->menu_id)
                        ->orderBy('arrange_no', 'asc')
                        ->get();
            };
        	if ($children->count() == 0 )
            {   $my_arr = array(
                    "id" => $child->menu_id,
                    "text" => $child->name,
                    "link" => $child->link,
                    "leaf"  => "true"
                );
            }
            else
            {   $my_arr = array(
                    "id" => $child->menu_id,
                    "text" => $child->name,
                    "link" => $child->link,
                    "expanded"  => "true",
                    "children" => Menu::getMenuChild($user_id, $children)
                );
            };
            array_push($arr, $my_arr);
            unset($children);
        };
        return $arr;
    }

    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function ActiveMenu($user_id)
    {	$arr = array();
        $my_arr = array();
        if(Empty($user_id))
        {   //$results = static::where('level_degree', '=', 0)->get();    
            $results = null;
        }
        else
        {   $results = DB::table('m_users as a')
                        ->join('user_role as b', 'a.user_id', '=', 'b.user_id')
                        ->join('role_menus as c', 'b.role_id', '=', 'c.role_id')
                        ->join('m_menus as d', 'c.menu_id', '=', 'd.menu_id')
                        ->select('d.menu_id','d.name', 'd.link')
                        ->where("a.user_id", "=", $user_id)
                        ->where("d.level_degree", "=", 0)
                        ->orderBy('arrange_no', 'asc')
                        ->get();
        };

        if ($results)
        {
        	foreach ($results as $result)
        	{	if(Empty($user_id))
                {   $children = static::where('parent_menu_id', '=', $result->menu_id)
                                ->orderBy('arrange_no', 'asc')
                                ->get();
                }
                else
                {   $children = DB::table('m_users as a')
                            ->join('user_role as b', 'a.user_id', '=', 'b.user_id')
                            ->join('role_menus as c', 'b.role_id', '=', 'c.role_id')
                            ->join('m_menus as d', 'c.menu_id', '=', 'd.menu_id')
                            ->select('d.menu_id','d.name', 'd.link')
                            ->where("a.user_id", "=", $user_id)
                            ->where("d.parent_menu_id", "=", $result->menu_id)
                            ->orderBy('arrange_no', 'asc')
                            ->get();
                };

        		if ($children->count() == 0 )
                {   $my_arr = array(
                        "id" => $result->menu_id,
                        "text" => $result->name,
                        "link" => $result->link,
                        "leaf"  => "true"
                    );
                }
                else
                {   $my_arr = array(
                        "id" => $result->menu_id,
                        "text" => $result->name,
                        "link" => $result->link,
                        "expanded"  => "true",
                        "children" => Menu::getMenuChild($user_id, $children)
                    );
                };
                array_push($arr, $my_arr);
            };
        };
    	return json_encode($arr);
    }

    public static function UserMenu($user_id)
    {   $arr = array();
        $my_arr = array();
        $results = DB::table('m_users as a')
                        ->join('user_role as b', 'a.user_id', '=', 'b.user_id')
                        ->join('role_menus as c', 'a.role_id', '=', 'c.role_id')
                        ->join('m_menus as d', 'c.menu_id', '=', 'd.menu_id')
                        ->select('d.menu_id','d.name', 'd.link')
                        ->where("a.user_id", "=", $user_id)
                        ->where("d.level_degree", "=", 0)
                        ->get();
        return $results;
    }

    public static function SearchMenu($request)
    {   $arr = array();
        $my_arr = array();
        // dd($request->name); 
        $results = static::orderBy('menu_id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->link)
            {   $results->where("link", "ilike", "%".$request->link."%");   };
            if ($request->status)
            {   $results->where("status", "=", $request->status);   };
        };
        return $results->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
