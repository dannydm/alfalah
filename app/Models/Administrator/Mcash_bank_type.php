<?php

namespace App\Models\Administrator;

use App\Models\AppModel;
use DB;

class Mcash_bank_type extends AppModel
{    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_cash_bank_type';
    protected $primaryKey = 'id';
    // public $sequence_name = 'm_coa_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Mcash_bank_typeSearch($request)
    {
        $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY id)) as record_id, * ")->orderBy('id', 'asc');
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->name) 
            {   $results->where("name", "ilike", "%" . $request->name . "%");   };
            if ($request->status) 
            {   $results->where("status", "=", self::setStatus($request->status));  };
        };
        return $results->get();
    }
    public static function Mcash_bank_typeSearchExt($request)
    {
        $subQuery = DB::table('m_coa as a')
            ->selectRaw("a.mcoa_id, a.name as coa_name,
                                (' [' || a.mcoa_id|| '] ' || a.name) as display")
            ->where('a.type','=',$request->type)
            ->where('a.need_value','=',$request->need_value)
            ->where('a.status', '=', 0);

        $results = mcash_bank_type::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.mcoa_id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }
    public static function Mcash_bank_typeSave($json_data,$user_id)
    {
        if (count($json_data) > 0) {   // begin transaction
            DB::beginTransaction();
            try {
                foreach ($json_data as $json) {
                    if ($json->created_date == '') {
                        $cash_bank_type = new Mcash_bank_type();
                        $cash_bank_type->created_date  = date('Ymd');
                    } else {
                        $cash_bank_type = Mcash_bank_type::where('id', '=', $json->id)->first();
                        $cash_bank_type->modified_date  = date('Ymd');
                    };
                    $cash_bank_type->id   = $json->id;
                    $cash_bank_type->name   = $json->name;
                    // $cash_bank_type->dept_id   = $json->dept_id;
                    $cash_bank_type->coa_id   = $json->coa_id;
                    $cash_bank_type->coa_name   = $json->coa_name;
                    $cash_bank_type->status = $json->status;
                    $result = $cash_bank_type->save();
                };
                // commit transaction
                $result = [true, "Save succesfully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function Mcash_bank_typeDelete($request)
    {
        DB::beginTransaction();
        try {
            $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Pk_cash_bank_type()
    {
        return $this->hasMany('App\Models\Mcash_bank_type', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
