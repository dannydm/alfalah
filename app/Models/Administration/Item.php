<?php

namespace App\Models\Administration;

use Illuminate\Database\Eloquent\Model;
use DB;

class Item extends Model
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_item';
	protected $primaryKey = 'item_id';
    public $timestamps = false;    // disable auto generate created value
    public $incrementing = false;  // disable auto generate primary key value
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    protected function getNextID($site)
    {
        $result = DB::select("select nextval('m_item_".$site."_seq')");
        $result = $site.$result['0']->nextval;
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getStatus($status)
    {   switch($status)
        {   case 0: $result = "Active";  break;
            case 1: $result = "InActive";  break;
        };
        return $result;
    }
    public static function setStatus($status)
    {   switch($status)
        {   case "Active": $result = 0; break;
            case "Inactive": $result = 1; break;
        };
        return $result;
    }
    public static function getSubDetail($status)
    {   switch($status)
        {   case 0: $result = "NO";  break;
            case 1: $result = "YES";  break;
        };
        return $result;
    }
    public static function setSubDetail($status)
    {   switch($status)
        {   case "NO": $result = 0; break;
            case "YES": $result = 1; break;
        };
        return $result;
    }
    public static function Search($request)
    {   $arr = array();
        $my_arr = array();
        $results = static::orderBy('item_id', 'asc');
        $results = DB::table('m_item as a')
                ->leftjoin('m_um as b', 'a.um_id', '=', 'b.um_id')
                ->select(DB::raw(" a.*, 
                    (   case a.status 
                            when 0 then 'Active'
                            when 1 then 'Inactive'
                        end
                    ) as status_name,
                    (   case a.has_subdetail 
                            when 0 then 'NO'
                            when 1 then 'YES'
                        end
                    ) as has_subdetail_name,
                    b.name as um_name"));
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->phone_area)
            {   $results->where("spec", "ilike", "%".$request->spec."%");   };
            if ($request->country_id)
            {   $results->where("um_id", "ilike", "%".$request->country_id."%");   };
            if ($request->status)
            {   $results->where("status", "=", Item::setStatus($request->status));   };
        };
        return $results->get();
    }
    public static function itemSearchExt($request)
    {   
        $subQuery = DB::table('m_item as a')
                    ->leftjoin('m_um as b', 'a.um_id', '=', 'b.um_id')
                    ->selectRaw("a.item_id, a.name, a.item_type, a.um_id,
                                (a.name || ' [' || a.item_id|| '] ') as display")
                    ->where('a.status', '=', 0);

        if ($request->s == "form")
        {   if ($request['t'])
            {   $subQuery->where("a.item_type", "=", $request['t']);   };
        };

        $results = Item::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.item_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function itemSave($json_data, $site, $user_id)
    {
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) {
                    if ($json->created_date == '')
                    {   $Item = new Item();
                        $Item->item_id   = $Item->getNextID($site);
                        $Item->item_type = $json->item_type;
                    }
                    else
                    {   $Item = Item::where('item_id', '=', $json->item_id)->first();
                        $Item->modified_date = date('Ymd');
                    };
                    
                    $Item->material_name = $json->material_name;
                    $Item->spec_name     = $json->spec_name;
                    $Item->name          = $json->material_name.' '.$json->spec_name;
                    $Item->um_id         = $json->um_id;
                    $Item->status        = Item::setStatus($json->status_name);
                    $Item->has_subdetail = Item::setSubDetail($json->has_subdetail_name);

                    $result = $Item->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function itemDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('item_id', '=', $request->item_id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function parentUM()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Administrator\UnitMeasurement', 'um_id', 'um_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
