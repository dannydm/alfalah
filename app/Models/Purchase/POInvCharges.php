<?php

namespace App\Models\Purchase;

use App\Models\AppModel;
use DB;

class POInvCharges extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'po_invoice_charges';
	protected $primaryKey = 'id';
    protected $sequence_name = 'po_invoice_charges_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    protected static function getPlusMinus($plus_minus)
    {   switch($plus_minus)
        {   case '+': $result = "-Plus";   break;
            case '-': $result = "-Minus";  break;
        };
        return $result;
    }
    protected static function setPlusMinus($plus_minus)
    {   switch($plus_minus)
        {   case "+" :
            case "+Plus" : 
                $result = '+'; 
            break;
            case "-": 
            case "-Minus": 
                $result = '-'; 
            break;
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   //$results = static::orderby('id', 'asc');
        $results = DB::table('m_charges as x1')
            ->leftjoin('po_invoice_charges as a', 'x1.id', '=', 'a.charges_id')
            ->select(DB::raw("a.*, 
                (   case when a.charges_id is null 
                        then x1.id 
                        else a.charges_id
                    end
                ) as charges_id,
                (   case when a.pct_amount is null 
                        then '0'
                        else a.pct_amount
                    end
                ) as pct_amount,
                (   case when a.plus_minus is null 
                        then '+'
                        else a.plus_minus
                    end
                ) as plus_minus,
                (   case when a.total_amount is null 
                        then '0'
                        else a.total_amount
                    end
                ) as total_amount,
                x1.name as charges_name
                "))
            ->where("x1.is_po", "=", 0)  // is_po = true
            ->orderby('x1.id', 'asc');

        if ($request->s == "form")
        {   if ( $request->invoice_no )
            {   $results->where("invoice_no", "ilike", "%".$request->invoice_no."%"); };
        };

        return $results->get();
    }
    // public static function detailPrintOut($po_no)
    // {   $results = DB::table('po_invoice_charges as a')
    //         ->leftjoin('m_charges as x1', 'x1.item_id', '=', 'a.item_id')
    //         ->select(DB::raw("
    //             a.po_no, a.pr_no, a.item_id, sum(a.quantity_primary) as quantity_primary, a.um_primary, 
    //             a.unit_price, sum(a.amount) as amount, x1.name as item_name
    //             "))
    //         ->where("a.po_no", "=", $po_no)
    //         ->groupby('a.po_no', 'a.pr_no', 'a.item_id', 'a.um_primary',
    //                 'a.unit_price', 'x1.name')
    //         ->orderby('x1.name');

    //     return $results->get();
    // }
    public static function pochargesSave($json_data, $site, $user_id, $po_no)
    {   if (count($json_data) > 0)
        {   // transaction control ikut parent orders
            foreach ($json_data as $json) 
            {   // save record
                if ($json->created_date == '')  
                {   $POCharges = new POCharges();

                    $POCharges->id         = self::getNextID($POCharges->sequence_name, $site);
                    $POCharges->po_no      = $po_no;
                    $POCharges->charges_id = $json->charges_id;
                    $POCharges->created_by = $user_id;
                }
                else 
                {   // update hardware
                    $POCharges = POCharges::where('po_no','=', $po_no)
                                    ->where('charges_id', '=', $json->charges_id)
                                    ->first();
                    $POCharges->modified_by   = $user_id;
                    $POCharges->modified_date = Date('d/m/Y');
                };

                $POCharges->plus_minus       = self::setPlusMinus($json->plus_minus);
                $POCharges->pct_amount       = $json->pct_amount;
                $POCharges->total_amount     = $json->total_amount;
                $POCharges->description      = $json->description;
                
                $result = $POCharges->save();
            };
        };
        return $result;
    }
    public static function pochargesDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentPO()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Purchase\PO', 'po_no', 'po_no');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
