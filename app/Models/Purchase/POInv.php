<?php

namespace App\Models\Purchase;

use App\Models\AppModel;
use App\Models\Purchase\POInvDetail;
use App\Models\Administrator\SiteDocument;
use DB;

class POInv extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'po_invoice_master';
	protected $primaryKey = 'invoice_no';
    // public $sequence_name = 'po_invoice_master_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   
        if ($request->s == "form")
        {   //$results = static::orderBy('po_no', 'asc');
            $results = DB::table('po_invoice_master as a')
                ->join('m_company as b', 'a.company_id','=','b.company_id')
                ->join('m_company as c', 'a.supplier_id','=','c.company_id')
                ->join('po_master as d', 'a.po_no','=','d.po_no')
                // ->leftjoin('m_users as e', 'a.approved_by','=','e.user_id')
                ->select(DB::raw(" a.*, 
                    to_char(a.receive_date, 'DD/MM/YYYY') as receive_date,
                    b.name as company_name,
                    a.supplier_id as party_id, c.name as party_name,
                    a.supplier_invoice_no as party_inv_no,
                    a.supplier_invoice_date as party_inv_date,
                    a.po_no as ref_no, a.po_type as ref_type, 
                    to_char(a.po_date,'DD/MM/YYYY') as ref_date,
                    d.currency_id as ref_currency
                    "))
                ->orderby('a.created_date', 'desc');

            // if ( $request->po_no )
            // {   $results->where("a.po_no", "ilike", "%".$request->po_no."%"); };
            // if ( $request->supplier_name )
            // {   $results->where("d.name", "ilike", "%".$request->supplier_name."%"); };
            // if ($request->po_status)
            // {   if ($request->po_status == "ALL") {  }
            //     else 
            //     {   $results->where("a.po_status", "=", self::setDraftFinal($request->po_status)); }
            // };
            // if ($request->approved_status)
            // {   if ($request->approved_status == "ALL") {  }
            //     else 
            //     {   $results->where("a.approved_status", "=", self::setApproveStatus($request->approved_status)); }
            // };
        };
        return $results->get();
    }
    public static function headerPrintOut($request)
    {   
        $results = DB::table('po_invoice_master as a')
            ->leftjoin('m_company as x1', 'x1.company_id', '=', 'a.company_id')
            ->leftjoin('m_users as x2', 'x2.user_id', '=', 'a.approved_by')
            ->leftjoin('m_company as x3', 'x3.company_id', '=', 'a.supplier_id')
            ->select(DB::raw("a.*,
                x1.name as company_name, 
                x2.name as approved_by_name,
                x3.name as supplier_name
                "))
            ->orderby('a.po_no');

        if ($request->s == "form")
        {   
            if ( $request->id )
            {   $results->where("a.id", "=", $request->id); };
        };

        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };
    }
    public static function poinvSave($head_data, $json_data, $modi_data, $site, $user_id)
    {   if (count($head_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($head_data as $json) 
                {   // save 
                    // dd($json);
                    if ($json->created_date == '')  
                    {   
                        $poinv              = new POInv();
                        // $sitedocument       = new SiteDocument();
                        // $poinv->invoice_no       = $sitedocument->getDocNum(config('app.system_company'), $site, 20, 2002, $json->supplier_id, "");
                        // $json->invoice_no        = $poinv->invoice_no;
                        // $poinv->id          = self::getNextID(self::sequencce_name, $site);
                        
                        $poinv->invoice_no   = "1234";
                        $poinv->company_id  = config('app.system_company');
                        $poinv->supplier_id = $json->party_id;
                        $poinv->po_no       = $json->ref_no;
                        $poinv->po_type     = $json->ref_type;
                        $poinv->po_date     = $json->receive_date;
                        
                        $poinv->created_by  = $user_id;

                        $poinvinvdetail     = new POInvDetail();
                    }
                    else 
                    {   // update 
                        $poinv = POInv::where('invoice_no', '=', trim($json->invoice_no))->first();
                        $poinv->modified_by = $user_id;
                        
                        $poinvinvdetail = POInvDetail::where('invoice_no', '=', $json->invoice_no)->first();
                        if (Empty($poinvinvdetail)) { $poinvinvdetail = new POInvDetail(); };
                    };

                    $poinv->receive_date          = $json->receive_date;
                    $poinv->due_date              = $json->due_date;
                    $poinv->xrate_value           = $json->xrate_value;
                    $poinv->supplier_invoice_no   = $json->party_inv_no;
                    $poinv->supplier_invoice_date = $json->party_inv_date;
                    $poinv->currency_id           = $json->currency_id;
                    $poinv->description           = $json->description;
                    
                    // $poinv->po_status       = self::setDraftFinal($json->po_status_name);
                    
                    // print_r($poinv); exit;
                    
                    $result = $poinv->save();

                    // save details
                    if ($result)
                    {   if (count($json_data) > 0)
                        {   $result = $poinvinvdetail->poinvdetailSave( $json_data, 
                                            $site, $user_id, $json->invoice_no); 
                        };
                    };
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function poDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function poApprove($request, $user_id)
    {   DB::beginTransaction();
        try 
        {   $poinv = static::where('id', '=', $request->id)->first();
            $poinv->approved_status = 1;
            $poinv->approved_date  = date("Y-m-d H:i:s");
            $poinv->approved_by    = $user_id;
            $result = $poinv->save();

            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function incomingRefSearch($request)
    {   

        if ($request->s == "form")
        {   $subQuery_1_clause_1 = "";
            $subQuery_1_clause_2 = "";
            // if ($mode == "search_grid")
            // {   
            //     $subQuery_1_clause_1 = "a.company_id, a.buyer_id, 
            //         a.order_no, 
            //         to_char(a.order_date, 'dd/mm/yyyy') as order_date, ";

            //     if ( $request->company_id )
            //     {   $subQuery_1_clause_2 = $subQuery_1_clause_2." and y.company_id = '".$request->company_id."' "; };
            //     if ( $request->site_id )
            //     {   $subQuery_1_clause_2 = $subQuery_1_clause_2." and y.site_id = '".$request->site_id."' "; };
            //     if ( $request->warehouse_id )
            //     {   $subQuery_1_clause_2 = $subQuery_1_clause_2." and y.warehouse_id = '".$request->warehouse_id."' "; };
            // };
            $subQuery_1 = DB::table('grin_master as a')
                ->join('grin_detail as b', 'a.grin_no','=','b.grin_no')
                ->join('po_master as c', 'a.ref_no','=','c.po_no')
                ->join('po_detail as d', function($join)
                    {   $join->on('d.po_no', '=', 'c.po_no')
                             ->on('d.item_id', '=', 'b.item_id');
                    })
                ->join('m_company as x1', 'a.company_id','=','x1.company_id')
                ->join('m_item as x2', 'b.item_id','=','x2.item_id')
                ->join('m_company as x3', 'c.supplier_id','=','x3.company_id')
                ->select(DB::raw($subQuery_1_clause_1."
                    a.company_id, x1.name as company_name,
                    b.item_id, x2.name as item_name, x2.um_id as um_primary,
                    c.currency_id, c.supplier_id, x3.name as supplier_name,
                    c.po_no, to_char(c.created_date,'DD/MM/YYYY') as po_date,
                    d.quantity_primary as po_qty,
                    b.quantity_primary as qc_qty
                    "))
                ->where('a.ref_type', '=', 'PO')
                ->orderby('c.po_no');

                // if ( $request->ref_no )
                // {   $subQuery_1->where("a.order_no", "=", trim($request->ref_no)); };
                // if ( $request->warehouse_id )
                // {   $subQuery_1->where("a.company_id", "=", trim($request->company_id))
                //         ->where("a.site_id", "=", trim($request->site_id))
                //         ->where("a.warehouse_id", "=", trim($request->warehouse_id)); 
                // };
        };


        // $subQuery_2_clause = "";
        // if ($mode == "search_grid")
        // {   if ( $request->company_id )
        //         {   $subQuery_2_clause = $subQuery_2_clause." and e1.company_id = '".$request->company_id."' "; };
        //         if ( $request->site_id )
        //         {   $subQuery_1_clause_2 = $subQuery_2_clause." and e1.site_id = '".$request->site_id."' "; };
        //         if ( $request->warehouse_id )
        //         {   $subQuery_1_clause_2 = $subQuery_2_clause." and e1.warehouse_id = '".$request->warehouse_id."' "; };
        // };
        // $subQuery_2 = Grin::selectRaw("x.*, 
        //         coalesce((   
        //             select coalesce(sum(e2.quantity_primary),0)
        //             from grin_master e1 
        //             join grin_detail e2 on (e1.grin_no = e2.grin_no)
        //             where e2.item_id = x.item_id
        //               and e1.grin_type = 'ID' ".$subQuery_2_clause."
        //         ),0) as incoming_qty,
        //         coalesce((   
        //             select coalesce(sum(e2.quantity_primary),0)
        //             from grin_master e1 
        //             join grin_detail e2 on (e1.grin_no = e2.grin_no)
        //             where e2.item_id = x.item_id
        //               and e1.grin_type = 'OD' 
        //               and e1.grin_subtype = 'O' ".$subQuery_2_clause."
        //         ),0) as outgoing_qty
        //         ")
        //     ->from(\DB::raw(' ( '.$subQuery_1->toSql().' ) as x'));

        // $subQuery_3_clause = "";
        // if ($mode == "search_grid")
        // {   $subQuery_3_clause = ", y1.name as buyer_name, y2.name as company_name "; };
        // $subQuery_3 = Grin::selectRaw("y.*, 
        //         (y.order_qty - y.orderout_qty) as remain_qty,
        //         (y.incoming_qty - y.outgoing_qty) as stock_qty,
        //         0 as quantity_primary, 0 as unit_price, 0 as amount, 0 as po_qty_primary,
        //         '' as remark, '' as created_date
        //         ".$subQuery_3_clause)
        //     ->from(\DB::raw(' ( '.$subQuery_2->toSql().' ) as y'));

        // if ($mode == "search_grid")
        // {   $subQuery_3->join('m_company as y1', 'y.buyer_id','=','y1.company_id')
        //         ->join('m_company as y2', 'y.company_id','=','y2.company_id');
        // };

        // $select_clause = "";
        // $results = Grin::selectRaw("z.*")
        //     ->from(\DB::raw(' ( '.$subQuery_3->toSql().' ) as z'))
        //     ->mergeBindings($subQuery_1)
        //     // ->where('z.stock_qty', '>', 0)
        //     ->orderby('z.item_id', 'asc')
        //     ->orderby('z.ref_no', 'asc');
        
        // if ( $request->stocks == "YES")
        // {   $results->where("z.stock_qty", ">", 0); };
        // if ( $request->stocks == "NO")
        // {   $results->where("z.stock_qty", "<", 1); };

        $results = $subQuery_1;

        return $results->get();
    }
    public static function incomingRefItems($request)
    {   
        if ($request->s == "form")
        {   $subQuery_1_clause_1 = "";
            $subQuery_1_clause_2 = "";
            // if ($mode == "search_grid")
            // {   
            //     $subQuery_1_clause_1 = "a.company_id, a.buyer_id, 
            //         a.order_no, 
            //         to_char(a.order_date, 'dd/mm/yyyy') as order_date, ";

            //     if ( $request->company_id )
            //     {   $subQuery_1_clause_2 = $subQuery_1_clause_2." and y.company_id = '".$request->company_id."' "; };
            //     if ( $request->site_id )
            //     {   $subQuery_1_clause_2 = $subQuery_1_clause_2." and y.site_id = '".$request->site_id."' "; };
            //     if ( $request->warehouse_id )
            //     {   $subQuery_1_clause_2 = $subQuery_1_clause_2." and y.warehouse_id = '".$request->warehouse_id."' "; };
            // };
            $subQuery_1 = DB::table('grin_master as a')
                ->join('grin_detail as b', 'a.grin_no','=','b.grin_no')
                ->join('m_item as x1', 'b.item_id','=','x1.item_id')
                ->join('po_detail as x2', function($join)
                    {   $join->on('a.ref_no', '=', 'x2.po_no')
                             ->on('b.item_id', '=', 'x2.item_id');
                    })
                ->join('po_master as x3', 'x3.po_no','=','x2.po_no')
                ->select(DB::raw($subQuery_1_clause_1."
                    a.ref_no, a.grin_no,
                    b.*, (b.quantity_primary + b.reject_primary) as total_primary,
                    x1.name as item_name, x1.um_id as um_primary,
                    x2.unit_price as po_price, x3.currency_id
                    "))
                ->where('a.ref_type', '=', 'PO')
                ->orderby('a.ref_no');

                if ( $request->ref_no )
                {   $subQuery_1->where("a.ref_no", "=", trim($request->ref_no)); };
                // if ( $request->warehouse_id )
                // {   $subQuery_1->where("a.company_id", "=", trim($request->company_id))
                //         ->where("a.site_id", "=", trim($request->site_id))
                //         ->where("a.warehouse_id", "=", trim($request->warehouse_id)); 
                // };
        };


        // $subQuery_2_clause = "";
        // if ($mode == "search_grid")
        // {   if ( $request->company_id )
        //         {   $subQuery_2_clause = $subQuery_2_clause." and e1.company_id = '".$request->company_id."' "; };
        //         if ( $request->site_id )
        //         {   $subQuery_1_clause_2 = $subQuery_2_clause." and e1.site_id = '".$request->site_id."' "; };
        //         if ( $request->warehouse_id )
        //         {   $subQuery_1_clause_2 = $subQuery_2_clause." and e1.warehouse_id = '".$request->warehouse_id."' "; };
        // };
        // $subQuery_2 = Grin::selectRaw("x.*, 
        //         coalesce((   
        //             select coalesce(sum(e2.quantity_primary),0)
        //             from grin_master e1 
        //             join grin_detail e2 on (e1.grin_no = e2.grin_no)
        //             where e2.item_id = x.item_id
        //               and e1.grin_type = 'ID' ".$subQuery_2_clause."
        //         ),0) as incoming_qty,
        //         coalesce((   
        //             select coalesce(sum(e2.quantity_primary),0)
        //             from grin_master e1 
        //             join grin_detail e2 on (e1.grin_no = e2.grin_no)
        //             where e2.item_id = x.item_id
        //               and e1.grin_type = 'OD' 
        //               and e1.grin_subtype = 'O' ".$subQuery_2_clause."
        //         ),0) as outgoing_qty
        //         ")
        //     ->from(\DB::raw(' ( '.$subQuery_1->toSql().' ) as x'));

        // $subQuery_3_clause = "";
        // if ($mode == "search_grid")
        // {   $subQuery_3_clause = ", y1.name as buyer_name, y2.name as company_name "; };
        // $subQuery_3 = Grin::selectRaw("y.*, 
        //         (y.order_qty - y.orderout_qty) as remain_qty,
        //         (y.incoming_qty - y.outgoing_qty) as stock_qty,
        //         0 as quantity_primary, 0 as unit_price, 0 as amount, 0 as po_qty_primary,
        //         '' as remark, '' as created_date
        //         ".$subQuery_3_clause)
        //     ->from(\DB::raw(' ( '.$subQuery_2->toSql().' ) as y'));

        // if ($mode == "search_grid")
        // {   $subQuery_3->join('m_company as y1', 'y.buyer_id','=','y1.company_id')
        //         ->join('m_company as y2', 'y.company_id','=','y2.company_id');
        // };

        // $select_clause = "";
        // $results = Grin::selectRaw("z.*")
        //     ->from(\DB::raw(' ( '.$subQuery_3->toSql().' ) as z'))
        //     ->mergeBindings($subQuery_1)
        //     // ->where('z.stock_qty', '>', 0)
        //     ->orderby('z.item_id', 'asc')
        //     ->orderby('z.ref_no', 'asc');
        
        // if ( $request->stocks == "YES")
        // {   $results->where("z.stock_qty", ">", 0); };
        // if ( $request->stocks == "NO")
        // {   $results->where("z.stock_qty", "<", 1); };

        $results = $subQuery_1;

        return $results->get();
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
