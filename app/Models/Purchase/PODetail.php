<?php

namespace App\Models\Purchase;

use App\Models\AppModel;
use DB;

class PODetail extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'po_detail';
	protected $primaryKey = 'id';
    public $sequence_name = 'po_detail_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   //$results = static::orderby('id', 'asc');
        $results = DB::table('po_detail as a')
            ->leftjoin('m_item as x1', 'x1.item_id', '=', 'a.item_id')
            ->select(DB::raw("a.*,
                x1.name as item_name
                "))
            ->orderby('a.id', 'asc');

        if ($request->s == "form")
        {   if ( $request->po_no )
            {   $results->where("po_no", "ilike", "%".$request->po_no."%"); };
        };

        return $results->get();
    }
    public static function detailPrintOut($po_no)
    {   $results = DB::table('po_detail as a')
            ->leftjoin('m_item as x1', 'x1.item_id', '=', 'a.item_id')
            ->select(DB::raw("
                a.po_no, a.pr_no, a.item_id, sum(a.quantity_primary) as quantity_primary, a.um_primary, 
                a.unit_price, sum(a.amount) as amount, x1.name as item_name
                "))
            ->where("a.po_no", "=", $po_no)
            ->groupby('a.po_no', 'a.pr_no', 'a.item_id', 'a.um_primary',
                    'a.unit_price', 'x1.name')
            ->orderby('x1.name');

        return $results->get();
    }
    public static function podetailSave($json_data, $site, $user_id, $po_no)
    {   if (count($json_data) > 0)
        {   // transaction control ikut parent orders
            foreach ($json_data as $json) 
            {   // save hardware
                if (trim($json->remark) == 'REMOVED')
                {   // remove record
                    // dd($json);
                    $result = static::where('id', '=', $json->id)->delete();
                    $result = [true, "Save succcefully"];
                }
                else
                {   // save record
                    if ($json->created_date == '')  
                    {   $PODetail = new PODetail();
                        $PODetail->id         = self::getNextID(self::sequence_name, $site);
                        $PODetail->po_no      = $po_no;
                        $PODetail->pr_no      = $json->pr_no;
                        $PODetail->item_id    = $json->item_id;
                        $PODetail->created_by = $user_id;

                        // switch ($grin_type)
                        // {
                        //     case 'ID': $doc_no = 20301; break;
                        //     case 'IR': $doc_no = 20302; break;
                        //     case 'OD': $doc_no = 20303; break;
                        //     case 'OR': $doc_no = 20304; break;
                        //     case 'NO': $doc_no = 20313; break;
                        // };
                        // $this->grin_no = $this->numgen->getDOCNum($company_id, $_COOKIE['cook_site'],
                        //                                             203, $doc_no, "", "");
                    }
                    else 
                    {   // update hardware
                        $PODetail = PODetail::where('id', '=', $json->id)->first();
                        $PODetail->modified_by   = $user_id;
                        $PODetail->modified_date = Date('d/m/Y');
                    };

                    $PODetail->unit_price       = $json->unit_price;
                    $PODetail->pr_qty           = $json->pr_qty;
                    $PODetail->quantity_primary = $json->quantity_primary;
                    $PODetail->um_primary       = $json->um_primary;
                    $PODetail->unit_price       = $json->unit_price;
                    $PODetail->amount           = $json->amount;
                    $PODetail->remark           = $json->remark;
                    
                    $result = $PODetail->save();
                };
            };
        };
        return $result;
    }
    public static function podetailDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentPO()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Purchase\PO', 'po_no', 'po_no');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
