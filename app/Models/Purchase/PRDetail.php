<?php

namespace App\Models\Purchase;

use App\Models\AppModel;
use DB;

class PRDetail extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'pr_detail';
	protected $primaryKey = 'id';
    public $sequence_name = 'pr_detail_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   //$results = static::orderby('id', 'asc');
        $results = DB::table('pr_detail as a')
            ->leftjoin('m_item as x1', 'x1.item_id', '=', 'a.item_id')
            ->select(DB::raw("a.*,
                x1.name as item_name
                "))
            ->orderby('a.id', 'asc');

        if ($request->s == "form")
        {   if ( $request->pr_no )
            {   $results->where("pr_no", "ilike", "%".$request->pr_no."%"); };
        };

        return $results->get();
    }
    public static function detailPrintOut($pr_no)
    {   $results = DB::table('pr_detail as a')
            ->leftjoin('m_item as x1', 'x1.item_id', '=', 'a.item_id')
            ->select(DB::raw("
                a.pr_no, a.item_id, a.quantity_primary, a.um_primary, 
                a.remark, x1.name as item_name
                "))
            ->where("a.pr_no", "=", $pr_no)
            ->orderby('x1.name');

        return $results->get();
    }
    public static function prdetailSave($json_data, $site, $user_id, $pr_no)
    {   
        if (count($json_data) > 0)
        {   // transaction control ikut parent orders
            foreach ($json_data as $json) 
            {   // save hardware
                if (trim($json->remark) == 'REMOVED')
                {   // remove record
                    // dd($json);
                    $result = static::where('id', '=', $json->id)->delete();
                    $result = [true, "Save succcefully"];
                }
                else
                {   // save record
                    if ($json->created_date == '')  
                    {   $PRDetail = new PRDetail();
                        $PRDetail->id         = self::getNextID(self::sequence_name, $site);
                        $PRDetail->pr_no      = $pr_no;
                        $PRDetail->item_id    = $json->item_id;
                        $PRDetail->created_by = $user_id;
                    }
                    else 
                    {   // update hardware
                        $PRDetail = PRDetail::where('id', '=', $json->id)->first();
                        $PRDetail->modified_by   = $user_id;
                        $PRDetail->modified_date = Date('d/m/Y');
                    };

                    $PRDetail->quantity_primary = $json->quantity_primary;
                    $PRDetail->um_primary       = $json->um_primary;
                    $PRDetail->remark           = $json->remark;
                    
                    $result = $PRDetail->save();
                };
            };
        };
        return $result;
    }
    public static function prdetailDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    public static function itemsneedpoSearch($request)
    {   //$results = static::orderby('id', 'asc');
        $subQuery = DB::table('pr_master as a')
            ->join('pr_detail as b', 'a.pr_no', '=', 'b.pr_no')
            ->join('m_item as c', 'b.item_id', '=', 'c.item_id')
            ->selectRaw("b.id, b.pr_no, trim(b.item_id) as item_id, 
                b.quantity_primary, b.um_primary, 
                b.remark, b.created_by, b.created_date, b.modified_by, b.modified_date, 
                a.order_no,
                c.name as item_name, 
                ( select coalesce(sum(d.quantity_primary),0)
                    from po_detail d
                    where d.pr_no = b.pr_no and d.item_id = b.item_id
                ) as po_qty")
            ->where('a.approved_status', '=', 1);

        if ($request->s == "form")
        {   if ( $request->pr_no )
            {   $subQuery->where("a.pr_no", "ilike", "%".$request->pr_no."%"); };
        };

        $results = PRDetail::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.order_no', 'asc');

        if ($request->s == "form")
        {   
            if ($request->is_complete)
            {   if ($request->is_complete == "YES")
                {   $results->whereRaw("( x.quantity_primary - x.po_qty ) < 0 "); }
                else if ($request->is_complete == "NO")
                {   $results->whereRaw("( x.quantity_primary - x.po_qty ) > 0 "); };
            };

        };

        return $results->get();
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentOrder()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Sales\Order', 'pr_no', 'pr_no');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
