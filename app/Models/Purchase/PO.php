<?php

namespace App\Models\Purchase;

use App\Models\AppModel;
use App\Models\Purchase\PODetail;
use App\Models\Purchase\POCharges;
use App\Models\Administrator\SiteDocument;
use DB;

class PO extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'po_master';
	protected $primaryKey = 'po_no';
    public $sequence_name = 'po_master_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   
        if ($request->s == "form")
        {   //$results = static::orderBy('po_no', 'asc');
            $results = DB::table('po_master as a')
                ->join('m_company as b', 'a.company_id','=','b.company_id')
                ->join('m_department as c', 'a.to_dept_id','=','c.dept_id')
                ->leftjoin('m_company as d', 'a.supplier_id','=','d.company_id')
                ->leftjoin('m_users as e', 'a.approved_by','=','e.user_id')
                ->select(DB::raw(" a.id,
                    a.company_id, a.po_no, a.po_type, a.supplier_id,
                    (   case a.po_status 
                            when 0 then 'Draft'
                            when 1 then 'Final'
                        end
                    ) as po_status_name, a.po_status,
                    to_char(a.created_date, 'dd/mm/yyyy') as po_date,
                    a.description, a.to_site_id, a.to_dept_id, 
                    (   case a.approved_status   
                            when 0 then 'Need Approval'
                            when 1 then 'Approved'
                        end
                    ) as approved_status_name, a.approved_status, 
                    a.approved_date, a.approved_by, e.name as approved_by_name,
                    a.payment_term, a.payment_term_id,
                    a.created_by, a.created_date,
                    a.modified_by, a.modified_date,
                    b.name as company_name, c.name as to_dept_name,
                    d.name as supplier_name,
                    (   select count(1)
                        from grin_master
                        where trim(ref_no) = trim(a.po_no)
                          and ref_type='PO'
                    ) as grin_count
                    "))
                ->orderby('a.created_date', 'desc');

            if ( $request->po_no )
            {   $results->where("a.po_no", "ilike", "%".$request->po_no."%"); };
            if ( $request->supplier_name )
            {   $results->where("d.name", "ilike", "%".$request->supplier_name."%"); };
            if ($request->po_status)
            {   if ($request->po_status == "ALL") {  }
                else 
                {   $results->where("a.po_status", "=", self::setDraftFinal($request->po_status)); }
            };
            if ($request->approved_status)
            {   if ($request->approved_status == "ALL") {  }
                else 
                {   $results->where("a.approved_status", "=", self::setApproveStatus($request->approved_status)); }
            };
        };
        return $results->get();
    }
    public static function headerPrintOut($request)
    {   $results = DB::table('po_master as a')
            ->leftjoin('m_company as x1', 'x1.company_id', '=', 'a.company_id')
            ->leftjoin('m_users as x2', 'x2.user_id', '=', 'a.approved_by')
            ->leftjoin('m_company as x3', 'x3.company_id', '=', 'a.supplier_id')
            ->select(DB::raw("a.*,
                x1.name as company_name, 
                x2.name as approved_by_name,
                x3.name as supplier_name
                "))
            ->orderby('a.po_no');

        if ($request->s == "form")
        {   
            if ( $request->id )
            {   $results->where("a.id", "=", $request->id); };
        };

        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };
    }
    public static function poSave($head_data, $json_data, $chg_data, $site, $user_id)
    {   if (count($head_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($head_data as $json) 
                {   // save 
                    // dd($json);
                    if ($json->created_date == '')  
                    {   $po                = new PO();
                        $sitedocument      = new SiteDocument();
                        $po->po_no         = $sitedocument->getDocNum(config('app.system_company'), 
                                                    $site, 20, 2002, $json->supplier_id, "");
                        $json->po_no       = $po->po_no;
                        $po->id            = self::getNextID(self::sequence_name, $site);
                        $po->company_id    = config('app.system_company');
                        $po->created_by    = $user_id;

                        $po_detail  = new PODetail();
                        $po_charges = new POCharges();
                    }
                    else 
                    {   // update 
                        $po = PO::where('po_no', '=', trim($json->po_no))->first();
                        $po->modified_by = $user_id;
                        
                        $po_detail = PODetail::where('po_no', '=', $json->po_no)->first();
                        if (Empty($po_detail)) { $po_detail = new PODetail(); };

                        $po_charges = POCharges::where('po_no', '=', $json->po_no)->first();
                        if (Empty($po_charges)) { $po_charges = new POCharges(); };
                    };

                    $po->to_site_id      = $json->to_site_id;
                    $po->to_dept_id      = $json->to_dept_id;
                    $po->supplier_id     = $json->supplier_id;
                    $po->currency_id     = $json->currency_id;
                    $po->po_status       = self::setDraftFinal($json->po_status_name);
                    $po->description     = $json->description;
                    $po->payment_term    = $json->payment_term;
                    $po->payment_term_id = $json->payment_term_id;

                    // print_r($po); exit;
                    
                    $result = $po->save();

                    // save details
                    if ($result)
                    {   if (count($json_data) > 0)
                        {   $result = $po_detail->podetailSave( $json_data, 
                                            $site, $user_id, $json->po_no); 
                        };
                        if (count($chg_data) > 0)
                        {   $result = $po_charges->pochargesSave( $chg_data, 
                                            $site, $user_id, $json->po_no); 
                        };
                    };
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function poDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function poApprove($request, $user_id)
    {   DB::beginTransaction();
        try 
        {   $po = static::where('id', '=', $request->id)->first();
            $po->approved_status = 1;
            $po->approved_date  = date("Y-m-d H:i:s");
            $po->approved_by    = $user_id;
            $result = $po->save();

            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
