<?php

namespace App\Models\Purchase;

use App\Models\AppModel;
use App\Models\Purchase\PRDetail;
use App\Models\Administrator\SiteDocument;
use DB;

class PR extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'pr_master';
	protected $primaryKey = 'pr_no';
    public $sequence_name = 'pr_master_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */  
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   
        if ($request->s == "form")
        {   //$results = static::orderBy('pr_no', 'asc');
            $results = DB::table('pr_master as a')
                ->join('m_company as b', 'a.company_id','=','b.company_id')
                ->join('m_department as c', 'a.to_dept_id','=','c.dept_id')
                ->leftjoin('m_users as d', 'a.approved_by','=','d.user_id')
                ->select(DB::raw(" a.id,
                    a.company_id, a.pr_no, a.order_no, a.pr_type, 
                    (   case a.pr_status 
                            when 0 then 'Draft'
                            when 1 then 'Final'
                        end
                    ) as pr_status_name, a.pr_status,
                    to_char(a.created_date, 'dd/mm/yyyy') as pr_date,
                    a.description, a.to_site_id, a.to_dept_id, 
                    (   case a.approved_status 
                            when 0 then 'Need Approval'
                            when 1 then 'Approved'
                        end
                    ) as approved_status_name, a.approved_status, 
                    a.approved_date, a.approved_by, d.username as approved_by_name,
                    a.created_by, a.created_date,
                    a.modified_by, a.modified_date,
                    b.name as company_name, c.name as to_dept_name,
                    (   select count(1)
                        from po_detail
                        where trim(pr_no) = trim(a.pr_no)
                    ) as po_count
                    "))
                // ->where("b1.item_id", "ilike", "D/34%")
                ->orderby('a.created_date', 'desc');

            if ( $request->pr_no )
            {   $results->where("a.pr_no", "ilike", "%".$request->pr_no."%"); };
            if ( $request->order_no )
            {   $results->where("a.order_no", "ilike", "%".$request->order_no."%"); };
            if ($request->pr_type)
            {   if ($request->pr_type == "ALL") {  }
                else 
                {   $results->where("a.pr_type", "=", $request->pr_type); }
            };
            if ($request->pr_status)
            {   if ($request->pr_status == "ALL") {  }
                else 
                {   $results->where("a.pr_status", "=", self::setDraftFinal($request->pr_status)); }
            };
            if ($request->approved_status)
            {   if ($request->approved_status == "ALL") {  }
                else 
                {   $results->where("a.approved_status", "=", self::setApproveStatus($request->approved_status)); }
            };
        };
        // dd($results->get());
        return $results->get();
    }
    public static function headerPrintOut($request)
    {   
        $results = DB::table('pr_master as a')
            ->leftjoin('m_company as x1', 'x1.company_id', '=', 'a.company_id')
            ->leftjoin('m_users as x2', 'x2.user_id', '=', 'a.approved_by')
            ->select(DB::raw("a.*,
                x1.name as company_name, 
                x2.name as approved_by_name
                "))
            ->orderby('a.pr_no');

        if ($request->s == "form")
        {   
            if ( $request->id )
            {   $results->where("a.id", "=", $request->id); };
        };

        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };
    }
    public static function prSave($head_data, $json_data, $modi_data, $site, $user_id)
    {   
        if (count($head_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($head_data as $json) 
                {   // save
                    if ($json->created_date == '')  
                    {   $pr                = new PR();
                        $sitedocument      = new SiteDocument();
                        $pr->pr_no         = $sitedocument->getDocNum(config('app.system_company'), 
                                                    $site, 20, 2001, "", "");
                        $json->pr_no       = $pr->pr_no;
                        $pr->id            = self::getNextID(self::sequence_name, $site);
                        $pr->company_id    = config('app.system_company');
                        $pr->created_by    = $user_id;

                        $pr_detail = new PRDetail();
                    }
                    else 
                    {   // update
                        $pr = PR::where('pr_no', '=', $json->pr_no)->first();
                        $pr->modified_by = $user_id;
                        $pr->modified_date = Date('Y-m-d');
                        
                        $pr_detail = PRDetail::where('pr_no', '=', $json->pr_no)->first();
                        if (Empty($pr_detail)) { $pr_detail = new PRDetail(); };
                    };

                    $pr->to_site_id  = $json->to_site_id;
                    $pr->to_dept_id  = $json->to_dept_id;
                    $pr->pr_status   = self::setDraftFinal($json->pr_status_name);
                    $pr->description = $json->description;
                    if (Empty($json->order_no)){}
                    else {
                        $pr->order_no   = $json->order_no;
                        $pr->pr_type    = 'OPR';
                    };
                    
                    $result = $pr->save();

                    // save details
                    if ($result)
                    {   if (count($json_data) > 0)
                        {   $result = $pr_detail->prdetailSave( $json_data, 
                                            $site, $user_id, $json->pr_no); 
                        };
                    };
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function prDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function prApprove($request, $user_id)
    {   DB::beginTransaction();
        try 
        {   $pr = static::where('id', '=', $request->id)->first();
            $pr->approved_status = 1;
            $pr->approved_date  = date("Y-m-d H:i:s");
            $pr->approved_by    = $user_id;
            $result = $pr->save();

            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
