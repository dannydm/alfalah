<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;
use DB;

class MachineType extends Model
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_machine_type';
	protected $primaryKey = 'mactype_id';
    public $timestamps = false;    // disable auto generate created value
    public $incrementing = false;  // disable auto generate primary key value
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getStatus($status)
    {   switch($status)
        {   case 0: $result = "Active";  break;
            case 1: $result = "InActive";  break;
        };
        return $result;
    }
    public static function setStatus($status)
    {   switch($status)
        {   case "Active": $result = 0; break;
            case "InActive": $result = 1; break;
        };
        return $result;
    }
    public static function machinetypeSearch($request) 
    {   $arr = array();
        $my_arr = array();
        $results = static::selectRaw("*, 
                        ( case status when 0 then 'Active' else 'Inactive' end) as status_name")
                    ->orderBy('mactype_id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->status == "ALL"){} else
            {   $results->where("status", "=", MachineType::setStatus($request->status));   };
        };
        return $results->get();
    }
    public static function machinetypeSearchExt($request)
    {   $subQuery = DB::table('m_machine_type as a')
                    ->selectRaw("a.mactype_id, a.name,
                                (a.mactype_id || '-' ||a.name) as display");

        $results = MachineType::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.mactype_id', 'asc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };

        return $results->get();
    }
    public static function machinetypeSave($json_data, $site)
    {   
        DB::beginTransaction();
        try 
        {   foreach ($json_data as $json) {
                if (Empty($json->created_date))
                {   $mctype = new MachineType(); 
                    $mctype->mactype_id = $site.$json->mactype_id;
                }
                else
                {   $mctype  = MachineType::where('mactype_id', '=', $json->mactype_id)
                                ->first();
                };
                
                $mctype->name    = $json->name;
                $mctype->status  = MachineType::setStatus($json->status_name);

                $result     = $mctype->save();
            };
            // commit transaction
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        unset($mctype);
        unset($json);
        return $result;
    }
    public static function machinetypeDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('mactype_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
