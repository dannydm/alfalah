<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;
use DB;

class Machine extends Model
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_machine';
	protected $primaryKey = 'mach_id';
    public $timestamps = false;    // disable auto generate created value
    public $incrementing = false;  // disable auto generate primary key value
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    // alter table bdj_asset add status numeric(1);
    // alter table bdj_asset add merk varchar(32);
    // alter table bdj_asset add model varchar(32);


    // update bdj_asset set status = 0 where item_description ilike 'A.%';
    // update bdj_asset set status = 2 where item_description ilike 'S.%';
    // update bdj_asset set status = 0 where item_description ilike 'A,%';
    // update bdj_asset set status = 0 where item_description ilike 'A %';

    // update bdj_asset set item_description = replace(item_description,'A.','');
    // update bdj_asset set item_description = replace(item_description,'A,','');
    // update bdj_asset set item_description = replace(item_description,'A ','');
    // update bdj_asset set item_description = replace(item_description,'S.','');

    // update bdj_asset set item_description = trim(item_description);
    // update bdj_asset                                        
    // set item_description = x.mactype_id
    // from (
    // select mactype_id, name from m_machine_type
    // ) x
    // where trim(item_description) = x.name;

    // select distinct(item_description) from bdj_asset ;
    // select * from m_machine_type where name ilike '%SEW%';
    // update bdj_asset set item_description = 16 where trim(item_description) = 'B UTON SEW';

    // select distinct(split_part(brand, ' ',1)) as brand from bdj_asset ;
    // update bdj_asset
    // set merk = x.machbrand_id
    // from (
    // select machbrand_id, name from m_machine_brand
    // ) x
    // where brand ilike '%'||x.name||'%';
    // update bdj_asset set brand = replace(brand, merk,'');
    // update bdj_asset set brand = trim(brand);


    // insert into m_machine (mach_id, mach_regid, mactype_id, serial_no, room, company_id, site_id, status, model, machbrand_id)
    // select seq_num::integer, barcode_id, item_description::integer, serial_num, location, 'SAA', 'PRB', status, brand, merk::integer
    // from bdj_asset order by seq_num;

    // in Excel
    // =CONCAT("insert into m_machine_type (mactype_id, name) values (",A2, ", '",B2,"');")

    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getStatus($status)
    {   switch($status)
        {   case 0: $result = "Active";  break;
            case 1: $result = "InActive";  break;
            case 2: $result = "Scraped";  break;
        };
        return $result;
    }
    public static function setStatus($status)
    {   switch($status)
        {   case "Active":   $result = 0; break;
            case "InActive": $result = 1; break;
            case "Scraped":  $result = 2; break;
        };
        return $result;
    }
    public static function machineSearch($request)
    {   $results = DB::table('m_machine as a')
                    ->join('m_machine_type as b', 'a.mactype_id', '=', 'b.mactype_id')
                    ->join('m_machine_brand as c', 'a.machbrand_id', '=', 'c.machbrand_id')
                    ->select(DB::raw("a.*,
                        (   case a.status 
                                when 0 then 'Active' 
                                when 1 then 'Inactive' 
                                when 2 then 'Scraped'
                            end
                        ) as status,
                        b.name as mactype_name, c.name as machbrand_name,
                        b.name||' '||c.name as name"))
                    ->orderBy('a.mactype_id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->status == "ALL"){} else
            {   $results->where("a.status", "=", Machine::setStatus($request->status));   };
        };
        return $results->get();
    }
    public static function machineSearchExt($request)
    {   $subQuery = DB::table('m_machine as a')
                    ->join('m_machine_type as b', 'a.mactype_id', '=', 'b.mactype_id')
                    ->join('m_machine_brand as c', 'a.machbrand_id', '=', 'c.machbrand_id')
                    ->selectRaw("a.asset_id, a.mach_regid, b.name||' '||c.name as name,
                                (a.mach_regid || ' [' || b.name||' '||c.name || '] ') as display");
                    // ->whereNotNull('a.asset_id');

        $results = AssetITDHardware::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.asset_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function machineSave($json_data)
    {   
        DB::beginTransaction();
        try 
        {   foreach ($json_data as $json) {
                if (Empty($json->created_date))
                {   $mctype = new Machine(); 
                    $mctype->mactype_id = $json->mactype_id;
                }
                else
                {   $mctype  = Machine::where('mactype_id', '=', $json->mactype_id)
                                ->first();
                };
                
                $mctype->name    = $json->name;
                $mctype->status  = Machine::setStatus($json->status_name);

                $result     = $mctype->save();
            };
            // commit transaction
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        unset($mctype);
        unset($json);
        return $result;
    }
    public static function machineDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('mactype_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
