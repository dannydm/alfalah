<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;
use DB;

class AssetITDHistory extends Model
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'asset_itd_hardware_history';
	protected $primaryKey = 'id';
    public $timestamps = false;    // disable auto generate created value
    public $incrementing = false;  // disable auto generate primary key value
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function SearchHistory($request)
    {   $arr = array();
        $my_arr = array();
        if ($request->s == "form")
        {   // search asset History
            if ($request->source == "ASSET")
            {
                DB::table('asset_itd_History as b1')
                    ->leftjoin('ocsweb_History as a', function($join)
                        {   $join->on('a.id', '=', 'b.id')
                                 ->on('a.deviceid', '=', 'b.deviceid');
                        })
                    ->select('a.*')
                    ->whereNull('a.id')
                    ->orderby('b1.id');
            } // search archive History
            else if ($request->source == "ARCHIVE")
            {   DB::table('asset_itd_History_history as b1')
                    ->select('b1.*')
                    ->orderby("b1.id");
            } else // search ocs / active History
            {   
                $results = DB::table('ocsweb_History as a')
                ->join('ocsweb_accountinfo as b', 'a.id', '=', 'b.History_id')
                ->leftjoin('asset_itd_History as b1', 'a.name', '=', 'b1.name')
                ->leftjoin('asset_reg_detail as b2', function($join)
                    {   $join->on('b1.doc_no', '=', 'b2.doc_no')
                             ->on('b1.asset_id', '=', 'b2.asset_id')
                             ->on('b1.serial_no', '=', 'b2.serial_no');
                    })
                ->leftjoin('m_department as b3', 'b1.dept_id', '=', 'b3.dept_id')
                ->leftjoin('m_item as b4', 'b1.item_id', '=', 'b4.item_id')
                ->select(DB::raw("
                    a.*, b.tag, b1.id as itd_id,
                    b1.asset_id, b1.doc_no, b1.serial_no, b1.item_id, b4.name as item_name,
                    b1.remark, b1.itd_asset_no, b1.status, b1.company_id, b1.site_id,
                    b1.dept_id, b3.name as dept_name,
                    trim(b1.is_server) as is_server, trim(b1.is_client) as is_client,
                    trim(b1.is_vm) as is_vm, b1.macaddr,
                    b1.comp_type, b1.parent_asset_id, b1.is_asset_reported,
                    b1.created_date, b1.created_by,
                    b1.modified_date, b1.modified_by,
                    b2.asset_id as ori_asset_id,
                    ( select array_to_string(array
                        ( select trim(macaddr) from ocsweb_networks
                          where a.id = History_id
                        ), ', ')
                    ) as ori_macaddr
                    "))
                ->orderby('b1.name');

                if ( $request->source == "MISSING" )
                {   $results->where("a.id", "<>", "b.id"); };

                if ( $request->doc_no )
                {   $results->where("a.doc_no", "ilike", "%".$request->doc_no."%"); };

                if ($request->asset_id)
                {   $results->where("b1.asset_id", "ilike", "%".$request->asset_id."%");   };
                
                if ($request->item_id)
                {   $results->where("b1.item_id", "ilike", "%".$request->item_id."%");   };
                
                if ($request->History_name)
                {   $History_name = $request->History_name;
                    $results->where(function($query) use($History_name)
                        {   $query->where("a.name", "ilike", "%".$History_name."%")
                                ->orWhere("b1.name", "ilike", "%".$History_name."%");
                        });
                };
                if ($request->tag)
                {   $results->where("b.tag", "ilike", "%".$request->tag."%");   };
                
                if ($request->username)
                {   $results->where("a.userdomain", "ilike", "%".$request->username."%");   };
                
                if ($request->os_name)
                {   $results->where("a.osname", "ilike", "%".$request->os_name."%");   };
                
                if ($request->processor)
                {   $results->where("a.procesort", "ilike", "%".$request->processor."%");   };
                
                if ($request->ip_address)
                {   $results->where("a.ipaddr", "ilike", "%".$request->ip_address."%");   };
                
                if ($request->is_monitor)
                {   if ($request->is_monitor == "NO") 
                    {   $results->whereNull('b1.created_date'); }
                    else if ($request->is_monitor == "YES") 
                    {   $results->whereNotNull('b1.created_date'); }
                };

                if ($request->status)
                {   if ($request->status == "ALL") {  }
                    else 
                    {   $results->where("b1.status", "=", $request->status); }
                };

                if ($request->comp_type)
                {   if ($request->comp_type == "ALL") {  }
                    else 
                    {   $results->where("b1.comp_type", "=", $request->comp_type); }
                };
            };
        };
        
        // dd($results->get());
        return $results->get();
    }

    // public static function setHistory($data, $user_id)
    // {   
    //     if ($data->created_date == '')  // insert data
    //     {   $this->asset_id = $data->asset_id;
    //         $this->name = $data->name;
    //         $this->created_by = $user_id;
    //     } 
    //     else // modified data
    //     {   $this->modified_by = $user_id; };
        
    //     $this->id                = $data->id;
    //     $this->deviceid          = $data->deviceid;
    //     $this->status            = $data->status;
    //     $this->tag               = $data->tag;
    //     $this->workgroup         = $data->workgroup;
    //     $this->userdomain        = $data->userdomain;
    //     $this->osname            = $data->osname;
    //     $this->osversion         = $data->osversion;
    //     $this->oscomments        = $data->oscomments;
    //     $this->processort        = $data->processort;
    //     $this->processors        = $data->processors;
    //     $this->processorn        = $data->processorn;
    //     $this->memory            = $data->memory;
    //     $this->swap              = $data->swap;
    //     $this->ipaddr            = $data->ipaddr;
    //     $this->dns               = $data->dns;
    //     $this->defaultgateway    = $data->defaultgateway;
    //     $this->etime             = $data->etime;
    //     $this->lastdate          = $data->lastdate;
    //     $this->lastcome          = $data->lastcome;
    //     $this->quality           = $data->quality;
    //     $this->fidelity          = $data->fidelity;
    //     $this->userid            = $data->userid;
    //     $this->type              = $data->type;
    //     $this->description       = $data->description;
    //     $this->macaddr           = $data->ori_macaddr;
    //     $this->wincompany        = $data->wincompany;
    //     $this->winowner          = $data->winowner;
    //     $this->winprodid         = $data->winprodid;
    //     $this->winprodkey        = $data->winprodkey;
    //     $this->useragent         = $data->useragent;
    //     $this->checksum          = $data->checksum;
    //     $this->sstate            = $data->sstate;
    //     $this->ipsrc             = $data->ipsrc;
    //     $this->uuid              = $data->uuid;
    //     $this->item_id           = $data->item_id;
    //     $this->itd_asset_no      = $data->itd_asset_no;
    //     $this->doc_no            = $data->doc_no;
    //     $this->serial_no         = $data->serial_no;
    //     $this->remark            = $data->remark;
    //     $this->company_id        = $data->company_id;
    //     $this->site_id           = $data->site_id;
    //     $this->dept_id           = $data->dept_id;
    //     $this->is_server         = $data->is_server;
    //     $this->is_client         = $data->is_client;
    //     $this->is_vm             = $data->is_vm;
    //     $this->is_asset_reported = $data->is_asset_reported;
    //     $this->comp_type         = $data->comp_type;
    //     $this->parent_asset_id   = $data->parent_asset_id;
    // }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
