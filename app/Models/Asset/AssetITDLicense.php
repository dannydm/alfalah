<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;
use DB;

class AssetITDLicense extends Model
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'asset_itd_license';
	protected $primaryKey = 'id';
    public $timestamps = false;    // disable auto generate created value
    public $incrementing = false;  // disable auto generate primary key value
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function licenseSearch($request)
    {   
        if ($request->allocation == "NO")
        {   $license_table = 'asset_itd_license as b1'; }
        else
        {   $license_table = 'asset_itd_license_allocation as b1'; };

        $results = DB::table('asset_itd_hardware as a')
                ->leftjoin('ocsweb_accountinfo as b', 'a.id', '=', 'b.hardware_id')
                ->leftjoin($license_table, function($join)
                    {   $join->on('a.asset_id', '=', 'b1.asset_id')
                             ->on('a.name', '=', 'b1.name');
                    })
                ->leftjoin('m_department as c', 'a.dept_id', '=', 'c.dept_id')
                ->leftjoin('m_item as d', 'b1.os_item_id', '=', 'd.item_id')
                ->leftjoin('m_item as d1', 'b1.cal_os_item_id', '=', 'd1.item_id')
                ->leftjoin('m_item as d2', 'b1.office_item_id', '=', 'd2.item_id')
                ->select(DB::raw("
                    (case when b1.asset_id is null then a.asset_id else b1.asset_id end) as asset_id,
                    a.name, a.userid, a.ipsrc, a.status, a.site_id, a.processort,
                    (case when a.tag is null then b.tag else a.tag end) as tag,
                    (case when b1.os_name <> a.osname then 1 else 0 end) as os_diff,
                    b1.os_name, b1.os_name as b1_os_name, a.osname as a_os_name,
                    b1.os_type, b1.os_key, b1.os_key as b1_os_key, a.winprodkey as a_os_key,
                    (case when b1.os_key_type is null then 'NA' else b1.os_key_type end) as os_key_type,
                    (case when b1.os_license_type is null then 'NA' else b1.os_license_type end) as os_license_type,
                    (case when b1.os_license_name is null then b1.os_name else b1.os_license_name end) as os_license_name,
                    (case when b1.os_license_key is null then b1.os_key else b1.os_license_key end) as os_license_key,
                    b1.os_asset_id, b1.os_item_id, d.name as os_item_name,
                    b1.cal_os_asset_id, b1.cal_os_item_id, d1.name as cal_os_item_name,
                    (case when b1.office_type is null then 'NON-MS' else b1.office_type end) as office_type,
                    (case when b1.office_license_type is null then 'NA' else b1.office_license_type end) as office_license_type,
                    b1.office_name, b1.office_asset_id, b1.office_item_id, d2.name as office_item_name,
                    (case when b1.office_license_name is null then b1.office_name else b1.office_license_name end) as office_license_name,
                    b1.office_key, b1.office_soft_id,
                    b1.av_type, b1.av_name, b1.av_key, b1.av_license_type, b1.av_license_name, b1.av_license_key,
                    (case when b1.diagram_type is null then 'NON-MS' else b1.diagram_type end) as diagram_type,
                    (case when b1.diagram_license_type is null then 'NA' else b1.diagram_license_type end) as diagram_license_type,
                    b1.diagram_name, b1.diagram_key, b1.diagram_soft_id, b1.remark,
                    b1.created_date, b1.created_by,
                    b1.modified_date, b1.modified_by,
                    c.name as dept_name
                    "))
                ->orderby('a.name');
        
        if ($request->s == "form")
        {   if ( $request->asset_id )
            {   $results->where("b1.asset_id", "ilike", "%".$request->asset_id."%"); };

            if ( $request->asset_name )
            {   $results->where("a.name", "ilike", "%".$request->asset_name."%"); };

            if ( $request->tag )
            {   $results->where("a.tag", "ilike", "%".$request->tag."%"); };

            if ( $request->status <> "ALL")
            {   $results->where("a.status", "=", $request->status); };

            if ($request->os_key)
                {   $v_query = $request->os_key;
                    $results->where(function($query) use($v_query)
                        {   $query->where("a.winprodkey", "ilike", "%".$v_query."%")
                                ->orWhere("b1.os_key", "ilike", "%".$v_query."%")
                                ->orWhere("b1.os_license_key", "ilike", "%".$v_query."%");
                        });
                };

            if ( $request->os_name )
            {   $results->where("a.osname", "ilike", "%".$request->os_name."%"); };

            if ( $request->os_type <> "ALL")
            {   $results->where("b1.os_type", "=", $request->os_type); };

            if ( $request->os_key_type <> "ALL")
            {   $results->where("b1.os_key_type", "=", $request->os_key_type); };

            if ( $request->os_item_name )
            {   $results->where("d.name", "ilike", "%".$request->os_item_name."%"); };
    
            if ( $request->os_lic_type <> "ALL")
            {   $results->where("b1.os_license_type", "=", $request->os_lic_type); };

            if ( $request->reported <> "ALL")
            {   $results->where("a.is_asset_reported", "=", $request->reported); };
        };
        // dd($results->get());
        return $results->get();
    }
    public static function licenseSave($json_data, $user_id)
    {   //dd($json_data);
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {   // save license
                    // dd($json);
                    if ($json->created_date == '')  
                    {   // insert license
                        $asset = new AssetITDLicense();
                        $asset->asset_id = $json->asset_id;
                        $asset->name = $json->name;
                        $asset->created_by = $user_id;
                    }
                    else 
                    {   // update license
                        $asset = AssetITDLicense::where('asset_id', '=', $json->asset_id)
                                    ->where('name', '=', $json->name)
                                    ->first();
                        $asset->modified_by = $user_id;
                    };
                    
                    $asset->os_type              = $json->os_type;
                    $asset->os_name              = $json->a_os_name;
                    $asset->os_key               = $json->a_os_key;
                    $asset->os_key_type          = $json->os_key_type; 
                    $asset->os_license_type      = $json->os_license_type; 
                    $asset->os_license_name      = $json->os_license_name;
                    $asset->os_license_key       = $json->os_license_key;
                    $asset->os_asset_id          = $json->os_asset_id;
                    $asset->os_item_id           = $json->os_item_id;
                    $asset->cal_os_asset_id      = $json->cal_os_asset_id;
                    $asset->cal_os_item_id       = $json->cal_os_item_id;
                    $asset->office_type          = $json->office_type;
                    $asset->office_name          = $json->office_name;
                    $asset->office_key           = $json->office_key;
                    $asset->office_license_type  = $json->office_license_type;
                    $asset->office_license_name  = $json->office_license_name;
                    $asset->office_soft_id       = $json->office_soft_id;
                    $asset->office_asset_id      = $json->office_asset_id;
                    $asset->office_item_id       = $json->office_item_id;
                    $asset->av_type              = $json->av_type;
                    $asset->av_name              = $json->av_name;
                    $asset->av_key               = $json->av_key;
                    $asset->av_license_type      = $json->av_license_type;
                    $asset->av_license_name      = $json->av_license_name;
                    $asset->av_license_key       = $json->av_license_key;
                    $asset->diagram_type         = $json->diagram_type;
                    $asset->diagram_name         = $json->diagram_name;
                    $asset->diagram_key          = $json->diagram_key;
                    $asset->diagram_license_type = $json->diagram_license_type;
                    $asset->diagram_soft_id      = $json->diagram_soft_id;
                    $asset->remark               = $json->remark;

                    $result = $asset->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function licenseDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('asset_id', '=', $request->id)
                                ->where('name', '=', $request->name)
                                ->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
