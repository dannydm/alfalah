<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;
use DB;

class MachineBrand extends Model
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_machine_brand';
	protected $primaryKey = 'machbrand_id';
    public $timestamps = false;    // disable auto generate created value
    public $incrementing = false;  // disable auto generate primary key value
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getStatus($status)
    {   switch($status)
        {   case 0: $result = "Active";  break;
            case 1: $result = "InActive";  break;
        };
        return $result;
    }
    public static function setStatus($status)
    {   switch($status)
        {   case "Active": $result = 0; break;
            case "InActive": $result = 1; break;
        };
        return $result;
    }
    public static function machinebrandSearch($request)
    {   $results = static::selectRaw("*, 
                        ( case status when 0 then 'Active' else 'Inactive' end) as status_name")
                    ->orderBy('machbrand_id', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->name)
            {   $results->where("name", "ilike", "%".$request->name."%");   };
            if ($request->status == "ALL"){} else
            {   $results->where("status", "=", MachineBrand::setStatus($request->status));   };
        };
        return $results->get();
    }
    public static function machinebrandSearchExt($request)
    {   $subQuery = DB::table('m_machine_brand as a')
                    ->selectRaw("a.machbrand_id, a.name,
                                (a.machbrand_id || '-' ||a.name) as display");

        $results = MachineBrand::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.machbrand_id', 'asc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };

        return $results->get();
    }
    public static function machinebrandSave($json_data, $site)
    {   
        DB::beginTransaction();
        try 
        {   foreach ($json_data as $json) {
                if (Empty($json->created_date))
                {   $mcbrand = new MachineBrand(); 
                    $mcbrand->machbrand_id = $site.$json->machbrand_id;
                }
                else
                {   $mcbrand  = MachineBrand::where('machbrand_id', '=', $json->machbrand_id)
                                ->first();
                };
                
                $mcbrand->name        = $json->name;
              //  $mcbrand->description = $json->description;
                $mcbrand->status      = MachineBrand::setStatus($json->status_name);

                $result     = $mcbrand->save();
            };
            // commit transaction
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        unset($mcbrand);
        unset($json);
        return $result;
    }
    public static function machinebrandDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('machbrand_id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
