<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;
use DB;

class AssetITDNonComputer extends Model
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'asset_itd_noncomputer';
	protected $primaryKey = 'asset_id';
    public $timestamps = false;    // disable auto generate created value
    public $incrementing = false;  // disable auto generate primary key value
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function Search($request)
    {   
        if ($request->s == "form")
        {   // search asset hardware
            $results = DB::table('asset_itd_noncomputer as b1')
                ->leftjoin('m_department as b3', 'b1.dept_id', '=', 'b3.dept_id')
                ->leftjoin('m_item as b4', 'b1.item_id', '=', 'b4.item_id')
                ->leftjoin('site_building as b5', 'b1.building_id', '=', 'b5.building_id')
                ->select('b1.*', 'b3.name as dept_name', 'b5.name as building_name')
                ->where("b1.item_id", "ilike", "D/34%")
                ->orderby('b1.name');

            if ($request->is_monitor)
            {   if ($request->is_monitor == "NO") 
                {   $results->whereNull('b1.created_date'); }
                else if ($request->is_monitor == "YES") 
                {   $results->whereNotNull('b1.created_date'); }
            };
            
            //     if ( $request->source == "MISSING" )
            //     {   $results->where("a.id", "<>", "b1.id"); 
            //         //$results->whereNull('b1.id');
            //     };

            //     if ( $request->doc_no )
            //     {   $results->where("a.doc_no", "ilike", "%".$request->doc_no."%"); };

            //     if ($request->asset_id)
            //     {   $results->where("b1.asset_id", "ilike", "%".$request->asset_id."%");   };
                
            //     if ($request->item_id)
            //     {   $results->where("b1.item_id", "ilike", "%".$request->item_id."%");   };
                
            //     if ($request->hardware_name)
            //     {   $hardware_name = $request->hardware_name;
            //         $results->where(function($query) use($hardware_name)
            //             {   $query->where("a.name", "ilike", "%".$hardware_name."%")
            //                     ->orWhere("b1.name", "ilike", "%".$hardware_name."%");
            //             });
            //     };
            //     if ($request->tag)
            //     {   $results->where("b.tag", "ilike", "%".$request->tag."%");   };
                
            //     if ($request->username)
            //     {   $results->where("a.userdomain", "ilike", "%".$request->username."%");   };
                
            //     if ($request->os_name)
            //     {   $results->where("a.osname", "ilike", "%".$request->os_name."%");   };
                
            //     if ($request->processor)
            //     {   $results->where("a.processort", "ilike", "%".$request->processor."%");   };
                
            //     if ($request->ip_address)
            //     {   $results->where("a.ipaddr", "ilike", "%".$request->ip_address."%");   };
                
         

            //     if ($request->status)
            //     {   if ($request->status == "ALL") {  }
            //         else 
            //         {   $results->where("b1.status", "=", $request->status); }
            //     };

            //     if ($request->comp_type)
            //     {   if ($request->comp_type == "ALL") {  }
            //         else 
            //         {   $results->where("b1.comp_type", "=", $request->comp_type); }
            //     };
            // };
        };
        
        // dd($results->get());
        return $results->get();
    }
    public static function Register($json_data, $type, $user_id)
    {   // register non computers data
        if ( ($type == 'reg') && (count($json_data) > 0) )
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {   $asset = new AssetITDNonComputer();
                    $asset->doc_no            = $json->doc_no;
                    $asset->asset_id          = $json->asset_id;
                    $asset->itd_asset_no      = $json->serial_no;
                    $asset->deviceid          = $json->asset_id;
                    $asset->name              = $json->item_name;
                    $asset->remark            = trim($json->remark);
                    $asset->status            = 'ACTIVE';
                    $asset->item_id           = $json->ref_item_id;
                    $asset->created_by        = $user_id;
                    $result = $asset->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function noncomputerSave($json_data, $type, $user_id)
    {   if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {   // save hardware
                    if ($json->created_date == '')  
                    {   // insert hardware through register procedure
                        // $asset = new AssetITDNonComputer();
                        // $asset->asset_id = $json->asset_id;
                        // $asset->name = $json->name;
                        // $asset->created_by = $user_id;
                    }
                    else 
                    {   // update hardware
                        $asset = AssetITDNonComputer::where('asset_id', '=', $json->asset_id)
                                    ->first();
                        $asset->modified_by = $user_id;
                    };

                    $asset->name              = $json->name;
                    $asset->remark            = trim($json->remark);
                    $asset->status            = $json->status;
                    $asset->company_id        = $json->company_id;
                    $asset->site_id           = $json->site_id;
                    $asset->dept_id           = $json->dept_id;
                    $asset->building_id       = $json->building_id;
                    $asset->comp_type         = $json->comp_type;

                    $result = $asset->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function noncomputerDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
