<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;
use DB;

class AssetDocCode extends Model
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'asset_doc_code';
	protected $primaryKey = 'id';
    public $timestamps = false;    // disable auto generate created value
    public $incrementing = false;  // disable auto generate primary key value
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getStatus($status)
    {   switch($status)
        {   case 0: $result = "Active";  break;
            case 1: $result = "Inactive";  break;
        };
        return $result;
    }
    public static function setStatus($status)
    {   switch($status)
        {   case "Active": $result = 0; break;
            case "Inactive": $result = 1; break;
        };
        return $result;
    }
    public static function docCodeSearch($request)
    {   
        $results = DB::table('asset_doc_code as a')
                ->join('m_department as b', 'a.dept_id', '=', 'b.dept_id')
                ->selectRaw("a.*, b.name as dept_name, 
                    ( case a.status when 0 then 'Active' else 'Inactive' end) as status_name");
        
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->company_id)
            {   $results->where("a.company_id", "ilike", "%".$request->company_id."%");   };
            if ($request->site_id)
            {   $results->where("a.site_id", "ilike", "%".$request->site_id."%");   };
            if ($request->name)
            {   $results->where("b.name", "ilike", "%".$request->name."%");   };
        };
        return $results->get();
    }
    public static function docCodeSave($json_data)
    {   
        DB::beginTransaction();
        try 
        {   foreach ($json_data as $json) {
                if (Empty($json->created_date))
                {   $doccode = new AssetDocCode(); 
                    $doccode->company_id = $json->company_id;
                    $doccode->site_id    = $json->site_id;
                    $doccode->doc_code   = $json->doc_code;
                }
                else
                {   $doccode  = AssetDocCode::where('company_id', '=', $json->company_id)
                                ->where('site_id', '=', $json->site_id)
                                ->where('doc_code', '=', $json->doc_code)
                                ->first();
                };
                
                $doccode->name    = $json->name;
                $doccode->dept_id = $json->dept_id;
                $doccode->status  = AssetDocCode::setStatus($json->status_name);

                $result     = $doccode->save();
            };
            // commit transaction
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        unset($doccode);
        unset($json);
        return $result;
    }
    public static function docCodeDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentCompany()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Company', 'company_id', 'company_id');
    }
    public function parentSite()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Site', 'site_id', 'site_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
