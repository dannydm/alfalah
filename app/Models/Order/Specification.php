<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;
use DB;

class Specification extends Model
{	/*      
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'specifications';
	protected $primaryKey = 'id';
    public $timestamps = false;    // disable auto generate created value
    public $incrementing = false;  // disable auto generate primary key value
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    protected function getNextID($site)
    {
        $result = DB::select("select nextval('specifications_".$site."_seq')");
        $result = $site.$result['0']->nextval;
        return $result;
    }
    
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function Search($request)
    {   
        if ($request->s == "form")
        {   // search asset hardware
            $results = DB::table('specifications as a')
                ->leftjoin('m_emp as x1', 'x1.emp_id', '=', 'a.reporter_id')
                ->leftjoin('m_emp as x2', 'x2.emp_id', '=', 'a.technician_id')
                ->leftjoin('m_users as x3', 'x3.user_id', '=', 'a.assigned_from')
                ->leftjoin('m_users as x4', 'x4.user_id', '=', 'a.assigned_to')
                ->select(DB::raw("
                    a.id, a.asset_id, a.mach_regid, a.name, a.asset_type,
                    TO_CHAR(a.broken_date,'DD/MM/YYYY') as broken_date,
                    TO_CHAR(a.broken_date,'HH24:MI') as broken_time,
                    TO_CHAR(a.repair_started,'DD/MM/YYYY') as repair_date,
                    TO_CHAR(a.repair_started,'HH24:MI') as repair_time,
                    TO_CHAR(a.repair_ended,'DD/MM/YYYY') as finish_date,
                    TO_CHAR(a.repair_ended,'HH24:MI') as finish_time,
                    a.broken_description, a.repair_action, a.reporter_id,
                    a.technician_id, 
                    a.assigned_to, a.assigned_from, 
                    a.created_date, a.created_by, 
                    a.modified_date, a.modified_by, 
                    x1.name as reporter_name, x2.name as technician_name,
                    x3.username as assigned_from_name, x4.username as assigned_to_name"
                    ));
                // ->where("b1.item_id", "ilike", "D/34%")
                // ->specificationby('b1.name');

            
            //     if ( $request->source == "MISSING" )
            //     {   $results->where("a.id", "<>", "b1.id"); 
            //         //$results->whereNull('b1.id');
            //     };

            //     if ( $request->doc_no )
            //     {   $results->where("a.doc_no", "ilike", "%".$request->doc_no."%"); };

            //     if ($request->asset_id)
            //     {   $results->where("b1.asset_id", "ilike", "%".$request->asset_id."%");   };
                
            //     if ($request->item_id)
            //     {   $results->where("b1.item_id", "ilike", "%".$request->item_id."%");   };
                
            //     if ($request->hardware_name)
            //     {   $hardware_name = $request->hardware_name;
            //         $results->where(function($query) use($hardware_name)
            //             {   $query->where("a.name", "ilike", "%".$hardware_name."%")
            //                     ->orWhere("b1.name", "ilike", "%".$hardware_name."%");
            //             });
            //     };
            //     if ($request->tag)
            //     {   $results->where("b.tag", "ilike", "%".$request->tag."%");   };
                
            //     if ($request->username)
            //     {   $results->where("a.userdomain", "ilike", "%".$request->username."%");   };
                
            //     if ($request->os_name)
            //     {   $results->where("a.osname", "ilike", "%".$request->os_name."%");   };
                
            //     if ($request->processor)
            //     {   $results->where("a.processort", "ilike", "%".$request->processor."%");   };
                
            //     if ($request->ip_address)
            //     {   $results->where("a.ipaddr", "ilike", "%".$request->ip_address."%");   };
                
         

            //     if ($request->status)
            //     {   if ($request->status == "ALL") {  }
            //         else 
            //         {   $results->where("b1.status", "=", $request->status); }
            //     };

            //     if ($request->comp_type)
            //     {   if ($request->comp_type == "ALL") {  }
            //         else 
            //         {   $results->where("b1.comp_type", "=", $request->comp_type); }
            //     };
            // };
        };
        
        // dd($results->get());
        return $results->get();
    }
    public static function specificationSave($json_data, $site, $user_id)
    {   if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {   // save hardware
                    if ($json->created_date == '')  
                    {   
                        $specification = new Specification();
                        $specification->id = $specification->getNextID($site);
                        if (Empty($json->mach_regid))
                        {   $specification->asset_id   = $json->asset_id;
                            $specification->asset_type = 'ITD';
                        }
                        else
                        {   $specification->asset_id = $json->mach_regid;
                            $specification->asset_type = 'MAC';
                        };
                        $specification->created_by = $user_id;
                    }
                    else 
                    {   // update hardware
                        $specification = Specification::where('id', '=', $json->id)
                                    ->first();
                        $specification->modified_by = $user_id;
                    };

                    $specification->name               = $json->asset_name;
                    $specification->reporter_id        = ($json->reporter_id === "" ? NULL : $json->reporter_id);
                    $specification->technician_id      = ($json->technician_id === "" ? NULL : $json->technician_id);
                    $specification->assigned_from      = ($json->assigned_from === "" ? NULL : $json->assigned_from);
                    $specification->assigned_to        = ($json->assigned_to === "" ? NULL : $json->assigned_to);
                    if (($json->broken_date ) )
                    {   if ($json->broken_time){} else { $json->broken_time = "00:00";};
                        $specification->broken_date = $json->broken_date.' '.$json->broken_time;
                    } 
                    else {   $specification->broken_date = null;  };
                    $specification->broken_description = trim($json->broken_description);
                    
                    if (($json->repair_date ) )
                    {   if ($json->repair_time){} else { $json->repair_time = "00:00";};
                        $specification->repair_started = $json->repair_date.' '.$json->repair_time;
                    } 
                    else {   $specification->repair_started = null;  };
                    $specification->repair_action      = trim($json->repair_action);

                    if (($json->finish_date ) )
                    {   if ($json->finish_time){} else { $json->finish_time = "00:00";};
                        $specification->repair_ended = $json->finish_date.' '.$json->finish_time;
                    } 
                    else {   $specification->repair_ended = null;  };
                    
                    $result = $specification->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function specificationDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)
                        ->where('asset_id', '=', $request->asset_id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
