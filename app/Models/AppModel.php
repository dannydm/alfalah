<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AppModel extends Model
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    public $timestamps = false;    // disable auto generate created value
    public $incrementing = false;  // disable auto generate primary key value
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getStatus($status)
    {   switch($status)
        {   case 0: $result = "Active";  break;
            case 1: $result = "InActive";  break;
        };
        return $result;
    }
    public static function setStatus($status)
    {   switch($status)
        {   case "Active": $result = 0; break;
            case "Inactive": $result = 1; break;
        };
        return $result;
    }
    public static function getYesNo($yesno)
    {   switch($yesno)
        {   case 0: $result = "Yes";  break;
            case 1: $result = "No";  break;
        };
        return $result;
    }
    public static function setYesNo($yesno)
    {   switch($yesno)
        {   case "Yes": $result = 0; break;
            case "No":  $result = 1; break;
        };
        return $result;
    }
    public static function getDraftFinal($status)
    {   switch($status)
        {   case 0: $result = "Draft";  break;
            case 1: $result = "Final";  break;
        };
        return $result;
    }
    public static function setDraftFinal($status)
    {   switch($status)
        {   case "Draft": $result = 0; break;
            case "Final": $result = 1; break;
        };
        return $result;
    }
    public static function getApproveStatus($status)
    {   switch($status)
        {   case 0: $result = "Need Approval";  break;
            case 1: $result = "Approved";       break;
        };
        return $result;
    }
    public static function setApproveStatus($status)
    {   switch($status)
        {   case "Need Approval": $result = 0;  break;
            case "Approved"     : $result = 1;  break;
        };
        return $result;
    }
    public static function getNextID($sequence_name, $site)
    {   $result = DB::select("select nextval('".$sequence_name.$site."_seq')");
        $result = $site.$result['0']->nextval;
        return $result;
    }
    public function makeTimeStamp($the_date,$the_format)
    {   switch($the_format)
        {   case "d/m/Y":
            {   $day    = substr($the_date,0,2);
                $month  = substr($the_date,3,2);
                $year   = substr($the_date,6,4);
            };
            break;
            case "Y-m-d":
            {   $year   = substr($the_date,0,4);
                $month  = substr($the_date,5,2);
                $day    = substr($the_date,8,2);
            };
            break;
        };
        return date("Y-m-d H:i:s", mktime(0, 0, 0, $month, $day, $year)); //Y-m-d H:i:s
    }
   public static function myTasks($user_id, $menu_id)
    {
        $results = DB::table('user_task')
                    ->select('task_action')
                    ->where('user_id', '=', $user_id)
                    ->where('menu_id', '=', $menu_id)
                    ->orderby('id', 'asc')
                    ->get();
        return $results->pluck('task_action')->toArray();
        // return $results;
    } 
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
