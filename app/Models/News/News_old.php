<?php

namespace App\Models\News;

use App\Models\AppModel;
use DB;

class News extends AppModel
{	
    /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------
    */
    // CREATE SEQUENCE news_seq;
    // CREATE TABLE news
    // (
    //   news_id numeric(18) NOT NULL DEFAULT nextval('news_seq'::regclass),
    //   title varchar(64) NOT NULL,
    //   category_1 numeric(1) NOT NULL,
    //   category_2 numeric(1) NOT NULL,
    //   category_3 numeric(1) NOT NULL,
    //   status numeric(1,0) NOT NULL DEFAULT 0,
    //   content varchar(2000) NOT NULL,
    //   publish_start_date timestamp,
    //   publish_end_date timestamp,
    //   created_date timestamp without time zone DEFAULT now(),
    //   created_by numeric(18) NOT NULL,
    //   modified_date timestamp without time zone DEFAULT now(),
    //   modified_by numeric(18),
    //   CONSTRAINT news_pk PRIMARY KEY (news_id),
    //   CONSTRAINT news_sts_ckc CHECK (status = 1::numeric OR status = 0::numeric)
    // );
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'news';
	protected $primaryKey = 'news_id';
    public $sequence_name = 'news_seq';
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */   
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getStatus($status)
    {   switch($status)
        {   case 0: $result = "Active";  break;
            case 1: $result = "InActive";  break;
        };
        return $result;
    }
    public static function setStatus($status)
    {   switch($status)
        {   case "Active":   $result = 0; break;
            case "InActive": $result = 1; break;
        };
        return $result;
    }
    public static function getCategory_1($status)
    {   switch($status)
        {   case 0: $result = "ALL";        break;
            case 1: $result = "TK";         break;
            case 2: $result = "SD";         break;
            case 3: $result = "SMP";        break;
            case 4: $result = "SMA";        break;
            case 5: $result = "perSiswa";   break;
        };
        return $result;
    }
    public static function setCategory_1($status)
    {   switch($status)
        {   case "ALL":      $result = 0; break;
            case "TK":       $result = 1; break;
            case "SD":       $result = 2; break;
            case "SMP":      $result = 3; break;
            case "SMA":      $result = 4; break;
            case "perSiswa": $result = 5; break;
        };
        return $result;
    }
    public static function Search($request)
    {   
        $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY news_id)) 
                        as record_id, *,
                        (   case category_1
                                when 0 then 'ALL'
                                when 1 then 'TK'
                                when 2 then 'SD'
                                when 3 then 'SMP'
                                when 4 then 'SMA'
                                when 5 then 'perSiswa'
                            end
                        )   as category_1_name,
                        (   case status 
                                when 0 then 'Active'
                                when 1 then 'InActive'
                            end
                        ) as status_name ")
                    ->orderBy('news_id', 'asc');

        if ($request->s == "form")
        {   
            if ( $request->news_id )
            {   $results->where("news_id", "=", $request->news_id); };
            if ( $request->title)
            {   $results->where("title", "ilike","%".$request->title."%"); };
            if ( $request->status)
            {   $results->where("status", "=", News::setStatus($request->status)); };
        };        
        return $results->get();

    }
    public static function SearchExt($request)
    {   
        $subQuery = DB::table('News as a')
                    ->selectRaw("a.news_id, 
                                (a.news_id) as display")
                    ->where('a.status', '=', 0);

        $results = News::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->Newsby('x.news_id', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function headerPrintOut($request)
    {   
        $results = DB::table('News as a')
            ->leftjoin('m_company as x1', 'x1.company_id', '=', 'a.company_id')
            ->leftjoin('m_container_type as x2', 'x2.container_type_id', '=', 'a.container_type')
            ->leftjoin('m_city as x3', 'x3.city_id', '=', 'a.port_loading')
            ->leftjoin('m_city as x4', 'x4.city_id', '=', 'a.port_discharge')
            ->leftjoin('m_company as x5', 'x5.company_id', '=', 'a.buyer_id')
            ->select(DB::raw("
                trim(a.id) as id, a.news_id, a.company_id, a.currency_id, a.description, 
                a.buyer_id, a.buyer_name, 
                to_char(a.News_date, 'dd/mm/yyyy') as News_date, 
                a.pi_no, to_char(a.pi_date, 'dd/mm/yyyy') as pi_date, 
                a.bank_id, a.bank_name, a.bank_account, a.bank_address, a.swift_code, 
                a.container_type, a.payment_term, a.payment_term_id,
                a.port_loading, a.port_discharge, 
                coalesce(a.cbm,0) as cbm, 
                coalesce(a.weight_gross, 0) as weight_gross, 
                coalesce(a.weight_nett, 0) as weight_nett,
                to_char(a.shipment_date,'dd/mm/yyyy') as shipment_date, 
                to_char(a.etd, 'dd/mm/yyyy') as etd, 
                to_char(a.eta,  'dd/mm/yyyy') as eta,
                x1.name as company_name, x1.address as company_address,
                x2.description as container_type_name,
                x3.name as port_loading_name, 
                x4.name as port_discharge_name,
                x5.address as buyer_address
                "))
            ->Newsby('a.id');

        if ($request->s == "form")
        {   
            if ( $request->id )
            {   $results->where("a.id", "=", $request->id); };
        };

        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };
    }
    public static function Newssave( $json_data, $user_id )
    {   
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   
                foreach ($json_data as $json) 
                {   // save News
                    if ($json->created_date == '')  
                    {   $News = new News();
                        // $News->news_id = ""; // auto generate default constraint 
                        // $News->company_id = env('APP_COMPANY');
                        // $News->company_id = config('app.system_company');
                        $News->content    = "New Content";
                        $News->created_by = $user_id;
                    }
                    else 
                    {   // update News
                        $News = News::where('news_id', '=', $json->news_id)->first();
                        $News->modified_by = $user_id;
                    };

                    $News->title              = $json->title;
                    $News->category_1         = $News->setCategory_1($json->category_1_name);
                    $News->category_2         = 0;
                    $News->category_3         = 0;
                    $News->status             = $News->setStatus($json->status_name);
                    // $News->content            = $json->content;
                    $News->publish_start_date = $json->publish_start_date;
                    $News->publish_end_date   = $json->publish_end_date;
                    $result                   = $News->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function Contentsave( $data, $user_id )
    {   
        if ($data->created_date == '') { $result = false; }
        else
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   
                // save News
                $News = News::where('news_id', '=', $data->news_id)->first();
                $News->modified_by = $user_id;
                $News->title              = $data->title;
                $News->category_1         = $News->setCategory_1($data->category_1_name);
                $News->category_2         = 0;
                $News->category_3         = 0;
                $News->status             = $News->setStatus($data->status_name);
                $News->content            = $data->content;
                $News->publish_start_date = $data->publish_start_date;
                $News->publish_end_date   = $data->publish_end_date;
                $result                   = $News->save();
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function Profile($nis)
    {   
        $result = static::where('nis', '=', $nis)->first();
        return $result;
    }
    public static function NewsDelete($request)
    {   
        DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)
                        ->where('asset_id', '=', $request->asset_id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function ActiveNews($request)
    {   
        $results = static::where('status', '=', 0)->get();
        $html_page = "";
        if (count($results) > 0 )
        {   foreach ($results as $result)
            {   $html_page = $html_page."<BR>".$result->content."<BR>"; }; 
        } 
        else
        {
            $html_page = "<H1> No Data Available </H1>";
        };
        return $html_page;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
