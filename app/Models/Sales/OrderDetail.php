<?php

namespace App\Models\Sales;

use App\Models\AppModel;
use DB;

class OrderDetail extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'order_detail';
	protected $primaryKey = 'id';
    public $sequence_name = 'order_detail_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */ 
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   //$results = static::orderby('id', 'asc')
        $results = DB::table('order_detail as a')
            ->leftjoin('m_item as b', 'a.item_id', '=', 'b.item_id')
            ->select(DB::raw(" a.*, b.name as item_name") )
            ->orderby("a.order_no", "asc")
            ->orderby("a.item_id", "asc");
        
        if ($request->s == "form")
        {   if ( $request->order_no )
            {   $results->where("order_no", "ilike", "%".$request->order_no."%"); };
        };

        return $results->get();
    }
    public static function detailPrintOut($order_no)
    {   $results = DB::table('order_detail as a')
            ->leftjoin('m_item as x1', 'x1.item_id', '=', 'a.item_id')
            ->select(DB::raw("
                a.item_id, a.quantity_fcl, a.quantity_bags, a.quantity_nut_pcs, 
                a.quantity_nut_kgs, a.unit_price, a.amount,
                x1.name as item_name
                "))
            ->where("order_no", "=", $order_no)
            ->orderby('x1.name');

        return $results->get();
    }
    public static function orderdetailSave($json_data, $site, $user_id, $company_id, $order_no)
    {   if (count($json_data) > 0)
        {   // transaction control ikut parent orders
            foreach ($json_data as $json) 
            {   // save hardware
                if ($json->created_date == '')  
                {   
                    $orderdetail = new OrderDetail();
                    $orderdetail->id         = self::getNextID(self::sequence_name, $site);
                    $orderdetail->order_no   = $order_no;
                    $orderdetail->item_id    = $json->item_id;
                    $orderdetail->created_by = $user_id;
                }
                else 
                {   // update hardware
                    $orderdetail = OrderDetail::where('id', '=', $json->id)->first();
                    $orderdetail->modified_by   = $user_id;
                    $orderdetail->modified_date = Date('d/m/Y');
                };

                $orderdetail->unit_price       = $json->unit_price;
                $orderdetail->quantity_fcl     = $json->quantity_fcl;
                $orderdetail->quantity_bags    = $json->quantity_bags;
                $orderdetail->quantity_nut_pcs = $json->quantity_nut_pcs;
                $orderdetail->quantity_nut_kgs = $json->quantity_nut_kgs;
                $orderdetail->currency_id      = $json->currency_id;
                $orderdetail->amount           = $json->amount;
                
                $result = $orderdetail->save();
            };
        };
        return $result;
    }
    public static function orderdetailDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentOrder()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Sales\Order', 'order_no', 'order_no');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
