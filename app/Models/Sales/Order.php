<?php

namespace App\Models\Sales;

use App\Models\AppModel;
use App\Models\Sales\OrderDetail;
use App\Models\Administrator\SiteDocument;
use DB;

class Order extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'orders';
	protected $primaryKey = 'order_no';
    public $sequence_name = 'orders_';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */   
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   
        $results = DB::table('orders as a')
            ->leftjoin('m_company as x1', 'x1.company_id', '=', 'a.company_id')
            ->leftjoin('m_container_type as x2', 'x2.container_type_id', '=', 'a.container_type')
            ->leftjoin('m_city as x3', 'x3.city_id', '=', 'a.port_loading')
            ->leftjoin('m_city as x4', 'x4.city_id', '=', 'a.port_discharge')
            ->select(DB::raw("
                trim(a.id) as id, a.order_no, a.company_id, a.currency_id, a.description, 
                a.buyer_id, a.buyer_name, a.status, 
                to_char(a.order_date, 'dd/mm/yyyy') as order_date, 
                a.created_by, a.created_date, a.modified_by, a.modified_date, 
                a.pi_no, to_char(a.pi_date, 'dd/mm/yyyy') as pi_date, a.repeat_order_no, 
                a.bank_id, a.bank_name, a.bank_account, a.bank_address, a.swift_code, 
                a.container_type, a.payment_term, a.payment_term_id,
                a.port_loading, a.port_discharge, 
                coalesce(a.cbm,0) as cbm, 
                coalesce(a.weight_gross, 0) as weight_gross, 
                coalesce(a.weight_nett, 0) as weight_nett,
                to_char(a.shipment_date,'dd/mm/yyyy') as shipment_date, 
                to_char(a.etd, 'dd/mm/yyyy') as etd, 
                to_char(a.eta,  'dd/mm/yyyy') as eta,
                x1.name as company_name, x2.description as container_type_name,
                x3.name as port_loading_name, x4.name as port_discharge_name
                "))
            ->orderby('a.id');

        if ($request->s == "form")
        {   
            if ( $request->id )
            {   $results->where("a.id", "=", $request->id); };

            //     if ( $request->doc_no )
            //     {   $results->where("a.doc_no", "ilike", "%".$request->doc_no."%"); };

            //     if ($request->asset_id)
            //     {   $results->where("b1.asset_id", "ilike", "%".$request->asset_id."%");   };
                
            //     if ($request->item_id)
            //     {   $results->where("b1.item_id", "ilike", "%".$request->item_id."%");   };
                
            //     if ($request->order_name)
            //     {   $order_name = $request->order_name;
            //         $results->where(function($query) use($order_name)
            //             {   $query->where("a.name", "ilike", "%".$order_name."%")
            //                     ->orWhere("b1.name", "ilike", "%".$order_name."%");
            //             });
            //     };
            //     if ($request->tag)
            //     {   $results->where("b.tag", "ilike", "%".$request->tag."%");   };
                
            //     if ($request->username)
            //     {   $results->where("a.userdomain", "ilike", "%".$request->username."%");   };
                
            //     if ($request->os_name)
            //     {   $results->where("a.osname", "ilike", "%".$request->os_name."%");   };
                
            //     if ($request->processor)
            //     {   $results->where("a.processort", "ilike", "%".$request->processor."%");   };
                
            //     if ($request->ip_address)
            //     {   $results->where("a.ipaddr", "ilike", "%".$request->ip_address."%");   };
                
         

            //     if ($request->status)
            //     {   if ($request->status == "ALL") {  }
            //         else 
            //         {   $results->where("b1.status", "=", $request->status); }
            //     };

            //     if ($request->comp_type)
            //     {   if ($request->comp_type == "ALL") {  }
            //         else 
            //         {   $results->where("b1.comp_type", "=", $request->comp_type); }
            //     };
            // };
        };
        
        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };
    }
    public static function SearchExt($request)
    {   
        $subQuery = DB::table('orders as a')
                    ->selectRaw("a.order_no, 
                                (a.order_no) as display")
                    ->where('a.status', '=', 0);

        $results = Order::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.order_no', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function headerPrintOut($request)
    {   
        $results = DB::table('orders as a')
            ->leftjoin('m_company as x1', 'x1.company_id', '=', 'a.company_id')
            ->leftjoin('m_container_type as x2', 'x2.container_type_id', '=', 'a.container_type')
            ->leftjoin('m_city as x3', 'x3.city_id', '=', 'a.port_loading')
            ->leftjoin('m_city as x4', 'x4.city_id', '=', 'a.port_discharge')
            ->leftjoin('m_company as x5', 'x5.company_id', '=', 'a.buyer_id')
            ->select(DB::raw("
                trim(a.id) as id, a.order_no, a.company_id, a.currency_id, a.description, 
                a.buyer_id, a.buyer_name, 
                to_char(a.order_date, 'dd/mm/yyyy') as order_date, 
                a.pi_no, to_char(a.pi_date, 'dd/mm/yyyy') as pi_date, 
                a.bank_id, a.bank_name, a.bank_account, a.bank_address, a.swift_code, 
                a.container_type, a.payment_term, a.payment_term_id,
                a.port_loading, a.port_discharge, 
                coalesce(a.cbm,0) as cbm, 
                coalesce(a.weight_gross, 0) as weight_gross, 
                coalesce(a.weight_nett, 0) as weight_nett,
                to_char(a.shipment_date,'dd/mm/yyyy') as shipment_date, 
                to_char(a.etd, 'dd/mm/yyyy') as etd, 
                to_char(a.eta,  'dd/mm/yyyy') as eta,
                x1.name as company_name, x1.address as company_address,
                x2.description as container_type_name,
                x3.name as port_loading_name, 
                x4.name as port_discharge_name,
                x5.address as buyer_address
                "))
            ->orderby('a.id');

        if ($request->s == "form")
        {   
            if ( $request->id )
            {   $results->where("a.id", "=", $request->id); };
        };

        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };
    }
    public static function orderSave(   $head_data, $json_data, $modi_data, 
                                        $site, $user_id, $company_id    )
    {   if (count($head_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($head_data as $json) 
                {   // save order
                    if ($json->created_date == '')  
                    {   $order = new Order();
                        $order->id         = self::getNextID(self::sequence_name, $site);
                        // $order->company_id = env('APP_COMPANY');
                        $order->company_id = config('app.system_company');
                        $order->order_no   = $json->order_no;
                        $order->order_date = $json->order_date;
                        $order->created_by = $user_id;

                        $order_detail = new OrderDetail();
                    }
                    else 
                    {   // update order
                        $order = Order::where('order_no', '=', $json->order_no)->first();
                        $order->modified_by = $user_id;

                        $order_detail = OrderDetail::where('order_no', '=', $json->order_no)->first();
                        if (Empty($order_detail)) { $order_detail = new OrderDetail(); };
                    };

                    $order->repeat_order_no = $json->repeat_order_no;
                    $order->buyer_id        = $json->buyer_id;
                    $order->buyer_name      = $json->buyer_name;
                    $order->currency_id     = $json->currency_id;
                    $order->pi_no           = $json->pi_no;
                    
                    if (Empty($json->pi_date ) && Empty(trim($json->pi_no)))
                    {   $order->pi_no = null;
                        $order->pi_date = null;
                    }
                    else if ($json->pi_date && Empty(trim($json->pi_no)))
                    {   $sitedocument = new SiteDocument();
                        $order->pi_no = $sitedocument->getDocNum(config('app.system_company'), 
                                            $site, 10, 1001, $json->buyer_id, "");
                        $order->pi_date     = $json->pi_date;
                    } else { $order->pi_date= $json->pi_date; };

                    $order->bank_id         = $json->bank_id;
                    $order->bank_name       = $json->bank_name;
                    $order->bank_account    = $json->bank_account;
                    $order->bank_address    = $json->bank_address;
                    $order->swift_code      = $json->swift_code;
                    $order->container_type  = $json->container_type;
                    $order->cbm             = $json->cbm;
                    $order->port_loading    = $json->port_loading;
                    $order->port_discharge  = $json->port_discharge;
                    $order->weight_gross    = $json->weight_gross;
                    $order->weight_nett     = $json->weight_nett;
                    $order->payment_term    = $json->payment_term;
                    $order->payment_term_id = $json->payment_term_id;

                    if ( $json->shipment_date )
                    {   $order->shipment_date = $json->shipment_date.' 00:00';  } 
                    else {   $order->shipment_date = null;  };

                    if ( $json->etd )
                    {   $order->etd = $json->etd.' 00:00';  } 
                    else {   $order->etd = null;  };

                    if ( $json->eta )
                    {   $order->eta = $json->eta.' 00:00';  } 
                    else {   $order->eta = null;  };
                    
                    $result = $order->save();
                    // save order details

                    if ($result)
                    {   if (count($json_data) > 0)
                        {   $result = $order_detail->orderdetailSave( $json_data, 
                                            $site, $user_id, $company_id, $json->order_no); 
                        };
                    };
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function orderDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)
                        ->where('asset_id', '=', $request->asset_id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function OrderDetail()
    {
        return $this->hasMany('App\Models\Sales\OrderDetail', 'order_no');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
