<?php

namespace App\Models\Ppdb;

use App\Models\AppModel;
use DB;

class CalonSiswaDetail extends AppModel {
    /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------
     */
    // drop sequence calon_siswa_detail_seq;
    // CREATE SEQUENCE calon_siswa_detail_seq;
    // drop table calon_siswa_detail;
    // CREATE TABLE public.calon_siswa_detail
    // (
    //     id numeric(18) NOT NULL DEFAULT nextval('calon_siswa_detail_seq'::regclass),
    //     no_pendaftaran varchar(50) NOT NULL,
    //     nama_ayah               varchar(64),
    //     alamat_ayah             varchar(128),
    //     tempat_lahir_ayah       varchar(64),
    //     tanggal_lahir_ayah      timestamp,
    //     telp_rumah_ayah         varchar(16),
    //     telp_hp_ayah            varchar(16),
    //     hubungan_ayah           varchar(32),
    //     agama_ayah              varchar(16),
    //     pendidikan_ayah         varchar(8),
    //     pekerjaan_ayah          varchar(64),
    //     jenis_pekerjaan_ayah    varchar(64),
    //     jabatan_ayah            varchar(64),
    //     institusi_ayah          varchar(64),
    //     institusi_alamat_ayah   varchar(128),
    //     institusi_telp_ayah     varchar(64),
    //     penghasilan_ayah        varchar(64),
    //     tanggungan_ayah         numeric(2),
    //     nama_ibu               varchar(64),
    //     alamat_ibu             varchar(128),
    //     tempat_lahir_ibu       varchar(64),
    //     tanggal_lahir_ibu      timestamp,
    //     telp_rumah_ibu         varchar(16),
    //     telp_hp_ibu            varchar(16),
    //     hubungan_ibu           varchar(32),
    //     agama_ibu              varchar(16),
    //     pendidikan_ibu         varchar(8),
    //     pekerjaan_ibu          varchar(64),
    //     jenis_pekerjaan_ibu    varchar(64),
    //     jabatan_ibu            varchar(64),
    //     institusi_ibu          varchar(64),
    //     institusi_alamat_ibu   varchar(128),
    //     institusi_telp_ibu     varchar(64),
    //     penghasilan_ibu        varchar(64),
    //     pengeluaran_ibu         numeric(2),
    //     nama_wali_ayah               varchar(64),
    //     alamat_wali_ayah             varchar(128),
    //     tempat_lahir_wali_ayah       varchar(64),
    //     tanggal_lahir_wali_ayah      timestamp,
    //     telp_rumah_wali_ayah         varchar(16),
    //     telp_hp_wali_ayah            varchar(16),
    //     hubungan_wali_ayah           varchar(32),
    //     agama_wali_ayah              varchar(16),
    //     pendidikan_wali_ayah         varchar(8),
    //     pekerjaan_wali_ayah          varchar(64),
    //     jenis_pekerjaan_wali_ayah    varchar(64),
    //     jabatan_wali_ayah            varchar(64),
    //     institusi_wali_ayah          varchar(64),
    //     institusi_alamat_wali_ayah   varchar(128),
    //     institusi_telp_wali_ayah     varchar(64),
    //     penghasilan_wali_ayah        varchar(64),
    //     tanggungan_wali_ayah         numeric(2),
    //     nama_wali_ibu               varchar(64),
    //     alamat_wali_ibu             varchar(128),
    //     tempat_lahir_wali_ibu       varchar(64),
    //     tanggal_lahir_wali_ibu      timestamp,
    //     telp_rumah_wali_ibu         varchar(16),
    //     telp_hp_wali_ibu            varchar(16),
    //     hubungan_wali_ibu           varchar(32),
    //     agama_wali_ibu              varchar(16),
    //     pendidikan_wali_ibu         varchar(8),
    //     pekerjaan_wali_ibu          varchar(64),
    //     jenis_pekerjaan_wali_ibu    varchar(64),
    //     jabatan_wali_ibu            varchar(64),
    //     institusi_wali_ibu          varchar(64),
    //     institusi_alamat_wali_ibu   varchar(128),
    //     institusi_telp_wali_ibu     varchar(64),
    //     penghasilan_wali_ibu        varchar(64),
    //     pengeluaran_wali_ibu         numeric(2),
    //     expense_listrik              numeric(18),
    //     expense_pdam                 numeric(18),
    //     expense_pulsa                numeric(18),
    //     expense_monthly              numeric(18),
    //     CONSTRAINT calon_siswa_detail_pk PRIMARY KEY (no_pendaftaran),
    //     CONSTRAINT calon_siswa_detail_ux UNIQUE (id),
    //     CONSTRAINT calon_siswa1_fk foreign key (no_pendaftaran) references calon_siswa(no_pendaftaran)
    // );

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
     */
    protected $table      = 'calon_siswa_detail';
    protected $primaryKey = 'no_pendaftaran';
    // public $sequence_name = 'calon_siswa_seq';
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */
    public function parentSiswa() {
        return $this->belongsTo('App\Models\Ppdb\CalonSiswa', 'no_pendaftaran', 'no_pendaftaran');
    }

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
     */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
     */
    public static function ParentsProfile($nopen) {
        $result = DB::table('calon_siswa_detail as a')
            ->selectRaw("a.* ")
            ->where('a.no_pendaftaran', '=', $nopen)->get();

        return $result;
    }

    public static function CalonSave($nopen, $detail_1, $detail_2, $mode) {
        if ($mode == "completing") {
            // // begin transaction
            // DB::beginTransaction();
            // try
            // {
            $Detail = CalonSiswaDetail::where('no_pendaftaran', '=', $nopen)->first();
            if ($Detail) {} else {
                $Detail                 = new CalonSiswaDetail();
                $Detail->no_pendaftaran = $nopen;
            };
            // data ayah
            $Detail->nama_ayah             = strtoupper($detail_1->nama_ayah);
            $Detail->alamat_ayah           = strtoupper($detail_1->alamat_ayah);
            $Detail->tempat_lahir_ayah     = strtoupper($detail_1->tempat_lahir_ayah);
            $Detail->telp_rumah_ayah       = $detail_1->telp_rumah_ayah;
            $Detail->telp_hp_ayah          = $detail_1->telp_hp_ayah;
            $Detail->hubungan_ayah         = $detail_1->hubungan_ayah;
            $Detail->agama_ayah            = $detail_1->agama_ayah;
            $Detail->pendidikan_ayah       = $detail_1->pendidikan_ayah;
            $Detail->pekerjaan_ayah        = strtoupper($detail_1->pekerjaan_ayah);
            $Detail->jenis_pekerjaan_ayah  = $detail_1->jenis_pekerjaan_ayah;
            $Detail->jabatan_ayah          = $detail_1->jabatan_ayah;
            $Detail->institusi_ayah        = strtoupper($detail_1->institusi_ayah);
            $Detail->institusi_alamat_ayah = strtoupper($detail_1->institusi_alamat_ayah);
            $Detail->institusi_telp_ayah   = $detail_1->institusi_telp_ayah;
            $Detail->penghasilan_ayah      = $detail_1->penghasilan_ayah;
            $Detail->tanggungan_ayah       = $detail_1->tanggungan_ayah;
            if (Empty($detail_1->tanggal_lahir_ayah)) {
                $detail_1->tanggal_lahir_ayah = null;
            } else {
                $Detail->tanggal_lahir_ayah = $Detail->makeTimeStamp($detail_1->tanggal_lahir_ayah, 'd/m/Y');
            };
            // data ibu
            $Detail->nama_ibu             = strtoupper($detail_1->nama_ibu);
            $Detail->alamat_ibu           = strtoupper($detail_1->alamat_ibu);
            $Detail->tempat_lahir_ibu     = strtoupper($detail_1->tempat_lahir_ibu);
            $Detail->telp_rumah_ibu       = $detail_1->telp_rumah_ibu;
            $Detail->telp_hp_ibu          = $detail_1->telp_hp_ibu;
            $Detail->hubungan_ibu         = $detail_1->hubungan_ibu;
            $Detail->agama_ibu            = $detail_1->agama_ibu;
            $Detail->pendidikan_ibu       = $detail_1->pendidikan_ibu;
            $Detail->pekerjaan_ibu        = strtoupper($detail_1->pekerjaan_ibu);
            $Detail->jenis_pekerjaan_ibu  = $detail_1->jenis_pekerjaan_ibu;
            $Detail->jabatan_ibu          = strtoupper($detail_1->jabatan_ibu);
            $Detail->institusi_ibu        = strtoupper($detail_1->institusi_ibu);
            $Detail->institusi_alamat_ibu = strtoupper($detail_1->institusi_alamat_ibu);
            $Detail->institusi_telp_ibu   = $detail_1->institusi_telp_ibu;
            $Detail->penghasilan_ibu      = $detail_1->penghasilan_ibu;
            $Detail->pengeluaran_ibu      = preg_replace("/[^0-9]/", "", $detail_1->pengeluaran_ibu);
            if (Empty($detail_1->tanggal_lahir_ibu)) {
                $detail_1->tanggal_lahir_ibu = null;
            } else {
                $Detail->tanggal_lahir_ibu = $Detail->makeTimeStamp($detail_1->tanggal_lahir_ibu, 'd/m/Y');
            };
            // data wali ayah
            $Detail->nama_wali_ayah             = strtoupper($detail_1->nama_wali_ayah);
            $Detail->alamat_wali_ayah           = strtoupper($detail_1->alamat_wali_ayah);
            $Detail->tempat_lahir_wali_ayah     = strtoupper($detail_1->tempat_lahir_wali_ayah);
            $Detail->telp_rumah_wali_ayah       = $detail_1->telp_rumah_wali_ayah;
            $Detail->telp_hp_wali_ayah          = $detail_1->telp_hp_wali_ayah;
            $Detail->hubungan_wali_ayah         = $detail_1->hubungan_wali_ayah;
            $Detail->agama_wali_ayah            = $detail_1->agama_wali_ayah;
            $Detail->pendidikan_wali_ayah       = $detail_1->pendidikan_wali_ayah;
            $Detail->pekerjaan_wali_ayah        = strtoupper($detail_1->pekerjaan_wali_ayah);
            $Detail->jenis_pekerjaan_wali_ayah  = $detail_1->jenis_pekerjaan_wali_ayah;
            $Detail->jabatan_wali_ayah          = strtoupper($detail_1->jabatan_wali_ayah);
            $Detail->institusi_wali_ayah        = strtoupper($detail_1->institusi_wali_ayah);
            $Detail->institusi_alamat_wali_ayah = strtoupper($detail_1->institusi_alamat_wali_ayah);
            $Detail->institusi_telp_wali_ayah   = $detail_1->institusi_telp_wali_ayah;
            $Detail->penghasilan_wali_ayah      = $detail_1->penghasilan_wali_ayah;
            $Detail->tanggungan_wali_ayah       = $detail_1->tanggungan_wali_ayah;
            if (Empty($detail_1->tanggal_lahir_wali_ayah)) {
                $detail_1->tanggal_lahir_wali_ayah = null;
            } else {
                $Detail->tanggal_lahir_wali_ayah = $Detail->makeTimeStamp($detail_1->tanggal_lahir_wali_ayah, 'd/m/Y');
            };
            // data wali ibu
            // $Detail->nama_wali_ibu              = $detail_1->nama_wali_ibu;
            // $Detail->alamat_wali_ibu            = $detail_1->alamat_wali_ibu;
            // $Detail->tempat_lahir_wali_ibu      = $detail_1->tempat_lahir_wali_ibu;
            // $Detail->tanggal_lahir_wali_ibu     = $detail_1->tanggal_lahir_wali_ibu;
            // $Detail->telp_rumah_wali_ibu        = $detail_1->telp_rumah_wali_ibu;
            // $Detail->telp_hp_wali_ibu           = $detail_1->telp_hp_wali_ibu;
            // $Detail->hubungan_wali_ibu          = $detail_1->hubungan_wali_ibu;
            // $Detail->agama_wali_ibu             = $detail_1->agama_wali_ibu;
            // $Detail->pendidikan_wali_ibu        = $detail_1->pendidikan_wali_ibu;
            // $Detail->pekerjaan_wali_ibu         = $detail_1->pekerjaan_wali_ibu;
            // $Detail->jenis_pekerjaan_wali_ibu   = $detail_1->jenis_pekerjaan_wali_ibu;
            // $Detail->jabatan_wali_ibu           = $detail_1->jabatan_wali_ibu;
            // $Detail->institusi_wali_ibu         = $detail_1->institusi_wali_ibu;
            // $Detail->institusi_alamat_wali_ibu  = $detail_1->institusi_alamat_wali_ibu;
            // $Detail->institusi_telp_wali_ibu    = $detail_1->institusi_telp_wali_ibu;
            // $Detail->penghasilan_wali_ibu       = $detail_1->penghasilan_wali_ibu;
            // $Detail->pengeluaran_wali_ibu       = $detail_1->pengeluaran_wali_ibu;
            // if (Empty($detail_1->tanggal_lahir_wali_ibu)) {
            //     $detail_1->tanggal_lahir_wali_ibu = null;
            // } else {
            //     $Detail->tanggal_lahir_wali_ibu   = $Detail->makeTimeStamp($detail_1->tanggal_lahir_wali_ibu, 'd/m/Y');
            // };

            $Detail->expense_listrik = preg_replace("/[^0-9]/", "", $detail_2->expense_listrik);
            $Detail->expense_pdam    = preg_replace("/[^0-9]/", "", $detail_2->expense_pdam);
            $Detail->expense_pulsa   = preg_replace("/[^0-9]/", "", $detail_2->expense_pulsa);
            $Detail->expense_monthly = preg_replace("/[^0-9]/", "", $detail_2->expense_monthly);
            //             print_r($Detail->expense_listrik);exit;
            $result = $Detail->save();
            // commit transaction
            //     $result = [true, $no_pendaftaran];
            //     DB::commit();
            // } catch (\Illuminate\Database\QueryException $e) {
            //     // rollback transaction
            //     $result = [false, "Error Executed ---> ".$e];
            //     DB::rollback();
            // };
            return $result;
        };
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
     */

    /*
|--------------------------------------------------------------------------
| MUTATORS
|--------------------------------------------------------------------------
 */
}
