<?php

namespace App\Models\Ppdb;

use App\Models\AppModel;
use App\Models\Administrator\SiteDocument;
use DB;

class TahunAjaran extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/
    // CREATE TABLE public.m_tahun_ajaran
    // (
    //   tahunajaran_id numeric(4,0) NOT NULL,
    //   name character varying(32) NOT NULL,
    //   status numeric(1,0) NOT NULL DEFAULT 0,
    //   created_date timestamp without time zone NOT NULL DEFAULT now(),
    //   last_update timestamp without time zone NOT NULL DEFAULT now(),
    //   is_ppdb numeric(1,0) DEFAULT 0,
    //   ppdb_last_no numeric(18,0),
    //   rapbs_last_no numeric(18,0),
    //   registration_last_no numeric(18,0),
    //   CONSTRAINT m_tahun_ajaran_pk PRIMARY KEY (tahunajaran_id)
    // )
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'm_tahun_ajaran';
	protected $primaryKey = 'tahunajaran_id';
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getStatus($status)
    {   switch($status)
        {   case 0: $result = "Active";  break;
            case 1: $result = "InActive";  break;
        };
        return $result;
    }
    public static function setStatus($status)
    {   switch($status)
        {   case "Active":   $result = 0; break;
            case "InActive": $result = 1; break;
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getPeriode($request)
    {
        $results = static::select("tahunajaran_id", "name")
                    ->where("is_ppdb", "=", 1)
                    ->orderBy('tahunajaran_id', 'desc'); 
        return $results->first();
    }

    public static function getPeriodeAng($request)
    {
        $results = static::select("tahunajaran_id", "name")
                    ->where("is_anggaran", "=", 1)
                    ->orderBy('tahunajaran_id', 'asc'); 
        return $results->first();
    }

    public static function Search($request)
    {
        $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY tahun_ajaran_id)) as record_id, * ")
                    ->orderBy('tahunajaran_id', 'desc');

        if ($request->s == "form")
        {
            // if ( $request->no_pendaftaran )
            // {   $results->where("no_pendaftarana", "=", $request->no_pendaftaran); };

            if ( $request->tahun_ajaran_id )
            {   $results->where("tahun_ajaran_id", "=", 
                    $request->tahun_ajaran_id); };

            // if ($request->nis)
            // {   $results->where("nis", "ilike", "%".$request->nis."%");   };
            // if ($request->nama_lengkap)
            // {   $results->where("nama_lengkap", "ilike", 
            //         "%".$request->nama_lengkap."%");   };
            // if ($request->nama_panggilan)
            // {   $results->where("nama_panggilan", "ilike", 
            //         "%".$request->nama_panggilan."%");   };
            // if ($request->alamat_rumah)
            // {   $results->where("alamat_rumah", "ilike", 
            //         "%".$request->alamat_rumah."%");   };
            // if ($request->jenis_kelamin== "ALL"){} 
            // else
            //     {   $results->where("jenis_kelamin", "=", 
            //         $request->jenis_kelamin);   };
        };
        return $results->get();
        // if ($request->pt) {   return $results->first();   }
        // else {  return $results->get(); };
    }
    public static function SearchExt($request)
    {
        $subQuery = DB::table('Ppdb as a')
                    ->selectRaw("a.Ppdb_no,
                                (a.Ppdb_no) as display")
                    ->where('a.status', '=', 0);

        $results = Ppdb::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->Ppdbby('x.Ppdb_no', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }


    public static function headerPrintOut($request)
    {
        $results = DB::table('Ppdb as a')
            ->leftjoin('m_company as x1', 'x1.company_id', '=', 'a.company_id')
            ->leftjoin('m_container_type as x2', 'x2.container_type_id', '=', 'a.container_type')
            ->leftjoin('m_city as x3', 'x3.city_id', '=', 'a.port_loading')
            ->leftjoin('m_city as x4', 'x4.city_id', '=', 'a.port_discharge')
            ->leftjoin('m_company as x5', 'x5.company_id', '=', 'a.buyer_id')
            ->select(DB::raw("
                trim(a.id) as id, a.Ppdb_no, a.company_id, a.currency_id, a.description, 
                a.buyer_id, a.buyer_name, 
                to_char(a.Ppdb_date, 'dd/mm/yyyy') as Ppdb_date, 
                a.pi_no, to_char(a.pi_date, 'dd/mm/yyyy') as pi_date, 
                a.bank_id, a.bank_name, a.bank_account, a.bank_address, a.swift_code, 
                a.container_type, a.payment_term, a.payment_term_id,
                a.port_loading, a.port_discharge, 
                coalesce(a.cbm,0) as cbm, 
                coalesce(a.weight_gross, 0) as weight_gross, 
                coalesce(a.weight_nett, 0) as weight_nett,
                to_char(a.shipment_date,'dd/mm/yyyy') as shipment_date, 
                to_char(a.etd, 'dd/mm/yyyy') as etd, 
                to_char(a.eta,  'dd/mm/yyyy') as eta,
                x1.name as company_name, x1.address as company_address,
                x2.description as container_type_name,
                x3.name as port_loading_name, 
                x4.name as port_discharge_name,
                x5.address as buyer_address
                "))
            ->Ppdbby('a.id');

        if ($request->s == "form")
        {   
            if ( $request->id )
            {   $results->where("a.id", "=", $request->id); };
        };

        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };
    }
    public static function Ppdbsave(   $head_data, $json_data, $modi_data, 
                                        $site, $user_id, $company_id    )
    {   if (count($head_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($head_data as $json) 
                {   // save Ppdb
                    if ($json->created_date == '')  
                    {   $Ppdb = new Ppdb();
                        $Ppdb->id         = self::getNextID(self::sequence_name, $site);
                        // $Ppdb->company_id = env('APP_COMPANY');
                        $Ppdb->company_id = config('app.system_company');
                        $Ppdb->Ppdb_no   = $json->Ppdb_no;
                        $Ppdb->Ppdb_date = $json->Ppdb_date;
                        $Ppdb->created_by = $user_id;

                        $Ppdb_detail = new PpdbDetail();
                    }
                    else 
                    {   // update Ppdb
                        $Ppdb = Ppdb::where('Ppdb_no', '=', $json->Ppdb_no)->first();
                        $Ppdb->modified_by = $user_id;

                        $Ppdb_detail = PpdbDetail::where('Ppdb_no', '=', $json->Ppdb_no)->first();
                        if (Empty($Ppdb_detail)) { $Ppdb_detail = new PpdbDetail(); };
                    };

                    $Ppdb->repeat_Ppdb_no = $json->repeat_Ppdb_no;
                    $Ppdb->buyer_id        = $json->buyer_id;
                    $Ppdb->buyer_name      = $json->buyer_name;
                    $Ppdb->currency_id     = $json->currency_id;
                    $Ppdb->pi_no           = $json->pi_no;
                    
                    if (Empty($json->pi_date ) && Empty(trim($json->pi_no)))
                    {   $Ppdb->pi_no = null;
                        $Ppdb->pi_date = null;
                    }
                    else if ($json->pi_date && Empty(trim($json->pi_no)))
                    {   $sitedocument = new SiteDocument();
                        $Ppdb->pi_no = $sitedocument->getDocNum(config('app.system_company'), 
                                            $site, 10, 1001, $json->buyer_id, "");
                        $Ppdb->pi_date     = $json->pi_date;
                    } else { $Ppdb->pi_date= $json->pi_date; };

                    $Ppdb->bank_id         = $json->bank_id;
                    $Ppdb->bank_name       = $json->bank_name;
                    $Ppdb->bank_account    = $json->bank_account;
                    $Ppdb->bank_address    = $json->bank_address;
                    $Ppdb->swift_code      = $json->swift_code;
                    $Ppdb->container_type  = $json->container_type;
                    $Ppdb->cbm             = $json->cbm;
                    $Ppdb->port_loading    = $json->port_loading;
                    $Ppdb->port_discharge  = $json->port_discharge;
                    $Ppdb->weight_gross    = $json->weight_gross;
                    $Ppdb->weight_nett     = $json->weight_nett;
                    $Ppdb->payment_term    = $json->payment_term;
                    $Ppdb->payment_term_id = $json->payment_term_id;

                    if ( $json->shipment_date )
                    {   $Ppdb->shipment_date = $json->shipment_date.' 00:00';  } 
                    else {   $Ppdb->shipment_date = null;  };

                    if ( $json->etd )
                    {   $Ppdb->etd = $json->etd.' 00:00';  } 
                    else {   $Ppdb->etd = null;  };

                    if ( $json->eta )
                    {   $Ppdb->eta = $json->eta.' 00:00';  } 
                    else {   $Ppdb->eta = null;  };
                    
                    $result = $Ppdb->save();
                    // save Ppdb details

                    if ($result)
                    {   if (count($json_data) > 0)
                        {   $result = $Ppdb_detail->PpdbdetailSave( $json_data, 
                                            $site, $user_id, $company_id, $json->Ppdb_no); 
                        };
                    };
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function Profile($nis)
    {   
        $result = static::where('nis', '=', $nis)->first();
        return $result;
    }
    public static function PpdbDelete($request)
    {   
        DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)
                        ->where('asset_id', '=', $request->asset_id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function RegisterUser($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('keyid', '=', $request->id)->first();
            if ($result)
            {   // register to laravel user
                $user           = new User();
                $user->password = Hash::make(trim($result->tanggal_lahir));
                $user->email    = trim($result->nis).'@alfalah.com';
                $user->name     = trim($result->nis);
                $user->save();

                if ($user)
                {   // register to Master user
                    $muser = new MUser();
                    $muser->user_id     = $user->id;
                    $muser->password    = "default";
                    $muser->username    = $user->name;
                    $muser->name        = $result->nama_lengkap;
                    $muser->auth_email  = $user->email;
                    $muser->status      = 0;
                    $muser->laravel_id  = $user->id;
                    
                    $muser->save();

                    if ($muser)
                    {   // register to Role user
                        $roleuser = new RoleUser();
                        $roleuser->user_id     = $user->id;
                        $roleuser->role_id     = 100;
                        $roleuser->save();
                        if ($roleuser)
                        {
                            $result = [true, "Save succcefully"];
                            DB::commit();
                        }
                        else
                        {   $result = [false, "Failed to Save on Role User"];
                            DB::rollback();
                        };
                    }
                    else
                    {   $result = [false, "Failed to Save on Master User"];
                        DB::rollback();
                    };
                }
                else
                {   $result = [false, "Failed to Save on Lvl User"];
                    DB::rollback();
                };
            }
            else
            {   $result = [false, "No Data Found"];
                DB::rollback();
            };          
            
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function ResetPassword($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('keyid', '=', $request->id)->first();
            if ($result)
            {   // register to laravel user
                $user = User::where('name', '=', $result->nis)->first();
                if ($user)
                {   
                    $user->password = Hash::make(trim($result->tanggal_lahir));
                    $user->save();
                    if ($user)
                    {
                        $result = [true, "Save succcefully"];
                        DB::commit();
                    }
                    else
                    {   $result = [false, "Failed to Save on Role User"];
                        DB::rollback();
                    };
                }
                else
                {   $result = [false, "User not found "];
                    DB::rollback();
                };
            }
            else
            {   $result = [false, "Ppdb not found "];
                DB::rollback();
            };          
            
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
