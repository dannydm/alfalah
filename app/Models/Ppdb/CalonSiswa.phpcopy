<?php

namespace App\Models\Ppdb;

use App\Models\AppModel;
use App\Models\Ppdb\CalonSiswaDetail;
use App\Models\Ppdb\TahunAjaran;
use DB;

class CalonSiswa extends AppModel {
    /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------
     */
    // drop sequence calon_siswa_seq;
    // CREATE SEQUENCE calon_siswa_seq;
    // drop table calon_siswa;
    // CREATE TABLE public.calon_siswa
    // (
    //     id numeric(18) NOT NULL DEFAULT nextval('calon_siswa_seq'::regclass),
    //     no_pendaftaran varchar(50) NOT NULL,
    //     no_registration varchar(50),
    //     tahunajaran_id numeric(4,0) NOT NULL,
    //     jenjang_sekolah_id numeric NOT NULL,
    //     nama_lengkap varchar(150),
    //     nama_panggilan varchar(50),
    //     gender char(1) NOT NULL default 'L',
    //     tempat_lahir varchar(50),
    //     tanggal_lahir timestamp,
    //     ibu_kandung varchar(150),
    //     alamat_rumah varchar(500),
    //     kecamatan varchar(32),
    //     kota varchar(32),
    //     telp_rumah varchar(128),
    //     warganegara varchar(32),
    //     agama varchar(16),
    //     keadaan_siswa varchar(16),
    //     tinggaldengan varchar(16),
    //     tinggi_badan numeric(3),
    //     berat_badan numeric(3),
    //     bloodtype char(2),
    //     penyakit_bawaan varchar(128),
    //     created_date timestamp without time zone DEFAULT now(),
    //     created_by numeric(18),
    //     modified_date timestamp without time zone DEFAULT now(),
    //     modified_by numeric(18),
    //     payment_by varchar(64),
    //     payment_amount numeric(19,2),
    //     payment_date timestamp,
    //     payment_confirm_by numeric(18),
    //     payment_confirm_date timestamp,
    //     CONSTRAINT calon_siswa_pk PRIMARY KEY (no_pendaftaran),
    //     CONSTRAINT calon_siswa_ux UNIQUE (nama_lengkap, tanggal_lahir, ibu_kandung),
    //     CONSTRAINT calon_siswa_gender_ckc CHECK (gender in ('L','P') ),
    //     CONSTRAINT m_dept2_fk foreign key (jenjang_sekolah_id) references m_department(dept_id),
    //     CONSTRAINT m_tahunajaran1_fk foreign key (tahunajaran_id) references m_tahun_ajaran(tahunajaran_id)
    // );
    //
    //
    // CONSTRAINT calon_siswa_bloodtype_ckc CHECK (gender in ('O', 'A','B', 'AB', 'X'))
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
     */
    protected $table      = 'calon_siswa';
    protected $primaryKey = 'no_pendaftaran';
    // public $sequence_name = 'calon_siswa_seq';
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */
    public function SiswaDetail() {
        return $this->hasMany('App\Models\Ppdb\CalonSiswaDetail', 'no_pendaftaran');}

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
     */
    public static function getStatus($status) {
        switch ($status) {
            case 0:
                $result = "Active";
                break;
            case 1:
                $result = "InActive";
                break;
        };
        return $result;
    }

    public static function setStatus($status) {
        switch ($status) {
            case "Active":
                $result = 0;
                break;
            case "InActive":
                $result = 1;
                break;
        };
        return $result;
    }

    public static function getGender($status) {
        switch ($status) {
            case 'ALL':
                $result = "ALL";
                break;
            case 'L':
                $result = "Laki-Laki";
                break;
            case 'P':
                $result = "Perempuan";
                break;
        };
        return $result;
    }

    public static function setGender($gender) {
        switch ($gender) {
            case 'ALL':
                $result = "ALL";
                break;

            case "Laki-Laki":
                $result = 'L';
                break;
            case "Perempuan":
                $result = 'P';
                break;
        };
        return $result;
    }

    public static function getJenjang($jenjang) {
        switch ($status) {
            case 0:
                $result = "ALL";
                break;
            case 1:
                $result = "SD";
                break;
            case 2:
                $result = "SMP";
                break;
            // case 3: $result = "";  break;
            case 5:
                $result = "SD ICP";
                break;
            case 6:
                $result = "KB";
                break; // PRA SEKOLAH
            case 7:
                $result = "TK";
                break;
        };
        return $result;
    }

    public static function setJenjang($jenjang) {
        switch ($jenjang) {
            case "ALL":
                $result = 0;
                break;
            case "SD":
                $result = 1;
                break;
            case "SMP":
                $result = 2;
                break;
            // case 3: $result = "";  break;
            case "SD ICP":
                $result = 5;
                break;
            case "KB":
                $result = 6;
                break; // PRA SEKOLAH
            case "TK":
                $result = 7;
                break;
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
     */
    public static function Search($request) {
        $results = DB::table('calon_siswa as a')
            ->join('m_department as b', 'b.dept_id', '=', 'a.jenjang_sekolah_id')
            ->leftjoin('m_users as c', 'c.user_id', '=', 'a.payment_confirm_by')
            ->leftjoin('calon_siswa_detail as d', 'd.no_pendaftaran', 'a.no_pendaftaran')
            ->orderBy('a.no_pendaftaran', 'desc');

        switch ($request->t) {
            case "daftar":
                {
                    $results->selectRaw("a.*, d.*, 
                        b.name as jenjang_sekolah_name, a.no_pendaftaran,                    

                        ( case when a.gender = 'L'
                            then 'Laki-Laki'
                            else 'Perempuan'
                          end
                        ) as gender_name,
                        c.name as payment_confirm_by_name
                        ")
                        ->whereNotNull("a.no_pendaftaran");
                
                };
                break;
            case "registration":
                {
                    $results->selectRaw("a.*,d.*,
                        b.name as jenjang_sekolah_name,
                        a.observation_result,
                        to_char(a.observation_date,'DD/MM/YYYY') as observation_date,
                        to_char(a.commitment_date,'DD/MM/YYYY') as commitment_date")
                        ->whereNotNull("a.no_registration");
                };
                break;
            case "observation":
                {
                    $results
                    ->whereNotNull("a.schedule_observation_date");
                };
                break;
        };

        if ($request->s == "form") {
            if ($request->no_pendaftaran) {
                $results->where("a.no_pendaftaran", "=", $request->no_pendaftaran);};
            if ($request->jenjang_sekolah_id == "ALL") {} else {
                $results->where("a.jenjang_sekolah_id", "=", $request->jenjang_sekolah_id);};
            if ($request->nama_lengkap) {
                $results->where("a.nama_lengkap", "ilike", "%" . $request->nama_lengkap . "%");};
            if ($request->nama_panggilan) {
                $results->where("a.nama_panggilan", "ilike", "%" . $request->nama_panggilan . "%");};
            if ($request->alamat_rumah) {
                $results->where("a.alamat_rumah", "ilike", "%" . $request->alamat_rumah . "%");};
            if ($request->gender == "ALL") {} else {
                $results->where("gender", "=", self::setGender($request->gender));};
            if (Empty($request->jenis_bayar)) {} else {
                if ($request->jenis_bayar == "ALL") {} else {
                    $results->where("a.jenis_bayar", "=", $request->jenis_bayar);};
            };

            if (Empty($request->sekolah_asal_internal)){}
            else { 
                if ($request->sekolah_asal_internal == "ALL") {} 
                else {
                    $results->where("a.sekolah_asal_internal", "=", $request->sekolah_asal_internal);
                };
            };

            // NULL Parameters
            if ($request->null_no_registration) {
                $results->whereNull("a.no_registration");
            } else if ($request->no_registration) {
                $results->where("a.no_registration", "=", $request->no_registration);
            };
            if ($request->null_observation_date) {
                $results->whereNull("a.observation_date");
            } else if ($request->observation_date) {
                $results->where("a.observation_date", "=", $request->observation_date);
            };
            if ($request->null_observation_result) {
                $results->whereNull("a.observation_result");
            } else if ($request->observation_result) {
                $results->where("a.observation_result", "=", $request->observation_result);
            };
        };

        return $results->get();

        // if ($request->pt) {   return $results->first();   }
        // else {  return $results->get(); };
    }

    public static function SearchExt($request) {
        $subQuery = DB::table('Siswa as a')
            ->selectRaw("a.Siswa_no,
                                (a.Siswa_no) as display")
            ->where('a.status', '=', 0);

        $results = Siswa::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->Siswaby('x.Siswa_no', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {$results->where("x.display", "ilike", "%" . $request['query'] . "%");};
        };
        return $results->get();
    }

    public static function headerPrintOut($request) {
        $results = DB::table('Siswa as a')
            ->leftjoin('m_company as x1', 'x1.company_id', '=', 'a.company_id')
            ->leftjoin('m_container_type as x2', 'x2.container_type_id', '=', 'a.container_type')
            ->leftjoin('m_city as x3', 'x3.city_id', '=', 'a.port_loading')
            ->leftjoin('m_city as x4', 'x4.city_id', '=', 'a.port_discharge')
            ->leftjoin('m_company as x5', 'x5.company_id', '=', 'a.buyer_id')
            ->select(DB::raw("
                trim(a.id) as id, a.Siswa_no, a.company_id, a.currency_id, a.description,
                a.buyer_id, a.buyer_name,
                to_char(a.Siswa_date, 'dd/mm/yyyy') as Siswa_date,
                a.pi_no, to_char(a.pi_date, 'dd/mm/yyyy') as pi_date,
                a.bank_id, a.bank_name, a.bank_account, a.bank_address, a.swift_code,
                a.container_type, a.payment_term, a.payment_term_id,
                a.port_loading, a.port_discharge,
                coalesce(a.cbm,0) as cbm,
                coalesce(a.weight_gross, 0) as weight_gross,
                coalesce(a.weight_nett, 0) as weight_nett,
                to_char(a.shipment_date,'dd/mm/yyyy') as shipment_date,
                to_char(a.etd, 'dd/mm/yyyy') as etd,
                to_char(a.eta,  'dd/mm/yyyy') as eta,
                x1.name as company_name, x1.address as company_address,
                x2.description as container_type_name,
                x3.name as port_loading_name,
                x4.name as port_discharge_name,
                x5.address as buyer_address
                "))
            ->Siswaby('a.id');

        if ($request->s == "form") {
            if ($request->id) {$results->where("a.id", "=", $request->id);};
        };

        if ($request->pt) {return $results->first();} else {return $results->get();};
    }

    public static function CalonSave($head_data, $detail1_data, $detail2_data, 
        $site, $user_id, $company_id, $mode) {
        $json = $head_data;
        // begin transaction
        DB::beginTransaction();
        try
        {
            $TahunAjaran = TahunAjaran::where('tahunajaran_id', '=', $json->tahunajaran_id)->first();

            if ($json->created_date == '') {
                $last_no                   = $TahunAjaran->ppdb_last_no + 1;
                $TahunAjaran->ppdb_last_no = $last_no;

                $no_pendaftaran = $TahunAjaran->tahunajaran_id
                . '/' . $json->jenjang_sekolah_id
                    . '/0/' . $last_no;

                $Calon                 = new CalonSiswa();
                $Calon->no_pendaftaran = $no_pendaftaran;
                $Calon->tahunajaran_id = $json->tahunajaran_id;
                $Calon->created_by     = $user_id;
                $Calon->bloodtype      = "AB";
            } else {
                // update Calon
                $Calon              = CalonSiswa::where('no_pendaftaran', '=', $json->no_pendaftaran)->first();
                $Calon->modified_by = $user_id;
                $no_pendaftaran     = $Calon->no_pendaftaran;
            };

            $Calon->jenjang_sekolah_id = $json->jenjang_sekolah_id;
            $Calon->nama_lengkap       = strtoupper($json->nama_lengkap);
            $Calon->nama_panggilan     = strtoupper($json->nama_panggilan);
            $Calon->tempat_lahir       = strtoupper($json->tempat_lahir);
            $Calon->tanggal_lahir      = $Calon->makeTimeStamp($json->tanggal_lahir, 'd/m/Y');
            $Calon->alamat_rumah       = strtoupper($json->alamat_rumah);
            $Calon->kecamatan          = strtoupper($json->kecamatan);
            $Calon->kota               = strtoupper($json->kota);
            $Calon->telp_rumah         = $json->telp_rumah;
            $Calon->warganegara        = $json->warganegara;
            $Calon->agama              = $json->agama;
            $Calon->ibu_kandung        = strtoupper($json->ibu_kandung);

            if ($mode == "completing") {
                $Calon->gender        = self::setGender($json->gender_name);
                $Calon->keadaan_siswa = $json->keadaan_siswa;
                $Calon->tinggaldengan = $json->tinggaldengan;
                $Calon->anakke        = $json->anakke;
                $Calon->brpsaudara    = $json->brpsaudara;

                $Calon->tinggi_badan    = $json->tinggi_badan;
                $Calon->berat_badan     = $json->berat_badan;
                $Calon->bloodtype       = $json->bloodtype;
                $Calon->penyakit_bawaan = strtoupper($json->penyakit_bawaan);

                $Calon->sekolah_asal_nama      = strtoupper($json->sekolah_asal_nama);
                $Calon->sekolah_asal_status    = $json->sekolah_asal_status;
                $Calon->sekolah_asal_internal  = $json->sekolah_asal_internal;
                $Calon->sekolah_asal_alamat    = strtoupper($json->sekolah_asal_alamat);
                $Calon->sekolah_asal_lulus     = $json->sekolah_asal_lulus;
                $Calon->sekolah_asal_akreditas = strtoupper($json->sekolah_asal_akreditas);
                $Calon->sekolah_asal_nilai_un  = $json->sekolah_asal_nilai_un;
                //tambahan baru, kelas pindahan dan saudara
                $Calon->kelas_pindahan = $json->kelas_pindahan;
                $Calon->saudara_nis    = $json->saudara_nis;
                $Calon->saudara_kelas  = $json->saudara_kelas;
                $Calon->saudara_nama   = strtoupper($json->saudara_nama);

                 //tambahan baru, upload 
                // dd($detail3_data);
                //  $Calon->kk_file       = $detail3_data->kk_file;
                //  $Calon->akte_file     = $detail3_data->akte_file;
                //  $Calon->foto_file     = $detail3_data->foto_file;
                //  $Calon->bukti_bayar   = $detail3_data->bukti_bayar;

                $result = CalonSiswaDetail::CalonSave($Calon->no_pendaftaran, $detail1_data, $detail2_data, $mode);
            };

            if ($TahunAjaran->save()) {$result = $Calon->save();};
            // commit transaction
            $result = [true, $no_pendaftaran];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }
    
    public static function FileSave($no_registration, $filename_1, $filename_2, $filename_3, $filename_4, $user_id) {
        // begin transaction
        $result = true;
        // DB::beginTransaction();
        try
        {
            $Calon = CalonSiswa::where('no_registration', '=', $no_registration)->first();
            if ($Calon) {
                $File = CalonSiswaFile::where('no_pendaftaran', '=', $Calon->no_pendaftaran)->first();
                if ($File) {
                    $result = $File->deleteFile($Calon->no_pendaftaran);
                };

                if ($filename_1) {
                    $the_file_1 = pg_escape_bytea(file_get_contents(storage_path("app/ppdb_upload") . "/" . $filename_1));
                    // $the_file_1 = pg_escape_bytea(utf8_encode(file_get_contents('/var/www/html/alfalah/storage/app/ppdb_upload/2620201013_ktpdogs.jpeg')));
                    if ($the_file_1) {
                        $result = CalonSiswaFile::FileSave(
                            'ktp',
                            $Calon->no_pendaftaran,
                            $the_file_1,
                            $filename_1,
                            storage_path("app/ppdb_upload") . "/" . $filename_1,
                            $user_id);
                    };
                };
                if ($filename_2) {
                    $the_file_2 = pg_escape_bytea(file_get_contents(storage_path("app/ppdb_upload") . "/" . $filename_2));
                    if ($the_file_2) {
                        $result = CalonSiswaFile::FileSave(
                            'kk',
                            $Calon->no_pendaftaran,
                            $the_file_2,
                            $filename_2,
                            storage_path("app/ppdb_upload") . "/" . $filename_2,
                            $user_id);
                    };
                };
                if ($filename_3) {
                    $the_file_3 = pg_escape_bytea(utf8_encode(file_get_contents(storage_path("app/ppdb_upload") . "/" . $filename_3)));
                    if ($the_file_3) {
                        $result = CalonSiswaFile::FileSave(
                            'siswa',
                            $Calon->no_pendaftaran,
                            $the_file_3,
                            $filename_3,
                            storage_path("app/ppdb_upload") . "/" . $filename_3,
                            $user_id);
                    };
                };
                if ($filename_4) {
                    $the_file_4 = pg_escape_bytea(utf8_encode(file_get_contents(storage_path("app/ppdb_upload") . "/" . $filename_4)));
                    if ($the_file_4) {
                        $result = CalonSiswaFile::FileSave(
                            'bukti',
                            $Calon->no_pendaftaran,
                            $the_file_4,
                            $filename_4,
                            storage_path("app/ppdb_upload") . "/" . $filename_4,
                            $user_id);
                    };
                };
                // print_r(DB::getQueryLog());
                // commit transaction

                // DB::commit();
            };
            // commit transaction
            $result = [true, "Save succcefully"];
            // DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            // DB::rollback();
        };
        return $result;
    }

    public static function RegistrationSave($head_data, $json_data, $modi_data,
        $site, $user_id, $company_id) {
        $json = $head_data;
        // begin transaction
        DB::beginTransaction();
        try
        {
            $no_pendaftaran = $json->no_pendaftaran;
            $Calon          = CalonSiswa::where('no_pendaftaran', '=', $no_pendaftaran)->first();
            $TahunAjaran    = TahunAjaran::where('tahunajaran_id', '=', $json->tahunajaran_id)->first();

            if ($json->no_registration == '') {
                // generate no registrasi
                $last_no                           = $TahunAjaran->registration_last_no + 1;
                $TahunAjaran->registration_last_no = $last_no;

                $no_registration = $TahunAjaran->tahunajaran_id
                . '/' . $json->jenjang_sekolah_id
                    . '/1/' . $last_no;

                $Calon->no_registration = $no_registration;
            } else {
                $no_registration = $Calon->no_registration;
            };

            $Calon->payment_date         = $Calon->makeTimeStamp($json->payment_date, 'd/m/Y');
            $Calon->payment_amount       = $json->payment_amount;
            $Calon->payment_by           = $json->payment_by;
            $Calon->jenis_bayar          = $json->jenis_bayar;
            $Calon->payment_confirm_date = date('Ymd');
            $Calon->payment_confirm_by   = $user_id;
            $Calon->observation_date     = $Calon->makeTimeStamp($json->observation_date, 'd/m/Y');
            $Calon->commitment_date      = $Calon->makeTimeStamp($json->commitment_date, 'd/m/Y');

            $nama_lengkap = $Calon->nama_lengkap;

            if ($TahunAjaran->save()) {$result = $Calon->save();};
            // commit transaction
            $result = [true, $no_registration, $nama_lengkap, $no_pendaftaran];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function ObservationSave($json_data, $site, $user_id, $company_id) {
        $json = $json_data;
        // begin transaction
        DB::beginTransaction();
        try
        {
            foreach ($json_data as $json) {
                if ($json->no_registration == '') {} else {
                    $Calon = CalonSiswa::where('no_registration', '=', $json->no_registration)
                        ->first();
                    $Calon->observation_date   = $Calon->makeTimeStamp($json->observation_date, 'd/m/Y');
                    $Calon->observation_result = $json->observation_result;
                    $Calon->commitment_date    = $Calon->makeTimeStamp($json->commitment_date, 'd/m/Y');
                }
                ;

                $result = $Calon->save();
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            }
            ;
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function Login($head_data, $json_data, $modi_data,
        $site, $user_id, $company_id) {
        $json = $head_data;
        // begin transaction
        DB::beginTransaction();
        try
        {
            $Calon = CalonSiswa::where('no_registration', '=', $json->no_registration)
                ->where('ibu_kandung', '=', strtoupper($json->ibu_kandung))
            //->where('tanggal_lahir','=',$json->tanggal_lahir)
                ->whereRaw("to_char(tanggal_lahir, 'dd/mm/yyyy') = ? ", $json->tanggal_lahir)
            //->whereRaw("tanggal_lahir = to_date($json->tanggal_lahir,'dd/mm/yyyy')")
                ->first();

            if ($Calon) {$result = [true, $Calon->no_pendaftaran];} else { $result = [false, "Siswa belum registrasi, Mohon hubungi petugas administrasi"];};

            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function StudentProfile($nopen) {
        $result = DB::table('calon_siswa as a')
            ->selectRaw("a.*,
                        b.name as jenjang_sekolah_name,
                        ( case when a.gender = 'L'
                            then 'Laki-Laki'
                            else 'Perempuan'
                          end
                        ) as gender_name,
                        c.name as payment_confirm_by_name
                        ")
            ->join('m_department as b', 'b.dept_id', '=', 'a.jenjang_sekolah_id')
            ->leftjoin('m_users as c', 'c.user_id', '=', 'a.payment_confirm_by')
            ->where('a.no_pendaftaran', '=', $nopen)->get();
        return $result;
    }

    public static function ParentsProfile($nopen) {
        $result = CalonSiswaDetail::ParentsProfile($nopen);
        return $result;
    }

    public static function SiswaDelete($request) {
        DB::beginTransaction();
        try
        {
            $result = static::where('id', '=', $request->id)
                ->where('asset_id', '=', $request->asset_id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
     */

    /*
|--------------------------------------------------------------------------
| MUTATORS
|--------------------------------------------------------------------------
 */
}
