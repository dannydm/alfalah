<?php

namespace App\Models\Ppdb;

use App\Models\AppModel;
use App\Models\Ppdb\CalonSiswa;
use DB;

class CalonSiswaFile extends AppModel {
    /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------
     */
    // CREATE SEQUENCE calon_siswa_file_seq;
    // drop table calon_siswa_file;
    // CREATE TABLE calon_siswa_file
    // (
    //     id numeric NOT NULL DEFAULT nextval('calon_siswa_file_seq'::regclass),
    //     no_pendaftaran varchar(50),
    //     type varchar(10),
    //     file_name  varchar(128),
    //     file_path  varchar(128),
    //     file_bytea bytea,
    //     modified_date timestamp default now(),
    //     modified_by numeric,
    //     created_date timestamp default now(),
    //     created_by numeric,
    //     CONSTRAINT calon_siswa_file_pk PRIMARY KEY (no_pendaftaran, type),
    //     CONSTRAINT calon_siswa_file_ux UNIQUE (id),
    //     CONSTRAINT calon_siswa1_fk FOREIGN KEY (no_pendaftaran) REFERENCES calon_siswa(no_pendaftaran)
    // );

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
     */
    protected $table      = 'calon_siswa_file';
    protected $primaryKey = 'id';
    // public $sequence_name = 'calon_siswa_seq';
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */
    public function siswa() {
        return $this->belongsTo(CalonSiswa::class);
    }

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
     */
    public static function Search($no_pendaftaran) {
        // $results = static::where('no_pendaftaran', '=', $no_pendaftaran)->get();
        $results = DB::table('calon_siswa_file')
            ->selectRaw('id, type, no_pendaftaran, file_name, file_path')
            ->where('no_pendaftaran', '=', $no_pendaftaran)
            ->orderby('id')
            ->get();
        return $results;
    }

    public static function FileSave($type, $no_pendaftaran, $the_file, $the_file_name, $the_file_path, $user_id) {

        $File                 = new CalonSiswaFile();
        $File->type           = $type;
        $File->no_pendaftaran = $no_pendaftaran;
        $File->file_name      = $the_file_name;
        $File->file_path      = $the_file_path;
        $File->file_bytea     = $the_file;
        $result               = $File->save();
        return $result;
    }

    public static function deleteFile($no_pendaftaran) {
        // $result = static::where('no_pendaftaran', '=', $no_pendaftaran)->delete();
        $results = static::where('no_pendaftaran', '=', $no_pendaftaran)->get();
        if ($results) {
            foreach ($results as $result) {
                if (is_file($result->file_name)) {
                    // 1. possibility
                    // Storage::delete($result->file_name);
                    // 2. possibility
                    unlink($result->file_name);
                }
            };
            $result = static::where('no_pendaftaran', '=', $no_pendaftaran)->delete();
        };
        $result = [true, "Save succcefully"];
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
     */

    /*
|--------------------------------------------------------------------------
| MUTATORS
|--------------------------------------------------------------------------
 */
};
