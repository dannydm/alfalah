<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $connection = 'mysql';
    protected $table = 'm_pgev';
	protected $primaryKey = 'pgev_id';
    public $timestamps = false;    // disable auto generate created value
    public $incrementing = false;  // disable auto generate primary key value
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function SearchPage($request)
    {   
        $results = static::orderBy('pg', 'asc')->orderBy('ev', 'asc');
        // searchForm parameter
        if ($request->s == "form")
        {   if ($request->page_name)
            {   $results->where("pg", "like", "%".$request->page_name."%");   };
            if ($request->event_name)
            {   $results->where("ev", "like", "%".$request->event_name."%");   };
        };
        
        return $results->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
