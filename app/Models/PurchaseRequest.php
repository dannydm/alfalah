<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class PurchaseRequest extends Model
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'purchase_requesition_master';
	protected $primaryKey = 'pr_no';
    public $timestamps = false;    // disable auto generate created value
    public $incrementing = false;  // disable auto generate primary key value
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function myPRSearch($request, $user_id)
    {   
        // $subQuery = DB::table('material_tracking as a')
        //             ->selectRaw("a.pr_no, a.po_no, a.grin_no, a.item_id, a.item_name,
        //                         to_char(pr_approved_date, 'dd/mm/yyyy') as pr_approved_date")
        //             ->where('a.pr_created_by', '=', $user_id)
        //             ->orderby('a.pr_no', 'desc')
        //             ->limit(10);

        // $results = AssetITDHardware::selectRaw("x.*, y.description")
        //             ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
        //             ->leftjoin('purchase_requisition_master as y', 'x.pr_no', '=', 'y.pr_no')
        //             ->mergeBindings($subQuery)
        //             ->orderby('x.pr_no', 'desc');


        $results = DB::table('purchase_requisition_master as a')
            ->join('purchase_requisition_detail as b', 'a.pr_no', '=', 'b.pr_no')
            ->leftjoin('purchase_order_detail as c', function($join)
                {   $join->on('b.pr_no', '=', 'c.pr_no')
                         ->on('b.item_id', '=', 'c.item_id');
                })
            ->leftjoin('grin_master as d', 'd.ref_no', '=', 'c.po_no')
            ->leftjoin('grin_detail as e', function($join)
                {   $join->on('d.grin_no', '=', 'e.grin_no')
                         ->on('c.item_id', '=', 'e.item_id');
                })
            ->leftjoin('m_item as f', 'e.item_id', '=', 'f.item_id')
            ->select('a.pr_no', 'c.po_no', 'd.grin_no', 'a.description', 
                    'e.item_id', 'f.name as item_name')
            ->where('a.created_by', '=', '77')
            ->orderby('a.pr_no', 'desc')
            ->limit(10);

        return $results->get();
    }
    // public static function registerSearch($request)
    // {   $arr = array();
    //     $my_arr = array();
    //     $results = DB::table('asset_reg_master as a')
    //             ->leftjoin('asset_reg_detail as b', 'a.doc_no', '=', 'b.doc_no')
    //             ->leftjoin('m_item as x1', 'b.ref_item_id', '=', 'x1.item_id')
    //             ->leftjoin('grin_detail as x2', function($join)
    //                 {   $join->on('a.ref_no', '=', 'x2.grin_no')
    //                          ->on('b.ref_item_id', '=', 'x2.item_id');
    //                 })
    //             ->leftjoin('purchase_order_detail as x3', function($join)
    //                 {   $join->on('x2.stock_ref_no', '=', 'x3.po_no')
    //                          ->on('x2.item_id', '=', 'x3.item_id');
    //                 })
    //             ->leftjoin('asset_itd_hardware as x4', 
    //                 'b.asset_id', '=', 'x4.asset_id')
    //             ->leftjoin('asset_itd_hardware_history as x5', 
    //                 'b.asset_id', '=', 'x5.asset_id')
    //             ->leftjoin('asset_itd_license as x6', 
    //                 'b.asset_id', '=', 'x6.os_asset_id')
    //             ->leftjoin('asset_itd_license as x7', 
    //                 'b.asset_id', '=', 'x7.cal_os_asset_id')
    //             ->leftjoin('asset_itd_license as x8', 
    //                 'b.asset_id', '=', 'x8.office_asset_id')
    //             ->select(DB::raw("
    //                     a.doc_no, a.ref_no, a.doc_date, a.remark, 
    //                     a.ref_vendor_id, a.created_date, a.modified_date, 
    //                     b.*, 
    //                     x1.name as item_name, x2.stock_grin_no, x2.stock_ref_no as po_no,
    //                     case
    //                         when x4.name is not null then x4.name
    //                         when x6.os_asset_id is not null then x6.name
    //                         when x7.cal_os_asset_id is not null then x7.name
    //                         when x8.office_asset_id is not null then x8.name
    //                         else x5.name
    //                     end as deviceid,
    //                     array_to_string(array(
    //                             select pr_no
    //                             from purchase_order_detail
    //                             where po_no = x2.stock_ref_no
    //                             group by pr_no),', '
    //                     ) as pr_no,
    //                     x5.id as historyid, x6.os_asset_id
    //                     "))
    //             ->wherein('a.doc_type', ['FIP', 'FIO'])
    //             ->orderby('a.doc_no', 'desc');

    //     // searchForm parameter, need to change using bind parameter
    //     if ($request->s == "form")
    //     {   if ($request->doc_no)
    //         {   $results->where("a.doc_no", "ilike", "%".$request->doc_no."%");   };
    //         if ($request->asset_id)
    //         {   $results->where("b.asset_id", "ilike", "%".$request->asset_id."%");   };
    //         if ($request->item_name)
    //         {   $results->where("x1.name", "ilike", "%".$request->item_name."%");   };
    //         if ($request->grin_id)
    //         {   $results->where("x2.stock_grin_no", "ilike", "%".$request->grin_id."%");   };
    //         if ($request->grin_no)
    //         {   $results->where("x2.grin_no", "ilike", "%".$request->grin_no."%");   };
    //         if ($request->po_no)
    //         {   $results->where("x3.po_no", "ilike", "%".$request->po_no."%");   };
    //         if ($request->pr_no)
    //         {   $results->where("x3.pr_ref_no", "ilike", "%".$request->pr_no."%");   };
            
    //         if ($request->item_type == 'IT')
    //         {   $results->where(function($query)
    //             {   $query->where("b.ref_item_id", "ilike", "%D/34%")
    //                     ->orWhere("b.ref_item_id", "ilike", "%D/36%");
    //             });
    //         }
    //         else if ($request->item_type == 'Non-IT')
    //         {   $results->where([
    //                 ["b.ref_item_id", "not ilike", "%D/34%"],
    //                 ["b.ref_item_id", "not ilike", "%D/36%"]
    //             ]); 
    //         };
            
    //         if ($request->registered)
    //         {   if ($request->registered == 'NO')
    //             {   $results->whereRaw('
    //                         (   ( x4.name is null ) and (x6.os_asset_id is null) and
    //                             ( x7.cal_os_asset_id is null ) and ( x8.office_asset_id is null )   )'
    //                 ); 
    //             } elseif ($request->registered == 'YES') 
    //             {   $results->whereRaw('
    //                             ( x4.name is not null ) or (x6.os_asset_id is not null) or
    //                             ( x7.cal_os_asset_id is not null ) or ( x8.office_asset_id is not null )'
    //                 ); 
    //             } elseif ($request->registered == 'ARC') 
    //             {   $results->whereNotNull("x5.id");   }

    //         };
    //     }
    //     elseif ($request->s == "combo")
    //     {   if ($request->role_id)
    //         {   $results->where("a.role_id", "=", $request->role_id);   };
    //     };

    //     // dd($results->get());
    //     return $results->get();
    // }
    // public static function registerSearchExt_Hardware($request)
    // {   
    //     $subQuery = DB::table('asset_reg_detail as a')
    //                 ->leftjoin('asset_itd_hardware as b', 'a.asset_id', '=', 'b.asset_id')
    //                 ->selectRaw("a.asset_id, a.ref_item_id,
    //                             (a.asset_id || ' [' || a.serial_no || '] ') as display")
    //                 ->whereNull('b.asset_id');

    //     $results = AssetITDHardware::selectRaw("x.*")
    //                 ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
    //                 ->mergeBindings($subQuery)
    //                 ->orderby('x.asset_id', 'desc');

    //     if ($request->s == "form")
    //     {   if ($request['query'])
    //         {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
    //     };

    //     // $results = DB::select("
    //     //     SELECT *
    //     //     FROM
    //     //     (   SELECT A.ASSET_ID, A.REF_ITEM_ID,
    //     //             (A.ASSET_ID || ' [' || A.SERIAL_NO || '] ') AS DISPLAY
    //     //         FROM ASSET_REG_DETAIL A
    //     //         LEFT OUTER JOIN ASSET_ITD_HARDWARE B ON (A.ASSET_ID = B.ASSET_ID)
    //     //         WHERE B.ASSET_ID IS NULL
    //     //     ) X
    //     //     ORDER BY X.ASSET_ID DESC
    //     //     ", $arr);
    //     // dd($results);
    //     return $results->get();
    // }
    // public static function registerSearchExt_License($request)
    // {   
    //     $subQuery = DB::table('asset_reg_detail as a')
    //                 ->leftjoin('m_item as a1', 'a.ref_item_id', '=', 'a1.item_id')
    //                 ->selectRaw("a.asset_id, a.ref_item_id,
    //                     (a.asset_id || ' [' || a1.name || ', '|| 
    //                         coalesce(a.remarks,'-') || '] ') as display");
    //     if ($request->s == "form")
    //     {   if ($request->type == "OS")
    //         {   
    //             $subQuery->leftjoin('asset_itd_license as b', 'a.asset_id', '=', 'b.os_asset_id')
    //                 ->where(function($query)
    //                     {   $query->where("a.ref_item_id", "ilike", "%D/36%")
    //                             ->orWhere("a.ref_item_id", "=", "D/34/114530");
    //                     })
    //                 ->where(function($query)
    //                     {   $query->where("a1.name", "not ilike", "%CAL%")
    //                             ->where("a1.name", "not ilike", "%SQL%")
    //                             ->where("a1.name", "not ilike", "%OFFICE%")
    //                             ->where("a1.name", "not ilike", "%EXCEL%")
    //                             ->where("a1.name", "not ilike", "%VISIO%");
    //                     })
    //                 ->whereNull("b.os_asset_id");   

    //         }   elseif ($request->type == "CALOS")
    //         {   
    //             $subQuery->leftjoin('asset_itd_license as b', 'a.asset_id', '=', 'b.cal_os_asset_id')
    //                 ->where("a.ref_item_id", "ilike", "%D/36%")
    //                 ->where("a1.name", "not ilike", "%CAL%")
    //                 ->whereNull("b.cal_os_asset_id");   

    //         }   elseif ($request->type == "OFC")
    //         {   
    //             $subQuery->leftjoin('asset_itd_license as b', 'a.asset_id', '=', 'b.office_asset_id')
    //                 ->where("a.ref_item_id", "ilike", "%D/36%")
    //                 ->where("a1.name", "not ilike", "%OFFICE%")
    //                 ->whereNull("b.office_asset_id");   
    //         };
    //     };

    //     $results = AssetITDHardware::selectRaw("x.*")
    //                 ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
    //                 ->mergeBindings($subQuery)
    //                 ->orderby('x.asset_id', 'asc');
        
    //     if ($request->s == "form")
    //     {   if ($request['query'])
    //         {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
    //     };

    //     return $results->get();
    // }
    // public static function hardwareSearch($request)
    // {   
    //     if ($request->s == "form")
    //     {   // search asset hardware
    //         if ($request->source == "ASSET")
    //         {   $results = DB::table('asset_itd_hardware as b1')
    //                 ->leftjoin('ocsweb_hardware as a', function($join)
    //                     {   $join->on('a.id', '=', 'b1.id')
    //                              ->on('a.deviceid', '=', 'b1.deviceid');
    //                     })
    //                 ->select('a.*')
    //                 ->whereNull('a.id')
    //                 ->orderby('b1.id');
    //         } // search archive hardware
    //         else if ($request->source == "ARCHIVE")
    //         {   $results = DB::table('asset_itd_hardware_history as b1')
    //                 ->select('b1.*')
    //                 ->orderby("b1.id");
    //         } else // search ocs / active hardware
    //         {   $results = DB::table('ocsweb_hardware as a')
    //             ->join('ocsweb_accountinfo as b', 'a.id', '=', 'b.hardware_id')
    //             ->leftjoin('asset_itd_hardware as b1', 'a.name', '=', 'b1.name')
    //             ->leftjoin('asset_reg_detail as b2', function($join)
    //                 {   $join->on('b1.doc_no', '=', 'b2.doc_no')
    //                          ->on('b1.asset_id', '=', 'b2.asset_id')
    //                          ->on('b1.serial_no', '=', 'b2.serial_no');
    //                 })
    //             ->leftjoin('m_department as b3', 'b1.dept_id', '=', 'b3.dept_id')
    //             ->leftjoin('m_item as b4', 'b1.item_id', '=', 'b4.item_id')
    //             ->select(DB::raw("
    //                 a.*, b.tag, b1.id as itd_id,
    //                 b1.asset_id, b1.doc_no, b1.serial_no, b1.item_id, b4.name as item_name,
    //                 b1.remark, b1.itd_asset_no, b1.status, b1.company_id, b1.site_id,
    //                 b1.dept_id, b3.name as dept_name,
    //                 trim(b1.is_server) as is_server, trim(b1.is_client) as is_client,
    //                 trim(b1.is_vm) as is_vm, b1.macaddr,
    //                 b1.comp_type, b1.parent_asset_id, b1.is_asset_reported,
    //                 b1.created_date, b1.created_by,
    //                 b1.modified_date, b1.modified_by,
    //                 b2.asset_id as ori_asset_id,
    //                 ( select array_to_string(array
    //                     ( select trim(macaddr) from ocsweb_networks
    //                       where a.id = hardware_id
    //                     ), ', ')
    //                 ) as ori_macaddr
    //                 "))
    //             ->orderby('b1.name');

    //             if ( $request->source == "MISSING" )
    //             {   $results->where("a.id", "<>", "b.id"); };

    //             if ( $request->doc_no )
    //             {   $results->where("a.doc_no", "ilike", "%".$request->doc_no."%"); };

    //             if ($request->asset_id)
    //             {   $results->where("b1.asset_id", "ilike", "%".$request->asset_id."%");   };
                
    //             if ($request->item_id)
    //             {   $results->where("b1.item_id", "ilike", "%".$request->item_id."%");   };
                
    //             if ($request->hardware_name)
    //             {   $hardware_name = $request->hardware_name;
    //                 $results->where(function($query) use($hardware_name)
    //                     {   $query->where("a.name", "ilike", "%".$hardware_name."%")
    //                             ->orWhere("b1.name", "ilike", "%".$hardware_name."%");
    //                     });
    //             };
    //             if ($request->tag)
    //             {   $results->where("b.tag", "ilike", "%".$request->tag."%");   };
                
    //             if ($request->username)
    //             {   $results->where("a.userdomain", "ilike", "%".$request->username."%");   };
                
    //             if ($request->os_name)
    //             {   $results->where("a.osname", "ilike", "%".$request->os_name."%");   };
                
    //             if ($request->processor)
    //             {   $results->where("a.procesort", "ilike", "%".$request->processor."%");   };
                
    //             if ($request->ip_address)
    //             {   $results->where("a.ipaddr", "ilike", "%".$request->ip_address."%");   };
                
    //             if ($request->is_monitor)
    //             {   if ($request->is_monitor == "NO") 
    //                 {   $results->whereNull('b1.created_date'); }
    //                 else if ($request->is_monitor == "YES") 
    //                 {   $results->whereNotNull('b1.created_date'); }
    //             };

    //             if ($request->status)
    //             {   if ($request->status == "ALL") {  }
    //                 else 
    //                 {   $results->where("b1.status", "=", $request->status); }
    //             };

    //             if ($request->comp_type)
    //             {   if ($request->comp_type == "ALL") {  }
    //                 else 
    //                 {   $results->where("b1.comp_type", "=", $request->comp_type); }
    //             };
    //         };
    //     };
        
    //     // dd($results->get());
    //     return $results->get();
    // }
    // public static function softwareSearch($request)
    // {   $arr = array();
    //     $my_arr = array();
    //     $results = DB::table('ocsweb_softwares as a')
    //         ->leftjoin('asset_itd_hardware as b1', 'a.hardware_id', '=', 'b1.id')
    //         ->select(DB::raw("a.name, count(1) as installed"))
    //         ->groupby('a.name')
    //         ->orderby('a.name', 'asc');

    //     if ($request->s == "form")
    //     {   // search asset hardware
    //         if ($request->asset_id)
    //         {   $results->where("b1.asset_id", "ilike", "%".$request->asset_id."%");   };

    //         if ($request->asset_name)
    //         {   $results->where("b1.name", "ilike", "%".$request->asset_name."%");   };

    //         if ($request->software_name)
    //         {   $results->where("a.name", "ilike", "%".$request->software_name."%");   };
                
    //     };
        
    //     // dd($results->get());
    //     return $results->get();
    // }
    // public static function assetSoftwareSearch($request)
    // {   $arr = array();
    //     $my_arr = array();
    //     $results = DB::table('asset_itd_hardware as a')
    //         ->leftjoin('ocsweb_softwares as b1', 'b1.hardware_id', '=', 'a.id')
    //         ->select('a.asset_id', 'a.name as asset_name', 'a.osname', 'b1.*')
    //         ->where('b1.installdate', '<>', '00/00/0000')
    //         ->orderby('a.name', 'asc')
    //         ->orderby('b1.name', 'asc');

    //     if ($request->s == "form")
    //     {   // search asset hardware
    //         if ($request->asset_id)
    //         {   $results->where("b1.asset_id", "ilike", "%".$request->asset_id."%");   };

    //         if ($request->asset_name)
    //         {   $results->where("a.name", "ilike", "%".$request->asset_name."%");   };

    //         if ($request->software_name)
    //         {   $results->where("b1.name", "ilike", "%".$request->software_name."%");   };
                
    //     };
        
    //     // dd($results->get());
    //     return $results->get();
    // }
    // public static function assetSoftwareSearchExt($request)
    // {   
    //     $subQuery = DB::table('asset_itd_hardware as a')
    //                 ->leftjoin('ocsweb_softwares as b', 'a.id', '=', 'b.hardware_id')
    //                 ->selectRaw("b.id, b.name as software_name,
    //                         (   case when b.name ilike '%MICROSOFT%' then 'MS'
    //                                 else 'NON-MS'
    //                             end
    //                         ) as software_type,
    //                         (b.name || ' [' || b.id || '] ') as display");
    //     if ($request->asset_id)
    //         {   $subQuery->where("a.asset_id", "=", $request->asset_id);   };

    //     $results = AssetITDHardware::selectRaw("x.*")
    //                 ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
    //                 ->mergeBindings($subQuery)
    //                 ->orderby('x.software_name', 'asc');

    //     if ($request->s == "form")
    //     {   if ($request['query'])
    //         {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
    //     };

    //     return $results->get();
    // }
    // public static function licenseSearch($request)
    // {   $arr = array();
    //     $my_arr = array();
    //     if ($request->allocation == "NO")
    //     {   $license_table = 'asset_itd_license as b1'; }
    //     else
    //     {   $license_table = 'asset_itd_license_allocation as b1'; };

    //     $results = DB::table('asset_itd_hardware as a')
    //             ->join('ocsweb_accountinfo as b', 'a.id', '=', 'b.hardware_id')
    //             ->leftjoin($license_table, function($join)
    //                 {   $join->on('a.asset_id', '=', 'b1.asset_id')
    //                          ->on('a.name', '=', 'b1.name');
    //                 })
    //             ->leftjoin('m_department as c', 'a.dept_id', '=', 'c.dept_id')
    //             ->leftjoin('m_item as d', 'b1.os_item_id', '=', 'd.item_id')
    //             ->leftjoin('m_item as d1', 'b1.cal_os_item_id', '=', 'd1.item_id')
    //             ->leftjoin('m_item as d2', 'b1.office_item_id', '=', 'd2.item_id')
    //             ->select(DB::raw("
    //                 (case when b1.asset_id is null then a.asset_id else b1.asset_id end) as asset_id,
    //                 a.name, a.userid, a.ipsrc, a.status, a.site_id,
    //                 (case when a.tag is null then b.tag else a.tag end) as tag,
    //                 (case when b1.os_name <> a.osname then 1 else 0 end) as os_diff,
    //                 b1.os_name, b1.os_name as b1_os_name, a.osname as a_os_name,
    //                 b1.os_type, b1.os_key, b1.os_key as b1_os_key, a.winprodkey as a_os_key,
    //                 (case when b1.os_key_type is null then 'na' else b1.os_key_type end) as os_key_type,
    //                 (case when b1.os_license_type is null then 'na' else b1.os_license_type end) as os_license_type,
    //                 (case when b1.os_license_name is null then b1.os_name else b1.os_license_name end) as os_license_name,
    //                 (case when b1.os_license_key is null then b1.os_key else b1.os_license_key end) as os_license_key,
    //                 b1.os_asset_id, b1.os_item_id, d.name as os_item_name,
    //                 b1.cal_os_asset_id, b1.cal_os_item_id, d1.name as cal_os_item_name,
    //                 (case when b1.office_type is null then 'non-ms' else b1.office_type end) as office_type,
    //                 (case when b1.office_license_type is null then 'na' else b1.office_license_type end) as office_license_type,
    //                 b1.office_name, b1.office_asset_id, b1.office_item_id, d2.name as office_item_name,
    //                 (case when b1.office_license_name is null then b1.office_name else b1.office_license_name end) as office_license_name,
    //                 b1.office_key, b1.office_soft_id,
    //                 b1.av_type, b1.av_name, b1.av_key, b1.av_license_type, b1.av_license_name, b1.av_license_key,
    //                 (case when b1.diagram_type is null then 'non-ms' else b1.diagram_type end) as diagram_type,
    //                 (case when b1.diagram_license_type is null then 'na' else b1.diagram_license_type end) as diagram_license_type,
    //                 b1.diagram_name, b1.diagram_key, b1.diagram_soft_id, b1.remark,
    //                 b1.created_date, b1.created_by,
    //                 b1.modified_date, b1.modified_by,
    //                 c.name as dept_name
    //                 "))
    //             ->orderby('a.name');
        
    //     if ($request->s == "form")
    //     {   if ( $request->asset_id )
    //         {   $results->where("b1.asset_id", "ilike", "%".$request->asset_id."%"); };

    //         if ( $request->asset_name )
    //         {   $results->where("a.name", "ilike", "%".$request->asset_name."%"); };

    //         if ( $request->tag )
    //         {   $results->where("a.tag", "ilike", "%".$request->tag."%"); };

    //         if ( $request->status <> "ALL")
    //         {   $results->where("a.status", "=", $request->status); };

    //         if ($request->os_key)
    //             {   $v_query = $request->os_key;
    //                 $results->where(function($query) use($v_query)
    //                     {   $query->where("a.winprodkey", "ilike", "%".$v_query."%")
    //                             ->orWhere("b1.os_key", "ilike", "%".$v_query."%")
    //                             ->orWhere("b1.os_license_key", "ilike", "%".$v_query."%");
    //                     });
    //             };

    //         if ( $request->os_type <> "ALL")
    //         {   $results->where("b1.os_type", "=", $request->os_type); };

    //         if ( $request->os_key_type <> "ALL")
    //         {   $results->where("b1.os_key_type", "=", $request->os_key_type); };
    
    //         if ( $request->os_lic_type <> "ALL")
    //         {   $results->where("b1.os_license_type", "=", $request->os_lic_type); };

    //         if ( $request->reported <> "ALL")
    //         {   $results->where("a.is_asset_reported", "=", $request->reported); };
    //     };
    //     // dd($results->get());
    //     return $results->get();
    // }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
