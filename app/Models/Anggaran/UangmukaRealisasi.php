<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use DB;

class UangmukaRealisasi extends AppModel
{
    /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/
    // -- Table: public.uangmuka_nota

    // -- DROP TABLE public.uangmuka_nota;
    
    // CREATE TABLE public.uangmuka_nota
    // (
    //     id numeric(18,0) NOT NULL,
    //     uangmuka_no character varying(30) COLLATE pg_catalog."default" NOT NULL,
    //     pengajuan_no character varying(20) COLLATE pg_catalog."default" NOT NULL,
    //     rapbs_no character varying(50) COLLATE pg_catalog."default" NOT NULL,
    //     coa_id character varying(8) COLLATE pg_catalog."default",
    //     nota_no character varying(30) COLLATE pg_catalog."default" NOT NULL,
    //     keterangan_nota character varying(256) COLLATE pg_catalog."default",
    //     volume_nota numeric(5,2) DEFAULT 0,
    //     tarif_nota numeric(18,2) DEFAULT 0,
    //     satuan_nota character varying(20) COLLATE pg_catalog."default",
    //     jumlah_nota numeric(20,2) DEFAULT 0,
    //     created_by numeric(18,0),
    //     created_date timestamp without time zone NOT NULL DEFAULT now(),
    //     modified_date timestamp without time zone DEFAULT now(),
    //     approve_date timestamp without time zone,
    //     approve_by numeric(1,0),
    //     approve_status numeric(1,0) DEFAULT 1,
    //     approve_reason character varying(256) COLLATE pg_catalog."default",
    //     seq_no numeric(18,2) NOT NULL,
    //     CONSTRAINT uangmuka_nota_pk PRIMARY KEY (uangmuka_no, nota_no, seq_no),
    //     CONSTRAINT uangmuka_nota_ux UNIQUE (id),
    //     CONSTRAINT uangmuka_detail1_fk FOREIGN KEY (id, uangmuka_no)
    //         REFERENCES public.uangmuka_detail (id, uangmuka_no) MATCH SIMPLE
    //         ON UPDATE NO ACTION
    //         ON DELETE CASCADE
    // )
    // WITH (
    //     OIDS = FALSE
    // )
    // TABLESPACE pg_default;
    
    // ALTER TABLE public.uangmuka_nota
        // OWNER to postgres;  
          /*
    
     *//*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'uangmuka_nota';
	protected $primaryKey = 'id';
    // public $sequence_name = 'uangmuka_nota_seq';
    
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */


    public static function UmRealisasiSearch($request, $user_id, $level)
    {
        $results = DB::table('uangmuka_master as a')
        // ->join('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah
                ->leftjoin('user_anggaran as q1',function($join)
                        {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                            ->on('q1.user_id','=','a.created_by');})

                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.created_by')
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, x8.username,
                    x2.name as urusan_mrapbs_id_name,
                    x3.name as program_mrapbs_id_name,
                    x4.name as kegiatan_mrapbs_id_name,
                    (   select sum(b.jum_ajuan) 
                        from uangmuka_detail as b 
                        where b.uangmuka_no = a.uangmuka_no
                        ) as total_biaya,

                        (   select sum(z.jumlah_nota) 
                        from uangmuka_nota as z 
                        where z.uangmuka_no = a.uangmuka_no
                        ) as total_realisasi
        
                "))
                ->where('a.approve_status_3','=',1)
                ->where("q1.user_id","=",$user_id)
            ->orderby('a.id', 'asc');

        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->uangmuka_no) 
            {   $results->where("uangmuka_no", "ilike", "%" . $request->uangmuka_no."%"); 
            };
            if ($request->uangmuka_keterangan_id)
            {   $results->where("uangmuka_keterangan_id", "ilike", "%".$request->uangmuka_keterangan_id."%");  
            };

            if ($level == 2 ) //admin level
            {
            $results->where('a.status', '=', 0);
            };
            if ($request->pt) {   return $results->first();   }
            else {  return $results->get(); };
            // if ($request->status) {
            //     $results->where("status", "=", self::setStatus($request->status));
            // };
        };
        return $results->get();
        }

    public static function UangmukaNotaSearch($request)
    {  
        $results = DB::table('uangmuka_nota as a')
            ->select(DB::raw("a.*, '' as action"))
            ->orderby('a.id', 'asc');
        
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->nota_no) 
            {   $results->where("nota_no", "ilike", "%" . $request->nota_no. "%");
            };

            if ($request->biaya_id) 
            {   $results->where("biaya_id", "ilike", "%" . $request->biaya_id. "%");
            };

        };

        return $results->get();
    }


    public static function UangmukaNotaSearch1($request)
    {
        $results = DB::table('uangmuka_nota as a')
                ->leftjoin('uangmuka_detail as b', 'b.id', '=', 'a.biaya_id')
                ->select(DB::raw("a.*, '' as action"))
                ->where('a.biaya_id','=',$request->id)
                ->orderby('a.id', 'asc');
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->id) 
            {   $results->where("a.biaya_id", "ilike", "%" . $request->id. "%");
            };

            if ($request->uangmuka_no) 
            {   $results->where("a.uangmuka_no", "ilike", "%" . $request->uangmuka_no. "%");
            };

        }; 
        return $results->get();

        }

        // public static function UangmukaNotaSearch1($request)
        // {
        //     $results = DB::table('uangmuka_nota as a')
        //             ->leftjoin('uangmuka_detail as b', 'b.id', '=', 'a.biaya_id')
        //             ->select(DB::raw("a.biaya_id as biaya_id, to_char(invoice_date,'dd/mm/yyyy') as invoice_date, 
        //             a.nota_no as nota_no, a.keterangan_nota as keterangan_nota,
        //             a.volume_nota as volume_nota, a.tarif_nota as tarif_nota,
        //             a.satuan_nota as satuan_nota, a.jumlah_nota as jumlah_nota, a.created_date as created_date,a.id as id"))
        //             ->where('a.biaya_id','=',$request->id)
        //             ->orderby('a.id', 'asc');
        //     // searchForm parameter
        //     if ($request->s == "form") 
        //     {
        //         if ($request->id) 
        //         {   $results->where("a.biaya_id", "ilike", "%" . $request->id. "%");
        //         };
    
        //         if ($request->uangmuka_no) 
        //         {   $results->where("a.uangmuka_no", "ilike", "%" . $request->uangmuka_no. "%");
        //         };
    
        //     }; 
        //     return $results->get();
    
        //     }
    

    public static function UangmukaNotaDelete($rapbs_no)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('nota_no', '=', $rapbs_no)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    public static function UangmukaRealisasiSave($json_data, $head_data,  $total,  $site, $user_id, $company_id)  
    { //print_r($json_data);exit;
    if (count($json_data) > 0)
       { 
        DB::beginTransaction();
        try 
          {  foreach ($json_data as $json)
             {
 
                if ($json->created_date == '')
                {   
                // save new record
                    $uangmukarealisasi = new UangmukaRealisasi();
                    $uangmukarealisasi->created_date = date('Ymd');
                    $uangmukarealisasi->nota_no   = trim($json->nota_no);
                    $uangmukarealisasi->pengajuan_no     = $json->pengajuan_no;
                    $uangmukarealisasi->rapbs_no         = $json->rapbs_no;
                    $uangmukarealisasi->uangmuka_no      = $json->uangmuka_no;
                    $uangmukarealisasi->biaya_id         = $json->biaya_id;
                    $uangmukarealisasi->coa_id           = $json->coa_id;
                }
                else 
                {  
                    $uangmukarealisasi = uangmukarealisasi::where('id', '=', $json->id)->first();
                    $uangmukarealisasi->modified_date =  date('Ymd');

                    $uangmuka_detail = UangmukaDetail::where('id', '=', $head_data)->first();
                    if (Empty($uangmuka_detail)) { $uangmuka_detail->realisasi_biaya = $total;};
                }; 

                    if (Empty($json->invoice_date)) {
                        $uangmukarealisasi->invoice_date = null;
                    } else {
                        $uangmukarealisasi->invoice_date =$uangmukarealisasi->makeTimeStamp($json->invoice_date, 'd/m/Y');
                    };

                    // $uangmukarealisasi->invoice_date     = $uangmukarealisasi->makeTimeStamp($json->invoice_date, 'd/m/Y');                      
                    $uangmukarealisasi->keterangan_nota  = $json->keterangan_nota;
                    $uangmukarealisasi->volume_nota      = $json->volume_nota;
                    $uangmukarealisasi->tarif_nota       = $json->tarif_nota;
                    $uangmukarealisasi->satuan_nota      = $json->satuan_nota;
                    $uangmukarealisasi->jumlah_nota      = $json->jumlah_nota;
                   
                    $result = $uangmukarealisasi->save();

                    if ($result) 
                    {  // print_r($total);exit;
                        $result = UangmukaDetail::NotaTotalSave(
                                $json,
                                $head_data, 
                                $total ); 
                    };
                    
                        // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            }    
        } 
            catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
    
        return $result;  
    
        }
     
    }


    public static function uangmukarealisasiDelete($id)
    {   DB::beginTransaction();
        try 
        {  // print_r($id);exit;
            $result = static::where('id', '=', $id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    public static function UmAppRealisasiSearch($request, $user_id, $level)
    {
        $results = DB::table('uangmuka_master as a')
        // ->join('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah
                ->leftjoin('user_anggaran as q1',function($join)
                        {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                            ;})

                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.created_by') 
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, x8.username,
                    x2.name as urusan_mrapbs_id_name,
                    x3.name as program_mrapbs_id_name,
                    x4.name as kegiatan_mrapbs_id_name,
                    (   select sum(b.jum_ajuan) 
                        from uangmuka_detail as b 
                        where b.uangmuka_no = a.uangmuka_no
                        ) as total_biaya,

                        (   select sum(z.jumlah_nota) 
                        from uangmuka_nota as z 
                        where z.uangmuka_no = a.uangmuka_no
                        ) as total_realisasi
        
                "))
                ->where('a.realisasi_hasil','<>',null)
                ->where("q1.user_id","=",$user_id)
                ->orderby('a.id', 'asc');

        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->uangmuka_no) 
            {   $results->where("uangmuka_no", "ilike", "%" . $request->uangmuka_no."%"); 
            };
            if ($request->uangmuka_keterangan_id)
            {   $results->where("uangmuka_keterangan_id", "ilike", "%".$request->uangmuka_keterangan_id."%");  
            };

            if ($request->pt) {   return $results->first();   }
            else {  return $results->get(); };
            // if ($request->status) {
            //     $results->where("status", "=", self::setStatus($request->status));
            // };
        };
        return $results->get();
        }
 
        public static function UmAppRealisasiKeuSearch($request, $user_id, $level)
        {
            $results = DB::table('uangmuka_master as a')
            // ->join('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah
                    ->leftjoin('user_anggaran as q1',function($join)
                            {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                                ->on('q1.user_id','=','a.created_by');})
    
                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                    ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                    ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                    ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                    ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.created_by')
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name, x8.username,
                        x2.name as urusan_mrapbs_id_name,
                        x3.name as program_mrapbs_id_name,
                        x4.name as kegiatan_mrapbs_id_name,
                        (   select sum(b.jum_ajuan) 
                            from uangmuka_detail as b 
                            where b.uangmuka_no = a.uangmuka_no
                            ) as total_biaya,
    
                            (   select sum(z.jumlah_nota) 
                            from uangmuka_nota as z 
                            where z.uangmuka_no = a.uangmuka_no
                            ) as total_realisasi
            
                    "))
 //               ->where('a.realisasi_hasil','<>',null)
                ->where('a.approve_status_4','=',1)
                // ->where("q1.user_id","=",$user_id)
                ->orderby('a.id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->uangmuka_no) 
                {   $results->where("uangmuka_no", "ilike", "%" . $request->uangmuka_no."%"); 
                };
                if ($request->uangmuka_keterangan_id)
                {   $results->where("uangmuka_keterangan_id", "ilike", "%".$request->uangmuka_keterangan_id."%");  
                };
    
                if ($level == 2 ) //admin level
                {
                $results->where('a.status', '=', 0);
                };
                if ($request->pt) {   return $results->first();   }
                else {  return $results->get(); };
                // if ($request->status) {
                //     $results->where("status", "=", self::setStatus($request->status));
                // };
            };
            return $results->get();
            }

    



    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
