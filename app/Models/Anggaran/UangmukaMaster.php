<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use App\Models\Administrator\Mrapbs;
use App\Models\Administrator\MUser;
use App\Models\Administrator\Mprogram;
use App\Models\Ppdb\TahunAjaran;
use App\Models\Anggaran\UangmukaDetail;
use App\Models\Anggaran\PengajuanMaster;
use App\Models\Anggaran\PengajuanDetail;
use App\Models\Administrator\SiteDocument;

use DB;

class UangmukaMaster extends AppModel
{   /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table      = 'uangmuka_master';
    protected $primaryKey = 'id';
    public $sequence_name = 'uangmuka_master_seq'; 
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function UangmukaDetail()
    {   return $this->hasMany('App\Models\Anggaran\UangmukaDetail', 'rapbs_no');    }

    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function headerPrintOut($request)
    {   
        $results = DB::table('user_anggaran as q1')
                ->join('uangmuka_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, 
                    x2.name as urusan_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name,
                    (   select sum(b.jum_ajuan) 
                        from uangmuka_detail as b 
                        where b.uangmuka_no = a.uangmuka_no
                    ) as total_biaya,
                    (   select sum(z.jumlah_nota) 
                    from uangmuka_nota as z 
                    where z.uangmuka_no = a.uangmuka_no
                    ) as total_realisasi ,
    
                    (   select sum(b.realisasi_biaya) 
                    from uangmuka_detail as b 
                    where b.uangmuka_no = a.uangmuka_no
                    ) as total_realisasi_biaya
                "))
            ->orderby('a.id', 'asc');

        if ($request->s == "form")
        {   
            if ( $request->uangmuka_no )
            {   $results->where("a.uangmuka_no", "=", $request->uangmuka_no); };
        };

        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };
    }


        public static function hduangmukaPrintOut($request)
    { //print_r($request);exit;  
        $results = DB::table('user_anggaran as q1')
                ->join('uangmuka_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, 
                    x2.name as urusan_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name, a.uangmuka_no,
                    (   select sum(b.jum_ajuan) 
                        from uangmuka_detail as b 
                        where b.uangmuka_no = a.uangmuka_no
                    ) as total_biaya
                ")) 
            ->orderby('a.id', 'asc');

        if ($request->s == "form")
        {   

            if ( $request->uangmuka_no )
            {   $results->where("a.uangmuka_no", "=", $request->uangmuka_no); }; 

        };

        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };

    }


    public static function UangmukaSearch($request, $user_id, $level)
    {
        $results = DB::table('pengajuan_master as a')
        // ->join('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah
                ->leftjoin('user_anggaran as q1',function($join)
                        {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                            ->on('q1.user_id','=','a.created_by');})

                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.created_by')
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, x2.name as urusan_mrapbs_id_name, x8.username,
                    x3.name as program_mrapbs_id_name, x4.name as kegiatan_mrapbs_id_name,a.pengajuan_keterangan_id,
                    (   select sum(b.jum_ajuan) 
                        from pengajuan_detail as b 
                        where b.pengajuan_no = a.pengajuan_no
                        ) as total_biaya,
                        (   select sum(c.jum_ajuan) 
                        from uangmuka_detail as c 
                        where c.pengajuan_no = a.pengajuan_no 
                        ) as total_pengajuan
                "))
                ->where('a.approve_status_3','=',1)
                ->where("q1.user_id","=",$user_id)
            ->orderby('a.id', 'asc');

        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->pengajuan_no) 
            {   $results->where("pengajuan_no", "ilike", "%" . $request->pengajuan_no . "%"); 
            };
            if ($request->pengajuan_keterangan_id)
            {   $results->where("pengajuan_keterangan_id", "ilike", "%".$request->pengajuan_keterangan_id."%");  
            };

            if ($level == 2 ) //admin level
            {
            $results->where('a.status', '=', 0);
            };


            if ($request->pt) {   return $results->first();   }
            else {  return $results->get(); };
            
        
                // if ($request->status) {
            //     $results->where("status", "=", self::setStatus($request->status));
            // };
        };

        return $results->get();

        }


    public static function UangmukaMasterSearchExt($request)
    {
        
        $subQuery = DB::table('m_rapbs as a')
        ->selectRaw("a.mrapbs_id, a.name,
        (a.name || ' [' || a.mrapbs_id|| '] ') as display")
        ->where('a.status', '=', 0);

        $results = UangmukaMaster::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }


    public static function UangmukaMasterSave($data, $detail1, $biaya, $site, $user_id, $company_id)
    {   // begin transaction

        //  print_r($biaya); exit;
        DB::beginTransaction();
        try {                 
          //  print_r($data1);exit; 
            if ($data->created_date == '')   
            {   
                if ($data->tahun_ajaran_id == ''){}
                else
                {  // print_r($data1[0]->urusan_mrapbs_id);exit;
                    $company_id = "YMDT";
                    $Document = DB::select( DB::raw("  
                        select b.*, c.doc_code 
                        from m_rapbs as a 
                        join site_documents as b on ( b.company_id = '".$company_id."' 
                            and b.site_id = '".$site."' and b.dept_id = a.dept_id ) 
                        join m_documents as c on ( c.doc_id = b.doc_id )
                        where a.mrapbs_id = '".$detail1[0]->urusan_mrapbs_id."'"
                    ) );
                        // print_r($Document);

                    $doc_code = $Document[0]->doc_code;
                    $seq_no = $Document[0]->um_last_doc + 1; 
                    $doc_id = $Document[0]->id;
                    $SiteDocument = SiteDocument::where('id', '=', $doc_id)->first();
                    $SiteDocument->um_last_doc = $seq_no;
                    $result = $SiteDocument->save();
                    $TahunAjaran = TahunAjaran::where('tahunajaran_id', '=', $data->tahun_ajaran_id)->first();
                    $uangmuka_no = $TahunAjaran->tahunajaran_id.'/UM/'.trim($doc_code).'/'.$seq_no;

                     $Uangmuka_master = new UangmukaMaster();

                   // print_r($data->uangmuka_due_date);exit; 
                    $Uangmuka_master->created_date = date('Ymd');
                    $Uangmuka_master->uangmuka_due_date = $Uangmuka_master->makeTimeStamp($data->uangmuka_due_date, 'd/m/Y');
                    $Uangmuka_master->created_by   = $user_id; 
                    $Uangmuka_master->uangmuka_no = $uangmuka_no;
                    $Uangmuka_master->uangmuka_keterangan_id = $data->uangmuka_keterangan_id;
                    $Uangmuka_master->bln_pengajuan          = date('Ymd');
                };
                // print_r($company_id);exit;
            } 
            else 
            {    
                $Uangmuka_master = UangmukaMaster::where('id', '=', $data->id)->first();
                $Uangmuka_master->modified_date = date('Ymd');
                $Uangmuka_master->modified_by   = $user_id;
                $Uangmuka_master->Uangmuka_no   = $Uangmuka_master->uangmuka_no;
        
            }; 
            $Uangmuka_master->rapbs_no      = $detail1[0]->rapbs_no;
            $Uangmuka_master->pengajuan_no  = $detail1[0]->pengajuan_no;

            $Uangmuka_master->urusan_mrapbs_id       = $detail1[0]->urusan_mrapbs_id;
            $Uangmuka_master->organisasi_mrapbs_id   = $detail1[0]->organisasi_mrapbs_id;
            $Uangmuka_master->program_mrapbs_id      = $detail1[0]->program_mrapbs_id;
            $Uangmuka_master->kegiatan_mrapbs_id     = $detail1[0]->kegiatan_mrapbs_id;
            $Uangmuka_master->sub_kegiatan_mrapbs_id = $detail1[0]->sub_kegiatan_mrapbs_id;
            $Uangmuka_master->sumberdana_id          = $detail1[0]->sumberdana_id;
            $Uangmuka_master->hasil                  = $detail1[0]->hasil;
            // $Uangmuka_master->pic_id                 = $detail1[0]->pic_id;
            $result = $Uangmuka_master->save();

            // save detail data
            if ($result) 
            {  
                $result = UangmukaDetail::UangmukaDetailSave(
                        $biaya, 
                        $site,
                        $user_id,
                        $uangmuka_no,
                        $detail1); 
            };

            // commit transaction
            $result = [true, "Save succesfully"];
            DB::commit();
        } 
        catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function UangMukaMasterDelete($request) //uangmuka reject
    {
        DB::beginTransaction(); 
        try {
            $result = UangMukaDetail::UangMukaDetailDelete($request->uangmuka_no);
            if ($result)
            {   $result = static::where('uangmuka_no', '=', $request->uangmuka_no)->delete();    
            };            
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }



    public static function PengajuanUangmukaDetailSearch($request)
    {  
        $results = DB::table('pengajuan_detail as a')
        ->leftjoin('pengajuan_master as q1', 'q1.pengajuan_no', '=','a.pengajuan_no')
        ->leftjoin('rapbs_master as c', 'c.rapbs_no', '=','a.rapbs_no')
        ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'q1.organisasi_mrapbs_id')
        ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'q1.urusan_mrapbs_id')
        ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'q1.program_mrapbs_id')
        ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'q1.kegiatan_mrapbs_id')
        ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'q1.sumberdana_id')
        ->select(DB::raw("a.rapbs_no, a.pengajuan_no,
                q1.organisasi_mrapbs_id,  
                q1.urusan_mrapbs_id,
                q1.program_mrapbs_id,
                q1.kegiatan_mrapbs_id,
                q1.sumberdana_id,
                x1.name as organisasi_mrapbs_id_name,
                x2.name as urusan_mrapbs_id_name,
                x3.name as program_mrapbs_id_name,
                x4.name as kegiatan_mrapbs_id_name,
                x5.name as sumberdana_id_name, 
                c.sub_kegiatan_mrapbs_id,c.hasil,
                (   select sum(b.jum_ajuan) 
                from pengajuan_detail as b 
                where b.rapbs_no = a.rapbs_no AND b.pengajuan_no = a.pengajuan_no
                
                ) as total
                "))
        ->groupby('a.rapbs_no', 'a.pengajuan_no',
                'x1.name','q1.urusan_mrapbs_id', 
                'x2.name','q1.organisasi_mrapbs_id',
                'x3.name','q1.program_mrapbs_id',
                'x4.name','q1.kegiatan_mrapbs_id',
                'x5.name','q1.sumberdana_id',
                'c.sub_kegiatan_mrapbs_id','c.hasil'
                )
        ->orderby('a.rapbs_no', 'asc');

        if ($request->s == "forms") 
        {
            if ($request->pengajuan_no) 
            {   $results->where("q1.pengajuan_no", "ilike", "%" . $request->pengajuan_no . "%");
            };
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };
    
        }
            return $results->get();
    }
    


    public static function KegiatanPrintOut($request)
    {  
        $results = DB::table('pengajuan_detail as a')
        ->join('rapbs_master as q1', 'q1.rapbs_no', '=','a.rapbs_no')
        ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'q1.organisasi_mrapbs_id')
        ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'q1.urusan_mrapbs_id')
        ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'q1.program_mrapbs_id')
        ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'q1.kegiatan_mrapbs_id')
        // ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'q1.sumberdana_id')

        ->select(DB::raw("a.rapbs_no, a.pengajuan_no,
                q1.organisasi_mrapbs_id,  
                q1.urusan_mrapbs_id,
                q1.program_mrapbs_id,
                q1.kegiatan_mrapbs_id,
                x1.name as organisasi_mrapbs_id_name,
                x2.name as urusan_mrapbs_id_name,
                x3.name as program_mrapbs_id_name,
                x4.name as kegiatan_mrapbs_id_name,
                sub_kegiatan_mrapbs_id, hasil,
                (   select sum(b.jumlah) 
                from pengajuan_detail as b 
                where b.pengajuan_no = a.pengajuan_no
                ) as total_biaya
                "))
        ->groupby('a.pengajuan_no','a.rapbs_no',
                'x1.name','q1.organisasi_mrapbs_id',
                'x2.name','q1.urusan_mrapbs_id',
                'x3.name','q1.program_mrapbs_id',
                'x4.name','q1.kegiatan_mrapbs_id',
                'sub_kegiatan_mrapbs_id','hasil'
                )
        ->orderby('a.rapbs_no', 'asc');

    // searchForm parameter
    if ($request->s == "forms") 
    {
        if ($request->pengajuan_no) 
        {   $results->where("pengajuan_no", "ilike", "%" . $request->pengajuan_no . "%");
        };
        if ($request->rapbs_no) 
        {   $results->where("rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
        };
        if ($request->pt) {   return $results->first();   }
            else {  return $results->get(); };
    }
        return $results->get();
    }

        public static function AppumSearch($request, $user_id, $level)
        {
            $results = DB::table('user_anggaran as q1')
                    ->join('uangmuka_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                    ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                    ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                    ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                    ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.created_by')
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name, x8.username,
                        x2.name as urusan_mrapbs_id_name,
                        x3.name as program_mrapbs_id_name,
                        x4.name as kegiatan_mrapbs_id_name,
                        (   select sum(b.jum_ajuan) 
                            from uangmuka_detail as b 
                            where b.uangmuka_no = a.uangmuka_no
                            ) as total_biaya
                    "))
                    // ->where('a.approve_status_1','=',1)
                    ->where("q1.user_id","=",$user_id);
 //               ->orderby('a.id', 'asc')
                // ->orderby('a.approve_status_2', 'asc');


            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->uangmuka_no) 
                {   $results->where("uangmuka_no", "ilike", "%" . $request->uangmuka_no . "%"); 
                };
    
                if ($level == 2 ) //admin level
                {
                $results->where('a.status', '=', 0);
                };
                if ($request->pt) {   return $results->first();   }
                else {  return $results->get(); };
            };
            return $results->get();
        }

    public static function AppcairSearch($request, $user_id, $level)
        {
            $results = DB::table('user_anggaran as q1')
                    ->join('uangmuka_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                    ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                    ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                    ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                    ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.created_by')
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name, x8.username,
                        x2.name as urusan_mrapbs_id_name,
                        x3.name as program_mrapbs_id_name,
                        x4.name as kegiatan_mrapbs_id_name,
                        (   select sum(b.jum_ajuan) 
                            from uangmuka_detail as b 
                            where b.uangmuka_no = a.uangmuka_no
                            ) as total_biaya
                    "))
                    ->where('a.approve_status_1','=',1)
                    ->where("q1.user_id","=",$user_id)
 //               ->orderby('a.id', 'asc')
                ->orderby('a.approve_status_2', 'asc');


            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->uangmuka_no) 
                {   $results->where("uangmuka_no", "ilike", "%" . $request->uangmuka_no . "%"); 
                };
    
                if ($level == 2 ) //admin level
                {
                $results->where('a.status', '=', 0);
                };
                if ($request->pt) {   return $results->first();   }
                else {  return $results->get(); };
            };
            return $results->get();
        }

    public static function PencairanSearch($request, $user_id, $level)
        {
            $results = DB::table('user_anggaran as q1')
                    ->join('uangmuka_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                    ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                    ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                    ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                    ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.created_by')
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name, x8.username,
                        x2.name as urusan_mrapbs_id_name,
                        x3.name as program_mrapbs_id_name,
                        x4.name as kegiatan_mrapbs_id_name,
                        a.sub_kegiatan_mrapbs_id as sub_kegiatan_mrapbs_id,
                        (   select sum(b.jum_ajuan) 
                            from uangmuka_detail as b 
                            where b.uangmuka_no = a.uangmuka_no
                            ) as total_biaya
                    "))
                    ->where('a.approve_status_2','=',1)
                    ->where("q1.user_id","=",$user_id)
                    ->orderby('a.id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->uangmuka_no) 
                {   $results->where("uangmuka_no", "ilike", "%" . $request->uangmuka_no."%"); 
                };
                if ($request->uangmuka_keterangan_id)
                {   $results->where("uangmuka_keterangan_id", "ilike", "%".$request->uangmuka_keterangan_id."%");  
                };
                if ($request->organisasi_mrapbs_id_name)
                {   $results->where("x1.name", "ilike", "%".$request->organisasi_mrapbs_id_name."%");  
                };
                if ($request->urusan_mrapbs_id_name)
                {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
                };
                if ($request->urusan_mrapbs_id_name)
                {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
                };
                if ($request->sub_kegiatan_mrapbs_id)
                {   $results->where("sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");  
                };
    
                if ($level == 2 ) //admin level
                {
                $results->where('a.status', '=', 0);
                };
                if ($request->pt) {   return $results->first();   }
                else {  return $results->get(); };
                // if ($request->status) {
                //     $results->where("status", "=", self::setStatus($request->status));
                // };
            };
            return $results->get();
            }
    
    public static function Pencairan_MonitoringbiayaSearch($request, $user_id, $level)
        {
            // $results = DB::table('uangmuka_master as a')
            //         // ->join('m_rapbs as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
            //         // ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
            //         ->join('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
            //         ->select(DB::raw("a.urusan_mrapbs_id, 
            //             x2.name as urusan_mrapbs_id_name,
            //             (   select sum(b.jum_ajuan) 
            //                 from uangmuka_detail as b 
            //                 where b.urusan_mrapbs_id = a.urusan_mrapbs_id
            //                 ) as total_pencairan
            //         "))
            //         ->groupby('a.urusan_mrapbs_id','a.approve_status_3','x2.name')
            //         ->where('a.approve_status_3','=',1)
            //     ->orderby('a.urusan_mrapbs_id', 'asc');
            //     return $results->get();
                // $subQuery_1 = DB::table('uangmuka_master as master');

        $subQuery = DB::table('rapbs_master as a')
        ->join('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
        ->join('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
        ->select(DB::raw("a.urusan_mrapbs_id, a.jumlahn,
            x1.name as urusan_mrapbs_id_name,
            x2.name as organisasi_mrapbs_id_name, 

            (   select sum(b.jumlah) from rapbs_detail as b  
                where b.rapbs_no = a.rapbs_no ) as total_biaya,

            (   select sum(d.jumlah) from rapbs_detail as d  
            where d.rapbs_no = a.rapbs_no AND a.hasil ~* 'PR-') as total_perubahan,

            ( select sum(e.jum_ajuan) from uangmuka_detail as e 
                inner join uangmuka_master as master on master.uangmuka_no = e.uangmuka_no
                where  e.rapbs_no = a.rapbs_no AND master.approve_status_3 =  1  ) as total_pencairan,

            (   select sum(c.realisasi_biaya) from uangmuka_detail as c where c.rapbs_no = a.rapbs_no
            ) as total_realisasi 
        ")) 
        ->orderby('a.urusan_mrapbs_id', 'asc');

        $results = RapbsMaster::selectRaw("x.urusan_mrapbs_id, x.urusan_mrapbs_id_name,x.organisasi_mrapbs_id_name, 
                            sum(x.total_biaya) as total_biaya, sum(x.total_perubahan) as total_perubahan,
                            sum(x.total_pencairan) as total_pencairan, 
                            sum(x.jumlahn) as jumlahn, 
                            sum(x.total_realisasi) as total_realisasi")

        ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
        ->mergeBindings($subQuery)
        ->groupby('x.urusan_mrapbs_id', 'x.urusan_mrapbs_id_name','x.organisasi_mrapbs_id_name')
        ->orderby('x.urusan_mrapbs_id', 'asc');

        $x = $results->get();
        //  print_r(DB::getQueryLog()); exit;
        return $x; 


        }
    


    public static function uangmukaMasterApproval1($request, $site, $user_id, $company_id) // approval uangmuka
        {
            DB::beginTransaction();
            try {
                $uangmuka_master = static::where('uangmuka_no', '=', $request->uangmuka_no)->first();  
                $uangmuka_master->approve_date_1   = date('Ymd his');
                $uangmuka_master->approve_status_1 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
                $uangmuka_master->approve_by_1     = $user_id;
                $uangmuka_master->approve_reason_1   = "status di approve";

                $result = $uangmuka_master->save();

                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
            return $result;
        }

    public static function uangmukaMasterApproval2($request, $site, $user_id, $company_id) // approval pencairan
        {
            DB::beginTransaction();
            try {
                $uangmuka_master = static::where('uangmuka_no', '=', $request->uangmuka_no)->first();  
                $uangmuka_master->approve_date_2   = date('Ymd his');
                $uangmuka_master->approve_status_2 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
                $uangmuka_master->approve_by_2     = $user_id;
                $uangmuka_master->approve_reason_2   = "approve pencairan";

                $result = $uangmuka_master->save();

                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
            return $result;
        }

    // public static function uangmukaMasterApproval3($request, $site, $user_id, $company_id) // pencairan
    //     {
    //         DB::beginTransaction();
    //         try {
    //             $uangmuka_master = static::where('uangmuka_no', '=', $request->uangmuka_no)->first();  
    //             $uangmuka_master->approve_date_3   = date('Ymd his');
    //             $uangmuka_master->approve_status_3 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
    //             $uangmuka_master->approve_by_3     = $user_id;
    //             $uangmuka_master->approve_reason_3   = "approve pencairan";
    //             $uangmuka_master->jenis_byr = $uangmuka_master->jenis_byr;
    // //             print_r($uangmuka_master->jenis_byr);exit; 
    //             $result = $uangmuka_master->save();

    //             $result = [true, "Save succcefully"];
    //             DB::commit();
    //         } catch (\Illuminate\Database\QueryException $e) {
    //             // rollback transaction
    //             $result = [false, "Error Executed ---> " . $e];
    //             DB::rollback();
    //         };
    //         return $result;
    //     }

        public static function pencairanApproval($uangmuka_no, $jenis_byr, $catatan, $site, $user_id, $company_id) // pencairan
        {
            DB::beginTransaction();
            try {
                $uangmuka_master = static::where('uangmuka_no', '=', $uangmuka_no)->first();  
                $uangmuka_master->approve_date_3   = date('Ymd his');
                $uangmuka_master->approve_status_3 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
                $uangmuka_master->approve_by_3     = $user_id;
                $uangmuka_master->approve_reason_3   = "approve pencairan";
                $uangmuka_master->jenis_byr = $jenis_byr;
                $uangmuka_master->catatan = $catatan;

//                print_r($jenis_byr);exit; 
                $result = $uangmuka_master->save();

                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
            return $result;
        }    

        public static function RealisasiHasil($uangmuka_no, $realisasi_hasil, $site, $user_id, $company_id) // pencairan
        {
            // print_r($realisasi_hasil);exit; 

            DB::beginTransaction();
            try {
                $uangmuka_master = static::where('uangmuka_no', '=', $uangmuka_no)->first();  
                $uangmuka_master->realisasi_hasil = $realisasi_hasil;
                $uangmuka_master->realisasi_date   = date('Ymd his');
                $result = $uangmuka_master->save();

                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
            return $result;
        }  
    
        public static function approvalRealisasi($request, $site, $user_id, $company_id) // realisasi
        {
            DB::beginTransaction();
            try {
                $uangmuka_master = static::where('uangmuka_no', '=', $request->uangmuka_no)->first();  
                $uangmuka_master->approve_date_4   = date('Ymd his');
                $uangmuka_master->approve_status_4 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
                $uangmuka_master->approve_by_4     = $user_id;
                $uangmuka_master->approve_reason_4   = "approve Realisasi";
                $result = $uangmuka_master->save();

                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
            return $result;
        }

        public static function approvalRealisasiKeu($request, $site, $user_id, $company_id) // realisasi
        {
            DB::beginTransaction();
            try {
                $uangmuka_master = static::where('uangmuka_no', '=', $request->uangmuka_no)->first();  
                $uangmuka_master->approve_date_5   = date('Ymd his');
                $uangmuka_master->approve_status_5 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
                $uangmuka_master->approve_by_5     = $user_id;
                $uangmuka_master->approve_reason_5   = "approve Realisasi Keuangan";
                $result = $uangmuka_master->save();

                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
            return $result;
        }


    public static function MonitoringcairSearch($request, $user_id, $level)
        {
            $results = DB::table('user_anggaran as q1')
                    ->join('uangmuka_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                    ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                    ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                    ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                    ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.created_by')
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name, x8.username,
                        x2.name as urusan_mrapbs_id_name,
                        x3.name as program_mrapbs_id_name,
                        x4.name as kegiatan_mrapbs_id_name,
                        (   select sum(b.jum_ajuan) 
                            from uangmuka_detail as b 
                            where b.uangmuka_no = a.uangmuka_no
                            ) as total_biaya
                    "))
                ->where('a.approve_status_1','=',1)
                ->where("q1.user_id","=",$user_id)
                ->orderby('a.id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->uangmuka_no) 
                {   $results->where("uangmuka_no", "ilike", "%" . $request->uangmuka_no."%"); 
                };
                if ($request->organisasi_mrapbs_id_name)
                {   $results->where("organisasi_mrapbs_id_name", "ilike", "%".$request->organisasi_mrapbs_id_name."%");  
                };
                if ($request->approve_status_3)
                {   $results->where("approve_status_3", "=", 
                    $request->approve_status_3);  
                };
                if ($level == 2 ) //admin level
                {
                $results->where('a.status', '=', 0);
                };
                if ($request->pt) {   return $results->first();   }
                else {  return $results->get(); };
                // if ($request->status) {
                //     $results->where("status", "=", self::setStatus($request->status));
                // };
            };
            return $results->get();
        }


        public static function UangmukaNotivRealisasiSearch($request, $user_id, $level)
        {
            $results = DB::table('user_anggaran as q1')
                    ->join('uangmuka_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                    ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.created_by')
                    ->select(DB::raw("a.*, 
                        x8.username, a.uangmuka_no
                    "))
                    ->where('a.approve_status_3','=',1)
                    ->where('a.approve_status_5','=',0)
                    ->where("a.created_by","=",$user_id)
                    ->orderby('a.id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->uangmuka_no) 
                {   $results->where("uangmuka_no", "ilike", "%" . $request->uangmuka_no."%"); 
                };
    
                if ($level == 2 ) //admin level
                {
                $results->where('a.status', '=', 0);
                };
                if ($request->pt) {   return $results->first();   }
                else {  return $results->get(); };
                // if ($request->status) {
                //     $results->where("status", "=", self::setStatus($request->status));
                // };
            };
            return $results->get();
            }




        /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    // public function Pk_Uangmuka_master()
    // {
    //     return $this->hasMany('App\Models\Uangmuka_master', 'id');
    // }

 //    public function fkMrapbs(){
	//     return $this->belongsTo('\App\Http\Models\Mrapbs', 'urusan_mrapbs_id', 'mrapbs_id');
	// }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
