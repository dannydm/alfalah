<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use DB;

class IncomeDetail extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/
    /*
    -- Table: public.income_detail

    -- DROP TABLE public.income_detail;

    CREATE TABLE public.income_detail
    (
        id numeric(18,0) NOT NULL DEFAULT nextval('income_detail_seq'::regclass),
        income_master_no character varying(50) COLLATE pg_catalog."default" NOT NULL,
        income_detail_id character varying(8) COLLATE pg_catalog."default",
        income_name character varying(100) COLLATE pg_catalog."default",
        income_uraian character varying(256) COLLATE pg_catalog."default",
        income_amount numeric(30,2),
        status numeric(1,0) DEFAULT 0,
        created_date timestamp without time zone NOT NULL DEFAULT now(),
        modified_date timestamp without time zone DEFAULT now(),
        CONSTRAINT income_detail_pk PRIMARY KEY (income_master_no, id),
        CONSTRAINT income_detail_ux UNIQUE (income_master_no, income_detail_id, income_uraian)
    )
    WITH (
        OIDS = FALSE
    )
    TABLESPACE pg_default;

    ALTER TABLE public.income_detail
    OWNER to postgres;    
     *//*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'income_detail';
	protected $primaryKey = 'id';
    public $sequence_name = 'income_detail_seq';
    
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function detailPrintOut($income_no)
    {   $results = DB::table('income_detail as a')
            ->leftjoin('income_master as x1', 'x1.income_no', '=', 'a.income_no')
            ->select(DB::raw("
                a.pengajuan_no, a.rapbs_no, a.coa_id, a.coa_name, a.uraian, a.vol_ajuan, 
                a.tarif, a.satuan, a.jum_ajuan,
                (   select sum(b.jum_ajuan) 
                from income_detail as b 
                where b.income_no = a.income_no
                ) as total_biaya
                "))
            ->where("x1.income_no", "=", $income_no)
            ->orderby('x1.income_no');

        return $results->get();
    }

    public static function incomeDetailSearch($request)
    {   //$results = static::orderby('id', 'asc');  
        //return $results->get();


        $results = DB::table('income_detail as a')
                ->select(DB::raw("a.*, '' as action"))
            ->orderby('a.id', 'asc');
        
        // searchForm parameter
        if ($request->s == "forms") 
        {

            if ($request->income_master_no) 
            {   $results->where("income_master_no", "ilike", "%" . $request->income_master_no. "%");
            };

        };

        return $results->get();
    }

    public static function incomeRinciSearch($request)
    {   //$results = static::orderby('id', 'asc');  
        //return $results->get();


        $results = DB::table('income_detail as a')
            ->select(DB::raw("a.*, '' as action"))
            ->orderby('a.id', 'asc');
        
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->income_id) 
            {   $results->where("income_id", "ilike", "%" . $request->income_id. "%");
            };

        };

        return $results->get();
    }


    public static function incomeDetailDelete($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    public static function incomeDetailSave( $json_data, $site, $user_id, $income_master_no)
    {        DB::enableQueryLog(); 
        if (count($json_data) > 0)
     {   // transaction control ikut parent orders
            foreach ($json_data as $json) 
            {   // save hardware
                if (trim($json->action) == 'REMOVED')
                {   $result = static::where('id', '=', $json->id)->delete();
                    $result = [true, "Save succcefully"];
                }
                else
                {   // save record
                    if ($json->created_date == '')  
                    {   $IncomeDetail = new IncomeDetail();
                        $IncomeDetail->income_master_no   = trim($income_master_no);
                    }
                    else 
                    {   // update hardware
                        $IncomeDetail = IncomeDetail::where('id', '=', $json->id)->first();
                        $IncomeDetail->modified_date = date('Ymd');
                    };

                    $IncomeDetail->income_detail_id    = $json->income_detail_id;
                    $IncomeDetail->income_name         = $json->income_name;
                    $IncomeDetail->income_uraian       = trim($json->income_uraian);
                    $IncomeDetail->income_amount       = $json->income_amount;
                    $result = $IncomeDetail->save();
                };
            };
        };
        // print_r(DB::getQueryLog());exit; 
        // return $result; (bingung ini harus ditanyakan)
    }

    public static function BudgetingSearch($request)
    {    DB::enableQueryLog();
        $subQuery = DB::table('rapbs_master as a')
                        ->join('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                        ->select(DB::raw("a.urusan_mrapbs_id, 
                            x1.name as urusan_mrapbs_id_name,  
                            (   select sum(b.jumlah) 
                                from rapbs_detail as b  
                                where b.rapbs_no = a.rapbs_no
                            ) as total_biaya
                        "))
            // ->where('a.approve_status_2','=',1)
        ->orderby('x1.mrapbs_id', 'asc');

        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x1.urusan", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };
        };

        $results = RapbsMaster::selectRaw("x.urusan_mrapbs_id, x.urusan_mrapbs_id_name,  
                        sum(x.total_biaya) as total_biaya")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->groupby('x.urusan_mrapbs_id', 'x.urusan_mrapbs_id_name')
            ->orderby('x.urusan_mrapbs_id', 'asc');

            $x = $results->get();
 //             print_r(DB::getQueryLog()); exit;
            return $x; 
    }

    public static function BudgetingAppSearch($request)
    {    DB::enableQueryLog();
        $subQuery = DB::table('rapbs_master as a')
                        ->join('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                        ->select(DB::raw("a.urusan_mrapbs_id, 
                            x1.name as urusan_mrapbs_id_name,  
                            (   select sum(b.jumlah) 
                                from rapbs_detail as b  
                                where b.rapbs_no = a.rapbs_no
                            ) as total_biaya
                        "))
            ->where('a.approve_status_3','=',1)
        ->orderby('x1.mrapbs_id', 'asc');

        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x1.urusan", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };
        };

        $results = RapbsMaster::selectRaw("x.urusan_mrapbs_id, x.urusan_mrapbs_id_name,  
                        sum(x.total_biaya) as total_biaya")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->groupby('x.urusan_mrapbs_id', 'x.urusan_mrapbs_id_name')
            ->orderby('x.urusan_mrapbs_id', 'asc');

            $x = $results->get();
 //             print_r(DB::getQueryLog()); exit;
            return $x; 
    }

    public static function DashboardAngSearch($request)
    {    DB::enableQueryLog();
        $subQuery = DB::table('rapbs_master as a')
                        ->join('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                        ->join('uangmuka_detail as x2','x2.urusan_mrapbs_id','=','a.urusan_mrapbs_id')
                        ->select(DB::raw("a.urusan_mrapbs_id, a.jumlahn,
                            x1.name as urusan_mrapbs_id_name,  
                            (   select sum(b.jumlah) 
                                from rapbs_detail as b  
                                where b.rapbs_no = a.rapbs_no
                            ) as total_biaya,
                            (   select sum(c.jum_ajuan) 
                                from uangmuka_detail as c  
                                where c.rapbs_no = a.rapbs_no
                            ) as total_uangmukanota

                        "))
            // ->where('a.approve_status_2','=',1)
        ->orderby('x1.mrapbs_id', 'asc');

        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x1.urusan_mrapbs_id_name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };
        };

        $results = RapbsMaster::selectRaw("x.urusan_mrapbs_id, x.urusan_mrapbs_id_name,  
                        sum(x.total_biaya) as total_biaya, sum(x.jumlahn) as jumlahn , sum(x.total_uangmukanota) as total_uangmukanota")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->groupby('x.urusan_mrapbs_id', 'x.urusan_mrapbs_id_name')
            ->orderby('x.urusan_mrapbs_id', 'asc');

            $x = $results->get();
 //             print_r(DB::getQueryLog()); exit;
            return $x; 
    }
 
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
