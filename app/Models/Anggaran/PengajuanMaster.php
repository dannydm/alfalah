<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use App\Models\Administrator\Mrapbs;
use App\Models\Administrator\MUser;
use App\Models\Administrator\Mprogram;
use App\Models\Ppdb\TahunAjaran;
use App\Models\Anggaran\RapbsMaster;
use App\Models\Anggaran\RapbsDetail;
use App\Models\Anggaran\PengajuanDetail;
use DB;

class PengajuanMaster extends AppModel
{   /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/
    // CREATE SEQUENCE pengajuan_master_seq;
    // CREATE TABLE public.pengajuan_master
    // (
        // id numeric(18,0) NOT NULL DEFAULT nextval('pengajuan_master_seq'::regclass),
        // pengajuan_no character varying(20) COLLATE pg_catalog."default" NOT NULL,
        // rapbs_no character varying(20) COLLATE pg_catalog."default" NOT NULL,
        // tahun_ajaran_id numeric(4,0),
        // urusan_mrapbs_id_1 numeric(18,0),
        // organisasi_mrapbs_id_1 numeric(18,0),
        // program_mrapbs_id_1 numeric(18,0),
        // kegiatan_mrapbs_id_1 numeric(18,0),
        // sumberdana_id numeric(4,0),
        // coa_id character varying(8) COLLATE pg_catalog."default",
        // masukan character varying(64) COLLATE pg_catalog."default",
        // tgt_masukan numeric(5,2),
        // keluaran character varying(64) COLLATE pg_catalog."default",
        // tgt_keluaran numeric(5,2),
        // hasil character varying(64) COLLATE pg_catalog."default",
        // sasaran character varying(64) COLLATE pg_catalog."default",
        // pic_id character varying(40) COLLATE pg_catalog."default",
        // tgl_mulai date,
        // tgl_selesai date,
        // jumlahn numeric(15,2) DEFAULT 0,
        // jumlahke_n numeric(15,2) DEFAULT 0,
        // status numeric(1,0) DEFAULT 0,
        // created_date timestamp without time zone NOT NULL DEFAULT now(),
        // modified_date timestamp without time zone DEFAULT now(),
        // sub_kegiatan_mrapbs_id character varying(64) COLLATE pg_catalog."default",
        // rapbsbiaya_total_sum numeric(18,2),
        // rapbs_validasi numeric(1,0),
        // created_by numeric(18,0),
        // modified_by numeric(18,0),
        // approve_date_1 timestamp without time zone,
        // approve_by_1 numeric(18,0),
        // approve_status_1 numeric(1,0) DEFAULT 1,
        // approve_reason_1 character varying(256) COLLATE pg_catalog."default",
        // approve_date_2 timestamp without time zone,
        // approve_by_2 numeric(18,0),
        // approve_status_2 numeric(1,0),
        // approve_reason_2 character varying(256) COLLATE pg_catalog."default",
        // approve_date_3 timestamp without time zone,
        // approve_by_3 numeric(18,0),
        // approve_status_3 numeric(1,0),
        // approve_reason_3 character varying(256) COLLATE pg_catalog."default",
        // approve_date_4 time without time zone,
        // approve_by_4 numeric(18,0),
        // approve_status_4 numeric(1,0),
        // approve_reason_4 character varying(256) COLLATE pg_catalog."default",
        // urusan_mrapbs_id character varying(18) COLLATE pg_catalog."default",
        // organisasi_mrapbs_id character varying(18) COLLATE pg_catalog."default",
        // program_mrapbs_id character varying(18) COLLATE pg_catalog."default",
        // kegiatan_mrapbs_id character varying(18) COLLATE pg_catalog."default",
        // name character varying(256) COLLATE pg_catalog."default",
        // CONSTRAINT pengajuan_master_pk PRIMARY KEY (pengajuan_no),
        // CONSTRAINT pengajuan_master_ux UNIQUE (id)
        // )
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table      = 'pengajuan_master';
    protected $primaryKey = 'pengajuan_no';
    public $sequence_name = 'pengajuan_master_seq';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function PengajuanDetail()
    {   return $this->hasMany('App\Models\Anggaran\PengajuanDetail', 'rapbs_no');    }

    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function headerPrintOut($request)
    {   
        $results = DB::table('user_anggaran as q1')
        ->join('pengajuan_master as a','q1.mrapbs_id','=','a.urusan_mrapbs_id')
        ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
        ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
        ->leftjoin('m_coa as x2', 'x2.mcoa_id', '=', 'a.coa_id')
        ->leftjoin('m_emp as x3', 'x3.emp_id', '=', 'a.pic_id')
        ->leftjoin('m_users as x4', 'x4.user_id', '=', 'a.created_by')
        ->leftjoin('m_users as x7', 'x7.user_id', '=', 'a.approve_by_2')
        ->leftjoin('m_users as x6', 'x6.user_id', '=', 'a.approve_by_3')
        ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.approve_by_4')
        ->select(DB::raw("a.*, 
            x1.name as urusan_mrapbs_id_name,
            x4.username as user, 
            x7.username as userpengajuan,
            x6.username as kabid,
            x8.username as ketua,
            (   select sum(b.jumlah) 
                from pengajuan_detail as b 
                where b.rapbs_no = a.pengajuan_no
            ) as total_biaya
        "))
    ->orderby('a.id', 'asc');

    // searchForm parameter
        if ($request->s == "form")
            {   
                if ( $request->pengajuan_no )
                {   $results->where("a.pengajuan_no", "=", $request->pengajuan_no); };
            };
        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };
    }

    public static function headerajuanPrintOut($request)
    {   
        DB::enableQueryLog();
      
        $results = DB::table('pengajuan_master as a')
        ->leftjoin('pengajuan_detail as b', 'b.pengajuan_no', '=','a.pengajuan_no')
        ->leftjoin('rapbs_master as c', 'c.rapbs_no', '=','b.rapbs_no')
        ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
        ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
        ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
        ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')

        ->select(DB::raw("b.rapbs_no, a.pengajuan_no,
                x1.name as urusan_mrapbs_id_name,
                x2.name as organisasi_mrapbs_id_name,
                x3.name as program_mrapbs_id_name,
                x4.name as kegiatan_mrapbs_id_name,
                c.sub_kegiatan_mrapbs_id as sub_kegiatan, c.hasil as hasil_ang,
                (   select sum(b.jum_ajuan) 
                from pengajuan_detail as b 
                where b.pengajuan_no = a.pengajuan_no
                ) as total
                "))
        ->orderby('a.pengajuan_no', 'asc');

        if ($request->s == "form")
        {   
            if ( $request->pengajuan_no )
            {   $results->where("a.pengajuan_no", "=", $request->pengajuan_no); };
        };


            if ($request->pt) {   return $results->first();   }
                else {  return $x->get(); };

                $x = $results->get();
                    // print_r(DB::getQueryLog()); exit;
            return $x;      
    } 


    public static function PengajuanMasterSearch($request, $user_id, $level)
    {
        $results = DB::table('rapbs_master as a')
        // ->join('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah
                ->leftjoin('user_anggaran as q1',function($join)
                        {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                            ->on('q1.user_id','=','a.created_by');})

            ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
            ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
            ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
            ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
            ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id') 
            ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
            ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
            ->leftjoin('m_users as x8', 'x8.user_id', '=', 'q1.user_id')
            ->select(DB::raw("a.*, 
                x1.name as organisasi_mrapbs_id_name, 
                x2.name as urusan_mrapbs_id_name, 
                x3.name as program_mrapbs_id_name, 
                x4.name as kegiatan_mrapbs_id_name,
                a.sub_kegiatan_mrapbs_id as sub_kegiatan_mrapbs_id, 
                x5.name as sumberdana_id_name, 
                x6.name as coa_id_name,
                x7.name as emp_name,
                (   select sum(b.jumlah) 
                    from rapbs_detail as b 
                    where b.rapbs_no = a.rapbs_no
                ) as total_biaya,
                (   select sum(b.jum_ajuan) 
                from pengajuan_detail as b 
                where b.rapbs_no = a.rapbs_no
            ) as sisa_biaya
            "))
            ->where("a.approve_status_3","=",1)
            // ->where("q1.user_id","=",$user_id)
        ->orderby('a.id', 'asc');

        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->organisasi_mrapbs_id_name) 
            {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->sub_kegiatan_mrapbs_id)
            {   $results->where("sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");  
            };

            if ($request->program_mrapbs_id_name)
            {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
            };
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };

            if ($level == 2) 
            {  $results->where("a.status","=",0 );}
            else
            {
                $results->where("a.created_by","=",$user_id); 
            };


        };

        return $results->get();
    }
    public static function PengajuanMasterSearchExt($request)
    {
        
        $subQuery = DB::table('m_rapbs as a')
        ->selectRaw("a.mrapbs_id, a.name,
        (a.name || ' [' || a.mrapbs_id|| '] ') as display")
        ->where('a.status', '=', 0);

        $results = PengajuanMaster::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }

    public static function PengajuanMasterSave($head, $json_data, $detail1, $site, $user_id, $company_id)
    {   // begin transaction
 //      print_r($head); exit;
        DB::beginTransaction();
        try {                 

            if ( $head->created_date== '')  
            {   
                $TahunAjaran = TahunAjaran::where('tahunajaran_id', '=', $head->tahun_ajaran_id)->first();

                $seq_no = $TahunAjaran->pengajuan_last_no + 1;
                $TahunAjaran->pengajuan_last_no = $seq_no;
                $result = $TahunAjaran->save();

                $pengajuan_no = $TahunAjaran->tahunajaran_id.'/ANG/'.$seq_no;
                $pengajuan_master = new PengajuanMaster();
                $pengajuan_master->created_date = date('Ymd');
                $pengajuan_master->created_by   = $user_id;
                $pengajuan_master->pengajuan_no = $pengajuan_no;
                $pengajuan_master->pengajuan_keterangan_id = $head->pengajuan_keterangan_id;
                $pengajuan_master->bln_pengajuan             = $pengajuan_master->makeTimeStamp($head->bln_pengajuan,'d/m/Y');
                $pengajuan_detail = new PengajuanDetail();
            } 
            else 
            {   
                $pengajuan_master = PengajuanMaster::where('id', '=', $head->id)->first();
                $pengajuan_master->modified_date = date('Ymd');
                $pengajuan_master->modified_by   = $user_id;
                $pengajuan_master->pengajuan_no  = $pengajuan_master->pengajuan_no;
                $pengajuan_master->rapbs_no      = $head->rapbs_no;

                $pengajuan_detail = PengajuanDetail::where('pengajuan_no', '=', $head->pengajuan_no)->first();
                if (Empty($pengajuan_detail)) { $pengajuan_detail = new PengajuanDetail(); };
            };
                $pengajuan_master->urusan_mrapbs_id       = $detail1->urusan_mrapbs_id;
                $pengajuan_master->organisasi_mrapbs_id   = $detail1->organisasi_mrapbs_id;
                $pengajuan_master->program_mrapbs_id      = $detail1->program_mrapbs_id;
                $pengajuan_master->kegiatan_mrapbs_id     = $detail1->kegiatan_mrapbs_id;
                $pengajuan_master->sub_kegiatan_mrapbs_id = $detail1->sub_kegiatan_mrapbs_id;
                $pengajuan_master->sumberdana_id          = $detail1->sumberdana_id;
                $pengajuan_master->hasil                  = $detail1->hasil;
                $pengajuan_master->pic_id                 = $detail1->pic_id;
                $result = $pengajuan_master->save();
            // save detail data
            if ($result) 
            {  
                $result = PengajuanDetail::PengajuanDetailSave(
                        $json_data,
                        $site,
                        $user_id,
                        $pengajuan_no);
                      
            };

            // commit transaction
            $result = [true, "Save succesfully"];
            DB::commit();
        } 
        catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function ApprovalMasterDelete($request) //hapus dari approval kabid reject
    {
        DB::beginTransaction();
        try {
            $result = PengajuanDetail::PengajuanDetailReject($request->pengajuan_no);
            if ($result)
            {   $result = static::where('pengajuan_no', '=', $request->pengajuan_no)->delete();    
            };            
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function PengajuanMasterApproval($request, $site, $user_id, $company_id)
    {
        DB::beginTransaction();
        try {
            $pengajuan_master = static::where('pengajuan_no', '=', $request->pengajuan_no)->first();  
            $pengajuan_master->approve_date_1   = date('Ymd his');
            $pengajuan_master->approve_status_1 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
            $pengajuan_master->approve_by_1     = $user_id;
            $pengajuan_master->approve_reason_1   = "Usulan Pengajuan";

            $result = $pengajuan_master->save();

            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function PengajuanMasterApprovalkabid($request, $site, $user_id, $company_id) //kegiatan / approval pengajuan
    {
        DB::beginTransaction();
        try {
            $pengajuan_master = static::where('pengajuan_no', '=', $request->pengajuan_no)->first();  
            $pengajuan_master->approve_date_2   = date('Ymd his');
            $pengajuan_master->approve_status_2 = 1; // 1 = approved, 0 = not yet approved, 2 = rejected
            $pengajuan_master->approve_by_2     = $user_id;
            $pengajuan_master->approve_reason_2   = "disetujui Oleh Kabid";

            $result = $pengajuan_master->save();

            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function PengajuanMasterApprovalKetua($request, $site, $user_id, $company_id) // approval kabid
    {
        DB::beginTransaction();
        try {
            $pengajuan_master = static::where('pengajuan_no', '=', $request->pengajuan_no)->first();  
            $pengajuan_master->approve_date_3   = date('Ymd his');
            $pengajuan_master->approve_status_3 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
            $pengajuan_master->approve_by_3     = $user_id;
            $pengajuan_master->approve_reason_3   = "Disetujui Ketua";

            $result = $pengajuan_master->save();

            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function PengajuanMasterApprovalKetua123( $json, $site, $user_id, $company_id)
    { 
        DB::beginTransaction();
        try { //print_r($json);exit;
            foreach ($json as $data) 
            {  
                if ( $data->pengajuan_no)
                 {
                    $pengajuan_master = static::where('pengajuan_no', '=', $data->pengajuan_no)->first();  
                    $pengajuan_master->approve_date_3   = date("Y-m-d H:i:s");
                    $pengajuan_master->approve_status_3 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
                    $pengajuan_master->approve_by_3     = $user_id;
                    $pengajuan_master->approve_reason_3   = "Disetujui Ketua";
                    $result = $pengajuan_master->save();
                }
                else {

                };
            };
            // commit transaction
            $result = [true, "Save succcefully"];
            DB::commit();

            }
            catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function PengajuanMasterapprovalcair( $json, $site, $user_id, $company_id)
    { 
        DB::beginTransaction();
        try { //print_r($json);exit;
            foreach ($json as $data) 
            {  
                if ( $data->pengajuan_no)
                 {
                    $pengajuan_master = static::where('pengajuan_no', '=', $data->pengajuan_no)->first();  
                    $pengajuan_master->approve_date_5   = date("Y-m-d H:i:s");
                    $pengajuan_master->approve_status_5 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
                    $pengajuan_master->approve_by_5     = $user_id;
                    $pengajuan_master->approve_reason_5   = "Dicairkan";
                    $result = $pengajuan_master->save();
                    }
                    else {

                    };
            };
            // commit transaction
            $result = [true, "Save succcefully"];
            DB::commit();

            }
            catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }


    public static function PengajuanMasterReject2($request, $site, $user_id, $company_id)
    {
        DB::beginTransaction();
        try {
            $pengajuan_master = static::where('pengajuan_no', '=', $request->pengajuan_no)->first();  
            $pengajuan_master->approve_date_2   = date('Ymd his');
            $pengajuan_master->approve_status_2 = 2; // 0 = approved, 1 = not yet approved, 2 = rejected
            $pengajuan_master->approve_by_2     = $user_id;
            $pengajuan_master->approve_reason_2   = "Test reject 2";

            $result = $pengajuan_master->save();

            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function ApbyPenetapanSearch($request)
    {
        $results = DB::table('pengajuan_master as a')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->select(DB::raw("a.*, 
                    x1.name as urusan_mrapbs_id_name, 
                    x2.name as organisasi_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name,
                    (   select sum(b.jumlah) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya
                "))
            ->where('approve_status_1','=',1)    
            ->orderby('a.id', 'asc');
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->urusan_mrapbs_id) 
            {   $results->where("urusan_mrapbs_id", "ilike", "%" . $request->urusan_mrapbs_id . "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x1.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->organisasi_mrapbs_id_name)
            {   $results->where("x1.name", "ilike", "%".$request->organisasi_mrapbs_id_name."%");  
            };
            // if ($request->status) {
            //     $results->where("status", "=", self::setStatus($request->status));
            // };
        };

        return $results->get();

        }

    public static function KegiatanSearch($request, $user_id, $level) 
    {
        $results = DB::table('pengajuan_master as a')
        // ->leftjoin('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah

        ->leftjoin('user_anggaran as q1',function($join)
                        {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                            ->on('q1.user_id','=','a.created_by')
                            ;})

                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id','=','a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id','=','a.urusan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.created_by')

                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, 
                    x2.name as urusan_mrapbs_id_name, 
                    x8.username,
                    (   select sum(b.jum_ajuan) 
                        from pengajuan_detail as b 
                        where b.pengajuan_no = a.pengajuan_no
                    ) as total_biaya
                "))
            //    ->where("q1.user_id","=",$user_id)
            ->orderby('a.id', 'asc');

        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->keterangan)
            {   $results->where("keterangan", "ilike", "%".$request->keterangan."%");  
            };
            if ($request->organisasi_mrapbs_id_name)
            {   $results->where("x1.name", "ilike", "%".$request->organisasi_mrapbs_id_name."%");  
            };

            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($level == 2) 
            {$results->where("a.status", "=" ,0) ;}
            else 
            {$results->where("a.created_by","=",$user_id) ;}; //admin level
            

            if ($request->pt) {   return $results->first();   }
            else {  return $results->get(); };
        };
        return $results->get();
    }

    public static function ApprovalKabidSearch($request, $user_id, $level)
        {
            $results = DB::table('user_anggaran as q1')
                    ->join('pengajuan_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                    ->leftjoin('m_coa as x2', 'x2.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x3', 'x3.emp_id', '=', 'a.pic_id')
                    ->leftjoin('m_users as x4', 'x4.user_id', '=', 'a.created_by')
                    ->leftjoin('m_users as x7', 'x7.user_id', '=', 'a.approve_by_2')
                    ->leftjoin('m_users as x6', 'x6.user_id', '=', 'a.approve_by_3')
                    ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.approve_by_4')
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name,
                        x4.username as user,
                        x7.username as userpengajuan,
                        x6.username as kabid,
                        x8.username as ketua,
                        (   select sum(b.jum_ajuan) 
                            from pengajuan_detail as b 
                            where b.pengajuan_no = a.pengajuan_no
                        ) as total_biaya
                    "))
                    ->where('a.approve_status_1','=',1)
                    ->where("q1.user_id","=",$user_id)
                ->orderby('a.id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->pengajuan_no) 
                {   $results->where("pengajuan_no", "ilike", "%" . $request->pengajuan_no . "%");
                };
                if ($request->pengajuan_keterangan_id)
                {   $results->where("pengajuan_keterangan_id", "ilike", "%".$request->pengajuan_keterangan_id."%");  
                };

                if ($level == 2 ) //admin level
                {
                    $results->where('a.status', '=', 0);
                };

                // if ($request->status) {
                //     $results->where("status", "=", self::setStatus($request->status));
                // };
            };
            return $results->get();
            }

        public static function ApprovalKetuaSearch($request, $user_id, $level)
        {
            $results = DB::table('user_anggaran as q1')
                    ->join('pengajuan_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                    ->leftjoin('m_coa as x2', 'x2.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x3', 'x3.emp_id', '=', 'a.pic_id')
                    ->leftjoin('m_users as x4', 'x4.user_id', '=', 'a.created_by')
                    ->leftjoin('m_users as x7', 'x7.user_id', '=', 'a.approve_by_3')
                    ->leftjoin('m_users as x6', 'x6.user_id', '=', 'a.approve_by_2')
                    ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.approve_by_4')
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name,
                        x4.username as user, 
                        x7.username as userpengajuan,
                        x6.username as kabid,
                        x8.username as ketua,
                        (   select sum(b.jum_ajuan) 
                            from pengajuan_detail as b 
                            where b.pengajuan_no = a.pengajuan_no
                        ) as total_biaya
                    "))
                    ->where('a.approve_status_2','=',1)
                    ->where("q1.user_id","=",$user_id)
                ->orderby('a.id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->pengajuan_no) 
                    {   $results->where("pengajuan_no", "ilike", "%" . $request->pengajuan_no . "%");
                    };
                if ($request->pengajuan_keterangan_id)
                    {   $results->where("pengajuan_keterangan_id", "ilike", "%".$request->pengajuan_keterangan_id."%");  
                    };

                if ($level == 2 ) //admin level
                    {
                    $results->where('a.status', '=', 0);
                    };
    
                    // if ($request->status) {
                //     $results->where("status", "=", self::setStatus($request->status));
                // };
            };
            return $results->get();
        }
            
    public static function ApprovalPengajuanDetailSearch($request) // mode pencarian dari daftar pengajuan (view kegiatan)
    {  
        $results = DB::table('pengajuan_detail as a')
        ->leftjoin('pengajuan_master as q1', 'q1.pengajuan_no', '=','a.pengajuan_no')
        ->leftjoin('rapbs_master as c', 'c.rapbs_no', '=','a.rapbs_no')
        ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'q1.urusan_mrapbs_id')
        ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'q1.organisasi_mrapbs_id')
        ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'q1.program_mrapbs_id')
        ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'q1.kegiatan_mrapbs_id')
        // ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'q1.sumberdana_id')

        ->select(DB::raw("a.rapbs_no, a.approve_status, a.pengajuan_no,
                q1.urusan_mrapbs_id,  
                q1.organisasi_mrapbs_id,
                q1.program_mrapbs_id,
                q1.kegiatan_mrapbs_id,
                x1.name as urusan_mrapbs_id_name,
                x2.name as organisasi_mrapbs_id_name,
                x3.name as program_mrapbs_id_name,
                x4.name as kegiatan_mrapbs_id_name,
                c.sub_kegiatan_mrapbs_id,c.hasil,
                (   select sum(b.jum_ajuan) 
                from pengajuan_detail as b 
                where b.rapbs_no = a.rapbs_no AND b.pengajuan_no = a.pengajuan_no
                ) as total
                "))
        ->groupby('a.rapbs_no', 'a.pengajuan_no','a.approve_status',
                'x1.name','q1.urusan_mrapbs_id', 
                'x2.name','q1.organisasi_mrapbs_id',
                'x3.name','q1.program_mrapbs_id',
                'x4.name','q1.kegiatan_mrapbs_id',
                'c.sub_kegiatan_mrapbs_id','c.hasil'
                )
        ->orderby('a.rapbs_no', 'asc');

    // searchForm parameter
        if ($request->s == "forms") 
        {
            if ($request->pengajuan_no) 
            {   $results->where("a.pengajuan_no", "ilike", "%" . $request->pengajuan_no . "%");
            };
            if ($request->rapbs_no) 
            {   $results->where("rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };
            if ($request->organisasi_mrapbs_id_name)  
            {   $results->where("organisasi_mrapbs_id_name", "ilike", "%" . $request->organisasi_mrapbs_id_name . "%");
            };

            if ($request->pt) {   return $results->first();   }
            else {  return $results->get(); };
    
        };
        return $results->get();
    }

    public static function KegiatanPrintOut($request)
    {  
        $results = DB::table('pengajuan_detail as a')
        ->join('rapbs_master as q1', 'q1.rapbs_no', '=','a.rapbs_no')
        ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'q1.organisasi_mrapbs_id')
        ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'q1.urusan_mrapbs_id')
        ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'q1.program_mrapbs_id')
        ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'q1.kegiatan_mrapbs_id')
        ->select(DB::raw("a.rapbs_no, a.pengajuan_no,
                q1.organisasi_mrapbs_id,  
                q1.urusan_mrapbs_id,
                q1.program_mrapbs_id,
                q1.kegiatan_mrapbs_id,
                x1.name as urusan_mrapbs_id_name,
                x2.name as organisasi_mrapbs_id_name,
                x3.name as program_mrapbs_id_name,
                x4.name as kegiatan_mrapbs_id_name,
                sub_kegiatan_mrapbs_id, hasil,
                (   select sum(b.jum_ajuan) 
                from pengajuan_detail as b 
                where b.pengajuan_no = a.pengajuan_no
                ) as total_biaya
                "))
        ->groupby('a.pengajuan_no','a.rapbs_no',
                'x1.name','q1.organisasi_mrapbs_id',
                'x2.name','q1.urusan_mrapbs_id',
                'x3.name','q1.program_mrapbs_id',
                'x4.name','q1.kegiatan_mrapbs_id',
                'sub_kegiatan_mrapbs_id','hasil'
                )
        ->orderby('a.rapbs_no', 'asc');

    // searchForm parameter
    if ($request->s == "forms") 
    {
        if ($request->pengajuan_no) 
        {   $results->where("pengajuan_no", "ilike", "%" . $request->pengajuan_no . "%");
        };
        if ($request->rapbs_no) 
        {   $results->where("rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
        };
    }
        if ($request->pt) {   return $results->first();   }
            else {  return $results->get(); };
    
        return $results->get();
    } 
    
    public static function ApprovalKetuaPengajuanDetailSearch($request)
    {   
        $results = DB::table('pengajuan_detail as a')
        ->leftjoin('pengajuan_master as q1', 'q1.pengajuan_no', '=','a.pengajuan_no')
        ->leftjoin('rapbs_master as c', 'c.rapbs_no', '=','a.rapbs_no')
        ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'q1.urusan_mrapbs_id')
        ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'q1.organisasi_mrapbs_id')
        ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'q1.program_mrapbs_id')
        ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'q1.kegiatan_mrapbs_id')
        // ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'q1.sumberdana_id')

        ->select(DB::raw("a.rapbs_no, a.approve_status, a.pengajuan_no,
                q1.urusan_mrapbs_id,  
                q1.organisasi_mrapbs_id,
                q1.program_mrapbs_id,
                q1.kegiatan_mrapbs_id,
                x1.name as urusan_mrapbs_id_name,
                x2.name as organisasi_mrapbs_id_name,
                x3.name as program_mrapbs_id_name,
                x4.name as kegiatan_mrapbs_id_name,
                c.sub_kegiatan_mrapbs_id,c.hasil,
                (   select sum(b.jum_ajuan) 
                from pengajuan_detail as b 
                where b.rapbs_no = a.rapbs_no AND b.pengajuan_no = a.pengajuan_no
                ) as total
                "))
        ->groupby('a.rapbs_no', 'a.pengajuan_no','a.approve_status',
                'x1.name','q1.urusan_mrapbs_id', 
                'x2.name','q1.organisasi_mrapbs_id',
                'x3.name','q1.program_mrapbs_id',
                'x4.name','q1.kegiatan_mrapbs_id',
                'c.sub_kegiatan_mrapbs_id','c.hasil'
                )
//        ->where('a.pengajuan_no','=',$request->pengajuan_no)
        ->orderby('a.pengajuan_no', 'asc');

    // searchForm parameter
        if ($request->s == "forms") 
        {
            if ($request->pengajuan_no) 
            {   $results->where("a.pengajuan_no", "ilike", "%" . $request->pengajuan_no. "%");
            };
        };
        return $results->get();
    }    

   

    public static function ApprovalSearch($request)
    {
        $results = DB::table('pengajuan_master as a')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->select(DB::raw("a.*, 
                    x1.name as urusan_mrapbs_id_name, 
                    x2.name as organisasi_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name,
                    (   select sum(b.jum_ajuan) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya
                "))
            ->where('approve_status_3','=',0)    
            ->orderby('a.id', 'asc');
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->urusan_mrapbs_id) 
            {   $results->where("urusan_mrapbs_id", "ilike", "%" . $request->urusan_mrapbs_id . "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x1.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->organisasi_mrapbs_id_name)
            {   $results->where("x1.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
                    // if ($request->status) {
            //     $results->where("status", "=", self::setStatus($request->status));
            // };
        };

        return $results->get();

        }

    public static function MonitoringSearch($request)
    {
        $results = DB::table('pengajuan_master as a')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->select(DB::raw("a.*, 
                    x1.name as urusan_mrapbs_id_name, 
                    x2.name as organisasi_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name,
                    (   select sum(b.jum_ajuan) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya
                "))
                
            ->orderby('a.id', 'asc');
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->urusan_mrapbs_id) 
            {   $results->where("urusan_mrapbs_id", "ilike", "%" . $request->urusan_mrapbs_id . "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x1.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->organisasi_mrapbs_id_name)
            {   $results->where("x2.name", "ilike", "%".$request->organisasi_mrapbs_id_name."%");  
            };
            if ($request->program_mrapbs_id_name)
            {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
            };
            if ($request->rapbs_no) 
            {   $results->where("rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };

        };

        return $results->get();

        }

    public static function RapbsRekapSearch($request)
    {
        $subQuery = DB::table('pengajuan_master as a')
                        ->join('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                        ->select(DB::raw("a.urusan_mrapbs_id, 
                            x1.name as urusan_mrapbs_id_name,  
                            (   select sum(b.jumlah) 
                                from rapbs_detail as b  
                                where b.rapbs_no = a.rapbs_no
                            ) as total_biaya 
                        "))
        ->where('approve_status_3','=',1)
        ->orderby('x1.mrapbs_id', 'asc');

        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->urusan_mrapbs_id) 
            {   $results->where("a.urusan_mrapbs_id", "ilike", "%" . $request->urusan_mrapbs_id . "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x1.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };
        };

        $results = PengajuanMaster::selectRaw("x.urusan_mrapbs_id, x.urusan_mrapbs_id_name, 
                        sum(x.total_biaya) as total_biaya ")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->groupby('x.urusan_mrapbs_id', 'x.urusan_mrapbs_id_name')
            ->orderby('x.urusan_mrapbs_id', 'asc');

        return $results->get();

        }


        public static function PengajuanMasterDelete($request) // reject pengajuan
    {
        DB::beginTransaction(); 
        try {
            $result = PengajuanDetail::PengajuanDetailReject($request->pengajuan_no);
            if ($result)
            {   $result = static::where('pengajuan_no', '=', $request->pengajuan_no)->delete();    
            };            
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    // public function Pk_pengajuan_master()
    // {
    //     return $this->hasMany('App\Models\pengajuan_master', 'id');
    // }

 //    public function fkMrapbs(){
	//     return $this->belongsTo('\App\Http\Models\Mrapbs', 'urusan_mrapbs_id', 'mrapbs_id');
	// }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
