<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use App\Models\Anggaran\RapbsDetail;
use App\Models\Anggaran\RapbscopyMaster;
use App\Models\Anggaran\RapbsMaster;
use DB;

class RapbscopyDetail extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/

    /*
    -- Drop table

    -- DROP TABLE public.temp_rapbs_detail;

    CREATE TABLE public.temp_rapbs_detail (
        id numeric(18) NOT NULL,
        rapbs_no varchar(50) NOT NULL,
        coa_id varchar(8) NULL,
        coa_name varchar(100) NULL,
        uraian varchar(256) NULL,
        volume numeric(11,2) NULL,
        tarif numeric(18,2) NULL,
        satuan varchar(20) NULL,
        jumlah numeric(20,2) NULL DEFAULT 0,
        status numeric(1) NULL DEFAULT 0,
        created_date timestamp NOT NULL DEFAULT now(),
        modified_date timestamp NULL DEFAULT now(),
        berulang numeric(5,2) NULL,
        remark varchar(255) NULL,
        created_by numeric(18) NULL,
        CONSTRAINT temp_rapbs_detail_pk PRIMARY KEY (id)
    );

     *//*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'temp_rapbs_detail';
	protected $primaryKey = 'id';
    // public $sequence_name = 'rapbs_detail_seq';
    
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
        */
    public static function RapbscopyDetailSearch($request)
    {   
        $results = DB::table('rapbs_detail as a')  
                ->leftjoin('rapbs_master as b','b.rapbs_no','=','a.rapbs_no') 
                ->select(DB::raw("a.*, '' as action")) 
                ->orderby('a.id', 'asc');
        
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no",'=', $request->rapbs_no);
            };

            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };
        };
        return $results->get();
    }

    public static function RapbstempDetailSearch($request)
    {   
        $results = DB::table('temp_rapbs_detail as a')  
                ->leftjoin('temp_rapbs_master as b','b.rapbs_no','=','a.rapbs_no') 
                ->select(DB::raw("a.*, '' as action")) 
                ->orderby('a.id', 'asc');
        
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no",'=', $request->rapbs_no);
            };

            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };
        };
        return $results->get();
    }

    public static function CopyDetailSave($json_data, $site, $user_id, $rapbs_no)
    {        DB::enableQueryLog(); 
      //  dd($rapbs_no);
        $Detail = DB::table('rapbs_detail as a')
        ->select(DB::raw("a.rapbs_no, a.coa_id, a.coa_name, a.uraian, a.volume, a.satuan, a.tarif, a.jumlah,a.created_date ,a.remark"))
        ->where('a.rapbs_no','=' ,$rapbs_no)->get();
        
            if (count($Detail) > 0) { 
            foreach ($Detail as $json) 
                {  // save hardware
                    $rapbscopydetail = new RapbscopyDetail();
                    $rapbscopydetail->rapbs_no   = trim($rapbs_no);
                    $rapbscopydetail->coa_id     = $json->coa_id;
                    $rapbscopydetail->coa_name   = $json->coa_name;
                    $rapbscopydetail->volume     = $json->volume;
                    $rapbscopydetail->uraian     = $json->uraian;
                    $rapbscopydetail->tarif      = $json->tarif;
                    $rapbscopydetail->satuan     = $json->satuan;
                    $rapbscopydetail->jumlah     = $json->jumlah;
                    $result = $rapbscopydetail->save();
                };
            };
            return $result;
        }

        public static function RapbsDetailtempSave($json_data, $site, $user_id, $rapbs_no)
        {        DB::enableQueryLog(); 
               if (count($json_data) > 0)
            {   // transaction control ikut parent orders
                foreach ($json_data as $json) 
                {   // save hardware
                    if (trim($json->remark) == 'REMOVED')
                    {   $result = static::where('id', '=', $json->id)->delete();
                        $result = [true, "Save succcefully"];
                    }
                    else
                    {   // save record
                        if ($json->created_date == '')  
                        {   $rapbscopydetail = new RapbscopyDetail();
                            $rapbscopydetail->rapbs_no   = trim($rapbs_no);
                        }
                        else 
                        {   // update hardware
                            $rapbscopydetail = RapbscopyDetail::where('id', '=', $json->id)->first();
                            $rapbscopydetail->modified_date = date('Ymd');
                        };
                        $rapbscopydetail->coa_id   = $json->coa_id;
                        $rapbscopydetail->coa_name = $json->coa_name;
                        $rapbscopydetail->uraian   = trim($json->uraian);
                        $rapbscopydetail->volume   = $json->volume;
                        $rapbscopydetail->tarif    = $json->tarif;
                        $rapbscopydetail->satuan   = $json->satuan;
                        $rapbscopydetail->jumlah   = $json->jumlah;
                        $rapbscopydetail->remark   = $json->remark;
                        $rapbscopydetail->berulang = $json->berulang;
                        $result = $rapbscopydetail->save();
                    };
                };
            };
        }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentRapbscopyMaster()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Anggaran\RapbscopyMaster', 'rapbs_no', 'rapbs_no');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
