<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use DB;

class PengajuanReject extends AppModel
{   /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table      = 'pengajuan_reject';
    protected $primaryKey = 'id';
    //public $sequence_name = 'pengajuan_reject_seq'; 
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
  
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */


    public static function RejectPengajuan( $catatan, $head_data, $site, $user_id, $company_id) 
    {
     if (count($head_data) > 0)
        { 
        DB::beginTransaction();
        try 
          {// print_r($head_data);exit;
            foreach ($head_data as $json)
             {

                $PengajuanReject = new PengajuanReject();

                    $PengajuanReject->created_date = date('Ymd');
                    $PengajuanReject->pengajuan_no            = $json->pengajuan_no;
                    $PengajuanReject->rapbs_no                = $json->rapbs_no;
                    $PengajuanReject->organisasi_mrapbs_id    = $json->organisasi_mrapbs_id ;
                    $PengajuanReject->urusan_mrapbs_id        = $json->urusan_mrapbs_id;
                    $PengajuanReject->program_mrapbs_id       = $json->program_mrapbs_id;
                    $PengajuanReject->kegiatan_mrapbs_id      = $json->kegiatan_mrapbs_id;
                    $PengajuanReject->sub_kegiatan_mrapbs_id  = $json->sub_kegiatan_mrapbs_id;
                    $PengajuanReject->sumberdana_id           = $json->sumberdana_id;  
                    $PengajuanReject->hasil                   = $json->hasil; 
                    $PengajuanReject->total_reject            = $json->total_biaya;  
                    $PengajuanReject->id_pengajuan            = $json->id;
                    $PengajuanReject->tahun_ajaran_id         = $json->tahun_ajaran_id;
                    $PengajuanReject->reject_date_1           = date('Ymd');
                    $PengajuanReject->reject_by_1             = $user_id;
                    $PengajuanReject->reject_reason_1         = $catatan;
                    $result =  $PengajuanReject->save();

                    // commit transaction
                    $result = [true, "Save succcefully"];
                    DB::commit();
                }
            } 
                catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        
            return $result;  
        }    
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    // public function Pk_Uangmuka_master()
    // {
    //     return $this->hasMany('App\Models\Uangmuka_master', 'id');
    // }

 //    public function fkMrapbs(){
	//     return $this->belongsTo('\App\Http\Models\Mrapbs', 'urusan_mrapbs_id', 'mrapbs_id');
	// }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
