<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use DB;

class UangmukaDetail extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/
    // create sequence Uangmuka_detail_seq;
    // CREATE TABLE public.Uangmuka_detail
    // (
    //   id numeric(18,0) NOT NULL DEFAULT nextval('Uangmuka_detail_seq'::regclass),
    //   rapbs_no character varying(20) NOT NULL,
    //   coa_id character varying(8),
    //   coa_name character varying(100),
    //   uraian character varying(256),
    //   volume numeric(5,2),
    //   tarif numeric(15,2),
    //   satuan character varying(20),
    //   jumlah numeric(15,2) DEFAULT 0,
    //   berulang numeric(5,2) DEFAULT 0,
    //   status numeric(1,0) DEFAULT 0,
    //   created_date timestamp without time zone NOT NULL DEFAULT now(),
    //   modified_date timestamp without time zone DEFAULT now(),
    //   CONSTRAINT Uangmuka_detail_pk PRIMARY KEY (rapbs_no, id),
    //   CONSTRAINT rapbsmaster1_fk FOREIGN KEY (rapbs_no) references rapbs_master(rapbs_no)
    // );
    /*
    
     *//*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'uangmuka_detail';
	protected $primaryKey = 'id';
    public $sequence_name = 'uangmuka_detail_seq';
    
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function detailPrintOut($uangmuka_no)
    {   $results = DB::table('uangmuka_detail as a')
            ->leftjoin('uangmuka_master as x1', 'x1.uangmuka_no', '=', 'a.uangmuka_no')
            ->select(DB::raw("
                a.pengajuan_no, a.rapbs_no, a.coa_id, a.coa_name, a.uraian, a.vol_ajuan, 
                a.tarif, a.satuan, a.jum_ajuan,
                (   select sum(b.jum_ajuan) 
                from uangmuka_detail as b 
                where b.uangmuka_no = a.uangmuka_no
                ) as total_biaya
                "))
            ->where("x1.uangmuka_no", "=", $uangmuka_no)
            ->orderby('x1.uangmuka_no');

        return $results->get();
    }

    public static function realisasidetailPrintOut($uangmuka_no)
    {   $results = DB::table('uangmuka_detail as a')
            ->leftjoin('uangmuka_master as x1', 'x1.uangmuka_no', '=', 'a.uangmuka_no')
            // ->leftjoin('uangmuka_nota as x2','x2.biaya_id','=','a.id')
            ->select(DB::raw("
                a.pengajuan_no, a.rapbs_no, a.coa_id, a.coa_name, a.uraian, a.vol_ajuan, 
                a.tarif, a.satuan, a.jum_ajuan, a.correction_acc ,a.correction_name,
                (   select sum(q.jumlah_nota) 
                from uangmuka_nota as q 
                where q.biaya_id = a.id
                ) as total_nota ,
                (   select sum(x.volume_nota) 
                from uangmuka_nota as x 
                where x.biaya_id = a.id
                ) as total_vol ,
                (   select sum(b.jum_ajuan) 
                from uangmuka_detail as b 
                where b.uangmuka_no = a.uangmuka_no
                ) as total_biaya 
                "))
            ->where("x1.uangmuka_no", "=", $uangmuka_no)
            ->orderby('x1.uangmuka_no');

        return $results->get();
    }

    public static function UangmukaDetailSearch($request)
    {   //$results = static::orderby('id', 'asc');  
        //return $results->get();


        $results = DB::table('pengajuan_detail as a')
                ->select(DB::raw("a.*, '' as action"))
            ->orderby('a.id', 'asc');
        
        // searchForm parameter
        if ($request->s == "forms") 
        {
            if ($request->pengajuan_no) 
            {   $results->where("pengajuan_no", "ilike", "%" . $request->pengajuan_no . "%");
            };

            if ($request->rapbs_no) 
            {   $results->where("rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };

        };

        return $results->get();
    }

    public static function UangmukaRinciSearch($request)
    {   //$results = static::orderby('id', 'asc');  
        //return $results->get();


        $results = DB::table('uangmuka_detail as a')
                ->select(DB::raw("a.*, '' as action"))
            ->orderby('a.id', 'asc');
        
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->uangmuka_no) 
            { //  $results->where("uangmuka_no", "ilike", "%" . $request->uangmuka_no. "%");
                $results->where("uangmuka_no", "=", $request->uangmuka_no);
            };

        };

        return $results->get();
    }

    public static function UangmukaRinciMonitoringSearch($request)
    {   //$results = static::orderby('id', 'asc');  
        //return $results->get();


        $results = DB::table('uangmuka_detail as a')
                ->select(DB::raw("a.*, '' as action"))
            ->orderby('a.id', 'asc');
        
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->urusan_mrapbs_id) 
            {   $results->where("urusan_mrapbs_id", "ilike", "%" . $request->urusan_mrapbs_id. "%");
            };

        };

        return $results->get();
    }


    public static function KegiatanDetailSearch($request)
    { 

        $results = DB::table('Uangmuka_detail as a')
                ->select(DB::raw("a.*, '' as action"))
            ->orderby('a.id', 'asc');
        
        // searchForm parameter
        if ($request->s == "forms") 
        {
            if ($request->Uangmuka_no) 
            {   $results->where("Uangmuka_no", "ilike", "%" . $request->Uangmuka_no . "%");
            };

            if ($request->rapbs_no) 
            {   $results->where("rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };

        };

        return $results->get();
    }

    public static function UangmukaDetailDelete($uangmuka_no)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('uangmuka_no', '=', $uangmuka_no)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    public static function UangmukaDetailSave( $biaya, $site, $user_id, $uangmuka_no,$detail1)

    { //print_r($json_data);exit;
        // if (Empty($json_data->created_date))
        if (count($biaya) > 0)
        {   // transaction control ikut parent orders
            foreach ($biaya as $json) 
            {   // save hardware
                if ($json->uangmuka_no == '')
                {   $UangmukaDetail = new UangmukaDetail();
                    $UangmukaDetail->uangmuka_no   = trim($uangmuka_no);
                }
                else 
                {  
                    $UangmukaDetail = UangmukaDetail::where('id', '=', $json->id)->first();
                    $UangmukaDetail->modified_date = Date('d/m/Y');
                };                        
                        $UangmukaDetail->pengajuan_no = $json->pengajuan_no;
                        $UangmukaDetail->rapbs_no = $json->rapbs_no;
                        $UangmukaDetail->coa_id   = $json->coa_id;
                        $UangmukaDetail->coa_name = $json->coa_name;
                        $UangmukaDetail->uraian   = $json->uraian;
                        $UangmukaDetail->volume   = $json->volume;
                        $UangmukaDetail->vol_sisa   = $json->vol_sisa;  //entri waktu apby:pengajuan apby
                        $UangmukaDetail->vol_ajuan   = $json->vol_ajuan; //entri waktu apby:pengajuan apby
                        $UangmukaDetail->tarif    = $json->tarif;
                        $UangmukaDetail->satuan   = $json->satuan;
                        $UangmukaDetail->jumlah   = $json->jumlah;
                        $UangmukaDetail->jum_ajuan   = $json->jum_ajuan; //entri waktu apby:pengajuan apby
                        $UangmukaDetail->berulang = $json->berulang;
                        $UangmukaDetail->urusan_mrapbs_id   = $detail1[0]->urusan_mrapbs_id;
                        $result = $UangmukaDetail->save();
                };
            };
        return $result;
    }

    public static function KegiatanReject($rapbs_no,$txt)
    { //  print_r($results); exit;
        DB::beginTransaction();
        try { 
            $results = static::where('rapbs_no','=',$rapbs_no)
            ->orderby('rapbs_no','asc')
            // ->where('approve_date', null)
            ->get();

            if ($results)
            { // print_r($results); exit; 
                foreach ($results as $result) {
                    //  print_r($result); exit;
                    $Uangmuka_detail = UangmukaDetail::where("approve_date", "=", $result->approve_date)
                    ->first();  
                    $Uangmuka_detail->approve_date   = date('Ymd his');
                    $Uangmuka_detail->approve_status = 2; // 0 = approved, 1 = not yet approved, 2 = rejected
                    $Uangmuka_detail->approve_reason   = $txt;
                    // print_r($Uangmuka_detail); exit;
                    $Uangmuka_detail->save();
        
                    }
                $result = [true, "Save succcefully"];
                DB::commit();

                }
            else
            {   $result = [false, "No Data Found"];
                DB::rollback();
            };          
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }


    public static function KegiatanReject1($rapbs_no, $txt)
    { //  print_r($txt); exit;
        DB::beginTransaction();
        try {
            $Uangmuka_detail = UangmukaDetail::where("rapbs_no", "=", $rapbs_no)
            ->first();  
            $Uangmuka_detail->approve_date   = date('Ymd his');
            $Uangmuka_detail->approve_status = 2; // 0 = approved, 1 = not yet approved, 2 = rejected
            $Uangmuka_detail->approve_reason   = $txt;

            $result = $Uangmuka_detail->save();

            $result = [true, "Save succcefully"];
            DB::commit();
        } 
        catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function KoreksiAcc($id, $koreksiacc,$koreksiname, $coa_id, $user_id)
    {
        DB::beginTransaction();
        try { //print_r($koreksiname); exit;
            $results = static::where('id','=',$id)
            ->orderby('id','asc')
            ->get();
            if ($results)
            { // print_r($koreksiname); exit; 
                foreach ($results as $result) {
                    //  print_r($result); exit;
                    $uangmuka_detail = UangmukaDetail::where("id", "=", $id)->first();
                    $uangmuka_detail->correction_date  = date('Ymd his');
                    $uangmuka_detail->correction_acc   = $koreksiacc;
                    $uangmuka_detail->correction_name  = $koreksiname;
                    $uangmuka_detail->correction_by    = $user_id;

                    $uangmuka_detail->save();
                    }
                $result = [true, "Save succcefully"];
                DB::commit();
                }
            else
            {   $result = [false, "No Data Found"];
                DB::rollback();
            };          
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function NotaTotalSave($json, $head_data, $total)
    { //  print_r($results); exit;
        DB::beginTransaction();
        try { 
            $results = static::where('uangmuka_no','=',$json->uangmuka_no)
            ->orderby('uangmuka_no','asc')
            ->get();
            if ($results)
            { // print_r($results); exit; 
                foreach ($results as $result) {
                    //  print_r($result); exit;
                    $uangmuka_detail = UangmukaDetail::where("id", "=", $head_data)->first();
                    $uangmuka_detail->realisasi_date   = date('Ymd his');
                    $uangmuka_detail->realisasi_biaya   = $total;
                    $uangmuka_detail->save();
                    }
                $result = [true, "Save succcefully"];
                DB::commit();
                }
            else
            {   $result = [false, "No Data Found"];
                DB::rollback();
            };          
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    // public static function NotaTotalSave( $head_data, $total)
    // { 
    //     //print_r($total);exit;
    //         DB::beginTransaction();
    //         try { 
    //             $uangmuka_detail = UangmukaDetail::where("id", "=", $head_data)->first();  
    //             $uangmuka_detail->realisasi_date   = Date('d/m/Y');
    //             $uangmuka_detail->realisasi_biaya = $total;
    //            // print_r($uangmuka_detail->realisasi_biaya);exit;
    //             $result = $uangmuka_detail->save();

    //             $result = [true, "Save succcefully"];
    //             DB::commit();
    //         } catch (\Illuminate\Database\QueryException $e) {
    //             // rollback transaction
    //             $result = [false, "Error Executed ---> " . $e];
    //             DB::rollback();
    //         };
    //         return $result;
    //     }  
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
