<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use App\Models\Administrator\Mincome;
use App\Models\Administrator\MUser;
use App\Models\Ppdb\TahunAjaran;
use App\Models\Anggaran\IncomeDetail;

use App\Models\Administrator\RoleUser;
use App\User;
use Illuminate\Support\Facades\Hash;

use DB;

class IncomeMaster extends AppModel
{   /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/

    /*
    -- Table: public.income_master

    -- DROP TABLE public.income_master;

    CREATE TABLE public.income_master
    (
    id numeric(18,0) NOT NULL DEFAULT nextval('income_master_seq'::regclass),
    income_master_no character varying(32) COLLATE pg_catalog."default" NOT NULL,
    tahun_ajaran_id numeric(4,0),
    income_description_name character varying(255) COLLATE pg_catalog."default",
    income_total_amount numeric(30,2) DEFAULT 0,
    status numeric(1,0) DEFAULT 0,
    created_date timestamp without time zone NOT NULL DEFAULT now(),
    modified_date timestamp without time zone DEFAULT now(),
    created_by numeric(18,0),
    modified_by numeric(18,0),
    approve_date_1 timestamp without time zone,
    approve_by_1 numeric(18,0),
    approve_status_1 numeric(1,0) DEFAULT 0,
    approve_reason_1 character varying(256) COLLATE pg_catalog."default",
    approve_date_2 timestamp without time zone,
    approve_by_2 numeric(18,0),
    approve_status_2 numeric(1,0),
    approve_reason_2 character varying(256) COLLATE pg_catalog."default",
    approve_date_3 timestamp without time zone,
    approve_by_3 numeric(18,0),
    approve_status_3 numeric(1,0),
    approve_reason_3 character varying(256) COLLATE pg_catalog."default",
    CONSTRAINT income_master_pk PRIMARY KEY (income_master_no),
    CONSTRAINT income_master_ux UNIQUE (id)
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table      = 'income_master';
    protected $primaryKey = 'id';
    public $sequence_name = 'income_master_seq'; 
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function IncomeDetail()
    {   return $this->hasMany('App\Models\Anggaran\IncomeDetail', 'rapbs_no');    }

    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function headerPrintOut($request)
    {   
        $results = DB::table('user_anggaran as q1')
                ->join('income_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                ->leftjoin('m_income as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, 
                    x2.name as urusan_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name,
                    (   select sum(b.jum_ajuan) 
                        from income_detail as b 
                        where b.income_no = a.income_no
                    ) as total_biaya,
                    (   select sum(z.jumlah_nota) 
                    from income_nota as z 
                    where z.income_no = a.income_no
                    ) as total_realisasi ,
    
                    (   select sum(b.realisasi_biaya) 
                    from income_detail as b 
                    where b.income_no = a.income_no
                    ) as total_realisasi_biaya
                "))
            ->orderby('a.id', 'asc');

        if ($request->s == "form")
        {   
            if ( $request->income_no )
            {   $results->where("a.income_no", "=", $request->income_no); };
        };

        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };
    }



    public static function incomeSearch($request, $user_id, $level)
    {
        $results = DB::table('income_master as a')
                ->select(DB::raw("a.*, 
                    (   select sum(b.income_amount) 
                        from income_detail as b 
                        where b.income_master_no = a.income_master_no
                        ) as income_total_amount
                "))
                // ->where("a.created_by","=",$user_id)
            ->orderby('a.id', 'asc');

        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->income_master_no) 
            {   $results->where("income_master_no", "ilike", "%" . $request->income_master_no . "%"); 
            };
            if ($request->income_description_name)
            {   $results->where("income_description_name", "ilike", "%".$request->income_description_name."%");  
            };

            if ($level == 2 ) //admin level
            {
            $results->where('a.status', '=', 0);
            };


            if ($request->pt) {   return $results->first();   }
            else {  return $results->get(); };
        };

        return $results->get();

        }


    public static function incomeMasterSearchExt($request)
    {
        
        $subQuery = DB::table('m_rapbs as a')
        ->selectRaw("a.mrapbs_id, a.name,
        (a.name || ' [' || a.mrapbs_id|| '] ') as display")
        ->where('a.status', '=', 0);

        $results = incomeMaster::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }


    public static function incomeMasterSave($head_data, $json_data, $site, $user_id, $company_id)
    { 
        DB::beginTransaction();
        try {                 
          //  print_r($data1);exit; 
            if ($head_data->created_date == '')   
            {   
                if ($head_data->created_date == ''){

                    $TahunAjaran = TahunAjaran::where('tahunajaran_id', '=', $head_data->tahun_ajaran_id)->first();
                    $income_master_no = $head_data->income_master_no;

                    $income_master = new incomeMaster();
                    $income_master->created_date = date('Ymd');
                    $income_master->created_by   = $user_id; 
                    $income_master->income_master_no = $income_master_no;
                    $income_master->income_description_name = $head_data->income_description_name;
                    $income_master->cost_presentage = $head_data->cost_presentage;
                    
                };
            } 
            else 
            {    
                $income_master = incomeMaster::where('id', '=', $head_data->id)->first();
                $income_master->modified_date = date('Ymd');
                $income_master->modified_by   = $user_id;
                $income_master_no             = $income_master->income_master_no;
            };
                $income_master->tahun_ajaran_id         = $head_data->tahun_ajaran_id;
                $income_master->income_description_name = $head_data->income_description_name;
                $income_master->cost_presentage         = $head_data->cost_presentage;
            $result = $income_master->save();

            // save detail data
            if ($result) 
            {  
                $result = incomeDetail::incomeDetailSave(
                        $json_data, 
                        $site,
                        $user_id,
                        $income_master_no
                        ); 
            };

            // commit transaction
            $result = [true, "Save succesfully"];
            DB::commit();
        } 
        catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function incomeMasterApproval1($request, $site, $user_id, $company_id) // approval income
        {
            DB::beginTransaction();
            try {
                $income_master = static::where('income_no', '=', $request->income_no)->first();  
                $income_master->approve_date_1   = date('Ymd his');
                $income_master->approve_status_1 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
                $income_master->approve_by_1     = $user_id;
                $income_master->approve_reason_1   = "status di approve";

                $result = $income_master->save();

                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
            return $result;
        }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    // public function Pk_income_master()
    // {
    //     return $this->hasMany('App\Models\income_master', 'id');
    // }

 //    public function fkMrapbs(){
	//     return $this->belongsTo('\App\Http\Models\Mrapbs', 'urusan_mrapbs_id', 'mrapbs_id');
	// }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
