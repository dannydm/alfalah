<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use App\Models\Administrator\Mrapbs;
use App\Models\Administrator\Mprogram;
use App\Models\Administrator\SiteDocument;
use App\Models\Administrator\Company;
use App\Models\Ppdb\TahunAjaran;
use App\Models\Anggaran\RapbsDetail;
// my task
use App\Models\Administrator\MUser;
use App\Models\Administrator\RoleUser;

use App\User;
use Illuminate\Support\Facades\Hash;

use DB;

class RapbsMaster extends AppModel
{   /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/
    // CREATE SEQUENCE rapbs_master_seq;
    // CREATE TABLE public.rapbs_master
    // (
    //   id numeric(18,0) NOT NULL DEFAULT nextval('rapbs_master_seq'::regclass),
    //   rapbs_no character varying(20) NOT NULL,
    //   tahun_ajaran_id numeric(4,0),
    //   urusan_mrapbs_id numeric(18,0),
    //   organisasi_mrapbs_id numeric(18,0),
    //   program_mrapbs_id numeric(18,0),
    //   kegiatan_mrapbs_id numeric(18,0),
    //   sumberdana_id numeric(4,0),
    //   coa_id character varying(8),
    //   masukan character varying(64),
    //   tgt_masukan numeric(5,2),
    //   keluaran character varying(64),
    //   tgt_keluaran numeric(5,2),
    //   hasil character varying(64),
    //   sasaran character varying(64),
    //   pic_id character varying(40),
    //   tgl_mulai date,
    //   tgl_selesai date,
    //   jumlahn numeric(15,2) DEFAULT 0,
    //   jumlahke_n numeric(15,2) DEFAULT 0,
    //   status numeric(1,0) DEFAULT 0,
    //   created_date timestamp without time zone NOT NULL DEFAULT now(),
    //   modified_date timestamp without time zone DEFAULT now(),
    //   approve_status_1 numeric(1) default 1, // default to reject
    //   approve_date_1 timestamp,
    //   approve_by_1 numeric(18),
    //   approve_reason varchar(256),
    //   sub_kegiatan_mrapbs_id character varying(64),
    //   rapbsbiaya_total_sum numeric(18,2),
    //   rapbs_validasi numeric(1,0),
    //   created_by numeric(18,0),
    //   modified_by numeric(18,0),
    //   CONSTRAINT rapbs_master_pk PRIMARY KEY (rapbs_no),
    //   CONSTRAINT rapbs_master_ux UNIQUE (id)
    // )
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table      = 'rapbs_master';
    protected $primaryKey = 'id';
    public $sequence_name = 'rapbs_master_seq';
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function headerusulanPrintOut($request)
    { //print_r($request);exit;  
        $results = DB::table('user_anggaran as q1')
                ->join('rapbs_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, 
                    x2.name as urusan_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name,
                    (   select sum(b.jumlah) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya
                "))
            ->orderby('a.id', 'asc');

        if ($request->s == "form")
        {   
            if ( $request->rapbs_no )
            {   $results->where("a.rapbs_no", "=", $request->rapbs_no); };
        };

        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };

    }

    public static function headerOut($request)
        {   
            $results = DB::table('rapbs_master as a')
            ->join('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
            ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
            ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
            ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
            ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
            ->select(DB::raw("a.*, 
                x1.name as organisasi_mrapbs_id_name, 
                x2.name as urusan_mrapbs_id_name, 
                x3.name as program_mrapbs_id_name, 
                x4.name as kegiatan_mrapbs_id_name, 
                x5.name as sumberdana_id_name, 
                (   select sum(b.jumlah) 
                from rapbs_detail as b 
                where b.rapbs_no = a.rapbs_no
            ) as total_biaya
                "))
                ->orderby('a.id');
    
            if ($request->s == "form")
            {   
                if ( $request->rapbs_no )
                {   $results->where("a.rapbs_no", "=", $request->rapbs_no); };
            };
    
            if ($request->pt) {   return $results->first();   }
            else {  return $results->get(); };
        }
    



    public static function RapbsMasterSearch($request, $user_id, $level)
    { // print_r($level);exit;
     
        $results = DB::table('rapbs_master as a')
        // ->join('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah
                ->leftjoin('user_anggaran as q1',function($join)
                        {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                            ->on('q1.user_id','=','a.created_by');})
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, 
                    x2.name as urusan_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name,a.status as status,
                    a.keluaran,a.hasil,a.sasaran,a.sub_kegiatan_mrapbs_id,
                    (   select sum(b.jumlah) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya
                "))

            // ->where("a.approve_status_1","=","0")
 //                       ->where('a.created_by','=',$user_id)
 //           ->where('a.tahun_ajaran_id','=',$request->tahun_ajaran_id)
            ->orderby('a.id', 'asc');

        // print($results);exit;
        // searchForm parameter
        if ($request->s == "form") 
        {   

            if ($request->tahun_ajaran_id) 
            {   $results->where("a.tahun_ajaran_id", "ilike", "%" . $request->tahun_ajaran_id. "%");
            };
            if ($request->organisasi_mrapbs_id_name) 
            {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->program_mrapbs_id_name)
            {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
            };
            if ($request->kegiatan_mrapbs_id_name)
            {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
            };
            if ($request->sub_kegiatan_mrapbs_id)
            {   $results->where("a.sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");  
            };
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };

            if ($level == 2) //admin level
            {}
            else { $results->where('a.created_by','=',$user_id); }; 

        };

        return $results->get();

        }


    public static function ApprovalusulanSearch($request, $user_id, $level)
    {  //user_anggaran where user_id = $user_id join rapbs_master as a where user_anggaran.mrapbs_id = rapbs_master.urusan_mrapbs_id 
//    print($user_id);exit;
        $results = DB::table('user_anggaran as q1')
                ->join('rapbs_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, 
                    x2.name as urusan_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name,
                    a.keluaran,a.hasil,a.sasaran,a.sub_kegiatan_mrapbs_id,
                    (   select sum(b.jumlah) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya
                "))
            // ->where("a.approve_status_1","=","0")
            ->where("q1.user_id","=",$user_id)
            ->orderby('a.id', 'asc');

        // print($results);exit;
        // searchForm parameter
        if ($request->s == "form") 
        {   
            if ($request->organisasi_mrapbs_id_name) 
            {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->program_mrapbs_id_name)
            {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
            };
            if ($request->kegiatan_mrapbs_id_name)
            {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
            };
            if ($request->sub_kegiatan_mrapbs_id)
            {   $results->where("a.sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");
            };    
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };

            if ($level == 2 ) //admin level
            {
                $results->where('a.status', '=', 0);
            };

        };

        return $results->get();
    }



    public static function PenetapanSearch($request, $user_id, $level)
    {  
        $results = DB::table('user_anggaran as q1')
        ->join('rapbs_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
        ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
        ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
        ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
        ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
        ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
        ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
        ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
        ->select(DB::raw("a.*, 
            x1.name as organisasi_mrapbs_id_name, 
            x2.name as urusan_mrapbs_id_name, 
            x3.name as program_mrapbs_id_name, 
            x4.name as kegiatan_mrapbs_id_name, 
            x5.name as sumberdana_id_name, 
            x6.name as coa_id_name,
            x7.name as emp_name,a.sub_kegiatan_mrapbs_id,
            (   select sum(b.jumlah) 
                from rapbs_detail as b 
                where b.rapbs_no = a.rapbs_no
            ) as total_biaya
        "))
        ->where("a.approve_status_1","=",1)
        ->where("q1.user_id","=",$user_id)
        ->orderby('a.id', 'asc');

        // print($results);exit;
        // searchForm parameter
        if ($request->s == "form") 
        {   
            if ($request->organisasi_mrapbs_id_name) 
            {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->program_mrapbs_id_name)
            {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
            };
            if ($request->kegiatan_mrapbs_id_name)
            {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
            };
            if ($request->sub_kegiatan_mrapbs_id)
            {   $results->where("a.sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");
            };    

            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };

            if ($level == 2 ) //admin level
            {
            $results->where('a.status', '=', 0);
            };
        };
        return $results->get();
    }


    public static function RapbsMasterSearchExt($request)
    {
        
        $subQuery = DB::table('m_rapbs as a')
        // ->leftjoin('rapbs_master as b', 'a.mrapbs_id', '=', 'b.urusan_mrapbs_id')
        // ->selectRaw("a.urusan_mrapbs_id, a.mrapbs_id, 
        ->selectRaw("a.mrapbs_id, a.name,
        (a.name || ' [' || a.mrapbs_id|| '] ') as display")
        ->where('a.status', '=', 0);

        $results = RapbsMaster::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }
    public static function RapbsMasterSave($data, $json_data, $site, $user_id, $company_id)
    {   // begin transaction
//        print_r($data);exit;
        DB::beginTransaction();
        DB::enableQueryLog(); 
        try {
            
 //            print_r($data);exit;
            if ($data->created_date == '')  
            {   
                if ($data->tahun_ajaran_id == ''){} 
                else
                {    
                    // print_r($site);exit;
                 //   print_r($data->urusan_mrapbs_id);exit;
                    $company_id = "YMDT";
                    $Document = DB::select( DB::raw(" 
                        select b.*, c.doc_code 
                        from m_rapbs as a 
                        join site_documents as b on ( b.company_id = '".$company_id."' 
                            and b.site_id = '".$site."' and b.dept_id = a.dept_id ) 
                        join m_documents as c on ( c.doc_id = b.doc_id )
                        where a.mrapbs_id = '".$data->urusan_mrapbs_id."'"
                    ) );

                    // print_r(DB::getQueryLog());exit;                    
                    
                    // print_r($Document);exit;

                    $doc_code = $Document[0]->doc_code;
                    $seq_no = $Document[0]->last_doc_no + 1;
                    
                    $doc_id = $Document[0]->id;
                    $SiteDocument = SiteDocument::where('id', '=', $doc_id)->first();
                    $SiteDocument->last_doc_no = $seq_no;
                    
                    $result = $SiteDocument->save();
                    $TahunAjaran = TahunAjaran::where('tahunajaran_id', '=', $data->tahun_ajaran_id)->first(); 
                    $rapbs_no = $TahunAjaran->tahunajaran_id.'/RAB/'.trim($doc_code).'/'.$seq_no;
                    
                   // print_r($rapbs_no);exit;

                    $rapbs_master = new RapbsMaster();
                    $rapbs_master->created_date = date('Ymd'); 
                    $rapbs_master->created_by   = $user_id;
                    $rapbs_master->rapbs_no = $rapbs_no;
                };
                // print_r($company_id);exit;
            } 
            else 
            {   
                $rapbs_master = RapbsMaster::where('id', '=', $data->id)->first();
                $rapbs_master->modified_date = date('Ymd');
                $rapbs_master->modified_by   = $user_id;
                $rapbs_no                    = $rapbs_master->rapbs_no;
            };
 //         print_r($json_data);exit;
            $rapbs_master->tahun_ajaran_id        = $data->tahun_ajaran_id;
            $rapbs_master->organisasi_mrapbs_id   = $data->organisasi_mrapbs_id;
            $rapbs_master->urusan_mrapbs_id       = $data->urusan_mrapbs_id ;
            $rapbs_master->program_mrapbs_id      = $data->program_mrapbs_id;
            $rapbs_master->kegiatan_mrapbs_id     = $data->kegiatan_mrapbs_id;
            $rapbs_master->sub_kegiatan_mrapbs_id = $data->sub_kegiatan_mrapbs_id;
            $rapbs_master->sumberdana_id          = $data->sumberdana_id;
            $rapbs_master->coa_id                 = $data->coa_id;
            $rapbs_master->masukan                = $data->masukan;
            $rapbs_master->tgt_masukan            = $data->tgt_masukan;
            $rapbs_master->keluaran               = $data->keluaran;
            $rapbs_master->tgt_keluaran           = $data->tgt_keluaran;
            $rapbs_master->hasil                  = $data->hasil;
            $rapbs_master->sasaran                = $data->sasaran;
            $rapbs_master->pic_id                 = $data->pic_id; 
            // $rapbs_master->rapbs_validasi         = $data->rapbs_validasi;
            $rapbs_master->tgl_mulai              = $rapbs_master->makeTimeStamp($data->tgl_mulai,'d/m/Y');
            $rapbs_master->tgl_selesai            =  $rapbs_master->makeTimeStamp($data->tgl_selesai,'d/m/Y');
           $rapbs_master->jumlahn   = $data->jumlahn;
 //        $rapbs_master->jumlahke_n   = $data->jumlahke_n;
            $rapbs_master->status                 = $data->status;

            $result = $rapbs_master->save();

            // save detail data
            if ($result) 
            {   $result = RapbsDetail::RapbsDetailSave(
                        $json_data,
                        $site,
                        $user_id,
                        $rapbs_no);
            };

            // commit transaction
            $result = [true, "Save succesfully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
        // $x = $results->get();

        // return $x;
    }

    public static function RapbsMasterDelete($request)
    {
        DB::beginTransaction();
        try {
            $result = RapbsDetail::RapbsDetailDelete($request->rapbs_no);
            if ($result)
            {   $result = static::where('rapbs_no', '=', $request->rapbs_no)->delete();    
            };            
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }
    public static function ApproveUsulan($request, $site, $user_id, $company_id)
    {
        DB::beginTransaction();
        try {
            $rapbs_master = static::where('rapbs_no', '=', $request->rapbs_no)->first();  
            $rapbs_master->approve_date_1   = date('Ymd his');
            $rapbs_master->approve_status_1 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
            $rapbs_master->approve_by_1     = $user_id;
            $rapbs_master->approve_reason_1   = "Disetujui Oleh Kabid";

            $result = $rapbs_master->save();

            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function PenetapanApprove($request, $site, $user_id, $company_id)
    {
        DB::beginTransaction();
        try {
            $rapbs_master = static::where('rapbs_no', '=', $request->rapbs_no)->first();  
            $rapbs_master->approve_date_2   = date('Ymd his');
            $rapbs_master->approve_status_2 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
            $rapbs_master->approve_by_2     = $user_id;
            $rapbs_master->approve_reason_2   = "Disetujui Oleh Keuangan";

            $result = $rapbs_master->save();

            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function Pengesahanketua($json, $site, $user_id, $company_id)
    { 
        DB::beginTransaction();
        try { 
            foreach ($json as $data) 
            { 
                if ( $data->rapbs_no)
                 {
                    $rapbs_master = static::where('rapbs_no', '=', $data->rapbs_no)->first();  
                    $rapbs_master->approve_date_3   = date("Y-m-d H:i:s");
                    $rapbs_master->approve_status_3 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
                    $rapbs_master->approve_by_3     = $user_id;
                    $rapbs_master->approve_reason_3   = "Disahkan ketua";
                    $result = $rapbs_master->save();
                    }
                    else {

                    };
            };
            // commit transaction
            $result = [true, "Save succcefully"];
            DB::commit();

            }
            catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }
    public static function Pengesahanpembina($json, $site, $user_id, $company_id)
    { 
        DB::beginTransaction();
        try { 
            foreach ($json as $data) 
            { 
                if ( $data->rapbs_no)
                 {
                    $rapbs_master = static::where('rapbs_no', '=', $data->rapbs_no)->first();  
                    $rapbs_master->approve_date_4   = date("Y-m-d H:i:s");
                    $rapbs_master->approve_status_4 = 1; // 0 = approved, 1 = not yet approved, 2 = rejected
                    $rapbs_master->approve_by_4     = $user_id;
                    $rapbs_master->approve_reason_4   = "Disahkan Pembina";
                    $result = $rapbs_master->save();
                    }
                    else {

                    };
            };
            // commit transaction
            $result = [true, "Save succcefully"];
            DB::commit();

            }
            catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }
    public static function RapbsMasterReject2($request, $site, $user_id, $company_id)
    {
        DB::beginTransaction();
        try {
            $rapbs_master = static::where('rapbs_no', '=', $request->rapbs_no)->first();  
            $rapbs_master->approve_date_2   = date('Ymd his');
            $rapbs_master->approve_status_2 = 2; // 0 = approved, 1 = not yet approved, 2 = rejected
            $rapbs_master->approve_by_2     = $user_id;
            $rapbs_master->approve_reason_2   = "Test reject 2";

            $result = $rapbs_master->save();

            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }

    public static function ApbyPenetapanSearch($request, $user_id, $level)
    
        {  //user_anggaran where user_id = $user_id join rapbs_master as a where user_anggaran.mrapbs_id = rapbs_master.urusan_mrapbs_id 
            $results = DB::table('user_anggaran as q1')
                    ->join('rapbs_master as a','q1.mrapbs_id','=','a.urusan_mrapbs_id')
                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                    ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                    ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                    ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                    ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                    ->select(DB::raw("a.*, 
                        x1.name as urusan_mrapbs_id_name, 
                        x2.name as organisasi_mrapbs_id_name, 
                        x3.name as program_mrapbs_id_name, 
                        x4.name as kegiatan_mrapbs_id_name, 
                        x5.name as sumberdana_id_name, 
                        x6.name as coa_id_name,
                        x7.name as emp_name,
                        (   select sum(b.jumlah) 
                            from rapbs_detail as b 
                            where b.rapbs_no = a.rapbs_no
                        ) as total_biaya
                    "))
                ->where("q1.user_id","=",$user_id)
                ->orderby('a.id', 'asc');
            
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->organisasi_mrapbs_id_name) 
                {   $results->where("x2.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
                };
                if ($request->urusan_mrapbs_id_name)
                {   $results->where("x1.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
                };
                if ($request->program_mrapbs_id_name)
                {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
                };
                if ($request->rapbs_no) 
                {   $results->where("rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
                };
                if ($level == 2) //admin level
                    {}
                else { $results->where('a.created_by','=',$user_id); }; 
    
                // if ($request->status) {
                //     $results->where("status", "=", self::setStatus($request->status));
                // };
            };
            return $results->get();
        }

    public static function KegiatanSearch($request)
    {
        $results = DB::table('rapbs_master as a')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->select(DB::raw("a.*, 
                    x1.name as urusan_mrapbs_id_name, 
                    x2.name as organisasi_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name,
                    (   select sum(b.jumlah) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya
                "))
            ->where('approve_status_2','=',1)    
            ->orderby('a.id', 'asc');
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->organisasi_mrapbs_id_name) 
            {   $results->where("x2.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x1.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->program_mrapbs_id_name)
            {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
            };
            if ($request->rapbs_no) 
            {   $results->where("rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };
                // if ($request->status) {
            //     $results->where("status", "=", self::setStatus($request->status));
            // };
        };

        return $results->get();

        }

    public static function ApprovalSearch($request)
    {
        $results = DB::table('rapbs_master as a')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->select(DB::raw("a.*, 
                    x1.name as urusan_mrapbs_id_name, 
                    x2.name as organisasi_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name,
                    (   select sum(b.jumlah) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya
                "))
            ->where('approve_status_3','=',0)    
            ->orderby('a.id', 'asc');
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->organisasi_mrapbs_id_name) 
            {   $results->where("x2.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x1.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->program_mrapbs_id_name)
            {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
            };
            if ($request->rapbs_no) 
            {   $results->where("rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };
                    // if ($request->status) {
            //     $results->where("status", "=", self::setStatus($request->status));
            // };
        };

        return $results->get();

        }

    public static function MonitoringSearch($request, $user_id, $level)
    {
        DB::enableQueryLog();
        $results = DB::table('rapbs_master as a')
        // ->join('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah
                ->leftjoin('user_anggaran as q1',function($join)
                        {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                            ->on('q1.user_id','=','a.created_by');})

                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')

                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')

 
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, 
                    x2.name as urusan_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name,a.status as status,
                    a.keluaran,a.hasil,a.sasaran,a.sub_kegiatan_mrapbs_id,
                    (   select sum(b.jumlah) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya
                "))
            // ->where("a.approve_status_1","=","0")
 //                       ->where('a.created_by','=',$user_id)
            ->orderby('a.id', 'asc');

        // print($results);exit;
        // searchForm parameter
        if ($request->s == "form") 
        {   
            if ($request->organisasi_mrapbs_id_name) 
            {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->program_mrapbs_id_name)
            {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
            };
            if ($request->kegiatan_mrapbs_id_name)
            {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
            };
            if ($request->sub_kegiatan_mrapbs_id)
            {   $results->where("a.sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");  
            };
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };

            // if ($level == 2) //admin level
            // {}
            // else { $results->where('a.urusan_mrapbs_id','=',$request->urusan_mrapbs_id); }; 

        };

        return $results->get();

        }

    public static function RapbsRekapSearch($request)
    {    DB::enableQueryLog();
        $subQuery = DB::table('rapbs_master as a')
                        ->join('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                        ->select(DB::raw("a.organisasi_mrapbs_id, 
                            x1.name as organisasi_mrapbs_id_name,  
                            (   select sum(b.jumlah) 
                                from rapbs_detail as b  
                                where b.rapbs_no = a.rapbs_no
                            ) as total_biaya
                        "))
//        ->where('approve_status_1','=',1)
        ->orderby('x1.mrapbs_id', 'asc');

        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->organisasi_mrapbs_id) 
            {   $results->where("a.organisasi_mrapbs_id", "ilike", "%" . $request->urusan_mrapbs_id . "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x1.urusan", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };
        };

        $results = RapbsMaster::selectRaw("x.organisasi_mrapbs_id, x.organisasi_mrapbs_id_name,  
                        sum(x.total_biaya) as total_biaya")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->groupby('x.organisasi_mrapbs_id', 'x.organisasi_mrapbs_id_name')
            ->orderby('x.organisasi_mrapbs_id', 'asc');

            $x = $results->get();
 //             print_r(DB::getQueryLog()); exit;
            return $x;      

        }

        public static function PengesahanSearch($request, $user_id, $level)
        {  //pengesahan oleh ketua, lanjut ke pengesahan oleh pembina di incomedetail::budgetingsearch
            $results = DB::table('user_anggaran as q1')
                ->join('rapbs_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->leftjoin('m_users as x8', 'x8.user_id', '=', 'q1.user_id')
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, 
                    x2.name as urusan_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name,a.sub_kegiatan_mrapbs_id,
                    (   select sum(b.jumlah) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya
                "))
                ->where("a.approve_status_1","=",1)
                ->where("a.approve_status_2","=",1)
                ->where("a.approve_status_3","=",null)
                ->where("q1.user_id","=",$user_id)
                ->orderby('a.id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->organisasi_mrapbs_id_name) 
                {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
                };
                if ($request->urusan_mrapbs_id_name)
                {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
                };
                if ($request->program_mrapbs_id_name)
                {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
                };
                if ($request->kegiatan_mrapbs_id_name)
                {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
                };
                if ($request->sub_kegiatan_mrapbs_id)
                {   $results->where("a.sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");
                };    
                if ($request->rapbs_no) 
                {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
                };

                
                if ($request->approve_status_3) 
                {   $results->where("a.approve_status_3", "ilike", "%" . $request->approve_status_3 . "%");
                };
    
                if ($level == 2 ) //admin level
                {
                    $results->where('a.status', '=', 0);
                };
    
            };

            return $results->get();
    
        }

        public static function PembinaSearch($request, $user_id, $level)
        {  //pengesahan oleh ketua, lanjut ke pengesahan oleh pembina di incomedetail::budgetingsearch
            $results = DB::table('user_anggaran as q1')
                ->join('rapbs_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                ->leftjoin('m_users as x8', 'x8.user_id', '=', 'q1.user_id')
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, 
                    x2.name as urusan_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x6.name as coa_id_name,
                    x7.name as emp_name, a.sub_kegiatan_mrapbs_id,
                    (   select sum(b.jumlah) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya
                "))
//                ->where("a.approve_status_2","=",1)
                ->where("a.approve_status_2","=",1)
                ->where("q1.user_id","=",$user_id)
                ->orderby('a.id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->organisasi_mrapbs_id_name) 
                {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
                };
                if ($request->urusan_mrapbs_id_name)
                {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
                };
                if ($request->program_mrapbs_id_name)
                {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
                };
                if ($request->kegiatan_mrapbs_id_name)
                {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
                };
                if ($request->sub_kegiatan_mrapbs_id)
                {   $results->where("a.sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");
                };    
    
                if ($request->rapbs_no) 
                {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
                };
    
                if ($level == 2 ) //admin level
                {
                    $results->where('a.status', '=', 0);
                };
    
            };

            return $results->get();
    
        }


        public static function SahRapbsSearch($request, $user_id, $level)
        {   
            $results = DB::table('user_anggaran as q1')
                ->join('rapbs_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_users as x8', 'x8.user_id', '=', 'q1.user_id')
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, 
                    x2.name as urusan_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    sub_kegiatan_mrapbs_id,
                    (   select sum(b.jumlah) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya
                "))
                // ->where("a.approve_status_2","=",1)
                ->where("a.approve_status_3","=",1)
                ->where("q1.user_id","=",$user_id)
            ->orderby('a.id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->organisasi_mrapbs_id_name) 
                {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
                };
                if ($request->urusan_mrapbs_id_name)
                {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
                };
                if ($request->program_mrapbs_id_name)
                {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
                };
                if ($request->kegiatan_mrapbs_id_name)
                {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
                };
                if ($request->sub_kegiatan_mrapbs_id)
                {   $results->where("sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");  
                };

                if ($request->rapbs_no) 
                {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
                };
    
                if ($level == 2 ) //admin level
                {
                    $results->where('a.status', '=', 0);
                };
    
            };

            return $results->get();
    
        }

        public static function SahRapbsPrint($request)
        {   
            $results = DB::table('user_anggaran as q1')
                ->join('rapbs_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_users as x8', 'x8.user_id', '=', 'q1.user_id')
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, 
                    x2.name as urusan_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    sub_kegiatan_mrapbs_id,
                    (   select sum(b.jumlah) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya
                "))
                // ->where("a.approve_status_2","=",1)
                ->where("a.approve_status_3","=",1)
                ->orderby('a.organisasi_mrapbs_id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->organisasi_mrapbs_id_name) 
                {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
                };
                if ($request->urusan_mrapbs_id_name)
                {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
                };
                if ($request->program_mrapbs_id_name)
                {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
                };
                if ($request->kegiatan_mrapbs_id_name)
                {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
                };
                if ($request->sub_kegiatan_mrapbs_id)
                {   $results->where("sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");  
                };

                if ($request->rapbs_no) 
                {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
                };
    
            };

            if ($request->pt) {   
                
                if ($request->urusan_mrapbs_id_name)
                {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
                };

                if ($request->organisasi_mrapbs_id_name) 
                {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
                };
                if ($request->urusan_mrapbs_id_name)
                {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
                };
                if ($request->program_mrapbs_id_name)
                {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
                };
                if ($request->kegiatan_mrapbs_id_name)
                {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
                };
                if ($request->sub_kegiatan_mrapbs_id)
                {   $results->where("sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");  
                };
            }
            else
            {  return $results->get(); };
    
        }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    // public function Pk_rapbs_master()
    // {
    //     return $this->hasMany('App\Models\rapbs_master', 'id');
    // }

 //    public function fkMrapbs(){
	//     return $this->belongsTo('\App\Http\Models\Mrapbs', 'urusan_mrapbs_id', 'mrapbs_id');
	// }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
