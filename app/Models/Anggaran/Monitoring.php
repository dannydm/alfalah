<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use App\Models\Administrator\Mrapbs;
use App\Models\Administrator\MUser;
use App\Models\Administrator\Mprogram;
use App\Models\Administrator\RoleUser;
use App\Models\Administrator\UserTask;
use App\Models\Administrator\AnggaranUser;

use App\Models\Ppdb\TahunAjaran;
use App\Models\Anggaran\RapbsDetail;
use App\Models\Anggaran\RapbsMaster;

use App\Models\Anggaran\UangmukaMaster;
use App\Models\Anggaran\UangmukaDetail;
use App\Models\Anggaran\UangmukaNota;

use App\Models\Anggaran\PengajuanMaster;
use App\Models\Anggaran\PengajuanDetail;

use App\Models\Anggaran\IncomeDetail;

use App\User;
use Illuminate\Support\Facades\Hash;

use DB;

class Monitoring extends AppModel
{   /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
   
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function MonitoringAllStatus($request, $user_id, $level)
    {
        DB::enableQueryLog();
        $results = DB::table('rapbs_master as a')
        // ->join('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah
                ->leftjoin('user_anggaran as q1',function($join)
                        {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                            ->on('q1.user_id','=','a.created_by');})

                ->leftjoin('pengajuan_detail as p1',function($join)
                    {$join->on('p1.rapbs_no','=','a.rapbs_no')
                        ->leftjoin('pengajuan_master as p2','p2.pengajuan_no','=','p1.pengajuan_no')
                    ->on('q1.user_id','=','a.created_by');})

                ->leftjoin('uangmuka_detail as u1',function($join)
                    {$join->on('u1.rapbs_no','=','a.rapbs_no')
                        ->leftjoin('uangmuka_master as u2','u2.uangmuka_no','=','u1.uangmuka_no')
                    ->on('q1.user_id','=','a.created_by');})

                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')

                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')

 
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, 
                    x2.name as urusan_mrapbs_id_name, 
                    x3.name as program_mrapbs_id_name, 
                    x4.name as kegiatan_mrapbs_id_name, 
                    x5.name as sumberdana_id_name, 
                    x7.name as emp_name,a.status as status,
                    a.keluaran,a.hasil,a.sasaran,a.sub_kegiatan_mrapbs_id,
                    p1.pengajuan_no as pengajuan_no,u1.uangmuka_no as uangmuka_no,
                    a.approve_date_3 as ketua_approve_date, p2.approve_date_3 as pengajuan_approve_date,
                    u2.approve_date_3 as pencairan_approve_date,
                    (   select sum(b.jumlah) 
                        from rapbs_detail as b 
                        where b.rapbs_no = a.rapbs_no
                    ) as total_biaya,
                    (   select sum(c.jum_ajuan) 
                    from pengajuan_detail as c 
                    where c.pengajuan_no = p1.pengajuan_no AND c.rapbs_no = p1.rapbs_no
                    ) as total_biaya_ajuan,
                    (   select sum(d.jum_ajuan) 
                    from uangmuka_detail as d 
                    where d.uangmuka_no = u1.uangmuka_no  AND d.rapbs_no = u1.rapbs_no
                    ) as jumlah_uangmuka

                ")) 
            ->groupby('a.rapbs_no','x1.name','x2.name','x3.name','x4.name','x5.name','x7.name','p1.pengajuan_no',
            'u1.uangmuka_no','p2.approve_date_3','u2.approve_date_3','p1.rapbs_no','u1.rapbs_no')
            ->orderby('pengajuan_no','asc');

        // searchForm parameter
        if ($request->s == "form") 
        {   
            if ($request->organisasi_mrapbs_id_name) 
            {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->program_mrapbs_id_name)
            {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
            };
            if ($request->kegiatan_mrapbs_id_name)
            {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
            };
            if ($request->sub_kegiatan_mrapbs_id)
            {   $results->where("a.sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");  
            };
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };

            // if ($level == 2) //admin level
            // {}
            // else { $results->where('a.created_by','=',$user_id); }; 
        };
        return $results->get();
        }

        public static function MonitoringuserAllStatus($request, $user_id, $level) //dipakai untuk 4/15
        {
            DB::enableQueryLog();
            $results = DB::table('rapbs_master as a')
            // ->join('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah
                    ->leftjoin('user_anggaran as q1',function($join)
                            {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                                ->on('q1.user_id','=','a.created_by');})
    
                    ->leftjoin('pengajuan_detail as p1',function($join)
                        {$join->on('p1.rapbs_no','=','a.rapbs_no')
                            ->leftjoin('pengajuan_master as p2','p2.pengajuan_no','=','p1.pengajuan_no')
                        ->on('q1.user_id','=','a.created_by');})
    
                    ->leftjoin('uangmuka_detail as u1',function($join)
                        {$join->on('u1.rapbs_no','=','a.rapbs_no')
                            ->leftjoin('uangmuka_master as u2','u2.uangmuka_no','=','u1.uangmuka_no')
                        ->on('q1.user_id','=','a.created_by');})
    
                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
    
                    ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                    ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                    ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                    ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
    
     
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name, 
                        x2.name as urusan_mrapbs_id_name, 
                        x3.name as program_mrapbs_id_name, 
                        x4.name as kegiatan_mrapbs_id_name, 
                        x5.name as sumberdana_id_name, 
                        x7.name as emp_name,a.status as status,
                        a.keluaran,a.hasil,a.sasaran,a.sub_kegiatan_mrapbs_id,
                        p1.pengajuan_no as pengajuan_no,u1.uangmuka_no as uangmuka_no,
                        a.approve_date_3 as ketua_approve_date, p2.approve_date_3 as pengajuan_approve_date,
                        u2.approve_date_3 as pencairan_approve_date,
                        (   select sum(b.jumlah) 
                            from rapbs_detail as b 
                            where b.rapbs_no = a.rapbs_no
                        ) as total_biaya,
                        (   select sum(c.jum_ajuan) 
                        from pengajuan_detail as c 
                        where c.pengajuan_no = p1.pengajuan_no AND c.rapbs_no = p1.rapbs_no
                        ) as total_biaya_ajuan,
                        (   select sum(d.jum_ajuan) 
                        from uangmuka_detail as d 
                        where d.uangmuka_no = u1.uangmuka_no  AND d.rapbs_no = u1.rapbs_no
                        ) as jumlah_uangmuka
    
                    ")) 
                ->groupby('a.rapbs_no','x1.name','x2.name','x3.name','x4.name','x5.name','x7.name','p1.pengajuan_no',
                'u1.uangmuka_no','p2.approve_date_3','u2.approve_date_3','p1.rapbs_no','u1.rapbs_no')
                ->orderby('pengajuan_no','asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {   
                if ($request->organisasi_mrapbs_id_name) 
                {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
                };
                if ($request->urusan_mrapbs_id_name)
                {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
                };
                if ($request->program_mrapbs_id_name)
                {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
                };
                if ($request->kegiatan_mrapbs_id_name)
                {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
                };
                if ($request->sub_kegiatan_mrapbs_id)
                {   $results->where("a.sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");  
                };
                if ($request->rapbs_no) 
                {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
                };
    
                if ($level == 2) 
                {  $results->where("a.status","=",0 );}
                else
                {
                    $results->where("a.created_by","=",$user_id); 
                };

            };
            return $results->get();
            }

        public static function MonitoringPengajuanSearch($request, $user_id, $level)
        // ($request, $user_id, $level, $MyTasks)
        {  
            
            $results = DB::table('pengajuan_master as a')
            // ->join('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah
                    ->leftjoin('user_anggaran as q1',function($join)
                            {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                               ->on('q1.user_id','=','a.created_by');})

                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                    ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                    ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                    ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                    
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name,
                        x2.name as urusan_mrapbs_id_name,
                        x3.name as program_mrapbs_id_name,
                        x4.name as kegiatan_mrapbs_id_name, 
                        (   select sum(b.jum_ajuan) 
                            from pengajuan_detail as b 
                            where b.pengajuan_no = a.pengajuan_no
                            ) as total_biaya
                    "))
              
                ->orderby('a.id', 'asc');              
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->pengajuan_no) 
                {   $results->where("pengajuan_no", "ilike", "%" . $request->pengajuan_no."%"); 
                };

                if ($request->uangmuka_keterangan_id)
                {   $results->where("pengajuan_keterangan_id", "ilike", "%".$request->pengajuan_keterangan_id."%");  
                };

                if ($level == 2 ) //admin level
                {
                $results->where('a.status', '=', 0);
                };

            };
            $x = $results->get();

        return $x;

            // return $results;
        }

        public static function MonitoringUserPengajuanSearch($request, $user_id, $level)
        // ($request, $user_id, $level, $MyTasks)
        {  
            
            $results = DB::table('pengajuan_master as a')
            // ->join('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah
                    ->leftjoin('user_anggaran as q1',function($join)
                            {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                               ->on('q1.user_id','=','a.created_by');})

                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                    ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                    ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                    ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                    
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name,
                        x2.name as urusan_mrapbs_id_name,
                        x3.name as program_mrapbs_id_name,
                        x4.name as kegiatan_mrapbs_id_name, 
                        (   select sum(b.jum_ajuan) 
                            from pengajuan_detail as b 
                            where b.pengajuan_no = a.pengajuan_no
                            ) as total_biaya
                    "))
              
                ->orderby('a.id', 'asc');
               
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->pengajuan_no) 
                {   $results->where("pengajuan_no", "ilike", "%" . $request->pengajuan_no."%"); 
                };

                if ($request->uangmuka_keterangan_id)
                {   $results->where("pengajuan_keterangan_id", "ilike", "%".$request->pengajuan_keterangan_id."%");  
                };

                if ($level == 2) 
                {  $results->where("a.status","=",0 );}
                else
                {
                    $results->where("a.created_by","=",$user_id); 
                };

            };

            $x = $results->get();

        return $x;

            // return $results;
        }

        public static function MonitoringcairSearch($request, $user_id, $level)
        {
            $results = DB::table('user_anggaran as q1')
                    ->join('uangmuka_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                    ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                    ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                    ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                    ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.created_by')
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name, x8.username,
                        x2.name as urusan_mrapbs_id_name,
                        x3.name as program_mrapbs_id_name,
                        x4.name as kegiatan_mrapbs_id_name,
                        (   select sum(b.jum_ajuan) 
                            from uangmuka_detail as b 
                            where b.uangmuka_no = a.uangmuka_no
                            ) as total_biaya
                    "))
                   ->where('a.approve_status_1','=',1)
                    ->where("q1.user_id","=",$user_id)
                ->orderby('a.id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->uangmuka_no) 
                {   $results->where("uangmuka_no", "ilike", "%" . $request->uangmuka_no."%"); 
                };
                if ($request->organisasi_mrapbs_id_name)
                {   $results->where("organisasi_mrapbs_id_name", "ilike", "%".$request->organisasi_mrapbs_id_name."%");  
                };
                if ($request->approve_status_3)
                {   $results->where("approve_status_3", "=", 
                    $request->approve_status_3);  
                };
                if ($level == 2 ) //admin level
                {
                $results->where('a.status', '=', 0);
                };

                if ($request->pt) {   return $results->first();   }
                else {  return $results->get(); };
                // if ($request->status) {
                //     $results->where("status", "=", self::setStatus($request->status));
                // };
            };
            return $results->get();
        }

        public static function MonitoringusercairSearch($request, $user_id, $level)
        {
            $results = DB::table('uangmuka_master as a')

                    ->leftjoin('user_anggaran as q1',function($join)
                    {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                    ->on('q1.user_id','=','a.created_by');})

//                    ->join('uangmuka_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                    ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                    ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                    ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                    ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.created_by')
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name, x8.username,
                        x2.name as urusan_mrapbs_id_name,
                        x3.name as program_mrapbs_id_name,
                        x4.name as kegiatan_mrapbs_id_name,
                        (   select sum(b.jum_ajuan) 
                            from uangmuka_detail as b 
                            where b.uangmuka_no = a.uangmuka_no
                            ) as total_biaya
                    "))
//                   ->where('a.approve_status_1','=',1)
//                    ->where("q1.user_id","=",$user_id)
                ->orderby('a.id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->uangmuka_no) 
                {   $results->where("uangmuka_no", "ilike", "%" . $request->uangmuka_no."%"); 
                };
                if ($request->organisasi_mrapbs_id_name)
                {   $results->where("organisasi_mrapbs_id_name", "ilike", "%".$request->organisasi_mrapbs_id_name."%");  
                };
                if ($request->approve_status_3)
                {   $results->where("approve_status_3", "=", 
                    $request->approve_status_3);  
                };
                if ($level == 2) 
                {  $results->where("a.status","=",0 );}
                else
                {
                    $results->where("a.created_by","=",$user_id); 
                };
                if ($request->pt) {   return $results->first();   }
                else {  return $results->get(); };
                // if ($request->status) {
                //     $results->where("status", "=", self::setStatus($request->status));
                // };
            };
            return $results->get();
        }

        public static function MonitoringPengajuanDetailSearch($request)
        {  // Print_r($pengajuan_no);exit;
            $results = DB::table('pengajuan_detail as a')  
                    // ->leftjoin('pengajuan_master as b','b.pengajuan_no','=','a.pengajuan_no') 
                    ->select(DB::raw("a.*, a.rapbs_no as rapbs_no,a.uraian as uraian,a.jum_ajuan as jum_ajuan "))
//                    ->where('a.pengajuan_no','=',$pengajuan_no) 
                    ->orderby('a.id', 'asc');
             // searchForm parameter
            // if ($request->s == "form") 
            // {
            //     if ($request->pengajuan_no) 
            //     {   $results->where("a.pengajuan_no", "ilike", "%" . $request->pengajuan_no."%"); 
            //     };
            // };
            return $results->get();
        }

        public static function DashboardUmSearch($request)
        {    DB::enableQueryLog();
            $subQuery = DB::table('pengajuan_master as a')
                            ->join('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                            ->leftjoin('uangmuka_master as u1','u1.urusan_mrapbs_id','a.urusan_mrapbs_id')
                            ->select(DB::raw("a.urusan_mrapbs_id, u1.approve_status_2, u1.approve_status_3,
                                x1.name as urusan_mrapbs_id_name,  
                                (   select sum(b.jum_ajuan) 
                                    from pengajuan_detail as b  
                                    where b.pengajuan_no = a.pengajuan_no
                                ) as total_ajuan,
                                (   select sum(u.jum_ajuan) 
                                from uangmuka_detail as u  
                                where u.rapbs_no = u1.rapbs_no
                            ) as total_uangmuka
                            "))
                // ->where('a.approve_status_2','=',1)
            ->orderby('x1.mrapbs_id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->urusan_mrapbs_id_name)
                {   $results->where("x1.urusan", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
                };
                if ($request->rapbs_no) 
                {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
                };
            };
    
            $results = PengajuanMaster::selectRaw("x.urusan_mrapbs_id, x.urusan_mrapbs_id_name, x.approve_status_2, x.approve_status_3, 
                            sum(x.total_ajuan) as total_ajuan, sum(x.total_uangmuka) as total_uangmuka")
                ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
                ->mergeBindings($subQuery)
                ->groupby('x.urusan_mrapbs_id', 'x.urusan_mrapbs_id_name','x.approve_status_2', 'x.approve_status_3')
                ->orderby('x.urusan_mrapbs_id', 'asc');
    
                $x = $results->get();
     //             print_r(DB::getQueryLog()); exit;
                return $x; 
        }

        public static function MonitoringrapbyuserSearch($request, $user_id, $level)
        {
            DB::enableQueryLog();
            $results = DB::table('rapbs_master as a')
            // ->join('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah
                    ->leftjoin('user_anggaran as q1',function($join)
                            {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                                ->on('q1.user_id','=','a.created_by');})
    
                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
    
                    ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                    ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                    ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                    ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
    
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name, 
                        x2.name as urusan_mrapbs_id_name, 
                        x3.name as program_mrapbs_id_name, 
                        x4.name as kegiatan_mrapbs_id_name, 
                        x5.name as sumberdana_id_name, 
                        x6.name as coa_id_name,
                        x7.name as emp_name,a.status as status,
                        a.keluaran,a.hasil,a.sasaran,a.sub_kegiatan_mrapbs_id,
                        (   select sum(b.jumlah) 
                            from rapbs_detail as b 
                            where b.rapbs_no = a.rapbs_no
                        ) as total_biaya
                    "))
                ->orderby('a.id', 'asc');
    
            // searchForm parameter
            if ($request->s == "form") 
            {   
                if ($request->organisasi_mrapbs_id_name) 
                {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
                };
                if ($request->urusan_mrapbs_id_name)
                {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
                };
                if ($request->program_mrapbs_id_name)
                {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
                };
                if ($request->kegiatan_mrapbs_id_name)
                {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
                };
                if ($request->sub_kegiatan_mrapbs_id)
                {   $results->where("a.sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");  
                };
                if ($request->rapbs_no) 
                {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
                };
                if ($level == 2) //admin level
                {}
                else { $results->where('a.created_by','=',$user_id); }; 
            };
            return $results->get();
            }
            
            public static function MonitoringRapbsDetailSearch($request)
            {   
                $results = DB::table('rapbs_detail as a')  
                        ->leftjoin('rapbs_master as b','b.rapbs_no','=','a.rapbs_no') 
                        ->select(DB::raw("a.*, '' as action")) 
                        ->orderby('a.id', 'asc');
                
                // searchForm parameter
                if ($request->s == "form") 
                {
                    if ($request->rapbs_no) 
                    {   $results->where("a.rapbs_no",'=', $request->rapbs_no);
                    };
        
                    if ($request->rapbs_no) 
                    {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
                    };
                };
                return $results->get();
            }


    public static function SerapanSearch($request)
    {    DB::enableQueryLog();

        // $subQuery_1 = DB::table('uangmuka_master as master');

        $subQuery = DB::table('rapbs_master as a')
                        ->join('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                        ->join('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                        ->select(DB::raw("a.urusan_mrapbs_id, a.jumlahn,
                            x1.name as urusan_mrapbs_id_name,x2.name as organisasi_mrapbs_id_name,

                            (   select sum(b.jumlah) from rapbs_detail as b  
                                where b.rapbs_no = a.rapbs_no ) as total_biaya,

                            (   select sum(d.jumlah) from rapbs_detail as d  
                            where d.rapbs_no = a.rapbs_no AND a.hasil ~* 'PR-') as total_perubahan,

                            ( select sum(e.jum_ajuan) from uangmuka_detail as e 
                                inner join uangmuka_master as master on master.uangmuka_no = e.uangmuka_no
                                where  e.rapbs_no = a.rapbs_no AND master.approve_status_3 =  1  ) as total_pencairan,

                            (   select sum(c.realisasi_biaya) from uangmuka_detail as c where c.rapbs_no = a.rapbs_no
                            ) as total_realisasi 
                        ")) 
        // ->where('a.approve_status_2','=',1)
        ->orderby('a.urusan_mrapbs_id', 'asc');

        $results = RapbsMaster::selectRaw("x.urusan_mrapbs_id, x.urusan_mrapbs_id_name,x.organisasi_mrapbs_id_name, 
                                sum(x.total_biaya) as total_biaya, sum(x.total_perubahan) as total_perubahan,
                                sum(x.total_pencairan) as total_pencairan, 
                                sum(x.jumlahn) as jumlahn, 
                                sum(x.total_realisasi) as total_realisasi")

            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->groupby('x.urusan_mrapbs_id', 'x.urusan_mrapbs_id_name','x.organisasi_mrapbs_id_name')
            ->orderby('x.urusan_mrapbs_id', 'asc');

            $x = $results->get();
            //  print_r(DB::getQueryLog()); exit;
            return $x; 
    }


    public static function SerapanKegiatanSearch($request)
    {    DB::enableQueryLog();
        $subQuery = DB::table('rapbs_master as a')
                        ->join('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
 //                       -leftjoin('uangmuka_detail as x2','x2.rapbs_no','=','a.rapbs_no')


                        ->select(DB::raw("a.urusan_mrapbs_id, a.jumlahn, a.rapbs_no, a.sub_kegiatan_mrapbs_id,
                            x1.name as urusan_mrapbs_id_name,  
                            (   select sum(b.jumlah) 
                                from rapbs_detail as b  
                                where b.rapbs_no = a.rapbs_no
                            ) as total_biaya,
                            (   select sum(d.jumlah) 
                            from rapbs_detail as d  
                            where d.rapbs_no = a.rapbs_no AND a.hasil ILIKE 'PR-'
                        ) as total_perubahan,

                            (   select sum(c.realisasi_biaya) 
                            from uangmuka_detail as c  
                            where c.rapbs_no = a.rapbs_no 
                        ) as total_realisasi 
                    ")) 
        // ->where('a.approve_status_2','=',1)
    //        ->where('x2.realisasi_biaya','>',0)
        ->orderby('x1.mrapbs_id', 'asc');

        $results = RapbsMaster::selectRaw("x.urusan_mrapbs_id, x.urusan_mrapbs_id_name, x.rapbs_no, x.sub_kegiatan_mrapbs_id, 
                        sum(x.total_biaya) as total_biaya,sum(x.total_perubahan) as total_perubahan, sum(x.jumlahn) as jumlahn, x.total_realisasi")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            
            ->groupby('x.urusan_mrapbs_id', 'x.urusan_mrapbs_id_name','x.rapbs_no','x.sub_kegiatan_mrapbs_id','x.total_realisasi')
            ->whereRaw('x.total_realisasi > 0')
            ->orderby('x.urusan_mrapbs_id', 'asc');

            // searchForm parameter
            if ($request->s == "form") 
            {
                if ($request->rapbs_no) 
                {   $results->where("x.rapbs_no",'=', $request->rapbs_no);
                };
                if ($request->realisasi_biaya) 
                {   $results->where("x.total_realisasi",'>', 0);
                };
                if ($request->urusan_mrapbs_id) 
                {   $results->where("x.urusan_mrapbs_id", "ilike", "%" . $request->urusan_mrapbs_id . "%");
                };
            };

            $x = $results->get();
 //             print_r(DB::getQueryLog()); exit;
            return $x;
        }        
            
        public static function CostMonitoringSearch($request) 
        {   
            $results = DB::table('uangmuka_detail as a')
            ->leftjoin('uangmuka_master as q1', 'q1.uangmuka_no', '=','a.uangmuka_no')
            ->select(DB::raw("a.coa_id as coa_id, a.coa_name as coa_name,
                    (   select sum(b.jum_ajuan) 
                    from uangmuka_detail as b 
                    where b.coa_id = a.coa_id 
                    ) as total_cost
                    "))
            ->groupby('a.coa_id', 'a.coa_name')
            ->where('q1.approve_status_5','=',1)
            ->orderby('a.coa_id', 'asc');

            return $results->get();
        }

        public static function RapbyMonitoringSearch($request) 
        {   //print_r($request);exit;
            $results = DB::table('income_detail as a')
            ->leftjoin('m_income as b','b.mincome_id','=','a.income_detail_id')
            ->select(DB::raw("a.*, a.income_detail_id , a.income_name, a.income_amount, b.sumberdana_id,
            (   select sum(c.income_amount) 
                    from income_detail as c
                    where c.income_detail_id = a.income_detail_id AND b.sumberdana_id = 1
                    ) as total_tt
            "))
            ->orderby('a.income_detail_id', 'asc') 
            ->get();


            $x = $results;
            return $x;
        }

        public static function masterdetailincomeMonitoring($request) 
        {   
            $results = DB::table('income_master as a')
            ->leftjoin('income_detail as b','b.income_master_no','=','a.income_master_no')
            ->select(DB::raw("a.income_master_no, a.income_description_name , a.cost_presentage"))
            ->groupby('a.income_master_no')
            ->orderby('a.income_master_no', 'asc');

            return $results->get();
        }


        public static function UrusanSearch($request)
        {    DB::enableQueryLog();
    
            $subQuery = DB::table('rapbs_master as a')
                            ->join('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                            ->join('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                            ->select(DB::raw("a.urusan_mrapbs_id, a.jumlahn,
                                x1.name as urusan_mrapbs_id_name,x2.name as organisasi_mrapbs_id_name,
    
                                (   select sum(b.jumlah) from rapbs_detail as b  
                                    where b.rapbs_no = a.rapbs_no ) as total_biaya,
    
                                (   select sum(d.jumlah) from rapbs_detail as d  
                                where d.rapbs_no = a.rapbs_no AND a.hasil ~* 'PR-') as total_perubahan,
    
                                ( select sum(e.jum_ajuan) from uangmuka_detail as e 
                                    inner join uangmuka_master as master on master.uangmuka_no = e.uangmuka_no
                                    where  e.rapbs_no = a.rapbs_no AND master.approve_status_3 =  1  ) as total_pencairan,
    
                                (   select sum(c.realisasi_biaya) from uangmuka_detail as c where c.rapbs_no = a.rapbs_no
                                ) as total_realisasi 
                            ")) 
            // ->where('a.approve_status_2','=',1)
            ->orderby('a.urusan_mrapbs_id', 'asc');
    
            $results = RapbsMaster::selectRaw("x.urusan_mrapbs_id, x.urusan_mrapbs_id_name,x.organisasi_mrapbs_id_name, 
                                    sum(x.total_biaya) as total_biaya, sum(x.total_perubahan) as total_perubahan,
                                    sum(x.total_pencairan) as total_pencairan, 
                                    sum(x.jumlahn) as jumlahn, 
                                    sum(x.total_realisasi) as total_realisasi")
    
                ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
                ->mergeBindings($subQuery)
                ->groupby('x.urusan_mrapbs_id', 'x.urusan_mrapbs_id_name','x.organisasi_mrapbs_id_name')
                ->orderby('x.urusan_mrapbs_id', 'asc');
    
                $x = $results->get();
                //  print_r(DB::getQueryLog()); exit;
                return $x; 
        }

        public static function UrusanChart($request)
        {    DB::enableQueryLog();
    
            $subQuery = DB::table('rapbs_master as a')
                            ->join('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                            ->join('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                            ->select(DB::raw("a.urusan_mrapbs_id, a.jumlahn,
                                x1.name as urusan_mrapbs_id_name,x2.name as organisasi_mrapbs_id_name,
    
                                (   select sum(b.jumlah) from rapbs_detail as b  
                                    where b.rapbs_no = a.rapbs_no ) as total_biaya,
    
                                (   select sum(d.jumlah) from rapbs_detail as d  
                                where d.rapbs_no = a.rapbs_no AND a.hasil ~* 'PR-') as total_perubahan,
    
                                ( select sum(e.jum_ajuan) from uangmuka_detail as e 
                                    inner join uangmuka_master as master on master.uangmuka_no = e.uangmuka_no
                                    where  e.rapbs_no = a.rapbs_no AND master.approve_status_3 =  1  ) as total_pencairan,
    
                                (   select sum(c.realisasi_biaya) from uangmuka_detail as c where c.rapbs_no = a.rapbs_no
                                ) as total_realisasi 
                            ")) 
            // ->where('a.approve_status_2','=',1)
            ->orderby('a.urusan_mrapbs_id', 'asc');
    
            $results = RapbsMaster::selectRaw("x.urusan_mrapbs_id, x.urusan_mrapbs_id_name,x.organisasi_mrapbs_id_name, 
                                    sum(x.total_biaya) as total_biaya, sum(x.total_perubahan) as total_perubahan,
                                    sum(x.total_pencairan) as total_pencairan, 
                                    sum(x.jumlahn) as jumlahn, 
                                    sum(x.total_realisasi) as total_realisasi")
    
                ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
                ->mergeBindings($subQuery)
                ->groupby('x.urusan_mrapbs_id', 'x.urusan_mrapbs_id_name','x.organisasi_mrapbs_id_name')
                ->orderby('x.urusan_mrapbs_id', 'asc')->get();
    
                $x = $results;
                //  print_r(DB::getQueryLog()); exit;
                return $x; 
        }

        public static function ApbyMonitoring($request) 
        {   
            $results = DB::table('uangmuka_detail as a')
            ->leftjoin('uangmuka_master as q1', 'q1.uangmuka_no', '=','a.uangmuka_no')
            ->select(DB::raw("a.coa_id as coa_id, a.coa_name as coa_name,
                    (   select sum(b.jum_ajuan) 
                    from uangmuka_detail as b 
                    where b.coa_id = a.coa_id 
                    ) as total_cost
                    "))
            ->groupby('a.coa_id', 'a.coa_name')
            ->where('q1.approve_status_5','=',1)
            ->orderby('a.coa_id', 'asc');

            return $results->get();
        }


        public static function IncomeMonitoring($request) 
        {   //print_r($request);exit;
            $results = DB::table('income_detail as a')
            ->select(DB::raw("a.*, a.income_detail_id , a.income_name, a.income_amount"))
            ->orderby('a.income_detail_id', 'asc') 
            ->get();


            $x = $results;
            return $x;
        }

    public static function ForecastingSearch($request) 
        {   DB::enableQueryLog();
            $results = DB::table('uangmuka_detail as a')
            ->leftjoin('uangmuka_master as q1', 'q1.uangmuka_no', '=','a.uangmuka_no')
            ->select(DB::raw("a.coa_id , a.coa_name ,
            ( 
            select date_part('month',q1.approve_date_5) as fs_month),
            (   select sum(b.jum_ajuan) 
            from uangmuka_detail as b 
            where b.coa_id = a.coa_id 
            ) as monthly_cost 
                    "))
            ->groupby('a.coa_id', 'a.coa_name','fs_month')
            ->where('q1.approve_status_5','=',1)
            ->orderby('a.coa_id', 'asc');

            $x = $results->get();
        //   print_r(DB::getQueryLog()); exit;
            return $x;      
    }

    public static function statusMonitoring1($request, $user_id) //dipakai untuk monitoring user approval status
    {
        DB::enableQueryLog();
        $results = DB::table('user_anggaran as a')
            ->leftjoin('rapbs_master as q1','q1.created_by','=','a.user_id') // ini bermasalah mungkin
            ->leftjoin('pengajuan_detail as p1',function($join)
                                    {$join->on('p1.rapbs_no','=','q1.rapbs_no')
                                        ->leftjoin('pengajuan_master as p2','p2.pengajuan_no','=','p1.pengajuan_no')
                                        ->groupby('p1.rapbs_no')
                                        ;})
            ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'q1.organisasi_mrapbs_id')
            ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'q1.urusan_mrapbs_id')
            ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'q1.program_mrapbs_id')
            ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'q1.kegiatan_mrapbs_id')
             ->select(DB::raw("a.*, 
                x1.name as organisasi_mrapbs_id_name, 
                x2.name as urusan_mrapbs_id_name, 
                x3.name as program_mrapbs_id_name, 
                x4.name as kegiatan_mrapbs_id_name, 
                q1.sub_kegiatan_mrapbs_id, q1.rapbs_no as rapbs_no, 
                p2.pengajuan_no as pengajuan_rapbs, p2.pengajuan_keterangan_id as pengajuan_keterangan, 
                p2.sub_kegiatan_mrapbs_id as pengajuan_sub_kegiatan, 
                p2.approve_date_1 as release_date,p2.approve_date_2 as kabid_date,p2.approve_date_3 as ketua_date,
                q1.approve_date_3 as ketua_approve_date,
                (   select sum(b.jumlah) 
                    from rapbs_detail as b 
                    where b.rapbs_no = q1.rapbs_no
                ) as total_biaya
            ")) 
        ->groupby('a.user_id','a.mrapbs_id','q1.rapbs_no','x1.name','x2.name','x3.name','x4.name','p2.pengajuan_no'
        , 'p2.pengajuan_keterangan_id','p2.sub_kegiatan_mrapbs_id', 'p2.approve_date_1','p2.approve_date_2','p2.approve_date_2')
        ->where('a.user_id','=',$user_id)
        ->orderby('rapbs_no','asc')->get();

        $x = $results;
        return $x;
    }
    
    public static function statusMonitoring($request, $user_id)
    {    DB::enableQueryLog();
        $results = DB::table('user_anggaran as a')
            ->leftjoin('rapbs_master as q1','q1.created_by','=','a.user_id') // ini bermasalah mungkin
            ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'q1.organisasi_mrapbs_id')
            ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'q1.urusan_mrapbs_id')
            ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'q1.program_mrapbs_id')
            ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'q1.kegiatan_mrapbs_id')
            ->select(DB::raw("a.*, q1.id,
                x1.name as organisasi_mrapbs_id_name, 
                x2.name as urusan_mrapbs_id_name, 
                x3.name as program_mrapbs_id_name, 
                x4.name as kegiatan_mrapbs_id_name, 
                q1.sub_kegiatan_mrapbs_id, q1.rapbs_no as rapbs_no, 
                q1.approve_date_3 as ketua_approve_date,
                (   select sum(b.jumlah) 
                    from rapbs_detail as b 
                    where b.rapbs_no = q1.rapbs_no
                ) as total_biaya
            ")) 
        ->groupby('a.user_id','a.mrapbs_id','q1.rapbs_no','x1.name','x2.name','x3.name','x4.name')
        ->where('a.user_id','=',$user_id)
        ->orderby('q1.id','asc')->get();

        $x = $results;
        return $x;
    }
    
    public static function historyPengajuan($request)
    {    DB::enableQueryLog();
        $results = DB::table('pengajuan_master as p1')
            ->leftjoin('pengajuan_detail as p2','p2.pengajuan_no','=','p1.pengajuan_no')
            ->leftjoin('rapbs_master as r1','r1.rapbs_no','=','p2.rapbs_no')
            ->select(DB::raw("p1.*, p1.id,
                p1.sub_kegiatan_mrapbs_id as pengajuan_sub_kegiatan, p2.rapbs_no as pengajuan_rapbs, p1.pengajuan_keterangan_id as pengajuan_keterangan,
                p1.created_date as release_date, p1.approve_date_1 as kabid_date,p1.approve_date_2 as ketua_date,
                (   select sum(b.jum_ajuan) 
                    from pengajuan_detail as b 
                    where b.pengajuan_no = p1.pengajuan_no
                ) as total_pengajuan
            ")) 
        ->groupby('p1.pengajuan_no','p1.sub_kegiatan_mrapbs_id','p2.rapbs_no','p2.pengajuan_no','p1.sub_kegiatan_mrapbs_id')
        // ->where('p2.rapbs_no','=',"r1.rapbs_no")
        ->orderby('p1.id','asc')->get();

        $x = $results;
        return $x;
    }

    public static function historyUangmuka($request)
    {    DB::enableQueryLog();
        $results = DB::table('uangmuka_master as u1')
            ->leftjoin('pengajuan_master as pm1','pm1.pengajuan_no','=','u1.pengajuan_no')
            ->leftjoin('rapbs_master as rm1','rm1.rapbs_no','=','u1.rapbs_no')
            ->select(DB::raw("u1.*, u1.uangmuka_no as uangmuka_no, u1.id,
                u1.sub_kegiatan_mrapbs_id as uangmuka_sub_kegiatan, u1.rapbs_no as uangmuka_rapbs, u1.uangmuka_keterangan_id as uangmuka_keterangan,
                u1.created_date as release_date, u1.approve_date_1 as uangmuka_kabid_date,u1.approve_date_2 as finance_approve_date,
                u1.approve_date_3 as receive_date,u1.approve_date_4 as approve_realisasi_kabid, u1.approve_date_5 as approve_realisasi_finance, 
                (   select sum(b.jum_ajuan) 
                    from uangmuka_detail as b 
                    where b.uangmuka_no = u1.uangmuka_no
                ) as total_uangmuka
            ")) 
        ->groupby('u1.uangmuka_no','u1.pengajuan_no','u1.sub_kegiatan_mrapbs_id','u1.rapbs_no')
        // ->where('p2.rapbs_no','=',"r1.rapbs_no")
        ->orderby('u1.id','asc')->get();

        $x = $results;
        return $x;
    }

    public static function BendaharaSearch($request, $user_id, $level)
    {  
        $results = DB::table('user_anggaran as q1')
        ->join('rapbs_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
        ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
        ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
        ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
        ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
        ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
        ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
        ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
        ->select(DB::raw("a.*, 
            x1.name as organisasi_mrapbs_id_name, 
            x2.name as urusan_mrapbs_id_name, 
            x3.name as program_mrapbs_id_name, 
            x4.name as kegiatan_mrapbs_id_name, 
            x5.name as sumberdana_id_name, 
            x6.name as coa_id_name,
            x7.name as emp_name,a.sub_kegiatan_mrapbs_id,
            (   select sum(b.jumlah) 
                from rapbs_detail as b 
                where b.rapbs_no = a.rapbs_no
            ) as total_biaya
        "))
        ->where("a.approve_status_2","=",1)
        ->where("q1.user_id","=",$user_id)
        ->orderby('a.id', 'asc');

        // print($results);exit;
        // searchForm parameter
        if ($request->s == "form") 
        {   
            if ($request->organisasi_mrapbs_id_name) 
            {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
            };
            if ($request->urusan_mrapbs_id_name)
            {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
            };
            if ($request->program_mrapbs_id_name)
            {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
            };
            if ($request->kegiatan_mrapbs_id_name)
            {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
            };
            if ($request->sub_kegiatan_mrapbs_id)
            {   $results->where("a.sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");
            };    

            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };

            if ($level == 2 ) //admin level
            {
            $results->where('a.status', '=', 0);
            };
        };
        return $results->get();
    }

    public static function Monitoringspj($request, $user_id, $level)
    {
        $results = DB::table('user_anggaran as q1')
                ->join('uangmuka_master as a','q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                ->leftjoin('m_users as x8', 'x8.user_id', '=', 'a.created_by')
                ->select(DB::raw("a.*, 
                    x1.name as organisasi_mrapbs_id_name, x8.username,
                    x2.name as urusan_mrapbs_id_name,
                    x3.name as program_mrapbs_id_name,
                    x4.name as kegiatan_mrapbs_id_name,
                    a.sub_kegiatan_mrapbs_id,
                    (   select sum(b.jum_ajuan) 
                        from uangmuka_detail as b 
                        where b.uangmuka_no = a.uangmuka_no AND a.approve_status_1 =  1
                    ) as total_uangmuka,
                    (   select sum(c.realisasi_biaya) 
                        from uangmuka_detail as c 
                        where c.uangmuka_no = a.uangmuka_no 
                    ) as total_realisasi
                "))
                // ->where('a.approve_status_1','=', 1 )
                ->where("q1.user_id","=",$user_id)
            ->orderby('a.id', 'asc');

        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->uangmuka_no) 
            {   $results->where("uangmuka_no", "ilike", "%" . $request->uangmuka_no."%"); 
            };
            if ($request->organisasi_mrapbs_id_name)
            {   $results->where("organisasi_mrapbs_id_name", "ilike", "%".$request->organisasi_mrapbs_id_name."%");  
            };
            if ($request->approve_status_3)
            {   $results->where("approve_status_3", "=", 
                $request->approve_status_3);  
            };
            if ($level == 2 ) //admin level
            {
            $results->where('a.status', '=', 0);
            };

        };
        $x = $results->get();
        //  print_r(DB::getQueryLog()); exit;
        return $x; 

    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    // public function Pk_Uangmuka_master()
    // {
    //     return $this->hasMany('App\Models\Uangmuka_master', 'id');
    // }

 //    public function fkMrapbs(){
	//     return $this->belongsTo('\App\Http\Models\Mrapbs', 'urusan_mrapbs_id', 'mrapbs_id');
	// }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
