<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use DB;

class UangmukaReject extends AppModel
{   /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table      = 'uangmuka_reject';
    protected $primaryKey = 'id';
    //public $sequence_name = 'uangmuka_reject_seq'; 
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
  
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
        // public static function RejectUangMuka($uangmuka_no, $catatan, $head_data, $site, $user_id, $company_id)
        // {  //print_r($head_data);exit;
        //     DB::beginTransaction();
        //    // $result = array();
        //     try {                 
    
        //         if ( $head_data->created_date == '')  
                   
        //             {  // $UangmukaReject = UangmukaReject::where('uangmuka_no', '=', $uangmuka_no)->first();
                        
        //                // $seq_no = $UangmukaReject->id + 1;
        //                // $UangmukaReject->id = $seq_no;
                       
        //                 $UangmukaReject = new UangmukaReject();
        //                 $UangmukaReject->created_date = date('Ymd');
        //                 $UangmukaReject->pengajuan_no            = $head_data->pengajuan_no;
        //                 $UangmukaReject->rapbs_no                = $head_data->rapbs_no;
        //                 $UangmukaReject->organisasi_mrapbs_id    = $head_data->organisasi_mrapbs_id ;
        //                 $UangmukaReject->program_mrapbs_id       = $head_data->program_mrapbs_id;
        //                 $UangmukaReject->kegiatan_mrapbs_id      = $head_data->kegiatan_mrapbs_id;
        //                 $UangmukaReject->sub_kegiatan_mrapbs_id  = $head_data->sub_kegiatan_mrapbs_id;
        //                 $UangmukaReject->sumberdana_id           = $head_data->sumberdana_id;  
        //                 $UangmukaReject->hasil                   = $head_data->hasil; 
        //                 $UangmukaReject->total_reject            = $head_data->total_biaya; 
        //                 $UangmukaReject->id_uangmuka             = $head_data->id;
        //                 $UangmukaReject->tahun_ajaran_id         = $head_data->tahun_ajaran_id;
        //                 $UangmukaReject->reject_date_1           = date('Ymd');
        //                 $UangmukaReject->reject_by_1             = $user_id;
        //                 $UangmukaReject->reject_reason_1         = $catatan;
                  
        //                 $result = $UangmukaReject->save();
                        
            
        //                 // commit transaction
        //                 $result = [true, "Save succcefully"];
        //                 DB::commit();
        //             }
                    
        //         } 
        //             catch (\Illuminate\Database\QueryException $e) {
        //             // rollback transaction
        //             $result = [false, "Error Executed ---> ".$e];
        //             DB::rollback();
        //         } ;
        //         // print_r($result);exit;
        //          return $result;       
        // }

    public static function RejectUangMuka( $catatan, $head_data, $site, $user_id, $company_id) 
    {
     if (count($head_data) > 0)
        { 
        DB::beginTransaction();
        try 
          {// print_r($head_data);exit;
            foreach ($head_data as $json)
             {

                $UangmukaReject = new UangmukaReject();

                    $UangmukaReject->created_date = date('Ymd');
                    $UangmukaReject->uangmuka_no             = $json->uangmuka_no;
                    $UangmukaReject->pengajuan_no            = $json->pengajuan_no;
                    $UangmukaReject->rapbs_no                = $json->rapbs_no;
                    $UangmukaReject->organisasi_mrapbs_id    = $json->organisasi_mrapbs_id ;
                    $UangmukaReject->urusan_mrapbs_id       = $json->urusan_mrapbs_id;
 
                    $UangmukaReject->program_mrapbs_id       = $json->program_mrapbs_id;
                    $UangmukaReject->kegiatan_mrapbs_id      = $json->kegiatan_mrapbs_id;
                    $UangmukaReject->sub_kegiatan_mrapbs_id  = $json->sub_kegiatan_mrapbs_id;
                    $UangmukaReject->sumberdana_id           = $json->sumberdana_id;  
                    $UangmukaReject->hasil                   = $json->hasil; 
                    $UangmukaReject->total_reject            = $json->total_biaya; 
                    $UangmukaReject->id_uangmuka             = $json->id;
                    $UangmukaReject->tahun_ajaran_id         = $json->tahun_ajaran_id;
                    $UangmukaReject->reject_date_1           = date('Ymd');
                    $UangmukaReject->reject_by_1             = $user_id;
                    $UangmukaReject->reject_reason_1         = $catatan;
                    
                    $result =  $UangmukaReject->save();

                    // commit transaction
                    $result = [true, "Save succcefully"];
                    DB::commit();
                }
            } 
                catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        
            return $result;  
        }    
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    // public function Pk_Uangmuka_master()
    // {
    //     return $this->hasMany('App\Models\Uangmuka_master', 'id');
    // }

 //    public function fkMrapbs(){
	//     return $this->belongsTo('\App\Http\Models\Mrapbs', 'urusan_mrapbs_id', 'mrapbs_id');
	// }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
