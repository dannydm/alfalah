<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use DB;

class RapbsDetail extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/
    // create sequence rapbs_detail_seq;
    // CREATE TABLE public.rapbs_detail
    // (
    //   id numeric(18,0) NOT NULL DEFAULT nextval('rapbs_detail_seq'::regclass),
    //   rapbs_no character varying(20) NOT NULL,
    //   coa_id character varying(8),
    //   coa_name character varying(100),
    //   uraian character varying(256),
    //   volume numeric(5,2),
    //   tarif numeric(15,2),
    //   satuan character varying(20),
    //   jumlah numeric(15,2) DEFAULT 0,
    //   berulang numeric(5,2) DEFAULT 0,
    //   status numeric(1,0) DEFAULT 0,
    //   created_date timestamp without time zone NOT NULL DEFAULT now(),
    //   modified_date timestamp without time zone DEFAULT now(),
    //   CONSTRAINT rapbs_detail_pk PRIMARY KEY (rapbs_no, id),
    //   CONSTRAINT rapbsmaster1_fk FOREIGN KEY (rapbs_no) references rapbs_master(rapbs_no)
    // );
    /*
    
     *//*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'rapbs_detail';
	protected $primaryKey = 'id';
    public $sequence_name = 'rapbs_detail_seq';
    
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
        */
    
        public static function detailPrintOut($rapbs_no)
    {   $results = DB::table('rapbs_detail as a')
            ->leftjoin('rapbs_master as x1', 'x1.rapbs_no', '=', 'a.rapbs_no')
            ->select(DB::raw("
                a.coa_id, a.coa_name, a.uraian, a.volume, 
                a.tarif, a.satuan, a.jumlah,
                (   select sum(b.jumlah) 
                from rapbs_detail as b 
                where b.rapbs_no = a.rapbs_no
                ) as total_biaya
                "))
            ->where("x1.rapbs_no", "=", $rapbs_no)
            ->orderby('x1.rapbs_no');

        return $results->get();
    }
    
    public static function RapbsDetailSearch($request)
    {   //$results = static::orderby('id', 'asc');  
        //return $results->get();

        
        $results = DB::table('rapbs_detail as a')  
                ->leftjoin('rapbs_master as b','b.rapbs_no','=','a.rapbs_no') 
                ->select(DB::raw("a.*, '' as action")) 
                ->orderby('a.id', 'asc');
        
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no",'=', $request->rapbs_no);
            };

            if ($request->rapbs_no) 
            {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };
        };

        return $results->get();
    }

    public static function PenetapanDetailSearch($request, $json_data)
    {   DB::enableQueryLog();
        $rapbs_arr = array();
        foreach ($json_data as $json) 
        {
            array_push($rapbs_arr, $json->rapbs_no );
        };
        // print_r($rabs_no); exit;

        $subQuery_2 = DB::table('rapbs_detail as a')
            ->select(DB::raw("a.coa_id, b.coa_id as coa_id2, a.tarif, a.volume, a.uraian, a.coa_name, a.rapbs_no, a.satuan, a.jumlah,
                            coalesce(sum(b.vol_ajuan),0) as vol_pakai
                            ")) //'' as pengajuan_no, b.id as rapbsdetail_id,  z.sub_kegiatan_mrapbs_id,
            ->leftjoin('pengajuan_detail as b', function($join)
                    {   $join->on('b.rapbs_no', '=', 'a.rapbs_no')
                            ->on('b.coa_id2', '=', 'a.coa_id')
                            ->on('b.uraian', '=', 'a.uraian');
                    })
            // ->leftjoin('rapbs_master as z', 'z.rapbs_no', '=', 'a.rapbs_no')
            ->whereIn('a.rapbs_no', $rapbs_arr)
            ->groupby('a.coa_id')
            ->groupby('b.coa_id2')
            ->groupby('a.tarif')
            ->groupby('a.volume')
            ->groupby('a.uraian')
            ->groupby('a.coa_name')
            ->groupby('a.rapbs_no')
            ->groupby('a.satuan')
            ->groupby('a.jumlah');

        $subQuery_1 = RapbsDetail::selectRaw('x.coa_id, x.coa_id2, 
                        x.volume, (x.volume - x.vol_pakai) as vol_sisa, x.vol_pakai, x.tarif ,
                        x.satuan,x.jumlah,x.uraian, x.coa_name,x.rapbs_no')
                    ->from(\DB::raw(' ( '.$subQuery_2->toSql().' ) as x'));
        
        $results = RapbsDetail::selectRaw(' y.coa_id,y.coa_id2, y.volume, y.vol_pakai,y.tarif ,y.satuan, y.jumlah,
                        y.uraian,y.coa_name,y.rapbs_no ,sum(y.vol_sisa) as vol_sisa, 0 as vol_ajuan')
                    ->from(\DB::raw(' ( '.$subQuery_1->toSql().' ) as y'))
                    ->mergeBindings($subQuery_2)
                    ->groupby('y.coa_id')
                    ->groupby('y.coa_id2')
                    ->groupby('y.volume')
                    ->groupby('y.tarif')
                    ->groupby('y.uraian')
                    ->groupby('y.coa_name')
                    ->groupby('y.rapbs_no')
                    ->groupby('y.satuan')
                    ->groupby('y.jumlah')
                    ->groupby('vol_pakai')
                    ->where('vol_sisa','>',0);
                    // ->orderby('y.coa_id', 'asc');
        $x = $results->get();
        print_r(DB::getQueryLog()); exit;
        return $x;      
    }

    public static function RapbsDetailSave($json_data, $site, $user_id, $rapbs_no)
    {        DB::enableQueryLog(); 
           if (count($json_data) > 0)
        {   // transaction control ikut parent orders
            foreach ($json_data as $json) 
            {   // save hardware
                if (trim($json->remark) == 'REMOVED')
                {   $result = static::where('id', '=', $json->id)->delete();
                    $result = [true, "Save succcefully"];
                }
                else
                {   // save record
                    if ($json->created_date == '')  
                    {   $RapbsDetail = new RapbsDetail();
                        $RapbsDetail->rapbs_no   = trim($rapbs_no);
                    }
                    else 
                    {   // update hardware
                        $RapbsDetail = RapbsDetail::where('id', '=', $json->id)->first();
                        $RapbsDetail->modified_date = date('Ymd');
                    };

                    $RapbsDetail->coa_id   = $json->coa_id;
                    $RapbsDetail->coa_name = $json->coa_name;
                    $RapbsDetail->uraian   = trim($json->uraian);
                    $RapbsDetail->volume   = $json->volume;
                    // $RapbsDetail->vol_sisa    = $json->vol_sisa; //entri waktu apby:pengajuan apby
                    // $RapbsDetail->vol_ajuan   = $json->vol_ajuan; //entri waktu apby:pengajuan apby
                    $RapbsDetail->tarif    = $json->tarif;
                    $RapbsDetail->satuan   = $json->satuan;
                    $RapbsDetail->jumlah   = $json->jumlah;
                    $RapbsDetail->remark   = $json->remark;
                    
                    // $RapbsDetail->jum_ajuan   = $json->jum_ajuan; //entri waktu apby:pengajuan apby
                    $RapbsDetail->berulang   = $json->berulang;
                    // $RapbsDetail->status   = $json->status;

                    // $RapbsDetail->status   = $RapbsDetail->setStatus($json->status);

                    $result = $RapbsDetail->save();
                };
            };
        };
        // print_r(DB::getQueryLog());exit; 
        // return $result; (bingung ini harus ditanyakan)
    }

    public static function RapbsDetailDelete($rapbs_no)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('rapbs_no', '=', $rapbs_no)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    public static function RapbsDetailBiayaDelete($request)
    {
        DB::beginTransaction();
        try {
              $result = static::where('id', '=', $request->id)->delete();    
                        
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parentRapbsMaster()
    {   // return $this->belongsTo(parentModel, current_column, parent_column)
        return $this->belongsTo('App\Models\Anggaran\RapbsMaster', 'rapbs_no', 'rapbs_no');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
