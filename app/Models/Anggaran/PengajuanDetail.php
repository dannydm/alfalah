<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use DB;

class PengajuanDetail extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------*/
    // create sequence pengajuan_detail_seq;
    // CREATE TABLE public.pengajuan_detail
    // (
    //   id numeric(18,0) NOT NULL DEFAULT nextval('pengajuan_detail_seq'::regclass),
    //   rapbs_no character varying(20) NOT NULL,
    //   coa_id character varying(8),
    //   coa_name character varying(100),
    //   uraian character varying(256),
    //   volume numeric(5,2),
    //   tarif numeric(15,2),
    //   satuan character varying(20),
    //   jumlah numeric(15,2) DEFAULT 0,
    //   berulang numeric(5,2) DEFAULT 0,
    //   status numeric(1,0) DEFAULT 0,
    //   created_date timestamp without time zone NOT NULL DEFAULT now(),
    //   modified_date timestamp without time zone DEFAULT now(),
    //   CONSTRAINT pengajuan_detail_pk PRIMARY KEY (rapbs_no, id),
    //   CONSTRAINT rapbsmaster1_fk FOREIGN KEY (rapbs_no) references rapbs_master(rapbs_no)
    // );
    /*
    
     *//*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'pengajuan_detail';
	protected $primaryKey = 'pengajuan_detail_pk';
    public $sequence_name = 'pengajuan_detail_seq';
    
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function detailPrintOut($request) 
    {   $results = DB::table('pengajuan_detail as a')
        ->leftjoin('rapbs_master as x1', 'x1.rapbs_no', '=', 'a.rapbs_no')
        ->select(DB::raw("
            a.pengajuan_no, a.rapbs_no,x1.sub_kegiatan_mrapbs_id, a.jumlah
            "))
        // ->where("a.pengajuan_no", "=", $pengajuan_no)
        ->orderby('a.rapbs_no');

        return $results->get();
    }

    public static function pengajuandetailPrintOut($pengajuan_no) 
    {   $results = DB::table('pengajuan_detail as a')
            ->leftjoin('pengajuan_master as x1', 'x1.pengajuan_no', '=', 'a.pengajuan_no')
            // ->leftjoin('rapbs_master as x2','x2.rapbs_no','=','a.rapbs_no')
            ->select(DB::raw("
                a.pengajuan_no, a.rapbs_no, a.coa_id, a.coa_name, 
                a.uraian, a.vol_ajuan, x1.sub_kegiatan_mrapbs_id as sub_kegiatan, x1.hasil as hasil_ang, 
                a.tarif, a.satuan, a.jum_ajuan,
                (   select sum(b.jum_ajuan) 
                from pengajuan_detail as b 
                where b.rapbs_no = a.rapbs_no and b.pengajuan_no = a.pengajuan_no
                ) as total_biaya 
                "))
            ->where("a.pengajuan_no", "=", $pengajuan_no)
            ->orderby('a.pengajuan_no');

            // if ($request->pt) {   return $results->first();   }
            // else {  return $results->get(); };
        return $results->get();
    }

    public static function kegiatandetailPrintOut($pengajuan_no) 
    {   DB::enableQueryLog();
           $results = DB::table('pengajuan_detail as a')
           ->leftjoin('rapbs_master as b', 'b.rapbs_no', '=','a.rapbs_no')
            ->select(DB::raw("
                a.rapbs_no, b.sub_kegiatan_mrapbs_id as sub_kegiatan, b.hasil as hasil_ang, 
                (   select sum(c.jum_ajuan) 
                from pengajuan_detail as c 
                where c.rapbs_no = a.rapbs_no and c.pengajuan_no = a.pengajuan_no  
                ) as total_biaya 
                "))
            ->where("a.pengajuan_no", "=", $pengajuan_no)
            ->groupby('a.rapbs_no','sub_kegiatan','hasil_ang','a.pengajuan_no')
            ->orderby('a.rapbs_no');

            return $results->get();

    }


    public static function kegiatanprint($request) // mode pencarian dari daftar pengajuan (view kegiatan)
    {  
        $results = DB::table('pengajuan_master as a')
        ->leftjoin('pengajuan_detail as q1', 'q1.pengajuan_no', '=','a.pengajuan_no')
        ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
        ->select(DB::raw("q1.pengajuan_no,
                x1.name as organisasi_mrapbs_id_name,
                (   select sum(b.jum_ajuan) 
                from pengajuan_detail as b 
                where b.pengajuan_no = q1.pengajuan_no 
                ) as total_pengajuan
                "))
        ->orderby('q1.pengajuan_no', 'asc');

    // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->pengajuan_no) 
            {   $results->where("a.pengajuan_no", "ilike", "%" . $request->pengajuan_no . "%");
            };
            // if ($request->rapbs_no) 
            // {   $results->where("rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            // };
            if ($request->organisasi_mrapbs_id_name)  
            {   $results->where("organisasi_mrapbs_id_name", "ilike", "%" . $request->organisasi_mrapbs_id_name . "%");
            };

            if ($request->pt) {   return $results->first();   }
            else {  return $results->get(); };
    
        };
        return $results->get();
    }


    public static function PengajuanDetailSearch($request)
    {   //$results = static::orderby('id', 'asc');  
        //return $results->get();


        $results = DB::table('pengajuan_detail as a') 
                ->select(DB::raw("a.*, '' as action"))
            ->orderby('a.id', 'asc');
        
        // searchForm parameter
        if ($request->s == "form") 
        {
            if ($request->pengajuan_no) 
            {   $results->where("pengajuan_no", "ilike", "%" . $request->pengajuan_no . "%");
            };

            if ($request->rapbs_no) 
            {   $results->where("rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };

        };

        return $results->get();
    }

   
    public static function KegiatanDetailSearch($request)
    { 

        $results = DB::table('pengajuan_detail as a')
                ->select(DB::raw("a.*, '' as action"))
            ->orderby('a.id', 'asc');
        
        // searchForm parameter
        if ($request->s == "forms") 
        {
            if ($request->pengajuan_no) 
            {   $results->where("pengajuan_no", "ilike", "%" . $request->pengajuan_no . "%");
            };

            if ($request->rapbs_no) 
            {   $results->where("rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };

        };

        return $results->get();
    }

    public static function AppDetailSearch($request)
    { 
        $results = DB::table('pengajuan_detail as a')
            ->leftjoin('rapbs_master as b','b.rapbs_no','=','a.rapbs_no')
            ->select(DB::raw("a.*, '' as action"))
            ->where('a.pengajuan_no','=',$request->pengajuan_no)
            ->orderby('a.id', 'asc');
        // searchForm parameter
        if ($request->s == "forms") 
        {
            if ($request->pengajuan_no) 
            {   $results->where("pengajuan_no", "ilike", "%" . $request->pengajuan_no . "%");
            };

            if ($request->rapbs_no) 
            {   $results->where("rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
            };
        };
        return $results->get();
    }


    public static function PengajuanDetailDelete($rapbs_no)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('rapbs_no', '=', $rapbs_no)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

    public static function PengajuanDetailSave( $json_data, $site, $user_id, $pengajuan_no)
     {   if (count($json_data) > 0) { 
         // transaction control ikut parent orders
        //  print_r($pengajuan_no);exit;
        foreach ($json_data as $json) 
            {  // save hardware

                $PengajuanDetail = new PengajuanDetail();
                $PengajuanDetail->pengajuan_no   = trim($pengajuan_no);
                
                $PengajuanDetail->rapbs_no  = $json->rapbs_no;
                $PengajuanDetail->coa_id    = $json->coa_id;
                $PengajuanDetail->coa_name  = $json->coa_name;
                $PengajuanDetail->uraian    = $json->uraian;
                if ($json->vol_sisa == 0 && $json->jum_ajuan > 0  ) 
                { $PengajuanDetail->vol_sisa    = 0 ;}
                else ($PengajuanDetail->vol_sisa  = $json->vol_sisa-$json->vol_ajuan);
                
                $PengajuanDetail->tarif     = $json->tarif;
                $PengajuanDetail->satuan    = $json->satuan;
                $PengajuanDetail->jumlah    = $json->jumlah;
                $PengajuanDetail->vol_ajuan = $json->vol_ajuan; //entri waktu apby:pengajuan apby
                  //entri waktu apby:pengajuan apby
                $PengajuanDetail->jum_ajuan = $json->jum_ajuan; //entri waktu apby:pengajuan apby

                // $PengajuanDetail->berulang = $json->berulang;

                $result = $PengajuanDetail->save();
            };
        };
        return $result;
    }

    // public static function KegiatanReject($rapbs_no,$txt , $user_id)
    // { //  print_r($results); exit;
    //     DB::beginTransaction();
    //     try { 
    //         $results = static::where('rapbs_no','=',$rapbs_no)
    //         ->orderby('rapbs_no','asc')
    //         // ->where('approve_date', null)
    //         ->get();

    //         if ($results)
    //         { // print_r($results); exit; 
    //             foreach ($results as $result) {
    //                 //  print_r($result); exit;
    //                 $pengajuan_detail = PengajuanDetail::where("rapbs_no", "=", $result->rapbs_no)
    //                 ->first();  
    //                 $pengajuan_detail->approve_date   = date('Ymd his');
    //                 $pengajuan_detail->approve_status = 2; // 0 = approved, 1 = not yet approved, 2 = rejected
    //                 $pengajuan_detail->approve_reason   = $txt;
    //                 $pengajuan_detail->approve_by   = $user_id;
 
    //                 // print_r($pengajuan_detail); exit;
    //                 $pengajuan_detail->save();
    //                 }
    //             $result = [true, "Save succcefully"];
    //             DB::commit();
    //             }
    //         else
    //         {   $result = [false, "No Data Found"];
    //             DB::rollback();
    //         };          
    //     } catch (\Illuminate\Database\QueryException $e) {
    //         // rollback transaction
    //         $result = [false, "Error Executed ---> " . $e];
    //         DB::rollback();
    //     };
    //     return $result;
    // }

    // public static function KegiatanReject1($rapbs_no, $txt)
    // { //  print_r($txt); exit;
    //     DB::beginTransaction();
    //     try {
    //         $pengajuan_detail = PengajuanDetail::where("rapbs_no", "=", $rapbs_no)
    //         ->first();  
    //         $pengajuan_detail->approve_date   = date('Ymd his');
    //         $pengajuan_detail->approve_status = 2; // 0 = approved, 1 = not yet approved, 2 = rejected
    //         $pengajuan_detail->approve_reason   = $txt;

    //         $result = $pengajuan_detail->save();

    //         $result = [true, "Save succcefully"];
    //         DB::commit();
    //     } 
    //     catch (\Illuminate\Database\QueryException $e) {
    //         // rollback transaction
    //         $result = [false, "Error Executed ---> " . $e];
    //         DB::rollback();
    //     };
    //     return $result;
    // }

    public static function PengajuanDetailReject($pengajuan_no) // klo berhasil hapus yg diatas
    {   DB::beginTransaction();
        try 
        {   $result = static::where('pengajuan_no', '=', $pengajuan_no)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }

 
    public static function UangMukaDetailSearch($request, $json_data, $pengajuan_no)
    {  // print_r($json_data);exit;
         DB::enableQueryLog();
        $subQuery = DB::table('pengajuan_detail as a')
            ->select(DB::raw("a.*, '' as uangmuka_no, b.id as pengajuandetail_id"))
            ->leftjoin('pengajuan_master as c','c.pengajuan_no' ,'=', 'a.pengajuan_no')
            ->leftjoin('uangmuka_detail as b', function($join)
                    {   $join->on('b.rapbs_no', '=', 'a.rapbs_no')
                        ->on('c.pengajuan_no', '=', 'a.pengajuan_no')
                        ->on('b.pengajuan_no','=','a.pengajuan_no')
                        ->on('b.coa_id', '=', 'a.coa_id')
                        ->on('b.uraian', '=', 'a.uraian');
                        
                    });

        $first_row = true;
        // searchForm parameter
        if ($request->s == "forms") 
        {  
            if ( count($json_data) > 0 )
            {   $rapbs_no = array();
                foreach ($json_data as $json) 
            //    print_r($pengajuan_no);exit;
                {  $subQuery->where('a.rapbs_no', '=', $json->rapbs_no)
                    ->where('a.pengajuan_no','=',$pengajuan_no) ;
                };    
            };
        };
        $results = PengajuanDetail::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->whereRaw('x.pengajuandetail_id is null')
                    ->orderby('x.id', 'asc');
        
        $x = $results->get();
//         print_r(DB::getQueryLog());
        // exit;
        return $x;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    // public function pengajuan_detail_pk()
    // {
    //     return $this->hasMany('App\Models\Anggaran\PengajuanDetail', "id", 'pengajuan_no', 'coa_id');
    // }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
