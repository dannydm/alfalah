<?php

namespace App\Models\Anggaran;

use App\Models\AppModel;
use App\Models\Administrator\Mrapbs;
use App\Models\Administrator\MUser;
use App\Models\Administrator\Mprogram;
use App\Models\Ppdb\TahunAjaran;
use App\Models\Anggaran\RapbsMaster;
use App\Models\Anggaran\RapbsDetail;
use App\Models\Anggaran\RapbscopyDetail;
use DB;

class RapbscopyMaster extends AppModel
{   /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
-- Table: public.temp_rapbs_master

-- DROP TABLE public.temp_rapbs_master;

CREATE TABLE public.temp_rapbs_master
(
    id numeric(18,0) NOT NULL,
    rapbs_no character varying(50) COLLATE pg_catalog."default" NOT NULL,
    tahun_ajaran_id numeric(4,0),
    sumberdana_id numeric(4,0),
    coa_id character varying(8) COLLATE pg_catalog."default",
    masukan text COLLATE pg_catalog."default",
    tgt_masukan numeric(5,2),
    keluaran text COLLATE pg_catalog."default",
    tgt_keluaran numeric(5,2),
    hasil text COLLATE pg_catalog."default",
    sasaran text COLLATE pg_catalog."default",
    pic_id character varying(40) COLLATE pg_catalog."default",
    tgl_mulai date,
    tgl_selesai date,
    jumlahn numeric(15,2) DEFAULT 0,
    jumlahke_n numeric(15,2) DEFAULT 0,
    status numeric(1,0) DEFAULT 0,
    created_date timestamp without time zone NOT NULL DEFAULT now(),
    modified_date timestamp without time zone DEFAULT now(),
    sub_kegiatan_mrapbs_id text COLLATE pg_catalog."default",
    created_by numeric(18,0),
    modified_by numeric(18,0),
    approve_copy_date_1 timestamp without time zone,
    approve_copy_by_1 numeric(18,0),
    approve_copy_status_1 numeric(1,0) DEFAULT 0,
    approve_copy_reason_1 character varying(256) COLLATE pg_catalog."default",
    urusan_mrapbs_id character varying(18) COLLATE pg_catalog."default",
    organisasi_mrapbs_id character varying(18) COLLATE pg_catalog."default",
    program_mrapbs_id character varying(18) COLLATE pg_catalog."default",
    kegiatan_mrapbs_id character varying(18) COLLATE pg_catalog."default",
    created_copy_date timestamp without time zone,
    created_copy_by numeric(18,0),
    CONSTRAINT temp_rapbs_master_pk PRIMARY KEY (rapbs_no),
    CONSTRAINT temp_rapbs_master_ux UNIQUE (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.temp_rapbs_master
    OWNER to postgres;
    |--------------------------------------------------------------------------*/
    
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table      = 'temp_rapbs_master';
    protected $primaryKey = 'id';
 //   public $sequence_name = 'uangmuka_master_seq'; 
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    // public function RapbscopyDetail()
    // {   return $this->hasMany('App\Models\Anggaran\RapbscopyDetail', 'rapbs_no');    }

    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function RapbscopySearch($request, $user_id, $level)
    {      // print_r($request);exit;
            $results = DB::table('temp_rapbs_master as a')
            // ->join('user_anggaran as q1','q1.mrapbs_id','=','a.organisasi_mrapbs_id') // ini bermasalah
                    ->leftjoin('user_anggaran as q1',function($join)
                            {$join->on('q1.mrapbs_id','=','a.organisasi_mrapbs_id')
                                ->on('q1.user_id','=','a.created_by');})
                    ->leftjoin('m_rapbs as x1', 'x1.mrapbs_id', '=', 'a.organisasi_mrapbs_id')
                    ->leftjoin('m_rapbs as x2', 'x2.mrapbs_id', '=', 'a.urusan_mrapbs_id')
                    ->leftjoin('m_program as x3', 'x3.mprogram_id', '=', 'a.program_mrapbs_id')
                    ->leftjoin('m_program as x4', 'x4.mprogram_id', '=', 'a.kegiatan_mrapbs_id')
                    ->leftjoin('m_sumberdana as x5', 'x5.sumberdana_id', '=', 'a.sumberdana_id')
                    ->leftjoin('m_coa as x6', 'x6.mcoa_id', '=', 'a.coa_id')
                    ->leftjoin('m_emp as x7', 'x7.emp_id', '=', 'a.pic_id')
                    ->select(DB::raw("a.*, 
                        x1.name as organisasi_mrapbs_id_name, 
                        x2.name as urusan_mrapbs_id_name, 
                        x3.name as program_mrapbs_id_name, 
                        x4.name as kegiatan_mrapbs_id_name, 
                        x5.name as sumberdana_id_name, 
                        x6.name as coa_id_name,
                        x7.name as emp_name,a.status as status,
                        a.keluaran,a.hasil,a.sasaran,a.sub_kegiatan_mrapbs_id,
                        (   select sum(b.jumlah) 
                            from rapbs_detail as b 
                            where b.rapbs_no = a.rapbs_no
                        ) as total_biaya
                    "))
    
                // ->where("a.approve_status_1","=","0")
     //                       ->where('a.created_by','=',$user_id)
     //           ->where('a.tahun_ajaran_id','=',$request->tahun_ajaran_id)
                ->orderby('a.id', 'asc');
    
            // print($results);exit;
            // searchForm parameter
            if ($request->s == "form") 
            {   
    
                if ($request->tahun_ajaran_id) 
                {   $results->where("a.tahun_ajaran_id", "ilike", "%" . $request->tahun_ajaran_id. "%");
                };
                if ($request->organisasi_mrapbs_id_name) 
                {   $results->where("x1.name", "ilike", "%" . $request->organisasi_mrapbs_id_name. "%");
                };
                if ($request->urusan_mrapbs_id_name)
                {   $results->where("x2.name", "ilike", "%".$request->urusan_mrapbs_id_name."%");  
                };
                if ($request->program_mrapbs_id_name)
                {   $results->where("x3.name", "ilike", "%".$request->program_mrapbs_id_name."%");  
                };
                if ($request->kegiatan_mrapbs_id_name)
                {   $results->where("x4.name", "ilike", "%".$request->kegiatan_mrapbs_id_name."%");  
                };
                if ($request->sub_kegiatan_mrapbs_id)
                {   $results->where("a.sub_kegiatan_mrapbs_id", "ilike", "%".$request->sub_kegiatan_mrapbs_id."%");  
                };
                if ($request->rapbs_no) 
                {   $results->where("a.rapbs_no", "ilike", "%" . $request->rapbs_no . "%");
                };
    
                if ($level == 2) //admin level
                {}
                else { $results->where('a.created_by','=',$user_id); }; 
    
            };
    
            return $results->get();
    
            }


    public static function UangmukaMasterSearchExt($request)
    {
        
        $subQuery = DB::table('m_rapbs as a')
        ->selectRaw("a.mrapbs_id, a.name,
        (a.name || ' [' || a.mrapbs_id|| '] ') as display")
        ->where('a.status', '=', 0);

        $results = UangmukaMaster::selectRaw("x.*")
            ->from(\DB::raw(' ( ' . $subQuery->toSql() . ' ) as x'))
            ->mergeBindings($subQuery)
            ->orderby('x.id', 'desc');

        if ($request->s == "form") {
            if ($request['query']) {
                $results->where("x.display", "ilike", "%" . $request['query'] . "%");
            };
        };
        return $results->get();
    }

    public static function rapbscopySave($json_data, $site, $user_id, $company_id)
    {   // begin transaction

        DB::beginTransaction();
        try {                 
            
//               print_r($head->rapbs_no);exit;
        foreach ($json_data as $data)
        {       //    print_r($data);exit;
                $rapbscopy_master = new RapbscopyMaster();
                $rapbscopy_master->created_date = date('Ymd');
                $rapbscopy_master->created_by   = $user_id;
                $rapbscopy_master->rapbs_no               = $data->rapbs_no;
                $rapbscopy_master->tahun_ajaran_id       = $data->tahun_ajaran_id;
                $rapbscopy_master->urusan_mrapbs_id       = $data->urusan_mrapbs_id;
                $rapbscopy_master->organisasi_mrapbs_id   = $data->organisasi_mrapbs_id;
                $rapbscopy_master->program_mrapbs_id      = $data->program_mrapbs_id;
                $rapbscopy_master->kegiatan_mrapbs_id     = $data->kegiatan_mrapbs_id;
                $rapbscopy_master->sub_kegiatan_mrapbs_id = $data->sub_kegiatan_mrapbs_id;
                $rapbscopy_master->sumberdana_id          = $data->sumberdana_id;
                $rapbscopy_master->hasil                  = $data->hasil;
                $rapbscopy_master->pic_id                 = $data->pic_id;
                $rapbscopy_master->masukan                = $data->masukan;
                $rapbscopy_master->tgt_masukan            = $data->tgt_masukan;
                $rapbscopy_master->keluaran               = $data->keluaran;
                $rapbscopy_master->tgt_keluaran           = $data->tgt_keluaran;
                $rapbscopy_master->hasil                  = $data->hasil;
                $rapbscopy_master->sasaran                = $data->sasaran;
                $rapbscopy_master->pic_id                 = $data->pic_id; 
                $rapbscopy_master->tgl_mulai              = $data->tgl_mulai;
                $rapbscopy_master->tgl_selesai            = $data->tgl_selesai;
                $rapbscopy_master->jumlahn                = $data->total_biaya;
                $rapbs_no = $rapbscopy_master->rapbs_no;
                $result = $rapbscopy_master->save();

            // save detail data
            if ($result) 
                {   $result = RapbscopyDetail::CopyDetailSave(
                            $json_data,
                            $site,
                            $user_id,
                            $rapbs_no);
                };
        }
            // commit transaction
            $result = [true, "Save succesfully"];
            DB::commit();
        } 
        catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> " . $e];
            DB::rollback();
        };
        return $result;
        }

        public static function RapbsMastertempSave($data, $json_data, $site, $user_id, $company_id)
        {   // begin transaction
            DB::beginTransaction();
            DB::enableQueryLog(); 
            try {
                
                if ($data->created_date == '')  
                {   
                    if ($data->tahun_ajaran_id == ''){} 
                    else
                    {    
                        $company_id = "YMDT";
                        $Document = DB::select( DB::raw(" 
                            select b.*, c.doc_code 
                            from m_rapbs as a 
                            join site_documents as b on ( b.company_id = '".$company_id."' 
                                and b.site_id = '".$site."' and b.dept_id = a.dept_id ) 
                            join m_documents as c on ( c.doc_id = b.doc_id )
                            where a.mrapbs_id = '".$data->urusan_mrapbs_id."'"
                        ) );
                        $doc_code = $Document[0]->doc_code;
                        $seq_no = $Document[0]->last_doc_no + 1;
                        
                        $doc_id = $Document[0]->id;
                        $SiteDocument = SiteDocument::where('id', '=', $doc_id)->first();
                        $SiteDocument->last_doc_no = $seq_no;
                        
                        $result = $SiteDocument->save();
                        $TahunAjaran = TahunAjaran::where('tahunajaran_id', '=', $data->tahun_ajaran_id)->first(); 
                        $rapbs_no = $TahunAjaran->tahunajaran_id.'/RAB/'.trim($doc_code).'/'.$seq_no;
                        
                       // print_r($rapbs_no);exit;
    
                        $rapbs_master = new RapbscopyMaster();
                        $rapbs_master->created_date = date('Ymd'); 
                        $rapbs_master->created_by   = $user_id;
                        $rapbs_master->rapbs_no = $rapbs_no;
                    };
                    // print_r($company_id);exit;
                } 
                else 
                {   
                    $rapbs_master = RapbscopyMaster::where('id', '=', $data->id)->first();
                    $rapbs_master->modified_date = date('Ymd');
                    $rapbs_master->modified_by   = $user_id;
                    $rapbs_no                    = $rapbs_master->rapbs_no;
                };
     //         print_r($json_data);exit;
                $rapbs_master->tahun_ajaran_id        = $data->tahun_ajaran_id;
                $rapbs_master->organisasi_mrapbs_id   = $data->organisasi_mrapbs_id;
                $rapbs_master->urusan_mrapbs_id       = $data->urusan_mrapbs_id ;
                $rapbs_master->program_mrapbs_id      = $data->program_mrapbs_id;
                $rapbs_master->kegiatan_mrapbs_id     = $data->kegiatan_mrapbs_id;
                $rapbs_master->sub_kegiatan_mrapbs_id = $data->sub_kegiatan_mrapbs_id;
                $rapbs_master->sumberdana_id          = $data->sumberdana_id;
                $rapbs_master->coa_id                 = $data->coa_id;
                $rapbs_master->masukan                = $data->masukan;
                $rapbs_master->tgt_masukan            = $data->tgt_masukan;
                $rapbs_master->keluaran               = $data->keluaran;
                $rapbs_master->tgt_keluaran           = $data->tgt_keluaran;
                $rapbs_master->hasil                  = $data->hasil;
                $rapbs_master->sasaran                = $data->sasaran;
                $rapbs_master->pic_id                 = $data->pic_id; 
                $rapbs_master->tgl_mulai              = $rapbs_master->makeTimeStamp($data->tgl_mulai,'d/m/Y');
                $rapbs_master->tgl_selesai            = $rapbs_master->makeTimeStamp($data->tgl_selesai,'d/m/Y');
                $rapbs_master->jumlahn                = $data->jumlahn;
                $rapbs_master->status                 = $data->status;
                $result = $rapbs_master->save();
    
                // save detail data
                if ($result) 
                {   $result = RapbscopyDetail::RapbsDetailtempSave(
                            $json_data,
                            $site,
                            $user_id,
                            $rapbs_no);
                };
    
                // commit transaction
                $result = [true, "Save succesfully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> " . $e];
                DB::rollback();
            };
            return $result;
        }
        /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
