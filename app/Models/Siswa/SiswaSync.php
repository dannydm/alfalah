<?php

namespace App\Models\Siswa;

use App\Models\AppModel;
use App\Models\Siswa\Siswa;
use DB;

class SiswaSync extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'siswa_sync';
	protected $primaryKey = 'keyid';
    // public $sequence_name = 'siswa_seq_';
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function SyncTable()
    {   DB::beginTransaction();
        try 
        {   // truncate table siswa_sync at first
            $result = SiswaSync::query()->truncate();
            if ($result)
            {   
                $sources = DB::connection('sikap')->select('select * from siswa '); 
                // where tanggal_entri > DATEADD(year,-1,GETDATE())
                if ($sources)
                {
                    foreach ($sources as $source) 
                    {   
                        $sync = SiswaSync::where('nis', '=', $source->nis)->first();

                        if ($sync) { } // do_nothing
                        else 
                        {   // create new record
                            $sync = new SiswaSync();
                            $sync->nis = $source->nis;
                        };

                        $sync->nama_lengkap          = $source->nama_lengkap;
                        $sync->keyid                 = $source->keyid;
                        $sync->no_pendaftaran        = $source->no_pendaftaran;
                        $sync->nama_panggilan        = $source->nama_panggilan;
                        $sync->jenis_kelamin         = $source->jenis_kelamin;
                        $sync->tempat_lahir          = $source->tempat_lahir;
                        $sync->tanggal_lahir         = $source->tanggal_lahir;
                        $sync->alamat_rumah          = $source->alamat_rumah;
                        $sync->no_telpon1            = $source->no_telpon1;
                        $sync->keterangan_no_telpon1 = $source->keterangan_no_telpon1;
                        $sync->no_telpon2            = $source->no_telpon2;
                        $sync->keterangan_no_telpon2 = $source->keterangan_no_telpon2;
                        $sync->keyid_jenjang_sekolah = $source->keyid_jenjang_sekolah;
                        $sync->keyid_tahun_ajaran    = $source->keyid_tahun_ajaran;
                        // $sync->foto                  = $source->foto;
                        $sync->tanggal_entri         = $source->tanggal_entri;
                        $sync->user_entri            = $source->user_entri;
                        $sync->tanggal_edit          = $source->tanggal_edit;
                        $sync->user_edit             = $source->user_edit;
                        $sync->Program_Name          = $source->Program_Name;
                        $sync->tahun_masuk           = $source->tahun_masuk;
                        $sync->sync_date             = date('Ymd his');

                        $result = $sync->save();
                    };
                };

                $result = [true, "Save succcefully"];
                DB::commit();
            }
            else
            {   $result = [false, "Siswa not found "];
                DB::rollback();
            };          
            
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
