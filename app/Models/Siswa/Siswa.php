<?php

namespace App\Models\Siswa;

use App\Models\AppModel;
use App\Models\Administrator\MUser;
use App\Models\Administrator\RoleUser;

use App\User;
use Illuminate\Support\Facades\Hash;
use DB;

class Siswa extends AppModel
{	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'siswa';
	protected $primaryKey = 'keyid';
    // public $sequence_name = 'siswa_seq_';
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Receivable()
    {
        return $this->hasMany('App\Models\Siswa\SiswaReceivable', 'keyid', 'keyid_siswa');
    }
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getStatus($status)
    {   switch($status)
        {   case 0: $result = "Active";  break;
            case 1: $result = "InActive";  break;
        };
        return $result;
    }
    public static function setStatus($status)
    {   switch($status)
        {   case "Active":   $result = 0; break;
            case "InActive": $result = 1; break;
        };
        return $result;
    }
    public static function mapJenjang($jenjang)
    {   switch($status)
        {   case 0: $result = "ALL";  break;
            case 1: $result = "SD";  break;
            case 2: $result = "SMP";  break;
            // case 3: $result = "";  break;
            case 5: $result = "SD ICP";  break;
            case 6: $result = "KB";  break; // PRA SEKOLAH
            case 7: $result = "TK";  break; 
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function ListJenjangSekolah($request)
    {   
        $results = DB::table('jenjang_siswa')
            ->select(DB::raw("distinct(nama_jenjang_sekolah) as jenjang_sekolah, keyid_jenjang_sekolah"))
            ->orderBy('keyid_jenjang_sekolah', 'asc');
        return $results->get();
    }
    public static function ListNamaKelas($request)
    {   
        $results = DB::table('jenjang_siswa')
            ->select(DB::raw("distinct(nama_kelas) as nama_kelas, keyid_kelas"))
            ->orderBy('keyid_kelas', 'asc');

        if ($request->s == "form")
        {   if ($request['jenjang_sekolah'])
            {   $results->where("nama_jenjang_sekolah", "ilike", "%".$request['jenjang_sekolah']."%");   };
        };

        return $results->get();
    }
    public static function ListRuangKelas($request)
    {   
        $subQuery = DB::table('jenjang_siswa')
                    ->select(DB::raw("distinct( nama_kelas ||' -- '|| nama_nama_kelas) as ruang_kelas, keyid_nama_kelas"));

        if ($request->s == "form")
        {   if ($request['jenjang_sekolah'])
            {   $subQuery->where("nama_jenjang_sekolah", "ilike", "%".$request['jenjang_sekolah']."%");   };

            if ($request['nama_kelas'])
            {   $subQuery->where("nama_kelas", "ilike", "%".$request['nama_kelas']."%");   };
        };

        $results = Siswa::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->Orderby('x.ruang_kelas', 'asc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.ruang_kelas", "ilike", "%".$request['query']."%");   };
        };

        return $results->get();
    }
    public static function Search($request)
    {   
        $results = DB::table('siswa as a')
            ->join('jenjang_siswa as b', 'b.keyid_siswa', '=', 'a.keyid')
            ->select(DB::raw(" (ROW_NUMBER () OVER (ORDER BY a.keyid)) as record_id, 
                a.*, b.nama_jenjang_sekolah, 
                ( b.nama_kelas ||' -- '|| b.nama_nama_kelas) as nama_kelas,
                (select 1 from users where users.name = nis) as is_user"))
            ->orderBy('a.nis', 'asc');

        if ($request->s == "form")
        {   
            // if ( $request->no_pendaftaran )
            // {   $results->where("no_pendaftarana", "=", $request->no_pendaftaran); };

            if ( $request->no_pendaftaran )
            {   $results->where("a.no_pendaftaran", "ilike", 
                    "%".$request->no_pendaftaran."%"); };

            if ($request->nis)
            {   $results->where("a.nis", "ilike", "%".$request->nis."%");   };
                
            if ($request->nama_lengkap)
            {   $results->where("a.nama_lengkap", "ilike", 
                    "%".$request->nama_lengkap."%");   };
            if ($request->nama_panggilan)
            {   $results->where("a.nama_panggilan", "ilike", 
                    "%".$request->nama_panggilan."%");   };
            if ($request->alamat_rumah)
            {   $results->where("a.alamat_rumah", "ilike", 
                    "%".$request->alamat_rumah."%");   };
            
            if ($request->jenis_kelamin == "ALL"){} 
            else
                {   $results->where("a.jenis_kelamin", "=", 
                    $request->jenis_kelamin);   };

            if ($request->jenjang_sekolah == "ALL"){} 
            else
                {   $results->where("b.nama_jenjang_sekolah", "=", 
                    $request->jenjang_sekolah);   };

            if ($request->nama_kelas == "ALL"){} 
            else
                {   $results->where("b.nama_kelas", "=", 
                    $request->nama_kelas);   };

            if ($request->ruang_kelas == "ALL"){} 
            else
                {   $results->where("b.nama_nama_kelas", "=", 
                    $request->ruang_kelas);   };
        };
        
        return $results->get();
        
        // if ($request->pt) {   return $results->first();   }
        // else {  return $results->get(); };
    }
    public static function SearchExt($request)
    {   
        $subQuery = DB::table('Siswa as a')
                    ->selectRaw("a.Siswa_no, 
                                (a.Siswa_no) as display")
                    ->where('a.status', '=', 0);

        $results = Siswa::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->Siswaby('x.Siswa_no', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function headerPrintOut($request)
    {   
        $results = DB::table('Siswa as a')
            ->leftjoin('m_company as x1', 'x1.company_id', '=', 'a.company_id')
            ->leftjoin('m_container_type as x2', 'x2.container_type_id', '=', 'a.container_type')
            ->leftjoin('m_city as x3', 'x3.city_id', '=', 'a.port_loading')
            ->leftjoin('m_city as x4', 'x4.city_id', '=', 'a.port_discharge')
            ->leftjoin('m_company as x5', 'x5.company_id', '=', 'a.buyer_id')
            ->select(DB::raw("
                trim(a.id) as id, a.Siswa_no, a.company_id, a.currency_id, a.description, 
                a.buyer_id, a.buyer_name, 
                to_char(a.Siswa_date, 'dd/mm/yyyy') as Siswa_date, 
                a.pi_no, to_char(a.pi_date, 'dd/mm/yyyy') as pi_date, 
                a.bank_id, a.bank_name, a.bank_account, a.bank_address, a.swift_code, 
                a.container_type, a.payment_term, a.payment_term_id,
                a.port_loading, a.port_discharge, 
                coalesce(a.cbm,0) as cbm, 
                coalesce(a.weight_gross, 0) as weight_gross, 
                coalesce(a.weight_nett, 0) as weight_nett,
                to_char(a.shipment_date,'dd/mm/yyyy') as shipment_date, 
                to_char(a.etd, 'dd/mm/yyyy') as etd, 
                to_char(a.eta,  'dd/mm/yyyy') as eta,
                x1.name as company_name, x1.address as company_address,
                x2.description as container_type_name,
                x3.name as port_loading_name, 
                x4.name as port_discharge_name,
                x5.address as buyer_address
                "))
            ->Siswaby('a.id');

        if ($request->s == "form")
        {   
            if ( $request->id )
            {   $results->where("a.id", "=", $request->id); };
        };

        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };
    }
    public static function Siswasave(   $head_data, $json_data, $modi_data, 
                                        $site, $user_id, $company_id    )
    {   if (count($head_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($head_data as $json) 
                {   // save Siswa
                    if ($json->created_date == '')  
                    {   $Siswa = new Siswa();
                        $Siswa->id         = self::getNextID(self::sequence_name, $site);
                        // $Siswa->company_id = env('APP_COMPANY');
                        $Siswa->company_id = config('app.system_company');
                        $Siswa->Siswa_no   = $json->Siswa_no;
                        $Siswa->Siswa_date = $json->Siswa_date;
                        $Siswa->created_by = $user_id;

                        $Siswa_detail = new SiswaDetail();
                    }
                    else 
                    {   // update Siswa
                        $Siswa = Siswa::where('Siswa_no', '=', $json->Siswa_no)->first();
                        $Siswa->modified_by = $user_id;

                        $Siswa_detail = SiswaDetail::where('Siswa_no', '=', $json->Siswa_no)->first();
                        if (Empty($Siswa_detail)) { $Siswa_detail = new SiswaDetail(); };
                    };

                    $Siswa->repeat_Siswa_no = $json->repeat_Siswa_no;
                    $Siswa->buyer_id        = $json->buyer_id;
                    $Siswa->buyer_name      = $json->buyer_name;
                    $Siswa->currency_id     = $json->currency_id;
                    $Siswa->pi_no           = $json->pi_no;
                    
                    if (Empty($json->pi_date ) && Empty(trim($json->pi_no)))
                    {   $Siswa->pi_no = null;
                        $Siswa->pi_date = null;
                    }
                    else if ($json->pi_date && Empty(trim($json->pi_no)))
                    {   $sitedocument = new SiteDocument();
                        $Siswa->pi_no = $sitedocument->getDocNum(config('app.system_company'), 
                                            $site, 10, 1001, $json->buyer_id, "");
                        $Siswa->pi_date     = $json->pi_date;
                    } else { $Siswa->pi_date= $json->pi_date; };

                    $Siswa->bank_id         = $json->bank_id;
                    $Siswa->bank_name       = $json->bank_name;
                    $Siswa->bank_account    = $json->bank_account;
                    $Siswa->bank_address    = $json->bank_address;
                    $Siswa->swift_code      = $json->swift_code;
                    $Siswa->container_type  = $json->container_type;
                    $Siswa->cbm             = $json->cbm;
                    $Siswa->port_loading    = $json->port_loading;
                    $Siswa->port_discharge  = $json->port_discharge;
                    $Siswa->weight_gross    = $json->weight_gross;
                    $Siswa->weight_nett     = $json->weight_nett;
                    $Siswa->payment_term    = $json->payment_term;
                    $Siswa->payment_term_id = $json->payment_term_id;

                    if ( $json->shipment_date )
                    {   $Siswa->shipment_date = $json->shipment_date.' 00:00';  } 
                    else {   $Siswa->shipment_date = null;  };

                    if ( $json->etd )
                    {   $Siswa->etd = $json->etd.' 00:00';  } 
                    else {   $Siswa->etd = null;  };

                    if ( $json->eta )
                    {   $Siswa->eta = $json->eta.' 00:00';  } 
                    else {   $Siswa->eta = null;  };
                    
                    $result = $Siswa->save();
                    // save Siswa details

                    if ($result)
                    {   if (count($json_data) > 0)
                        {   $result = $Siswa_detail->SiswadetailSave( $json_data, 
                                            $site, $user_id, $company_id, $json->Siswa_no); 
                        };
                    };
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function Profile($nis)
    {   
        $result = DB::table('siswa as a')
            ->join('jenjang_siswa as b', 'b.keyid_siswa', '=', 'a.keyid')
            ->select(DB::raw(" (ROW_NUMBER () OVER (ORDER BY a.keyid)) as record_id, 
                a.*, b.nama_jenjang_sekolah, 
                ( b.nama_kelas ||' -- '|| b.nama_nama_kelas) as nama_kelas,
                (select 1 from users where users.name = nis) as is_user"))
            ->where('nis', '=', $nis)->first();

        return $result;
    }
    public static function SiswaDelete($request)
    {   
        DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)
                        ->where('asset_id', '=', $request->asset_id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function RegisterUser($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('keyid', '=', $request->id)->first();
            if ($result)
            {   // register to laravel user
                $user           = new User();
                $user->password = Hash::make(trim($result->tanggal_lahir));
                $user->email    = trim($result->nis).'@alfalah.com';
                $user->name     = trim($result->nis);
                $user->save();

                if ($user)
                {   // register to Master user
                    $muser = new MUser();
                    $muser->user_id     = $user->id;
                    $muser->password    = "default";
                    $muser->username    = $user->name;
                    $muser->name        = $result->nama_lengkap;
                    $muser->auth_email  = $user->email;
                    $muser->status      = 0;
                    $muser->laravel_id  = $user->id;
                    $muser->note        = 'student';
                    
                    $muser->save();

                    if ($muser)
                    {   // register to Role user
                        $roleuser = new RoleUser();
                        $roleuser->user_id     = $user->id;
                        $roleuser->role_id     = 100;
                        $roleuser->save();
                        if ($roleuser)
                        {
                            $result = [true, "Save succcefully"];
                            DB::commit();
                        }
                        else
                        {   $result = [false, "Failed to Save on Role User"];
                            DB::rollback();
                        };
                    }
                    else
                    {   $result = [false, "Failed to Save on Master User"];
                        DB::rollback();
                    };
                }
                else
                {   $result = [false, "Failed to Save on Lvl User"];
                    DB::rollback();
                };
            }
            else
            {   $result = [false, "No Data Found"];
                DB::rollback();
            };          
            
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function RegisterAllUsers($request)
    {   DB::beginTransaction();
        try 
        {   $results = static::selectRaw(" *, users.id as user_id")
                ->leftjoin('users', 'users.name', '=', 'siswa.nis')
                ->orderBy('siswa.nis', 'asc')
                ->where('users.id', null)
                ->get();

            if ($results)
            {   // register to laravel user
                foreach ($results as $result) 
                {   //print_r($result);
                    $user           = new User();
                    $user->password = Hash::make(trim($result->tanggal_lahir));
                    $user->email    = trim($result->nis).'@alfalah.com';
                    $user->name     = trim($result->nis);
                    $user->save();

                    if ($user)
                    {   // register to Master user
                        $muser = new MUser();
                        $muser->user_id     = $user->id;
                        $muser->password    = "default";
                        $muser->username    = $user->name;
                        $muser->name        = $result->nama_lengkap;
                        $muser->company_id  = 'YMDT';
                        $muser->site_id     = "SBY";
                        $muser->dept_id     = $result->keyid_jenjang_sekolah;
                        $muser->auth_email  = $user->email;
                        $muser->status      = 0;
                        $muser->laravel_id  = $user->id;
                        
                        $muser->save();

                        if ($muser)
                        {   // register to Role user
                            $roleuser = new RoleUser();
                            $roleuser->user_id     = $user->id;
                            $roleuser->role_id     = 100;
                            $roleuser->save();
                            if ($roleuser)
                            {
                                $result = [true, "Save succcefully"];
                                DB::commit();
                            }
                            else
                            {   $result = [false, "Failed to Save on Role User"];
                                DB::rollback();
                            };
                        }
                        else
                        {   $result = [false, "Failed to Save on Master User"];
                            DB::rollback();
                        };
                    }
                    else
                    {   $result = [false, "Failed to Save on Lvl User"];
                        DB::rollback();
                    };
                };
            }
            else
            {   $result = [false, "No Data Found"];
                DB::rollback();
            };          
            
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function ResetPassword($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('keyid', '=', $request->id)->first();
            if ($result)
            {   // register to laravel user
                $user = User::where('name', '=', $result->nis)->first();
                if ($user)
                {   
                    $user->password = Hash::make(trim($result->tanggal_lahir));
                    $user->save();
                    if ($user)
                    {
                        $result = [true, "Save succcefully"];
                        DB::commit();
                    }
                    else
                    {   $result = [false, "Failed to Save on Role User"];
                        DB::rollback();
                    };
                }
                else
                {   $result = [false, "User not found "];
                    DB::rollback();
                };
            }
            else
            {   $result = [false, "Siswa not found "];
                DB::rollback();
            };          
            
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function SyncTable()
    {   DB::beginTransaction();
        try 
        {   // truncate table siswa_sync at first
            $sources = DB::connection('sikap')->select('select * from siswa '); 
            // where tanggal_entri > DATEADD(year,-1,GETDATE())
            if ($sources)
            {
                foreach ($sources as $source) 
                {   
                    $sync = Siswa::where('nis', '=', $source->nis)->first();

                    if ($sync) { } // do_nothing
                    else 
                    {   // create new record
                        $sync = new Siswa();
                        $sync->nis = $source->nis;
                    };

                    $sync->nama_lengkap          = $source->nama_lengkap;
                    $sync->keyid                 = $source->keyid;
                    $sync->no_pendaftaran        = $source->no_pendaftaran;
                    $sync->nama_panggilan        = $source->nama_panggilan;
                    $sync->jenis_kelamin         = $source->jenis_kelamin;
                    $sync->tempat_lahir          = $source->tempat_lahir;
                    $sync->tanggal_lahir         = $source->tanggal_lahir;
                    $sync->alamat_rumah          = $source->alamat_rumah;
                    $sync->no_telpon1            = $source->no_telpon1;
                    $sync->keterangan_no_telpon1 = $source->keterangan_no_telpon1;
                    $sync->no_telpon2            = $source->no_telpon2;
                    $sync->keterangan_no_telpon2 = $source->keterangan_no_telpon2;
                    $sync->keyid_jenjang_sekolah = $source->keyid_jenjang_sekolah;
                    $sync->keyid_tahun_ajaran    = $source->keyid_tahun_ajaran;
                    // $sync->foto                  = $source->foto;
                    $sync->tanggal_entri         = $source->tanggal_entri;
                    $sync->user_entri            = $source->user_entri;
                    $sync->tanggal_edit          = $source->tanggal_edit;
                    $sync->user_edit             = $source->user_edit;
                    $sync->Program_Name          = $source->Program_Name;
                    $sync->tahun_masuk           = $source->tahun_masuk;
                    $sync->sync_date             = date('Ymd his');

                    $sync->save();
                };
                DB::commit();
                $result = [true, "Syncronize table succcefull "];
            }
            else
            {   $result = [false, "Siswa not found "];
                DB::rollback();
            };          
            
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function ChartbyJenjang()
    {   
        $subQuery = DB::table('siswa as a')
                        ->selectRaw("
                            a.keyid_jenjang_sekolah, 
                            b.name as jenjang_sekolah_name, 
                            count(1) as total ")
                        ->join('m_department as b', 'b.dept_id', '=', 'a.keyid_jenjang_sekolah')
                        // ->where('a.tahunajaran_id', '=', '2019')
                        ->groupby('a.keyid_jenjang_sekolah', 'b.name');

        $results = Siswa::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.total', 'desc');
        
        return $results->get();
    }
    public static function ChartbyGender()
    {   
        $subQuery = DB::table('siswa as a')
                        ->selectRaw("
                            a.keyid_jenjang_sekolah, 
                            a.jenis_kelamin,
                            b.name as jenjang_sekolah_name, 
                            count(1) as total ")
                        ->join('m_department as b', 'b.dept_id', '=', 'a.keyid_jenjang_sekolah')
                        // ->where('a.tahunajaran_id', '=', '2019')
                        ->groupby('a.keyid_jenjang_sekolah', 'a.jenis_kelamin', 'b.name');

        $results = Siswa::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->orderby('x.jenjang_sekolah_name', 'asc')
                    ->orderby('x.jenis_kelamin', 'asc');
        
        return $results->get();
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
