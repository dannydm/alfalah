<?php

namespace App\Models\Siswa;

use App\Models\AppModel;
use DB;

class BankTransferSync extends AppModel
{	
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'import_transfer_bank_sync';
	protected $primaryKey = 'keyid';
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */   
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function SyncTable()
    {   DB::beginTransaction();
        try 
        {   // truncate table siswa_sync at first
            $result = BankTransferSync::query()->truncate();
            if ($result)
            {   
                $sources = DB::connection('sikap')->select('select * from import_transfer_bank '); 

                // dd($sources);
                // where tanggal_entri > DATEADD(year,-1,GETDATE())
                if ($sources)
                {
                    foreach ($sources as $source) 
                    {   
                        $sync = BankTransferSync::where('keyid', '=', $source->keyid)->first();

                        if ($sync) { } // do_nothing
                        else 
                        {   // create new record
                            $sync = new BankTransferSync();
                            $sync->keyid = $source->keyid;
                        };

                        $sync->keyid             = $source->keyid;
                        $sync->keyid_import_bank = $source->keyid_import_bank;
                        $sync->trxid             = $source->TrxId;
                        $sync->tanggal_transfer  = $source->tanggal_transfer;
                        $sync->jam_transfer      = $source->jam_transfer;
                        $sync->dk                = $source->dk;
                        $sync->mutasi            = $source->mutasi;
                        $sync->saldo             = $source->saldo;
                        $sync->keterangan        = $source->keterangan;
                        $sync->va                = $source->va;
                        $sync->nis               = $source->nis;
                        $sync->nama_siswa        = $source->nama_siswa;
                        $sync->no_jurnal         = $source->no_jurnal;
                        $sync->no_posting        = $source->no_posting;
                        $sync->tgl_posting       = $source->tgl_posting;
                        $sync->tanggal_entri     = $source->tanggal_entri;
                        $sync->user_entri        = $source->user_entri;
                        $sync->tanggal_edit      = $source->tanggal_edit;
                        $sync->user_edit         = $source->user_edit;
                        $sync->jml_terpakai      = $source->jml_terpakai;

                        $sync->sync_date             = date('Ymd his');

                        $result = $sync->save();
                    };
                };

                $result = [true, "Save succcefully"];
                DB::commit();
            }
            else
            {   $result = [false, "Source Data not found "];
                DB::rollback();
            };          
            
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
