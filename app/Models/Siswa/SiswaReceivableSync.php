<?php

namespace App\Models\Siswa;

use App\Models\AppModel;
use DB;

class SiswaReceivableSync extends AppModel
{	
    /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'kartu_piutang_siswa_sync';
	protected $primaryKey = 'keyid';
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */   
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function SyncTable()
    {   DB::beginTransaction();
        try 
        {   // truncate table siswa_sync at first
            $result = SiswaReceivableSync::query()->truncate();
            if ($result)
            {   
                $sources = DB::connection('sikap')->select('select * from kartu_piutang_siswa'); // where tanggal_entri setahun terakhir
                if ($sources)
                {
                    foreach ($sources as $source) 
                    {   //print_r($result);

                        
                        $sync = SiswaReceivableSync::where('keyid', '=', $source->keyid)->first();

                        if ($sync) { } // do_nothing
                        else 
                        {   // create new record
                            $sync = new SiswaReceivableSync();
                            $sync->keyid             = $source->keyid;
                        };
                        
                        $sync->keyid_tahun_ajaran    = $source->keyid_tahun_ajaran;
                        $sync->nama_tahun_ajaran     = $source->nama_tahun_ajaran;
                        $sync->keyid_siswa           = $source->keyid_siswa;
                        $sync->nama_siswa            = $source->nama_siswa;
                        $sync->keyid_virtual_account = $source->keyid_virtual_account;
                        $sync->nama_virtual_account  = $source->nama_virtual_account;
                        $sync->keyid_jenjang_siswa   = $source->keyid_jenjang_siswa;
                        $sync->nama_jenjang_siswa    = $source->nama_jenjang_siswa;
                        $sync->keyid_biaya_sekolah   = $source->keyid_biaya_sekolah;
                        $sync->nama_biaya_sekolah    = $source->nama_biaya_sekolah;
                        $sync->bulan                 = $source->bulan;
                        $sync->status                = $source->status;
                        $sync->piutang               = $source->piutang;
                        $sync->potongan              = $source->potongan;
                        $sync->pembayaran            = $source->pembayaran;
                        $sync->tanggal_entri         = $source->tanggal_entri;
                        $sync->user_entri            = $source->user_entri;
                        $sync->program_name          = $source->program_name;
                        $sync->bultah                = $source->bultah;
                        $sync->tipe_trans            = $source->tipe_trans;
                        $sync->sync_date             = date('Ymd his');

                        $result = $sync->save();
                    };
                };

                $result = [true, "Save succcefully"];
                DB::commit();
            }
            else
            {   $result = [false, "Siswa not found "];
                DB::rollback();
            };          
            
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
