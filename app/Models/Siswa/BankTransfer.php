<?php

namespace App\Models\Siswa;

use App\Models\AppModel;
use DB;

class BankTransfer extends AppModel
{	
    /*
    |--------------------------------------------------------------------------
    | DB STRUCTURES
    |--------------------------------------------------------------------------
    */
    // CREATE TABLE IMPORT_TRANSFER_BANK(
    //  keyid numeric NOT NULL,
    //  keyid_import_bank numeric NULL,
    //  TrxId varchar(50) NOT NULL,
    //  tanggal_transfer varchar(30) NULL,
    //  jam_transfer varchar(50) NULL,
    //  dk varchar(50) NULL,
    //  mutasi numeric(18, 2) NULL,
    //  saldo numeric(18, 2) NULL,
    //  keterangan varchar(500) NULL,
    //  va varchar(50) NULL,
    //  nis varchar(50) NULL,
    //  nama_siswa varchar(500) NULL,
    //  no_jurnal varchar(50) NULL,
    //  no_posting varchar(50) NULL,
    //  tgl_posting date NULL,
    //  tanggal_entri date NULL,
    //  user_entri varchar(50) NULL,
    //  tanggal_edit date NULL,
    //  user_edit varchar(50) NULL,
    //  jml_terpakai decimal(18, 2) NULL,
    //  constraint import_transfer_bank_pk primary key (keyid),
    //  constraint import_transfer_bank_ux unique (TrxId)
    // );

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'import_transfer_bank';
	protected $primaryKey = 'keyid';
    // public $sequence_name = 'import_transfer_bank_seq_';
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    // public function SiswaDetail()
    // {
    //     return $this->hasMany('App\Models\Sales\SiswaDetail', 'Siswa_no');
    // }
    /*
    |--------------------------------------------------------------------------
    | PRIVATE FUNCTIONS
    |--------------------------------------------------------------------------
    */   
    /*
    |--------------------------------------------------------------------------
    | PUBLIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function Search($request)
    {   
        $results = static::selectRaw("(ROW_NUMBER () OVER (ORDER BY keyid)) as record_id, * ")
                    ->orderBy('nis', 'asc')
                    ->orderBy('tanggal_transfer', 'asc')
                    ->orderBy('jam_transfer', 'asc');

        if ($request->s == "form")
        {   
            if ( $request->nis )
            {   $results->where("nis", "ilike","%".$request->nis."%"); };
            if ( $request->nama_siswa )
            {   $results->where("nama_siswa", "ilike",
                "%".$request->nama_siswa."%"); };
            if ( $request->va )
            {   $results->where("keterangan", "ilike",
                "%".$request->va."%"); };
            if ( $request->keyid_siswa )
            {   $results->where("keyid_siswa", "=", $request->keyid_siswa); };
        };
        
        // if ($request->pt) {   return $results->first();   }
        // else {  return $results->get(); };
        
        return $results->get();

    }
    public static function SearchExt($request)
    {   
        $subQuery = DB::table('import_transfer_bank as a')
                    ->selectRaw("a.Siswa_no, 
                                (a.Siswa_no) as display")
                    ->where('a.status', '=', 0);

        $results = Siswa::selectRaw("x.*")
                    ->from(\DB::raw(' ( '.$subQuery->toSql().' ) as x'))
                    ->mergeBindings($subQuery)
                    ->Siswaby('x.Siswa_no', 'desc');

        if ($request->s == "form")
        {   if ($request['query'])
            {   $results->where("x.display", "ilike", "%".$request['query']."%");   };
        };
        return $results->get();
    }
    public static function headerPrintOut($request)
    {   
        $results = DB::table('import_transfer_bank as a')
            ->leftjoin('m_company as x1', 'x1.company_id', '=', 'a.company_id')
            ->leftjoin('m_container_type as x2', 'x2.container_type_id', '=', 'a.container_type')
            ->leftjoin('m_city as x3', 'x3.city_id', '=', 'a.port_loading')
            ->leftjoin('m_city as x4', 'x4.city_id', '=', 'a.port_discharge')
            ->leftjoin('m_company as x5', 'x5.company_id', '=', 'a.buyer_id')
            ->select(DB::raw("
                trim(a.id) as id, a.Siswa_no, a.company_id, a.currency_id, a.description, 
                a.buyer_id, a.buyer_name, 
                to_char(a.Siswa_date, 'dd/mm/yyyy') as Siswa_date, 
                a.pi_no, to_char(a.pi_date, 'dd/mm/yyyy') as pi_date, 
                a.bank_id, a.bank_name, a.bank_account, a.bank_address, a.swift_code, 
                a.container_type, a.payment_term, a.payment_term_id,
                a.port_loading, a.port_discharge, 
                coalesce(a.cbm,0) as cbm, 
                coalesce(a.weight_gross, 0) as weight_gross, 
                coalesce(a.weight_nett, 0) as weight_nett,
                to_char(a.shipment_date,'dd/mm/yyyy') as shipment_date, 
                to_char(a.etd, 'dd/mm/yyyy') as etd, 
                to_char(a.eta,  'dd/mm/yyyy') as eta,
                x1.name as company_name, x1.address as company_address,
                x2.description as container_type_name,
                x3.name as port_loading_name, 
                x4.name as port_discharge_name,
                x5.address as buyer_address
                "))
            ->Siswaby('a.id');

        if ($request->s == "form")
        {   
            if ( $request->id )
            {   $results->where("a.id", "=", $request->id); };
        };

        if ($request->pt) {   return $results->first();   }
        else {  return $results->get(); };
    }
    public static function Siswasave(   $head_data, $json_data, $modi_data, 
                                        $site, $user_id, $company_id    )
    {   if (count($head_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($head_data as $json) 
                {   // save Siswa
                    if ($json->created_date == '')  
                    {   $import_transfer_bank = new Siswa();
                        $Siswa->id         = self::getNextID(self::sequence_name, $site);
                        // $Siswa->company_id = env('APP_COMPANY');
                        $Siswa->company_id = config('app.system_company');
                        $Siswa->Siswa_no   = $json->Siswa_no;
                        $Siswa->Siswa_date = $json->Siswa_date;
                        $Siswa->created_by = $user_id;

                        $Siswa_detail = new SiswaDetail();
                    }
                    else 
                    {   // update Siswa
                        $import_transfer_bank = Siswa::where('Siswa_no', '=', $json->Siswa_no)->first();
                        $Siswa->modified_by = $user_id;

                        $Siswa_detail = SiswaDetail::where('Siswa_no', '=', $json->Siswa_no)->first();
                        if (Empty($Siswa_detail)) { $Siswa_detail = new SiswaDetail(); };
                    };

                    $Siswa->repeat_Siswa_no = $json->repeat_Siswa_no;
                    $Siswa->buyer_id        = $json->buyer_id;
                    $Siswa->buyer_name      = $json->buyer_name;
                    $Siswa->currency_id     = $json->currency_id;
                    $Siswa->pi_no           = $json->pi_no;
                    
                    if (Empty($json->pi_date ) && Empty(trim($json->pi_no)))
                    {   $Siswa->pi_no = null;
                        $Siswa->pi_date = null;
                    }
                    else if ($json->pi_date && Empty(trim($json->pi_no)))
                    {   $sitedocument = new SiteDocument();
                        $Siswa->pi_no = $sitedocument->getDocNum(config('app.system_company'), 
                                            $site, 10, 1001, $json->buyer_id, "");
                        $Siswa->pi_date     = $json->pi_date;
                    } else { $Siswa->pi_date= $json->pi_date; };

                    $Siswa->bank_id         = $json->bank_id;
                    $Siswa->bank_name       = $json->bank_name;
                    $Siswa->bank_account    = $json->bank_account;
                    $Siswa->bank_address    = $json->bank_address;
                    $Siswa->swift_code      = $json->swift_code;
                    $Siswa->container_type  = $json->container_type;
                    $Siswa->cbm             = $json->cbm;
                    $Siswa->port_loading    = $json->port_loading;
                    $Siswa->port_discharge  = $json->port_discharge;
                    $Siswa->weight_gross    = $json->weight_gross;
                    $Siswa->weight_nett     = $json->weight_nett;
                    $Siswa->payment_term    = $json->payment_term;
                    $Siswa->payment_term_id = $json->payment_term_id;

                    if ( $json->shipment_date )
                    {   $Siswa->shipment_date = $json->shipment_date.' 00:00';  } 
                    else {   $Siswa->shipment_date = null;  };

                    if ( $json->etd )
                    {   $Siswa->etd = $json->etd.' 00:00';  } 
                    else {   $Siswa->etd = null;  };

                    if ( $json->eta )
                    {   $Siswa->eta = $json->eta.' 00:00';  } 
                    else {   $Siswa->eta = null;  };
                    
                    $result = $Siswa->save();
                    // save import_transfer_bank details

                    if ($result)
                    {   if (count($json_data) > 0)
                        {   $result = $Siswa_detail->SiswadetailSave( $json_data, 
                                            $site, $user_id, $company_id, $json->Siswa_no); 
                        };
                    };
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        return $result;
    }
    public static function Profile($nis)
    {   
        $result = static::where('nis', '=', $nis)->first();
        return $result;
    }
    public static function SiswaDelete($request)
    {   
        DB::beginTransaction();
        try 
        {   $result = static::where('id', '=', $request->id)
                        ->where('asset_id', '=', $request->asset_id)->delete();
            $result = [true, "Save succcefully"];
            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function RegisterUser($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('keyid', '=', $request->id)->first();
            if ($result)
            {   // register to laravel user
                $user           = new User();
                $user->password = Hash::make(trim($result->tanggal_lahir));
                $user->email    = trim($result->nis).'@alfalah.com';
                $user->name     = trim($result->nis);
                $user->save();

                if ($user)
                {   // register to Master user
                    $muser = new MUser();
                    $muser->user_id     = $user->id;
                    $muser->password    = "default";
                    $muser->username    = $user->name;
                    $muser->name        = $result->nama_lengkap;
                    $muser->auth_email  = $user->email;
                    $muser->status      = 0;
                    $muser->laravel_id  = $user->id;
                    
                    $muser->save();

                    if ($muser)
                    {   // register to Role user
                        $roleuser = new RoleUser();
                        $roleuser->user_id     = $user->id;
                        $roleuser->role_id     = 100;
                        $roleuser->save();
                        if ($roleuser)
                        {
                            $result = [true, "Save succcefully"];
                            DB::commit();
                        }
                        else
                        {   $result = [false, "Failed to Save on Role User"];
                            DB::rollback();
                        };
                    }
                    else
                    {   $result = [false, "Failed to Save on Master User"];
                        DB::rollback();
                    };
                }
                else
                {   $result = [false, "Failed to Save on Lvl User"];
                    DB::rollback();
                };
            }
            else
            {   $result = [false, "No Data Found"];
                DB::rollback();
            };          
            
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function ResetPassword($request)
    {   DB::beginTransaction();
        try 
        {   $result = static::where('keyid', '=', $request->id)->first();
            if ($result)
            {   // register to laravel user
                $user = User::where('name', '=', $result->nis)->first();
                if ($user)
                {   
                    $user->password = Hash::make(trim($result->tanggal_lahir));
                    $user->save();
                    if ($user)
                    {
                        $result = [true, "Save succcefully"];
                        DB::commit();
                    }
                    else
                    {   $result = [false, "Failed to Save on Role User"];
                        DB::rollback();
                    };
                }
                else
                {   $result = [false, "User not found "];
                    DB::rollback();
                };
            }
            else
            {   $result = [false, "import_transfer_bank not found "];
                DB::rollback();
            };          
            
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    public static function Receivable($nis)
    {   
        $result = static::where('keyid_siswa', '=', $nis)
                    ->orderby('bultah', 'asc')
                    ->get();
        return $result;
    }
    public static function SyncTable()
    {   DB::beginTransaction();
        try 
        {   // truncate table siswa_sync at first
            $sources = DB::connection('sikap')->select('select * from import_transfer_bank '); 
            // where tanggal_entri > DATEADD(year,-1,GETDATE())
            if ($sources)
            {
                foreach ($sources as $source) 
                {   
                    $sync = BankTransfer::where('keyid', '=', $source->keyid)->first();

                    if ($sync) { } // do_nothing
                    else 
                    {   // create new record
                        $sync = new BankTransfer();
                        $sync->keyid = $source->keyid;
                    };

                    $sync->keyid             = $source->keyid;
                    $sync->keyid_import_bank = $source->keyid_import_bank;
                    $sync->trxid             = $source->TrxId;
                    $sync->tanggal_transfer  = $source->tanggal_transfer;
                    $sync->jam_transfer      = $source->jam_transfer;
                    $sync->dk                = $source->dk;
                    $sync->mutasi            = $source->mutasi;
                    $sync->saldo             = $source->saldo;
                    $sync->keterangan        = $source->keterangan;
                    $sync->va                = $source->va;
                    $sync->nis               = $source->nis;
                    $sync->nama_siswa        = $source->nama_siswa;
                    $sync->no_jurnal         = $source->no_jurnal;
                    $sync->no_posting        = $source->no_posting;
                    $sync->tgl_posting       = $source->tgl_posting;
                    $sync->tanggal_entri     = $source->tanggal_entri;
                    $sync->user_entri        = $source->user_entri;
                    $sync->tanggal_edit      = $source->tanggal_edit;
                    $sync->user_edit         = $source->user_edit;
                    $sync->jml_terpakai      = $source->jml_terpakai;

                    $sync->sync_date             = date('Ymd his');

                    $result = $sync->save();
                };
                $result = [true, "Save succcefully"];
                DB::commit();
            }
            else
            {   $result = [false, "Source Data not found "];
                DB::rollback();
            };          
            
        } catch (\Illuminate\Database\QueryException $e) {
            // rollback transaction
            $result = [false, "Error Executed ---> ".$e];
            DB::rollback();
        };
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
