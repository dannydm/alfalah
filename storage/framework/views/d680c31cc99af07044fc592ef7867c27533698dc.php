<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Language" content="en-us" />
    <title><?php echo e(config('app.name', 'ALFALAH')); ?></title>
    <meta name="keywords" content="<?php echo e(config('app.name', 'ALFALAH')); ?>" />
    <meta name="description" content="<?php echo e(config('app.name', 'ALFALAH')); ?>" />
    <meta name="copyright" content="<?php echo e(config('app.name', 'ALFALAH')); ?>" />
    <!-- ** css stylesheet ** -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/js/ext3/resources/css/ext-all.css')); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/js/ext3/resources/css/ext-all.css')); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/js/ext3/examples/shared/icons/silk.css')); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/js/ext3/examples/shared/examples.css')); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/js/ext3/examples/grid/grid-examples.css')); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/js/ext3/examples/ux/css/Portal.css')); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/js/ext3/examples/ux/css/LockingGridView.css')); ?>" />

    <!-- ** Javascript ** -->
    <!-- ChartJS -->
    <script type="text/javascript" src="<?php echo e(asset('/bower_components/jquery/dist/jquery.min.js')); ?>"></script>
    
    <script src="<?php echo e(asset('/academy/code/highcharts.js')); ?>"></script>
    <script src="<?php echo e(asset('/academy/code/modules/exporting.js')); ?>"></script>
    <script src="<?php echo e(asset('/academy/code/modules/export-data.js')); ?>"></script>
    <script src="<?php echo e(asset('/academy/code/modules/accessibility.js')); ?>"></script>

    <!-- ExtJS library: base/adapter -->
    <script type="text/javascript" src="<?php echo e(asset('/js/ext3/adapter/ext/ext-base.js')); ?>"></script>
    <!-- ExtJS library: all widgets -->
    <script type="text/javascript" src="<?php echo e(asset('/js/ext3/ext-all.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('/js/ext3/examples/ux/ux-all.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(asset('/js/ext3/examples/ux/LockingGridView.js')); ?>"></script>
    <!-- alfalah core -->
    <script type="text/javascript">
    // create namespace
    Ext.namespace('alfalah', 'alfalah.core', 'alfalah.popup');
    // create application
    alfalah.core = function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.image_url;
        this.init_mode;
        this.target;
        this.action;
        this.Grid;
        this.Columns;
        this.Records;
        this.DataStore;
        this.Searchs;
        this.menuPanel;
        this.viewport;

        // private functions
        //**********************
        // public space
        //**********************
        return {
            // execute at the very last time
            // public properties, e.g. strings to translate
            // public methods
            server_date: new Date().format('Y'),
            start_date : new Date(this.server_date, 0, 1),
            end_date   : new Date(this.server_date, 11, 31),
            page_limit : 75,
            page_start : 0,
            ajax_url   : "<?php echo e(env('APP_URL')); ?>",
            user_id    : '<?php echo e($USER_ID); ?>',
            user_name    : '<?php echo e($USERNAME); ?>',
            user_site    : '<?php echo e($USERSITE); ?>',
            "setParameter" : function(Ajax, Image, Mode)
            {   //this.ajax_url =  Ajax;
                image_url = Image;
                init_mode= Mode;
            },
            "logout" : function()
            {   window.location = "<?php echo e(url('/logout')); ?>";
            },
            "prepare_component" : function()
            {   // NOTE: This is an example showing simple state management. During development,
                // it is generally best to disable state management as dynamically-generated ids
                // can change across page loads, leading to unpredictable results.  The developer
                // should ensure that stable state ids are set for stateful components in real apps.
                Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
                // Ext.BLANK_IMAGE_URL = '/newalfalah/frontend/web/js/ext3/resources/images/default/s.gif ';
                this.start_date = new Date(this.server_date, 0, 1);
                this.end_date = new Date(this.server_date, 11, 31);

                this.menuPanel = new Ext.tree.TreePanel(
                {   id: 'menu_panel',
                    jsId: 'menu_panel',
                    title: 'Menu',
                    iconCls: 'silk-folder',
                    width: 250,
                    height:400,
                    rootVisible: false,
                    useArrows: false,
                    autoScroll: true,
                    animate: false,
                    enableDD: false,
                    containerScroll: true,
                    border: true,
                    expanded: true,
                    lines: true,
                    loader: new Ext.tree.TreeLoader({
                        requestMethod: 'GET',
                        dataUrl: "<?php echo e(url('/dashboard/0/1')); ?>",
                        baseParams: { limit:this.page_limit, start:this.page_start },
                        listeners : {
                            loadexception : function (treeloader, node, response){
                                Ext.Msg.show(
                                {   title :'E R R O R ',
                                    msg : 'Session Time Out, Please Re-Login again.',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                                //go to login page
                                alfalah.core.logout();
                            }
                        }
                    }),
                    root: new Ext.tree.AsyncTreeNode(),
                    bbar: [
                        {   text:'Logout',
                            tooltip:'Logout System',
                            iconCls: 'silk-door-out',
                            handler : function(){   window.location = "<?php echo e(url('/logout')); ?>"; },
                            scope : this
                        }],
                    listeners: {    scope : this,
                        click: {    stopEvent:true,
                                    fn: function(node)
                                        {   var centerPanel = Ext.getCmp('center_panel');
                                            var newTab_id = Ext.getCmp("tabMenu"+node.attributes.id);
                                            if (newTab_id) {}
                                            else
                                            {   newTab_id = "tabMenu"+node.attributes.id;
                                                if ( (centerPanel) &&
                                                    ( Ext.util.Format.trim(node.attributes.link) !== '.'))
                                                {   var newTab = new Ext.TabPanel(
                                                    {   id : newTab_id,
                                                        jsId : newTab_id,
                                                        title:  node.attributes.text,
                                                        region: 'center',
                                                        iconCls: 'silk-grid',
                                                        closable : true,
                                                        deferredRender : false,
                                                        autoLoad:   {
                                                            showMask: true,
                                                            url: node.attributes.link+'?&tabId='+newTab_id+'&m='+node.attributes.id,
                                                            scripts : true,
                                                            deferredRender : false,
                                                            mode: 'iframe',
                                                            maskMsg: 'Loading page ... '
                                                        },
                                                        listeners:
                                                        {   tabchange: function(tabPanel,newTab)
                                                            {   var thisObj = newTab.getUpdater();
                                                                if(thisObj) thisObj.refresh();
                                                            },
                                                        },
                                                    });
                                                    centerPanel.beginUpdate();
                                                    centerPanel.add(newTab);
                                                    centerPanel.setActiveTab(newTab);
                                                    centerPanel.endUpdate();
                                                    this.viewport.doLayout();
                                                };
                                            };
                                        }
                    }}
                });
            },
            "build_layout": function()
            {   this.viewport = new Ext.Viewport(
                {   id: 'viewport',
                    jsId: 'viewport',
                    layout:'border',
                    items:[
                    {   region:'west',
                        id:'west_panel',
                        jsId:'west_panel',
                        title:'ALFALAH System',
                        split:true,
                        width: 200,
                        minSize: 175,
                        maxSize: 400,
                        collapsible: true,
                        //margins:'10 0 5 5',
                        //cmargins:'10 5 5 5',
                        layout:'accordion',
                        layoutConfig:
                        {   animate:true
                        },
                        items: [ this.menuPanel,
                        //{   id:'user_tab',
                        //    jsId:'user_tab',
                        //    title:'User',
                        //    //html: Ext.example.shortBogusMarkup,
                        //    border:false,
                        //    autoScroll:true,
                        //    iconCls: 'silk-user',
                        //    tbar: [
                        //    {   text:'Logout',
                        //        tooltip:'Logout System',
                        //        iconCls: 'silk-door-out',
                        //        handler : function()
                        //        {   window.location = "< ? php echo url::site(false); ?>alfalah/base/signup/1/0/"; },
                        //        scope : this
                        //    }],
                        //}
                        ]
                    },
                    {   region: 'center',
                        id:'center_panel',
                        jsId:'center_panel',
                        xtype: 'tabpanel', // TabPanel itself has no title
                        // autoHeight:true,
                        // autoScroll:true,
                        // enableTabScroll:true,
                        //defaults:{autoScroll:true},
                        activeTab: 0,
                        items: [
                            {   id:'DashboardTab',
                                jsId:'DashboardTab',
                                title : 'Dashboard',
                                iconCls: 'silk-layout',
                                region: 'center',
                                closable : false,
                                deferredRender : false,
                                autoLoad:   {
                                    method : 'GET',
                                    showMask: true,
                                    url: "<?php echo e(url('/dashboard/0/0')); ?>",
                                    params:
                                    {   tabId: "DashboardTab",
                                    },
                                    scripts : true,
                                    discardUrl:true,
                                    deferredRender : false,
                                    mode: 'iframe',
                                    maskMsg: 'Loading page ... '
                                }
                            }

                        ]
                    }]
                });
            },
            "finalize_comp_and_layout" : function()
            {   // overide TextField Validation to exclude empty string
                Ext.override(Ext.form.TextField, {
                    validator:function(text){
                        return (text.length==0 || Ext.util.Format.trim(text).length!=0);
                    }
                });
            },
            "initialize": function()
            {   Ext.QuickTips.init();
                this.prepare_component();
                this.build_layout();
                this.finalize_comp_and_layout();
            },
            // combo grid column renderer
            comboRenderer : function(combo, value)
            {   if (Ext.isEmpty(value)) { return "";    }
                else
                {   var record = combo.findRecord(combo.valueField, value);
                    return record ? record.get(combo.displayField) : combo.valueNotFoundText;
                };

                //return function(value)
                //{   var record = combo.findRecord(combo.valueField, value);
                //    return record ? record.get(combo.displayField) : combo.valueNotFoundText;
                //}
            },
            // date grid column renderer
            dateRenderer : function(value)
            {   if (Ext.isEmpty(value))
                {   result = "";    }
                else
                {   the_value = Date.parseDate(value.substr(0,19), "Y-m-d H:i:s", true);
                    result = the_value.format('d/m/Y H:i:s');
                };
                return result;
            },
            shortdateRenderer : function(value, sourceFormat, targetFormat)
            {   if (Ext.isEmpty(value))
                {   result = "";    }
                else
                {   result = Date.parseDate(value.substr(0,10), sourceFormat, true).format(targetFormat); };
                return result;
            },
            printButton: function(button, event, DataStore)
            {   document.location.href = DataStore.proxy.url+'?'+Ext.urlEncode(DataStore.baseParams)+'&pt='+button.print_type;
            },
            printOut: function(button, event, the_url, the_parameter)
            {   document.location.href = the_url+'?'+Ext.urlEncode(the_parameter)+'&pt='+button.print_type;
            },
            gridColumnWrap : function(data)
            {   return '<div style="white-space:normal !important;">'+ data +'</div>';
            },
            copy_below : function (button, event, the_datastore, the_xy, the_field)
            {   //copy below
                the_record_count = the_datastore.getCount();
                for (var the_x = the_xy[0]+1; the_x < the_record_count; the_x++)
                {   a = the_datastore.getAt(the_xy[0]).get(the_field);
                    the_datastore.getAt(the_x).set(the_field, a);
                };
            },
            getCookie : function(value)
            {   cName = "";
                pCOOKIES = new Array();
                pCOOKIES = document.cookie.split('; ');
                for(bb = 0; bb < pCOOKIES.length; bb++)
                {   NmeVal  = new Array();
                    NmeVal  = pCOOKIES[bb].split('=');
                    if(NmeVal[0] == value)
                    {   cName = unescape(NmeVal[1]);    };
                }
                return cName;
            },
            getJScripts: function(source, callback)
            {   //console.log('getJScripts');
                function loadNextScript()
                {   //console.log('loadNextScript');
                    var done = false;
                    var head = document.getElementsByTagName('head')[0];
                    var script = document.createElement('script');
                    script.type = 'text/javascript';
                    script.onreadystatechange = function () {
                        if (this.readyState == 'complete' || this.readyState == 'loaded') {
                            scriptLoaded();
                        }
                    }
                    script.onload = scriptLoaded;
                    script.src = source.shift(); // grab next script off front of array
                    //console.log(script.src);
                    head.appendChild(script);

                    function scriptLoaded() {
                        //console.log('scriptLoaded');
                        // check done variable to make sure we aren't getting notified more than once on the same script
                        if (!done) {
                            script.onreadystatechange = script.onload = null;   // kill memory leak in IE
                            done = true;
                            if (source.length != 0) {
                                loadNextScript();
                            }
                            else { callback(); };
                        }
                    }
                }
                loadNextScript();
            },
            // New Components
            newDataStore : function(new_url, is_remote, new_base_param )
            {   var newDS =  new Ext.data.Store(
                {   proxy:  new Ext.data.HttpProxy(
                    {   method  : 'GET',
                        url     : new_url,
                    }),
                    baseParams  : new_base_param,
                    reader      : new Ext.data.JsonReader(),
                    remoteFilter: is_remote
                });

                return newDS;
            },
            newGroupingStore : function(new_url, is_remote, new_base_param, new_groupField )
            {   var newDS =  new Ext.data.GroupingStore(
                {   proxy:  new Ext.data.HttpProxy(
                    {   method  : 'GET',
                        url     : new_url,
                    }),
                    baseParams  : new_base_param,
                    reader      : new Ext.data.JsonReader(),
                    remoteFilter: is_remote,
                    groupField  : new_groupField
                });

                return newDS;
            },
            newSearchNull : function(source, the_prefix) //'calonsiswa_N'
            {   var the_search_null = [];
                var v_search;
                Ext.each(source,
                    function(the_col)
                    {   if (the_col.search_null== true)
                        {   v_search =
                            {   id            : the_prefix+the_col.dataIndex,
                                cid           : 'null_'+the_col.dataIndex,
                                fieldLabel    : the_col.header,
                                xtype         : 'combo',
                                store         : new Ext.data.SimpleStore(
                                {   fields    : ['status'],
                                    data      : [ [''], ['NULL']]
                                }),
                                displayField  : 'status',
                                valueField    : 'status',
                                value         : '',
                                mode          : 'local',
                                triggerAction : 'all',
                                selectOnFocus : true,
                                editable      : false,
                                width         : 70,
                                x             : 55
                            };
                            the_search_null.push(v_search);
                        };
                    });
                return the_search_null;
            },
            // Get Values
            getSearchParameter : function(search_items)
            {   var the_parameter = {};
                Ext.each(search_items,
                    function(item)
                    {   v_param = {};
                        //console.log(item);console.log(item.xtype);
                        switch (item.xtype)
                        {   case "datefield" :
                            {   v_param[ item.cid ] = Ext.getCmp(item.id).getValue();
                                if (v_param[ item.cid ])
                                { v_param[ item.cid ] = v_param[ item.cid ].format("d/m/Y"); };
                            };
                            break;
                            case "combo" :
                                v_param[ item.cid] = Ext.getCmp(item.id).getValue();
                            break;
                            default :
                                v_param[ item.cid ] = Ext.getCmp(item.id).getValue();
                            break;
                        };
                        // if (Ext.isDate(Ext.get(item.id).getValue()) )
                        // {   v_param[ item.cid ] = Ext.get(item.id).getValue().format("d/m/Y");  }
                        // else //check additional transformation items
                        // {
                        // };
                        the_parameter = Ext.apply( the_parameter, v_param);
                    }
                );
                return the_parameter;
            },
            getHeadData : function(the_fields)
            {   //console.log(the_fields);
                var head_data = [];
                var v_json = {};
                Ext.each(the_fields,
                    function(the_field)
                    {   //console.log(the_field);
                        a = Ext.getCmp(the_field);

                        if (Ext.isDate(a.getValue()) ) //is_date
                        {   v_json[a.name] = a.getValue().format("d/m/Y");  }
                        // else if ( a.xtype == "combo") // is_combo_box
                        // {   v_json[a.name] = a.lastSelectionText; }
                        else
                        {   v_json[a.name] = a.getValue();  };
                    });
                head_data.push(v_json);
                return head_data;
            },
            getDetailData : function(the_records)
            {   var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        if (Ext.isEmpty(the_record.store)) {} // deleted record
                        else
                        {   Ext.iterate(the_record.data, function(key, value)
                            {   v_data = {};
                                if (Ext.isDate( value ) )
                                {   v_data[ key ] = value.format("d/m/Y");  }
                                else
                                {   v_data[ key ] = value;  };
                                v_json= Ext.apply( v_json, v_data);
                            });
                            json_data.push(v_json);
                        };
                    });
                return json_data;
            },
            validateFields : function(the_fields)
            {   //console.log('validateFields');
                var is_validate = true;
                Ext.each( the_fields,
                    function(the_field)
                    {   a = Ext.getCmp(the_field);
                        b = a.getValue();
                        if (Ext.isEmpty(b) ) { is_validate = false;};

                        if ( (a.validate() == false) || (is_validate == false) )
                        {   is_validate = false;
                            Ext.Msg.show(
                            {   title:'E R R O R ',
                                msg: 'Column '+a.fieldLabel+' is mandatory.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                            return false;
                        };
                    });
                return is_validate;
            },
            validateGridQuantity : function(the_records, the_fields)
            {   var is_validate = true;
                Ext.each(the_records,
                    function(the_record)
                    {   Ext.iterate(the_record.data, function(key, value)
                        {   Ext.each(the_fields,
                                function(the_field)
                                {   if ( (the_field == key) && (value ==0) )
                                    {   is_validate = false;
                                        Ext.Msg.show(
                                        {   title   :'E R R O R ',
                                            msg     : 'Found zero quantity <br>on Column '+key+
                                                      '<br>of items <br>'+the_record.data.item_name,
                                            buttons : Ext.Msg.OK,
                                            icon    : Ext.MessageBox.ERROR,
                                            width   : 200,
                                        });
                                        return false;  // break looping
                                    };
                                });
                        });
                    });
                return is_validate;
            },
            jsonGet : function( the_url, the_headers, the_parameter, the_success)
            {   Ext.Ajax.request(
                {   method: 'GET',
                    url: the_url,
                    headers: the_headers,
                    params: the_parameter,
                    success: the_success,
                    failure: function()
                    {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                });
            },
            jsonSave : function(the_url, the_params, the_datastore, the_scope)
            {   Ext.Ajax.request(
                {   method: 'POST',
                    url: the_url,
                    params  : the_params,
                    success : function(response, options)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   the_datastore.reload();    };
                    },
                    failure: function(response, options)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'S E R V E R    E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.server_message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        };
                    },
                    scope: the_scope
                });
            },
            submitGrid : function( the_datastore, the_url, the_headers, the_parameter)
            {   Ext.Ajax.request(
                {   method: 'POST',
                    url: the_url,
                    headers: the_headers,
                    params: the_parameter,
                    success: function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   the_datastore.reload(); };
                    },
                    failure: function()
                    {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    // scope: this
                });
            },
            submitForm : function( the_form_name, the_datastore, the_url, the_headers, the_parameter)
            {   var the_form = Ext.getCmp(the_form_name);
                Ext.Ajax.request(
                {   method: 'POST',
                    url: the_url,
                    headers: the_headers,
                    params: the_parameter,
                    success: function(response)
                    {   var the_response = Ext.decode(response.responseText);
                        if (the_response.success == false)
                        {   Ext.Msg.show(
                            {   title :'E R R O R ',
                                msg : 'Server Message : '+'\n'+the_response.message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                             });
                        }
                        else
                        {   the_form.destroy();
                            the_datastore.reload();
                        };
                    },
                    failure: function()
                    {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
                    },
                    // scope: this
                });
            }
        }; // end of public space
    }(); // end of app
    // create application
    alfalah.popup = function()
    {   // do NOT access DOM from here; elements don't exist yet
        // execute at the first time
        // private variables
        this.tabId = '';

        // private functions

        // public space
        return {
            // execute at the very last time
            //public variable
            centerPanel : 0,
            config :{   id : 'xXx',
                        title : 'No Title',
                        width : 600,
                        height : 300,
                        popup : '',
                        target : '',
                        //target_field : '',
                        //target_type: ['grid', 'input']
                        target_type: 'input',
                        parameter: ''
                    },
            // public methods
            initialize: function()
            {   //this.prepare_component();
                //this.build_layout();
                //this.finalize_comp_and_layout();
            },
            // prepare the component before layout drawing
            prepare_component: function()
            {
            },
            // build the layout
            build_layout: function()
            {
            },
            // finalize the component and layout drawing
            finalize_comp_and_layout: function()
            {},
            autoload: function(the_url)
            {   var the_object = new Ext.Window(
                    {   id: this.config['id'],
                        title: this.config['title'],
                        width: this.config['width'],
                        height: this.config['height'],
                        modal: true,
                        layout: 'fit',
                        resizable : true,
                        //closeAction:'hide',
                        closeAction:'destroy',
                        popConfig : this.config,
                        autoLoad : {
                            // url : '< ? php echo url::site(false); ?>alfalah/base/popup/'+ this.config['popup'],
                            url : the_url,
                            // params : this.config,
                            // scripts: true
                        }
                    });
                // the_object.load({
                //        url : the_url,
                //        scripts: true
                //     });
                return the_object;
            },
            load: function(popup_object)
            {   if (Ext.isObject(popup_object))
                {   var the_object = new Ext.Window(
                    {   id: this.config['id'],
                        title: this.config['title'],
                        width: this.config['width'],
                        height: this.config['height'],
                        modal: true,
                        layout: 'fit',
                        resizable : false,
                        closeAction:'hide',
                        //closeAction:'destroy',
                        popConfig : this.config
                    });
                }
                else
                {   var the_object = new Ext.Window(
                    {   id: this.config['id'],
                        title: this.config['title'],
                        width: this.config['width'],
                        height: this.config['height'],
                        modal: true,
                        layout: 'fit',
                        resizable : false,
                        closeAction:'hide',
                        //closeAction:'destroy',
                        popConfig : this.config,
                        autoLoad : {
                            // url : '< ? php echo url::site(false); ?>alfalah/base/popup/'+ this.config['popup'],
                            params : this.config,
                            scripts: true
                        }
                    });
                    //the_object.load({
                    //    url : 'http://localhost/ko3/index/alfalah/base/popup/currency', //+ this.config['popup'],
                    //    scripts: true
                    //});
                };
                return the_object;
            },
        }; // end of public space
    }(); // end of app
    Ext.onReady(alfalah.core.initialize, alfalah.core);
    </script>
</head>
<body>
    &nbsp;
</body>
</html>
