<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.news');

// create application
alfalah.news = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '<?php echo e($TABID); ?>';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '<?php echo e(csrf_token()); ?>',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.news.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.news.Tab);
            this.centerPanel.setActiveTab(this.news.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.news.news= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "Id", width : 50,
                    dataIndex : 'news_id', sortable: true,
                    tooltip:"Id",
                },
                {   header: "Title", width : 200,
                    dataIndex : 'title', sortable: true,
                    tooltip:"Title",
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Category", width : 100,
                    dataIndex : 'category_1_name', sortable: true,
                    tooltip:"Category",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["label"],
                                               data : [["ALL"], ["KB"], ["TK"], ["SD"],["SD ICP"],["SMP"], ["Siswa"]]
                                        }),
                            displayField:"label",
                            valueField:"label",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
                {   header: "Start", width : 100,
                    dataIndex : 'publish_start_date', sortable: true,
                    tooltip:"Publish Start Date",
                    renderer: function(value)
                    {  return alfalah.core.shortdateRenderer(value,'Y-m-d', 'd/m/Y'); 
                    },
                    editor: new Ext.form.DateField({
                        format: 'm/d/y',
                        minValue: '01/01/20',
                        // disabledDays: [0, 6],
                        // disabledDaysText: 'Days are not available on the weekends'
                    })
                },
                {   header: "End", width : 100,
                    dataIndex : 'publish_end_date', sortable: true,
                    tooltip:"Publish End Date",
                    renderer: function(value)
                    {  return alfalah.core.shortdateRenderer(value,'Y-m-d', 'd/m/Y'); 
                    },
                    editor: new Ext.form.DateField({
                        format: 'm/d/y',
                        minValue: '01/01/20',
                        // disabledDays: [0, 6],
                        // disabledDaysText: 'Days are not available on the weekends'
                    })
                },
                {   header: "Status", width : 100,
                    dataIndex : 'status_name', sortable: true,
                    tooltip:"Status",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                               fields: ["label"],
                                               data : [["Active"], ["InActive"]]
                                        }),
                            displayField:"label",
                            valueField:"label",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'news_id', type: 'integer'},
                {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'news_id',
                    cid: 'id',
                    fieldLabel: 'ID',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'news_news_title',
                    cid: 'title',
                    fieldLabel: 'Title',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'news_news_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [ [''], ['Active'], ['InActive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_newsSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.news_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "<?php echo e(url('/news/2/0')); ?>", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.news.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                <?php if(array_key_exists('SAVE', $MyTasks)): ?>
                    {   text:'Add',
                        tooltip:'Add Record',
                        iconCls: 'silk-add',
                        handler : this.Grid_add,
                        scope : this
                    },
                    {   text:'Edit',
                        tooltip:'Edit Record',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_edit,
                        scope : this
                    },
                    {   text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Grid_save,
                        scope : this
                    },'-',
                <?php endif; ?>
                <?php if(array_key_exists('PRINT_XLS', $MyTasks)): ?>
                    {   print_type : "xls",
                        text:'Print Excel',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                <?php endif; ?>
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_newsGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_newsPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_newsPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_newsGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                    //Ext.getCmp(tabId+"_newsSearchBtn").handler.call();
                                    //Ext.getCmp('submit').handler.call(Ext.getCmp('submit').scope);
                                }
                            }
                        }),
                        ' records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_newsTab",
                jsId : tabId+"_newsTab",
                title:  "Pengumuman",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items: new Ext.TabPanel(
                        {   border:false,
                            activeTab:0,
                            tabPosition:'bottom',
                            items: [
                            new Ext.FormPanel(
                            {   title: 'S E A R C H',
                                labelWidth: 50,
                                defaultType: 'textfield',
                                items : this.Searchs,
                                frame: true,
                                autoScroll : true,
                                tbar: [
                                {   text:'Search',
                                    tooltip:'Search',
                                    iconCls: 'silk-zoom',
                                    handler : this.news_search_handler,
                                    scope : this,
                                }]
                            }),
                            // new Ext.FormPanel(
                            // {   title: 'N U L L',
                            //     labelWidth: 50,
                            //     defaultType: 'textfield',
                            //     items : this.Searchs_null,
                            //     frame: true,
                            //     autoScroll : true,
                            // }),
                            ]
                        })
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {},
    <?php if(array_key_exists('SAVE', $MyTasks)): ?>
        Grid_add : function(button, event)
        {   
            this.Grid.stopEditing();
            this.Grid.store.insert( 0,
                new this.Records (
                {   news_id: "",
                    title: "New Title",
                    category_1_name: "ALL",
                    // content: "New Content",
                    status: 0,
                    status_name: "Active",
                    publish_start_date: "",
                    publish_end_date: "",
                    created_date: "",
                    modified_date: ""
                }));
            // placed the edit cursor on third-column
            this.Grid.startEditing(0, 1);   
        },
        Grid_edit : function(button, event)
        {   var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var form_order = Ext.getCmp("form_order");
                if (form_order)
                {   Ext.Msg.show(
                    {   title :'E R R O R ',
                        msg : 'Order Form Available',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else
                {   var centerPanel = Ext.getCmp('center_panel');
                    alfalah.news.forms.initialize(the_record.record);
                    centerPanel.beginUpdate();
                    centerPanel.add(alfalah.news.forms.Tab);
                    centerPanel.setActiveTab(alfalah.news.forms.Tab);
                    centerPanel.endUpdate();
                    alfalah.core.viewport.doLayout();
                };
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },
        Grid_save : function(button, event)
        {   var the_records = this.DataStore.getModifiedRecords();

            // console.log(the_records);
            // check data modification
            if ( the_records.length == 0 )
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data modified ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                     });
            } else
            {   
                // var json_data = alfalah.core.getDetailData(the_records);
                // console.log(json_data);

                var json_data = [];
                var v_json = {};
                Ext.each(the_records,
                    function(the_record)
                    {   var v_data = {};
                        var v_json = {};
                        if (Ext.isEmpty(the_record.store)) {} // deleted record
                        else
                        {   Ext.iterate(the_record.data, function(key, value) 
                            {   v_data = {};
                                if (Ext.isDate( value ) )
                                {   v_data[ key ] = value.format("d/m/Y");  }
                                if (key == "content")
                                {   v_data[ key ] = "";   }
                                else
                                {   v_data[ key ] = value;  };
                                v_json= Ext.apply( v_json, v_data);
                            });
                            json_data.push(v_json);    
                        };
                    });
                // return json_data;


                alfalah.core.submitGrid(
                    this.DataStore, 
                    "<?php echo e(url('/news/2/1')); ?>",
                    {   'x-csrf-token': alfalah.news.sid }, 
                    {   json: Ext.encode(json_data) }
                );
            };
        },
    <?php endif; ?>
        // news receivable search button
        news_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_newsSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            };

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.news.task,
                        act: alfalah.news.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.news.forms= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;

    // private functions

    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        category   : true,
        load_1     : true,
        load_2     : true,
        load_3     : true,
        // public methods
        initialize: function(the_record)
        {   this.Records = the_record;
            console.log(this.Records);

            if (this.Records.data.category_1 == "6") { this.category = false; }
            else this.category = true;

            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   
            // this.JenjangDS = alfalah.core.newDataStore(
            //     "<?php echo e(url('/siswa/2/80')); ?>", false,
            //     {   s:"init", limit:this.page_limit, start:this.page_start }
            // );
            // this.JenjangDS.load();

            this.KelasDS = alfalah.core.newDataStore(
                "<?php echo e(url('/siswa/2/81')); ?>", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );

            this.RuangKelasDS = alfalah.core.newDataStore(
                "<?php echo e(url('/siswa/2/82')); ?>", false,
                {   s:"init", limit:this.page_limit, start:this.page_start }
            );

            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   bodyStyle:'padding:5px',
                // autoScroll : true,
                // autoHeight : true,
                title : 'Pengumuman',
                tbar: [
                    {   text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        scope : this
                    }    
                ],
                items: [ 
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [ // hidden columns
                        {   id : 'news_news_id',
                            xtype:'textfield',
                            fieldLabel: 'ID',
                            name: 'news_id',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'news_category_1',
                            xtype:'textfield',
                            fieldLabel: 'category_1',
                            name: 'category_1',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'news_status', 
                            xtype:'textfield',
                            fieldLabel: 'Status',
                            name: 'status',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'news_status_name', 
                            xtype:'textfield',
                            fieldLabel: 'Status.Name',
                            name: 'status_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        },
                        {   id : 'news_created_date', 
                            xtype:'textfield',
                            fieldLabel: 'Created Date',
                            name: 'created_date',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                            hidden : true,
                        }]
                    },
                    {   columnWidth:.3,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'news_publish_start_date',
                            xtype:'datefield',
                            fieldLabel: 'Start',
                            name: 'publish_start_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            value : new Date()
                        },
                        {   id : 'news_publish_end_date',
                            xtype:'datefield',
                            fieldLabel: 'End',
                            name: 'publish_end_date',
                            anchor:'95%',
                            allowBlank: false,
                            format: 'd/m/Y',
                            value : new Date()
                        },
                        ]
                    },
                    {   columnWidth:.7,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'news_title', 
                            xtype:'textfield',
                            fieldLabel: 'Title',
                            name: 'title',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : false,
                        },
                        {   id : 'news_category_1_name', 
                            xtype:'textfield',
                            fieldLabel: 'Category',
                            name: 'category_1_name',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : true,
                        },
                        ]
                    }]
                },
                {   layout:'column',
                    border:false,
                    items:[
                    // {   columnWidth:.3,
                    //     layout: 'form',
                    //     border:false,
                    //     items: [
                    //     // {   id : 'news_keyid_jenjang_sekolah', 
                    //     //     xtype : 'combo',
                    //     //     fieldLabel: 'Jenjang',
                    //     //     labelSeparator : '',
                    //     //     name: 'jenjang_sekolah',
                    //     //     anchor:'95%',
                    //     //     allowBlank: false,
                    //     //     readOnly : false,
                    //     //     store: this.JenjangDS,
                    //     //     displayField: 'jenjang_sekolah',
                    //     //     valueField: 'keyid_jenjang_sekolah',
                    //     //     mode : 'local',
                    //     //     forceSelection: true,
                    //     //     triggerAction: 'all',
                    //     //     selectOnFocus:true,
                    //     //     editable: false,
                    //     //     typeAhead: true,
                    //     //     width : 100,
                    //     //     value: 'ALL',
                    //     //     hidden : this.category,
                    //     //     listeners : { scope : this,
                    //     //         'select' : function (combo, record, indexVal)
                    //     //         {   
                    //     //             console.log('jenjang_sekolah on select');
                    //     //             console.log(combo);
                    //     //             console.log(record);
                    //     //             console.log(indexVal);

                    //     //             this.KelasDS.baseParams = Ext.apply( {},
                    //     //             {   task: alfalah.news.task,
                    //     //                 act: alfalah.news.act,
                    //     //                 a:2, b:0, s:"form", 
                    //     //                 limit:this.page_limit, start:this.page_start,
                    //     //                 jenjang_sekolah : record.data.jenjang_sekolah });
                    //     //             this.KelasDS.reload();
                    //     //             Ext.getCmp('news_nama_kelas').setValue('ALL');

                    //     //             this.RuangKelasDS.baseParams = Ext.apply( {},
                    //     //             {   task: alfalah.news.task,
                    //     //                 act: alfalah.news.act,
                    //     //                 a:2, b:0, s:"form", 
                    //     //                 limit:this.page_limit, start:this.page_start,
                    //     //                 jenjang_sekolah : record.data.jenjang_sekolah });
                    //     //             this.RuangKelasDS.reload();
                    //     //             Ext.getCmp('news_keyid_ruang_kelas').setValue('ALL');
                    //     //         },
                    //     //     }
                    //     // },
                    //     ]
                    // },
                    {   columnWidth:.5,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'news_keyid_nama_kelas', 
                            xtype : 'combo',
                            fieldLabel: 'Kelas',
                            labelSeparator : '',
                            name: 'nama_kelas',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : false,
                            store: this.KelasDS,
                            displayField: 'nama_kelas',
                            valueField: 'keyid_kelas',
                            hiddenName: 'keyid_kelas',
                            hiddenVAlue: '',
                            mode : 'local',
                            forceSelection: true,
                            triggerAction: 'all',
                            selectOnFocus:true,
                            editable: false,
                            typeAhead: true,
                            width : 100,
                            value: 'ALL',
                            hidden : this.category,
                            listeners : { scope : this,
                                'select' : function (combo, record, indexVal)
                                {   
                                    // console.log('nama_kelas on select');
                                    // console.log(combo);
                                    // console.log(record);
                                    // console.log(indexVal);

                                    this.RuangKelasDS.baseParams = Ext.apply( {},
                                    {   task: alfalah.news.task,
                                        act: alfalah.news.act,
                                        a:2, b:0, s:"form", 
                                        limit:this.page_limit, start:this.page_start,
                                        nama_kelas : record.data.nama_kelas });
                                    this.RuangKelasDS.reload();
                                    Ext.getCmp('news_keyid_ruang_kelas').setValue('ALL');
                                },
                            }
                        },
                        ]
                    },
                    {   columnWidth:.5,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'news_keyid_ruang_kelas', 
                            xtype : 'combo',
                            fieldLabel: 'Ruang',
                            labelSeparator : '',
                            name: 'ruang_kelas',
                            anchor:'95%',
                            allowBlank: false,
                            readOnly : false,
                            store: this.RuangKelasDS,
                            displayField: 'ruang_kelas',
                            valueField: 'keyid_nama_kelas',
                            mode : 'local',
                            forceSelection: true,
                            triggerAction: 'all',
                            selectOnFocus:true,
                            editable: false,
                            typeAhead: true,
                            width : 100,
                            value: 'ALL',
                            hidden : this.category,
                        },
                        ]
                    }]
                },  // payment term area
                {   layout:'column',
                    border:false,
                    items:[
                    {   columnWidth:.9,
                        layout: 'form',
                        border:false,
                        items: [
                        {   id : 'news_content', 
                            xtype:'htmleditor',
                            fieldLabel: 'Content',
                            name: 'content',
                            anchor:'95%',
                            allowBlank: true,
                            readOnly : false,
                        }, 
                            ]
                    }]
                }
                ]
            });
        },
        // build the layout
        build_layout: function()
        {   if (Ext.isObject(this.Records))
            {   the_title = "Edit Pengumuman"; }
            else { the_title = "New Pengumuman"; };

            this.Tab = new Ext.Panel(
            {   id : "news_form",
                jsId : "news_form",
                // title:  the_title,
                title : '<span style="background-color: yellow;">'+the_title+'</span>',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region: 'center',
                    xtype: 'tabpanel', // TabPanel itself has no title
                    activeTab: 0,
                    items:[this.Form]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   
            if (Ext.isObject(this.Records))
            {   Ext.each(this.Records.fields.items,
                    function(the_field)
                    {   //console.log(the_field.name);
                        switch (the_field.name)
                        {   case "news_id" :
                            case "title" :
                            case "category_1" :
                            case "category_1_name" :
                            case "content" :
                            case "status" :
                            case "status_name" :
                            case "created_date" :
                            {   Ext.getCmp('news_'+the_field.name).setValue(this.Records.data[the_field.name]);
                            };
                            break;
                            case "publish_start_date" :
                            case "publish_end_date" :
                            {   Ext.getCmp('news_'+the_field.name).setValue(alfalah.core.shortdateRenderer(this.Records.data[the_field.name],'Y-m-d', 'd/m/Y'));
                            };
                            break;
                            case "keyid_kelas" :
                            {   vt = this;
                                the_kelas = this.Records.data[the_field.name];
                                this.KelasDS.on( 'load', function( store, records, options ) 
                                {   if (vt.load_1) 
                                    {   Ext.getCmp('news_keyid_nama_kelas').setValue(the_kelas);
                                        // vt.load_1 = false;
                                    };
                                });
                            };
                            break;
                            case "nama_kelas":
                            {   
                                this.RuangKelasDS.baseParams = Ext.apply( {},
                                {   task: alfalah.news.task,
                                    act: alfalah.news.act,
                                    a:2, b:0, s:"form", 
                                    limit:this.page_limit, start:this.page_start,
                                    nama_kelas : this.Records.data[the_field.name] });
                            };
                            break;
                            case "keyid_nama_kelas" :
                            // case "nama_nama_kelas" :
                            {   vt = this;
                                the_ruang = this.Records.data[the_field.name];
                                // Ext.getCmp('news_keyid_ruang_kelas').setValue(the_ruang);
                                this.RuangKelasDS.on( 'load', function( store, records, options ) 
                                {   if (vt.load_2) 
                                    {   Ext.getCmp('news_keyid_ruang_kelas').setValue(the_ruang);
                                        // vt.load_2 = false;
                                        the_ruang = 'ALL';
                                    };
                                }); 
                            };

                            break;
                        };
                    }, this);
            };
            
            this.KelasDS.load();
            this.RuangKelasDS.load();
                

            // if (this.first_load) 
            // {   this.first_load = false;
            // };
        },
        // forms grid save records
        Form_save : function(button, event)
        {   var head_data = [];
            var json_data = [];
            var modi_data = [];
            var v_json = {};
            // header validation
            if (alfalah.core.validateFields([
                'news_title', 'news_category_1_name', 'news_publish_start_date', 'news_publish_end_date']))
            {   // detail validation
                // header
                head_data = alfalah.core.getHeadData([
                    'news_news_id', 'news_title', 
                    'news_category_1', 'news_category_1_name',
                    'news_publish_start_date', 'news_publish_end_date', 
                    'news_status', 'news_status_name',
                    'news_content', 'news_created_date',
                    // 'news_keyid_jenjang_sekolah', 
                    'news_keyid_nama_kelas', 
                    'news_keyid_ruang_kelas',
                    ]);
                

                a = Ext.getCmp('news_keyid_nama_kelas');
                head_data[0][a.name+'_text'] = a.lastSelectionText;
                // head_data.push(v_json);

                a = Ext.getCmp('news_keyid_ruang_kelas');
                head_data[0][a.name+'_text'] = a.lastSelectionText;
                // head_data.push(v_json);

                // submit data
                alfalah.core.submitForm(
                    'news_form', 
                    alfalah.news.news.DataStore,
                    "<?php echo e(url('/news/2/6')); ?>",
                    {   'x-csrf-token': alfalah.news.sid },
                    {   task: 'save',
                        head : Ext.encode(head_data),
                        json : {},
                    });
            };
        },
    }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.news.initialize, alfalah.news);
// end of file
</script>
<div>&nbsp;</div>