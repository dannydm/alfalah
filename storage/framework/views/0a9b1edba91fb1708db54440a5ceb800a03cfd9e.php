<script type="text/javascript">
// create namespace
Ext.namespace('alfalah.uangmuka');

// create application
alfalah.uangmuka = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.tabId = '<?php echo e($TABID); ?>';
    // private functions
    // public space
    return {
        centerPanel : 0,
        sid : '<?php echo e(csrf_token()); ?>',
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   this.centerPanel = Ext.getCmp(tabId);
            this.uangmuka.initialize();
        },
        // build the layout
        build_layout: function()
        {   this.centerPanel.beginUpdate();
            this.centerPanel.add(this.uangmuka.Tab);
            this.centerPanel.setActiveTab(this.uangmuka.Tab);
            this.centerPanel.endUpdate();
            alfalah.core.viewport.doLayout();
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   console.log(4); },
    }; // end of public space
}(); // end of app
// create application
alfalah.uangmuka.uangmuka= function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        // public methods
        initialize: function()
        {   this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {
            // var cbSelModel = new Ext.grid.CheckboxSelectionModel();
            this.Columns = [
                // cbSelModel,
                {   header: "Nomor Pengajuan", width : 100,
                    dataIndex : 'pengajuan_no', sortable: true,
                    tooltip:"Nomor pengajuan",
                    // renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    // {   //approve
                    //     if ( record.data.approve_status_2 == 1) { metaData.attr = "style = background-color:lime"; }
                    //     // not yet approve
                    //     else if ( record.data.approve_status_2 == null){ metaData.attr = "style = background-color:yellow;"; }
                    //     // rejected
                    //     else {  metaData.attr = "style = background-color:red;"; };

                    //     return value;
                    // }
                },
                {   header: "Keterangan", width : 200,
                    dataIndex : 'pengajuan_keterangan_id',
                    sortable: true,
                    tooltip:"Keterangan Pengajuan",
                },
                {   header: "Organisasi", width : 100,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    hidden :true,
                    tooltip:"Organisasi dan Urusan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width :200,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    hidden : true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 200,
                        dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                        tooltip:"nama sub kegiatan",
                        renderer : function(value, metaData, record, rowIndex, colIndex, store)
                        {   return alfalah.core.gridColumnWrap(value);
                        }
                    },

                {   header: "Jumlah", width : 100,
                    dataIndex : 'total_biaya', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Sisa Pengajuan", width : 100,
                    dataIndex : 'total_pengajuan', sortable: true,
                    tooltip:"Jumlah total sisa pengajuan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    { // value = (record.data.total_biaya);
                        if (value > 0) value = parseInt(record.data.total_biaya) - parseInt(record.data.total_pengajuan)
                        else value = (record.data.total_biaya);     
                        metaData.attr="style = text-align:right;";
                        // { metaData.attr = "style = background-color:yellow ;";}
                        return Ext.util.Format.number(value, '0,000');   
                    },
                },

                {   header: "Pengajuan by", width : 150,
                    dataIndex : 'created_by', sortable: true,
                    tooltip:"Diajukan oleh",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = ' ['+value+'] '+record.data.username;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Status", width : 50,
                    hidden : true,
                    dataIndex : 'status', sortable: true,
                    tooltip:" Status",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"uangmuka Created Date",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"uangmuka Last Updated",
                    css : "background-color: #DCFFDE;",
                    hidden :true,
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Records = Ext.data.Record.create(
            [   {name: 'id', type: 'integer'},
                // {name: 'modified_date', type: 'date'},
            ]);
            this.Searchs = [
                {   id: 'uangmuka_urusan',
                    cid: "organisasi_mrapbs_id_name",
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'uangmuka_organisasi',
                    cid: 'urusan_mrapbs_id_name',
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'uangmuka_uangmuka',
                    cid: 'kegiatan_mrapbs_id_name',
                    fieldLabel: 'Kegiatan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'uangmuka_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },

            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_uangmukaSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                handler : this.uangmuka_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "<?php echo e(url('/uangmuka/1/0')); ?>", false,
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();
            this.Ds_notiv_rapbs = alfalah.core.newDataStore(
                "<?php echo e(url('/uangmuka/1/200')); ?>", false,
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );
            this.Ds_notiv_rapbs.load();


            this.Grid = new Ext.grid.EditorGridPanel(
            {   
                store:  this.DataStore,
                columns: this.Columns,
                stripeRows :true,
                // selModel: cbSelModel,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.uangmuka.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Detail Kegiatan',
                        tooltip:'view Pengajuan berdasar rapbs.no',
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_pengajuan,
                        scope : this
                    },
                    {   print_type : "pdf",
                        text:'Print PDF',
                        hidden :true,
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : this.Grid_pdf,
                        scope : this
                    },
                    {   print_type : "xls",
                        text:'Print Excell',
                        tooltip:'Print to Excell SpreadSheet',
                        iconCls: 'silk-page-white-excel',
                        hidden :true,
                        handler : function(button, event){ alfalah.core.printButton(button, event, this.DataStore); },
                        scope : this
                    },
                    {   text:'Approve',
                        tooltip:'uangmuka Anggaran',
                        // xType : 'dataview',
                        hidden : true,
                        style:'background-color:lime',
                        iconCls: 'silk-tick',
                        handler : this.Grid_approve,
                        scope : this
                    },
                    {   text:'Not Yet Approve',
                        tooltip:'Anggaran Belum di approve',
                        // xType : 'dataview',
                        hidden : true,
                        style:'background-color:yellow',
                        iconCls: 'silk-stop',
                        // handler : this.Grid_reject,
                        scope : this
                    },'-',
                    {   text:'Reject',
                        tooltip:'Anggaran Ditolak',
                        // xType : 'dataview',
                        hidden : true,
                        style:'background-color:red',
                        iconCls: 'silk-cancel',
                        handler : this.Grid_reject,
                        scope : this
                    },
                    '->',
                    '_T O T A L_',
                    new Ext.form.TextField(
                    {   id : 'grand_total_id',
                        // store:this.DataStore,
                        displayField: 'total_data',
                        valueField: 'total_data',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        // listeners :
                        // {   "beforeedit" : this.Grid_grand_total },
                        // scope : this
                    }),
                ],
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_uangmukaGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_uangmukaPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_uangmukaPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_uangmukaGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        'records at a time'
                    ]
                }),
            });
        },
        // build the layout
        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_uangmukaTab",
                jsId : tabId+"_uangmukaTab",
                title:  "Data Pengajuan Kegiatan",
                region: 'center',
                layout: 'border',
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.uangmuka_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },
        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {
            this.DataStore.on( 'load', function( store, records, options )
            {
                console.log(records);
                var total_data = 0;
                Ext.each(records,
                    function(the_record)
                    { 
                        total_data = total_data + parseInt(the_record.data.total_biaya);
                    });

                console.log("total jumlah = "+total_data);
//                console.log(Ext.getCmp('grand_total_id').setValue(total_data));
                Ext.getCmp('grand_total_id').setValue(Ext.util.Format.number(total_data, '0,000'));
            });
            // this.DataStore.load();
        },

        Grid_pengajuan: function(button, event) // icon detail kegiatan
        {  var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record) 
            {
                var centerPanel = Ext.getCmp('center_panel');
                alfalah.uangmuka.pengajuan.initialize(the_record);
                centerPanel.beginUpdate();
                centerPanel.add(alfalah.uangmuka.pengajuan.Tab);
                centerPanel.setActiveTab(alfalah.uangmuka.pengajuan.Tab);
                centerPanel.endUpdate();
                alfalah.core.viewport.doLayout();
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },

        Grid_pdf : function(button, event)
        {     var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                console.log(the_record)
                alfalah.core.printOut(button, event, this.DataStore.proxy.url, 
                    {   s: "form",
                        uangmuka_no : the_record.uangmuka_no });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };
        },

        // Grid_pdf : function(button, event)
        // {     var the_record = this.Grid.getSelectionModel().selection;
        //     if ( the_record )
        //     {   the_record = the_record.record.data;
        //         console.log(the_record)
        //         alfalah.core.printOut(button, event, "<?php echo e(url('/appum/1/3')); ?>", 
        //             {   s: "form",
        //                 uangmuka_no : the_record.uangmuka_no });
        //     }
        //     else
        //     {   Ext.Msg.show(
        //             {   title:'I N F O ',
        //                 msg: 'No Data Selected ! ',
        //                 buttons: Ext.Msg.OK,
        //                 icon: Ext.MessageBox.INFO
        //             });
                
        //     };
        // },

        // uangmuka search button
        uangmuka_search_handler : function(button, event)
        {   var the_search = true;
            if ( this.DataStore.getModifiedRecords().length > 0 )
            {   Ext.Msg.show(
                    {   title:'W A R N I N G ',
                        msg: ' Modified Data Found, Do you want to save it before search process ? ',
                        buttons: Ext.Msg.YESNO,
                        fn: function(buttonId, text)
                            {   if (buttonId =='yes')
                                {   Ext.getCmp(tabId+'_uangmukaSaveBtn').handler.call();
                                    the_search = false;
                                } else the_search = true;
                            },
                        icon: Ext.MessageBox.WARNING
                     });
            }; 

            if (the_search == true) // no modification records flag then we can go to search
            {   the_parameter = alfalah.core.getSearchParameter(this.Searchs);
                this.DataStore.baseParams = Ext.apply( the_parameter,
                    {   task: alfalah.uangmuka.task,
                        act: alfalah.uangmuka.act,
                        a:2, b:0, s:"form",
                        limit:this.page_limit, start:this.page_start });
                this.DataStore.reload();
            };
        },
    }; // end of public space
}(); // end of app
// create application
alfalah.uangmuka.pengajuan = function()
{   // do NOT access DOM from here; elements don't exist yet
    // execute at the first time
    // private variables
    this.Tab;
    this.Grid;
    this.Columns;
    this.Records;
    this.DataStore;
    this.Searchs;
    this.SearchBtn;
    this.Form;
    this.updateTab;
    this.updateForm;
    this.DetailRecords;

    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        load_1 :true,
        // public methods
        initialize: function(the_records)  
        {   console.log('initialize uangmuka.pengajuan');
            if (the_records)
            {   console.log(the_records);
                this.Records = the_records; 
            };
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.total_biaya_rapbs();
        },

        // prepare the component before layout drawing
        prepare_component: function()
        {
            this.Columns = [
                {   header: "RAPBS.No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"RAPBS No",
                },
                {   header: "PENGAJUAN.No", width : 100,
                    dataIndex : 'pengajuan_no', sortable: true,
                    tooltip:"RAPBS No",
                },

                {   header: "Organisasi / Urusan", width : 150,
                    dataIndex : 'organisasi_mrapbs_id_name',
                    sortable: true,
                    tooltip:"Jenjang/Bidang/Departemen",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = '<b>'+value+'</b><br>'+record.data.urusan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Program / Kegiatan", width : 250,
                    dataIndex : 'program_mrapbs_id_name', sortable: true,
                    tooltip:"Nama Program",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   result = value+'<br>'+record.data.kegiatan_mrapbs_id_name;
                        return alfalah.core.gridColumnWrap(result);
                    }
                },
                {   header: "Sub Kegiatan", width : 200,
                    dataIndex : 'sub_kegiatan_mrapbs_id', sortable: true,
                    tooltip:"nama sub kegiatan",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   return alfalah.core.gridColumnWrap(value);
                    }
                },
                {   header: "Sumber Dana", width : 150,
                    dataIndex : 'sumberdana_id_name', sortable: true,
                    // hidden :true,
                    tooltip:"sumber pendanaan",
                    // editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Jumlah Pengajuan Biaya", width : 100,
                    id : 'total_biaya_urusan',
                    dataIndex : 'total', sortable: true,
                    tooltip:"Jumlah total biaya",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   metaData.attr="style = text-align:right;";
                        return Ext.util.Format.number(value, '0,000');
                    },
                },
                {   header: "Status", width : 50,
                    dataIndex : 'status', sortable: true,
                    tooltip:" Status",
                },
                {   header: "Created", width : 50,
                    dataIndex : 'created', sortable: true,
                    tooltip:"kegiatan Created Date",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                },
                {   header: "Updated", width : 50,
                    dataIndex : 'modified_date', sortable: true,
                    tooltip:"kegiatan Last Updated",
                    css : "background-color: #DCFFDE;",
                    renderer: function(value){  return alfalah.core.dateRenderer(value); }
                }
            ];
            this.Searchs = [
                {   id: 'pengajuan_urusan',
                    cid: "urusan_mrapbs_id_name",
                    fieldLabel: 'Urusan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengajuan_organisasi',
                    cid: 'organisasi_mrapbs_id_name',
                    fieldLabel: 'Organisasi',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengajuan_kegiatan',
                    cid: 'kegiatan_mrapbs_id_name',
                    fieldLabel: 'Kegiatan',
                    labelSeparator : '',
                    xtype: 'textfield',
                    width : 120
                },
                {   id: 'pengajuan_status',
                    cid: 'status',
                    fieldLabel: 'Status',
                    labelSeparator : '',
                    xtype : 'combo',
                    store : new Ext.data.SimpleStore(
                    {   fields: ['status'],
                        data : [[''], ['Active'], ['Inactive']]
                    }),
                    displayField:'status',
                    valueField :'status',
                    mode : 'local',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    editable: false,
                    width : 100,
                    value: 'Active'
                },
            ];
            this.SearchBtn = new Ext.Button(
            {   id : tabId+"_pengajuanSearchBtn",
                fieldLabel: '',
                text:'Search',
                tooltip:'Search',
                iconCls: 'silk-zoom',
                xtype: 'button',
                width : 120,
                // handler : this.kegiatan_search_handler,
                scope : this
            });
            this.DataStore = alfalah.core.newDataStore(
                "<?php echo e(url('/uangmuka/1/99')); ?>", false, 
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );
            // this.DataStore.load();

            this.Ds_notiv_rapbs = alfalah.core.newDataStore(
                "<?php echo e(url('/uangmuka/1/200')); ?>", false,
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );
            this.Ds_notiv_rapbs.load();

            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                //selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
                enableColLock: false,
                //trackMouseOver: true,
                loadMask: true,
                height : alfalah.uangmuka.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                //stripeRows : true,
                // inline buttons
                //buttons: [{text:'Save'},{text:'Cancel'}],
                //buttonAlign:'center',
                tbar: [
                    {   text:'Pengajuan Uang Muka',
                        tooltip:'mengajukan uang muka kegiatan / biaya', 
                        iconCls: 'silk-page-white-edit',
                        handler : this.Grid_uangmuka,
                        scope : this
                    },
                    {   print_type : "pdf",
                        text:'Print PDF',
                        // hidden :true,
                        tooltip:'Print to PDF',
                        iconCls: 'silk-page-white-acrobat',
                        handler : this.Grid_pdf,
                        scope : this
                    },

                    '->',
                    '_T O T A L_B I A Y A',
                    new Ext.form.TextField(
                    {   id : 'total_uangmuka_id',
                        // store:this.DataStore,
                        displayField: 'total_uangmuka',
                        valueField: 'total_uangmuka',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                ],
 
                bbar: new Ext.PagingToolbar(
                {   id : tabId+'_pengajuanumGridBBar',
                    store: this.DataStore,
                    pageSize: this.page_limit,
                    displayInfo: true,
                    emptyMsg: 'No data found',
                    items : [
                        '-',
                        'Displayed : ',
                        new Ext.form.ComboBox(
                        {   id : tabId+'_pengajuanumPageCombo',
                            store: new Ext.data.SimpleStore(
                                        {   fields: ['value'],
                                            data : [[50],[75],[100],[125],[150]]
                                        }),
                            displayField:'value',
                            valueField :'value',
                            value : 75,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            hiddenName: 'pagesize',
                            width:50,
                            listeners : { scope : this,
                                'select' : function (a,b,c)
                                {   this.page_limit = Ext.get(tabId+'_pengajuanumPageCombo').getValue();
                                    bbar = Ext.getCmp(tabId+'_pengajuanumGridBBar');
                                    bbar.pageSize = parseInt(this.page_limit);
                                    this.page_start = ( bbar.getPageData().activePage -1) * this.page_limit;
                                    this.SearchBtn.handler.call(this.SearchBtn.scope);
                                }
                            }
                        }),
                        'records at a time',
                    ]
                }),
            });

        },

        build_layout: function()
        {   this.Tab = new Ext.Panel(
            {   id : tabId+"_pengajuanumTab",
                jsId : tabId+"_pengajuanumTab",
                title:  "PENGAJUAN UANG MUKA",
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   title: 'Parameters',
                    region: 'east',     // position for region
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 400,
                    collapsible: true,
                    layout : 'fit',
                    items:[
                    {   //title: 'S E A R C H',
                        labelWidth: 50,
                        defaultType: 'textfield',
                        xtype: 'form',
                        frame: true,
                        autoScroll : true,
                        items : this.Searchs,
                        tbar: [
                            {   text:'Search',
                                tooltip:'Search',
                                iconCls: 'silk-zoom',
                                handler : this.newrapbs_search_handler,
                                scope : this,
                            }]
                    }]
                },
                {   region: 'center',     // center region is required, no width/height specified
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }
                ]
            });
        },

        // finalize the component and layout drawing
        finalize_comp_and_layout: function()
        {   
            the_records = this.Records
            console.log('finalize_comp_and_layout uangmuka.pengajuan');
                console.log(the_records);
            if (the_records) {

            this.DataStore.baseParams = Ext.apply( the_parameter,    
            { 
                s:"forms", limit:this.page_limit, 
                start:this.page_start,
                pengajuan_no : the_records.record.data.pengajuan_no 
            })
            this.DataStore.load();}  
            console.log(this.DataStore.baseParams);  
        },

        // Grid_UM: function(button, event)
        // {   this.Grid.stopEditing();
        //     var selection_data = alfalah.pencairan.pencairan.Grid.getSelectionModel().selection;
        //     if (selection_data)
        //     {   var app_data = selection_data.record.data.approve_status_3;
        //         var data = selection_data.record.data.uangmuka_no;

        //         if (app_data == 0) {
                    
        //             Ext.MessageBox.show({
        //                 title: 'Verification',
        //                 msg: 'Anda harus Menyelesaikan Realisasi No :.<br /><br />',
        //                 buttons: Ext.MessageBox.OKCANCEL,
        //                 width : 350,
        //               //  activeItem : true,
        //                 fn: function (buttonId, jenis_byr, catatan, opt) {
        //                     if (buttonId =='ok' )
        //                         {   
        //                             Ext.Ajax.request(
        //                             {   method: 'POST',
        //                                 url: "<?php echo e(url('/pencairan/1/100')); ?>",
        //                                 headers: {   'x-csrf-token': alfalah.pencairan.sid },
        //                                 params: {   uangmuka_no: data,
        //                                             jenis_byr : Ext.get('jenis_bayar').getValue(), 
        //                                             catatan : Ext.get('cttn_id').getValue(),
        //                                         },
        //                                 success: function(response)
        //                                 {   var the_response = Ext.decode(response.responseText);
        //                                     if (the_response.success == false)
        //                                     {   Ext.Msg.show(
        //                                         {   title :'E R R O R ',
        //                                             msg : 'Server Message : '+'\n'+the_response.message,
        //                                             buttons: Ext.Msg.OK,
        //                                             icon: Ext.MessageBox.ERROR
        //                                         });
        //                                     }
        //                                     else
        //                                     {   //alfalah.pencairan.pencairan.DataStore.reload(); 
        //                                         Ext.Msg.alert("Print Kwitansi : ", "Harap Menunggu", 
        //                                         function (btn) { if (btn == "ok") 
        //                                         { 
        //                                             Ext.getCmp('print_kwitansi_id').el.dom.click();
        //                                           //  alfalah.pencairan.pencairan.DataStore.reload();
        //                                         }});
        //                                     };
        //                                 },
        //                                 failure: function()
        //                                 {   Ext.Msg.alert("Save Data Failed : ", "Server communication failure");
        //                                 },
        //                             });
                                       
        //                     }
                            
        //                 }
        //             }
        //         ) 
                    
        //         }
        //         else
        //         {  
        //             Ext.Msg.show(
        //             {   title   :'I N F O ',
        //                 msg     : 'Data Has Been Verification',
        //                 buttons : Ext.Msg.OK,
        //                 icon    : Ext.MessageBox.INFO,
        //                 width   : 200
        //             });
                   

        //         }    
        //     }
        //     else
        //     {   Ext.Msg.show(
        //         {   title   :'I N F O ',
        //             msg     : 'No Selected Record ! ',
        //             buttons : Ext.Msg.OK,
        //             icon    : Ext.MessageBox.INFO,
        //             width   : 200
        //         });
        //     };
        // },

        Grid_uangmuka: function(button, event)
        { 
            var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   var get_data_uangmuka = alfalah.uangmuka.pengajuan.Ds_notiv_rapbs.data.items;
                Ext.each(get_data_uangmuka,
                    function(the_field)
                        { console.log(the_field.data.created_by);
                            if (the_field.data.approve_status_3 == 1 && the_field.data.approve_status_5 == 0) 
                            { 
                                Ext.Msg.show({
                                    title: 'P E R H A T I A N',
                                    msg: 'Anda harus Menyelesaikan Realisasi No : <table class ="x-data-table"><thead><th>Rapbs No</th><><th>Kegiatan</th></thead></table><br />',
                                    buttons: Ext.Msg.OKCANCEL,
                                    icon: Ext.Msg.WARNING,
                                    minWidth: 400,
                                    width: 400,
                                    fn: function (buttonId, jenis_byr, catatan, opt) {}
                                });
                            }
                        });

                        var centerPanel = Ext.getCmp('center_panel');
                        alfalah.uangmuka.kegiatan.initialize(the_record);
                        centerPanel.beginUpdate();
                        centerPanel.add(alfalah.uangmuka.kegiatan.Tab);
                        centerPanel.setActiveTab(alfalah.uangmuka.kegiatan.Tab);
                        centerPanel.endUpdate();
                        alfalah.core.viewport.doLayout();

            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
            };
        },

        Grid_pdf : function(button, event)
        {     var the_record = this.Grid.getSelectionModel().selection;
            if ( the_record )
            {   the_record = the_record.record.data;
                console.log(the_record)
                alfalah.core.printOut(button, event, "<?php echo e(url('/appum/1/3')); ?>", 
                    {   s: "form",
                        uangmuka_no : the_record.uangmuka_no });
            }
            else
            {   Ext.Msg.show(
                    {   title:'I N F O ',
                        msg: 'No Data Selected ! ',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                
            };
        },

        total_biaya_rapbs: function()
        { 
            alfalah.uangmuka.pengajuan.DataStore.on( 'load', function( store, records, options )
            {   console.log('records');
                console.log(records);
                var total_biaya_rapbs = 0;
                Ext.each(records,
                    function(the_record)
                    { console.log('the_record.data');
                        console.log(the_record.data);
                        total_biaya_rapbs = total_biaya_rapbs + parseInt(the_record.data.total);
                    });

                Ext.getCmp('total_uangmuka_id').setValue(Ext.util.Format.number(total_biaya_rapbs, '0,000'));
            });
        },

    }; // end of public space
}(); // end of app
// create application
alfalah.uangmuka.kegiatan= function()
{   // do NOT access DOM from here; elements don't exist yet
// execute at the first time
    // private variables
    this.Tab;
    this.Searchs;
    this.SearchBtn;
    this.Form;
    this.updateTab;
    this.updateForm;
    this.Records;
    this.DetailRecords;
    // private functions
    // public space
    return {
        // execute at the very last time
        // public variable
        page_limit : 75,
        page_start : 0,
        load_1     : true,
        // user_data : '',
        // public methods
        initialize: function(the_records)  
        {   console.log('initialize uangmuka kegiatan');
            if (the_records)
            {   console.log(the_records);
                this.Records = the_records; 
            };
            this.prepare_component();
            this.build_layout();
            this.finalize_comp_and_layout();
            this.total_detail_uangmuka();
        },
        // prepare the component before layout drawing
        prepare_component: function()
        {   console.log('prepare');
            console.log(this.Records);

            /************************************
                F O R M S
            ************************************/
            this.Form = new Ext.FormPanel(
            {   id : 'uangmukaFrm',
                // width: '100%',
                frame: true,
                title : 'H E A D E R',
                // autoHeight: true,
                bodyStyle: 'padding: 10px 10px 0 10px;',
                labelWidth: 100,
                defaults: {
                    anchor: '100%',
                    allowBlank: false,
                    msgTarget: 'side'
                },
                items: [
                {   layout:'column',
                    border:false,
                    items:
                    [
                        {   columnWidth:.3,
                            layout: 'form',
                            border:false,
                            items: [ // hidden columns
                                {   id : 'uangmukaFrm_id',
                                    xtype:'textfield',
                                    fieldLabel: 'ID',
                                    name: 'id',
                                    anchor:'95%',
                                    allowBlank: true,
                                    readOnly : true,
                                    value : '',
                                    hidden : true,
                                },
                                {   id : 'uangmukaFrm_created_date', 
                                    xtype:'textfield',
                                    fieldLabel: 'Created Date',
                                    name: 'created_date',
                                    anchor:'95%',
                                    allowBlank: true,
                                    readOnly : true,
                                    hidden : true,
                                    value : '',
                                },
                                {   id : 'uangmukaFrm_tahun_ajaran_id',  
                                    xtype:'textfield',
                                    fieldLabel: 'Tahun',
                                    name: 'tahun_ajaran_id',
                                    anchor:'65%',
                                    allowBlank: false,
                                    readOnly : true,
                                    value : '<?php echo e($TAHUNANGGARAN_ID); ?>',
                                    hidden : true,
                                },
                                {   id : 'uangmukaFrm_uangmuka_no', 
                                    xtype:'textfield',
                                    fieldLabel: 'No ',
                                    name: 'uangmuka_no',
                                    anchor:'95%',
                                    allowBlank: true,
                                    readOnly : true,
                                    value : 'by-system',
                                },
                                {   id : 'uangmukaFrm_uangmuka_tgl', 
                                    xtype:'datefield',
                                    fieldLabel: 'Bulan Pengajuan',
                                    format: 'm/Y',
                                    name: 'uangmuka_tgl',
                                    anchor:'95%',
                                    // readOnly : true,
                                    allowBlank: false,
                                    value : new Date(),
                                },
                                {   id : 'uangmukaFrm_uangmuka_keterangan_id', 
                                    xtype:'textfield',
                                    fieldLabel: 'keterangan',
                                    name: 'uangmuka_keterangan_id',
                                    value: 'Pengajuan UM'+' '+ new Date().format('d/m/Y'), 
                                    anchor:'95%',
                                    // readOnly : true,
                                    allowBlank: false,
                                },
                                {   id : 'uangmukaFrm_uangmuka_due_date', 
                                    xtype:'datefield',
                                    fieldLabel: 'Jatuh Tempo',
                                    format: 'd/m/Y',
                                    name: 'uangmuka_due_date',
                                    anchor:'95%',
                                    readOnly : true,
                                    allowBlank: false,
                                    // value : '',
                                    listeners : { scope : this,
                                        'beforerender' : function (record, a, get_due_date)
                                        {  
                                           // var a = new Date().format('d/m/Y');
                                            var get_due_date = new Date(new Date().getTime()+(14*24*60*60*1000));            
                                            Ext.getCmp('uangmukaFrm_uangmuka_due_date').setValue(get_due_date);
                                            
                                        }
                                    }
                                },
                            ]
                    },
                    {   columnWidth:.5,
                            layout: 'form',
                            border: false,
                            items : [ 
                                {
                                    xtype: 'fieldset',
                                    //title: 'Keterangan ',
                                    autoHeight: true,
                                    anchor:'100%',
                                    items: [
                                        {   border:false,
                                            contentEl: 'syarat_content'
                                        }]
                                }
                            ]
                        }
                ]
            }],
        });
            /************************************
                G R I D S
            ************************************/ 
            var cbSelModel = new Ext.grid.CheckboxSelectionModel();  
            this.Columns = [ 
                cbSelModel,
                {   header: "ID", width : 50,
                    dataIndex : 'rapbs_id', sortable: true,
                    tooltip:"ID",
                    hidden: true,
                },
                {   header: "Pengajuan No", width : 100,
                    dataIndex : 'pengajuan_no', sortable: true,
                    tooltip:"Doc.No",
                },
                {   header: "Apbs No", width : 100,
                    dataIndex : 'rapbs_no', sortable: true,
                    tooltip:"Doc.No",
                },
                {   header: "Pos.Belanja.No", width : 100,
                    dataIndex : 'coa_id', sortable: true,
                    tooltip:"Pos Belanja",
                    readonly:true,
                },
                {   header: "Name", width : 200,
                    dataIndex : 'coa_name', sortable: true,
                    readonly:true,
                    tooltip:"Pos Belanja Name",
                    editor: new Ext.form.ComboBox(
                    {   store: this.coaDS_2,
                        typeAhead: false,
                        width: 250,
                        readonly:true,
                        displayField: 'display',
                        valueField: 'coa_name',
                        forceSelection: true,
                        loadingText: 'Searching...',
                        pageSize:25,
                        hideTrigger:true,
                        // triggerAction: "All",
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">','{display}','</div></tpl>'
                            ),
                        itemSelector: 'div.search-item',
                        listeners : {
                            scope: this,
                                'select' : function (combo, record, indexVal)
                                {   combo.gridEditor.record.data.coa_id = record.data.mcoa_id;
                                    combo.gridEditor.record.data.coa_name = record.data.coa_name;
                                    alfalah.uangmuka.forms.Grid.getView().refresh();
                                },
                        }
                    }),
                },
                {   header: "Uraian", width : 150,
                    dataIndex : 'uraian', sortable: true,
                    tooltip:"Uraian",
                    readonly:true,
                },
                {   header: "Volume", width : 100,
                    dataIndex : 'vol_ajuan', sortable: true,
                    tooltip:"Volume",
                    readonly:true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.volume = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Satuan", width : 100,
                    dataIndex : 'satuan', sortable: true,
                    tooltip:"Satuan",
                    readonly:true,
                },
                {   header: "Tarif", width : 100,
                    dataIndex : 'tarif', sortable: true,
                    tooltip:"Tarif",
                    readonly:true,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {   
                        record.data.tarif = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {
                    header: "Jumlah", width : 100,
                    dataIndex : 'jum_ajuan', sortable: true,
                    readonly:true,
                    tooltip:"Jumlah",
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {  
                        record.data.jum_ajuan = value;
                        return Ext.util.Format.number(value, '0,000');
                    }
                },
                {   header: "Ber-ulang", width : 60,
                    dataIndex : 'berulang', sortable: true,
                    tooltip:"variabel pembagi anggaran yg berulang",
                    hidden:true,
                    editor : new Ext.form.TextField({allowBlank: false}),
                },
                {   header: "Status", width : 100,
                    dataIndex : 'status_name', sortable: true,
                    readonly:true,
                    hidden :true,
                    tooltip:"Status",
                    editor :  new Ext.Editor(
                        new Ext.form.ComboBox(
                        {   store: new Ext.data.SimpleStore({
                                            fields: ["label"],
                                            data : [["Active"], ["InActive"]]
                                        }),
                            displayField:"label",
                            valueField:"label",
                            mode: 'local',
                            typeAhead: true,
                            triggerAction: "all",
                            selectOnFocus:true,
                            forceSelection :true
                        }),
                        {autoSize:true}
                    )
                },
            ];
            
            this.DataStore = alfalah.core.newDataStore(
                "<?php echo e(url('/uangmuka/1/10')); ?>", false,
                {   s:"forms", limit:this.page_limit, start:this.page_start }
            );
            //  this.DataStore.load();

            this.Grid = new Ext.grid.EditorGridPanel(
            {   store:  this.DataStore,
                columns: this.Columns,
                selModel: cbSelModel,
                enableColLock: false,
                loadMask: true,
                height : alfalah.uangmuka.centerPanel.container.dom.clientHeight-50,
                anchor: '100%',
                autoScroll  : true,
                frame: true,
                tbar: [
                    {   text:'Save',
                        tooltip:'Save Record',
                        iconCls: 'icon-save',
                        handler : this.Form_save,
                        hidden :false,
                        scope : this
                    },
                    '-',
                    {   text:'Approve',
                        tooltip:'Approve Record',
                        iconCls: 'silk-tick',
                        hidden :true,
                        handler : this.Grid_approve,
                        scope : this
                    },
                    '->',
                    '_T O T A L_B I A Y A',
                    new Ext.form.TextField(
                    {   id : 'total_detail_id',
                        // store:this.DataStore,
                        displayField: 'total_uangmuka',
                        valueField: 'total_uangmuka',
                        allowBlank : false,
                        readOnly : true,
                        style: "text-align: right; background-color: #ffff80; background-image:none;",
                        mode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        selectOnFocus: true,
                    }),
                ],

            });

            this.detailTab = new Ext.Panel(
            {   title:  "D E T A I L",
                region: 'center',
                layout: 'border',
                items: [
                {   region: 'center', 
                    xtype: 'container',
                    layout: 'fit',
                    items:[this.Grid]
                }],
            });
        },
        // build the layout
        build_layout: function()
        {   
            if (Ext.isObject(this.Records))
                {   the_title = "PENGAJUAN UANG MUKA ANGGARAN"; }
            else { the_title = "PENGAJUAN UANG MUKA ANGGARAN"; };

            this.Tab = new Ext.Panel(
            {   id : tabId+"_kegiatanTab",
                jsId : tabId+"_kegiatanTab",
                title:  '<span style="background-color: yellow;">'+the_title+'</span>',
                iconCls: 'silk-note_add',
                region: 'center',
                layout: 'border',
                closable : true,
                autoScroll  : true,
                items: [
                {   region   : 'center', // center region is required, no width/height specified
                    xtype    : 'tabpanel',
                    activeTab: 0,
                    items    : [this.Form, this.detailTab]
                }]
            });
        },
    // finalize the component and layout drawing
    finalize_comp_and_layout: function()

    {   console.log('finalize_comp_and_layout');

        the_records = this.Records   
        this.TargetRecord = Ext.data.Record.create(
        [ 
            {name: 'rapbs_id', type: 'string'},
            {name: 'rapbs_no', type: 'string'},
            {name: 'pengajuan_id', type: 'string'},
            {name: 'pengajuan_no', type: 'string'},
        ]);
        if (the_records)
        { 
            console.log('the_records');
            console.log(the_records);
            var json_data = [];
            var v_data = {};
            Ext.each(the_records,
                function(the_record)
                {    
                    console.log('the_record.record.test searching');
                    console.log(the_records.record.data.pengajuan_no);
                    v_data = {};
                    v_data[ 'rapbs_no' ] = the_records.record.data.rapbs_no;
                    json_data.push(v_data);  
                }, this );
            this.DataStore.baseParams = Ext.apply( the_parameter,
                {  
                    s:"forms", limit:this.page_limit, start:this.page_start,
                    rapbs_no : Ext.encode(json_data),
                });
                console.log('this.DataStore.baseParams');
            console.log(this.DataStore.baseParams);
            this.DataStore.load();
        };
    },

    Form_save : function(button, event)
    {   var head_data = [];
        var detail1_data =[];
        var json_data = [];
        var modi_data = [];
        var v_json = {};
        
        console.log('form save');
        // header validation
        if (alfalah.core.validateFields([
            'uangmukaFrm_uangmuka_no','uangmukaFrm_uangmuka_tgl','uangmukaFrm_uangmuka_keterangan_id','uangmukaFrm_uangmuka_due_date']))
        {          
            data_pengajuan = alfalah.core.getHeadData(alfalah.uangmuka.kegiatan.Form.getForm().items.keys);                    
            data_detail1 = alfalah.core.getDetailData(alfalah.uangmuka.pengajuan.Grid.getSelectionModel().selection.record);
            data_detail2 = alfalah.core.getDetailData(alfalah.uangmuka.kegiatan.Grid.getSelectionModel().selections.items);
 
            data_detail = Ext.apply(data_detail2)

            console.log('data_pengajuan');
            console.log(data_pengajuan);

            console.log('data_detail1');
            console.log(data_detail);

            console.log('data_detail2');
            console.log(data_detail2);
            
            alfalah.core.submitForm(
                tabId+"_kegiatanTab", 
                alfalah.uangmuka.kegiatan.DataStore,
                "<?php echo e(url('/uangmuka/1/1')); ?>",
                {   'x-csrf-token': alfalah.uangmuka.sid },
                {   task: 'save',
                    head : Ext.encode(data_pengajuan),
                    json : Ext.encode(data_detail),
                    detail1 : Ext.encode(data_detail1),
                });
            };
        },

        total_detail_uangmuka : function()
        { 
            alfalah.uangmuka.kegiatan.DataStore.on( 'load', function( store, records, options )
            {   console.log('records');
                console.log(records);
                var total_biaya_rapbs = 0;
                Ext.each(records,
                    function(the_record)
                    { console.log('the_record.data');
                        console.log(the_record.data);
                        total_biaya_rapbs = total_biaya_rapbs + parseInt(the_record.data.jum_ajuan);
                    });

                Ext.getCmp('total_detail_id').setValue(Ext.util.Format.number(total_biaya_rapbs, '0,000'));
            });
        },


     }; // end of public space
}(); // end of app
// On Ready
Ext.onReady(alfalah.uangmuka.initialize, alfalah.uangmuka);
// end of file
</script>
<div>&nbsp;
<div id="syarat_content" class="x-hide-display">
        <div align="left">
            <ol><h1><big>PASTIKAN DATA ANDA SUDAH BENAR !!!</big></h1></ol>
            <ol>
                <li><h3>1. Nomor Uang Muka Ditentukan By System</h3></li><br>
                <li><h3>2. Bulan Pengajuan Adalah bulan terlaksananya Kegiatan</h3></li><br>
                <li><h3>3. Field Keterangan Boleh diganti, Default adalah "Pengajuan dan tanggal Pengajuan"</h3></li><br>
                <li><h3>4. Jatuh tempo adalah tanggal jatuh tempo 14 hari sejak pengajuan ini dibuat </h3></li><br>
                </li>
                <br>
            </ol>
            <ol><h1><big>DATA AKAN TERHAPUS SECARA OTOMATIS JIKA DALAM 14 HARI PENGAJUAN INI TIDAK DIAMBIL !!!</big></h1></ol>
        </div>
    </div>
</div>
