<?php

use Illuminate\Http\Request;
// use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Administrator\MUser;
use App\Models\Siswa\Siswa;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/ 

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
 });

//Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/dashboard/{task}/{subtask}', 'DashboardController@index');
//Route::post('logout', 'Auth\LoginController@logout');

Route::get('/realisasi/{task}/{subtask}', 'Newanggaran\RealisasiController@index');

