<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/', 'WelcomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard/{task}/{subtask}', 'DashboardController@index');
Route::get('logout', 'Auth\LoginController@logout');
// Administrator Route
Route::any('/country/{task}/{subtask}', 'Administrator\CountryController@index');
Route::any('/company/{task}/{subtask}', 'Administrator\CompanyController@index');
Route::any('/permission/{task}/{subtask}', 'Administrator\PermissionController@index');
Route::any('/parameter/{task}/{subtask}', 'Administrator\ParameterController@index');
Route::any('/tahunajaran/{task}/{subtask}', 'Administrator\TahunajaranController@index');
Route::any('/mcoa/{task}/{subtask}', 'Administrator\McoaController@index');
Route::any('/mrapbs/{task}/{subtask}', 'Administrator\MrapbsController@index');
Route::any('/employee/{task}/{subtask}', 'HR\EmployeeController@index');
Route::any('/anggaranuser/{task}/{subtask}', 'Administrator\AnggaranuserController@index');

// Application Route
Route::any('/siswa/{task}/{subtask}', 'Siswa\SiswaController@index');
Route::any('/news/{task}/{subtask}', 'News\NewsController@index');
Route::any('/sales/{task}/{subtask}', 'Sales\SalesController@index');
Route::any('/purchase/{task}/{subtask}', 'Purchase\PurchaseController@index');
Route::any('/shipping/{task}/{subtask}', 'Shipping\ShippingController@index');
Route::any('/inventory/{task}/{subtask}', 'Inventory\InventoryController@index');
Route::any('/accounting/{task}/{subtask}', 'Accounting\AccountingController@index');
Route::any('/administration/{task}/{subtask}', 'Administration\AdministrationController@index');
Route::any('/fpdf', 'Controller@create_report_xls');

// PPDB Application Route
Route::any('/ppdb/{task}/{subtask}', 'Ppdb\PpdbController@index');

//anggaran aplication route
Route::any('/anggaran/{task}/{subtask}', 'Anggaran\AnggaranController@index');
Route::any('/newrapbs/{task}/{subtask}', 'Newanggaran\NewanggaranController@index');
Route::any('/newrapbskeu/{task}/{subtask}', 'Newanggaran\NewrapbskeuController@index');
Route::any('/approvalusulan/{task}/{subtask}', 'Newanggaran\ApprovalusulanController@index');
Route::any('/pengajuan/{task}/{subtask}', 'Newanggaran\PengajuanController@index');
Route::any('/apby/{task}/{subtask}', 'Newanggaran\ApbyController@index');
Route::any('/kegiatan/{task}/{subtask}', 'Newanggaran\KegiatanController@index');
Route::any('/approval/{task}/{subtask}', 'Newanggaran\ApprovalController@index');
Route::any('/approvalketua/{task}/{subtask}', 'Newanggaran\ApprovalketuaController@index');
Route::any('/uangmuka/{task}/{subtask}', 'Newanggaran\UangmukaController@index');
Route::any('/appum/{task}/{subtask}', 'Newanggaran\AppumController@index');
Route::any('/approvalcair/{task}/{subtask}', 'Newanggaran\ApprovalcairController@index');
Route::any('/pencairan/{task}/{subtask}', 'Newanggaran\PencairanController@index');
Route::any('/realisasi/{task}/{subtask}', 'Newanggaran\RealisasiController@index');
Route::any('/monitoring/{task}/{subtask}', 'Newanggaran\MonitoringController@index');
// Route::get('/test', function (Request $request) {
//     echo "<BR>HUAHAHAHA<BR>";
//     dd($_SESSIONS());

// });

// Route::get('/test', function (Codedge\Fpdf\Fpdf\Fpdf $fpdf) {
//     // $report = new \Fpdf();
//     $fpdf->AddPage();
//     $fpdf->SetFont('Courier', 'B', 18);
//     $fpdf->Cell(50, 25, 'Hello World!');
//     $fpdf->Output();

//     // Fpdf::AddPage();
//     // Fpdf::SetFont('Courier', 'B', 18);
//     // Fpdf::Cell(50, 25, 'Hello World!');
//     // Fpdf::Output();

// });

// Route::get('/test', function () {
// 	// this is query builder sample
// 	$results = DB::table('asset_reg_master as a')
//                 ->leftjoin('asset_reg_detail as b', 'a.doc_no', '=', 'b.doc_no')
//                 ->leftjoin('m_item as x1', 'b.ref_item_id', '=', 'x1.item_id')
//                 ->leftjoin('grin_detail as x2', function($join)
//                     {   $join->on('a.ref_no', '=', 'x2.grin_no')
//                              ->on('b.ref_item_id', '=', 'x2.item_id');
//                     })
//                 ->leftjoin('purchase_order_detail as x3', function($join)
//                     {   $join->on('x2.stock_ref_no', '=', 'x3.po_no')
//                              ->on('x2.item_id', '=', 'x3.item_id');
//                     })
//                 ->leftjoin('asset_itd_hardware as x4', 
//                     'b.asset_id', '=', 'x4.asset_id')
//                 ->leftjoin('asset_itd_hardware_history as x5', 
//                     'b.asset_id', '=', 'x5.asset_id')
//                 ->leftjoin('asset_itd_license as x6', 
//                     'b.asset_id', '=', 'x6.os_asset_id')
//                 ->leftjoin('asset_itd_license as x7', 
//                     'b.asset_id', '=', 'x7.cal_os_asset_id')
//                 ->leftjoin('asset_itd_license as x8', 
//                     'b.asset_id', '=', 'x8.office_asset_id')
//                 ->select(DB::raw(
//                         'a.doc_no', 'a.ref_no', 'a.doc_date', 'a.remark', 
//                         'a.ref_vendor_id', 'a.created_date', 'a.modified_date',
//                         'b.*',
//                         'x1.name as item_name', 'x2.stock_grin_no', 'x2.stock_ref_no as po_no',
//                         'case
//                             when x4.name is not null then x4.name
//                             when x6.os_asset_id is not null then x6.name
//                             when x7.cal_os_asset_id is not null then x7.name
//                             when x8.office_asset_id is not null then x8.name
//                             else x5.name
//                         end as deviceid',
//                         "array_to_string(array(
//                                 select pr_no
//                                 from purchase_order_detail
//                                 where po_no = x2.stock_ref_no
//                                 group by pr_no),', '
//                         ) as pr_no",
//                         'x5.id as historyid', 'x6.os_asset_id'))
//                 ->wherein('a.doc_type', ['FIP', 'FIO'])
//                 ->orderby('a.doc_no', 'asc');
	
// 	if ($request->doc_no)
//             {   $results->where("a.doc_no", "ilike", "%".$request->doc_no."%");   };
//             if ($request->asset_id)
//             {   $results->where("b.asset_id", "ilike", "%".$request->asset_id."%");   };
//             if ($request->item_name)
//             {   $results->where("x1.name", "ilike", "%".$request->item_name."%");   };
//             if ($request->grin_id)
//             {   $results->where("x2.stock_grin_no", "ilike", "%".$request->grin_id."%");   };
//             if ($request->grin_no)
//             {   $results->where("x2.grin_no", "ilike", "%".$request->grin_no."%");   };
//             if ($request->po_no)
//             {   $results->where("x3.po_no", "ilike", "%".$request->po_no."%");   };
//             if ($request->pr_no)
//             {   $results->where("x3.pr_ref_no", "ilike", "%".$request->pr_no."%");   };
            
//             // if ($request->item_type == 'IT')
//             // {   $results->where([
//             //         ["b.ref_item_id", "ilike", "%D/34%"],
//             //         ["b.ref_item_id", "ilike", "%D/36%"]
//             //     ]);   
//             // }
//             // else if ($request->item_type == 'Non-IT')
//             // {   $results->where("b.ref_item_id", "not ilike", "%D/34%");   };

//     // DB::table('name')->whereBetween('column', array(1, 100))->get();
// 	// DB::table('name')->whereIn('column', array(1, 2, 3))->get();
// 	// DB::table('name')->whereNotIn('column', array(1, 2, 3))->get();
// 	// DB::table('name')->whereNull('column')->get();
// 	// DB::table('name')->whereNotNull('column')->get();

//     $tests = $results->get();
//     dd($tests);

	
// 	// return $tests->forPage(1, 10);
// 	return $tests->toArray();
// });
